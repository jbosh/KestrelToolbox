#!/usr/bin/env bash

usage() {
    cat << EOF

    build-nuget.sh <package.nupkg> <feed dir>
    A tool to install packages to a local custom feed without full "nuget.exe".

    package:
        The path to the *.nupkg file.
    feed dir:
        Root directory for the feed.
EOF
}

if [[ "$#" != 2 ]] ; then
    echo "Expected 2 arguments."
    usage
    exit 1
fi

srcPkgFile="$1"
feedDir="$2"
if ! [[ "$srcPkgFile" =~ \.nupkg$ ]] ; then
    echo "Expected first argument to be nupkg file. ($srcPkgFile)"
    usage
    exit 1
fi

if [ ! -f "$srcPkgFile" ] ; then
    echo "Could not find $srcPkgFile"
    exit 1
fi

if [ ! -d "$feedDir" ] ; then
    echo "Could not find dest feed dir $feedDir"
    exit 1
fi

feedDir="$(realpath "$feedDir")"

version="$(echo "${srcPkgFile}" | sed 's/\(.\+\/\)\?\(.\+\)\.\([0-9]\+\.[0-9]\+\.[0-9]\+\(-.\+\)\?\)\.nupkg$/\3/')"
packageName="$(echo "${srcPkgFile}" | sed 's/\(.\+\/\)\?\(.\+\)\.\([0-9]\+\.[0-9]\+\.[0-9]\+\(-.\+\)\?\)\.nupkg$/\2/')"

sha512="$(openssl dgst -sha512 -binary "$srcPkgFile" | base64 -w0)"
echo "Package: $packageName"
echo "Version: $version"

dstDir="$feedDir/${packageName,,}/${version,,}"

mkdir -p "$dstDir"
cp "$srcPkgFile" "$dstDir/${packageName,,}.${version,,}.nupkg" || exit 1
unzip -p "$srcPkgFile" "$packageName.nuspec" > "$dstDir/${packageName,,}.nuspec" || exit 1
echo "$sha512" > "$dstDir/${packageName,,}.${version,,}.nupkg.sha512" || exit 1

