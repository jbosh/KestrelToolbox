// <copyright file="Extensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// A set of extension methods.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Gets the full metadata name for looking up a symbol using compilation services.
    /// </summary>
    /// <param name="symbol">The symbol.</param>
    /// <returns>Metadata name of that symbol.</returns>
    public static string GetFullMetadataName(this ISymbol symbol)
    {
        if (IsRootNamespace(symbol))
        {
            return string.Empty;
        }

        var builder = new StringBuilder(symbol.MetadataName);
        var previous = symbol;

        symbol = symbol.ContainingSymbol;

        while (!IsRootNamespace(symbol))
        {
            if (symbol is ITypeSymbol && previous is ITypeSymbol)
            {
                _ = builder.Insert(0, '+');
            }
            else
            {
                _ = builder.Insert(0, '.');
            }

            _ = builder.Insert(
                0,
                symbol.OriginalDefinition.ToDisplayString(SymbolDisplayFormat.MinimallyQualifiedFormat)
            );
            symbol = symbol.ContainingSymbol;
        }

        return builder.ToString();
    }

    /// <summary>
    /// Gets a value indicating whether <paramref name="source"/> type can be coerced into <paramref name="target"/>
    /// type.
    /// </summary>
    /// <param name="source">The original type needing coercion.</param>
    /// <param name="target">The target type to be coerced into.</param>
    /// <returns>True if coercion is possible, false if not.</returns>
    public static bool CanCoerceTo(this ITypeSymbol source, ITypeSymbol target)
    {
        if (source.Equals(target, SymbolEqualityComparer.Default))
        {
            return true;
        }

        if (source is INamedTypeSymbol namedTypeSymbol)
        {
            foreach (var @interface in namedTypeSymbol.Interfaces)
            {
                if (@interface.Equals(target, SymbolEqualityComparer.Default))
                {
                    return true;
                }
            }

            var baseType = namedTypeSymbol.BaseType;
            if (baseType != null)
            {
                return CanCoerceTo(baseType, target);
            }
        }

        return false;
    }

    private static bool IsRootNamespace(ISymbol symbol) => symbol is INamespaceSymbol { IsGlobalNamespace: true };

    private static SymbolDisplayFormat GetFullNameFormat { get; } =
        new(
            globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Included,
            typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
            genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
            miscellaneousOptions: SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
                | SymbolDisplayMiscellaneousOptions.UseSpecialTypes
                | SymbolDisplayMiscellaneousOptions.IncludeNullableReferenceTypeModifier
                | SymbolDisplayMiscellaneousOptions.ExpandNullable
        );

    /// <summary>
    /// Gets the full name of a symbol that's usable to reference that type in code.
    /// </summary>
    /// <param name="symbol">The symbol.</param>
    /// <returns>Stringified name of that symbol.</returns>
    public static string GetFullName(this ISymbol symbol) => symbol.ToDisplayString(GetFullNameFormat);

    /// <summary>
    /// Gets the name for <see cref="MethodReferenceKey"/>.
    /// </summary>
    /// <param name="symbol">The symbol.</param>
    /// <returns>Name for <see cref="MethodReferenceKey"/>.</returns>
    public static string GetMethodKeyName(this ITypeSymbol symbol) =>
        symbol.IsValueType ? symbol.GetFullName() : $"{symbol.GetFullNameNonNullable()}?";

    /// <summary>
    /// Converts a string to a string literal in c# land.
    /// </summary>
    /// <param name="value">The value to encode.</param>
    /// <returns>The encoded string value that contains quotes.</returns>
    public static string ToLiteral(this string value)
    {
        return SyntaxFactory
            .LiteralExpression(SyntaxKind.StringLiteralExpression, SyntaxFactory.Literal(value))
            .ToFullString();
    }

    /// <summary>
    /// Gets the full name of a symbol that's usable to reference that type in code.
    /// </summary>
    /// <param name="symbol">The symbol.</param>
    /// <returns>Stringified name of that symbol.</returns>
    public static string GetFullNameNonNullable(this ISymbol symbol)
    {
        var fullName = symbol.GetFullName();
        return fullName.TrimEnd('?');
    }

    [SuppressMessage(
        "StyleCop.CSharp.ReadabilityRules",
        "SA1118:Parameter should not span multiple lines",
        Justification = "It's long."
    )]
    private static readonly SymbolDisplayFormat GetMethodNameFormat = new(
        globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Included,
        typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
        propertyStyle: SymbolDisplayPropertyStyle.NameOnly,
        genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
        memberOptions: SymbolDisplayMemberOptions.IncludeContainingType
            | SymbolDisplayMemberOptions.IncludeExplicitInterface,
        miscellaneousOptions: SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
            | SymbolDisplayMiscellaneousOptions.UseSpecialTypes
            | SymbolDisplayMiscellaneousOptions.UseAsterisksInMultiDimensionalArrays
            | SymbolDisplayMiscellaneousOptions.UseErrorTypeSymbolName
            | SymbolDisplayMiscellaneousOptions.ExpandNullable
    );

    /// <summary>
    /// Gets the full name of a symbol that's usable to reference that type in code. This is for methods.
    /// </summary>
    /// <param name="symbol">The symbol.</param>
    /// <returns>Stringified name of that symbol for use when calling it.</returns>
    public static string GetFullMethodName(this IMethodSymbol symbol)
    {
        return symbol.ToDisplayString(GetMethodNameFormat);
    }

    /// <summary>
    /// Gets the friendly name attribute for a symbol.
    /// </summary>
    /// <param name="symbol">The symbol.</param>
    /// <param name="result">Result or null if it does not exist.</param>
    /// <returns>True if the friendly name attribute was found, false if not.</returns>
    public static bool TryGetFriendlyNameAttribute(this ITypeSymbol symbol, [NotNullWhen(true)] out string? result)
    {
        foreach (var attribute in symbol.GetAttributes())
        {
            if (
                attribute.AttributeClass?.GetFullName()
                == "global::KestrelToolbox.Serialization.DataAnnotations.FriendlyNameAttribute"
            )
            {
                if (
                    attribute.ConstructorArguments.Length == 0
                    || attribute.ConstructorArguments[0].Value is not string value
                )
                {
                    // Silently ignore this. Compiler should catch this for user.
                    continue;
                }

                result = value;
                return true;
            }
        }

        result = default;
        return false;
    }

    private static SymbolDisplayFormat GetMethodByNameFormat { get; } =
        new(
            globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Included,
            typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
            genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
            miscellaneousOptions: SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
                | SymbolDisplayMiscellaneousOptions.UseSpecialTypes
                | SymbolDisplayMiscellaneousOptions.IncludeNullableReferenceTypeModifier
                | SymbolDisplayMiscellaneousOptions.ExpandNullable,
            memberOptions: SymbolDisplayMemberOptions.IncludeParameters
                | SymbolDisplayMemberOptions.IncludeContainingType
        );

    /// <summary>
    /// Gets all methods by a fully qualified name within <paramref name="containingSymbol"/>.
    /// </summary>
    /// <param name="compilation">Compilation unit to use for search.</param>
    /// <param name="containingSymbol">The containing symbol to start searching for the method.</param>
    /// <param name="fullyQualifiedName">Fully qualified name of the method.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The list of method symbols that match the method name. This makes it possible to find overloaded methods.</returns>
    public static List<IMethodSymbol> GetMethodsByName(
        this Compilation compilation,
        ISymbol containingSymbol,
        string fullyQualifiedName,
        CancellationToken cancellationToken
    )
    {
        string methodName;
        string? methodQualifiers;
        var methodNameIndex = fullyQualifiedName.LastIndexOf('.');
        if (methodNameIndex == -1)
        {
            methodName = fullyQualifiedName;
            methodQualifiers = null;
        }
        else
        {
            methodName = fullyQualifiedName.Substring(methodNameIndex + 1);
            methodQualifiers = fullyQualifiedName.Remove(methodNameIndex);
        }

        var allSymbols = compilation
            .GetSymbolsWithName(methodName, SymbolFilter.Member, cancellationToken)
            .OfType<IMethodSymbol>()
            .ToArray();
        while (containingSymbol != null)
        {
            var queryName =
                methodQualifiers == null
                    ? $"{containingSymbol.ToDisplayString(GetMethodByNameFormat)}.{methodName}("
                    : $"{containingSymbol.ToDisplayString(GetMethodByNameFormat)}.{methodQualifiers}.{methodName}(";

            var symbols = allSymbols
                .Where(s => s.ToDisplayString(GetMethodByNameFormat).StartsWith(queryName, StringComparison.Ordinal))
                .ToList();
            if (symbols.Count != 0)
            {
                return symbols;
            }

            containingSymbol = containingSymbol.ContainingSymbol;
            if (containingSymbol is IModuleSymbol)
            {
                queryName = methodQualifiers == null ? $"{methodName}(" : $"{methodQualifiers}.{methodName}(";

                symbols = allSymbols
                    .Where(s =>
                        s.ToDisplayString(GetMethodByNameFormat).StartsWith(queryName, StringComparison.Ordinal)
                    )
                    .ToList();
                if (symbols.Count != 0)
                {
                    return symbols;
                }

                break;
            }
        }

        return new List<IMethodSymbol>();
    }

    /// <summary>
    /// Gets whether a specific attribute exists on a method.
    /// </summary>
    /// <param name="methodSymbol">The method symbol.</param>
    /// <param name="attributeFullName">Full name of the attribute to search for.</param>
    /// <returns>True if the attribute exists, false if not.</returns>
    public static bool AttributeExists(IMethodSymbol methodSymbol, string attributeFullName)
    {
        return Enumerable.Any(
            methodSymbol.GetAttributes(),
            attribute => attribute.AttributeClass?.GetFullName() == attributeFullName
        );
    }

    /// <summary>
    /// Converts a <see cref="Accessibility"/> to the CSharp type string accessibility.
    /// </summary>
    /// <param name="accessibility">Accessibility to use.</param>
    /// <returns>The csharp version.</returns>
    public static string ToCSharpString(this Accessibility accessibility)
    {
        return accessibility switch
        {
            Accessibility.Private => "private",
            Accessibility.Public => "public",
            Accessibility.Internal => "internal",
            Accessibility.Protected => "protected",
            Accessibility.ProtectedAndInternal => "protected internal",
            Accessibility.NotApplicable => "public",
            Accessibility.ProtectedOrInternal => "protected internal",
            _ => "public",
        };
    }
}
