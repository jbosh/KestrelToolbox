// <copyright file="CodeBuilder.SyntaxGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// Class for building code like a <see cref="StringBuilder"/>.
/// </summary>
public partial class CodeBuilder
{
    private static readonly SymbolDisplayFormat FullyQualifiedDisplayFormat = new(
        globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Included,
        typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
        genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
        miscellaneousOptions: SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
            | SymbolDisplayMiscellaneousOptions.UseSpecialTypes
            | SymbolDisplayMiscellaneousOptions.ExpandNullable,
        memberOptions: SymbolDisplayMemberOptions.IncludeContainingType
    );

    /// <summary>
    /// Generates syntax for expressions.
    /// </summary>
    /// <param name="semanticModel">The semantic model.</param>
    /// <param name="syntax">The expression to generate code for.</param>
    /// <returns>The generated code.</returns>
    public static string Generate(SemanticModel semanticModel, ExpressionSyntax syntax)
    {
        switch (syntax)
        {
            case ArrayCreationExpressionSyntax arrayCreationExpressionSyntax:
            {
                var arrayType = arrayCreationExpressionSyntax.Type;
                var type = semanticModel
                    .GetSymbolInfo(arrayType.ElementType)
                    .Symbol?.ToDisplayString(FullyQualifiedDisplayFormat);
                var rank = string.Concat(
                    arrayType.RankSpecifiers.Select(r =>
                        $"[{string.Join(", ", r.Sizes.Select(s => Generate(semanticModel, s)))}]"
                    )
                );
                var initializer = Generate(semanticModel, arrayCreationExpressionSyntax.Initializer);
                if (initializer == null)
                {
                    return $"new {type}{rank}";
                }

                return $"new {type}{rank} {{ {initializer} }}";
            }

            case GenericNameSyntax genericNameSyntax:
            {
                var symbol = semanticModel.GetSymbolInfo(genericNameSyntax);
                return symbol.Symbol?.ToDisplayString(FullyQualifiedDisplayFormat) ?? string.Empty;
            }

            case IdentifierNameSyntax identifierNameSyntax:
            {
                var symbol = semanticModel.GetSymbolInfo(identifierNameSyntax);

                return symbol.Symbol?.ToDisplayString(FullyQualifiedDisplayFormat) ?? string.Empty;
            }

            case LiteralExpressionSyntax literalExpressionSyntax:
            {
                return literalExpressionSyntax.Token.Text;
            }

            case ImplicitObjectCreationExpressionSyntax implicitObjectCreationExpressionSyntax:
            {
                return $"new({Generate(semanticModel, implicitObjectCreationExpressionSyntax.ArgumentList)})";
            }

            case InvocationExpressionSyntax invocationExpressionSyntax:
            {
                var arguments = Generate(semanticModel, invocationExpressionSyntax.ArgumentList);
                if (invocationExpressionSyntax.Expression is MemberAccessExpressionSyntax memberAccessExpression)
                {
                    var symbol = semanticModel.GetSymbolInfo(memberAccessExpression.Name);
                    if (symbol.Symbol is IMethodSymbol methodSymbol && methodSymbol.IsExtensionMethod)
                    {
                        // This is an extension method. Need to rearrange the callback.
                        var expression = Generate(semanticModel, memberAccessExpression.Expression);
                        if (arguments != string.Empty)
                        {
                            arguments = $"{expression}, {arguments}";
                        }
                        else
                        {
                            arguments = expression;
                        }

                        var reducedFrom = methodSymbol.ReducedFrom!;
                        var reducedParts = reducedFrom.ToDisplayParts(FullyQualifiedDisplayFormat);
                        var extensionMethodName = MakeSpecific(reducedParts, methodSymbol.TypeArguments);

                        return $"{extensionMethodName}({arguments})";
                    }
                }

                return $"{Generate(semanticModel, invocationExpressionSyntax.Expression)}({arguments})";

                [SuppressMessage("Style", "IDE0010:Add missing cases", Justification = "There's a lot.")]
                static string MakeSpecific(
                    ImmutableArray<SymbolDisplayPart> parts,
                    ImmutableArray<ITypeSymbol> typeArguments
                )
                {
                    var typeIndex = 0;
                    var outputParts = new string?[parts.Length];
                    for (var i = 0; i < parts.Length; i++)
                    {
                        var part = parts[i];
                        switch (part.Kind)
                        {
                            case SymbolDisplayPartKind.TypeParameterName:
                            {
                                var type = typeArguments[typeIndex];
                                typeIndex++;
                                outputParts[i] = type.ToDisplayString(FullyQualifiedDisplayFormat);
                                break;
                            }

                            default:
                            {
                                outputParts[i] = part.ToString();
                                break;
                            }
                        }
                    }

                    return string.Concat(outputParts);
                }
            }

            case MemberAccessExpressionSyntax memberAccessExpression:
            {
                var expression = Generate(semanticModel, memberAccessExpression.Expression);
                var name = memberAccessExpression.Name;
                return $"{expression}.{name}";
            }

            case ObjectCreationExpressionSyntax objectCreationExpressionSyntax:
            {
                var type = semanticModel
                    .GetSymbolInfo(objectCreationExpressionSyntax.Type)
                    .Symbol?.ToDisplayString(FullyQualifiedDisplayFormat);
                var initializer = Generate(semanticModel, objectCreationExpressionSyntax.Initializer);
                var arguments = Generate(semanticModel, objectCreationExpressionSyntax.ArgumentList);
                if (initializer == null)
                {
                    return $"new {type}({arguments})";
                }

                return $"new {type}({arguments}) {{ {initializer} }}";
            }

            case PostfixUnaryExpressionSyntax postfixUnaryExpressionSyntax:
            {
                var operand = Generate(semanticModel, postfixUnaryExpressionSyntax.Operand);
                var token = postfixUnaryExpressionSyntax.OperatorToken.Text;
                return $"{operand}{token}";
            }

            case NullableTypeSyntax:
            {
                return "null";
            }

            default:
            {
                return syntax.GetText().ToString();
            }
        }
    }

    /// <summary>
    /// Generates syntax for expressions.
    /// </summary>
    /// <param name="semanticModel">The semantic model.</param>
    /// <param name="listSyntax">The expression list to generate code for.</param>
    /// <returns>The generated code.</returns>
    public static string Generate(SemanticModel semanticModel, ArgumentListSyntax? listSyntax)
    {
        if (listSyntax == null || listSyntax.Arguments.Count == 0)
        {
            return string.Empty;
        }

        return string.Join(", ", listSyntax.Arguments.Select(a => Generate(semanticModel, a)));
    }

    /// <summary>
    /// Generates syntax for expressions.
    /// </summary>
    /// <param name="semanticModel">The semantic model.</param>
    /// <param name="argumentSyntax">The argument to generate code for.</param>
    /// <returns>The generated code.</returns>
    public static string Generate(SemanticModel semanticModel, ArgumentSyntax argumentSyntax)
    {
        if (argumentSyntax.NameColon != null)
        {
            return $"{argumentSyntax.NameColon.Name.GetText()}: {Generate(semanticModel, argumentSyntax.Expression)}";
        }

        return Generate(semanticModel, argumentSyntax.Expression);
    }

    /// <summary>
    /// Generates syntax for expressions.
    /// </summary>
    /// <param name="semanticModel">The semantic model.</param>
    /// <param name="initializerSyntax">The initializer to generate code for.</param>
    /// <returns>The generated code.</returns>
    public static string? Generate(SemanticModel semanticModel, InitializerExpressionSyntax? initializerSyntax)
    {
        if (initializerSyntax == null || initializerSyntax.Expressions.Count == 0)
        {
            return null;
        }

        return string.Join(", ", initializerSyntax.Expressions.Select(e => Generate(semanticModel, e)));
    }
}
