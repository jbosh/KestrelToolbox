// <copyright file="MethodReferenceKey.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.CodeGen.Serializable;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// Key for managing multiple methods in a <see cref="SerializableAttributesGenerator"/>.
/// </summary>
[SuppressMessage(
    "Design",
    "CA1036:Override methods on comparable types",
    Justification = "Only used for keys in dictionary."
)]
public sealed class MethodReferenceKey : IComparable<MethodReferenceKey>, IEquatable<MethodReferenceKey>
{
    /// <summary>
    /// Gets type of serializable attribute this method belongs to.
    /// </summary>
    public SerializableAttributeType Type { get; }

    /// <summary>
    /// Gets the global fully qualified symbol name.
    /// </summary>
    public string SymbolName { get; }

    /// <inheritdoc />
    public override string ToString() => $"{this.Type} '{this.SymbolName}'";

    /// <summary>
    /// Initializes a new instance of the <see cref="MethodReferenceKey"/> class.
    /// </summary>
    /// <param name="type">Serializable type.</param>
    /// <param name="symbolName">Full name of the symbol.</param>
    public MethodReferenceKey(SerializableAttributeType type, string symbolName)
    {
        this.Type = type;
        this.SymbolName = symbolName;
    }

    /// <inheritdoc />
    public int CompareTo(MethodReferenceKey? other)
    {
        if (other is null)
        {
            return 1;
        }

        var compare = this.Type.CompareTo(other.Type);
        if (compare != 0)
        {
            return compare;
        }

        return string.Compare(this.SymbolName, other.SymbolName, StringComparison.Ordinal);
    }

    /// <inheritdoc />
    public bool Equals(MethodReferenceKey? other)
    {
        return this.Type == other?.Type && this.SymbolName == other.SymbolName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return this.Equals(obj as MethodReferenceKey);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Type, this.SymbolName);
    }

    /// <summary>
    /// Compares two <see cref="MethodReferenceKey"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(MethodReferenceKey? left, MethodReferenceKey? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="MethodReferenceKey"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(MethodReferenceKey? left, MethodReferenceKey? right)
    {
        return !Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="MethodReferenceKey"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator <=(MethodReferenceKey? left, MethodReferenceKey? right)
    {
        if (ReferenceEquals(left, right))
        {
            return true;
        }

        if (left is null)
        {
            return false;
        }

        if (right is null)
        {
            return true;
        }

        return left.CompareTo(right) <= 0;
    }

    /// <summary>
    /// Compares two <see cref="MethodReferenceKey"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator <(MethodReferenceKey? left, MethodReferenceKey? right)
    {
        if (ReferenceEquals(left, right))
        {
            return false;
        }

        if (left is null)
        {
            return false;
        }

        if (right is null)
        {
            return true;
        }

        return left.CompareTo(right) < 0;
    }

    /// <summary>
    /// Compares two <see cref="MethodReferenceKey"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator >=(MethodReferenceKey? left, MethodReferenceKey? right)
    {
        if (ReferenceEquals(left, right))
        {
            return true;
        }

        if (left is null)
        {
            return true;
        }

        if (right is null)
        {
            return false;
        }

        return left.CompareTo(right) >= 0;
    }

    /// <summary>
    /// Compares two <see cref="MethodReferenceKey"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator >(MethodReferenceKey? left, MethodReferenceKey? right)
    {
        if (ReferenceEquals(left, right))
        {
            return false;
        }

        if (left is null)
        {
            return true;
        }

        if (right is null)
        {
            return false;
        }

        return left.CompareTo(right) > 0;
    }
}
