// <copyright file="TypedefAttributesExecutor.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

// #define PRINT_BUILD_COUNT
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Typedefs;

/// <summary>
/// Executor to generate route attribute stuff.
/// </summary>
public sealed class TypedefAttributesExecutor : IEquatable<TypedefAttributesExecutor>
{
#if PRINT_BUILD_COUNT
    private static readonly Dictionary<string, int> BuildCounts = new();
#endif // PRINT_BUILD_COUNT

    private readonly List<Diagnostic> diagnostics;
    private readonly CacheableTypedefAttribute attribute;

    /// <summary>
    /// Initializes a new instance of the <see cref="TypedefAttributesExecutor"/> class.
    /// </summary>
    /// <param name="attribute">Attributes to generate for.</param>
    /// <param name="diagnostics">Diagnostics for this type.</param>
    public TypedefAttributesExecutor(CacheableTypedefAttribute attribute, List<Diagnostic> diagnostics)
    {
        this.attribute = attribute;
        this.diagnostics = diagnostics;
    }

    /// <summary>
    /// Generate code for this class.
    /// </summary>
    /// <param name="context">Context of source generation.</param>
    public void Execute(SourceProductionContext context)
    {
        if (this.diagnostics.Count != 0)
        {
            foreach (var diagnostic in this.diagnostics)
            {
                context.ReportDiagnostic(diagnostic);
            }
        }

        var attribute = this.attribute;
        var declaringType = attribute.DeclaringTypeDeclaration;
        var aliasType = attribute.AliasType;
        var features = attribute.TypedefFeatures;

        var builder = new CodeBuilder();

#if PRINT_BUILD_COUNT
        lock (BuildCounts)
        {
            _ = BuildCounts.TryGetValue(classSymbol.FullName, out var buildCount);
            BuildCounts[classSymbol.FullName] = buildCount + 1;
            globalBuilder.AppendLine($"// Route build count: {buildCount}");
        }
#endif // PRINT_BUILD_COUNT

        if (declaringType.Namespace.Length != 0)
        {
            builder.AppendLine($"namespace {declaringType.Namespace};");
            builder.AppendLine();
        }

        builder.AppendLine("#nullable enable");

        // Build up class list so we can have the correct type.
        foreach (var containingType in declaringType.ContainingTypes.Take(declaringType.ContainingTypes.Length - 1))
        {
            builder.AppendLine(containingType);
            builder.PushBlock();
        }

        var declaringTypeDeclaration = declaringType.ContainingTypes[declaringType.ContainingTypes.Length - 1];
        if (features.HasFlag(TypedefFeatures.Equatable))
        {
            builder.AppendLine($"{declaringTypeDeclaration} : global::System.IEquatable<{declaringType.Name}>");
        }
        else
        {
            builder.AppendLine(declaringTypeDeclaration);
        }

        builder.PushBlock();

        builder.AppendLine($"private readonly {aliasType.FullName} value;");
        builder.AppendLine();

        builder.AppendAndSplitLines(
            $$"""
            /// <summary>
            /// Initializes a new instance of the <see cref="{{declaringType.Name}}"/> class.
            /// </summary>
            /// <param name="value">Value to alias using this typedef.</param>
            """
        );
        builder.AppendLine($"public {declaringType.Name}({aliasType.FullName} value) => this.value = value;");
        builder.AppendLine();

        if (features.HasFlag(TypedefFeatures.FromMethods))
        {
            builder.AppendAndSplitLines(
                $$"""
                /// <summary>
                /// Initializes a new instance of the <see cref="{{declaringType.Name}}"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                /// <returns>New instance of the <see cref="{{declaringType.Name}}"/> class.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                """
            );
            if (aliasType.IsNullable || aliasType.IsValueType)
            {
                builder.AppendLine(
                    $"public static {declaringType.Name}? From({aliasType.FullName}? value) => value is null ? null : new {declaringType.Name}(value.Value);"
                );
            }
            else
            {
                builder.AppendLine(
                    $"public static {declaringType.Name}? From({aliasType.FullName}? value) => value is null ? null : new {declaringType.Name}(value);"
                );
            }

            builder.AppendLine();

            if (aliasType.IsNullable || aliasType.IsValueType)
            {
                builder.AppendAndSplitLines(
                    $$"""
                    /// <summary>
                    /// Initializes a new instance of the <see cref="{{declaringType.Name}}"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    /// <returns>New instance of the <see cref="{{declaringType.Name}}"/> class.</returns>
                    """
                );
                builder.AppendLine(
                    $"public static {declaringType.Name} From({aliasType.FullName} value) => new {declaringType.Name}(value);"
                );
                builder.AppendLine();
            }
        }

        if (aliasType.IsNullable || !aliasType.IsValueType)
        {
            builder.AppendAndSplitLines(
                """
                /// <summary>
                /// Gets the aliased type of this typedef.
                /// </summary>
                /// <param name="value">Value to cast into the aliased type.</param>
                /// <returns>The aliased type.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                """
            );

            builder.AppendLine(
                $"public static explicit operator {aliasType.FullName}?({declaringType.Name}? value) => value?.value;"
            );
            builder.AppendLine();

            builder.AppendAndSplitLines(
                """
                /// <summary>
                /// Gets a typedef from the aliased type.
                /// </summary>
                /// <param name="value">Value to cast into the typedef.</param>
                /// <returns>The typedef.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                """
            );
            builder.AppendLine(
                $"public static explicit operator {declaringType.Name}?({aliasType.FullName}? value) => value == null ? null : new {declaringType.Name}(value);"
            );
            builder.AppendLine();
        }
        else
        {
            builder.AppendAndSplitLines(
                """
                /// <summary>
                /// Gets the aliased type of this typedef.
                /// </summary>
                /// <param name="value">Value to cast into the aliased type.</param>
                /// <returns>The aliased type.</returns>
                """
            );

            builder.AppendLine(
                $"public static explicit operator {aliasType.FullName}({declaringType.Name} value) => value.value;"
            );
            builder.AppendLine();

            builder.AppendAndSplitLines(
                """
                /// <summary>
                /// Gets a typedef from the aliased type.
                /// </summary>
                /// <param name="value">Value to cast into the typedef.</param>
                /// <returns>The typedef.</returns>
                """
            );
            builder.AppendLine(
                $"public static explicit operator {declaringType.Name}({aliasType.FullName} value) => new {declaringType.Name}(value);"
            );
            builder.AppendLine();
        }

        if (features.HasFlag(TypedefFeatures.Equatable))
        {
            builder.AppendLine("/// <inheritdoc />");
            var questionMarkInEquals = attribute.DeclaringType.IsValueType ? string.Empty : "?";
            builder.AppendLine(
                $"public bool Equals({declaringType.Name}{questionMarkInEquals} other) => this.value.Equals(other{questionMarkInEquals}.value);"
            );
            builder.AppendLine();

            builder.AppendAndSplitLines(
                """
                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                """
            );
            if (attribute.DeclaringType.IsValueType)
            {
                builder.AppendLine(
                    $"public static bool operator==({declaringType.Name} left, {declaringType.Name} right) => left.Equals(right);"
                );
            }
            else
            {
                builder.AppendLine(
                    $"public static bool operator==({declaringType.Name}? left, {declaringType.Name}? right) => object.Equals(left, right);"
                );
            }

            builder.AppendLine();

            builder.AppendAndSplitLines(
                """
                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                """
            );
            if (attribute.DeclaringType.IsValueType)
            {
                builder.AppendLine(
                    $"public static bool operator!=({declaringType.Name} left, {declaringType.Name} right) => !left.Equals(right);"
                );
            }
            else
            {
                builder.AppendLine(
                    $"public static bool operator!=({declaringType.Name}? left, {declaringType.Name}? right) => !object.Equals(left, right);"
                );
            }

            builder.AppendLine();
        }

        builder.AppendAndSplitLines(attribute.AliasedMembers);

        for (var i = 0; i < declaringType.ContainingTypes.Length; i++)
        {
            builder.PopBlock();
        }

        var contents = builder.ToString();
        var sourcePath = $"Typedef/{declaringType.FullName}.g.cs";
        sourcePath = sourcePath.Replace("global::", string.Empty).Replace('<', '(').Replace('>', ')');
        context.AddSource(sourcePath, contents);
    }

    /// <inheritdoc />
    public bool Equals(TypedefAttributesExecutor? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.diagnostics.SequenceEqual(other.diagnostics) && this.attribute.Equals(other.attribute);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not TypedefAttributesExecutor other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.attribute);
    }

    /// <summary>
    /// Compares two <see cref="TypedefAttributesExecutor"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(TypedefAttributesExecutor? left, TypedefAttributesExecutor? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="TypedefAttributesExecutor"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(TypedefAttributesExecutor? left, TypedefAttributesExecutor? right)
    {
        return !Equals(left, right);
    }
}
