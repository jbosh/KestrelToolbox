// <copyright file="TypedefAttributesGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Typedefs;

/// <summary>
/// Generator for TypedefAttributes.
/// </summary>
public sealed class TypedefAttributesGenerator : IIncrementalGenerator
{
    /// <inheritdoc />
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        var foundMethods = context
            .SyntaxProvider.ForAttributeWithMetadataName(
                "KestrelToolbox.Types.TypedefAttribute",
                IsValidQuick,
                GatherTypedefAttributes
            )
            .Where(n => n != null);
        var combinedClasses = foundMethods.Where(n => n != null);
        context.RegisterSourceOutput(combinedClasses, static (ctx, s) => s!.Execute(ctx));
    }

    private static bool IsValidQuick(SyntaxNode syntaxNode, CancellationToken cancellationToken)
    {
        if (!syntaxNode.IsKind(SyntaxKind.ClassDeclaration) && !syntaxNode.IsKind(SyntaxKind.StructDeclaration))
        {
            return false;
        }

        return true;
    }

    private static TypedefAttributesExecutor? GatherTypedefAttributes(
        GeneratorAttributeSyntaxContext context,
        CancellationToken cancellationToken
    )
    {
        var semanticModel = context.SemanticModel;
        var typedefContext = new TypedefGeneratorContext(semanticModel);
        var typeDeclarationSyntax = (TypeDeclarationSyntax)context.TargetNode;

        var typeDeclarationSymbol = semanticModel.GetDeclaredSymbol(typeDeclarationSyntax, cancellationToken);
        if (typeDeclarationSymbol == null)
        {
            return null;
        }

        var typedefDeclaration = CacheableTypedefAttribute.GetTypedefFromDeclaration(
            typeDeclarationSyntax,
            semanticModel,
            cancellationToken
        );
        if (typedefDeclaration == null)
        {
            return null;
        }

        if (!typeDeclarationSyntax.Modifiers.Any(m => m.IsKind(SyntaxKind.PartialKeyword)))
        {
            typedefContext.ReportDiagnostic(
                DiagnosticMessages.CreateTypedefClassIsNotPartial(
                    typeDeclarationSyntax.GetLocation(),
                    typeDeclarationSyntax.Identifier.Text
                )
            );
        }

        var aliasType = typedefDeclaration.Value.alias;
        var features = typedefDeclaration.Value.features;

        var builder = new CodeBuilder();
        if (features.HasFlag(TypedefFeatures.AliasMethods))
        {
            foreach (var member in aliasType.GetMembers())
            {
                switch (member)
                {
                    case IMethodSymbol methodSymbol:
                    {
                        if (methodSymbol.IsStatic || methodSymbol.DeclaredAccessibility != Accessibility.Public)
                        {
                            continue;
                        }

                        switch (methodSymbol.MethodKind)
                        {
                            case MethodKind.Ordinary:
                            {
                                var isDotnet9 = methodSymbol.Parameters.Any(p => p.IsParamsCollection);
                                var returnType = methodSymbol.ReturnType.GetFullName();
                                var xmlDocParameters = string.Join(
                                    ",",
                                    methodSymbol.Parameters.Select(GetXmlDocArgument)
                                );
                                var parameters = string.Join(
                                    ", ",
                                    methodSymbol.Parameters.Select(p => p.ToDisplayString())
                                );
                                var arguments = string.Join(", ", methodSymbol.Parameters.Select(GetArgument));

                                var overrideString = default(string);
                                var isEquals = false;
                                for (
                                    var overridenMethod = methodSymbol.OverriddenMethod;
                                    overridenMethod != null;
                                    overridenMethod = overridenMethod.OverriddenMethod
                                )
                                {
                                    switch (overridenMethod.ToDisplayString())
                                    {
                                        case "object.ToString()":
                                        case "object.GetHashCode()":
                                        {
                                            overrideString = "override ";
                                            break;
                                        }

                                        case "object.Equals(object?)":
                                        {
                                            isEquals = true;
                                            break;
                                        }

                                        default:
                                        {
                                            // Not inherited.
                                            break;
                                        }
                                    }
                                }

                                if (isDotnet9)
                                {
                                    builder.AppendPreprocessor("#if NET9_0_OR_GREATER");
                                }

                                builder.AppendLine(
                                    $"/// <inheritdoc cref=\"{aliasType.GetFullName()}.{methodSymbol.Name}({xmlDocParameters})\" />"
                                );
                                if (isEquals)
                                {
                                    builder.AppendLine("public override bool Equals(object? obj) =>");
                                    builder.PushIndent();
                                    builder.AppendLine(
                                        $"obj is {typeDeclarationSymbol.GetFullNameNonNullable()} other && this.Equals(other);"
                                    );
                                    builder.PopIndent();
                                }
                                else
                                {
                                    builder.AppendLine(
                                        $"public {overrideString}{returnType} {methodSymbol.Name}({parameters}) =>"
                                    );
                                    builder.PushIndent();
                                    builder.AppendLine($"this.value.{methodSymbol.Name}({arguments});");
                                    builder.PopIndent();
                                }

                                if (isDotnet9)
                                {
                                    builder.AppendPreprocessor("#endif // NET9_0_OR_GREATER");
                                }

                                builder.AppendLine();

                                break;

                                static string GetArgument(IParameterSymbol parameter)
                                {
                                    var refKind = parameter.RefKind switch
                                    {
                                        RefKind.Out => "out ",
                                        RefKind.Ref => "ref ",
                                        RefKind.None => string.Empty,
                                        RefKind.In => string.Empty,
                                        RefKind.RefReadOnlyParameter => "ref readonly ",
                                        _ => string.Empty,
                                    };

                                    return $"{refKind}{parameter.Name}";
                                }
                            }

                            case MethodKind.Constructor:
                            case MethodKind.PropertyGet:
                            case MethodKind.PropertySet:
                            {
                                // Don't generate these types. They're either generated elsewhere or not useful.
                                break;
                            }

                            case MethodKind.AnonymousFunction:
                            case MethodKind.Conversion:
                            case MethodKind.DelegateInvoke:
                            case MethodKind.Destructor:
                            case MethodKind.EventAdd:
                            case MethodKind.EventRaise:
                            case MethodKind.EventRemove:
                            case MethodKind.ExplicitInterfaceImplementation:
                            case MethodKind.UserDefinedOperator:
                            case MethodKind.ReducedExtension:
                            case MethodKind.StaticConstructor:
                            case MethodKind.BuiltinOperator:
                            case MethodKind.DeclareMethod:
                            case MethodKind.LocalFunction:
                            case MethodKind.FunctionPointerSignature:
                            default:
                            {
                                // Unknown type, let's just ignore it.
                                break;
                            }
                        }

                        break;
                    }

                    case IPropertySymbol propertySymbol:
                    {
                        if (propertySymbol.IsStatic || propertySymbol.DeclaredAccessibility != Accessibility.Public)
                        {
                            continue;
                        }

                        if (propertySymbol.Parameters.Length != 0)
                        {
                            // This is a `this[]` property.
                            Debug.Assert(propertySymbol.Name == "this[]", "Unexpected property with parameters.");
                            var parameters = string.Join(
                                ", ",
                                propertySymbol.Parameters.Select(s => s.ToDisplayString())
                            );
                            var xmlDocParameters = string.Join(
                                ",",
                                propertySymbol.Parameters.Select(GetXmlDocArgument)
                            );
                            var arguments = string.Join(", ", propertySymbol.Parameters.Select(s => s.Name));
                            builder.AppendLine(
                                $"/// <inheritdoc cref=\"{aliasType.GetFullName()}.this[{xmlDocParameters}]\" />"
                            );
                            builder.AppendLine($"public {propertySymbol.Type.GetFullName()} this[{parameters}]");
                            using (builder.AppendBlock())
                            {
                                if (propertySymbol.GetMethod != null)
                                {
                                    builder.AppendLine($"get => this.value[{arguments}];");
                                }

                                if (propertySymbol.SetMethod != null)
                                {
                                    builder.AppendLine($"set => this.value[{arguments}] = value;");
                                }
                            }
                        }
                        else
                        {
                            builder.AppendLine(
                                $"/// <inheritdoc cref=\"{aliasType.GetFullName()}.{propertySymbol.Name}\" />"
                            );
                            builder.AppendLine($"public {propertySymbol.Type.GetFullName()} {propertySymbol.Name}");
                            using (builder.AppendBlock())
                            {
                                if (propertySymbol.GetMethod != null)
                                {
                                    builder.AppendLine($"get => this.value.{propertySymbol.Name};");
                                }

                                if (propertySymbol.SetMethod != null)
                                {
                                    builder.AppendLine($"set => this.value.{propertySymbol.Name} = value;");
                                }
                            }
                        }

                        break;
                    }

                    default:
                    {
                        // Unknown type, let's just ignore it.
                        break;
                    }
                }
            }
        }

        var cacheableDeclaringDeclarationType = CacheableTypeDeclarationSymbol.FromTypeSymbol(
            typeDeclarationSymbol,
            typeDeclarationSyntax,
            cancellationToken
        );

        var cacheableDeclaringType = CacheableTypeSymbol.FromITypeSymbol(
            typedefContext.SymbolCache,
            typedefContext.ReportDiagnostic,
            typeDeclarationSymbol
        );

        var cacheableAliasType = CacheableTypeSymbol.FromITypeSymbol(
            typedefContext.SymbolCache,
            typedefContext.ReportDiagnostic,
            aliasType
        );

        var body = builder.ToString();
        return new TypedefAttributesExecutor(
            new CacheableTypedefAttribute(
                cacheableDeclaringDeclarationType,
                cacheableDeclaringType,
                cacheableAliasType,
                features,
                body
            ),
            typedefContext.Diagnostics
        );

        static string GetXmlDocArgument(IParameterSymbol parameter)
        {
            var refKind = parameter.RefKind switch
            {
                RefKind.Out => "out ",
                RefKind.Ref => "ref ",
                RefKind.None => string.Empty,
                RefKind.In => string.Empty,
                RefKind.RefReadOnlyParameter => "ref readonly ",
                _ => string.Empty,
            };

            return $"{refKind}{parameter.Type.GetFullNameNonNullable().Replace('<', '{').Replace('>', '}')}";
        }
    }
}
