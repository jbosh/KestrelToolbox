// <copyright file="BoolSerializableSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Attribute for how to serialize bools.
/// </summary>
public sealed class BoolSerializableSettingsAttribute : IEquatable<BoolSerializableSettingsAttribute>
{
    /// <summary>
    /// Gets a value indicating whether the bool can be parsed from values that are mostly correct.
    /// Examples would be <c>True</c>, <c>FALSE</c> or properties with whitespace in them.
    /// </summary>
    public bool AllowFlexibleStrings { get; }

    /// <summary>
    /// Gets a value indicating whether the bool can be parsed from '0' and '1'.
    /// </summary>
    public bool AllowNumbers { get; }

    /// <summary>
    /// Gets a value indicating whether serialization should be done as numbers.
    /// </summary>
    public bool SerializeAsNumbers { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="BoolSerializableSettingsAttribute"/> class.
    /// </summary>
    public BoolSerializableSettingsAttribute() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="BoolSerializableSettingsAttribute"/> class.
    /// </summary>
    /// <param name="allowFlexibleStrings">
    /// A value indicating whether the bool can be parsed from values that are mostly correct.
    /// Examples would be <c>True</c>, <c>FALSE</c> or properties with whitespace in them.
    /// </param>
    /// <param name="allowNumbers">A value indicating whether the bool can be parsed from '0' and '1'.</param>
    /// <param name="serializeAsNumbers">A value indicating whether serialization should be done as numbers.</param>
    public BoolSerializableSettingsAttribute(bool allowFlexibleStrings, bool allowNumbers, bool serializeAsNumbers)
    {
        this.AllowFlexibleStrings = allowFlexibleStrings;
        this.AllowNumbers = allowNumbers;
        this.SerializeAsNumbers = serializeAsNumbers;
    }

    /// <summary>
    /// Gets a new instance of <see cref="BoolSerializableSettingsAttribute"/> from matching attribute data.
    /// </summary>
    /// <param name="attribute">Attribute data for <c>BoolSerializableSettingsAttribute</c>.</param>
    /// <returns>The found settings.</returns>
    public static BoolSerializableSettingsAttribute FromAttribute(AttributeData attribute)
    {
        var allowFlexibleStrings = false;
        var allowNumbers = false;
        var serializeAsNumbers = false;
        foreach (var argument in attribute.NamedArguments)
        {
            switch (argument.Key)
            {
                case "AllowFlexibleStrings":
                {
                    var value = argument.Value;
                    if (value.Value is bool boolValue)
                    {
                        allowFlexibleStrings = boolValue;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                    break;
                }

                case "AllowNumbers":
                {
                    var value = argument.Value;
                    if (value.Value is bool boolValue)
                    {
                        allowNumbers = boolValue;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                    break;
                }

                case "SerializationType":
                {
                    var value = argument.Value;
                    if (
                        value.Type?.GetFullName()
                            == "global::KestrelToolbox.Serialization.DataAnnotations.BoolSerializableSettingsSerializationType"
                        && value.Value is int serializationTypeValue
                    )
                    {
                        serializeAsNumbers = serializationTypeValue == 1;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                    break;
                }

                default:
                {
                    throw new NotImplementedException();
                }
            }
        }

        return new BoolSerializableSettingsAttribute(allowFlexibleStrings, allowNumbers, serializeAsNumbers);
    }

    /// <inheritdoc />
    public bool Equals(BoolSerializableSettingsAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.AllowFlexibleStrings == other.AllowFlexibleStrings
            && this.AllowNumbers == other.AllowNumbers
            && this.SerializeAsNumbers == other.SerializeAsNumbers;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is BoolSerializableSettingsAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.AllowFlexibleStrings, this.AllowNumbers, this.SerializeAsNumbers);
    }

    /// <summary>
    /// Compares two <see cref="BoolSerializableSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(BoolSerializableSettingsAttribute? left, BoolSerializableSettingsAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="BoolSerializableSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(BoolSerializableSettingsAttribute? left, BoolSerializableSettingsAttribute? right)
    {
        return !Equals(left, right);
    }
}
