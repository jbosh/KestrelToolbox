// <copyright file="ValidValuesAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Attribute to specify valid values.
/// </summary>
public sealed class ValidValuesAttribute : IEquatable<ValidValuesAttribute>
{
    /// <summary>
    /// Gets the list of values that are available in C# token form.
    /// </summary>
    /// <remarks>
    /// Strings will be properly quoted and number types will have proper suffixes if necessary.
    /// </remarks>
    public List<string> Values { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidValuesAttribute"/> class.
    /// </summary>
    /// <param name="values">The list of values that are available in C# token form.</param>
    public ValidValuesAttribute(List<string> values)
    {
        this.Values = values;
    }

    /// <summary>
    /// Gets a <see cref="ValidValuesAttribute"/> from a property attribute.
    /// </summary>
    /// <param name="attribute">Property attribute data.</param>
    /// <returns>The new instance of <see cref="ValidValuesAttribute"/>.</returns>
    public static ValidValuesAttribute FromAttribute(AttributeData attribute)
    {
        var values = new List<string>();
        if (attribute.ConstructorArguments.Length != 0)
        {
            // This should be a compiler error for the user before it gets to code gen.
            // Just ignore it silently.
            var args = attribute.ConstructorArguments[0];

            foreach (var value in args.Values.Distinct().OrderBy(v => v.Value))
            {
#pragma warning disable IDE0010
                switch (value.Type?.SpecialType ?? SpecialType.None)
#pragma warning restore IDE0010
                {
                    case SpecialType.System_SByte:
                    case SpecialType.System_Int16:
                    case SpecialType.System_Int32:
                    case SpecialType.System_Int64:
                    case SpecialType.System_Byte:
                    case SpecialType.System_UInt16:
                    case SpecialType.System_UInt32:
                    case SpecialType.System_UInt64:
                    case SpecialType.System_Single:
                    case SpecialType.System_Decimal:
                    case SpecialType.System_Double:
                    case SpecialType.System_String:
                    {
                        // Works (probably).
                        break;
                    }

                    default:
                    {
                        throw new NotImplementedException("Unknown type.");
                    }
                }

                values.Add(value.ToCSharpString());
            }
        }

        return new ValidValuesAttribute(values);
    }

    /// <inheritdoc />
    public bool Equals(ValidValuesAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Values.SequenceEqual(other.Values);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is ValidValuesAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return this.Values.GetHashCode();
    }

    /// <summary>
    /// Compares two <see cref="ValidValuesAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(ValidValuesAttribute? left, ValidValuesAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="ValidValuesAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(ValidValuesAttribute? left, ValidValuesAttribute? right)
    {
        return !Equals(left, right);
    }
}
