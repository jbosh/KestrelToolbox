// <copyright file="DateOnlyParseSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Attribute for specifying how to serialize DateOnly.
/// </summary>
public sealed class DateOnlyParseSettingsAttribute : IEquatable<DateOnlyParseSettingsAttribute>
{
    /// <summary>
    /// Gets the format for serialization.
    /// </summary>
    public string Format { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="DateOnlyParseSettingsAttribute"/> class.
    /// </summary>
    /// <param name="format">The format to serialize with.</param>
    public DateOnlyParseSettingsAttribute(string format)
    {
        this.Format = format;
    }

    /// <summary>
    /// Gets serialization settings from attribute data.
    /// </summary>
    /// <param name="attribute">Attribute data to parse.</param>
    /// <returns><see cref="DateOnlyParseSettingsAttribute"/> instance.</returns>
    public static DateOnlyParseSettingsAttribute FromAttribute(AttributeData attribute)
    {
        var format = "o"; // ISO 8601
        foreach (var namedArgument in attribute.NamedArguments)
        {
#pragma warning disable IDE0010
            switch (namedArgument.Key)
#pragma warning restore IDE0010
            {
                case "Format":
                {
                    format = namedArgument.Value.ToCSharpString();
                    break;
                }
            }
        }

        return new DateOnlyParseSettingsAttribute(format);
    }

    /// <inheritdoc />
    public bool Equals(DateOnlyParseSettingsAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Format == other.Format;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is DateOnlyParseSettingsAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return this.Format.GetHashCode();
    }

    /// <summary>
    /// Compares two <see cref="DateOnlyParseSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(DateOnlyParseSettingsAttribute? left, DateOnlyParseSettingsAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="DateOnlyParseSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(DateOnlyParseSettingsAttribute? left, DateOnlyParseSettingsAttribute? right)
    {
        return !Equals(left, right);
    }
}
