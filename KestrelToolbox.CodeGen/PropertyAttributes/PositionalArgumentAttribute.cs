// <copyright file="PositionalArgumentAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Attribute for how to handle empty arguments.
/// </summary>
public sealed class PositionalArgumentAttribute : IEquatable<PositionalArgumentAttribute>
{
    /// <summary>
    /// Gets name to output when printing help.
    /// </summary>
    public string Name { get; }

    /// <inheritdoc/>
    public override string ToString() => $"PositionalArg({this.Name})";

    /// <summary>
    /// Initializes a new instance of the <see cref="PositionalArgumentAttribute"/> class.
    /// </summary>
    /// <param name="name">The output name when printing help.</param>
    public PositionalArgumentAttribute(string name)
    {
        this.Name = name;
    }

    /// <summary>
    /// Gets a new instance of <see cref="BoolSerializableSettingsAttribute"/> from matching attribute data.
    /// </summary>
    /// <param name="attribute">Attribute data for <c>BoolSerializableSettingsAttribute</c>.</param>
    /// <returns>The found settings.</returns>
    public static PositionalArgumentAttribute FromAttribute(AttributeData attribute)
    {
        var name = attribute.ConstructorArguments[0].ToCSharpString();
        return new PositionalArgumentAttribute(name);
    }

    /// <inheritdoc />
    public bool Equals(PositionalArgumentAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Name == other.Name;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is PositionalArgumentAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Name);
    }

    /// <summary>
    /// Compares two <see cref="PositionalArgumentAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(PositionalArgumentAttribute? left, PositionalArgumentAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="PositionalArgumentAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(PositionalArgumentAttribute? left, PositionalArgumentAttribute? right)
    {
        return !Equals(left, right);
    }
}
