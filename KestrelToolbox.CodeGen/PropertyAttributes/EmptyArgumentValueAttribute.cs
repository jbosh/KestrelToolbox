// <copyright file="EmptyArgumentValueAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Globalization;
using KestrelToolbox.CodeGen.Polyfill;
using KestrelToolbox.CodeGen.Serializable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Attribute for how to handle empty arguments.
/// </summary>
public sealed class EmptyArgumentValueAttribute : IEquatable<EmptyArgumentValueAttribute>
{
    /// <summary>
    /// Gets the csharp value when empty occurs.
    /// </summary>
    public string Value { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The csharp value to use when parsing fails.</param>
    public EmptyArgumentValueAttribute(string value)
    {
        this.Value = value;
    }

    /// <summary>
    /// Gets a new instance of <see cref="BoolSerializableSettingsAttribute"/> from matching attribute data.
    /// </summary>
    /// <param name="context">Context for parsing.</param>
    /// <param name="attribute">Attribute data for <c>BoolSerializableSettingsAttribute</c>.</param>
    /// <returns>The found settings.</returns>
    public static EmptyArgumentValueAttribute? FromAttribute(
        SerializableAttributesGeneratorContext context,
        AttributeData attribute
    )
    {
        switch (attribute.ConstructorArguments.Length)
        {
            case 1:
            {
                var constant = attribute.ConstructorArguments[0];
                var value = constant.ToCSharpString();
                switch (constant.Type?.GetFullName())
                {
                    case "float":
                    {
                        value += "F";
                        break;
                    }

                    default:
                    {
                        // Defaults are fine.
                        break;
                    }
                }

                return new EmptyArgumentValueAttribute(value);
            }

            case 2:
            {
                var type = (INamedTypeSymbol)attribute.ConstructorArguments[0].Value!;
                if (attribute.ConstructorArguments[1].Value is not string value)
                {
                    context.ReportDiagnostic(
                        DiagnosticMessages.CreateInvalidEmptyArgumentValueType(
                            null,
                            "string",
                            attribute.ConstructorArguments[1].Value?.GetType().FullName ?? "null"
                        )
                    );
                    return null;
                }

                switch (type.GetFullName())
                {
                    case "decimal":
                    {
                        if (!decimal.TryParse(value, out var decimalValue))
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateInvalidEmptyArgumentValueType(null, type.GetFullName(), value)
                            );
                            return null;
                        }

                        return new EmptyArgumentValueAttribute($"{decimalValue}M");
                    }

                    case "global::System.TimeSpan":
                    {
                        if (!TimeSpanParser.TryParse(value, out var timeSpanValue))
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateInvalidEmptyArgumentValueType(null, type.GetFullName(), value)
                            );
                            return null;
                        }

                        return new EmptyArgumentValueAttribute($"new global::System.TimeSpan({timeSpanValue.Ticks})");
                    }

                    case "global::System.DateTime":
                    {
                        if (
                            !DateTime.TryParse(
                                value,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None,
                                out var dateTimeValue
                            )
                        )
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateInvalidEmptyArgumentValueType(null, type.GetFullName(), value)
                            );
                            return null;
                        }

                        return new EmptyArgumentValueAttribute(
                            $"new global::System.DateTime({dateTimeValue.Ticks}, global::System.DateTimeKind.{dateTimeValue.Kind})"
                        );
                    }

                    case "global::System.DateTimeOffset":
                    {
                        if (
                            !DateTimeOffset.TryParse(
                                value,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None,
                                out var dateTimeOffsetValue
                            )
                        )
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateInvalidEmptyArgumentValueType(null, type.GetFullName(), value)
                            );
                            return null;
                        }

                        return new EmptyArgumentValueAttribute(
                            $"new global::System.DateTimeOffset({dateTimeOffsetValue.Ticks}, new global::System.TimeSpan({dateTimeOffsetValue.Offset.Ticks}))"
                        );
                    }

                    case "global::System.DateOnly":
                    {
                        return new EmptyArgumentValueAttribute(
                            $"global::System.DateOnly.Parse({attribute.ConstructorArguments[1].ToCSharpString()}, global::System.Globalization.CultureInfo.InvariantCulture)"
                        );
                    }

                    case "global::System.TimeOnly":
                    {
                        return new EmptyArgumentValueAttribute(
                            $"global::System.TimeOnly.Parse({attribute.ConstructorArguments[1].ToCSharpString()}, global::System.Globalization.CultureInfo.InvariantCulture)"
                        );
                    }

                    default:
                    {
                        throw new NotImplementedException();
                    }
                }
            }

            default:
            {
                // Compiler should never let us get here.
                throw new NotImplementedException();
            }
        }
    }

    /// <inheritdoc />
    public bool Equals(EmptyArgumentValueAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return object.Equals(this.Value, other.Value);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is EmptyArgumentValueAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Value);
    }

    /// <summary>
    /// Compares two <see cref="EmptyArgumentValueAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(EmptyArgumentValueAttribute? left, EmptyArgumentValueAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="EmptyArgumentValueAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(EmptyArgumentValueAttribute? left, EmptyArgumentValueAttribute? right)
    {
        return !Equals(left, right);
    }
}
