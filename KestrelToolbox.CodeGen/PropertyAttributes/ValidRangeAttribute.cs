// <copyright file="ValidRangeAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Attribute to specify ranges on a property.
/// </summary>
public sealed class ValidRangeAttribute : IEquatable<ValidRangeAttribute>
{
    /// <summary>
    /// Gets the minimum value that has been made into a C# token.
    /// </summary>
    public string Min { get; }

    /// <summary>
    /// Gets the maximum value that has been made into a C# token.
    /// </summary>
    public string Max { get; }

    /// <summary>
    /// Gets the minimum value as it was originally specified in code.
    /// </summary>
    public string MinOriginal { get; }

    /// <summary>
    /// Gets the maximum value as it was originally specified in code.
    /// </summary>
    public string MaxOriginal { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="min">The minimum value that has been made into a C# token.</param>
    /// <param name="max">The maximum value that has been made into a C# token.</param>
    /// <param name="minOriginal">The minimum value as it was originally specified in code.</param>
    /// <param name="maxOriginal">The maximum value as it was originally specified in code.</param>
    public ValidRangeAttribute(string min, string max, string minOriginal, string maxOriginal)
    {
        this.Min = min;
        this.Max = max;
        this.MinOriginal = minOriginal;
        this.MaxOriginal = maxOriginal;
    }

    /// <summary>
    /// Gets the range attribute from <paramref name="attribute"/>.
    /// </summary>
    /// <param name="attribute">The attribute data for a ValidRangeAttribute.</param>
    /// <returns>The range attribute data.</returns>
    public static ValidRangeAttribute FromAttribute(AttributeData attribute)
    {
        if (attribute.ConstructorArguments.Length == 2)
        {
            var minValue = attribute.ConstructorArguments[0].ToCSharpString();
            var maxValue = attribute.ConstructorArguments[1].ToCSharpString();
            return new ValidRangeAttribute(minValue, maxValue, minValue, maxValue);
        }
        else
        {
            var type = attribute.ConstructorArguments[0];
            var min = attribute.ConstructorArguments[1];
            var max = attribute.ConstructorArguments[2];

            if (
                type.Kind != TypedConstantKind.Type
                || type.Value is not INamedTypeSymbol typeValue
                || min.Value is not string minValue
                || max.Value is not string maxValue
            )
            {
                throw new NotImplementedException("Error handling.");
            }

            if (typeValue.GetFullName() == "decimal")
            {
                var minResult = minValue;
                var maxResult = maxValue;
                if (!minResult.EndsWith("M", StringComparison.InvariantCultureIgnoreCase))
                {
                    minResult += "M";
                }

                if (!maxResult.EndsWith("M", StringComparison.InvariantCultureIgnoreCase))
                {
                    maxResult += "M";
                }

                return new ValidRangeAttribute(minResult, maxResult, minValue, maxValue);
            }
            else
            {
                var minResult =
                    $"Convert.ChangeType(\"{minValue}\", typeof({typeValue.GetFullName()}), global::System.Globalization.CultureInfo.InvariantCulture)";
                var maxResult =
                    $"Convert.ChangeType(\"{maxValue}\", typeof({typeValue.GetFullName()}), global::System.Globalization.CultureInfo.InvariantCulture)";

                return new ValidRangeAttribute(minResult, maxResult, minValue, maxValue);
            }
        }
    }

    /// <inheritdoc />
    public bool Equals(ValidRangeAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Min == other.Min
            && this.Max == other.Max
            && this.MinOriginal == other.MinOriginal
            && this.MaxOriginal == other.MaxOriginal;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is ValidRangeAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Min, this.Max, this.MinOriginal, this.MaxOriginal);
    }

    /// <summary>
    /// Compares two <see cref="ValidRangeAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(ValidRangeAttribute? left, ValidRangeAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="ValidRangeAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(ValidRangeAttribute? left, ValidRangeAttribute? right)
    {
        return !Equals(left, right);
    }
}
