// <copyright file="JsonSerializableCustomPropertyAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;
using SerializableAttribute = KestrelToolbox.CodeGen.Serializable.SerializableAttribute;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Instance of <see cref="JsonSerializableCustomPropertyAttribute"/>.
/// </summary>
public sealed class JsonSerializableCustomPropertyAttribute : IEquatable<JsonSerializableCustomPropertyAttribute>
{
    /// <summary>
    /// Gets the method name to be used for custom parsing.
    /// </summary>
    public string? ParsingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used for custom serialization.
    /// </summary>
    public string? SerializingMethodName { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="JsonSerializableCustomPropertyAttribute"/> class.
    /// </summary>
    /// <param name="parsingMethodName">The method name to be used for custom parsing.</param>
    /// <param name="serializingMethodName">The method name to be used for custom serialization.</param>
    public JsonSerializableCustomPropertyAttribute(string? parsingMethodName, string? serializingMethodName)
    {
        this.ParsingMethodName = parsingMethodName;
        this.SerializingMethodName = serializingMethodName;
    }

    /// <summary>
    /// Generates a new instance of <see cref="JsonSerializableCustomPropertyAttribute"/> from <see cref="SerializableAttribute"/>.
    /// </summary>
    /// <param name="attribute">The attribute to make cacheable.</param>
    /// <returns>The new instance of <see cref="JsonSerializableCustomPropertyAttribute"/>.</returns>
    public static JsonSerializableCustomPropertyAttribute FromAttribute(AttributeData attribute)
    {
        var parseMethodName = default(string);
        var serializeMethodName = default(string);
        foreach (var namedArgument in attribute.NamedArguments)
        {
            switch (namedArgument.Key)
            {
                case "ParseMethod":
                {
                    parseMethodName = (string?)namedArgument.Value.Value;
                    break;
                }

                case "SerializeMethod":
                {
                    serializeMethodName = (string?)namedArgument.Value.Value;
                    break;
                }

                default:
                {
                    // Nothing to do.
                    break;
                }
            }
        }

        return new JsonSerializableCustomPropertyAttribute(parseMethodName, serializeMethodName);
    }

    /// <inheritdoc />
    public bool Equals(JsonSerializableCustomPropertyAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.ParsingMethodName == other.ParsingMethodName
            && this.SerializingMethodName == other.SerializingMethodName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj)
            || (obj is JsonSerializableCustomPropertyAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.SerializingMethodName, this.ParsingMethodName);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableCustomPropertyAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(
        JsonSerializableCustomPropertyAttribute? left,
        JsonSerializableCustomPropertyAttribute? right
    )
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableCustomPropertyAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(
        JsonSerializableCustomPropertyAttribute? left,
        JsonSerializableCustomPropertyAttribute? right
    )
    {
        return !Equals(left, right);
    }
}
