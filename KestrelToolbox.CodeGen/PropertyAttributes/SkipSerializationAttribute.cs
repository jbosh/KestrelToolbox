// <copyright file="SkipSerializationAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.PropertyAttributes;

/// <summary>
/// Condition under which to skip serialization.
/// </summary>
[Flags]
public enum SkipSerializationCondition
{
    /// <summary>
    /// Always serialize the property.
    /// </summary>
    None = 0,

    /// <summary>
    /// If the value is the default, property is ignored during serialization.
    /// </summary>
    WhenDefault = 1 << 0,

    /// <summary>
    /// If the value is null, property is ignored during serialization.
    /// </summary>
    WhenNull = 1 << 1,

    /// <summary>
    /// If the value is empty, property is ignored during serialization.
    /// </summary>
    /// <remarks>
    /// This is for strings and collections.
    /// </remarks>
    WhenEmpty = 1 << 2,
}

/// <summary>
/// Attribute for skipping serialization.
/// </summary>
public sealed class SkipSerializationAttribute : IEquatable<SkipSerializationAttribute>
{
    /// <summary>
    /// Gets the conditions under which this property will skip serialization.
    /// </summary>
    public SkipSerializationCondition Condition { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="SkipSerializationAttribute"/> class.
    /// </summary>
    /// <param name="condition">Condition under which this property will skip serialization.</param>
    public SkipSerializationAttribute(SkipSerializationCondition condition)
    {
        this.Condition = condition;
    }

    /// <summary>
    /// Gets a new instance of <see cref="SkipSerializationAttribute"/> from matching attribute data.
    /// </summary>
    /// <param name="attribute">Attribute data for <c>SkipSerializationAttribute</c>.</param>
    /// <returns>The found settings.</returns>
    public static SkipSerializationAttribute FromAttribute(AttributeData attribute)
    {
        var condition = SkipSerializationCondition.None;
        if (attribute.ConstructorArguments.Length != 0)
        {
            var argument = attribute.ConstructorArguments[0];
            var value = argument.Value;
            if (value is int intValue)
            {
                condition = (SkipSerializationCondition)intValue;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        return new SkipSerializationAttribute(condition);
    }

    /// <inheritdoc />
    public bool Equals(SkipSerializationAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Condition == other.Condition;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is SkipSerializationAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return (int)this.Condition;
    }

    /// <summary>
    /// Compares two <see cref="SkipSerializationAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(SkipSerializationAttribute? left, SkipSerializationAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="SkipSerializationAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(SkipSerializationAttribute? left, SkipSerializationAttribute? right)
    {
        return !Equals(left, right);
    }
}
