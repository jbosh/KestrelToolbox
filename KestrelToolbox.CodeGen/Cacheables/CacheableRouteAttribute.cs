// <copyright file="CacheableRouteAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using KestrelToolbox.CodeGen.Routes;
using KestrelToolbox.CodeGen.Serializable;
using Microsoft.CodeAnalysis;
using SerializableAttribute = KestrelToolbox.CodeGen.Serializable.SerializableAttribute;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// RouteAttribute that can be cached.
/// </summary>
public sealed class CacheableRouteAttribute : IEquatable<CacheableRouteAttribute>
{
    /// <summary>
    /// Gets a value indicating whether the method is static.
    /// </summary>
    public bool IsStatic { get; }

    /// <summary>
    /// Gets the short name of the method.
    /// </summary>
    public string MethodName { get; }

    /// <summary>
    /// Gets a list of parameters for the method.
    /// </summary>
    public List<CacheableRouteParameter> Parameters { get; }

    /// <summary>
    /// Gets a list of RouteAttributes on this method.
    /// </summary>
    public List<RouteAttribute> Attributes { get; }

    /// <summary>
    /// Gets a list of accepted content types.
    /// </summary>
    public HashSet<string> AcceptContentTypes { get; }

    /// <summary>
    /// Gets a list of accepted content types.
    /// </summary>
    public List<AuthorizeAttribute> AuthorizeAttributes { get; }

    /// <summary>
    /// Gets a value indicating whether this route needs to generate any additional code.
    /// </summary>
    /// <see cref="Generate"/>
    public bool ShouldGenerate => this.AcceptContentTypes.Count != 0 || this.Parameters.Count != 0;

    /// <summary>
    /// Initializes a new instance of the <see cref="CacheableRouteAttribute"/> class.
    /// </summary>
    /// <param name="isStatic">A value indicating whether the method is static.</param>
    /// <param name="methodName">The short name of the method.</param>
    /// <param name="parameters">A list of parameters for the method.</param>
    /// <param name="attributes">A list of RouteAttributes on this method.</param>
    /// <param name="acceptContentTypes">A list of accepted content types.</param>
    /// <param name="authorizeAttributes">AuthorizeAttribute list.</param>
    public CacheableRouteAttribute(
        bool isStatic,
        string methodName,
        List<CacheableRouteParameter> parameters,
        List<RouteAttribute> attributes,
        HashSet<string> acceptContentTypes,
        List<AuthorizeAttribute> authorizeAttributes
    )
    {
        this.IsStatic = isStatic;
        this.MethodName = methodName;
        this.Parameters = parameters;
        this.Attributes = attributes;
        this.AcceptContentTypes = acceptContentTypes;
        this.AuthorizeAttributes = authorizeAttributes;
    }

    /// <summary>
    /// Generates local callback method for route info.
    /// </summary>
    /// <param name="context">Context of the request.</param>
    /// <param name="builder">Builder to append to.</param>
    /// <param name="localMethodName">Expected name of the local method to be used externally.</param>
    /// <returns>True if generation was successful, false if not.</returns>
    public bool Generate(SourceProductionContext context, CodeBuilder builder, string localMethodName)
    {
        builder.AppendLine(
            $"{(this.IsStatic ? "static " : string.Empty)}async global::System.Threading.Tasks.Task {localMethodName}(global::Microsoft.AspNetCore.Http.HttpContext context)"
        );
        using (builder.AppendBlock())
        {
            builder.AppendLine("var request = context.Request;");
            builder.AppendLine("var response = context.Response;");

            if (this.AcceptContentTypes.Count != 0)
            {
                builder.AppendLine(
                    $"if (!global::KestrelToolbox.Internal.WebHelpers.IsAcceptableContentType(request.Headers.ContentType, {string.Join(", ", this.AcceptContentTypes.Select(t => t.ToLiteral()))}))"
                );
                using (builder.AppendBlock())
                {
                    builder.AppendLine("response.StatusCode = 415;");
                    builder.AppendLine(
                        "await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, \"Unsupported content type.\");"
                    );
                    builder.AppendLine("return;");
                }
            }

            if (this.Parameters.Count != 0)
            {
                builder.AppendLine("var routeValues = request.RouteValues;");
                for (var parameterIndex = 0; parameterIndex < this.Parameters.Count; parameterIndex++)
                {
                    var parameter = this.Parameters[parameterIndex];
                    var variableName = $"param{parameterIndex}";
                    if (parameter.Type.FullName == "string")
                    {
                        builder.AppendLine(
                            $"var {variableName} = (string?)routeValues[{parameter.Name.ToLiteral()}] ?? string.Empty;"
                        );
                    }
                    else if (parameter.JsonBodyAttribute != null)
                    {
                        var jsonBodyAttribute = parameter.JsonBodyAttribute;
                        string parsingMethodName;
                        if (jsonBodyAttribute.ParseMethod != null)
                        {
                            parsingMethodName = jsonBodyAttribute.ParseMethod;
                        }
                        else if (
                            jsonBodyAttribute.ParameterType.SelfSerializableAttributes?.Find(r =>
                                r.Type == SerializableAttributeType.Json
                            ) != null
                        )
                        {
                            var attribute = jsonBodyAttribute.ParameterType.SelfSerializableAttributes!.Find(r =>
                                r.Type == SerializableAttributeType.Json
                            );
                            parsingMethodName = $"{attribute.SerializedType.FullName}.{attribute.ParsingMethodName}";
                        }
                        else
                        {
                            parsingMethodName = SerializableAttribute.GetDefaultParsingMethodName(
                                SerializableAttributeType.Json
                            );
                        }

                        builder.AppendLine(
                            "var readResult = await global::KestrelToolbox.Extensions.PipeReaderExtensions.PipeReaderExtensions.ReadToEndAsync(request.BodyReader);"
                        );
                        var jsonType = $"{parameter.Type.FullName}{(parameter.Type.IsValueType ? string.Empty : "?")}";
                        builder.AppendLine(
                            $"if (!{parsingMethodName}(readResult.Buffer, out {jsonType} {variableName}, out var jsonErrors))"
                        );
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine("request.BodyReader.AdvanceTo(readResult.Buffer.End);");
                            builder.AppendLine("response.StatusCode = 400;");
                            builder.AppendLine(
                                "await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, jsonErrors);"
                            );
                            builder.AppendLine("return;");
                        }

                        builder.AppendLine("request.BodyReader.AdvanceTo(readResult.Buffer.End);");
                    }
                    else if (parameter.QueryParametersAttribute != null)
                    {
                        var queryParametersAttribute = parameter.QueryParametersAttribute;
                        string parsingMethodName;
                        if (queryParametersAttribute.ParseMethod != null)
                        {
                            parsingMethodName = queryParametersAttribute.ParseMethod;
                        }
                        else if (
                            queryParametersAttribute.ParameterType.SelfSerializableAttributes?.Find(r =>
                                r.Type == SerializableAttributeType.QueryString
                            ) != null
                        )
                        {
                            var attribute = queryParametersAttribute.ParameterType.SelfSerializableAttributes!.Find(r =>
                                r.Type == SerializableAttributeType.QueryString
                            );
                            parsingMethodName = $"{attribute.SerializedType.FullName}.{attribute.ParsingMethodName}";
                        }
                        else
                        {
                            parsingMethodName = SerializableAttribute.GetDefaultParsingMethodName(
                                SerializableAttributeType.QueryString
                            );
                        }

                        var queryParameterType =
                            $"{parameter.Type.FullName}{(parameter.Type.IsValueType ? string.Empty : "?")}";
                        builder.AppendLine(
                            $"if (!{parsingMethodName}(context.Request.Query, out {queryParameterType} {variableName}, out var queryErrors))"
                        );
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine("response.StatusCode = 400;");
                            builder.AppendLine(
                                "await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, queryErrors);"
                            );
                            builder.AppendLine("return;");
                        }
                    }
                    else
                    {
                        var parsingMethodName = parameter.Type.FullName switch
                        {
                            "int" => "int.TryParse",
                            "uint" => "uint.TryParse",
                            "long" => "long.TryParse",
                            "ulong" => "ulong.TryParse",
                            "bool" => "bool.TryParse",
                            "float" => "float.TryParse",
                            "double" => "double.TryParse",
                            "decimal" => "decimal.TryParse",
                            "global::System.Guid" => "global::System.Guid.TryParse",
                            "global::KestrelToolbox.Types.UUIDv1" => "global::KestrelToolbox.Types.UUIDv1.TryParse",
                            _ => null,
                        };

                        if (parsingMethodName == null)
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateRouteUnreferencedParameter(
                                    parameter.GetLocation(),
                                    parameter.Name
                                )
                            );
                            return false;
                        }

                        builder.AppendLine(
                            $"if (!{parsingMethodName}((string?)routeValues[{parameter.Name.ToLiteral()}] ?? string.Empty, out var {variableName}))"
                        );
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine("response.StatusCode = 400;");
                            builder.AppendLine(
                                $"await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, \"{parameter.Name} was not a valid {parameter.Type.FriendlyName}.\");"
                            );
                            builder.AppendLine("return;");
                        }
                    }
                }

                var methodParameters = string.Join(
                    ", ",
                    Enumerable.Range(0, this.Parameters.Count).Select(i => $"param{i}")
                );

                builder.AppendLine(
                    $"await {(this.IsStatic ? string.Empty : "this.")}{this.MethodName}(context, {methodParameters});"
                );
            }
            else
            {
                builder.AppendLine($"await {(this.IsStatic ? string.Empty : "this.")}{this.MethodName}(context);");
            }
        }

        return true;
    }

    /// <summary>
    /// Cacheable version of method parameters.
    /// </summary>
    public sealed class CacheableRouteParameter : IEquatable<CacheableRouteParameter>
    {
        /// <summary>
        /// Gets the name of the parameter.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the type of the parameter.
        /// </summary>
        public CacheableTypeSymbol Type { get; }

        /// <summary>
        /// Gets JsonBodyAttribute if it exists.
        /// </summary>
        public CacheableJsonBodyAttribute? JsonBodyAttribute { get; }

        /// <summary>
        /// Gets QueryParametersAttribute if it exists.
        /// </summary>
        public CacheableQueryParametersAttribute? QueryParametersAttribute { get; }

        /// <summary>
        /// Gets the location (if in source) for this class declaration.
        /// </summary>
        /// <returns>The location.</returns>
        public Location? GetLocation() => this.location?.GetLocation();

        private readonly CacheableLocation? location;

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheableRouteParameter"/> class.
        /// </summary>
        /// <param name="name">The name of the parameter.</param>
        /// <param name="type">The type of the parameter.</param>
        /// <param name="jsonBodyAttribute">JsonBodyAttribute if it exists.</param>
        /// <param name="queryParametersAttribute">CacheableQueryParametersAttribute if it exists.</param>
        /// <param name="location">The location (if in source) for this class declaration.</param>
        public CacheableRouteParameter(
            string name,
            CacheableTypeSymbol type,
            CacheableJsonBodyAttribute? jsonBodyAttribute,
            CacheableQueryParametersAttribute? queryParametersAttribute,
            CacheableLocation? location
        )
        {
            this.Name = name;
            this.Type = type;
            this.JsonBodyAttribute = jsonBodyAttribute;
            this.QueryParametersAttribute = queryParametersAttribute;
            this.location = location;
        }

        /// <inheritdoc />
        public bool Equals(CacheableRouteParameter? other)
        {
            if (other is null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Equals(this.location, other.location)
                && this.Name == other.Name
                && this.Type.Equals(other.Type)
                && Equals(this.JsonBodyAttribute, other.JsonBodyAttribute)
                && Equals(this.QueryParametersAttribute, other.QueryParametersAttribute);
        }

        /// <inheritdoc />
        public override bool Equals(object? obj)
        {
            return ReferenceEquals(this, obj) || (obj is CacheableRouteParameter other && this.Equals(other));
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return HashCode.Combine(
                this.location,
                this.Name,
                this.Type,
                this.JsonBodyAttribute,
                this.QueryParametersAttribute
            );
        }

        /// <summary>
        /// Compares two <see cref="CacheableRouteParameter"/> instances for equality.
        /// </summary>
        /// <param name="left">Left hand side.</param>
        /// <param name="right">Right hand side.</param>
        /// <returns>True if the two instance are equivalent.</returns>
        public static bool operator ==(CacheableRouteParameter? left, CacheableRouteParameter? right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Compares two <see cref="CacheableRouteParameter"/> instances for equality.
        /// </summary>
        /// <param name="left">Left hand side.</param>
        /// <param name="right">Right hand side.</param>
        /// <returns>True if the two instance are not equivalent.</returns>
        public static bool operator !=(CacheableRouteParameter? left, CacheableRouteParameter? right)
        {
            return !Equals(left, right);
        }
    }

    /// <inheritdoc />
    public bool Equals(CacheableRouteAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.IsStatic == other.IsStatic
            && this.MethodName == other.MethodName
            && this.Parameters.SequenceEqual(other.Parameters)
            && this.Attributes.SequenceEqual(other.Attributes)
            && this.AcceptContentTypes.SetEquals(other.AcceptContentTypes)
            && this.AuthorizeAttributes.SequenceEqual(other.AuthorizeAttributes);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is CacheableRouteAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(
            this.IsStatic,
            this.MethodName,
            this.Parameters,
            this.Attributes,
            this.AcceptContentTypes,
            this.AuthorizeAttributes
        );
    }

    /// <summary>
    /// Compares two <see cref="CacheableRouteAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableRouteAttribute? left, CacheableRouteAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableRouteAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableRouteAttribute? left, CacheableRouteAttribute? right)
    {
        return !Equals(left, right);
    }
}
