// <copyright file="CacheableArgument.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// Cacheable version of ArgumentSyntax and AttributeArgumentSyntax.
/// </summary>
public sealed class CacheableArgument : IEquatable<CacheableArgument>
{
    /// <summary>
    /// Gets the value for <c>name:</c> syntax.
    /// </summary>
    public string? NameColon { get; }

    /// <summary>
    /// Gets the value for <c>name=</c> syntax.
    /// </summary>
    public string? NameEquals { get; }

    /// <summary>
    /// Gets value of the argument.
    /// </summary>
    public string? Value { get; }

    /// <inheritdoc />
    public override string ToString()
    {
        if (this.NameColon != null)
        {
            return $"{this.NameColon}: {this.Value}";
        }

        if (this.NameEquals != null)
        {
            return $"{this.NameEquals} = {this.Value}";
        }

        return this.Value ?? "null";
    }

    private CacheableArgument(string? nameColon, string? nameEquals, string? value)
    {
        this.NameColon = nameColon;
        this.NameEquals = nameEquals;
        this.Value = value;
    }

    /// <summary>
    /// Creates a new instance of <see cref="CacheableArgument"/>.
    /// </summary>
    /// <param name="semanticModel">Semantic model.</param>
    /// <param name="argumentSyntax">Syntax to get data from.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>New instance of <see cref="CacheableArgument"/>.</returns>
    public static CacheableArgument FromArgumentSyntax(
        SemanticModel semanticModel,
        AttributeArgumentSyntax argumentSyntax,
        CancellationToken cancellationToken
    )
    {
        var nameColon = default(string);
        var nameEquals = default(string);
        var value = semanticModel.GetConstantValue(argumentSyntax.Expression, cancellationToken).Value;

        if (argumentSyntax.NameColon != null)
        {
            nameColon = argumentSyntax.NameColon.Name.Identifier.Text;
        }
        else if (argumentSyntax.NameEquals != null)
        {
            nameEquals = argumentSyntax.NameEquals.Name.Identifier.Text;
        }

        var valueLiteral = value switch
        {
            string s => s.ToLiteral(),
            _ => value?.ToString(),
        };
        return new CacheableArgument(nameColon, nameEquals, valueLiteral);
    }

    /// <inheritdoc />
    public bool Equals(CacheableArgument? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.NameColon == other.NameColon
            && this.NameEquals == other.NameEquals
            && Equals(this.Value, other.Value);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not CacheableArgument other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.NameColon, this.NameEquals, this.Value);
    }

    /// <summary>
    /// Compares two <see cref="CacheableArgument"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableArgument? left, CacheableArgument? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableArgument"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableArgument? left, CacheableArgument? right)
    {
        return !Equals(left, right);
    }
}
