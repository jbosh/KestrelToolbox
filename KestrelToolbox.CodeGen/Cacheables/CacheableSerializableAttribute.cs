// <copyright file="CacheableSerializableAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Serializable;
using KestrelToolbox.CodeGen.Typedefs;
using Microsoft.CodeAnalysis;
using SerializableAttribute = KestrelToolbox.CodeGen.Serializable.SerializableAttribute;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// Cacheable edition of <see cref="SerializableAttribute"/>.
/// </summary>
public sealed class CacheableSerializableAttribute : IEquatable<CacheableSerializableAttribute>
{
    /// <summary>
    /// Gets the attribute type for this <see cref="CacheableSerializableAttribute"/>.
    /// </summary>
    public SerializableAttributeType Type { get; }

    /// <summary>
    /// Gets a value indicating whether this type is static. This is only valuable for classes.
    /// </summary>
    public bool IsStatic { get; }

    /// <summary>
    /// Gets the declaring type that will contain this attribute.
    /// </summary>
    public CacheableTypeSymbol DeclaringType { get; private set; }

    /// <summary>
    /// Gets the type symbol for full serialized type.
    /// </summary>
    public CacheableTypeSymbol SerializedType { get; private set; }

    /// <summary>
    /// Gets the type information referenced by TypedefAttribute.
    /// </summary>
    public CacheableTypeSymbol? AliasedTypedefType { get; }

    /// <summary>
    /// Gets the method name to be used for parsing.
    /// </summary>
    public string ParsingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used for serialization.
    /// </summary>
    public string SerializingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used for custom parsing.
    /// </summary>
    public string? CustomParsingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used for custom serialization.
    /// </summary>
    public string? CustomSerializingMethodName { get; }

    /// <summary>
    /// Gets a value indicating whether parsing should be generated.
    /// </summary>
    public bool GenerateParse { get; }

    /// <summary>
    /// Gets a value indicating whether serialization should be generated.
    /// </summary>
    public bool GenerateSerialize { get; }

    /// <summary>
    /// Gets a value indicating whether this type has been added to generation implicitly.
    /// </summary>
    public bool IsImplicitlyGenerated { get; }

    /// <summary>
    /// Gets a value indicating whether this attribute was created through self reference.
    /// </summary>
    public bool IsSelfReference { get; }

    /// <summary>
    /// Gets the visibility of the generated methods.
    /// </summary>
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Gets the location (if in source) for this serializable attribute.
    /// </summary>
    /// <returns>The location.</returns>
    public Location? GetLocation() => this.CacheableLocation?.GetLocation();

    /// <summary>
    /// Gets a cacheable location.
    /// </summary>
    public CacheableLocation? CacheableLocation { get; }

    /// <inheritdoc />
    public override string ToString() => $"{this.Type}: {this.SerializedType.FullName}";

    /// <summary>
    /// Initializes a new instance of the <see cref="CacheableSerializableAttribute"/> class.
    /// </summary>
    /// <param name="type">The attribute type for this <see cref="CacheableSerializableAttribute"/>.</param>
    /// <param name="isStatic">A value indicating whether this type is static. This is only valuable for classes.</param>
    /// <param name="declaringType">The declaring type that will contain this attribute.</param>
    /// <param name="serializedType">The type symbol for full serialized type.</param>
    /// <param name="parsingMethodName">The method name to be used for parsing.</param>
    /// <param name="serializingMethodName">The method name to be used for serializing.</param>
    /// <param name="customParsingMethodName">The method name to be used for custom parsing.</param>
    /// <param name="customSerializingMethodName">The method name to be used for custom serialization.</param>
    /// <param name="isImplicitlyGenerated">A value indicating whether this attribute was implicitly generated.</param>
    /// <param name="isSelfReference">A value indicating whether this attribute was created through self reference.</param>
    /// <param name="generateParse">A value indicating whether parsing should be generated.</param>
    /// <param name="generateSerialize">A value indicating whether serialization should be generated.</param>
    /// <param name="cacheableLocation">The location (if in source) for this serializable attribute.</param>
    /// <param name="aliasedTypedefType">The type information referenced by TypedefAttribute.</param>
    /// <param name="visibility">Visibility of the generated methods.</param>
    public CacheableSerializableAttribute(
        SerializableAttributeType type,
        bool isStatic,
        CacheableTypeSymbol declaringType,
        CacheableTypeSymbol serializedType,
        string parsingMethodName,
        string serializingMethodName,
        string? customParsingMethodName,
        string? customSerializingMethodName,
        bool isImplicitlyGenerated,
        bool isSelfReference,
        bool generateParse,
        bool generateSerialize,
        CacheableLocation? cacheableLocation,
        CacheableTypeSymbol? aliasedTypedefType,
        SerializableVisibility visibility
    )
    {
        this.Type = type;
        this.IsStatic = isStatic;
        this.DeclaringType = declaringType;
        this.SerializedType = serializedType;
        this.ParsingMethodName = parsingMethodName;
        this.SerializingMethodName = serializingMethodName;
        this.IsImplicitlyGenerated = isImplicitlyGenerated;
        this.IsSelfReference = isSelfReference;
        this.CustomParsingMethodName = customParsingMethodName;
        this.CustomSerializingMethodName = customSerializingMethodName;
        this.GenerateParse = generateParse;
        this.GenerateSerialize = generateSerialize;
        this.CacheableLocation = cacheableLocation;
        this.AliasedTypedefType = aliasedTypedefType;
        this.Visibility = visibility;
    }

    /// <summary>
    /// Generates a new instance of <see cref="CacheableSerializableAttribute"/> from <see cref="SerializableAttribute"/>.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="attribute">The attribute to make cacheable.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The new instance of <see cref="CacheableSerializableAttribute"/>.</returns>
    public static CacheableSerializableAttribute FromSerializableAttribute(
        SerializableAttributesGeneratorContext context,
        SerializableAttribute attribute,
        CancellationToken cancellationToken
    )
    {
        var isStatic = attribute.IsStatic;
        var location = CacheableLocation.FromLocation(attribute.GetLocation());
        if (attribute.CustomParsingMethodName != null)
        {
            var compilation = context.SemanticModel.Compilation;
            var methodSymbols = compilation.GetMethodsByName(
                context.SemanticModel.GetDeclaredSymbol(attribute.TypeDeclarationSyntax, cancellationToken)!,
                attribute.CustomParsingMethodName,
                cancellationToken
            );

            var foundSymbol = false;
            foreach (var methodSymbol in methodSymbols)
            {
                if (
                    JsonSerializableCustomParseMethod.TryGetMethodFromMethodSymbol(
                        methodSymbol,
                        attribute.SerializedType,
                        out _
                    )
                )
                {
                    foundSymbol = true;
                }
            }

            if (!foundSymbol)
            {
                context.ReportDiagnostic(
                    DiagnosticMessages.CreateSerializableCustomMethodNotFound(
                        attribute.AttributeSyntax.GetLocation(),
                        "JsonSerializable",
                        "parse",
                        attribute.CustomParsingMethodName
                    )
                );
            }
        }

        if (attribute.CustomSerializingMethodName != null)
        {
            var compilation = context.SemanticModel.Compilation;
            var methodSymbols = compilation.GetMethodsByName(
                context.SemanticModel.GetDeclaredSymbol(attribute.TypeDeclarationSyntax, cancellationToken)!,
                attribute.CustomSerializingMethodName,
                cancellationToken
            );

            var foundSymbol = false;
            foreach (var methodSymbol in methodSymbols)
            {
                if (
                    JsonSerializableCustomSerializeMethod.TryGetMethodFromMethodSymbol(
                        methodSymbol,
                        attribute.SerializedType,
                        out _
                    )
                )
                {
                    foundSymbol = true;
                }
            }

            if (!foundSymbol)
            {
                context.ReportDiagnostic(
                    DiagnosticMessages.CreateSerializableCustomMethodNotFound(
                        attribute.AttributeSyntax.GetLocation(),
                        "JsonSerializable",
                        "serialize",
                        attribute.CustomSerializingMethodName
                    )
                );
            }
        }

        return new CacheableSerializableAttribute(
            attribute.Type,
            isStatic,
            CacheableTypeSymbol.FromITypeSymbol(context.SymbolCache, context.ReportDiagnostic, attribute.DeclaringType),
            CacheableTypeSymbol.FromITypeSymbol(
                context.SymbolCache,
                context.ReportDiagnostic,
                attribute.SerializedType
            ),
            attribute.ParsingMethodName,
            attribute.SerializingMethodName,
            attribute.CustomParsingMethodName,
            attribute.CustomSerializingMethodName,
            attribute.IsImplicitlyGenerated,
            attribute.IsSelfReference,
            attribute.GenerateParse,
            attribute.GenerateSerialize,
            location,
            attribute.AliasedTypedefType != null
                ? CacheableTypeSymbol.FromITypeSymbol(
                    context.SymbolCache,
                    context.ReportDiagnostic,
                    attribute.AliasedTypedefType
                )
                : null,
            attribute.Visibility
        );
    }

    /// <summary>
    /// Gets a <see cref="CacheableSerializableAttribute"/> for a specific class. This is built to be used very
    /// specifically for <see cref="CacheableTypeSymbol"/> because it will return null for attribute.
    /// </summary>
    /// <param name="symbolCache">Cache of symbols so we don't recurse forever.</param>
    /// <param name="reportDiagnostic">Callback to report diagnostics.</param>
    /// <param name="symbol"><see cref="ITypeSymbol"/> to make cacheable.</param>
    /// <returns>
    /// The partially initialized <see cref="CacheableSerializableAttribute"/> for <paramref name="symbol"/>.
    /// if the attribute exists. Null if it does not.
    /// </returns>
    /// <remarks>
    /// This method will never return anything if <paramref name="symbol"/> is not a class or struct.
    /// </remarks>
    public static List<CacheableSerializableAttribute>? GetSelfReferenceAttributes(
        Dictionary<string, CacheableTypeSymbol> symbolCache,
        Action<Diagnostic> reportDiagnostic,
        ITypeSymbol symbol
    )
    {
        if (symbol.TypeKind is not TypeKind.Class and not TypeKind.Struct)
        {
            return null;
        }

        if (symbol is not INamedTypeSymbol namedTypeSymbol)
        {
            return null;
        }

        var result = new List<CacheableSerializableAttribute>();
        var queryFullName = namedTypeSymbol.GetFullName();
        var attributes = namedTypeSymbol.GetAttributes();
        foreach (var attribute in attributes)
        {
            var attributeClassFullName = attribute.AttributeClass?.GetFullName();
            var attributeType = attributeClassFullName switch
            {
                "global::KestrelToolbox.Serialization.EnumSerializableAttribute" => SerializableAttributeType.Enum,
                "global::KestrelToolbox.Serialization.QueryStringSerializableAttribute" =>
                    SerializableAttributeType.QueryString,
                "global::KestrelToolbox.Serialization.CommandLineSerializableAttribute" =>
                    SerializableAttributeType.CommandLine,
                "global::KestrelToolbox.Serialization.JsonSerializableAttribute" => SerializableAttributeType.Json,
                _ => SerializableAttributeType.Unknown,
            };

            if (attributeType == SerializableAttributeType.Unknown)
            {
                if (attributeClassFullName == "global::KestrelToolbox.Types.TypedefAttribute")
                {
                    if (attribute.ConstructorArguments.Length != 2)
                    {
                        // Compiler should handle errors on this.
                        continue;
                    }

                    if (attribute.ConstructorArguments[0].Value is not INamedTypeSymbol aliasedTypeSymbol)
                    {
                        // Compiler should handle errors on this.
                        continue;
                    }

                    if (attribute.ConstructorArguments[1].Value is not int typdefFeaturesInt)
                    {
                        // Compiler should handle errors on this.
                        continue;
                    }

                    var aliasedType = CacheableTypeSymbol.FromITypeSymbol(
                        symbolCache,
                        reportDiagnostic,
                        aliasedTypeSymbol
                    );

                    var features = (TypedefFeatures)typdefFeaturesInt;
                    if (features.HasFlag(TypedefFeatures.JsonSerializable))
                    {
                        result.Add(
                            new CacheableSerializableAttribute(
                                SerializableAttributeType.Json,
                                false,
                                null!,
                                null!,
                                SerializableAttribute.GetDefaultParsingMethodName(SerializableAttributeType.Json),
                                SerializableAttribute.GetDefaultSerializingMethodName(SerializableAttributeType.Json),
                                null,
                                null,
                                false,
                                false,
                                true,
                                true,
                                null,
                                aliasedType,
                                SerializableVisibility.File
                            )
                        );
                    }
                }

                continue;
            }

            var constructorArguments = attribute.ConstructorArguments;
            if (constructorArguments.Length == 0)
            {
                // Compiler should handle errors on this.
                continue;
            }

            var serializingMethodName = SerializableAttribute.GetDefaultSerializingMethodName(attributeType);
            var parsingMethodName = SerializableAttribute.GetDefaultParsingMethodName(attributeType);
            var customParsingMethodName = default(string);
            var customSerializingMethodName = default(string);
            var generateParse = true;
            var generateSerialize = true;
            if (constructorArguments[0].Values.Length == 0)
            {
                // No arguments means use the local class.
                if (symbol.GetFullName() != queryFullName)
                {
                    continue;
                }
            }
            else if (
                !constructorArguments[0]
                    .Values.Any(c => c.Value is not ITypeSymbol typeSymbol || typeSymbol.GetFullName() == queryFullName)
            )
            {
                // Didn't find symbol.
                continue;
            }

            foreach (var namedArgument in attribute.NamedArguments)
            {
                var value = namedArgument.Value;
                switch (namedArgument.Key)
                {
                    case "ParsingMethodName":
                    {
                        if (value.Value is string stringValue)
                        {
                            parsingMethodName = stringValue;
                        }

                        break;
                    }

                    case "SerializingMethodName":
                    {
                        if (value.Value is string stringValue)
                        {
                            serializingMethodName = stringValue;
                        }

                        break;
                    }

                    case "CustomParsingMethodName":
                    {
                        if (value.Value is string stringValue)
                        {
                            customParsingMethodName = stringValue;
                        }

                        break;
                    }

                    case "CustomSerializingMethodName":
                    {
                        if (value.Value is string stringValue)
                        {
                            customSerializingMethodName = stringValue;
                        }

                        break;
                    }

                    case "SourceGenerationMode":
                    {
                        if (value.Value is int intValue)
                        {
                            generateSerialize = ((1 << 0) & intValue) != 0;
                            generateParse = ((1 << 1) & intValue) != 0;
                        }

                        break;
                    }

                    default:
                    {
                        // Compiler should handle this. We'll silently ignore here.
                        break;
                    }
                }
            }

            result.Add(
                new CacheableSerializableAttribute(
                    attributeType,
                    false,
                    null!,
                    null!,
                    parsingMethodName,
                    serializingMethodName,
                    customParsingMethodName,
                    customSerializingMethodName,
                    false,
                    false,
                    generateParse,
                    generateSerialize,
                    null,
                    null,
                    SerializableVisibility.Public
                )
            );
        }

        return result.Count == 0 ? null : result;
    }

    /// <inheritdoc />
    public bool Equals(CacheableSerializableAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return Equals(this.CacheableLocation, other.CacheableLocation)
            && this.Type == other.Type
            && this.DeclaringType.SimpleEquals(other.DeclaringType)
            && this.SerializedType.SimpleEquals(other.SerializedType)
            && this.ParsingMethodName == other.ParsingMethodName
            && this.SerializingMethodName == other.SerializingMethodName
            && this.CustomParsingMethodName == other.CustomParsingMethodName
            && this.CustomSerializingMethodName == other.CustomSerializingMethodName
            && this.GenerateParse == other.GenerateParse
            && this.GenerateSerialize == other.GenerateSerialize
            && this.Visibility == other.Visibility;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is CacheableSerializableAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Type, this.SerializedType, this.DeclaringType, this.CacheableLocation);
    }

    /// <summary>
    /// Compares two <see cref="CacheableSerializableAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableSerializableAttribute? left, CacheableSerializableAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableSerializableAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableSerializableAttribute? left, CacheableSerializableAttribute? right)
    {
        return !Equals(left, right);
    }

    /// <summary>
    /// Sets <see cref="SerializedType"/> and sets it to be the appropriate null annotation.
    /// </summary>
    /// <param name="declaringType">Declaring type.</param>
    /// <param name="serializedType">Serialized type.</param>
    /// <seealso cref="GetSelfReferenceAttributes"/>
    /// <remarks>
    /// This method should only be used immediately after <see cref="GetSelfReferenceAttributes"/>.
    /// </remarks>
    internal void SetSelfSerializedTypeUnsafe(CacheableTypeSymbol declaringType, CacheableTypeSymbol serializedType)
    {
        this.DeclaringType = declaringType;
        this.SerializedType = serializedType;
    }
}
