// <copyright file="CacheableJsonBodyAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Threading;
using KestrelToolbox.CodeGen.Routes;
using KestrelToolbox.CodeGen.Serializable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// Cacheable version of JsonBodyAttribute.
/// </summary>
public sealed class CacheableJsonBodyAttribute : IEquatable<CacheableJsonBodyAttribute>
{
    /// <summary>
    /// Gets the name of the parameter.
    /// </summary>
    public string ParameterName { get; }

    /// <summary>
    /// Gets the type of the parameter.
    /// </summary>
    public CacheableTypeSymbol ParameterType { get; }

    /// <summary>
    /// Gets the supported mime type of this JsonBodyAttribute.
    /// </summary>
    public string MimeType { get; }

    /// <summary>
    /// Gets the visibility of the parse method.
    /// </summary>
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Gets the parse method to use if custom parsing is desired. If this value is null, json parsing will be generated.
    /// </summary>
    public string? ParseMethod { get; }

    private CacheableJsonBodyAttribute(
        string parameterName,
        CacheableTypeSymbol parameterType,
        string mimeType,
        string? parseMethod,
        SerializableVisibility visibility
    )
    {
        this.ParameterName = parameterName;
        this.ParameterType = parameterType;
        this.MimeType = mimeType;
        this.ParseMethod = parseMethod;
        this.Visibility = visibility;
    }

    /// <summary>
    /// Creates a new instance of <see cref="CacheableJsonBodyAttribute"/>.
    /// </summary>
    /// <param name="context">Context for the parsing.</param>
    /// <param name="parameterSyntax">Parameter that has the attribute.</param>
    /// <param name="attributeSyntax">Attribute attached to the parameter.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The new instance of <see cref="CacheableJsonBodyAttribute"/> on success, false if not.</returns>
    public static CacheableJsonBodyAttribute? FromSyntax(
        RouteAttributesGeneratorContext context,
        ParameterSyntax parameterSyntax,
        AttributeSyntax attributeSyntax,
        CancellationToken cancellationToken
    ) =>
        FromSyntax(
            context.SemanticModel,
            context.ReportDiagnostic,
            context.SymbolCache,
            parameterSyntax,
            attributeSyntax,
            cancellationToken
        );

    /// <summary>
    /// Creates a new instance of <see cref="CacheableJsonBodyAttribute"/>.
    /// </summary>
    /// <param name="context">Context for the parsing.</param>
    /// <param name="parameterSyntax">Parameter that has the attribute.</param>
    /// <param name="attributeSyntax">Attribute attached to the parameter.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The new instance of <see cref="CacheableJsonBodyAttribute"/> on success, false if not.</returns>
    public static CacheableJsonBodyAttribute? FromSyntax(
        SerializableAttributesGeneratorContext context,
        ParameterSyntax parameterSyntax,
        AttributeSyntax attributeSyntax,
        CancellationToken cancellationToken
    ) =>
        FromSyntax(
            context.SemanticModel,
            context.ReportDiagnostic,
            context.SymbolCache,
            parameterSyntax,
            attributeSyntax,
            cancellationToken
        );

    /// <summary>
    /// Creates a new instance of <see cref="CacheableJsonBodyAttribute"/>.
    /// </summary>
    /// <param name="semanticModel">Semantic model.</param>
    /// <param name="reportDiagnostic">Callback for reporting diagnostics.</param>
    /// <param name="symbolCache">Cache of symbols so we don't recurse forever.</param>
    /// <param name="parameterSyntax">Parameter that has the attribute.</param>
    /// <param name="attributeSyntax">Attribute attached to the parameter.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The new instance of <see cref="CacheableJsonBodyAttribute"/> on success, false if not.</returns>
    public static CacheableJsonBodyAttribute? FromSyntax(
        SemanticModel semanticModel,
        Action<Diagnostic> reportDiagnostic,
        Dictionary<string, CacheableTypeSymbol> symbolCache,
        ParameterSyntax parameterSyntax,
        AttributeSyntax attributeSyntax,
        CancellationToken cancellationToken
    )
    {
        var mimeType = "application/json";
        var parseMethod = default(string);

        // Routes are used in other files. Must be public as they cannot access file local.
        var visibility = SerializableVisibility.Private;

        var parameterName = parameterSyntax.Identifier.Text;
        if (parameterSyntax.Type == null)
        {
            return null;
        }

        var parameterType = semanticModel.GetTypeInfo(parameterSyntax.Type, cancellationToken).Type;
        if (parameterType == null)
        {
            return null;
        }

        var arguments = attributeSyntax.ArgumentList?.Arguments;
        if (arguments != null)
        {
            for (var argumentIndex = 0; argumentIndex < arguments.Value.Count; argumentIndex++)
            {
                var argument = arguments.Value[argumentIndex];
                var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                if (argument.NameEquals == null)
                {
                    return null;
                }

                var name = argument.NameEquals.Name.Identifier.Text;
                switch (name)
                {
                    case "MimeType":
                    {
                        if (value.Value is not string stringValue)
                        {
                            return null;
                        }

                        mimeType = stringValue;
                        break;
                    }

                    case "ParseMethod":
                    {
                        if (value.Value is not string stringValue)
                        {
                            return null;
                        }

                        parseMethod = stringValue;
                        break;
                    }

                    default:
                    {
                        return null;
                    }
                }
            }
        }

        var cacheableParameterType = CacheableTypeSymbol.FromITypeSymbol(symbolCache, reportDiagnostic, parameterType);
        return new CacheableJsonBodyAttribute(parameterName, cacheableParameterType, mimeType, parseMethod, visibility);
    }

    /// <inheritdoc />
    public bool Equals(CacheableJsonBodyAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.ParameterName == other.ParameterName
            && this.ParameterType.Equals(other.ParameterType)
            && this.MimeType == other.MimeType
            && this.ParseMethod == other.ParseMethod;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is CacheableJsonBodyAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.ParameterName, this.ParameterType, this.MimeType, this.ParseMethod);
    }

    /// <summary>
    /// Compares two <see cref="CacheableJsonBodyAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableJsonBodyAttribute? left, CacheableJsonBodyAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableJsonBodyAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableJsonBodyAttribute? left, CacheableJsonBodyAttribute? right)
    {
        return !Equals(left, right);
    }
}
