// <copyright file="CacheableTypeDeclarationSymbol.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// A cacheable class symbol for generators.
/// </summary>
public sealed class CacheableTypeDeclarationSymbol : IEquatable<CacheableTypeDeclarationSymbol?>
{
    /// <summary>
    /// Gets the namespace for this symbol.
    /// </summary>
    public string Namespace { get; }

    /// <summary>
    /// Gets the full class name for this symbol.
    /// </summary>
    public string FullName { get; }

    /// <summary>
    /// Gets the name for this symbol.
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Gets the type of this type declaration symbol.
    /// </summary>
    public TypeKind TypeKind { get; }

    /// <summary>
    /// Gets the list of containing types in full (struct X, class Y, etc...).
    /// </summary>
    public ImmutableArray<string> ContainingTypes { get; }

    /// <summary>
    /// Gets the location (if in source) for this class declaration.
    /// </summary>
    /// <returns>The location.</returns>
    public Location? GetLocation() => this.location?.GetLocation();

    private readonly CacheableLocation? location;

    /// <inheritdoc />
    public override string ToString() => this.FullName;

    /// <summary>
    /// Initializes a new instance of the <see cref="CacheableTypeDeclarationSymbol"/> class.
    /// </summary>
    /// <param name="namespace">Namespace of this symbol.</param>
    /// <param name="name">Name of the class.</param>
    /// <param name="fullName">Full name of the class.</param>
    /// <param name="typeKind">The type of this type declaration symbol.</param>
    /// <param name="containingTypes">List of containing types in full (struct X, class Y, etc...).</param>
    /// <param name="location">Location (if in source) for this class declaration.</param>
    public CacheableTypeDeclarationSymbol(
        string @namespace,
        string name,
        string fullName,
        TypeKind typeKind,
        IEnumerable<string> containingTypes,
        CacheableLocation? location
    )
    {
        this.Namespace = @namespace;
        this.Name = name;
        this.FullName = fullName;
        this.TypeKind = typeKind;
        this.ContainingTypes = containingTypes.ToImmutableArray();
        this.location = location;
    }

    /// <summary>
    /// Gets a new instance of <see cref="CacheableTypeDeclarationSymbol"/>.
    /// </summary>
    /// <param name="symbol">Semantic symbol of the class. Should be derived from <paramref name="syntax"/>.</param>
    /// <param name="syntax">Syntax of the class.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>New instance of <see cref="CacheableTypeDeclarationSymbol"/>.</returns>
    public static CacheableTypeDeclarationSymbol FromTypeSymbol(
        INamedTypeSymbol symbol,
        ClassDeclarationSyntax syntax,
        CancellationToken cancellationToken
    ) => FromTypeSymbol(symbol, CacheableLocation.FromLocation(syntax.GetLocation()), cancellationToken);

    /// <summary>
    /// Gets a new instance of <see cref="CacheableTypeDeclarationSymbol"/>.
    /// </summary>
    /// <param name="symbol">Semantic symbol of the class. Should be derived from <paramref name="syntax"/>.</param>
    /// <param name="syntax">Syntax of the method that started generation.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>New instance of <see cref="CacheableTypeDeclarationSymbol"/>.</returns>
    public static CacheableTypeDeclarationSymbol FromTypeSymbol(
        INamedTypeSymbol symbol,
        MethodDeclarationSyntax syntax,
        CancellationToken cancellationToken
    ) => FromTypeSymbol(symbol, CacheableLocation.FromLocation(syntax.GetLocation()), cancellationToken);

    /// <summary>
    /// Gets a new instance of <see cref="CacheableTypeDeclarationSymbol"/>.
    /// </summary>
    /// <param name="symbol">Semantic symbol of the class. Should be derived from <paramref name="syntax"/>.</param>
    /// <param name="syntax">Syntax of the type that started generation.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>New instance of <see cref="CacheableTypeDeclarationSymbol"/>.</returns>
    public static CacheableTypeDeclarationSymbol FromTypeSymbol(
        INamedTypeSymbol symbol,
        TypeDeclarationSyntax syntax,
        CancellationToken cancellationToken
    ) => FromTypeSymbol(symbol, CacheableLocation.FromLocation(syntax.GetLocation()), cancellationToken);

    /// <summary>
    /// Gets a new instance of <see cref="CacheableTypeDeclarationSymbol"/>.
    /// </summary>
    /// <param name="symbol">Semantic symbol of the class.</param>
    /// <param name="location">The location where errors should occur about this class.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>New instance of <see cref="CacheableTypeDeclarationSymbol"/>.</returns>
    public static CacheableTypeDeclarationSymbol FromTypeSymbol(
        INamedTypeSymbol symbol,
        CacheableLocation? location,
        CancellationToken cancellationToken
    )
    {
        var @namespace = symbol.ContainingNamespace.ToDisplayString(NamespaceQualifiedFormat);

        var containingTypes = new List<string>();
        foreach (var r in symbol.DeclaringSyntaxReferences)
        {
            if (r.GetSyntax(cancellationToken) is not TypeDeclarationSyntax typeDeclaration)
            {
                continue;
            }

            var lines = new List<string>
            {
                $"{typeDeclaration.Modifiers} {typeDeclaration.Keyword.ValueText} {typeDeclaration.Identifier}",
            };

            var parent = typeDeclaration.Parent as TypeDeclarationSyntax;
            while (parent != null && IsValidParent(parent))
            {
                lines.Add(
                    $"{parent.Modifiers} {parent.Keyword.ValueText} {parent.Identifier}{parent.TypeParameterList}"
                );
                parent = parent.Parent as TypeDeclarationSyntax;
            }

            lines.Reverse();
            containingTypes = lines;
            break;
        }

        if (containingTypes.Count == 0)
        {
            throw new NotImplementedException();
        }

        return new CacheableTypeDeclarationSymbol(
            @namespace,
            symbol.Name,
            symbol.GetFullName(),
            symbol.TypeKind,
            containingTypes,
            location
        );

        static bool IsValidParent(TypeDeclarationSyntax parent)
        {
#pragma warning disable IDE0072
            return parent.Kind() switch
#pragma warning restore IDE0072
            {
                SyntaxKind.ClassDeclaration => true,
                SyntaxKind.StructDeclaration => true,
                _ => false,
            };
        }
    }

    /// <inheritdoc />
    public bool Equals(CacheableTypeDeclarationSymbol? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return Equals(this.location, other.location)
            && this.Namespace == other.Namespace
            && this.FullName == other.FullName
            && this.ContainingTypes.SequenceEqual(other.ContainingTypes);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj is CacheableTypeDeclarationSymbol other && this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.location, this.FullName);
    }

    /// <summary>
    /// Compares two <see cref="CacheableTypeDeclarationSymbol"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableTypeDeclarationSymbol? left, CacheableTypeDeclarationSymbol? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableTypeDeclarationSymbol"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableTypeDeclarationSymbol? left, CacheableTypeDeclarationSymbol? right)
    {
        return !Equals(left, right);
    }

    private static SymbolDisplayFormat NamespaceQualifiedFormat { get; } =
        new(
            globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Omitted,
            typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
            genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
            memberOptions: SymbolDisplayMemberOptions.IncludeParameters
                | SymbolDisplayMemberOptions.IncludeType
                | SymbolDisplayMemberOptions.IncludeRef
                | SymbolDisplayMemberOptions.IncludeContainingType,
            kindOptions: SymbolDisplayKindOptions.IncludeMemberKeyword,
            parameterOptions: SymbolDisplayParameterOptions.IncludeName
                | SymbolDisplayParameterOptions.IncludeType
                | SymbolDisplayParameterOptions.IncludeParamsRefOut
                | SymbolDisplayParameterOptions.IncludeDefaultValue,
            localOptions: SymbolDisplayLocalOptions.IncludeType,
            miscellaneousOptions: SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
                | SymbolDisplayMiscellaneousOptions.UseSpecialTypes
                | SymbolDisplayMiscellaneousOptions.IncludeNullableReferenceTypeModifier
                | SymbolDisplayMiscellaneousOptions.ExpandNullable
        );
}
