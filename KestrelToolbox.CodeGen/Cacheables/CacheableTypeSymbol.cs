// <copyright file="CacheableTypeSymbol.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using KestrelToolbox.CodeGen.Serializable;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// Cacheable version of <see cref="ITypeSymbol"/>.
/// </summary>
public sealed class CacheableTypeSymbol : IEquatable<CacheableTypeSymbol>
{
    /// <summary>
    /// Gets the <see cref="TypeKind"/> of this symbol.
    /// </summary>
    public TypeKind TypeKind { get; }

    /// <summary>
    /// Gets the full name of the symbol.
    /// </summary>
    public string FullName { get; }

    /// <summary>
    /// Gets the full name of the symbol without nullable information.
    /// </summary>
    public string FullNameNonNullable { get; }

    /// <summary>
    /// Gets the full name of the type this type is constructed from (if any).
    /// </summary>
    public string? ConstructedFromFullName { get; }

    /// <summary>
    /// Gets the element type if this instance is an array type.
    /// </summary>
    public CacheableTypeSymbol? ArrayElementType { get; private set; }

    /// <summary>
    /// Gets the list of generic type arguments.
    /// </summary>
    public CacheableTypeSymbol[] TypeArguments { get; private set; }

    /// <summary>
    /// Gets the full names of all interfaces on this type.
    /// </summary>
    public CacheableTypeSymbol[] Interfaces { get; private set; }

    /// <summary>
    /// Gets the method key name that always includes nullable annotations.
    /// </summary>
    public string MethodKeyName => this.IsValueType ? this.FullName : $"{this.FullNameNonNullable}?";

    /// <summary>
    /// Gets a value indicating whether this symbol is of type <see cref="System.Nullable"/>.
    /// </summary>
    public bool IsNullable { get; }

    /// <summary>
    /// Gets a value indicating whether this symbol is a value type.
    /// </summary>
    public bool IsValueType { get; }

    /// <summary>
    /// Gets the friendly name for this type when used in errors.
    /// </summary>
    public string FriendlyName { get; }

    /// <summary>
    /// Gets a value indicating whether a type has a default constructor.
    /// </summary>
    public bool HasDefaultConstructor { get; }

    /// <summary>
    /// Gets a value indicating whether a type is a named type.
    /// </summary>
    public bool IsNamedType { get; }

    /// <summary>
    /// Gets the nullability of this property.
    /// </summary>
    public NullableAnnotation NullableAnnotation { get; }

    /// <summary>
    /// Gets the underlying type if this type is an enum.
    /// </summary>
    public string? EnumUnderlyingType { get; }

    /// <summary>
    /// Gets settings for serializing this enum if the type is an enum.
    /// </summary>
    public EnumSerializableSettingsAttribute? EnumSerializableSettingsAttribute { get; }

    /// <summary>
    /// Gets the list of enum members if this type is an enum.
    /// </summary>
    public EnumMember[]? EnumMembers { get; }

    /// <summary>
    /// Gets the custom parse method for query strings.
    /// </summary>
    public QueryStringSerializableCustomParseMethod? QueryStringSerializableCustomParseMethod { get; }

    /// <summary>
    /// Gets the custom parse method for query strings.
    /// </summary>
    public CommandLineSerializableCustomParseMethod? CommandLineSerializableCustomParseMethod { get; }

    /// <summary>
    /// Gets the custom parse method for json.
    /// </summary>
    public JsonSerializableCustomParseMethod? JsonSerializableCustomParseMethod { get; }

    /// <summary>
    /// Gets the custom parse method for json.
    /// </summary>
    public JsonSerializableCustomSerializeMethod? JsonSerializableCustomSerializeMethod { get; }

    /// <summary>
    /// Gets the serializable attribute for self if it exists.
    /// </summary>
    public List<CacheableSerializableAttribute>? SelfSerializableAttributes { get; }

    /// <summary>
    /// Gets the non nullable type of this symbol. This is equivalent to <c>this</c> if the type is not <c>System.Nullable</c>.
    /// </summary>
    private CacheableTypeSymbol? nonNullableType;

    /// <inheritdoc />
    public override string ToString() => $"{this.FullName} ({this.TypeKind})";

    private CacheableTypeSymbol(
        TypeKind typeKind,
        string fullName,
        string fullNameNonNullable,
        string? constructedFromFullName,
        bool isNullable,
        bool isValueType,
        string friendlyName,
        bool hasDefaultConstructor,
        bool isNamedType,
        NullableAnnotation nullableAnnotation,
        string? enumUnderlyingType,
        EnumSerializableSettingsAttribute? enumSerializableSettingsAttribute,
        EnumMember[]? enumMembers,
        QueryStringSerializableCustomParseMethod? queryStringSerializableCustomParseMethod,
        CommandLineSerializableCustomParseMethod? commandLineSerializableCustomParseMethod,
        JsonSerializableCustomParseMethod? jsonSerializableCustomParseMethod,
        JsonSerializableCustomSerializeMethod? jsonSerializableCustomSerializeMethod,
        List<CacheableSerializableAttribute>? selfSerializableAttributes
    )
    {
        this.TypeKind = typeKind;
        this.FullName = fullName;
        this.FullNameNonNullable = fullNameNonNullable;
        this.ConstructedFromFullName = constructedFromFullName;
        this.nonNullableType = default;
        this.ArrayElementType = default;
        this.TypeArguments = Array.Empty<CacheableTypeSymbol>();
        this.IsNullable = isNullable;
        this.Interfaces = Array.Empty<CacheableTypeSymbol>();
        this.IsValueType = isValueType;
        this.FriendlyName = friendlyName;
        this.HasDefaultConstructor = hasDefaultConstructor;
        this.IsNamedType = isNamedType;
        this.NullableAnnotation = nullableAnnotation;
        this.EnumUnderlyingType = enumUnderlyingType;
        this.EnumSerializableSettingsAttribute = enumSerializableSettingsAttribute;
        this.EnumMembers = enumMembers;
        this.QueryStringSerializableCustomParseMethod = queryStringSerializableCustomParseMethod;
        this.CommandLineSerializableCustomParseMethod = commandLineSerializableCustomParseMethod;
        this.JsonSerializableCustomParseMethod = jsonSerializableCustomParseMethod;
        this.JsonSerializableCustomSerializeMethod = jsonSerializableCustomSerializeMethod;
        this.SelfSerializableAttributes = selfSerializableAttributes;
    }

    /// <summary>
    /// Gets the non nullable type of this symbol. This is equivalent to <c>this</c> if the type is not <c>System.Nullable</c>.
    /// </summary>
    public CacheableTypeSymbol NonNullableType => this.nonNullableType ?? this;

    /// <summary>
    /// Gets a new instance of <see cref="CacheableTypeSymbol"/>.
    /// </summary>
    /// <param name="symbolCache">Cache of symbols so we don't recurse forever.</param>
    /// <param name="reportDiagnostic">Callback to report diagnostics.</param>
    /// <param name="symbol"><see cref="ITypeSymbol"/> to make cacheable.</param>
    /// <returns>Instance of <see cref="CacheableTypeSymbol"/>.</returns>
    public static CacheableTypeSymbol FromITypeSymbol(
        Dictionary<string, CacheableTypeSymbol> symbolCache,
        Action<Diagnostic> reportDiagnostic,
        ITypeSymbol symbol
    )
    {
        var typeKind = symbol.TypeKind;
        var fullName = symbol.GetFullName();
        var fullNameNonNullable = symbol.GetFullNameNonNullable();
        if (symbolCache.TryGetValue(fullName, out var returnSymbol))
        {
            return returnSymbol;
        }

        var isNullable = symbol.OriginalDefinition.GetFullName() == "global::System.Nullable<T>";
        var isValueType = symbol.IsValueType;
        var friendlyName = GetFriendlyName(symbol);
        var hasDefaultConstructor = false;
        var isNamedType = false;
        var constructedFromFullName = default(string);
        var nullableAnnotation = symbol.NullableAnnotation;
        switch (symbol)
        {
            case INamedTypeSymbol namedTypeSymbol:
            {
                isNamedType = true;
                hasDefaultConstructor = namedTypeSymbol.Constructors.Any(m => m.Parameters.Length == 0);

                constructedFromFullName = namedTypeSymbol.ConstructedFrom.GetFullName();
                break;
            }

            default:
            {
                // Nothing to do.
                break;
            }
        }

        var enumUnderlyingType = default(string);
        var enumSerializableSettingsAttribute = default(EnumSerializableSettingsAttribute);
        var enumMembers = default(EnumMember[]);
        if (typeKind == TypeKind.Enum)
        {
            var enumType = (INamedTypeSymbol)symbol;
            enumUnderlyingType = enumType.EnumUnderlyingType!.GetFullName();
            _ = EnumSerializableSettingsAttribute.TryGet(symbol, out enumSerializableSettingsAttribute);
            enumMembers = EnumMember.GetMembers(enumType).ToArray();
        }

        _ = QueryStringSerializableCustomParseMethod.TryGetMethod(
            symbol,
            out var queryStringSerializableCustomParseMethod
        );
        _ = CommandLineSerializableCustomParseMethod.TryGetMethod(
            symbol,
            out var commandLineSerializableCustomParseMethod
        );
        _ = JsonSerializableCustomParseMethod.TryGetMethodFromClass(symbol, out var jsonSerializableCustomParseMethod);
        _ = JsonSerializableCustomSerializeMethod.TryGetMethodFromClass(
            symbol,
            out var jsonSerializableCustomSerializeMethod
        );
        var selfSerializableAttributes = CacheableSerializableAttribute.GetSelfReferenceAttributes(
            symbolCache,
            reportDiagnostic,
            symbol
        );

        returnSymbol = new CacheableTypeSymbol(
            typeKind,
            fullName,
            fullNameNonNullable,
            constructedFromFullName,
            isNullable,
            isValueType,
            friendlyName,
            hasDefaultConstructor,
            isNamedType,
            nullableAnnotation,
            enumUnderlyingType,
            enumSerializableSettingsAttribute,
            enumMembers,
            queryStringSerializableCustomParseMethod,
            commandLineSerializableCustomParseMethod,
            jsonSerializableCustomParseMethod,
            jsonSerializableCustomSerializeMethod,
            selfSerializableAttributes
        );

        // Make sure we update the self-reference.
        if (selfSerializableAttributes != null)
        {
            foreach (var attribute in selfSerializableAttributes)
            {
                attribute?.SetSelfSerializedTypeUnsafe(returnSymbol, returnSymbol);
            }
        }

        symbolCache.Add(fullName, returnSymbol);
        switch (symbol)
        {
            case INamedTypeSymbol namedTypeSymbol:
            {
                if (isNullable)
                {
                    returnSymbol.nonNullableType = FromITypeSymbol(
                        symbolCache,
                        reportDiagnostic,
                        namedTypeSymbol.TypeArguments[0]
                    );
                }

                var typeArguments = namedTypeSymbol
                    .TypeArguments.Select(t => FromITypeSymbol(symbolCache, reportDiagnostic, t))
                    .ToArray();
                var interfaces = namedTypeSymbol
                    .Interfaces.Select(i => FromITypeSymbol(symbolCache, reportDiagnostic, i))
                    .ToArray();

                returnSymbol.Interfaces = interfaces;
                returnSymbol.TypeArguments = typeArguments;
                break;
            }

            case IArrayTypeSymbol arrayTypeSymbol:
            {
                returnSymbol.ArrayElementType = FromITypeSymbol(
                    symbolCache,
                    reportDiagnostic,
                    arrayTypeSymbol.ElementType
                );
                break;
            }

            default:
            {
                // Nothing to do.
                break;
            }
        }

        return returnSymbol;
    }

    /// <summary>
    /// Gets the friendly name for errors of a type.
    /// </summary>
    /// <param name="type">Type to get name of.</param>
    /// <returns>The friendly name.</returns>
    public static string GetFriendlyName(ITypeSymbol type)
    {
        if (type.TryGetFriendlyNameAttribute(out var friendlyNameAttribute))
        {
            return friendlyNameAttribute;
        }

        return type.GetFullNameNonNullable() switch
        {
            "bool" => "bool",
            "byte" => "uint8",
            "sbyte" => "int8",
            "short" => "int16",
            "int" => "int32",
            "long" => "int64",
            "ushort" => "uint16",
            "uint" => "uint32",
            "ulong" => "uint64",
            "string" => "string",
            "float" => "single",
            "double" => "double",
            "decimal" => "decimal",
            "global::System.Guid" => "Guid",
            "global::System.DateOnly" => "DateOnly",
            "global::System.DateTime" => "DateTime",
            "global::System.DateTimeOffset" => "DateTimeOffset",
            "global::System.TimeOnly" => "TimeOnly",
            "global::System.TimeSpan" => "TimeSpan",
            "global::System.Text.Json.Nodes.JsonNode" => "object",
            "global::System.Text.Json.Nodes.JsonObject" => "object",
            "global::System.Text.Json.Nodes.JsonArray" => "array",
            "global::KestrelToolbox.Types.UUIDv1" => "UUIDv1",
            _ => type.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat),
        };
    }

    /// <summary>
    /// Compare two <see cref="CacheableTypeSymbol"/> in the simplest way.
    /// </summary>
    /// <param name="left">First type to compare.</param>
    /// <param name="right">Second type to compare.</param>
    /// <returns>True if the two are equivalent, false if not.</returns>
    /// <remarks>
    /// This is to prevent infinite recursion in <see cref="Equals(CacheableTypeSymbol?)"/> when comparing with types
    /// that contain themselves. They should be short-circuited by reference comparison, but I don't believe that those
    /// survive caching multiple times.
    /// </remarks>
    public static bool SimpleEquals(CacheableTypeSymbol? left, CacheableTypeSymbol? right)
    {
        if (ReferenceEquals(left, right))
        {
            return true;
        }

        if (left is null || right is null)
        {
            return false;
        }

        return SuperSimpleEquals(left.nonNullableType, right.nonNullableType)
            && left.TypeKind == right.TypeKind
            && left.FullName == right.FullName
            && left.FullNameNonNullable == right.FullNameNonNullable
            && left.ConstructedFromFullName == right.ConstructedFromFullName
            && SuperSimpleEquals(left.ArrayElementType, right.ArrayElementType)
            && SuperSimpleEquals(left.TypeArguments, right.TypeArguments)
            && SuperSimpleEquals(left.Interfaces, right.Interfaces)
            && left.IsNullable == right.IsNullable
            && left.IsValueType == right.IsValueType
            && left.FriendlyName == right.FriendlyName
            && left.HasDefaultConstructor == right.HasDefaultConstructor
            && left.IsNamedType == right.IsNamedType
            && left.NullableAnnotation == right.NullableAnnotation
            && Equals(left.JsonSerializableCustomParseMethod, right.JsonSerializableCustomParseMethod)
            && Equals(left.JsonSerializableCustomSerializeMethod, right.JsonSerializableCustomSerializeMethod)
            && Equals(left.QueryStringSerializableCustomParseMethod, right.QueryStringSerializableCustomParseMethod);
    }

    /// <summary>
    /// Recursively finds an interface with the exact type. Please don't fill out generic parameters.
    /// <c>global::System.Collections.Generic.IEnumerable&lt;T&gt;</c>.
    /// </summary>
    /// <param name="name">Name of the interface to find.</param>
    /// <returns>The symbol of the interface if found.</returns>
    public CacheableTypeSymbol? FindInterface(string name)
    {
        if (this.ConstructedFromFullName == name)
        {
            return this;
        }

        foreach (var childInterface in this.Interfaces)
        {
            var child = childInterface.FindInterface(name);
            if (child != null)
            {
                return child;
            }
        }

        return null;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not CacheableTypeSymbol other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public bool Equals(CacheableTypeSymbol? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return SimpleEquals(this.nonNullableType, other.nonNullableType)
            && this.TypeKind == other.TypeKind
            && this.FullName == other.FullName
            && this.FullNameNonNullable == other.FullNameNonNullable
            && this.ConstructedFromFullName == other.ConstructedFromFullName
            && SimpleEquals(this.ArrayElementType, other.ArrayElementType)
            && SimpleEquals(this.TypeArguments, other.TypeArguments)
            && SimpleEquals(this.Interfaces, other.Interfaces)
            && this.IsNullable == other.IsNullable
            && this.IsValueType == other.IsValueType
            && this.FriendlyName == other.FriendlyName
            && this.HasDefaultConstructor == other.HasDefaultConstructor
            && this.IsNamedType == other.IsNamedType
            && this.NullableAnnotation == other.NullableAnnotation
            && Equals(this.JsonSerializableCustomParseMethod, other.JsonSerializableCustomParseMethod)
            && Equals(this.JsonSerializableCustomSerializeMethod, other.JsonSerializableCustomSerializeMethod)
            && Equals(this.QueryStringSerializableCustomParseMethod, other.QueryStringSerializableCustomParseMethod)
            && Equals(this.SelfSerializableAttributes, other.SelfSerializableAttributes);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.FullName, this.TypeKind);
    }

    /// <summary>
    /// Compares two <see cref="CacheableTypeSymbol"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableTypeSymbol? left, CacheableTypeSymbol? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableTypeSymbol"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableTypeSymbol? left, CacheableTypeSymbol? right)
    {
        return !Equals(left, right);
    }

    /// <summary>
    /// Compare two <see cref="CacheableTypeSymbol"/> in the simplest way.
    /// </summary>
    /// <param name="other">Other type to compare.</param>
    /// <returns>True if the two are equivalent, false if not.</returns>
    /// <remarks>
    /// This is to prevent infinite recursion in <see cref="Equals(CacheableTypeSymbol?)"/> when comparing with types
    /// that contain themselves. They should be short circuited by reference comparison but I don't believe that those
    /// survive caching multiple times.
    /// </remarks>
    public bool SimpleEquals(CacheableTypeSymbol? other) => SimpleEquals(this, other);

    private static bool SimpleEquals(CacheableTypeSymbol[] left, CacheableTypeSymbol[] right)
    {
        if (left.Length != right.Length)
        {
            return false;
        }

        for (var i = 0; i < left.Length; i++)
        {
            if (SimpleEquals(left[i], right[i]))
            {
                return false;
            }
        }

        return true;
    }

    private static bool SuperSimpleEquals(CacheableTypeSymbol? left, CacheableTypeSymbol? right)
    {
        if (ReferenceEquals(left, right))
        {
            return true;
        }

        if (left is null || right is null)
        {
            return false;
        }

        return left.FullName == right.FullName && left.TypeKind == right.TypeKind;
    }

    private static bool SuperSimpleEquals(CacheableTypeSymbol[] left, CacheableTypeSymbol[] right)
    {
        if (left.Length != right.Length)
        {
            return false;
        }

        for (var i = 0; i < left.Length; i++)
        {
            if (SuperSimpleEquals(left[i], right[i]))
            {
                return false;
            }
        }

        return true;
    }
}
