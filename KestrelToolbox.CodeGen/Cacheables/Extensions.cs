// <copyright file="Extensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// Extension methods to help out cacheables.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Aggregates a hash code across a collection.
    /// </summary>
    /// <param name="collection">The collection to get hash codes of.</param>
    /// <typeparam name="T">Type of collection.</typeparam>
    /// <returns>Hash code of the collection.</returns>
    public static int AggregateHashCode<T>(this IList<T> collection)
    {
        var hashCode = collection.Count.GetHashCode();
        return collection.Aggregate(hashCode, (current, item) => (current * 397) ^ (item?.GetHashCode() ?? 0));
    }

    /// <summary>
    /// Aggregates a hash code across a collection.
    /// </summary>
    /// <param name="collection">The collection to get hash codes of.</param>
    /// <typeparam name="T">Type of collection.</typeparam>
    /// <returns>Hash code of the collection.</returns>
    public static int AggregateHashCode<T>(this IEnumerable<T> collection)
    {
        var value = 0;
        var count = 0;
        foreach (var item in collection)
        {
            count++;
            value = (value * 397) ^ (item?.GetHashCode() ?? 0);
        }

        value = (value * 397) ^ count.GetHashCode();
        return value;
    }

    /// <summary>
    /// Gets the <see cref="Location"/> from the first <see cref="SyntaxReference"/> in a collection.
    /// </summary>
    /// <param name="syntaxReferences">Collection of <see cref="SyntaxReference"/>.</param>
    /// <returns>The <see cref="Location"/> in source or <c>null</c>.</returns>
    public static Location? GetFirstLocationOrDefault(this ImmutableArray<SyntaxReference> syntaxReferences)
    {
        if (syntaxReferences.Length == 0)
        {
            return null;
        }

        var syntaxReference = syntaxReferences[0];
        return syntaxReference.SyntaxTree.GetLocation(syntaxReference.Span);
    }
}
