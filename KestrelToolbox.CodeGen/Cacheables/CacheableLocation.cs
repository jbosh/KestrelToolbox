// <copyright file="CacheableLocation.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;

namespace KestrelToolbox.CodeGen.Cacheables;

/// <summary>
/// Cacheable location.
/// </summary>
public sealed class CacheableLocation : IEquatable<CacheableLocation>
{
    /// <summary>
    /// Gets the file path of the location.
    /// </summary>
    public string FilePath { get; }

    /// <summary>
    /// Gets the source span for this location.
    /// </summary>
    public TextSpan SourceSpan { get; }

    /// <summary>
    /// Gets the line position of this location.
    /// </summary>
    public LinePositionSpan LinePositionSpan { get; }

    /// <summary>
    /// The original location for reporting errors in code. It balloons memory to keep around but it's not possible to silence errors
    /// without having the original SyntaxTree.
    /// </summary>
    private readonly Location originalLocation;

    /// <summary>
    /// Initializes a new instance of the <see cref="CacheableLocation"/> class.
    /// </summary>
    /// <param name="filePath">The file path of the location.</param>
    /// <param name="sourceSpan">The source span for this location.</param>
    /// <param name="linePositionSpan">The line position of this location.</param>
    /// <param name="originalLocation">The location of the original. </param>
    private CacheableLocation(
        string filePath,
        TextSpan sourceSpan,
        LinePositionSpan linePositionSpan,
        Location originalLocation
    )
    {
        this.FilePath = filePath;
        this.SourceSpan = sourceSpan;
        this.LinePositionSpan = linePositionSpan;
        this.originalLocation = originalLocation;
    }

    /// <summary>
    /// Gets a <see cref="CacheableLocation"/> from <see cref="Location"/>. Will return null if <paramref name="location"/> is not in source.
    /// </summary>
    /// <param name="location">Location to make cacheable.</param>
    /// <returns>New instance of <see cref="CacheableLocation"/> or null if it is not in source.</returns>
    public static CacheableLocation? FromLocation(Location? location)
    {
        if (location == null || !location.IsInSource)
        {
            return null;
        }

        var lineSpan = location.GetLineSpan();
        return new CacheableLocation(lineSpan.Path, location.SourceSpan, lineSpan.Span, location);
    }

    /// <summary>
    /// Gets a <see cref="Location"/> for use in diagnostics.
    /// </summary>
    /// <returns>The new instance of <see cref="Location"/>.</returns>
    public Location GetLocation() => this.originalLocation;

    /// <inheritdoc />
    public bool Equals(CacheableLocation? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.FilePath == other.FilePath
            && this.SourceSpan.Equals(other.SourceSpan)
            && this.LinePositionSpan.Equals(other.LinePositionSpan);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is CacheableLocation other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.FilePath, this.SourceSpan, this.LinePositionSpan);
    }

    /// <summary>
    /// Compares two <see cref="CacheableLocation"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CacheableLocation? left, CacheableLocation? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CacheableLocation"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CacheableLocation? left, CacheableLocation? right)
    {
        return !Equals(left, right);
    }
}
