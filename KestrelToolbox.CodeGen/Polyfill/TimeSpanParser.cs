// <copyright file="TimeSpanParser.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Linq;

namespace KestrelToolbox.CodeGen.Polyfill;

/// <summary>
/// Helper class for parsing <see cref="TimeSpan"/> from more complicated input.
/// </summary>
/// <remarks>
/// This should be identical to real version.
/// </remarks>
internal static class TimeSpanParser
{
    private static readonly char[] ColonSeparator = new[] { ':' };

    /// <summary>
    /// Tries to parse timespan in the format of 0.0 parses into seconds. 1:23 parses to 1 hour, 23 minutes.
    /// </summary>
    /// <param name="input">Formatted timespan.</param>
    /// <param name="result">Result of the parsing.</param>
    /// <returns>True if parsing is successful, false otherwise.</returns>
    /// <remarks>
    /// Any times that have a suffix will be parsed as a floating point value.
    /// Valid suffixes.
    /// <ul>
    /// <li>s - minute</li>
    /// <li>m - minute</li>
    /// <li>h - hour</li>
    /// <li>d - day</li>
    /// </ul>
    /// <ul>
    /// <li>ns - nanosecond</li>
    /// <li>ms - millisecond</li>
    /// <li>min - minute</li>
    /// <li>hr - hour</li>
    /// <li>yr - year</li>
    /// </ul>
    /// </remarks>
    public static bool TryParse(string? input, out TimeSpan result)
    {
        result = TimeSpan.Zero;

        if (input == null || string.IsNullOrEmpty(input))
        {
            return false;
        }

        if (IsLetter(input[input.Length - 1]))
        {
            // has a suffix, we should just have a double value
            var suffixIndex = input.Length - 1;
            for (; suffixIndex >= 0; suffixIndex--)
            {
                if (!IsLetter(input[suffixIndex]))
                {
                    break;
                }
            }

            suffixIndex++;

            if (suffixIndex <= 0)
            {
                return false;
            }

            var suffix = input.Substring(suffixIndex);
            var number = input.Substring(0, suffixIndex);
            if (!double.TryParse(number, out var d))
            {
                return false;
            }

            switch (suffix)
            {
                case "y":
                case "yr":
                case "yrs":
                case "years":
                {
                    result = TimeSpan.FromDays(d * 365.0);
                    break;
                }

                case "d":
                case "day":
                case "days":
                {
                    result = TimeSpan.FromDays(d);
                    break;
                }

                case "h":
                case "hr":
                case "hrs":
                {
                    result = TimeSpan.FromHours(d);
                    break;
                }

                case "m":
                case "min":
                {
                    result = TimeSpan.FromMinutes(d);
                    break;
                }

                case "s":
                case "sec":
                {
                    result = TimeSpan.FromSeconds(d);
                    break;
                }

                case "ms":
                {
                    result = TimeSpan.FromMilliseconds(d);
                    break;
                }

                case "us":
                {
                    result = TimeSpan.FromMilliseconds(d / 1000.0);
                    break;
                }

                case "ns":
                {
                    result = TimeSpan.FromMilliseconds(d / 1000000.0);
                    break;
                }

                default:
                {
                    return false;
                }
            }
        }
        else
        {
            using var enumerator = input.Split(ColonSeparator, 3).Select(s => s).GetEnumerator();

            if (!enumerator.MoveNext())
            {
                // No input, this shouldn't happen in practice
                return false;
            }

            var hours = 0.0;
            var minutes = 0.0;
            if (!double.TryParse(enumerator.Current, out var seconds))
            {
                return false;
            }

            if (enumerator.MoveNext())
            {
                hours = seconds;
                seconds = 0.0;
                if (!double.TryParse(enumerator.Current, out minutes))
                {
                    return false;
                }

                if (enumerator.MoveNext())
                {
                    if (!double.TryParse(enumerator.Current, out seconds))
                    {
                        return false;
                    }

                    if (enumerator.MoveNext())
                    {
                        return false;
                    }
                }
            }

            var totalSeconds = (((hours * 60) + minutes) * 60) + seconds;
            var ticks = totalSeconds * TimeSpan.TicksPerSecond;
            result = new TimeSpan((long)ticks);
        }

        return true;
    }

    private static bool IsLetter(char c) => c is (>= 'a' and <= 'z') or (>= 'A' and <= 'Z');
}
