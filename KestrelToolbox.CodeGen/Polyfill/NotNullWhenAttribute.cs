// <copyright file="NotNullWhenAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

#if NETSTANDARD2_0
namespace System.Diagnostics.CodeAnalysis;

/// <summary>
/// Specifies that when a method returns <see cref="ReturnValue"/>, the parameter will not be null even if the corresponding type allows it.
/// </summary>
[AttributeUsage(AttributeTargets.Parameter)]
[SuppressMessage(
    "StyleCop.CSharp.DocumentationRules",
    "SA1623:Property summary documentation should match accessors",
    Justification = "Dotnet standard library."
)]
internal sealed class NotNullWhenAttribute : Attribute
{
    /// <summary>
    /// Gets the return value condition.
    /// </summary>
    public bool ReturnValue { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="NotNullWhenAttribute"/> class.
    /// </summary>
    /// <param name="returnValue">The return value condition. If the method returns this value, the associated parameter will not be <c>null</c>.</param>
    public NotNullWhenAttribute(bool returnValue)
    {
        this.ReturnValue = returnValue;
    }
}

#endif // NETSTANDARD2_0
