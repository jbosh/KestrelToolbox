// <copyright file="StringExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

#if NETSTANDARD2_0
namespace System;

/// <summary>
/// Polyfill extensions for <see cref="string"/>.
/// </summary>
public static class StringExtensions
{
    /// <summary>
    /// Determines whether this string instance starts with the specified character.
    /// </summary>
    /// <param name="s">String to search.</param>
    /// <param name="value">The character to compare.</param>
    /// <returns><c>true</c> if <paramref name="value"/> matches the beginning of this <see cref="string"/>; otherwise, <c>false</c>.</returns>
    public static bool StartsWith(this string s, char value)
    {
        if (s.Length == 0)
        {
            return false;
        }

        return s[0] == value;
    }
}
#endif // NETSTANDARD2_0
