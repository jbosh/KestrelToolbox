// <copyright file="CommandLineSerializableGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for CommandLineSerializable.
/// </summary>
public sealed class CommandLineSerializableGenerator
    : ISerializableGenerator,
        IEquatable<CommandLineSerializableGenerator>
{
    /// <inheritdoc />
    public Dictionary<MethodReferenceKey, MethodReference> MethodReferences { get; set; }

    /// <inheritdoc />
    public SerializableAttributesExecutor ParentExecutor { get; set; } = null!;

    private readonly CacheableSerializableAttribute serializableAttribute;
    private readonly CacheableTypeSymbol serializedType;
    private readonly PropertyMember[] members;

    private CommandLineSerializableGenerator(
        CacheableSerializableAttribute serializableAttribute,
        CacheableTypeSymbol serializedType,
        PropertyMember[] members,
        Dictionary<MethodReferenceKey, MethodReference> methodReferences
    )
    {
        this.serializedType = serializedType;
        this.serializableAttribute = serializableAttribute;
        this.members = members;
        this.MethodReferences = methodReferences;
        this.Visibility = serializableAttribute.Visibility;
    }

    /// <inheritdoc />
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Prepares a new instance of <see cref="CommandLineSerializableGenerator"/> using semantic model or null if there was an error.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="serializableAttribute"><see cref="Serializable.SerializableAttribute"/> that initiated parsing these command line parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The <see cref="CommandLineSerializableGenerator"/> or null if there was an error.</returns>
    public static CommandLineSerializableGenerator? Prepare(
        SerializableAttributesGeneratorContext context,
        SerializableAttribute serializableAttribute,
        CancellationToken cancellationToken
    )
    {
        var serializedType = serializableAttribute.SerializedType;
        SerializableAttributesExecutor.GetNullableType(serializedType, out var baseType, out _);
        if (baseType.TypeKind is not TypeKind.Class and not TypeKind.Struct)
        {
            context.ReportDiagnostic(
                DiagnosticMessages.CreateCommandLineSerializableTypeInvalid(
                    serializableAttribute.AttributeSyntax.GetLocation(),
                    baseType.GetFullName(),
                    baseType.TypeKind
                )
            );
            return null;
        }

        var members = PropertyMember.GetMembers(context, baseType).ToList();
        cancellationToken.ThrowIfCancellationRequested();

        var methodReferences = new Dictionary<MethodReferenceKey, MethodReference>();
        var duplicateNames = new HashSet<string>();
        foreach (var member in members)
        {
            foreach (var name in member.member.Names)
            {
                if (!duplicateNames.Add(name))
                {
                    context.ReportDiagnostic(
                        DiagnosticMessages.CreateCommandLineSerializableDuplicateName(
                            serializableAttribute.GetLocation(),
                            baseType.GetFullName(),
                            name
                        )
                    );

                    return null;
                }
            }

            var memberType = member.member.Type;

            if (SerializableAttributesExecutor.IsBuiltInType(memberType))
            {
                continue;
            }

            if (memberType.CommandLineSerializableCustomParseMethod != null)
            {
                continue;
            }

            MethodReferenceKey key;
#pragma warning disable IDE0010
            switch (memberType.NonNullableType.TypeKind)
#pragma warning restore IDE0010
            {
                case TypeKind.Enum:
                {
                    key = new MethodReferenceKey(SerializableAttributeType.Enum, memberType.MethodKeyName);
                    break;
                }

                case TypeKind.Class:
                case TypeKind.Struct:
                {
                    key = new MethodReferenceKey(SerializableAttributeType.CommandLine, memberType.MethodKeyName);
                    break;
                }

                default:
                {
                    // Unknown type. This should not happen and is an unsupported type.
                    continue;
                }
            }

            if (
                !context.TryGetSerializedType(key, out var memberSerializableAttribute)
                || !memberSerializableAttribute.GenerateParse
            )
            {
                memberSerializableAttribute = Serializable.SerializableAttribute.FromProperty(
                    context.SymbolCache,
                    context.ReportDiagnostic,
                    key.Type,
                    serializableAttribute,
                    member.symbol.Type,
                    true,
                    false
                );
                if (
                    !context.TryAddSerializedType(
                        key,
                        memberSerializableAttribute,
                        serializableAttribute.SerializedType
                    )
                )
                {
                    return null;
                }
            }

            if (!methodReferences.ContainsKey(key))
            {
                var parsingMethodName = memberSerializableAttribute.ParsingMethodName;
                var parsingType = memberType.NonNullableType.FullName;
                var serializingMethodName = memberSerializableAttribute.SerializingMethodName;
                methodReferences.Add(
                    key,
                    new MethodReference(
                        parsingType,
                        parsingMethodName,
                        serializingMethodName,
                        memberSerializableAttribute.Visibility,
                        memberSerializableAttribute.IsImplicitlyGenerated
                    )
                );
            }
        }

        var cacheableSerializableAttribute = CacheableSerializableAttribute.FromSerializableAttribute(
            context,
            serializableAttribute,
            cancellationToken
        );
        var cacheableSerializedType = CacheableTypeSymbol.FromITypeSymbol(
            context.SymbolCache,
            context.ReportDiagnostic,
            serializedType
        );
        return new CommandLineSerializableGenerator(
            cacheableSerializableAttribute,
            cacheableSerializedType,
            members.Select(p => p.member).ToArray(),
            methodReferences
        );
    }

    /// <inheritdoc />
    public ISerializableGenerator Combine(Action<Diagnostic> reportDiagnostic, ISerializableGenerator otherGenerator)
    {
        if (otherGenerator is not CommandLineSerializableGenerator)
        {
            throw new NotImplementedException($"Should never combine mismatched {nameof(ISerializableGenerator)}.");
        }

        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public void PrepareToGenerate(Dictionary<MethodReferenceKey, ISerializableGenerator> generators)
    {
        // No prep at this time.
    }

    /// <inheritdoc />
    public bool Generate(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        try
        {
            if (this.serializableAttribute.GenerateParse)
            {
                var returnType =
                    $"{this.serializedType.FullName}{(this.serializedType.IsValueType ? string.Empty : "?")}";

                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Try to parse a string[] into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="args">List of command line arguments.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"public static bool {this.serializableAttribute.ParsingMethodName}(string[] args, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out {returnType} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)"
                );
                using (builder.AppendBlock())
                {
                    this.GenerateCommandLineParseMethodBody(reportDiagnostic, builder);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            reportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            return false;
        }
    }

    /// <inheritdoc />
    public bool Equals(CommandLineSerializableGenerator? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.serializableAttribute.Equals(other.serializableAttribute)
            && this.serializedType.Equals(other.serializedType)
            && this.members.SequenceEqual(other.members)
            && this.MethodReferences.SequenceEqual(other.MethodReferences);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is CommandLineSerializableGenerator other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return this.serializableAttribute.GetHashCode();
    }

    /// <summary>
    /// Compares two <see cref="CommandLineSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(CommandLineSerializableGenerator? left, CommandLineSerializableGenerator? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CommandLineSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(CommandLineSerializableGenerator? left, CommandLineSerializableGenerator? right)
    {
        return !Equals(left, right);
    }

    private void GenerateCommandLineParseMethodBody(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        var serializedType = this.serializableAttribute.SerializedType;
        var baseType = serializedType.NonNullableType;

        // Setup variables and their defaults.
        var requiredProperties = new Dictionary<string, (PropertyMember member, string boolName)>();

        var positionalArgumentsAttribute = default(string);
        var positionalArgumentRequired = false;

        var memberVariableNames = new string?[this.members.Length];
        for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
        {
            var member = this.members[memberIndex];
            if (member.PositionalArgument != null)
            {
                if (positionalArgumentsAttribute != null)
                {
                    reportDiagnostic(
                        DiagnosticMessages.CreateMultiplePositionalArgumentProperties(member.GetLocation())
                    );

                    return;
                }

                if (!member.IsParsedAsArray())
                {
                    reportDiagnostic(DiagnosticMessages.CreateInvalidPositionalArgumentType(member.GetLocation()));
                    return;
                }

                CacheableTypeSymbol elementType;
                if (member.Type.ArrayElementType != null)
                {
                    elementType = member.Type.ArrayElementType;
                }
                else
                {
                    elementType = member.Type.TypeArguments[0];
                }

                if (elementType.FullName != "string")
                {
                    reportDiagnostic(DiagnosticMessages.CreateInvalidPositionalArgumentType(member.GetLocation()));
                    return;
                }

                memberVariableNames[memberIndex] = builder.GetVariableName("member");
                positionalArgumentRequired = member.IsRequired;
                positionalArgumentsAttribute = memberVariableNames[memberIndex];
                builder.AppendLine(
                    $"var {positionalArgumentsAttribute} = new global::System.Collections.Generic.List<string>();"
                );

                continue;
            }

            if (member.Names.Count == 0)
            {
                memberVariableNames[memberIndex] = null;
                continue;
            }

            var name = member.Names[0];
            var memberVariableName = builder.GetVariableName("member");
            memberVariableNames[memberIndex] = memberVariableName;

            var propertyType = member.Type;
            if (member.IsParsedAsArray())
            {
                CacheableTypeSymbol elementType;
                if (member.Type.ArrayElementType != null)
                {
                    elementType = member.Type.ArrayElementType;
                }
                else
                {
                    elementType = propertyType.TypeArguments[0];
                }

                builder.AppendLine(
                    $"var {memberVariableName} = new global::System.Collections.Generic.List<{elementType.FullName}>();"
                );
            }
            else if (member.IsParsedAsSet())
            {
                var elementType = propertyType.TypeArguments[0];
                builder.AppendLine(
                    $"var {memberVariableName} = new global::System.Collections.Generic.HashSet<{elementType.FullName}>();"
                );
            }
            else
            {
                string defaultValue;

                if (member.IsRequired)
                {
                    defaultValue = "default!";
                }
                else
                {
                    defaultValue = member.DefaultValue ?? "default";

                    if (
                        defaultValue == "default"
                        && propertyType
                            is {
                                IsValueType: false,
                                NullableAnnotation: NullableAnnotation.None or NullableAnnotation.NotAnnotated
                            }
                    )
                    {
                        reportDiagnostic(
                            DiagnosticMessages.CreateCommandLineSerializableViolatesNullConstraints(
                                member.GetLocation(),
                                this.serializedType.FullName,
                                member.PropertyName
                            )
                        );

                        defaultValue = "default!";
                    }
                }

                builder.AppendLine($"{propertyType.FullName} {memberVariableName} = {defaultValue};");
            }

            if (member.IsRequired)
            {
                var boolName = builder.GetVariableName("required");
                builder.AppendLine($"var {boolName} = false;");
                requiredProperties.Add(name, (member, boolName));
            }
        }

        builder.AppendLine("for (var argIndex = 0; argIndex < args.Length; argIndex++)");
        using (builder.AppendBlock())
        {
            builder.AppendLine("var arg = args[argIndex];");
            builder.AppendLine("switch (arg)");
            using (builder.AppendBlock())
            {
                for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
                {
                    var member = this.members[memberIndex];
                    if (member.Names.Count == 0)
                    {
                        continue;
                    }

                    var name = member.Names[0];
                    var memberVariableName = memberVariableNames[memberIndex];
                    foreach (var memberName in member.Names)
                    {
                        builder.AppendLine($"case \"{memberName}\":");
                    }

                    using (builder.AppendBlock())
                    {
                        if (requiredProperties.TryGetValue(name, out var requiredTuple))
                        {
                            builder.AppendLine($"{requiredTuple.boolName} = true;");
                        }

                        if (member.IsParsedAsArray())
                        {
                            CacheableTypeSymbol elementType;
                            if (member.Type.ArrayElementType != null)
                            {
                                elementType = member.Type.ArrayElementType;
                            }
                            else
                            {
                                elementType = member.Type.TypeArguments[0];
                            }

                            var value = this.ParseIndividualCommandLineElement(
                                reportDiagnostic,
                                builder,
                                name,
                                member,
                                elementType
                            );
                            builder.AppendLine($"{memberVariableName}.Add({value});");
                        }
                        else if (member.IsParsedAsSet())
                        {
                            var elementType = member.Type.TypeArguments[0];
                            var value = this.ParseIndividualCommandLineElement(
                                reportDiagnostic,
                                builder,
                                name,
                                member,
                                elementType
                            );
                            builder.AppendLine($"{memberVariableName}.Add({value});");
                        }
                        else
                        {
                            var value = this.ParseIndividualCommandLineElement(
                                reportDiagnostic,
                                builder,
                                name,
                                member,
                                member.Type
                            );
                            builder.AppendLine($"{memberVariableName} = {value};");
                        }

                        builder.AppendLine("break;");
                    }
                }

                builder.AppendLine("default:");
                using (builder.AppendBlock())
                {
                    if (positionalArgumentsAttribute == null)
                    {
                        builder.AppendLine("result = default;");
                        builder.AppendLine("error = $\"Unknown argument given: {arg}\";");
                        builder.AppendLine("return false;");
                    }
                    else
                    {
                        builder.AppendLine($"{positionalArgumentsAttribute}.Add(arg);");
                        builder.AppendLine("break;");
                    }
                }
            }
        }

        // Loop through required properties and search for ones that have not been set.
        if (requiredProperties.Count != 0)
        {
            var anyFailedName = builder.GetVariableName();
            builder.AppendLine($"var {anyFailedName} = true;");
            foreach (var pair in requiredProperties)
            {
                builder.AppendLine($"{anyFailedName} &= {pair.Value.boolName};");
            }

            builder.AppendLine($"if (!{anyFailedName})");
            using (builder.AppendBlock())
            {
                var errorList = builder.GetVariableName();
                builder.AppendLine($"var {errorList} = new global::System.Collections.Generic.List<string>();");
                foreach (var pair in requiredProperties)
                {
                    builder.AppendLine($"if (!{pair.Value.boolName})");
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine($"{errorList}.Add(\"{pair.Value.member.Names[0]}\");");
                    }
                }

                builder.AppendLine(
                    $"error = $\"A required property was missing on {serializedType.FriendlyName}. Missing: {{string.Join(\", \", {errorList})}}.\";"
                );
                builder.AppendLine("result = default;");
                builder.AppendLine("return false;");
            }
        }

        if (positionalArgumentsAttribute != null)
        {
            if (positionalArgumentRequired)
            {
                builder.AppendLine($"if ({positionalArgumentsAttribute}.Count == 0)");
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        null,
                        "Some required positional arguments were missing."
                    );
                }
            }
        }

        bool hasDefaultConstructor;
        if (serializedType.IsNamedType)
        {
            hasDefaultConstructor = serializedType.HasDefaultConstructor;
        }
        else
        {
            throw new NotImplementedException($"Unknown type {serializedType}.");
        }

        if (hasDefaultConstructor)
        {
            builder.AppendLine($"result = new {baseType.FullNameNonNullable}()");
            builder.PushBlock();
            for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
            {
                var member = this.members[memberIndex];
                var memberVariableName = memberVariableNames[memberIndex];
                if (memberVariableName != null)
                {
                    if (member.PropertyName == null)
                    {
                        throw new Exception("PropertyName on a member cannot be null.");
                    }

                    if (member.IsParsedAsArray())
                    {
                        if (member.Type.ArrayElementType != null)
                        {
                            builder.AppendLine($"{member.PropertyName} = {memberVariableName}.ToArray(),");
                        }
                        else
                        {
                            builder.AppendLine($"{member.PropertyName} = {memberVariableName},");
                        }
                    }
                    else if (member.Type.ConstructedFromFullName == "global::System.Collections.Frozen.FrozenSet<T>")
                    {
                        builder.AppendLine(
                            $"{member.PropertyName} = global::System.Collections.Frozen.FrozenSet.ToFrozenSet({memberVariableName}),"
                        );
                    }
                    else
                    {
                        builder.AppendLine($"{member.PropertyName} = {memberVariableName},");
                    }
                }
            }

            builder.PopBlock(";");
        }
        else
        {
            var outputName = builder.GetVariableName("output");
            builder.AppendLine(
                $"var {outputName} = ({baseType.FullNameNonNullable})global::System.Runtime.CompilerServices.RuntimeHelpers.GetUninitializedObject(typeof({baseType.FullNameNonNullable}));"
            );
            for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
            {
                var member = this.members[memberIndex];
                var memberVariableName = memberVariableNames[memberIndex];
                if (memberVariableName != null)
                {
                    if (member.PropertyName == null)
                    {
                        throw new Exception("PropertyName on a member cannot be null.");
                    }

                    if (member.IsParsedAsArray())
                    {
                        if (member.Type.ArrayElementType != null)
                        {
                            builder.AppendLine($"{outputName}.{member.PropertyName} = {memberVariableName}.ToArray();");
                        }
                        else
                        {
                            builder.AppendLine($"{outputName}.{member.PropertyName} = {memberVariableName};");
                        }
                    }
                    else if (PropertyMember.IsParsedAsFrozenSet(member.Type))
                    {
                        builder.AppendLine(
                            $"{outputName}.{member.PropertyName} = global::System.Collections.Frozen.FrozenSet.ToFrozenSet({memberVariableName});"
                        );
                    }
                    else
                    {
                        builder.AppendLine($"{outputName}.{member.PropertyName} = {memberVariableName};");
                    }
                }
            }

            builder.AppendLine($"result = {outputName};");
        }

        builder.AppendLine("error = default;");
        builder.AppendLine("return true;");
    }

    private string ParseIndividualCommandLineElement(
        Action<Diagnostic> reportDiagnostic,
        CodeBuilder builder,
        string name,
        PropertyMember property,
        CacheableTypeSymbol elementType
    )
    {
        var baseElementType = elementType.NonNullableType;

        var outputValueName = builder.GetVariableName("output");
        builder.AppendLine($"{elementType.FullName} {outputValueName};");

        switch (baseElementType.FullName)
        {
            case "bool":
            {
                var inputValueName = builder.GetVariableName("input");

                builder.AppendLine(
                    $"if (argIndex + 1 >= args.Length || !global::KestrelToolbox.Serialization.Helpers.CommandLineParser.TryParseBool(args[argIndex + 1], out var {inputValueName}))"
                );

                AppendEmptyArgumentValueClauses(inputValueName);
                break;
            }

            case "byte":
            case "byte?":
            case "decimal":
            case "decimal?":
            case "double":
            case "double?":
            case "float":
            case "float?":
            case "int":
            case "int?":
            case "long":
            case "long?":
            case "sbyte":
            case "sbyte?":
            case "short":
            case "short?":
            case "uint":
            case "uint?":
            case "ulong":
            case "ulong?":
            case "ushort":
            case "ushort?":
            {
                var inputValueName = builder.GetVariableName("input");

                builder.AppendLine(
                    $"if (argIndex + 1 >= args.Length || !{baseElementType.FullNameNonNullable}.TryParse(args[argIndex + 1], out var {inputValueName}))"
                );

                AppendEmptyArgumentValueClauses(inputValueName);

                SerializableAttributesExecutor.ValidateNumberType(builder, outputValueName, name, property);
                break;
            }

            case "string":
            case "string?":
            {
                builder.AppendLine("if (argIndex + 1 >= args.Length)");

                if (property.EmptyArgumentValue != null)
                {
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine($"{outputValueName} = {property.EmptyArgumentValue.Value};");
                    }

                    using (builder.AppendBlock("else"))
                    {
                        builder.AppendLine($"{outputValueName} = args[argIndex + 1];");
                        builder.AppendLine("argIndex++;");
                    }
                }
                else
                {
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            $"Was not of type {baseElementType.FriendlyName}."
                        );
                    }

                    builder.AppendLine($"{outputValueName} = args[argIndex + 1];");
                    builder.AppendLine("argIndex++;");
                }

                SerializableAttributesExecutor.ValidateStringType(builder, outputValueName, name, property);
                break;
            }

            case "global::System.Guid":
            case "global::KestrelToolbox.Types.UUIDv1":
            case "global::System.TimeOnly":
            case "global::System.DateTime":
            case "global::System.DateTimeOffset":
            {
                var inputValueName = builder.GetVariableName("input");

                builder.AppendLine(
                    $"if (argIndex + 1 >= args.Length || !{baseElementType.FullNameNonNullable}.TryParse(args[argIndex + 1], out var {inputValueName}))"
                );

                AppendEmptyArgumentValueClauses(inputValueName);
                break;
            }

            case "global::System.DateOnly":
            {
                var inputValueName = builder.GetVariableName("input");
                var dateOnlySettings = property.DateOnlyParseSettings;
                if (dateOnlySettings != null)
                {
                    builder.AppendLine(
                        $"if (argIndex + 1 >= args.Length || !global::System.DateOnly.TryParseExact(args[argIndex + 1], {dateOnlySettings.Format}, out var {inputValueName}))"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"if (argIndex + 1 >= args.Length || !global::System.DateOnly.TryParse(args[argIndex + 1], out var {inputValueName}))"
                    );
                }

                AppendEmptyArgumentValueClauses(inputValueName);
                break;
            }

            case "global::System.TimeSpan":
            {
                var inputValueName = builder.GetVariableName("input");

                builder.AppendLine(
                    $"if (argIndex + 1 >= args.Length || !global::KestrelToolbox.Serialization.TimeSpanParser.TryParse(args[argIndex + 1], out var {inputValueName}))"
                );

                AppendEmptyArgumentValueClauses(inputValueName);
                break;
            }

            default:
            {
                if (baseElementType.TypeKind == TypeKind.Enum)
                {
                    var key = new MethodReferenceKey(SerializableAttributeType.Enum, baseElementType.MethodKeyName);
                    var methodReference = this.MethodReferences[key];

                    var inputValueName = builder.GetVariableName("input");
                    builder.AppendLine("if (argIndex + 1 < args.Length)");
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine(
                            $"if (!{this.GetMethodName(methodReference.ParsingMethodName, methodReference.Visibility)}(args[argIndex + 1], out {methodReference.ParsingType} {inputValueName}, out error))"
                        );
                        if (property.EmptyArgumentValue != null)
                        {
                            using (builder.AppendBlock())
                            {
                                builder.AppendLine($"{outputValueName} = {property.EmptyArgumentValue.Value};");
                            }

                            using (builder.AppendBlock("else"))
                            {
                                builder.AppendLine($"{outputValueName} = {inputValueName};");
                                builder.AppendLine("argIndex++;");
                            }
                        }
                        else
                        {
                            using (builder.AppendBlock())
                            {
                                builder.AppendLine($"error = $\"{name} {{error}}\";");
                                SerializableAttributesExecutor.ReturnError(builder);
                            }

                            builder.AppendLine($"{outputValueName} = {inputValueName};");
                            builder.AppendLine("argIndex++;");
                        }
                    }

                    using (builder.AppendBlock("else"))
                    {
                        if (property.EmptyArgumentValue != null)
                        {
                            builder.AppendLine($"{outputValueName} = {property.EmptyArgumentValue.Value};");
                        }
                        else
                        {
                            builder.AppendLine($"error = $\"{name} Missing argument\";");
                            SerializableAttributesExecutor.ReturnError(builder);
                        }
                    }
                }
                else
                {
                    reportDiagnostic(
                        DiagnosticMessages.CreatePropertyTypeUnknown(
                            property.GetLocation(),
                            baseElementType.FullName,
                            "QueryStringSerializable"
                        )
                    );
                    throw new NotImplementedException($"Couldn't ParseIndividualElement {baseElementType.FullName}.");
                }

                break;
            }
        }

        return outputValueName;

        void AppendEmptyArgumentValueClauses(string inputValueName)
        {
            if (property.EmptyArgumentValue != null)
            {
                using (builder.AppendBlock())
                {
                    builder.AppendLine($"{outputValueName} = {property.EmptyArgumentValue.Value};");
                }

                using (builder.AppendBlock("else"))
                {
                    builder.AppendLine($"{outputValueName} = {inputValueName};");
                    builder.AppendLine("argIndex++;");
                }
            }
            else
            {
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                builder.AppendLine($"{outputValueName} = {inputValueName};");
                builder.AppendLine("argIndex++;");
            }
        }
    }

    private string GetMethodName(string name, SerializableVisibility visibility) =>
        this.ParentExecutor.GetMethodName(
            this.serializableAttribute.DeclaringType.FullNameNonNullable,
            name,
            visibility
        );
}
