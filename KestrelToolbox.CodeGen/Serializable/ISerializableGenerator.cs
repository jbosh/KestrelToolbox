// <copyright file="ISerializableGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generate code into <see cref="CodeBuilder"/>.
/// </summary>
public interface ISerializableGenerator
{
    /// <summary>
    /// Gets the visibility of this generator.
    /// </summary>
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Gets or sets the parent executor for this generator.
    /// </summary>
    public SerializableAttributesExecutor ParentExecutor { get; set; }

    /// <summary>
    /// Generate new code appended to <see cref="CodeBuilder"/>.
    /// </summary>
    /// <param name="reportDiagnostic">Action for reporting diagnostics.</param>
    /// <param name="builder">Builder to append into.</param>
    /// <returns>Returns true if generation was successful, false if there were errors.</returns>
    public bool Generate(Action<Diagnostic> reportDiagnostic, CodeBuilder builder);

    /// <summary>
    /// Combines another <see cref="ISerializableGenerator"/> into this one and returns the newly combined one.
    /// </summary>
    /// <param name="reportDiagnostic">Reports diagnostics.</param>
    /// <param name="otherGenerator">The other generator to combine.</param>
    /// <returns>The combination of <c>this</c> and <paramref name="otherGenerator"/>.</returns>
    public ISerializableGenerator Combine(Action<Diagnostic> reportDiagnostic, ISerializableGenerator otherGenerator);

    /// <summary>
    /// Gets or sets the method references used by this generator.
    /// </summary>
    public Dictionary<MethodReferenceKey, MethodReference> MethodReferences { get; set; }

    /// <summary>
    /// Finalizes any last bits and makes ready generate.
    /// </summary>
    /// <param name="generators">All generators associated with this generator's hosting class.</param>
    public void PrepareToGenerate(Dictionary<MethodReferenceKey, ISerializableGenerator> generators);
}
