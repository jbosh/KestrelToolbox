// <copyright file="EnumSerializableGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for EnumSerializable.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.OrderingRules",
    "SA1202:Elements should be ordered by access",
    Justification = "These classes are complicated."
)]
public sealed class EnumSerializableGenerator : ISerializableGenerator, IEquatable<EnumSerializableGenerator?>
{
    /// <inheritdoc />
    public Dictionary<MethodReferenceKey, MethodReference> MethodReferences { get; set; } = new();

    /// <inheritdoc />
    public SerializableAttributesExecutor ParentExecutor { get; set; } = null!;

    private readonly CacheableSerializableAttribute serializableAttribute;
    private readonly EnumSerializableSettingsAttribute enumSerializableSettingsAttribute;
    private readonly CacheableTypeSymbol serializedType;
    private readonly string underlyingType;
    private readonly EnumMember[] members;
    private readonly string errorName;

    private CacheableTypeSymbol GetEnumType() => this.serializedType.NonNullableType;

    private EnumSerializableGenerator(
        CacheableSerializableAttribute serializableAttribute,
        EnumSerializableSettingsAttribute enumSerializableSettingsAttribute,
        CacheableTypeSymbol serializedType,
        string underlyingType,
        EnumMember[] members,
        string errorName
    )
    {
        this.serializedType = serializedType;
        this.enumSerializableSettingsAttribute = enumSerializableSettingsAttribute;
        this.underlyingType = underlyingType;
        this.serializableAttribute = serializableAttribute;
        this.members = members;
        this.errorName = errorName;
        this.Visibility = serializableAttribute.Visibility;
    }

    /// <inheritdoc />
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Prepares a new instance of <see cref="EnumSerializableGenerator"/> using semantic model or null if there was an error.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="serializableAttribute"><see cref="Serializable.SerializableAttribute"/> that initiated parsing this enum.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The <see cref="EnumSerializableGenerator"/> or null if there was an error.</returns>
    public static EnumSerializableGenerator? Prepare(
        SerializableAttributesGeneratorContext context,
        SerializableAttribute serializableAttribute,
        CancellationToken cancellationToken
    )
    {
        var serializedType = serializableAttribute.SerializedType;
        SerializableAttributesExecutor.GetNullableType(serializedType, out var baseType, out _);
        if (baseType.TypeKind != TypeKind.Enum)
        {
            context.ReportDiagnostic(
                DiagnosticMessages.CreateSerializableEnumTypeNotEnum(
                    serializableAttribute.GetLocation(),
                    baseType.GetFullName(),
                    baseType.TypeKind
                )
            );

            return null;
        }

        var enumType = (INamedTypeSymbol)baseType;
        cancellationToken.ThrowIfCancellationRequested();

        _ = EnumSerializableSettingsAttribute.TryGet(enumType, out var enumSerializableSettingsAttribute);
        enumSerializableSettingsAttribute ??= new EnumSerializableSettingsAttribute();
        cancellationToken.ThrowIfCancellationRequested();

        var members = EnumMember.GetMembers(enumType).ToArray();
        cancellationToken.ThrowIfCancellationRequested();

        var underlyingType = "System.Int32";
        if (enumType.EnumUnderlyingType != null)
        {
            underlyingType = enumType.EnumUnderlyingType.GetFullName();
        }

        var duplicateNames = new HashSet<string>();
        foreach (var member in members)
        {
            foreach (var name in member.Names)
            {
                if (!duplicateNames.Add(name))
                {
                    context.ReportDiagnostic(
                        DiagnosticMessages.CreateEnumSerializableDuplicateName(
                            serializableAttribute.GetLocation(),
                            enumType.GetFullName(),
                            name
                        )
                    );
                }
            }
        }

        var errorName = serializedType.ToDisplayString(SymbolDisplayFormat.MinimallyQualifiedFormat);

        var cacheableSerializableAttribute = CacheableSerializableAttribute.FromSerializableAttribute(
            context,
            serializableAttribute,
            cancellationToken
        );
        var cacheableSerializableType = CacheableTypeSymbol.FromITypeSymbol(
            context.SymbolCache,
            context.ReportDiagnostic,
            serializedType
        );
        return new EnumSerializableGenerator(
            cacheableSerializableAttribute,
            enumSerializableSettingsAttribute,
            cacheableSerializableType,
            underlyingType,
            members,
            errorName
        );
    }

    /// <inheritdoc />
    public ISerializableGenerator Combine(Action<Diagnostic> reportDiagnostic, ISerializableGenerator otherGenerator)
    {
        if (otherGenerator is not EnumSerializableGenerator)
        {
            throw new NotImplementedException($"Should never combine mismatched {nameof(ISerializableGenerator)}.");
        }

        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public void PrepareToGenerate(Dictionary<MethodReferenceKey, ISerializableGenerator> generators)
    {
        // No prep at this time.
    }

    /// <inheritdoc />
    public bool Generate(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        try
        {
            if (this.serializableAttribute.GenerateParse)
            {
                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="value">Enum string value.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"public static bool {this.serializableAttribute.ParsingMethodName}(string value, out {this.serializedType.FullName} result) => {this.serializableAttribute.ParsingMethodName}(value, out result, out _);"
                );
                builder.AppendLine();

                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="value">Enum string value.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This val,ue is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"public static bool {this.serializableAttribute.ParsingMethodName}(string value, out {this.serializedType.FullName} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)"
                );
                using (builder.AppendBlock())
                {
                    this.GenerateEnumParseMethodBody(builder);
                }
            }

            if (this.serializableAttribute.GenerateSerialize)
            {
                var returnTypeNull = this.serializedType.IsNullable;
                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Serializes an enum value to the equivalent string format using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="value">The value to serialize to a string.</param>
                    /// <returns>The string version of <paramref name="value" />.</returns>);
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"public static string{(returnTypeNull ? "?" : string.Empty)} {this.serializableAttribute.SerializingMethodName}({this.serializedType.FullName} value)"
                );
                using (builder.AppendBlock())
                {
                    this.GenerateEnumSerializeMethodBody(builder);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            reportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            return false;
        }
    }

    /// <summary>
    /// Generates the body for a parse method. This should be surrounded by `builder.AppendBlock` and the signature should be exactly <c>bool TryParse(string value, out {enumFullName} result, [System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)</c>.
    /// </summary>
    /// <param name="builder">Builder to add to.</param>
    private void GenerateEnumParseMethodBody(CodeBuilder builder)
    {
        if (this.serializableAttribute.CustomParsingMethodName != null)
        {
            builder.AppendLine(
                $"return {this.serializableAttribute.CustomParsingMethodName}(value, out result, out error);"
            );
        }
        else if (this.enumSerializableSettingsAttribute.SerializeAsNumbers)
        {
            builder.AppendLine($"if (!{this.underlyingType}.TryParse(value, out var intValue))");
            using (builder.AppendBlock())
            {
                builder.AppendLine($"error = \"'{this.errorName}' must be an integer.\";");
                builder.AppendLine("result = default;");
                builder.AppendLine("return false;");
            }

            if (
                !this.enumSerializableSettingsAttribute.AllowAnyNumber
                || this.enumSerializableSettingsAttribute.InvalidValue != null
            )
            {
                builder.AppendLine("switch (intValue)");
                using (builder.AppendBlock())
                {
                    var hasMember = false;
                    foreach (var member in this.members)
                    {
                        builder.AppendLine($"case {member.ConstantValue}:");
                        hasMember = true;
                    }

                    if (hasMember)
                    {
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine("break;");
                        }
                    }

                    builder.AppendLine("default:");
                    using (builder.AppendBlock())
                    {
                        if (this.enumSerializableSettingsAttribute.InvalidValue != null)
                        {
                            builder.AppendLine(
                                $"intValue = ({this.underlyingType}){this.enumSerializableSettingsAttribute.InvalidValue};"
                            );
                            builder.AppendLine("break;");
                        }
                        else
                        {
                            var allNumbers = this.members.Select(m => m.ConstantValue).OrderBy(c => c);
                            Serializable.SerializableAttributesExecutor.ReturnError(
                                builder,
                                this.errorName,
                                $"Must be one of: {string.Join(", ", allNumbers)}."
                            );
                        }
                    }
                }
            }

            builder.AppendLine($"result = ({this.GetEnumType().FullName})intValue;");
            builder.AppendLine("error = default;");
            builder.AppendLine("return true;");
        }
        else
        {
            builder.AppendLine("switch (value)");
            using (builder.AppendBlock())
            {
                var hasName = false;
                var allNames = new List<string>();
                foreach (var member in this.members)
                {
                    if (member.Names.Count == 0)
                    {
                        continue;
                    }

                    foreach (var memberName in member.Names)
                    {
                        allNames.Add(memberName);
                        builder.AppendLine($"case \"{memberName}\":");
                        hasName = true;
                    }

                    if (hasName)
                    {
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine($"result = {member.FullName};");
                            builder.AppendLine("break;");
                        }
                    }
                }

                builder.AppendLine("default:");
                using (builder.AppendBlock())
                {
                    if (this.enumSerializableSettingsAttribute.InvalidValue == null)
                    {
                        allNames.Sort();
                        Serializable.SerializableAttributesExecutor.ReturnError(
                            builder,
                            this.errorName,
                            $"Must be one of: {string.Join(", ", allNames.OrderBy(v => v))}."
                        );
                    }
                    else
                    {
                        builder.AppendLine(
                            $"result = ({this.GetEnumType().FullName}){this.enumSerializableSettingsAttribute.InvalidValue};"
                        );
                        builder.AppendLine("break;");
                    }
                }
            }

            builder.AppendLine("error = default;");
            builder.AppendLine("return true;");
        }
    }

    /// <summary>
    /// Generates the body for a serialize method. This should be surrounded by `builder.AppendBlock` and the signature should be exactly <c>string Serialize({enumFullName} value)</c>.
    /// </summary>
    /// <param name="builder">Builder to add to.</param>
    private void GenerateEnumSerializeMethodBody(CodeBuilder builder)
    {
        if (this.enumSerializableSettingsAttribute.InvalidValue != null)
        {
            builder.AppendLine("switch (value)");
            using (builder.AppendBlock())
            {
                foreach (var member in this.members)
                {
                    builder.AppendLine($"case {member.FullName}:");
                }

                using (builder.AppendBlock())
                {
                    builder.AppendLine("break;");
                }

                builder.AppendLine("default:");
                using (builder.AppendBlock())
                {
                    builder.AppendLine(
                        $"value = ({this.GetEnumType().FullName}){this.enumSerializableSettingsAttribute.InvalidValue};"
                    );
                    builder.AppendLine("break;");
                }
            }
        }

        if (this.enumSerializableSettingsAttribute.SerializeAsNumbers)
        {
            builder.AppendLine(
                $"return (({this.underlyingType})value).ToString(System.Globalization.CultureInfo.InvariantCulture);"
            );
        }
        else
        {
            builder.AppendLine("return value switch");
            builder.PushBlock();
            foreach (var member in this.members)
            {
                if (member.Names.Count == 0)
                {
                    continue;
                }

                builder.AppendLine($"{member.FullName} => \"{member.Names[0]}\",");
            }

            if (this.serializedType.IsNullable)
            {
                builder.AppendLine("_ => null,");
            }
            else
            {
                builder.AppendLine("_ => value.ToString(),");
            }

            builder.PopBlock(";");
        }
    }

    /// <inheritdoc />
    public bool Equals(EnumSerializableGenerator? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.serializableAttribute.Equals(other.serializableAttribute)
            && this.enumSerializableSettingsAttribute.Equals(other.enumSerializableSettingsAttribute)
            && this.serializedType.Equals(other.serializedType)
            && this.underlyingType == other.underlyingType
            && this.members.Equals(other.members)
            && this.errorName == other.errorName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is EnumSerializableGenerator other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.serializableAttribute, this.enumSerializableSettingsAttribute, this.errorName);
    }

    /// <summary>
    /// Compares two <see cref="EnumSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(EnumSerializableGenerator? left, EnumSerializableGenerator? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="EnumSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(EnumSerializableGenerator? left, EnumSerializableGenerator? right)
    {
        return !Equals(left, right);
    }
}
