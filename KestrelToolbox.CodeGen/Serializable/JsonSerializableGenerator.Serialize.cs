// <copyright file="JsonSerializableGenerator.Serialize.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics;
using KestrelToolbox.CodeGen.Cacheables;
using KestrelToolbox.CodeGen.PropertyAttributes;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for JsonSerializable.
/// </summary>
public sealed partial class JsonSerializableGenerator
{
    private void GenerateJsonSerializeMethodBody(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        var serializedType = this.serializableAttribute.SerializedType;
        var baseType = serializedType.NonNullableType;

        if (this.serializableAttribute.CustomSerializingMethodName != null)
        {
            builder.AppendLine($"{this.serializableAttribute.CustomSerializingMethodName}(writer, value);");
        }
        else if (this.serializableAttribute.AliasedTypedefType != null)
        {
            var aliasedTypedefType = this.serializableAttribute.AliasedTypedefType;
            if (IsHandledAsJsonIndividualElement(aliasedTypedefType, false))
            {
                this.WriteIndividualElement(
                    builder,
                    null,
                    "value.value",
                    PropertyMember.FromType(aliasedTypedefType),
                    aliasedTypedefType
                );
            }
            else if (
                this.MethodReferences.TryGetValue(
                    new MethodReferenceKey(SerializableAttributeType.Json, aliasedTypedefType.MethodKeyName),
                    out var aliasedMethodReference
                )
            )
            {
                if (aliasedMethodReference.CustomSerializingMethodName != null)
                {
                    builder.AppendLine(
                        $"{this.GetMethodName(aliasedMethodReference.CustomSerializingMethodName, this.serializableAttribute.Visibility)}(writer, ({aliasedTypedefType.FullNameNonNullable})value);"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"{this.GetMethodName(aliasedMethodReference.SerializingMethodName, aliasedMethodReference.Visibility)}(writer, ({aliasedTypedefType.FullNameNonNullable})value);"
                    );
                }
            }
            else
            {
                builder.AppendLine($"{this.serializableAttribute.SerializingMethodName}(writer, value);");
            }
        }
        else if (IsHandledAsJsonIndividualElement(serializedType, false))
        {
            this.WriteIndividualElement(
                builder,
                null,
                "value",
                PropertyMember.FromType(serializedType),
                serializedType
            );
        }
        else if (
            serializedType.IsValueType
            && serializedType.IsNullable
            && (this.members.Length == 0 || Array.TrueForAll(this.members, n => n.Names.Count == 0))
            && this.MethodReferences.TryGetValue(
                new MethodReferenceKey(SerializableAttributeType.Json, baseType.MethodKeyName),
                out var baseMethodReference
            )
        )
        {
            builder.AppendLine("if (value.HasValue)");
            using (builder.AppendBlock())
            {
                if (baseMethodReference.CustomSerializingMethodName != null)
                {
                    builder.AppendLine(
                        $"{this.GetMethodName(baseMethodReference.CustomSerializingMethodName, this.serializableAttribute.Visibility)}(writer, value.Value);"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"{this.GetMethodName(baseMethodReference.SerializingMethodName, baseMethodReference.Visibility)}(writer, value.Value);"
                    );
                }
            }

            using (builder.AppendBlock("else"))
            {
                builder.AppendLine("writer.WriteNullValue();");
            }
        }
        else
        {
            if (this.members.Length == 0 || Array.TrueForAll(this.members, n => n.Names.Count == 0))
            {
                reportDiagnostic(
                    DiagnosticMessages.CreateJsonSerializableDidNotHaveAnyMembers(
                        this.serializableAttribute.GetLocation(),
                        serializedType.FullName
                    )
                );
            }

            builder.AppendLine("writer.WriteStartObject();");
            foreach (var member in this.members)
            {
                if (member.Names.Count == 0)
                {
                    continue;
                }

                var name = member.Names[0];
                var memberVariableName = builder.GetVariableName("member");
                builder.AppendLine($"var {memberVariableName} = value.{member.PropertyName};");
                this.WriteIndividualElement(builder, name, memberVariableName, member, member.Type);
            }

            builder.AppendLine("writer.WriteEndObject();");
        }
    }

    private void WriteIndividualElement(
        CodeBuilder builder,
        string? name,
        string inputName,
        PropertyMember propertyMember,
        CacheableTypeSymbol elementType
    )
    {
        var baseElementType = elementType.NonNullableType;
        if (
            propertyMember.CustomPropertyAttribute != null
            && propertyMember.CustomPropertyAttribute.SerializingMethodName != null
        )
        {
            WriteSimple(
                (writtenValue) =>
                {
                    var customSerializeMethod = propertyMember.CustomPropertyAttribute.SerializingMethodName;
                    if (name != null)
                    {
                        builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                    }

                    builder.AppendLine($"{customSerializeMethod}(writer, {writtenValue});");
                }
            );

            return;
        }

        switch (baseElementType.FullName)
        {
            case "bool":
            {
                var boolSerializableSettingsAttribute = propertyMember.BoolSerializableSettings;

                var serializeAsNumbers = boolSerializableSettingsAttribute.SerializeAsNumbers;

                WriteSimple(
                    (writtenValue) =>
                    {
                        if (serializeAsNumbers)
                        {
                            if (name == null)
                            {
                                builder.AppendLine($"writer.WriteNumberValue({writtenValue} ? 1 : 0);");
                            }
                            else
                            {
                                builder.AppendLine($"writer.WriteNumber({name.ToLiteral()}, {writtenValue} ? 1 : 0);");
                            }
                        }
                        else
                        {
                            if (name == null)
                            {
                                builder.AppendLine($"writer.WriteBooleanValue({writtenValue});");
                            }
                            else
                            {
                                builder.AppendLine($"writer.WriteBoolean({name.ToLiteral()}, {writtenValue});");
                            }
                        }
                    }
                );

                break;
            }

            case "byte":
            case "byte?":
            case "decimal":
            case "decimal?":
            case "double":
            case "double?":
            case "float":
            case "float?":
            case "int":
            case "int?":
            case "long":
            case "long?":
            case "sbyte":
            case "sbyte?":
            case "short":
            case "short?":
            case "uint":
            case "uint?":
            case "ulong":
            case "ulong?":
            case "ushort":
            case "ushort?":
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteNumberValue({writtenValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteNumber({name.ToLiteral()}, {writtenValue});");
                        }
                    }
                );
                break;
            }

            case "string":
            case "string?":
            {
                WriteString();
                break;
            }

            case "byte[]":
            case "byte[]?":
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteBase64StringValue({writtenValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteBase64String({name.ToLiteral()}, {writtenValue});");
                        }
                    },
                    emptyValueComparison: $"{inputName}.Length != 0"
                );
                break;
            }

            case "global::System.DateTime":
            case "global::System.DateTimeOffset":
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        var localizedValue =
                            $"{writtenValue}.ToUniversalTime().ToString(\"yyyy-MM-dd'T'HH:mm:ss.fffZ\", global::System.Globalization.CultureInfo.InvariantCulture)";

                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteStringValue({localizedValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteString({name.ToLiteral()}, {localizedValue});");
                        }
                    }
                );

                break;
            }

            case "global::System.DateOnly":
            {
                var dateOnlySettingsAttribute = propertyMember.DateOnlyParseSettings;
                var format = dateOnlySettingsAttribute?.Format;

                WriteSimple(
                    (writtenValue) =>
                    {
                        string localizedValue;
                        if (format != null)
                        {
                            localizedValue =
                                $"{writtenValue}.ToString({format}, global::System.Globalization.CultureInfo.InvariantCulture)";
                        }
                        else
                        {
                            localizedValue =
                                $"{writtenValue}.ToString(global::System.Globalization.CultureInfo.InvariantCulture)";
                        }

                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteStringValue({localizedValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteString({name.ToLiteral()}, {localizedValue});");
                        }
                    }
                );

                break;
            }

            case "global::System.TimeOnly":
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        var localizedValue = $"{writtenValue}.ToString(\"HH:mm:ss.FFFFFF\")";

                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteStringValue({localizedValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteString({name.ToLiteral()}, {localizedValue});");
                        }
                    }
                );

                break;
            }

            case "global::System.TimeSpan":
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        var localizedValue = $"{writtenValue}.ToString()";

                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteStringValue({localizedValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteString({name.ToLiteral()}, {localizedValue});");
                        }
                    }
                );

                break;
            }

            case "global::System.Guid":
            case "global::KestrelToolbox.Types.UUIDv1":
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        var localizedValue = $"{writtenValue}.ToString()";

                        if (name == null)
                        {
                            builder.AppendLine($"writer.WriteStringValue({localizedValue});");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteString({name.ToLiteral()}, {localizedValue});");
                        }
                    }
                );
                break;
            }

            case "global::System.Text.Json.Nodes.JsonNode":
            case "global::System.Text.Json.Nodes.JsonNode?":
            case "global::System.Text.Json.Nodes.JsonObject":
            case "global::System.Text.Json.Nodes.JsonObject?":
            case "global::System.Text.Json.Nodes.JsonArray":
            case "global::System.Text.Json.Nodes.JsonArray?":
            {
                var emptyValueComparison = baseElementType.FullName switch
                {
                    "global::System.Text.Json.Nodes.JsonNode" => default,
                    "global::System.Text.Json.Nodes.JsonNode?" => default,
                    "global::System.Text.Json.Nodes.JsonObject" => $"{inputName}.Count != 0",
                    "global::System.Text.Json.Nodes.JsonObject?" => $"{inputName} != null && {inputName}.Count != 0",
                    "global::System.Text.Json.Nodes.JsonArray" => $"{inputName}.Count != 0",
                    "global::System.Text.Json.Nodes.JsonArray?" => $"{inputName} != null && {inputName}.Count != 0",
                    _ => default,
                };
                WriteSimple(
                    (writtenValue) =>
                    {
                        if (name != null)
                        {
                            builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                        }

                        builder.AppendLine($"{writtenValue}.WriteTo(writer);");
                    },
                    emptyValueComparison: emptyValueComparison
                );
                break;
            }

            default:
            {
                if (
                    this.MethodReferences.TryGetValue(
                        new MethodReferenceKey(SerializableAttributeType.Json, baseElementType.MethodKeyName),
                        out var subObject
                    )
                    && subObject.CustomSerializingMethodName != null
                )
                {
                    WriteSimple(
                        (writtenValue) =>
                        {
                            if (name != null)
                            {
                                builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                            }

                            builder.AppendLine(
                                $"{this.GetMethodName(subObject.CustomSerializingMethodName, subObject.Visibility)}(writer, {writtenValue});"
                            );
                        }
                    );
                }
                else if (baseElementType.JsonSerializableCustomSerializeMethod != null)
                {
                    WriteSimple(
                        (writtenValue) =>
                        {
                            var customSerializeMethod = baseElementType.JsonSerializableCustomSerializeMethod;
                            if (name != null)
                            {
                                builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                            }

                            builder.AppendLine($"{customSerializeMethod.FullMethodName}(writer, {writtenValue});");
                        }
                    );
                }
                else if (
                    baseElementType.SelfSerializableAttributes?.Find(r =>
                        r.Type == SerializableAttributeType.Json && r.GenerateSerialize
                    ) != null
                )
                {
                    var serializableAttribute = baseElementType.SelfSerializableAttributes!.Find(r =>
                        r.Type == SerializableAttributeType.Json
                    );
                    WriteSimple(
                        (writtenValue) =>
                        {
                            if (name != null)
                            {
                                builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                            }

                            builder.AppendLine(
                                $"{baseElementType.FullNameNonNullable}.{serializableAttribute.SerializingMethodName}(writer, {writtenValue});"
                            );
                        }
                    );
                }
                else if (PropertyMember.IsPropertyTuple(baseElementType))
                {
                    builder.AppendLine($"writer.WritePropertyName({inputName}.Key);");
                    var tempVariable = builder.GetVariableName("token");
                    builder.AppendLine($"var {tempVariable} = {inputName}.Value;");
                    this.WriteIndividualElement(
                        builder,
                        null,
                        tempVariable,
                        PropertyMember.FromType(baseElementType.TypeArguments[1]),
                        baseElementType.TypeArguments[1]
                    );
                }
                else if (PropertyMember.IsParsedAsArray(baseElementType))
                {
                    WriteArray();
                }
                else if (baseElementType.TypeKind == TypeKind.Enum)
                {
                    WriteEnumValue();
                }
                else if (SerializableAttributesExecutor.FindGenericStringDictionary(baseElementType) != null)
                {
                    WriteDictionary();
                }
                else if (PropertyMember.IsIEnumerable(baseElementType))
                {
                    WriteEnumerable();
                }
                else if (subObject != null)
                {
                    WriteSimple(
                        (writtenValue) =>
                        {
                            if (name != null)
                            {
                                builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                            }

                            builder.AppendLine(
                                $"{this.GetMethodName(subObject.SerializingMethodName, subObject.Visibility)}(writer, {writtenValue});"
                            );
                        }
                    );
                }
                else
                {
                    throw new NotImplementedException(
                        $"Type {baseElementType.FullName} didn't get added to method references."
                    );
                }

                break;
            }
        }

        void WriteSimple(Action<string> onWriteCallback, string? emptyValueComparison = null)
        {
            var isNullable = elementType.IsNullable;

            var skipWritingCondition = propertyMember.SkipSerialization?.Condition ?? SkipSerializationCondition.None;
            var canWriteNull = true;
            if (elementType.NullableAnnotation != NullableAnnotation.Annotated && !isNullable)
            {
                skipWritingCondition &= ~SkipSerializationCondition.WhenNull;
                canWriteNull = false;
            }

            if (skipWritingCondition != SkipSerializationCondition.None)
            {
                var conditions = new List<string>();
                if (skipWritingCondition.HasFlag(SkipSerializationCondition.WhenNull))
                {
                    conditions.Add($"{inputName} != null");
                    canWriteNull = false;
                }

                if (skipWritingCondition.HasFlag(SkipSerializationCondition.WhenDefault))
                {
                    string defaultValueComparison;
                    if (SerializableAttributesExecutor.IsBasicType(baseElementType.FullName))
                    {
                        if (propertyMember.DefaultValue != null)
                        {
                            defaultValueComparison =
                                $"{inputName} != ({baseElementType.FullName}){propertyMember.DefaultValue}";
                        }
                        else
                        {
                            defaultValueComparison = $"{inputName} != default({baseElementType.FullName}";
                        }
                    }
                    else
                    {
                        if (propertyMember.DefaultValue != null)
                        {
                            defaultValueComparison =
                                $"!object.Equals({inputName}, ({baseElementType.FullName}){propertyMember.DefaultValue})";
                        }
                        else
                        {
                            defaultValueComparison =
                                $"!object.Equals({inputName}, default({baseElementType.FullName}))";
                        }
                    }

                    conditions.Add(defaultValueComparison);
                }

                if (skipWritingCondition.HasFlag(SkipSerializationCondition.WhenEmpty) && emptyValueComparison != null)
                {
                    conditions.Add(emptyValueComparison);
                }

                if (conditions.Count != 0)
                {
                    builder.PushBlock($"if ({string.Join(" && ", conditions)})");
                }
                else
                {
                    // Incorrect skip serialization attributes shouldn't kill the build.
                    builder.PushBlock("if (true)");
                }
            }

            if (isNullable)
            {
                if (canWriteNull)
                {
                    builder.AppendLine($"if ({inputName}.HasValue)");
                    using (builder.AppendBlock())
                    {
                        onWriteCallback($"{inputName}.Value");
                    }

                    using (builder.AppendBlock("else"))
                    {
                        if (name == null)
                        {
                            builder.AppendLine("writer.WriteNullValue();");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteNull({name.ToLiteral()});");
                        }
                    }
                }
                else
                {
                    onWriteCallback($"{inputName}.Value");
                }
            }
            else
            {
                if (canWriteNull)
                {
                    builder.AppendLine($"if ({inputName} != null)");
                    using (builder.AppendBlock())
                    {
                        onWriteCallback(inputName);
                    }

                    using (builder.AppendBlock("else"))
                    {
                        if (name == null)
                        {
                            builder.AppendLine("writer.WriteNullValue();");
                        }
                        else
                        {
                            builder.AppendLine($"writer.WriteNull({name.ToLiteral()});");
                        }
                    }
                }
                else
                {
                    onWriteCallback(inputName);
                }
            }

            if (skipWritingCondition != SkipSerializationCondition.None)
            {
                builder.PopBlock();
            }
        }

        void WriteString()
        {
            var skipWritingCondition = propertyMember.SkipSerialization?.Condition ?? SkipSerializationCondition.None;
            var canBeNull = elementType.NullableAnnotation == NullableAnnotation.Annotated;
            if (!canBeNull)
            {
                skipWritingCondition &= ~SkipSerializationCondition.WhenNull;
            }

            var currentInputName = inputName;
            var trimValue = propertyMember.TrimValue;
            if (trimValue != null && trimValue.OnSerialize)
            {
                var tmpVariable = builder.GetVariableName("token");
                if (trimValue.TrimChars == null)
                {
                    builder.AppendLine(
                        $"var {tmpVariable} = {currentInputName}{(canBeNull ? "?" : string.Empty)}.Trim();"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"var {tmpVariable} = {currentInputName}{(canBeNull ? "?" : string.Empty)}.Trim({trimValue.TrimChars});"
                    );
                }

                currentInputName = tmpVariable;
            }

            if (propertyMember.NullOnEmpty)
            {
                var tmpVariable = builder.GetVariableName("token");
                builder.AppendLine(
                    $"var {tmpVariable} = string.IsNullOrEmpty({currentInputName}) ? default : {currentInputName};"
                );
                currentInputName = tmpVariable;
            }

            if (skipWritingCondition != SkipSerializationCondition.None)
            {
                var defaultValue = propertyMember.DefaultValue ?? "default";

                var conditions = new List<string>();
                if (skipWritingCondition.HasFlag(SkipSerializationCondition.WhenNull))
                {
                    conditions.Add($"{currentInputName} != null");
                }

                if (skipWritingCondition.HasFlag(SkipSerializationCondition.WhenDefault))
                {
                    conditions.Add(
                        $"!string.Equals({currentInputName}, {defaultValue}, global::System.StringComparison.Ordinal)"
                    );
                }

                if (skipWritingCondition.HasFlag(SkipSerializationCondition.WhenEmpty))
                {
                    if (
                        baseElementType.NullableAnnotation == NullableAnnotation.Annotated
                        && !skipWritingCondition.HasFlag(SkipSerializationCondition.WhenNull)
                    )
                    {
                        conditions.Add($"{currentInputName}?.Length != 0");
                    }
                    else
                    {
                        conditions.Add($"{currentInputName}.Length != 0");
                    }
                }

                builder.PushBlock($"if ({string.Join(" && ", conditions)})");
            }

            if (name == null)
            {
                builder.AppendLine($"writer.WriteStringValue({currentInputName});");
            }
            else
            {
                builder.AppendLine($"writer.WriteString({name.ToLiteral()}, {currentInputName});");
            }

            if (skipWritingCondition != SkipSerializationCondition.None)
            {
                builder.PopBlock();
            }
        }

        void WriteArray()
        {
            var arrayElementType = baseElementType.ArrayElementType;
            if (arrayElementType == null)
            {
                // Not an array, must be a list.
                Debug.Assert(
                    baseElementType.ConstructedFromFullName == "global::System.Collections.Generic.List<T>",
                    "Expecting an array."
                );
                arrayElementType = baseElementType.TypeArguments[0];
            }

            if (
                arrayElementType.ConstructedFromFullName
                    == "global::System.Collections.Generic.KeyValuePair<TKey, TValue>"
                && arrayElementType.TypeArguments[0].FullName == "string"
            )
            {
                // Special case KeyValuePair<string, _>[] to be a dictionary.
                WriteDictionary();
                return;
            }

            var emptyValueComparison =
                $"{inputName}.{(baseElementType.TypeKind == TypeKind.Array ? "Length" : "Count")} != 0";

            WriteSimple(
                (writtenValue) =>
                {
                    var arrayLoopValue = builder.GetVariableName("token");
                    if (name != null)
                    {
                        builder.AppendLine($"writer.WriteStartArray({name.ToLiteral()});");
                    }
                    else
                    {
                        builder.AppendLine("writer.WriteStartArray();");
                    }

                    builder.AppendLine($"foreach (var {arrayLoopValue} in {writtenValue})");
                    using (builder.AppendBlock())
                    {
                        if (!this.TrySerializeEnumerableChildElement(builder, arrayElementType, arrayLoopValue))
                        {
                            this.WriteIndividualElement(
                                builder,
                                null,
                                arrayLoopValue,
                                PropertyMember.FromType(arrayElementType),
                                arrayElementType
                            );
                        }
                    }

                    builder.AppendLine("writer.WriteEndArray();");
                },
                emptyValueComparison: emptyValueComparison
            );
        }

        void WriteDictionary()
        {
            CacheableTypeSymbol arrayElementType;
            if (baseElementType.ArrayElementType != null)
            {
                arrayElementType = baseElementType.ArrayElementType.TypeArguments[1];
            }
            else if (
                baseElementType.ConstructedFromFullName == "global::System.Collections.Generic.Dictionary<TKey, TValue>"
            )
            {
                arrayElementType = baseElementType.TypeArguments[1];
            }
            else
            {
                var enumerableInterface = baseElementType.FindInterface(
                    "global::System.Collections.Generic.IEnumerable<T>"
                );
                Debug.Assert(
                    enumerableInterface != null,
                    "Needed to not have a null interface show up in dictionary writing."
                );
                var keyValuePairType = enumerableInterface!.TypeArguments[0];
                Debug.Assert(
                    keyValuePairType.ConstructedFromFullName
                        == "global::System.Collections.Generic.KeyValuePair<TKey, TValue>",
                    "Unexpected type of enumerable in dictionary"
                );
                arrayElementType = keyValuePairType.TypeArguments[1];
            }

            var emptyValueComparison =
                $"{inputName}.{(baseElementType.TypeKind == TypeKind.Array ? "Length" : "Count")} != 0";

            WriteSimple(
                (writtenValue) =>
                {
                    var arrayLoopValue = builder.GetVariableName("token");
                    if (name != null)
                    {
                        builder.AppendLine($"writer.WriteStartObject({name.ToLiteral()});");
                    }
                    else
                    {
                        builder.AppendLine("writer.WriteStartObject();");
                    }

                    builder.AppendLine($"foreach (var {arrayLoopValue} in {writtenValue})");
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine($"writer.WritePropertyName({arrayLoopValue}.Key);");
                        var pairValueName = builder.GetVariableName("token");
                        builder.AppendLine($"var {pairValueName} = {arrayLoopValue}.Value;");

                        if (!this.TrySerializeEnumerableChildElement(builder, arrayElementType, pairValueName))
                        {
                            this.WriteIndividualElement(
                                builder,
                                null,
                                pairValueName,
                                PropertyMember.FromType(arrayElementType),
                                arrayElementType
                            );
                        }
                    }

                    builder.AppendLine("writer.WriteEndObject();");
                },
                emptyValueComparison: emptyValueComparison
            );
        }

        void WriteEnumerable()
        {
            var enumerableType = baseElementType.FindInterface("global::System.Collections.Generic.IEnumerable<T>");
            Debug.Assert(enumerableType != null, "Shouldn't call WriteEnumerable with non interface.");

            var arrayElementType = enumerableType!.TypeArguments[0];
            if (
                arrayElementType.ConstructedFromFullName
                    == "global::System.Collections.Generic.KeyValuePair<TKey, TValue>"
                && arrayElementType.TypeArguments[0].FullName == "string"
            )
            {
                // Special case IEnumerable<KeyValuePair<string, _>> to be a dictionary.
                WriteDictionary();
                return;
            }

            WriteSimple(
                (writtenValue) =>
                {
                    var arrayLoopValue = builder.GetVariableName("token");
                    if (name != null)
                    {
                        builder.AppendLine($"writer.WriteStartArray({name.ToLiteral()});");
                    }
                    else
                    {
                        builder.AppendLine("writer.WriteStartArray();");
                    }

                    builder.AppendLine($"foreach (var {arrayLoopValue} in {writtenValue})");
                    using (builder.AppendBlock())
                    {
                        if (!this.TrySerializeEnumerableChildElement(builder, arrayElementType, arrayLoopValue))
                        {
                            this.WriteIndividualElement(
                                builder,
                                null,
                                arrayLoopValue,
                                PropertyMember.FromType(arrayElementType),
                                arrayElementType
                            );
                        }
                    }

                    builder.AppendLine("writer.WriteEndArray();");
                }
            );
        }

        void WriteEnumValue()
        {
            if (elementType.FullName == this.serializableAttribute.SerializedType.FullName)
            {
                // If we're generating the serialize method for enum, do it here.
                var enumSerializableSettings =
                    baseElementType.EnumSerializableSettingsAttribute ?? new EnumSerializableSettingsAttribute();
                Debug.Assert(
                    baseElementType.EnumMembers != null,
                    "Enum members should never be null for an enum type."
                );
                var enumMembers = baseElementType.EnumMembers!;
                var invalidValue = enumSerializableSettings.InvalidValue;

                WriteSimple(
                    (writtenValue) =>
                    {
                        if (enumSerializableSettings.SerializeAsNumbers)
                        {
                            if (name != null)
                            {
                                builder.AppendLine(
                                    $"writer.WriteNumber({name}, ({baseElementType.EnumUnderlyingType}){writtenValue});"
                                );
                            }
                            else
                            {
                                builder.AppendLine(
                                    $"writer.WriteNumberValue(({baseElementType.EnumUnderlyingType}){writtenValue});"
                                );
                            }
                        }
                        else
                        {
                            var tempVariableName = builder.GetVariableName("token");
                            builder.AppendLine($"var {tempVariableName} = {writtenValue} switch");
                            builder.PushBlock();
                            var defaultValue = default(string);
                            foreach (var member in enumMembers)
                            {
                                if (member.Names.Count == 0)
                                {
                                    continue;
                                }

                                if (invalidValue == member.ConstantValue)
                                {
                                    defaultValue = member.Names[0].ToLiteral();
                                }

                                builder.AppendLine($"{member.FullName} => {member.Names[0].ToLiteral()},");
                            }

                            defaultValue ??= $"(({baseElementType.EnumUnderlyingType}){writtenValue}).ToString()";
                            builder.AppendLine($"_ => {defaultValue}.ToString(),");
                            builder.PopBlock(";");

                            if (name != null)
                            {
                                builder.AppendLine($"writer.WriteString({name}, {tempVariableName});");
                            }
                            else
                            {
                                builder.AppendLine($"writer.WriteStringValue({tempVariableName});");
                            }
                        }
                    }
                );
            }
            else if (
                this.MethodReferences.TryGetValue(
                    new MethodReferenceKey(SerializableAttributeType.Json, baseElementType.MethodKeyName),
                    out var serializedEnumAttribute
                )
            )
            {
                WriteSimple(
                    (writtenValue) =>
                    {
                        if (name != null)
                        {
                            builder.AppendLine($"writer.WritePropertyName({name.ToLiteral()});");
                        }

                        builder.AppendLine(
                            $"{this.GetMethodName(serializedEnumAttribute.SerializingMethodName, serializedEnumAttribute.Visibility)}(writer, {writtenValue});"
                        );
                    }
                );
            }
            else
            {
                throw new NotImplementedException(
                    $"Enum {elementType.FullName} didn't get added to method references."
                );
            }
        }
    }

    private bool TrySerializeEnumerableChildElement(CodeBuilder builder, CacheableTypeSymbol type, string value)
    {
        if (
            SerializableAttributesExecutor.IsBasicType(type)
            || !this.MethodReferences.TryGetValue(
                new MethodReferenceKey(SerializableAttributeType.Json, type.MethodKeyName),
                out var method
            )
        )
        {
            return false;
        }

        var supportsNull = type.IsNullable || type.NullableAnnotation == NullableAnnotation.Annotated;
        if (supportsNull)
        {
            builder.AppendLine($"if ({value} == null)");
            using (builder.AppendBlock())
            {
                builder.AppendLine("writer.WriteNullValue();");
            }

            using (builder.AppendBlock("else"))
            {
                builder.AppendLine(
                    $"{this.GetMethodName(method.SerializingMethodName, method.Visibility)}(writer, {value});"
                );
            }
        }
        else
        {
            builder.AppendLine(
                $"{this.GetMethodName(method.SerializingMethodName, method.Visibility)}(writer, {value});"
            );
        }

        return true;
    }
}
