// <copyright file="SerializableAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using KestrelToolbox.CodeGen.Typedefs;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Type of <see cref="SerializableAttribute"/>.
/// </summary>
public enum SerializableAttributeType
{
    /// <summary>
    /// The type is unknown.
    /// </summary>
    Unknown,

    /// <summary>
    /// Type is an Enum.
    /// </summary>
    Enum,

    /// <summary>
    /// Type is a QueryString.
    /// </summary>
    QueryString,

    /// <summary>
    /// Type is a Json.
    /// </summary>
    Json,

    /// <summary>
    /// Type is json anonymous.
    /// </summary>
    JsonAnonymous,

    /// <summary>
    ///  Type is CommandLine.
    /// </summary>
    CommandLine,
}

/// <summary>
/// Generic class for finding SerializableAttributes.
/// </summary>
public class SerializableAttribute
{
    /// <summary>
    /// Gets what type of attribute this is.
    /// </summary>
    public SerializableAttributeType Type { get; }

    /// <summary>
    /// Gets a value indicating whether the declaring type is static.
    /// </summary>
    public bool IsStatic { get; }

    /// <summary>
    /// Gets the accessibility of the generated methods.
    /// </summary>
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Gets the declaring type that will contain this attribute.
    /// </summary>
    public ITypeSymbol DeclaringType { get; }

    /// <summary>
    /// Gets the type information referenced by this <see cref="SerializableAttribute"/>.
    /// </summary>
    public ITypeSymbol SerializedType { get; }

    /// <summary>
    /// Gets the type information referenced by TypedefAttribute.
    /// </summary>
    public ITypeSymbol? AliasedTypedefType { get; }

    /// <summary>
    /// Gets syntax information used by <see cref="SerializableAttribute"/>.
    /// </summary>
    public AttributeSyntax AttributeSyntax { get; }

    /// <summary>
    /// Gets the syntax for the class declaration associated with this attribute.
    /// </summary>
    public TypeDeclarationSyntax TypeDeclarationSyntax { get; }

    /// <summary>
    /// Gets the method name to be used.
    /// </summary>
    public string ParsingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used.
    /// </summary>
    public string SerializingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used for custom parsing.
    /// </summary>
    public string? CustomParsingMethodName { get; }

    /// <summary>
    /// Gets the method name to be used for custom serialization.
    /// </summary>
    public string? CustomSerializingMethodName { get; }

    /// <summary>
    /// Gets a value indicating whether parsing should be generated.
    /// </summary>
    public bool GenerateParse { get; }

    /// <summary>
    /// Gets a value indicating whether serialization should be generated.
    /// </summary>
    public bool GenerateSerialize { get; }

    /// <summary>
    /// Gets a value indicating whether this type has been added to generation implicitly.
    /// </summary>
    public bool IsImplicitlyGenerated { get; }

    /// <summary>
    /// Gets a value indicating whether this attribute was created through self reference.
    /// </summary>
    public bool IsSelfReference { get; }

    /// <inheritdoc />
    public override string ToString() => $"{this.SerializedType}";

    private const bool DefaultGenerateParse = true;
    private const bool DefaultGenerateSerialize = true;

    /// <summary>
    /// Initializes a new instance of the <see cref="SerializableAttribute"/> class.
    /// </summary>
    /// <param name="type">The type of the attribute.</param>
    /// <param name="isStatic">A value indicating whether the declaring type is static.</param>
    /// <param name="declaringType">The declaring type that will contain this attribute.</param>
    /// <param name="serializedType">The serialized type name of the attribute.</param>
    /// <param name="attributeSyntax">Syntax location of the attribute or null if not known.</param>
    /// <param name="typeDeclarationSyntax">Syntax for the type declaration associated with this attribute.</param>
    /// <param name="parsingMethodName">Name of parsing method.</param>
    /// <param name="serializingMethodName">Name of serializing method.</param>
    /// <param name="customParsingMethodName">Name of custom parsing method.</param>
    /// <param name="customSerializingMethodName">Name of custom serializing method.</param>
    /// <param name="generateParse">True if should generate parsing.</param>
    /// <param name="generateSerialize">True if should generate serialization.</param>
    /// <param name="isImplicitlyGenerated">A value indicating whether this type has been added to generation implicitly.</param>
    /// <param name="isSelfReference">A value indicating whether this attribute was created through self reference.</param>
    /// <param name="aliasedTypedefType">The type information referenced by TypedefAttribute.</param>
    /// <param name="visibility">Visibility of the generated methods.</param>
    public SerializableAttribute(
        SerializableAttributeType type,
        bool isStatic,
        ITypeSymbol declaringType,
        ITypeSymbol serializedType,
        AttributeSyntax attributeSyntax,
        TypeDeclarationSyntax typeDeclarationSyntax,
        string parsingMethodName,
        string serializingMethodName,
        string? customParsingMethodName,
        string? customSerializingMethodName,
        bool generateParse,
        bool generateSerialize,
        bool isImplicitlyGenerated = false,
        bool isSelfReference = false,
        ITypeSymbol? aliasedTypedefType = null,
        SerializableVisibility visibility = SerializableVisibility.Public
    )
    {
        this.Type = type;
        this.IsStatic = isStatic;
        this.DeclaringType = declaringType;
        this.SerializedType = serializedType;
        this.AliasedTypedefType = aliasedTypedefType;
        this.AttributeSyntax = attributeSyntax;
        this.TypeDeclarationSyntax = typeDeclarationSyntax;
        this.ParsingMethodName = parsingMethodName;
        this.SerializingMethodName = serializingMethodName;
        this.CustomParsingMethodName = customParsingMethodName;
        this.CustomSerializingMethodName = customSerializingMethodName;
        this.GenerateParse = generateParse;
        this.GenerateSerialize = generateSerialize;
        this.IsImplicitlyGenerated = isImplicitlyGenerated;
        this.IsSelfReference = isSelfReference;
        this.Visibility = visibility;
    }

    /// <summary>
    /// Get attributes that are usable by <see cref="SerializableAttribute"/>. This is a performance optimization to filter out entries earlier in Roslyn pipeline.
    /// </summary>
    /// <param name="typeDeclarationSyntax">Syntax for the enum.</param>
    /// <param name="semanticModel">Semantic model to get information about compilation.</param>
    /// <param name="cancellationToken">Cancellation token to early out.</param>
    /// <returns>True collection of all applicable attributes.</returns>
    public static IEnumerable<AttributeSyntax> FilterAttributes(
        TypeDeclarationSyntax typeDeclarationSyntax,
        SemanticModel semanticModel,
        CancellationToken cancellationToken
    )
    {
        foreach (var attributeList in typeDeclarationSyntax.AttributeLists)
        {
            foreach (var attribute in attributeList.Attributes)
            {
                var info = semanticModel.GetSymbolInfo(attribute, cancellationToken);
                var fullName = info.Symbol?.ContainingType.GetFullName();
                switch (fullName)
                {
                    case "global::KestrelToolbox.Serialization.EnumSerializableAttribute":
                    case "global::KestrelToolbox.Serialization.QueryStringSerializableAttribute":
                    case "global::KestrelToolbox.Serialization.CommandLineSerializableAttribute":
                    case "global::KestrelToolbox.Serialization.JsonSerializableAttribute":
                    {
                        yield return attribute;
                        break;
                    }

                    case "global::KestrelToolbox.Types.TypedefAttribute":
                    {
                        var typedefAttributePair = CacheableTypedefAttribute.GetTypedefFromAttribute(
                            attribute,
                            semanticModel,
                            cancellationToken
                        );
                        if (typedefAttributePair is not null)
                        {
                            if (typedefAttributePair.Value.features.HasFlag(TypedefFeatures.JsonSerializable))
                            {
                                yield return attribute;
                            }
                        }

                        break;
                    }

                    default:
                    {
                        // Do not return.
                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Get attributes that are usable by <see cref="SerializableAttribute"/>. This is a performance optimization to filter out entries earlier in Roslyn pipeline.
    /// </summary>
    /// <param name="context">Context for filtering.</param>
    /// <param name="methodDeclarationSyntax">Syntax for the method declaration that could be a route.</param>
    /// <param name="semanticModel">Semantic model to get information about compilation.</param>
    /// <param name="cancellationToken">Cancellation token to early out.</param>
    /// <returns>AttributeSyntax if a json body attribute was found that is not already declared.</returns>
    public static SerializableAttribute? FilterRouteAttributes(
        SerializableAttributesGeneratorContext context,
        MethodDeclarationSyntax methodDeclarationSyntax,
        SemanticModel semanticModel,
        CancellationToken cancellationToken
    )
    {
        var foundRouteAttribute = false;
        foreach (var attributeList in methodDeclarationSyntax.AttributeLists)
        {
            if (foundRouteAttribute)
            {
                continue;
            }

            foreach (var attribute in attributeList.Attributes)
            {
                var info = semanticModel.GetSymbolInfo(attribute, cancellationToken);
                var fullName = info.Symbol?.ContainingType.GetFullName();
                if (fullName != "global::KestrelToolbox.Web.RouteAttribute")
                {
                    continue;
                }

                foundRouteAttribute = true;
                break;
            }
        }

        if (!foundRouteAttribute)
        {
            return null;
        }

        var parameters = methodDeclarationSyntax.ParameterList.Parameters;
        if (parameters.Count == 0)
        {
            return null;
        }

        if (parameters[0].Type == null)
        {
            return null;
        }

        {
            var type = semanticModel.GetTypeInfo(parameters[0].Type!, cancellationToken);
            if (type.Type?.GetFullName() != "global::Microsoft.AspNetCore.Http.HttpContext")
            {
                return null;
            }
        }

        var declaringTypeSymbol = semanticModel.GetDeclaredSymbol(methodDeclarationSyntax.Parent!, cancellationToken);
        if (declaringTypeSymbol is not INamedTypeSymbol declaringType)
        {
            // Compiler should handle this.
            return null;
        }

        foreach (var parameter in parameters.Skip(1))
        {
            if (parameter.Type == null)
            {
                continue;
            }

            var parameterTypeInfo = semanticModel.GetTypeInfo(parameter.Type, cancellationToken);
            var parameterType = parameterTypeInfo.Type;
            if (parameterType == null)
            {
                continue;
            }

            foreach (var attributeList in parameter.AttributeLists)
            {
                foreach (var attributeSyntax in attributeList.Attributes)
                {
                    var info = semanticModel.GetSymbolInfo(attributeSyntax, cancellationToken)!;
                    switch (info.Symbol?.GetFullName())
                    {
                        case "JsonBodyAttribute":
                        {
                            var jsonBodyAttribute = CacheableJsonBodyAttribute.FromSyntax(
                                context,
                                parameter,
                                attributeSyntax,
                                cancellationToken
                            );
                            if (jsonBodyAttribute == null)
                            {
                                return null;
                            }

                            if (jsonBodyAttribute.ParseMethod != null)
                            {
                                return null;
                            }

                            if (methodDeclarationSyntax.Parent is not TypeDeclarationSyntax typeDeclarationSyntax)
                            {
                                return null;
                            }

                            var selfReferences = CacheableSerializableAttribute.GetSelfReferenceAttributes(
                                context.SymbolCache,
                                context.ReportDiagnostic,
                                parameterType
                            );

                            // Visibility for json body attributes is always private. Cannot be "file" because they're
                            // generated separately.
                            var visibility = SerializableVisibility.Private;
                            return new SerializableAttribute(
                                SerializableAttributeType.Json,
                                false,
                                declaringType,
                                parameterType,
                                attributeSyntax,
                                typeDeclarationSyntax,
                                GetDefaultParsingMethodName(SerializableAttributeType.Json),
                                GetDefaultSerializingMethodName(SerializableAttributeType.Json),
                                null,
                                null,
                                true,
                                false,
                                isImplicitlyGenerated: true,
                                visibility: visibility,
                                isSelfReference: selfReferences?.Find(r => r.Type == SerializableAttributeType.Json)
                                    != null
                            );
                        }

                        case "QueryParametersAttribute":
                        {
                            var queryParametersAttribute = CacheableQueryParametersAttribute.FromSyntax(
                                context,
                                parameter,
                                attributeSyntax,
                                cancellationToken
                            );
                            if (queryParametersAttribute == null)
                            {
                                return null;
                            }

                            if (queryParametersAttribute.ParseMethod != null)
                            {
                                return null;
                            }

                            if (methodDeclarationSyntax.Parent is not TypeDeclarationSyntax typeDeclarationSyntax)
                            {
                                return null;
                            }

                            var selfReferences = CacheableSerializableAttribute.GetSelfReferenceAttributes(
                                context.SymbolCache,
                                context.ReportDiagnostic,
                                parameterType
                            );

                            // Visibility for query parameters attributes is always private. Cannot be "file" because they're
                            // generated separately.
                            var visibility = SerializableVisibility.Private;
                            return new SerializableAttribute(
                                SerializableAttributeType.QueryString,
                                false,
                                declaringType,
                                parameterType,
                                attributeSyntax,
                                typeDeclarationSyntax,
                                GetDefaultParsingMethodName(SerializableAttributeType.QueryString),
                                GetDefaultSerializingMethodName(SerializableAttributeType.QueryString),
                                null,
                                null,
                                true,
                                false,
                                isImplicitlyGenerated: true,
                                visibility: visibility,
                                isSelfReference: selfReferences?.Find(r =>
                                    r.Type == SerializableAttributeType.QueryString
                                ) != null
                            );
                        }

                        default:
                        {
                            // Nothing to do.
                            break;
                        }
                    }
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Try to get a collection of full <see cref="SerializableAttribute"/> from a semantic symbol.
    /// </summary>
    /// <param name="reportDiagnostic">Report diagnostic.</param>
    /// <param name="typeDeclarationSyntax">The syntax for the type declaration associated with this attribute.</param>
    /// <param name="serializableAttribute"><see cref="SerializableAttribute"/> found from <see cref="FilterAttributes"/>.</param>
    /// <param name="semanticModel">Semantic model.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>Collection of attributes. This is empty if parsing has failed.</returns>
    public static SerializableAttribute[] GetAttributes(
        Action<Diagnostic> reportDiagnostic,
        TypeDeclarationSyntax typeDeclarationSyntax,
        AttributeSyntax serializableAttribute,
        SemanticModel semanticModel,
        CancellationToken cancellationToken
    )
    {
        var serializedTypes = new List<ITypeSymbol>();
        var customSerializingMethodName = default(string);
        var customParsingMethodName = default(string);
        var generateParse = DefaultGenerateParse;
        var generateSerialize = DefaultGenerateSerialize;

        var attributeSymbol =
            semanticModel.GetSymbolInfo(serializableAttribute, cancellationToken).Symbol?.ContainingSymbol
            ?? throw new Exception("Couldn't get symbol.");

        var declaringTypeSymbol = semanticModel.GetDeclaredSymbol(typeDeclarationSyntax, cancellationToken);
        if (declaringTypeSymbol is not INamedTypeSymbol declaringType)
        {
            // Compiler should handle this.
            return Array.Empty<SerializableAttribute>();
        }

        var isStatic = declaringType.IsStatic;

        if (attributeSymbol.GetFullName() == "global::KestrelToolbox.Types.TypedefAttribute")
        {
            var typedefPair = CacheableTypedefAttribute.GetTypedefFromAttribute(
                serializableAttribute,
                semanticModel,
                cancellationToken
            );
            if (typedefPair is null)
            {
                return Array.Empty<SerializableAttribute>();
            }

            var serializedTypeSymbolInfo = semanticModel.GetDeclaredSymbol(typeDeclarationSyntax, cancellationToken);
            if (serializedTypeSymbolInfo is not INamedTypeSymbol serializedType)
            {
                return Array.Empty<SerializableAttribute>();
            }

            return new SerializableAttribute[]
            {
                new(
                    SerializableAttributeType.Json,
                    isStatic,
                    declaringType,
                    serializedType,
                    serializableAttribute,
                    typeDeclarationSyntax,
                    GetDefaultParsingMethodName(SerializableAttributeType.Json),
                    GetDefaultSerializingMethodName(SerializableAttributeType.Json),
                    customParsingMethodName,
                    customSerializingMethodName,
                    generateParse,
                    generateSerialize,
                    isImplicitlyGenerated: false,
                    aliasedTypedefType: typedefPair.Value.alias,
                    visibility: SerializableVisibility.Public
                ),
                new(
                    SerializableAttributeType.Json,
                    isStatic,
                    declaringType,
                    typedefPair.Value.alias,
                    serializableAttribute,
                    typeDeclarationSyntax,
                    GetDefaultParsingMethodName(SerializableAttributeType.Json),
                    GetDefaultSerializingMethodName(SerializableAttributeType.Json),
                    customParsingMethodName,
                    customSerializingMethodName,
                    generateParse,
                    generateSerialize,
                    isImplicitlyGenerated: true,
                    visibility: SerializableVisibility.File
                ),
            };
        }

        var type = attributeSymbol.GetFullName() switch
        {
            "global::KestrelToolbox.Serialization.EnumSerializableAttribute" => SerializableAttributeType.Enum,
            "global::KestrelToolbox.Serialization.QueryStringSerializableAttribute" =>
                SerializableAttributeType.QueryString,
            "global::KestrelToolbox.Serialization.CommandLineSerializableAttribute" =>
                SerializableAttributeType.CommandLine,
            "global::KestrelToolbox.Serialization.JsonSerializableAttribute" => SerializableAttributeType.Json,
            _ => throw new NotImplementedException($"Unknown serializable attribute {attributeSymbol.Name}."),
        };

        var serializingMethodName = GetDefaultSerializingMethodName(type);
        var parsingMethodName = GetDefaultParsingMethodName(type);

        var arguments = serializableAttribute.ArgumentList?.Arguments;
        if (arguments != null)
        {
            foreach (var argument in arguments.Value)
            {
                var nameEquals = argument.NameEquals;
                if (nameEquals == null)
                {
                    if (argument.Expression is not TypeOfExpressionSyntax typeOfExpressionSyntax)
                    {
                        // Compiler should handle this.
                        return Array.Empty<SerializableAttribute>();
                    }

                    if (
                        semanticModel.GetSymbolInfo(typeOfExpressionSyntax.Type, cancellationToken).Symbol
                        is not ITypeSymbol serializedTypeArgument
                    )
                    {
                        // Compiler should handle this.
                        return Array.Empty<SerializableAttribute>();
                    }

                    if (!IsPublic(serializedTypeArgument))
                    {
                        reportDiagnostic(
                            DiagnosticMessages.CreatSerializableMustBePublic(
                                argument.GetLocation(),
                                serializedTypeArgument.Name,
                                argument.Expression.ToString()
                            )
                        );
                    }
                    else
                    {
                        serializedTypes.Add(serializedTypeArgument);
                    }
                }
                else
                {
                    var key = nameEquals.Name.Identifier.Text;
                    switch (key)
                    {
                        case "ParsingMethodName":
                        {
                            var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                            if (!value.HasValue || value.Value is not string stringValue)
                            {
                                // Compiler should handle this.
                                return Array.Empty<SerializableAttribute>();
                            }

                            parsingMethodName = stringValue;
                            break;
                        }

                        case "SerializingMethodName":
                        {
                            var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                            if (!value.HasValue || value.Value is not string stringValue)
                            {
                                // Compiler should handle this.
                                return Array.Empty<SerializableAttribute>();
                            }

                            serializingMethodName = stringValue;
                            break;
                        }

                        case "CustomParsingMethodName":
                        {
                            var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                            if (!value.HasValue || value.Value is not string stringValue)
                            {
                                // Compiler should handle this.
                                return Array.Empty<SerializableAttribute>();
                            }

                            customParsingMethodName = stringValue;
                            break;
                        }

                        case "CustomSerializingMethodName":
                        {
                            var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                            if (!value.HasValue || value.Value is not string stringValue)
                            {
                                // Compiler should handle this.
                                return Array.Empty<SerializableAttribute>();
                            }

                            customSerializingMethodName = stringValue;
                            break;
                        }

                        case "SourceGenerationMode":
                        {
                            var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                            if (!value.HasValue || value.Value is not int intValue)
                            {
                                // Compiler should handle this.
                                return Array.Empty<SerializableAttribute>();
                            }

                            generateSerialize = ((1 << 0) & intValue) != 0;
                            generateParse = ((1 << 1) & intValue) != 0;

                            break;
                        }

                        default:
                        {
                            // Compiler should handle this.
                            return Array.Empty<SerializableAttribute>();
                        }
                    }
                }
            }
        }

        if (serializedTypes.Count == 0)
        {
            serializedTypes.Add(declaringType);
        }

        var result = new SerializableAttribute[serializedTypes.Count];
        for (var index = 0; index < serializedTypes.Count; index++)
        {
            var serializedType = serializedTypes[index];
            result[index] = new SerializableAttribute(
                type,
                isStatic,
                declaringType,
                serializedType,
                serializableAttribute,
                typeDeclarationSyntax,
                parsingMethodName,
                serializingMethodName,
                customParsingMethodName,
                customSerializingMethodName,
                generateParse,
                generateSerialize
            );
        }

        return result;
    }

    /// <summary>
    /// Initializes a new <see cref="SerializableAttribute"/> from properties of that type.
    /// </summary>
    /// <param name="symbolCache">Cache of symbols so we don't recurse forever.</param>
    /// <param name="reportDiagnostic">Callback to report diagnostics.</param>
    /// <param name="type">The attribute type to create.</param>
    /// <param name="parentSerializableAttribute">The <see cref="SerializableAttribute"/> from the containing class or structure.</param>
    /// <param name="typeSymbol">Type that needs to exist.</param>
    /// <param name="generateParse">True if parsing should be generated.</param>
    /// <param name="generateSerialize">True if serialization should be generated.</param>
    /// <returns>New instance of <see cref="SerializableAttribute"/> class.</returns>
    public static SerializableAttribute FromProperty(
        Dictionary<string, CacheableTypeSymbol> symbolCache,
        Action<Diagnostic> reportDiagnostic,
        SerializableAttributeType type,
        SerializableAttribute parentSerializableAttribute,
        ITypeSymbol typeSymbol,
        bool generateParse,
        bool generateSerialize
    )
    {
        var selfReference = CacheableSerializableAttribute
            .GetSelfReferenceAttributes(symbolCache, reportDiagnostic, typeSymbol)
            ?.Find(r => r.Type == type);

        var parsingMethodName =
            selfReference != null
                ? GetFullyQualifiedName(typeSymbol.GetFullNameNonNullable(), selfReference.ParsingMethodName)
                : GetDefaultParsingMethodName(type);
        var serializingMethodName =
            selfReference != null
                ? GetFullyQualifiedName(typeSymbol.GetFullNameNonNullable(), selfReference.SerializingMethodName)
                : GetDefaultSerializingMethodName(type);
        var customParseMethod =
            selfReference?.CustomParsingMethodName != null
                ? GetFullyQualifiedName(typeSymbol.GetFullNameNonNullable(), selfReference.CustomParsingMethodName)
                : null;
        var customSerializeMethod =
            selfReference?.CustomSerializingMethodName != null
                ? GetFullyQualifiedName(typeSymbol.GetFullNameNonNullable(), selfReference.CustomSerializingMethodName)
                : null;

        return new SerializableAttribute(
            type,
            false,
            parentSerializableAttribute.DeclaringType,
            typeSymbol,
            parentSerializableAttribute.AttributeSyntax,
            parentSerializableAttribute.TypeDeclarationSyntax,
            parsingMethodName,
            serializingMethodName,
            customParseMethod,
            customSerializeMethod,
            generateParse,
            generateSerialize,
            isImplicitlyGenerated: true,
            visibility: SerializableVisibility.File,
            isSelfReference: selfReference != null
        );

        static string GetFullyQualifiedName(string fullName, string name)
        {
            if (name.Contains('.'))
            {
                // Has periods, probably fully qualified. If not, we're toast anyway.
                return name;
            }

            // No periods mean it's class local.
            return $"{fullName}.{name}";
        }
    }

    /// <summary>
    /// Gets the default name for parsing an entity.
    /// </summary>
    /// <param name="type">The type that will be parsed.</param>
    /// <returns>The short name for parsing a type.</returns>
    public static string GetDefaultParsingMethodName(SerializableAttributeType type) =>
        type switch
        {
            SerializableAttributeType.Enum => "TryParseEnum",
            SerializableAttributeType.Json => "TryParseJson",
            SerializableAttributeType.QueryString => "TryParseQueryString",
            SerializableAttributeType.CommandLine => "TryParseCommandLine",
            SerializableAttributeType.JsonAnonymous => throw new NotImplementedException(),
            SerializableAttributeType.Unknown => throw new NotImplementedException(),
            _ => throw new NotImplementedException(),
        };

    /// <summary>
    /// Gets the default name for serializing an entity.
    /// </summary>
    /// <param name="type">The type that will be parsed.</param>
    /// <returns>The short name for serializing a type.</returns>
    public static string GetDefaultSerializingMethodName(SerializableAttributeType type) =>
        type switch
        {
            SerializableAttributeType.Enum => "SerializeEnum",
            SerializableAttributeType.Json => "SerializeJson",
            SerializableAttributeType.QueryString => "SerializeQueryString",
            SerializableAttributeType.CommandLine => "SerializeCommandLine",
            SerializableAttributeType.JsonAnonymous => throw new NotImplementedException(),
            SerializableAttributeType.Unknown => throw new NotImplementedException(),
            _ => throw new NotImplementedException(),
        };

    /// <summary>
    /// Gets the location of this attribute.
    /// </summary>
    /// <returns>The location in source code.</returns>
    public Location GetLocation() => this.AttributeSyntax.GetLocation();

    private static bool IsPublic(ISymbol symbol)
    {
        if (symbol.DeclaredAccessibility == Accessibility.NotApplicable)
        {
            // NotApplicable is for assembly and other. If we hit there, we're public.
            return true;
        }

        if (symbol.DeclaredAccessibility != Accessibility.Public)
        {
            return false;
        }

        if (symbol.ContainingSymbol != null)
        {
            return IsPublic(symbol.ContainingSymbol);
        }

        return true;
    }
}
