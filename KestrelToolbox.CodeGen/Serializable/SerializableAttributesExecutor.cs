// <copyright file="SerializableAttributesExecutor.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

// #define PRINT_BUILD_COUNT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Executor for <see cref="SerializableAttributesGenerator"/>.
/// </summary>
public sealed partial class SerializableAttributesExecutor : IEquatable<SerializableAttributesExecutor?>
{
#if PRINT_BUILD_COUNT
    private static readonly Dictionary<string, int> BuildCounts = new();
#endif // PRINT_BUILD_COUNT

    private readonly List<Diagnostic> diagnostics;
    private readonly CacheableTypeDeclarationSymbol typeDeclarationSymbol;
    private readonly List<ISerializableGenerator> types;
    private readonly string fileLocalMethodClassName;

    /// <summary>
    /// Initializes a new instance of the <see cref="SerializableAttributesExecutor"/> class.
    /// </summary>
    /// <param name="typeDeclarationSymbol">Overall class symbol for this <see cref="SerializableAttributesExecutor"/>.</param>
    /// <param name="types">Types to generate methods for.</param>
    /// <param name="diagnostics">Diagnostics for this <paramref name="typeDeclarationSymbol"/> type.</param>
    public SerializableAttributesExecutor(
        CacheableTypeDeclarationSymbol typeDeclarationSymbol,
        List<ISerializableGenerator> types,
        List<Diagnostic> diagnostics
    )
    {
        this.typeDeclarationSymbol = typeDeclarationSymbol;
        this.types = types;
        this.diagnostics = diagnostics;
        var normalizedFullName = Regex.Replace(typeDeclarationSymbol.FullName, @"[<>:\.]", "_");
        this.fileLocalMethodClassName = $"__KestrelToolbox_Serializable__File_{normalizedFullName}";
        foreach (var type in this.types)
        {
            type.ParentExecutor = this;
        }
    }

    /// <summary>
    /// Generate code for this class.
    /// </summary>
    /// <param name="context">Context of source generation.</param>
    public void Execute(SourceProductionContext context)
    {
        var cancellationToken = context.CancellationToken;
        if (this.diagnostics.Count != 0)
        {
            foreach (var diagnostic in this.diagnostics)
            {
                context.ReportDiagnostic(diagnostic);
            }
        }

        if (this.types.Count == 0)
        {
            // We need to make it here to generate diagnostics. Don't generate any files if
            // we're empty.
            return;
        }

        var typeSymbol = this.typeDeclarationSymbol;

        var builder = new CodeBuilder();

#if PRINT_BUILD_COUNT
        lock (BuildCounts)
        {
            _ = BuildCounts.TryGetValue(classSymbol.FullName, out var buildCount);
            BuildCounts[classSymbol.FullName] = buildCount + 1;
            builder.AppendLine($"// Serializable build count: {buildCount}");
        }
#endif // PRINT_BUILD_COUNT

        if (typeSymbol.Namespace.Length != 0)
        {
            builder.AppendLine($"namespace {typeSymbol.Namespace};");
            builder.AppendLine();
        }

        builder.AppendLine("#nullable enable");

        // Build up class list so we can have the correct type.
        foreach (var containingType in typeSymbol.ContainingTypes)
        {
            builder.AppendLine(containingType);
            builder.PushBlock();
        }

        var fileLocalBuilder = new CodeBuilder();
        fileLocalBuilder.AppendLine("""[global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]""");
        fileLocalBuilder.AppendLine($"file static class {this.fileLocalMethodClassName}");
        fileLocalBuilder.PushBlock();
        var fileLocalBuilderHasElements = false;

        foreach (var type in this.types)
        {
            cancellationToken.ThrowIfCancellationRequested();
            builder.ResetVariableNames();
            try
            {
                var subBuilder = new CodeBuilder();
                if (type.Generate(context.ReportDiagnostic, subBuilder))
                {
                    switch (type.Visibility)
                    {
                        case SerializableVisibility.File:
                        {
                            if (!subBuilder.Empty)
                            {
                                fileLocalBuilderHasElements = true;
                                fileLocalBuilder.Append(subBuilder);
                            }

                            break;
                        }

                        case SerializableVisibility.Public:
                        case SerializableVisibility.Private:
                        {
                            builder.Append(subBuilder);
                            break;
                        }

                        default:
                        {
                            throw new NotImplementedException("Unknown visibility.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Nothing to do, reporting should send true error to the user.
                context.ReportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            }
        }

        fileLocalBuilder.PopBlock();

        for (var i = 0; i < typeSymbol.ContainingTypes.Length; i++)
        {
            builder.PopBlock();
        }

        if (fileLocalBuilderHasElements)
        {
            builder.Append(fileLocalBuilder);
        }

        var contents = builder.ToString();
        var sourcePath = $"SerializableAttribute/{typeSymbol.FullName}.g.cs";
        sourcePath = sourcePath.Replace("global::", string.Empty).Replace('<', '(').Replace('>', ')');
        context.AddSource(sourcePath, contents);
    }

    /// <inheritdoc />
    public bool Equals(SerializableAttributesExecutor? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.diagnostics.SequenceEqual(other.diagnostics)
            && this.typeDeclarationSymbol.Equals(other.typeDeclarationSymbol)
            && this.types.SequenceEqual(other.types);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is SerializableAttributesExecutor other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.typeDeclarationSymbol, this.types.AggregateHashCode());
    }

    /// <summary>
    /// Compares two <see cref="SerializableAttributesExecutor"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(SerializableAttributesExecutor? left, SerializableAttributesExecutor? right) =>
        Equals(left, right);

    /// <summary>
    /// Compares two <see cref="SerializableAttributesExecutor"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(SerializableAttributesExecutor? left, SerializableAttributesExecutor? right) =>
        !Equals(left, right);
}
