// <copyright file="CommandLineSerializableCustomParseMethod.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Representation of a method with <c>CommandLineSerializableCustomParseAttribute</c> attribute on it.
/// </summary>
public sealed class CommandLineSerializableCustomParseMethod : IEquatable<CommandLineSerializableCustomParseMethod>
{
    private const string AttributeName =
        "global::KestrelToolbox.Serialization.DataAnnotations.CommandLineSerializableCustomParseAttribute";

    /// <summary>
    /// Gets the fully qualified method name for this attribute.
    /// </summary>
    public string FullMethodName { get; }

    /// <summary>
    /// Gets the fully qualified type name of the output type.
    /// </summary>
    public string ResultTypeFullName { get; }

    private CommandLineSerializableCustomParseMethod(string fullMethodName, string resultTypeFullName)
    {
        this.FullMethodName = fullMethodName;
        this.ResultTypeFullName = resultTypeFullName;
    }

    /// <summary>
    /// Tries to get a <see cref="CommandLineSerializableCustomParseMethod"/> from a type.
    /// </summary>
    /// <param name="symbol">Symbol to search.</param>
    /// <param name="customParseAttribute">Resulting attribute if it exists.</param>
    /// <returns>True if found, false if not.</returns>
    public static bool TryGetMethod(
        ITypeSymbol symbol,
        [NotNullWhen(true)] out CommandLineSerializableCustomParseMethod? customParseAttribute
    )
    {
        foreach (var member in symbol.GetMembers())
        {
            if (member is not IMethodSymbol methodSymbol)
            {
                continue;
            }

            if (!Extensions.AttributeExists(methodSymbol, AttributeName))
            {
                continue;
            }

            if (methodSymbol.Parameters.Length != 3)
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var valueParameter = methodSymbol.Parameters[0];
            var resultParameter = methodSymbol.Parameters[1];
            var errorsParameter = methodSymbol.Parameters[2];
            if (
                resultParameter.RefKind != RefKind.Out
                || errorsParameter.Type.GetFullName() != "string?"
                || errorsParameter.RefKind != RefKind.Out
            )
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            if (!symbol.Equals(resultParameter.Type, SymbolEqualityComparer.Default))
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var valueFullName = valueParameter.Type.GetFullName();
            if (valueFullName != "string")
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            customParseAttribute = new CommandLineSerializableCustomParseMethod(
                methodSymbol.GetFullMethodName(),
                resultParameter.Type.GetFullName()
            );
            return true;
        }

        customParseAttribute = default;
        return false;
    }

    /// <inheritdoc />
    public bool Equals(CommandLineSerializableCustomParseMethod? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.FullMethodName == other.FullMethodName && this.ResultTypeFullName == other.ResultTypeFullName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not CommandLineSerializableCustomParseMethod other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.FullMethodName, this.ResultTypeFullName);
    }

    /// <summary>
    /// Compares two <see cref="CommandLineSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(
        CommandLineSerializableCustomParseMethod? left,
        CommandLineSerializableCustomParseMethod? right
    )
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="CommandLineSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(
        CommandLineSerializableCustomParseMethod? left,
        CommandLineSerializableCustomParseMethod? right
    )
    {
        return !Equals(left, right);
    }
}
