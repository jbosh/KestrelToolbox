// <copyright file="SerializableVisibility.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Visibility level of a generator.
/// </summary>
public enum SerializableVisibility
{
    /// <summary>
    /// Generator is public.
    /// </summary>
    Public,

    /// <summary>
    /// Generator is private to the class/struct.
    /// </summary>
    Private,

    /// <summary>
    /// Generator is file local.
    /// </summary>
    File,
}

/// <summary>
/// Extensions for <see cref="SerializableVisibility"/>.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.DocumentationRules",
    "SA1649:File name should match first type name",
    Justification = "Extensions."
)]
public static class SerializableVisibilityExtensions
{
    /// <summary>
    /// Returns the max visibility of two visibilities.
    /// </summary>
    /// <param name="a">Left hand side.</param>
    /// <param name="b">Right hand side.</param>
    /// <returns>The max visibility.</returns>
    public static SerializableVisibility Max(this SerializableVisibility a, SerializableVisibility b)
    {
        // Use min because they're sorted ascending.
        return (SerializableVisibility)Math.Min((int)a, (int)b);
    }
}
