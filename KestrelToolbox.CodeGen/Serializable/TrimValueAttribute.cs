// <copyright file="TrimValueAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Attribute to represent TrimValueAttribute.
/// </summary>
public sealed class TrimValueAttribute : IEquatable<TrimValueAttribute>
{
    /// <summary>
    /// Gets trimming characters. Or null if default.
    /// </summary>
    /// <remarks>
    /// This value is in the form of <c>new[] {'a', 'b'}</c>.
    /// </remarks>
    public string? TrimChars { get; }

    /// <summary>
    /// Gets a value indicating whether to trim on serialization.
    /// </summary>
    public bool OnSerialize { get; }

    /// <summary>
    /// Gets a value indicating whether to trim on parsing.
    /// </summary>
    public bool OnParse { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="TrimValueAttribute"/> class.
    /// </summary>
    /// <param name="trimChars">Gets trimming characters. Or null if default.</param>
    /// <param name="onSerialize">A value indicating whether to trim on serialization.</param>
    /// <param name="onParse">A value indicating whether to trim on parsing.</param>
    public TrimValueAttribute(string? trimChars, bool onSerialize, bool onParse)
    {
        this.TrimChars = trimChars;
        this.OnSerialize = onSerialize;
        this.OnParse = onParse;
    }

    /// <summary>
    /// Generates a new instance of <see cref="TrimValueAttribute"/> from a property.
    /// </summary>
    /// <param name="attribute">Attribute data on the property.</param>
    /// <returns>The new instance of <see cref="TrimValueAttribute"/>.</returns>
    public static TrimValueAttribute FromAttribute(AttributeData attribute)
    {
        var trimChars = default(string);
        var onSerialize = true;
        var onParse = true;
        foreach (var argument in attribute.NamedArguments)
        {
            switch (argument.Key)
            {
                case "OnSerialize":
                {
                    var value = argument.Value;
                    if (value.Value is bool onSerializeValue)
                    {
                        onSerialize = onSerializeValue;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                    break;
                }

                case "OnParse":
                {
                    var value = argument.Value;
                    if (value.Value is bool onParseValue)
                    {
                        onParse = onParseValue;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                    break;
                }

                default:
                {
                    throw new NotImplementedException();
                }
            }
        }

        if (attribute.ConstructorArguments.Length == 1)
        {
            trimChars = $"new[] {attribute.ConstructorArguments[0].ToCSharpString()}";
        }

        return new TrimValueAttribute(trimChars, onSerialize, onParse);
    }

    /// <inheritdoc />
    public bool Equals(TrimValueAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.TrimChars == other.TrimChars
            && this.OnSerialize == other.OnSerialize
            && this.OnParse == other.OnParse;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is TrimValueAttribute other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.TrimChars, this.OnSerialize, this.OnParse);
    }

    /// <summary>
    /// Compares two <see cref="TrimValueAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(TrimValueAttribute? left, TrimValueAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="TrimValueAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(TrimValueAttribute? left, TrimValueAttribute? right)
    {
        return !Equals(left, right);
    }
}
