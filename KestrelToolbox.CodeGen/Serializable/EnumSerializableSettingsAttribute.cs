// <copyright file="EnumSerializableSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// An instance of EnumSerializableSettingsAttribute that can be used on syntax or symbols.
/// </summary>
public sealed class EnumSerializableSettingsAttribute : IEquatable<EnumSerializableSettingsAttribute>
{
    private const string AttributeName =
        "global::KestrelToolbox.Serialization.DataAnnotations.EnumSerializableSettingsAttribute";

    /// <summary>
    /// Gets a value indicating whether the enum uses numbers for serialization.
    /// </summary>
    public bool SerializeAsNumbers { get; }

    /// <summary>
    /// Gets a value indicating whether the enum is allowed to be invalid numbers.
    /// </summary>
    public bool AllowAnyNumber { get; }

    /// <summary>
    /// Gets the invalid value if parsing failure should set to something.
    /// </summary>
    public object? InvalidValue { get; }

    private const bool DefaultSerializeAsNumbers = false;
    private const bool DefaultAllowAnyNumber = false;

    /// <summary>
    /// Initializes a new instance of the <see cref="EnumSerializableSettingsAttribute"/> class.
    /// </summary>
    public EnumSerializableSettingsAttribute()
    {
        this.SerializeAsNumbers = DefaultSerializeAsNumbers;
        this.AllowAnyNumber = DefaultAllowAnyNumber;
    }

    private EnumSerializableSettingsAttribute(bool serializeAsNumbers, bool allowAnyNumber, object? invalidValue)
    {
        this.SerializeAsNumbers = serializeAsNumbers;
        this.AllowAnyNumber = allowAnyNumber;
        this.InvalidValue = invalidValue;
    }

    /// <summary>
    /// Try to get enum serialization attribute proper from a symbol.
    /// </summary>
    /// <param name="symbol">Enum symbol.</param>
    /// <param name="result">Resulting <see cref="EnumSerializableSettingsAttribute"/> if it's found.</param>
    /// <returns>True if EnumSerializableSettingsAttribute was found.</returns>
    public static bool TryGet(ITypeSymbol symbol, [NotNullWhen(true)] out EnumSerializableSettingsAttribute? result)
    {
        var serializeAsNumbers = DefaultSerializeAsNumbers;
        var allowAnyNumber = DefaultAllowAnyNumber;
        var invalidValue = default(object);

        result = default;
        foreach (var attribute in symbol.GetAttributes())
        {
            if (attribute.AttributeClass?.GetFullName() != AttributeName)
            {
                continue;
            }

            foreach (var argument in attribute.NamedArguments)
            {
                var key = argument.Key;
                switch (key)
                {
                    case "SerializationType":
                    {
                        var value = argument.Value;
                        if (
                            value.Type?.GetFullName()
                                != "global::KestrelToolbox.Serialization.DataAnnotations.EnumSerializableSettingsSerializationType"
                            || value.Value is not int serializationTypeValue
                        )
                        {
                            return false;
                        }

                        serializeAsNumbers = serializationTypeValue == 1;
                        break;
                    }

                    case "AllowAnyNumber":
                    {
                        var value = argument.Value;
                        if (value.Value is not bool useNumbersValue)
                        {
                            return false;
                        }

                        allowAnyNumber = useNumbersValue;
                        break;
                    }

                    case "InvalidValue":
                    {
                        if (argument.Value.Value == null)
                        {
                            return false;
                        }

                        invalidValue = argument.Value.Value;
                        break;
                    }

                    default:
                    {
                        return false;
                    }
                }
            }

            result = new EnumSerializableSettingsAttribute(serializeAsNumbers, allowAnyNumber, invalidValue);
            return true;
        }

        return false;
    }

    /// <inheritdoc />
    public bool Equals(EnumSerializableSettingsAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.SerializeAsNumbers == other.SerializeAsNumbers
            && this.AllowAnyNumber == other.AllowAnyNumber
            && Equals(this.InvalidValue, other.InvalidValue);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not EnumSerializableSettingsAttribute other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.SerializeAsNumbers, this.AllowAnyNumber, this.InvalidValue);
    }

    /// <summary>
    /// Compares two <see cref="EnumSerializableSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(EnumSerializableSettingsAttribute? left, EnumSerializableSettingsAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="EnumSerializableSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(EnumSerializableSettingsAttribute? left, EnumSerializableSettingsAttribute? right)
    {
        return !Equals(left, right);
    }
}
