// <copyright file="QueryStringSerializableCustomParseMethod.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Representation of a method with <c>QueryStringSerializableCustomParse</c> attribute on it.
/// </summary>
public sealed class QueryStringSerializableCustomParseMethod : IEquatable<QueryStringSerializableCustomParseMethod>
{
    private const string AttributeName =
        "global::KestrelToolbox.Serialization.DataAnnotations.QueryStringSerializableCustomParseAttribute";

    /// <summary>
    /// Gets the fully qualified method name for this attribute.
    /// </summary>
    public string FullMethodName { get; }

    /// <summary>
    /// Gets the fully qualified type name of the output type.
    /// </summary>
    public string ResultTypeFullName { get; }

    /// <summary>
    /// Gets a value indicating whether this custom parse method operates on arrays or individual string elements.
    /// </summary>
    public bool IsIndividualElement { get; }

    private QueryStringSerializableCustomParseMethod(
        string fullMethodName,
        string resultTypeFullName,
        bool isIndividualElement
    )
    {
        this.FullMethodName = fullMethodName;
        this.ResultTypeFullName = resultTypeFullName;
        this.IsIndividualElement = isIndividualElement;
    }

    /// <summary>
    /// Tries to get a <see cref="QueryStringSerializableCustomParseMethod"/> from a type.
    /// </summary>
    /// <param name="symbol">Symbol to search.</param>
    /// <param name="customParseAttribute">Resulting attribute if it exists.</param>
    /// <returns>True if found, false if not.</returns>
    public static bool TryGetMethod(
        ITypeSymbol symbol,
        [NotNullWhen(true)] out QueryStringSerializableCustomParseMethod? customParseAttribute
    )
    {
        foreach (var member in symbol.GetMembers())
        {
            if (member is not IMethodSymbol methodSymbol)
            {
                continue;
            }

            if (!Extensions.AttributeExists(methodSymbol, AttributeName))
            {
                continue;
            }

            if (methodSymbol.Parameters.Length != 3)
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var valueParameter = methodSymbol.Parameters[0];
            var resultParameter = methodSymbol.Parameters[1];
            var errorsParameter = methodSymbol.Parameters[2];
            if (
                resultParameter.RefKind != RefKind.Out
                || errorsParameter.Type.GetFullName() != "string?"
                || errorsParameter.RefKind != RefKind.Out
            )
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            if (!symbol.Equals(resultParameter.Type, SymbolEqualityComparer.Default))
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var valueFullName = valueParameter.Type.GetFullName();
            if (valueFullName is not "string" and not "global::Microsoft.Extensions.Primitives.StringValues")
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var isIndividualElement = valueParameter.Type.GetFullName() switch
            {
                "string" => true,
                "global::Microsoft.Extensions.Primitives.StringValues" => false,
                _ => throw new NotImplementedException(),
            };
            customParseAttribute = new QueryStringSerializableCustomParseMethod(
                methodSymbol.GetFullMethodName(),
                resultParameter.Type.GetFullName(),
                isIndividualElement
            );
            return true;
        }

        customParseAttribute = default;
        return false;
    }

    /// <inheritdoc />
    public bool Equals(QueryStringSerializableCustomParseMethod? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.FullMethodName == other.FullMethodName
            && this.ResultTypeFullName == other.ResultTypeFullName
            && this.IsIndividualElement == other.IsIndividualElement;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not QueryStringSerializableCustomParseMethod other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.FullMethodName, this.ResultTypeFullName, this.IsIndividualElement);
    }

    /// <summary>
    /// Compares two <see cref="QueryStringSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(
        QueryStringSerializableCustomParseMethod? left,
        QueryStringSerializableCustomParseMethod? right
    )
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="QueryStringSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(
        QueryStringSerializableCustomParseMethod? left,
        QueryStringSerializableCustomParseMethod? right
    )
    {
        return !Equals(left, right);
    }
}
