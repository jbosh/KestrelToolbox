// <copyright file="JsonSerializeAnonymousAttributeGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for JsonSerializableAnonymousAttribute.
/// </summary>
public sealed class JsonSerializeAnonymousAttributeGenerator
    : ISerializableGenerator,
        IEquatable<JsonSerializeAnonymousAttributeGenerator>
{
    /// <inheritdoc />
    public Dictionary<MethodReferenceKey, MethodReference> MethodReferences { get; set; } = new();

    /// <inheritdoc />
    public SerializableAttributesExecutor ParentExecutor { get; set; } = null!;

    /// <summary>
    /// Gets the method name to generate for serializing anonymous types. Defaults to SerializeJsonAnonymous.
    /// </summary>
    public string SerializingMethodName { get; }

    /// <summary>
    /// Gets the field name for storing the instance of JsonAnonymousSerializer.
    /// </summary>
    public string FieldName { get; }

    /// <summary>
    /// Gets the location of this attribute.
    /// </summary>
    /// <returns>The location in source code.</returns>
    public Location? GetLocation() => this.location?.GetLocation();

    private readonly CacheableLocation? location;
    private List<ISerializableGenerator>? allTypes;

    /// <summary>
    /// Initializes a new instance of the <see cref="JsonSerializeAnonymousAttributeGenerator"/> class.
    /// </summary>
    /// <param name="serializingMethodName">The method name to generate for serializing anonymous types. Defaults to SerializeJsonAnonymous.</param>
    /// <param name="fieldName">The field name for storing the instance of JsonAnonymousSerializer.</param>
    /// <param name="location">The location of this attribute.</param>
    public JsonSerializeAnonymousAttributeGenerator(
        string serializingMethodName,
        string fieldName,
        CacheableLocation? location
    )
    {
        this.SerializingMethodName = serializingMethodName;
        this.FieldName = fieldName;
        this.location = location;
    }

    /// <inheritdoc />
    public SerializableVisibility Visibility => SerializableVisibility.Public;

    /// <summary>
    /// Get attributes that are usable by <see cref="JsonSerializeAnonymousAttributeGenerator"/>. This is a performance optimization to filter out entries earlier in Roslyn pipeline.
    /// </summary>
    /// <param name="typeDeclarationSyntax">Syntax for the enum.</param>
    /// <param name="semanticModel">Semantic model to get information about compilation.</param>
    /// <param name="cancellationToken">Cancellation token to early out.</param>
    /// <returns>True collection of all applicable attributes.</returns>
    public static IEnumerable<AttributeSyntax> FilterAttributes(
        TypeDeclarationSyntax typeDeclarationSyntax,
        SemanticModel semanticModel,
        CancellationToken cancellationToken
    )
    {
        foreach (var attributeList in typeDeclarationSyntax.AttributeLists)
        {
            foreach (var attribute in attributeList.Attributes)
            {
                var info = semanticModel.GetSymbolInfo(attribute, cancellationToken);
                var fullName = info.Symbol?.ContainingType.GetFullName();
                switch (fullName)
                {
                    case "global::KestrelToolbox.Serialization.JsonSerializeAnonymousAttribute":
                    {
                        yield return attribute;
                        break;
                    }

                    default:
                    {
                        // Do not return.
                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Try to get a collection of full <see cref="JsonSerializeAnonymousAttributeGenerator"/> from a semantic symbol.
    /// </summary>
    /// <param name="serializableAttribute"><see cref="JsonSerializeAnonymousAttributeGenerator"/> found from <see cref="FilterAttributes"/>.</param>
    /// <param name="semanticModel">Semantic model.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>Collection of attributes. This is empty if parsing has failed.</returns>
    public static JsonSerializeAnonymousAttributeGenerator? GetAttributes(
        AttributeSyntax serializableAttribute,
        SemanticModel semanticModel,
        CancellationToken cancellationToken
    )
    {
        var serializingMethodName = "SerializeJsonAnonymous";
        var fieldName = "JsonAnonymousSerializer";

        var arguments = serializableAttribute.ArgumentList?.Arguments;
        if (arguments != null)
        {
            foreach (var argument in arguments.Value)
            {
                var nameEquals = argument.NameEquals;
                if (nameEquals == null)
                {
                    continue;
                }

                var key = nameEquals.Name.Identifier.Text;
                switch (key)
                {
                    case "SerializingMethodName":
                    {
                        var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                        if (!value.HasValue || value.Value is not string stringValue)
                        {
                            // Compiler should handle this.
                            return null;
                        }

                        serializingMethodName = stringValue;
                        break;
                    }

                    case "FieldName":
                    {
                        var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
                        if (!value.HasValue || value.Value is not string stringValue)
                        {
                            // Compiler should handle this.
                            return null;
                        }

                        fieldName = stringValue;
                        break;
                    }

                    default:
                    {
                        // Compiler should handle this.
                        return null;
                    }
                }
            }
        }

        var location = CacheableLocation.FromLocation(serializableAttribute.GetLocation());
        return new JsonSerializeAnonymousAttributeGenerator(serializingMethodName, fieldName, location);
    }

    /// <inheritdoc />
    public bool Generate(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        try
        {
            builder.AppendLine(
                $"private static readonly global::KestrelToolbox.Serialization.Helpers.JsonAnonymousSerializer {this.FieldName} = new global::System.Func<global::KestrelToolbox.Serialization.Helpers.JsonAnonymousSerializer>(() =>"
            );
            builder.PushBlock();

            builder.AppendLine(
                "var serializer = new global::KestrelToolbox.Serialization.Helpers.JsonAnonymousSerializer();"
            );
            Debug.Assert(
                this.allTypes != null,
                $"Should call {nameof(this.PrepareToGenerate)} before {nameof(this.Generate)}."
            );
            foreach (var type in this.allTypes!)
            {
                if (type is not JsonSerializableGenerator generator)
                {
                    continue;
                }

                generator.AddToAnonymousSerializer(builder);
            }

            builder.AppendLine("return serializer;");
            builder.PopBlock(")();");

            builder.AppendAndSplitLines(
                $$"""
                /// <summary>
                /// Asynchronously serialize an object of type <typeparamref name="T"/> to a stream.
                /// </summary>
                /// <param name="stream">The stream to write to.</param>
                /// <param name="value">The object to serialize.</param>
                /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                /// <param name="cancellationToken">Cancellation token.</param>
                /// <typeparam name="T">The type to serialize.</typeparam>
                /// <returns>void.</returns>
                public static Task {{this.SerializingMethodName}}Async<T>(global::System.IO.Stream stream, T value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default) =>
                    {{this.FieldName}}.SerializeAsync(stream, value, options);


                /// <summary>
                /// Asynchronously serialize an object of type <typeparamref name="T"/> to a stream.
                /// </summary>
                /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                /// <param name="value">The object to serialize.</param>
                /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                /// <param name="cancellationToken">Cancellation token.</param>
                /// <typeparam name="T">The type to serialize.</typeparam>
                /// <returns>void.</returns>
                public static Task {{this.SerializingMethodName}}Async<T>(global::System.Buffers.IBufferWriter<byte> bufferWriter, T value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default) =>
                    {{this.FieldName}}.SerializeAsync(bufferWriter, value, options);

                /// <summary>
                /// Serialize an object of type <typeparamref name="T"/> to a string.
                /// </summary>
                /// <param name="value">The object to serialize.</param>
                /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                /// <typeparam name="T">The type to serialize.</typeparam>
                /// <returns>Json encoded string value.</returns>
                public static string {{this.SerializingMethodName}}<T>(T value, global::System.Text.Json.JsonWriterOptions options = default) => {{this.FieldName}}.Serialize(value, options);

                /// <summary>
                /// Serialize an object of type <typeparamref name="T"/> to a stream.
                /// </summary>
                /// <param name="stream">The stream to write to.</param>
                /// <param name="value">The object to serialize.</param>
                /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                /// <typeparam name="T">The type to serialize.</typeparam>
                public static void {{this.SerializingMethodName}}<T>(global::System.IO.Stream stream, T value, global::System.Text.Json.JsonWriterOptions options = default) => {{this.FieldName}}.Serialize(stream, value, options);

                /// <summary>
                /// Serialize an object of type <typeparamref name="T"/> to a stream.
                /// </summary>
                /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                /// <param name="value">The object to serialize.</param>
                /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                /// <typeparam name="T">The type to serialize.</typeparam>
                public static void {{this.SerializingMethodName}}<T>(global::System.Buffers.IBufferWriter<byte> bufferWriter, T value, global::System.Text.Json.JsonWriterOptions options = default) => {{this.FieldName}}.Serialize(bufferWriter, value, options);

                /// <summary>
                /// Serialize an object of type <typeparamref name="T"/> to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                /// </summary>
                /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                /// <param name="value">The object to serialize.</param>
                /// <typeparam name="T">The type to serialize.</typeparam>
                public static void {{this.SerializingMethodName}}<T>(global::System.Text.Json.Utf8JsonWriter writer, T value) => {{this.FieldName}}.Serialize(writer, value);
                """
            );
            return true;
        }
        catch (Exception ex)
        {
            reportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            return false;
        }
    }

    /// <inheritdoc />
    public ISerializableGenerator Combine(Action<Diagnostic> reportDiagnostic, ISerializableGenerator otherGenerator)
    {
        throw new NotSupportedException("Cannot combine anonymous json. They are all the same.");
    }

    /// <inheritdoc />
    public void PrepareToGenerate(Dictionary<MethodReferenceKey, ISerializableGenerator> generators)
    {
        this.allTypes = generators.Values.ToList();
    }

    /// <inheritdoc />
    public bool Equals(JsonSerializeAnonymousAttributeGenerator? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return Equals(this.location, other.location)
            && this.SerializingMethodName == other.SerializingMethodName
            && this.FieldName == other.FieldName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not JsonSerializeAnonymousAttributeGenerator other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.location, this.SerializingMethodName, this.FieldName);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializeAnonymousAttributeGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(
        JsonSerializeAnonymousAttributeGenerator? left,
        JsonSerializeAnonymousAttributeGenerator? right
    )
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializeAnonymousAttributeGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(
        JsonSerializeAnonymousAttributeGenerator? left,
        JsonSerializeAnonymousAttributeGenerator? right
    )
    {
        return !Equals(left, right);
    }
}
