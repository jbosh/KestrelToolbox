// <copyright file="PropertyMember.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using KestrelToolbox.CodeGen.Cacheables;
using KestrelToolbox.CodeGen.PropertyAttributes;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Class to represent properties in a serializable object.
/// </summary>
public sealed class PropertyMember : IEquatable<PropertyMember>
{
    private const string NameAttribute = "global::KestrelToolbox.Serialization.DataAnnotations.NameAttribute";

    /// <summary>
    /// Gets the names of this property.
    /// </summary>
    public List<string> Names { get; }

    /// <summary>
    /// Gets the property name in code.
    /// </summary>
    public string? PropertyName { get; }

    /// <summary>
    /// Gets the type of this property.
    /// </summary>
    public CacheableTypeSymbol Type { get; }

    /// <summary>
    /// Gets a value indicating whether this value needs to be set using init.
    /// </summary>
    public bool IsInitOnly { get; }

    /// <summary>
    /// Gets a value indicating whether any value is converted to a string for this string property.
    /// </summary>
    public bool AllowAnyString { get; }

    /// <summary>
    /// Gets the value of EmptyArgumentValueAttribute.
    /// </summary>
    public EmptyArgumentValueAttribute? EmptyArgumentValue { get; }

    /// <summary>
    /// Gets a value indicating whether <c>Required: AllowOnlyWhiteSpaceAttribute</c> is on the property.
    /// </summary>
    /// <returns>True if white space is allowed, false if not.</returns>
    public bool AllowOnlyWhiteSpace { get; }

    /// <summary>
    /// Gets the length that the array is this property represents is allowed to be.
    /// </summary>
    public ValidRangeAttribute? ArrayLength { get; }

    /// <summary>
    /// Gets any bool serializable settings attributes.
    /// </summary>
    /// <see cref="BoolSerializableSettings"/>
    public BoolSerializableSettingsAttribute BoolSerializableSettings { get; }

    /// <summary>
    /// Gets the Custom property attribute.
    /// </summary>
    public JsonSerializableCustomPropertyAttribute? CustomPropertyAttribute { get; }

    /// <summary>
    /// Gets the default value of a property (if it exists).
    /// </summary>
    public string? DefaultValue { get; }

    /// <summary>
    /// Gets the description of the property (if it exists).
    /// </summary>
    public string? Description { get; }

    /// <summary>
    /// Gets the date parse only settings attribute.
    /// </summary>
    public DateOnlyParseSettingsAttribute? DateOnlyParseSettings { get; }

    /// <summary>
    /// Gets a value indicating whether the email address attribute is applied.
    /// </summary>
    public bool EmailAddress { get; }

    /// <summary>
    /// Gets a value indicating whether the null on empty attribute is applied.
    /// </summary>
    public bool NullOnEmpty { get; }

    /// <summary>
    /// Gets the value of PositionalArgumentAttribute.
    /// </summary>
    public PositionalArgumentAttribute? PositionalArgument { get; }

    /// <summary>
    /// Gets a value indicating whether this property is required to be initialized.
    /// </summary>
    public bool IsRequired { get; }

    /// <summary>
    /// Gets the range of allowable values.
    /// </summary>
    /// <returns>Range values as strings. Non-original values are converted to be more C# friendly (like decimal adding M suffix).</returns>
    public ValidRangeAttribute? Range { get; }

    /// <summary>
    /// Gets the conditions under which serialization is skipped.
    /// </summary>
    public SkipSerializationAttribute? SkipSerialization { get; }

    /// <summary>
    /// Gets the length this string is allowed to be.
    /// </summary>
    public ValidRangeAttribute? StringLength { get; }

    /// <summary>
    /// Gets the settings for trimming a string value.
    /// </summary>
    public TrimValueAttribute? TrimValue { get; }

    /// <summary>
    /// Gets the valid values that are allowed for a property.
    /// </summary>
    public ValidValuesAttribute? ValidValues { get; }

    /// <summary>
    /// Gets <see cref="Location"/> of this property.
    /// </summary>
    /// <returns>The <see cref="Location"/> or <c>null</c> if it is not in source.</returns>
    public Location? GetLocation() => this.location?.GetLocation();

    private readonly CacheableLocation? location;

    private PropertyMember(
        List<string> names,
        string? propertyName,
        CacheableTypeSymbol type,
        bool isInitOnly,
        bool allowAnyString,
        EmptyArgumentValueAttribute? emptyArgumentValue,
        bool allowOnlyWhiteSpace,
        ValidRangeAttribute? arrayLength,
        BoolSerializableSettingsAttribute boolSerializableSettings,
        JsonSerializableCustomPropertyAttribute? customPropertyAttribute,
        DateOnlyParseSettingsAttribute? dateOnlyParseSettings,
        string? defaultValue,
        string? description,
        bool emailAddress,
        bool isRequired,
        bool nullOnEmpty,
        PositionalArgumentAttribute? positionalArgument,
        ValidRangeAttribute? range,
        SkipSerializationAttribute? skipSerialization,
        ValidRangeAttribute? stringLength,
        TrimValueAttribute? trimValue,
        ValidValuesAttribute? validValues,
        CacheableLocation? location
    )
    {
        this.Names = names;
        this.PropertyName = propertyName;
        this.Type = type;
        this.IsInitOnly = isInitOnly;
        this.AllowAnyString = allowAnyString;
        this.EmptyArgumentValue = emptyArgumentValue;
        this.AllowOnlyWhiteSpace = allowOnlyWhiteSpace;
        this.ArrayLength = arrayLength;
        this.BoolSerializableSettings = boolSerializableSettings;
        this.CustomPropertyAttribute = customPropertyAttribute;
        this.DateOnlyParseSettings = dateOnlyParseSettings;
        this.DefaultValue = defaultValue;
        this.Description = description;
        this.EmailAddress = emailAddress;
        this.IsRequired = isRequired;
        this.NullOnEmpty = nullOnEmpty;
        this.PositionalArgument = positionalArgument;
        this.Range = range;
        this.SkipSerialization = skipSerialization;
        this.StringLength = stringLength;
        this.TrimValue = trimValue;
        this.ValidValues = validValues;
        this.location = location;
    }

    /// <summary>
    /// Gets a new instance of <see cref="PropertyMember"/> from property and names.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="names">Names of the property.</param>
    /// <param name="property">Symbol representing the property.</param>
    /// <returns>A new instance of the <see cref="PropertyMember"/> class.</returns>
    public static PropertyMember FromSymbol(
        SerializableAttributesGeneratorContext context,
        List<string> names,
        IPropertySymbol property
    )
    {
        var allowAnyString = false;
        var emptyArgumentValue = default(EmptyArgumentValueAttribute);
        var allowOnlyWhiteSpace = true;
        var arrayLength = default(ValidRangeAttribute);
        var boolSerializableSettingsAttribute = default(BoolSerializableSettingsAttribute);
        var customPropertyAttribute = default(JsonSerializableCustomPropertyAttribute);
        var dateOnlyParseSettingsAttribute = default(DateOnlyParseSettingsAttribute);
        var defaultValue = default(string);
        var description = default(string);
        var emailAddress = false;
        var nullOnEmpty = false;
        var positionalArgument = default(PositionalArgumentAttribute);
        var isRequired = property.IsRequired;
        var range = default(ValidRangeAttribute);
        var stringLength = default(ValidRangeAttribute);
        var skipSerializationAttribute = default(SkipSerializationAttribute);
        var trimValue = default(TrimValueAttribute);
        var validValues = default(ValidValuesAttribute);
        var type = CacheableTypeSymbol.FromITypeSymbol(context.SymbolCache, context.ReportDiagnostic, property.Type);
        var location = CacheableLocation.FromLocation(property.DeclaringSyntaxReferences.GetFirstLocationOrDefault());
        var isInitOnly = property.SetMethod?.IsInitOnly ?? false;

        foreach (var attribute in property.GetAttributes())
        {
            var fullName = attribute.AttributeClass?.GetFullName();
            switch (fullName)
            {
                case "global::KestrelToolbox.Serialization.DataAnnotations.AllowAnyStringAttribute":
                {
                    allowAnyString = true;
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.EmptyArgumentValueAttribute":
                {
                    emptyArgumentValue = EmptyArgumentValueAttribute.FromAttribute(context, attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.ArrayLengthAttribute":
                {
                    arrayLength = GetRangeAttributeFromLengthAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.BoolSerializableSettingsAttribute":
                {
                    boolSerializableSettingsAttribute = BoolSerializableSettingsAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.JsonSerializableCustomPropertyAttribute":
                {
                    customPropertyAttribute = JsonSerializableCustomPropertyAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.DateOnlyParseSettingsAttribute":
                {
                    dateOnlyParseSettingsAttribute = DateOnlyParseSettingsAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute":
                {
                    defaultValue = GetDefaultValue(property.Type, attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.DescriptionAttribute":
                {
                    description = attribute.ConstructorArguments[0].ToCSharpString();
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.EmailAddressAttribute":
                {
                    emailAddress = true;
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.NullOnEmptyAttribute":
                {
                    nullOnEmpty = true;
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.PositionalArgumentAttribute":
                {
                    positionalArgument = PositionalArgumentAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.ValidRangeAttribute":
                {
                    range = ValidRangeAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.RequiredAttribute":
                {
                    isRequired = true;

                    foreach (var argument in attribute.NamedArguments)
                    {
                        switch (argument.Key)
                        {
                            case "AllowOnlyWhiteSpace":
                            {
                                if (argument.Value.Value is not bool boolValue)
                                {
                                    throw new NotImplementedException("errors");
                                }

                                allowOnlyWhiteSpace = boolValue;
                                break;
                            }

                            default:
                            {
                                throw new NotImplementedException("Error handling.");
                            }
                        }
                    }

                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.SkipSerializationAttribute":
                {
                    skipSerializationAttribute = SkipSerializationAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.StringLengthAttribute":
                {
                    stringLength = GetRangeAttributeFromLengthAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.TrimValueAttribute":
                {
                    trimValue = TrimValueAttribute.FromAttribute(attribute);
                    break;
                }

                case "global::KestrelToolbox.Serialization.DataAnnotations.ValidValuesAttribute":
                {
                    validValues = ValidValuesAttribute.FromAttribute(attribute);
                    break;
                }

                default:
                {
                    // Unknown property.
                    break;
                }
            }
        }

        var skipSerializationOnDefault =
            skipSerializationAttribute?.Condition.HasFlag(SkipSerializationCondition.WhenDefault) ?? false;

        if (defaultValue == null && ((!isRequired && isInitOnly) || skipSerializationOnDefault))
        {
            // For init only elements that are not required, try to get the real deal init value. Other values will
            // be set live at parse time or use the actual value that was constructed.
            foreach (var declaringSyntaxReference in property.DeclaringSyntaxReferences)
            {
                if (declaringSyntaxReference.GetSyntax() is not PropertyDeclarationSyntax propertyDeclarationSyntax)
                {
                    continue;
                }

                var initializerValue = propertyDeclarationSyntax.Initializer?.Value;
                if (initializerValue == null)
                {
                    // We have a valid property declaration syntax. Set to default so generator knows it can set it raw.
                    defaultValue ??= "default";
                    continue;
                }

                var semanticModel = context.SemanticModel;
                if (propertyDeclarationSyntax.SyntaxTree != semanticModel.SyntaxTree)
                {
                    // Property was in a different syntax tree from the one we're compiling. This should probably
                    // be a performance warning.
                    try
                    {
                        semanticModel = semanticModel.Compilation.GetSemanticModel(
                            propertyDeclarationSyntax.SyntaxTree
                        );
                    }
                    catch (ArgumentException)
                    {
                        // Semantic model is not part of compilation. This can happen during live builds for the
                        // IDE. Luckily, knowing that a property has a default value isn't a huge deal for
                        // autocomplete.
                        continue;
                    }
                }

                defaultValue = CodeBuilder.Generate(semanticModel, initializerValue);
            }
        }

        return new PropertyMember(
            names,
            property.Name,
            type,
            isInitOnly,
            allowAnyString,
            emptyArgumentValue,
            allowOnlyWhiteSpace,
            arrayLength,
            boolSerializableSettingsAttribute ?? new BoolSerializableSettingsAttribute(),
            customPropertyAttribute,
            dateOnlyParseSettingsAttribute,
            defaultValue,
            description,
            emailAddress,
            isRequired,
            nullOnEmpty,
            positionalArgument,
            range,
            skipSerializationAttribute,
            stringLength,
            trimValue,
            validValues,
            location
        );
    }

    /// <summary>
    /// Gets a new instance of <see cref="PropertyMember"/> class from <see cref="CacheableTypeSymbol"/>.
    /// </summary>
    /// <param name="type">Type of property to instantiate.</param>
    /// <returns>The newly instantiated <see cref="PropertyMember"/>.</returns>
    /// <remarks>
    /// This <see cref="PropertyMember"/> will use all defaults for as if there were no attributes on the property and no names.
    /// </remarks>
    public static PropertyMember FromType(CacheableTypeSymbol type)
    {
        return new PropertyMember(
            new List<string>(),
            null,
            type,
            false,
            false,
            null,
            true,
            null,
            new BoolSerializableSettingsAttribute(),
            null,
            null,
            null,
            null,
            false,
            false,
            false,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );
    }

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as an array.
    /// </summary>
    /// <param name="type">Type to see if it is an array.</param>
    /// <returns>True if the property is parsed as an array, false if not.</returns>
    public static bool IsParsedAsArray(CacheableTypeSymbol type)
    {
        if (type.ArrayElementType != null)
        {
            return type.ArrayElementType.FullName != "byte";
        }

        if (type.ConstructedFromFullName == "global::System.Collections.Generic.List<T>")
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as an array.
    /// </summary>
    /// <param name="type">Type to see if it is an array.</param>
    /// <returns>True if the property is parsed as an array, false if not.</returns>
    public static bool IsParsedAsArray(ITypeSymbol type)
    {
        if (type is IArrayTypeSymbol arrayTypeSymbol)
        {
            return arrayTypeSymbol.ElementType.GetFullName() != "byte";
        }

        if (
            type is INamedTypeSymbol namedTypeSymbol
            && namedTypeSymbol.ConstructedFrom.GetFullName() == "global::System.Collections.Generic.List<T>"
        )
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <param name="type">Type to see if it is a set.</param>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public static bool IsParsedAsSet(CacheableTypeSymbol type)
    {
        if (type.ConstructedFromFullName == "global::System.Collections.Generic.HashSet<T>")
        {
            return true;
        }

        if (IsParsedAsFrozenSet(type))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <param name="type">Type to see if it is a set.</param>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public static bool IsParsedAsSet(ITypeSymbol type)
    {
        if (
            type is INamedTypeSymbol namedTypeSymbol
            && namedTypeSymbol.ConstructedFrom.GetFullName() == "global::System.Collections.Generic.HashSet<T>"
        )
        {
            return true;
        }

        if (IsParsedAsFrozenSet(type))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <param name="type">Type to see if it is a set.</param>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public static bool IsParsedAsFrozenSet(CacheableTypeSymbol type) =>
        type.ConstructedFromFullName == "global::System.Collections.Frozen.FrozenSet<T>";

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <param name="type">Type to see if it is a set.</param>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public static bool IsParsedAsFrozenSet(ITypeSymbol type) =>
        type is INamedTypeSymbol namedTypeSymbol
        && namedTypeSymbol.ConstructedFrom.GetFullName() == "global::System.Collections.Frozen.FrozenSet<T>";

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <param name="type">Type to see if it is a set.</param>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public static bool IsParsedAsFrozenDictionary(CacheableTypeSymbol type) =>
        type.ConstructedFromFullName == "global::System.Collections.Frozen.FrozenDictionary<TKey, TValue>";

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <param name="type">Type to see if it is a set.</param>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public static bool IsParsedAsFrozenDictionary(ITypeSymbol type) =>
        type is INamedTypeSymbol namedTypeSymbol
        && namedTypeSymbol.ConstructedFrom.GetFullName()
            == "global::System.Collections.Frozen.FrozenDictionary<TKey, TValue>";

    /// <summary>
    /// Gets a value indicating whether <paramref name="type"/> is a IEnumerable&lt;T&gt;.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns>True if <paramref name="type"/> is IEnumerable&amp;lt;T&amp;gt;.</returns>
    public static bool IsIEnumerable(CacheableTypeSymbol type)
    {
        return type.FindInterface("global::System.Collections.Generic.IEnumerable<T>") != null;
    }

    /// <summary>
    /// Gets a value indicating whether <paramref name="type"/> is a KeyValuePair&lt;string, _&gt; for use in becoming properties.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns>True if <paramref name="type"/> is usable as properties.</returns>
    public static bool IsPropertyTuple(CacheableTypeSymbol type)
    {
        return type.ConstructedFromFullName == "global::System.Collections.Generic.KeyValuePair<TKey, TValue>"
            && type.TypeArguments[0].FullName == "string";
    }

    /// <summary>
    /// Gets a value indicating whether <paramref name="type"/> is a KeyValuePair&lt;string, _&gt; for use in becoming properties.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns>True if <paramref name="type"/> is usable as properties.</returns>
    public static bool IsPropertyTuple(ITypeSymbol type)
    {
        if (type is INamedTypeSymbol namedTypeSymbol)
        {
            return namedTypeSymbol.ConstructedFrom.GetFullName()
                    == "global::System.Collections.Generic.KeyValuePair<TKey, TValue>"
                && namedTypeSymbol.TypeArguments[0].GetFullName() == "string";
        }

        return false;
    }

    /// <summary>
    /// Gets all members of a symbol.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="symbol">Symbol to get properties of.</param>
    /// <returns>Collection of properties with the symbols they use.</returns>
    public static IEnumerable<(IPropertySymbol symbol, PropertyMember member)> GetMembers(
        SerializableAttributesGeneratorContext context,
        ITypeSymbol symbol
    ) => GetMembers(context, symbol.GetMembers());

    /// <summary>
    /// Converts ISymbol collection to PropertyMember.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="members">Members to convert.</param>
    /// <returns>Collection of properties with the symbols they use.</returns>
    public static IEnumerable<(IPropertySymbol symbol, PropertyMember member)> GetMembers(
        SerializableAttributesGeneratorContext context,
        ImmutableArray<ISymbol> members
    )
    {
        foreach (var member in members)
        {
            if (member is not IPropertySymbol propertySymbol)
            {
                continue;
            }

            var names = new List<string>();
            foreach (var attribute in member.GetAttributes())
            {
                if (!attribute.AttributeClass?.GetFullName().Equals(NameAttribute, StringComparison.Ordinal) ?? true)
                {
                    continue;
                }

                if (attribute.NamedArguments.Length != 0)
                {
                    throw new NotImplementedException(
                        $"Named arguments are not implemented on NameAttribute on {member.GetFullMetadataName()}"
                    );
                }

                foreach (var constructorArgument in attribute.ConstructorArguments)
                {
                    if (constructorArgument.Kind == TypedConstantKind.Primitive)
                    {
                        names.Add(constructorArgument.Value!.ToString()!);
                    }
                    else
                    {
                        foreach (var value in constructorArgument.Values)
                        {
                            names.Add(value.Value!.ToString()!);
                        }
                    }
                }
            }

            yield return (propertySymbol, FromSymbol(context, names, propertySymbol));
        }
    }

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as an array.
    /// </summary>
    /// <returns>True if the property is parsed as an array, false if not.</returns>
    public bool IsParsedAsArray() => IsParsedAsArray(this.Type);

    /// <summary>
    /// Gets a value indicating whether the symbol is parsed as a set.
    /// </summary>
    /// <returns>True if the property is parsed as a set, false if not.</returns>
    public bool IsParsedAsSet() => IsParsedAsSet(this.Type);

    /// <inheritdoc />
    public bool Equals(PropertyMember? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Names.SequenceEqual(other.Names)
            && this.PropertyName == other.PropertyName
            && this.Type.Equals(other.Type)
            && this.AllowAnyString == other.AllowAnyString
            && Equals(this.EmptyArgumentValue, other.EmptyArgumentValue)
            && this.AllowOnlyWhiteSpace == other.AllowOnlyWhiteSpace
            && Equals(this.ArrayLength, other.ArrayLength)
            && this.BoolSerializableSettings.Equals(other.BoolSerializableSettings)
            && this.DefaultValue == other.DefaultValue
            && this.Description == other.Description
            && Equals(this.DateOnlyParseSettings, other.DateOnlyParseSettings)
            && this.EmailAddress == other.EmailAddress
            && this.NullOnEmpty == other.NullOnEmpty
            && Equals(this.PositionalArgument, other.PositionalArgument)
            && this.IsRequired == other.IsRequired
            && Equals(this.Range, other.Range)
            && Equals(this.SkipSerialization, other.SkipSerialization)
            && Equals(this.StringLength, other.StringLength)
            && Equals(this.TrimValue, other.TrimValue)
            && Equals(this.ValidValues, other.ValidValues)
            && Equals(this.location, other.location);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is PropertyMember other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Names.AggregateHashCode(), this.Type, this.location);
    }

    /// <summary>
    /// Compares two <see cref="PropertyMember"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(PropertyMember? left, PropertyMember? right) => Equals(left, right);

    /// <summary>
    /// Compares two <see cref="PropertyMember"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(PropertyMember? left, PropertyMember? right) => !Equals(left, right);

    private static string GetDefaultValue(ITypeSymbol typeSymbol, AttributeData attribute)
    {
        switch (attribute.ConstructorArguments.Length)
        {
            case 1:
            {
                var value = attribute.ConstructorArguments[0];
                return typeSymbol.GetFullName() switch
                {
                    "float" => $"{value.ToCSharpString()}F",
                    _ => value.ToCSharpString(),
                };
            }

            case 2:
            {
                var type = attribute.ConstructorArguments[0];
                var value = attribute.ConstructorArguments[1];
                if (value.Value is not string stringValue)
                {
                    throw new NotImplementedException("Error handling.");
                }

                var typeName = ((ISymbol)type.Value!).GetFullName();
                switch (typeName)
                {
                    case "global::System.DateTime":
                    {
                        var parsedValue = DateTime.Parse((string)value.Value, CultureInfo.InvariantCulture);
                        return $"new global::System.DateTime({parsedValue.Ticks}, global::System.DateTimeKind.{parsedValue.Kind})";
                    }

                    case "global::System.DateTimeOffset":
                    {
                        var parsedValue = DateTimeOffset.Parse((string)value.Value, CultureInfo.InvariantCulture);
                        return $"new global::System.DateTimeOffset({parsedValue.Ticks}, global::System.TimeSpan.FromTicks({parsedValue.Offset.Ticks}))";
                    }

                    case "global::System.DateOnly":
                    case "global::System.TimeOnly":
                    {
                        return $"{typeName}.Parse({value.ToCSharpString()}, System.Globalization.CultureInfo.InvariantCulture)";
                    }

                    case "global::System.TimeSpan":
                    {
                        var convertedValue = (TimeSpan)
                            System
                                .ComponentModel.TypeDescriptor.GetConverter(typeof(TimeSpan))
                                .ConvertFromInvariantString(stringValue)!;
                        return $"new TimeSpan({convertedValue.Ticks})";
                    }

                    case "decimal":
                    {
                        var result = stringValue;
                        if (!result.EndsWith("M", StringComparison.InvariantCultureIgnoreCase))
                        {
                            result += "M";
                        }

                        return result;
                    }

                    default:
                    {
                        if (type.Value is INamedTypeSymbol namedTypeSymbol)
                        {
                            if (namedTypeSymbol.TypeKind == TypeKind.Enum)
                            {
                                return $"{typeName}.{value.Value}";
                            }
                        }

                        throw new NotImplementedException("Unknown default value type.");
                    }
                }
            }

            default:
            {
                throw new NotImplementedException("Not implemented 2 constructor arguments for DefaultValue.");
            }
        }
    }

    private static ValidRangeAttribute GetRangeAttributeFromLengthAttribute(AttributeData attribute)
    {
        string min;
        string max;
        switch (attribute.ConstructorArguments.Length)
        {
            case 1:
            {
                min = "0";
                max = attribute.ConstructorArguments[0].ToCSharpString();
                break;
            }

            case 2:
            {
                min = attribute.ConstructorArguments[0].ToCSharpString();
                max = attribute.ConstructorArguments[1].ToCSharpString();
                break;
            }

            default:
            {
                min = "0";
                max = "int.MaxValue";
                break;
            }
        }

        foreach (var argument in attribute.NamedArguments)
        {
            switch (argument.Key)
            {
                case "Minimum":
                {
                    min = argument.Value.ToCSharpString();
                    break;
                }

                case "Maximum":
                {
                    min = argument.Value.ToCSharpString();
                    break;
                }

                default:
                {
                    // Compiler error.
                    break;
                }
            }
        }

        if (min.StartsWith('"') || max.StartsWith('"'))
        {
            // Compiler error earlier on. Don't worry about it.
            min = "0";
            max = "int.MaxValue";
        }

        return new ValidRangeAttribute(min, max, min, max);
    }
}
