// <copyright file="QueryStringSerializableGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for QueryStringSerializable.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.OrderingRules",
    "SA1202:Elements should be ordered by access",
    Justification = "These classes are complicated."
)]
[SuppressMessage(
    "StyleCop.CSharp.OrderingRules",
    "SA1204:Static elements should appear before instance elements",
    Justification = "Static is interchangeable."
)]
public sealed class QueryStringSerializableGenerator
    : ISerializableGenerator,
        IEquatable<QueryStringSerializableGenerator>
{
    /// <inheritdoc />
    public Dictionary<MethodReferenceKey, MethodReference> MethodReferences { get; set; }

    /// <inheritdoc />
    public SerializableAttributesExecutor ParentExecutor { get; set; } = null!;

    private readonly CacheableSerializableAttribute serializableAttribute;
    private readonly CacheableTypeSymbol serializedType;
    private readonly PropertyMember[] members;

    private QueryStringSerializableGenerator(
        CacheableSerializableAttribute serializableAttribute,
        CacheableTypeSymbol serializedType,
        PropertyMember[] members,
        Dictionary<MethodReferenceKey, MethodReference> methodReferences
    )
    {
        this.serializedType = serializedType;
        this.serializableAttribute = serializableAttribute;
        this.members = members;
        this.MethodReferences = methodReferences;
        this.Visibility = serializableAttribute.Visibility;
    }

    /// <inheritdoc />
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Prepares a new instance of <see cref="QueryStringSerializableGenerator"/> using semantic model or null if there was an error.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="serializableAttribute"><see cref="Serializable.SerializableAttribute"/> that initiated parsing this query string.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The <see cref="QueryStringSerializableGenerator"/> or null if there was an error.</returns>
    public static QueryStringSerializableGenerator? Prepare(
        SerializableAttributesGeneratorContext context,
        SerializableAttribute serializableAttribute,
        CancellationToken cancellationToken
    )
    {
        var serializedType = serializableAttribute.SerializedType;
        SerializableAttributesExecutor.GetNullableType(serializedType, out var baseType, out _);
        if (baseType.TypeKind is not TypeKind.Class and not TypeKind.Struct)
        {
            context.ReportDiagnostic(
                DiagnosticMessages.CreateQueryStringSerializableTypeInvalid(
                    serializableAttribute.AttributeSyntax.GetLocation(),
                    serializableAttribute.SerializedType.GetFullName(),
                    baseType.TypeKind
                )
            );
            return null;
        }

        var members = PropertyMember.GetMembers(context, baseType).ToList();
        cancellationToken.ThrowIfCancellationRequested();

        var methodReferences = new Dictionary<MethodReferenceKey, MethodReference>();
        var duplicateNames = new HashSet<string>();
        foreach (var member in members)
        {
            if (member.member.Names.Count > 1)
            {
                context.ReportDiagnostic(
                    DiagnosticMessages.CreateQueryStringSerializablePropertyHasMultipleNames(
                        serializableAttribute.AttributeSyntax.GetLocation(),
                        serializableAttribute.SerializedType.GetFullName(),
                        member.symbol.Name
                    )
                );
            }

            foreach (var name in member.member.Names)
            {
                if (!duplicateNames.Add(name))
                {
                    context.ReportDiagnostic(
                        DiagnosticMessages.CreateQueryStringSerializableDuplicateName(
                            serializableAttribute.GetLocation(),
                            baseType.GetFullName(),
                            name
                        )
                    );

                    return null;
                }
            }

            var memberType = member.member.Type;

            if (SerializableAttributesExecutor.IsBuiltInType(memberType))
            {
                continue;
            }

            if (memberType.QueryStringSerializableCustomParseMethod != null)
            {
                continue;
            }

            MethodReferenceKey key;
#pragma warning disable IDE0010
            switch (memberType.NonNullableType.TypeKind)
#pragma warning restore IDE0010
            {
                case TypeKind.Enum:
                {
                    key = new MethodReferenceKey(
                        SerializableAttributeType.Enum,
                        memberType.NonNullableType.MethodKeyName
                    );
                    break;
                }

                case TypeKind.Class:
                case TypeKind.Struct:
                {
                    key = new MethodReferenceKey(
                        SerializableAttributeType.QueryString,
                        memberType.NonNullableType.MethodKeyName
                    );
                    break;
                }

                default:
                {
                    // Unknown type. This should not happen and is an unsupported type.
                    continue;
                }
            }

            if (
                !context.TryGetSerializedType(key, out var memberSerializableAttribute)
                || !memberSerializableAttribute.GenerateParse
            )
            {
                memberSerializableAttribute = Serializable.SerializableAttribute.FromProperty(
                    context.SymbolCache,
                    context.ReportDiagnostic,
                    key.Type,
                    serializableAttribute,
                    member.symbol.Type,
                    true,
                    false
                );
                if (
                    !context.TryAddSerializedType(
                        key,
                        memberSerializableAttribute,
                        serializableAttribute.SerializedType
                    )
                )
                {
                    return null;
                }
            }

            if (!methodReferences.ContainsKey(key))
            {
                var parsingMethodName = memberSerializableAttribute.ParsingMethodName;
                var parsingType = memberType.NonNullableType.FullName;
                var serializingMethodName = memberSerializableAttribute.SerializingMethodName;
                methodReferences.Add(
                    key,
                    new MethodReference(
                        parsingType,
                        parsingMethodName,
                        serializingMethodName,
                        memberSerializableAttribute.Visibility,
                        memberSerializableAttribute.IsImplicitlyGenerated
                    )
                );
            }
        }

        var cacheableSerializableAttribute = CacheableSerializableAttribute.FromSerializableAttribute(
            context,
            serializableAttribute,
            cancellationToken
        );
        var cacheableSerializedType = CacheableTypeSymbol.FromITypeSymbol(
            context.SymbolCache,
            context.ReportDiagnostic,
            serializedType
        );
        return new QueryStringSerializableGenerator(
            cacheableSerializableAttribute,
            cacheableSerializedType,
            members.Select(p => p.member).ToArray(),
            methodReferences
        );
    }

    /// <inheritdoc />
    public ISerializableGenerator Combine(Action<Diagnostic> reportDiagnostic, ISerializableGenerator otherGenerator)
    {
        if (otherGenerator is not QueryStringSerializableGenerator other)
        {
            throw new NotImplementedException($"Should never combine mismatched {nameof(ISerializableGenerator)}.");
        }

        throw new NotImplementedException(
            $"{this.serializedType.FullName} cannot combine with {other.serializedType.FullName}"
        );
    }

    /// <inheritdoc />
    public void PrepareToGenerate(Dictionary<MethodReferenceKey, ISerializableGenerator> generators)
    {
        // No prep at this time.
    }

    /// <inheritdoc />
    public bool Generate(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        if (this.serializableAttribute.IsSelfReference)
        {
            // Self reference attributes do not generate.
            return true;
        }

        try
        {
            if (this.serializableAttribute.GenerateParse)
            {
                var returnType =
                    $"{this.serializedType.FullName}{(this.serializedType.IsValueType ? string.Empty : "?")}";

                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="queryCollection">Query string collection.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"public static bool {this.serializableAttribute.ParsingMethodName}(global::Microsoft.AspNetCore.Http.IQueryCollection queryCollection, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out {returnType} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)"
                );
                using (builder.AppendBlock())
                {
                    this.GenerateQueryStringParseMethodBody(reportDiagnostic, builder);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            reportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            return false;
        }
    }

    private void GenerateQueryStringParseMethodBody(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        var serializedType = this.serializableAttribute.SerializedType;
        var baseType = serializedType.NonNullableType;

        var memberVariableNames = new string?[this.members.Length];
        for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
        {
            var member = this.members[memberIndex];
            if (member.Names.Count == 0)
            {
                memberVariableNames[memberIndex] = null;
                continue;
            }

            var name = member.Names[0];
            var memberVariableName = builder.GetVariableName("member");
            memberVariableNames[memberIndex] = memberVariableName;

            var propertyType = member.Type;
            var inputValuesName = builder.GetVariableName("inputValues");
            builder.AppendLine($"{propertyType.FullName} {memberVariableName};");

            builder.AppendLine($"if (queryCollection.TryGetValue(\"{name}\", out var {inputValuesName}))");
            using (builder.AppendBlock())
            {
                if (propertyType.QueryStringSerializableCustomParseMethod is { IsIndividualElement: false })
                {
                    var customParseMethod = propertyType.QueryStringSerializableCustomParseMethod;
                    var outputValueName = builder.GetVariableName("output");
                    builder.AppendLine(
                        $"if (!{customParseMethod.FullMethodName}({inputValuesName}, out {customParseMethod.ResultTypeFullName} {outputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(builder);
                    }

                    builder.AppendLine($"{memberVariableName} = {outputValueName};");
                }
                else if (member.IsParsedAsArray() || member.IsParsedAsSet())
                {
                    bool isArray;
                    CacheableTypeSymbol elementType;
                    if (propertyType.ArrayElementType != null)
                    {
                        isArray = true;
                        elementType = propertyType.ArrayElementType;
                    }
                    else
                    {
                        isArray = false;
                        elementType = propertyType.TypeArguments[0];
                    }

                    var minLength = default(string);
                    var maxLength = default(string);

                    var arrayLength = member.ArrayLength;
                    if (arrayLength != null)
                    {
                        minLength = arrayLength.Min;
                        maxLength = arrayLength.Max;
                    }

                    if (minLength != null)
                    {
                        builder.AppendLine($"if ({inputValuesName}.Count < {minLength})");
                        using (builder.AppendBlock())
                        {
                            if (long.TryParse(minLength, out var minLengthInt))
                            {
                                SerializableAttributesExecutor.ReturnError(
                                    builder,
                                    name,
                                    $"Must be longer than {minLengthInt - 1} element{(minLengthInt - 1 == 1 ? string.Empty : "s")}."
                                );
                            }
                            else
                            {
                                SerializableAttributesExecutor.ReturnError(
                                    builder,
                                    name,
                                    $"Must be longer than at least {minLength} elements."
                                );
                            }
                        }
                    }

                    if (maxLength != null)
                    {
                        builder.AppendLine($"if ({inputValuesName}.Count > {maxLength})");
                        using (builder.AppendBlock())
                        {
                            SerializableAttributesExecutor.ReturnError(
                                builder,
                                name,
                                $"Must be shorter than {maxLength} elements."
                            );
                        }
                    }

                    var arrayValueName = builder.GetVariableName("value");
                    var indexName = builder.GetVariableName("index");

                    string constructor;
                    if (isArray)
                    {
                        if (elementType.ArrayElementType != null)
                        {
                            constructor = $"new {elementType.ArrayElementType.FullName}[{inputValuesName}.Count][]";
                        }
                        else
                        {
                            constructor = $"new {elementType.FullName}[{inputValuesName}.Count]";
                        }
                    }
                    else
                    {
                        constructor =
                            $"new global::System.Collections.Generic.List<{elementType.FullName}>({inputValuesName}.Count)";
                    }

                    builder.AppendLine($"var {arrayValueName} = {constructor};");

                    builder.AppendLine(
                        $"for (var {indexName} = 0; {indexName} < {inputValuesName}.Count; {indexName}++)"
                    );
                    using (builder.AppendBlock())
                    {
                        var iteratedValueName = builder.GetVariableName("iter");
                        builder.AppendLine(
                            $"var {iteratedValueName} = (string?){inputValuesName}[{indexName}] ?? string.Empty;"
                        );

                        var elementValueName = this.ParseIndividualQueryStringElement(
                            reportDiagnostic,
                            builder,
                            iteratedValueName,
                            name,
                            member,
                            elementType
                        );
                        if (isArray)
                        {
                            builder.AppendLine($"{arrayValueName}[{indexName}] = {elementValueName};");
                        }
                        else
                        {
                            builder.AppendLine($"{arrayValueName}.Add({elementValueName});");
                        }
                    }

                    if (member.IsParsedAsSet())
                    {
                        if (PropertyMember.IsParsedAsFrozenSet(member.Type))
                        {
                            builder.AppendLine(
                                $"{memberVariableName} = global::System.Collections.Frozen.FrozenSet.ToFrozenSet({arrayValueName});"
                            );
                        }
                        else
                        {
                            builder.AppendLine(
                                $"{memberVariableName} = global::System.Linq.Enumerable.ToHashSet({arrayValueName});"
                            );
                        }
                    }
                    else
                    {
                        builder.AppendLine($"{memberVariableName} = {arrayValueName};");
                    }
                }
                else
                {
                    var stringName = GetSingleStringValue(builder, inputValuesName, name);
                    var foundValueName = this.ParseIndividualQueryStringElement(
                        reportDiagnostic,
                        builder,
                        stringName,
                        name,
                        member,
                        propertyType
                    );
                    builder.AppendLine($"{memberVariableName} = {foundValueName};");
                }
            }

            var defaultValue = member.DefaultValue;
            using (builder.AppendBlock("else"))
            {
                if (member.IsRequired)
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        $"Required query parameter '{name}' was missing."
                    );
                }
                else if (defaultValue != null)
                {
                    builder.AppendLine($"{memberVariableName} = {defaultValue};");
                }
                else
                {
                    builder.AppendLine($"{memberVariableName} = default;");
                }
            }
        }

        if (memberVariableNames.Length == 0 || Array.TrueForAll(memberVariableNames, n => n == null))
        {
            reportDiagnostic(
                DiagnosticMessages.CreateQueryStringSerializableDidNotHaveAnyMembers(
                    this.serializableAttribute.GetLocation(),
                    serializedType.FullName
                )
            );
        }

        bool hasDefaultConstructor;
        if (serializedType.IsNamedType)
        {
            hasDefaultConstructor = serializedType.HasDefaultConstructor;
        }
        else
        {
            throw new NotImplementedException($"Unknown type {serializedType}.");
        }

        if (hasDefaultConstructor)
        {
            builder.AppendLine($"result = new {baseType.FullNameNonNullable}()");
            builder.PushBlock();
            for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
            {
                var member = this.members[memberIndex];
                var memberVariableName = memberVariableNames[memberIndex];
                if (memberVariableName != null)
                {
                    if (member.PropertyName == null)
                    {
                        throw new Exception("PropertyName on a member cannot be null.");
                    }

                    builder.AppendLine($"{member.PropertyName} = {memberVariableName},");
                }
            }

            builder.PopBlock(";");
        }
        else
        {
            var outputName = builder.GetVariableName("output");
            builder.AppendLine(
                $"var {outputName} = ({baseType.FullNameNonNullable})global::System.Runtime.CompilerServices.RuntimeHelpers.GetUninitializedObject(typeof({baseType.FullNameNonNullable}));"
            );
            for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
            {
                var member = this.members[memberIndex];
                var memberVariableName = memberVariableNames[memberIndex];
                if (memberVariableName != null)
                {
                    if (member.PropertyName == null)
                    {
                        throw new Exception("PropertyName on a member cannot be null.");
                    }

                    builder.AppendLine($"{outputName}.{member.PropertyName} = {memberVariableName};");
                }
            }

            builder.AppendLine($"result = {outputName};");
        }

        builder.AppendLine("error = default;");
        builder.AppendLine("return true;");
    }

    private string ParseIndividualQueryStringElement(
        Action<Diagnostic> reportDiagnostic,
        CodeBuilder builder,
        string inputValueName,
        string name,
        PropertyMember property,
        CacheableTypeSymbol elementType
    )
    {
        var baseElementType = elementType.NonNullableType;

        string outputValueName;
        switch (baseElementType.FullName)
        {
            case "byte[]":
            case "byte[]?":
            {
                // System.Byte[] is special because it uses base64
                outputValueName = builder.GetVariableName("output");
                builder.AppendLine(
                    $"if (!global::KestrelToolbox.Serialization.Helpers.QueryStringParser.TryParseBase64String({inputValueName}, out var {outputValueName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(builder, name, "Was not a url encoded base64 string.");
                }

                break;
            }

            case "bool":
            {
                outputValueName = builder.GetVariableName("output");
                builder.AppendLine(
                    $"if (!global::KestrelToolbox.Serialization.Helpers.QueryStringParser.TryParseBool({inputValueName}, out var {outputValueName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                break;
            }

            case "byte":
            case "byte?":
            case "decimal":
            case "decimal?":
            case "double":
            case "double?":
            case "float":
            case "float?":
            case "int":
            case "int?":
            case "long":
            case "long?":
            case "sbyte":
            case "sbyte?":
            case "short":
            case "short?":
            case "uint":
            case "uint?":
            case "ulong":
            case "ulong?":
            case "ushort":
            case "ushort?":
            {
                outputValueName = builder.GetVariableName("output");

                builder.AppendLine(
                    $"if (!{baseElementType.FullNameNonNullable}.TryParse({inputValueName}, out var {outputValueName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                SerializableAttributesExecutor.ValidateNumberType(builder, outputValueName, name, property);
                break;
            }

            case "string":
            case "string?":
            {
                outputValueName = builder.GetVariableName("output");
                builder.AppendLine();

                builder.AppendLine($"var {outputValueName} = {inputValueName};");

                SerializableAttributesExecutor.ValidateStringType(builder, outputValueName, name, property);
                break;
            }

            case "global::System.Guid":
            case "global::KestrelToolbox.Types.UUIDv1":
            case "global::System.TimeOnly":
            case "global::System.DateTime":
            case "global::System.DateTimeOffset":
            {
                outputValueName = builder.GetVariableName("output");
                builder.AppendLine(
                    $"if (!{baseElementType.FullNameNonNullable}.TryParse({inputValueName}, out var {outputValueName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                break;
            }

            case "global::System.DateOnly":
            {
                outputValueName = builder.GetVariableName("output");
                var dateOnlySettings = property.DateOnlyParseSettings;
                if (dateOnlySettings != null)
                {
                    builder.AppendLine(
                        $"if (!global::System.DateOnly.TryParseExact({inputValueName}, {dateOnlySettings.Format}, out var {outputValueName}))"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"if (!global::System.DateOnly.TryParse({inputValueName}, out var {outputValueName}))"
                    );
                }

                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                break;
            }

            case "global::System.TimeSpan":
            {
                outputValueName = builder.GetVariableName("output");
                builder.AppendLine(
                    $"if (!global::KestrelToolbox.Serialization.TimeSpanParser.TryParse({inputValueName}, out var {outputValueName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                break;
            }

            default:
            {
                if (baseElementType.QueryStringSerializableCustomParseMethod is { IsIndividualElement: true })
                {
                    outputValueName = builder.GetVariableName("output");
                    var customParseMethod = baseElementType.QueryStringSerializableCustomParseMethod;
                    builder.AppendLine(
                        $"if (!{customParseMethod.FullMethodName}({inputValueName}, out {customParseMethod.ResultTypeFullName} {outputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(builder);
                    }
                }
                else if (baseElementType.TypeKind == TypeKind.Enum)
                {
                    var key = new MethodReferenceKey(SerializableAttributeType.Enum, baseElementType.MethodKeyName);
                    var methodReference = this.MethodReferences[key];

                    outputValueName = builder.GetVariableName("output");
                    builder.AppendLine(
                        $"if (!{methodReference.ParsingMethodName}({inputValueName}, out {methodReference.ParsingType} {outputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine($"error = $\"{name} > {{error}}\";");
                        SerializableAttributesExecutor.ReturnError(builder);
                    }
                }
                else
                {
                    reportDiagnostic(
                        DiagnosticMessages.CreatePropertyTypeUnknown(
                            property.GetLocation(),
                            baseElementType.FullName,
                            "QueryStringSerializable"
                        )
                    );
                    throw new NotImplementedException($"Couldn't ParseIndividualElement {baseElementType.FullName}.");
                }

                break;
            }
        }

        return outputValueName;
    }

    private static string GetSingleStringValue(CodeBuilder builder, string inputValueName, string name)
    {
        var outputValueName = builder.GetVariableName("token");
        builder.AppendLine($"if ({inputValueName}.Count == 0)");
        using (builder.AppendBlock())
        {
            SerializableAttributesExecutor.ReturnError(builder, name, "Must contain values.");
        }

        builder.AppendLine($"if ({inputValueName}.Count != 1)");
        using (builder.AppendBlock())
        {
            SerializableAttributesExecutor.ReturnError(builder, name, "Cannot have multiple values.");
        }

        builder.AppendLine($"var {outputValueName} = {inputValueName}[0] ?? string.Empty;");

        return outputValueName;
    }

    /// <inheritdoc />
    public bool Equals(QueryStringSerializableGenerator? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.serializableAttribute.Equals(other.serializableAttribute)
            && this.serializedType.Equals(other.serializedType)
            && this.members.SequenceEqual(other.members)
            && this.MethodReferences.SequenceEqual(other.MethodReferences);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is QueryStringSerializableGenerator other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return this.serializableAttribute.GetHashCode();
    }

    /// <summary>
    /// Compares two <see cref="QueryStringSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(QueryStringSerializableGenerator? left, QueryStringSerializableGenerator? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="QueryStringSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(QueryStringSerializableGenerator? left, QueryStringSerializableGenerator? right)
    {
        return !Equals(left, right);
    }
}
