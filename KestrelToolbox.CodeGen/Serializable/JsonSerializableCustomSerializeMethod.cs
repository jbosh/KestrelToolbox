// <copyright file="JsonSerializableCustomSerializeMethod.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Representation of a method with <c>JsonSerializableCustomParseAttribute</c> attribute on it.
/// </summary>
public sealed class JsonSerializableCustomSerializeMethod : IEquatable<JsonSerializableCustomSerializeMethod>
{
    private const string AttributeName =
        "global::KestrelToolbox.Serialization.DataAnnotations.JsonSerializableCustomSerializeAttribute";

    /// <summary>
    /// Gets the fully qualified method name for this attribute.
    /// </summary>
    public string FullMethodName { get; }

    /// <summary>
    /// Gets the fully qualified type name of the output type.
    /// </summary>
    public string ResultTypeFullName { get; }

    private JsonSerializableCustomSerializeMethod(string fullMethodName, string resultTypeFullName)
    {
        this.FullMethodName = fullMethodName;
        this.ResultTypeFullName = resultTypeFullName;
    }

    /// <summary>
    /// Tries to get a <see cref="JsonSerializableCustomSerializeMethod"/> from a type.
    /// </summary>
    /// <param name="symbol">Class or struct symbol to search properties for.</param>
    /// <param name="customSerializeAttribute">Resulting attribute if it exists.</param>
    /// <returns>True if found, false if not.</returns>
    public static bool TryGetMethodFromClass(
        ITypeSymbol symbol,
        [NotNullWhen(true)] out JsonSerializableCustomSerializeMethod? customSerializeAttribute
    )
    {
        foreach (var member in symbol.GetMembers())
        {
            if (member is not IMethodSymbol methodSymbol)
            {
                continue;
            }

            if (!Extensions.AttributeExists(methodSymbol, AttributeName))
            {
                continue;
            }

            if (methodSymbol.Parameters.Length != 2)
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var writerParameter = methodSymbol.Parameters[0];
            var valueParameter = methodSymbol.Parameters[1];
            if (
                writerParameter.RefKind != RefKind.None
                || writerParameter.Type.GetFullName() != "global::System.Text.Json.Utf8JsonWriter"
            )
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            if (!symbol.Equals(valueParameter.Type, SymbolEqualityComparer.Default))
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            customSerializeAttribute = new JsonSerializableCustomSerializeMethod(
                methodSymbol.GetFullMethodName(),
                valueParameter.Type.GetFullName()
            );
            return true;
        }

        customSerializeAttribute = default;
        return false;
    }

    /// <summary>
    /// Tries to get a custom parse method from <paramref name="methodSymbol"/>.
    /// </summary>
    /// <param name="methodSymbol">Symbol to try.</param>
    /// <param name="valueParameterSymbol">Expected variable type.</param>
    /// <param name="customParseAttribute">Output <see cref="JsonSerializableCustomSerializeMethod"/> or null if not found.</param>
    /// <returns>True if the method matches, false if not.</returns>
    public static bool TryGetMethodFromMethodSymbol(
        IMethodSymbol methodSymbol,
        ITypeSymbol valueParameterSymbol,
        [NotNullWhen(true)] out JsonSerializableCustomSerializeMethod? customParseAttribute
    )
    {
        if (methodSymbol.Parameters.Length != 2)
        {
            customParseAttribute = null;
            return false;
        }

        var writerParameter = methodSymbol.Parameters[0];
        var valueParameter = methodSymbol.Parameters[1];
        if (
            valueParameter.RefKind != RefKind.None
            || writerParameter.Type.GetFullName() != "global::System.Text.Json.Utf8JsonWriter"
        )
        {
            customParseAttribute = null;
            return false;
        }

        if (valueParameter.RefKind != RefKind.None || !valueParameterSymbol.CanCoerceTo(valueParameter.Type))
        {
            customParseAttribute = null;
            return false;
        }

        customParseAttribute = new JsonSerializableCustomSerializeMethod(
            methodSymbol.GetFullMethodName(),
            valueParameter.Type.GetFullName()
        );
        return true;
    }

    /// <inheritdoc />
    public bool Equals(JsonSerializableCustomSerializeMethod? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.FullMethodName == other.FullMethodName && this.ResultTypeFullName == other.ResultTypeFullName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not JsonSerializableCustomSerializeMethod other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.FullMethodName, this.ResultTypeFullName);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableCustomSerializeMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(
        JsonSerializableCustomSerializeMethod? left,
        JsonSerializableCustomSerializeMethod? right
    )
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="QueryStringSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(
        JsonSerializableCustomSerializeMethod? left,
        JsonSerializableCustomSerializeMethod? right
    )
    {
        return !Equals(left, right);
    }
}
