// <copyright file="EnumMember.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Class for representing a member or value of an enum.
/// </summary>
public sealed class EnumMember : IEquatable<EnumMember>
{
    private const string NameAttribute = "global::KestrelToolbox.Serialization.DataAnnotations.NameAttribute";

    [SuppressMessage(
        "StyleCop.CSharp.ReadabilityRules",
        "SA1118:Parameter should not span multiple lines",
        Justification = "It's long."
    )]
    private static readonly SymbolDisplayFormat EnumFullNameDisplayFormat = new(
        globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Included,
        typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
        propertyStyle: SymbolDisplayPropertyStyle.NameOnly,
        genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
        memberOptions: SymbolDisplayMemberOptions.IncludeParameters
            | SymbolDisplayMemberOptions.IncludeContainingType
            | SymbolDisplayMemberOptions.IncludeExplicitInterface,
        parameterOptions: SymbolDisplayParameterOptions.IncludeParamsRefOut | SymbolDisplayParameterOptions.IncludeType,
        miscellaneousOptions: SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
            | SymbolDisplayMiscellaneousOptions.UseSpecialTypes
            | SymbolDisplayMiscellaneousOptions.UseAsterisksInMultiDimensionalArrays
            | SymbolDisplayMiscellaneousOptions.UseErrorTypeSymbolName
            | SymbolDisplayMiscellaneousOptions.IncludeNullableReferenceTypeModifier
            | SymbolDisplayMiscellaneousOptions.ExpandNullable
    );

    /// <summary>
    /// Gets the list of names that this member can go by.
    /// </summary>
    public List<string> Names { get; }

    /// <summary>
    /// Gets the constant value that represents this member.
    /// </summary>
    public object? ConstantValue { get; }

    /// <summary>
    /// Gets the fully qualified name for this member.
    /// </summary>
    public string FullName { get; }

    /// <inheritdoc />
    public override string ToString() => $"{this.FullName} ({this.ConstantValue}): {string.Join(", ", this.Names)}";

    /// <summary>
    /// Initializes a new instance of the <see cref="EnumMember"/> class.
    /// </summary>
    /// <param name="names">Names this member can go by.</param>
    /// <param name="symbol">Actual field symbol for this member.</param>
    public EnumMember(List<string> names, IFieldSymbol symbol)
    {
        this.Names = names;
        this.ConstantValue = symbol.ConstantValue;
        this.FullName = symbol.ToDisplayString(EnumFullNameDisplayFormat);
    }

    /// <summary>
    /// Gets a set of members from a <see cref="INamedTypeSymbol"/>.
    /// </summary>
    /// <param name="enumSymbol">Symbol to get members of.</param>
    /// <returns>Collection of members.</returns>
    public static IEnumerable<EnumMember> GetMembers(INamedTypeSymbol enumSymbol) =>
        GetMembers(enumSymbol.GetMembers());

    /// <summary>
    /// Gets a set of members from a <see cref="INamedTypeSymbol"/>.
    /// </summary>
    /// <param name="members">List of members to process.</param>
    /// <returns>Collection of members.</returns>
    public static IEnumerable<EnumMember> GetMembers(ImmutableArray<ISymbol> members)
    {
        foreach (var member in members)
        {
            if (member is not IFieldSymbol fieldSymbol)
            {
                continue;
            }

            var names = new List<string>();
            foreach (var attribute in member.GetAttributes())
            {
                if (!attribute.AttributeClass?.GetFullName().Equals(NameAttribute, StringComparison.Ordinal) ?? true)
                {
                    continue;
                }

                if (attribute.NamedArguments.Length != 0)
                {
                    throw new NotImplementedException(
                        $"Named arguments are not implemented on NameAttribute on {member.GetFullMetadataName()}"
                    );
                }

                foreach (var constructorArgument in attribute.ConstructorArguments)
                {
                    if (constructorArgument.Kind == TypedConstantKind.Primitive)
                    {
                        names.Add(constructorArgument.Value!.ToString()!);
                    }
                    else
                    {
                        foreach (var value in constructorArgument.Values)
                        {
                            names.Add(value.Value!.ToString()!);
                        }
                    }
                }
            }

            yield return new EnumMember(names, fieldSymbol);
        }
    }

    /// <inheritdoc />
    public bool Equals(EnumMember? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Names.SequenceEqual(other.Names)
            && Equals(this.ConstantValue, other.ConstantValue)
            && this.FullName == other.FullName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is EnumMember other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.ConstantValue, this.FullName);
    }

    /// <summary>
    /// Compares two <see cref="EnumMember"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(EnumMember? left, EnumMember? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="EnumMember"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(EnumMember? left, EnumMember? right)
    {
        return !Equals(left, right);
    }
}
