// <copyright file="SerializableAttributesGeneratorContext.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Context for generation during <see cref="SerializableAttributesGenerator"/>.
/// </summary>
public sealed class SerializableAttributesGeneratorContext
{
    /// <summary>
    /// Gets the semantic model.
    /// </summary>
    public SemanticModel SemanticModel { get; }

    /// <summary>
    /// Gets the set of symbols that have been added so far by fully qualified name.
    /// </summary>
    /// <remarks>This cache is used to prevent infinite recursion when fetching types.</remarks>
    public Dictionary<string, CacheableTypeSymbol> SymbolCache { get; } = new();

    /// <summary>
    /// Gets the list of diagnostics that have been added.
    /// </summary>
    public List<Diagnostic> Diagnostics { get; } = new();

    private Dictionary<MethodReferenceKey, SerializableAttribute> SerializedTypes { get; } = new();

    private Queue<(
        MethodReferenceKey methodReferenceKey,
        SerializableAttribute serializableAttribute
    )> PendingSerializedTypes { get; } = new();

    /// <summary>
    /// Initializes a new instance of the <see cref="SerializableAttributesGeneratorContext"/> class.
    /// </summary>
    /// <param name="semanticModel">Semantic model used for parsing.</param>
    public SerializableAttributesGeneratorContext(SemanticModel semanticModel)
    {
        this.SemanticModel = semanticModel;
    }

    /// <summary>
    /// Adds a diagnostic to be reported for compilation.
    /// </summary>
    /// <param name="diagnostic">Diagnostic.</param>
    public void ReportDiagnostic(Diagnostic diagnostic) => this.Diagnostics.Add(diagnostic);

    /// <summary>
    /// Tries to get a serialized type based on method reference.
    /// </summary>
    /// <param name="methodReferenceKey">Method reference key information.</param>
    /// <param name="serializableAttribute">Found serializable attribute or default if it does not exist.</param>
    /// <returns>True if the serializable attribute was found, false if not.</returns>
    public bool TryGetSerializedType(
        MethodReferenceKey methodReferenceKey,
        [NotNullWhen(true)] out SerializableAttribute? serializableAttribute
    )
    {
        return this.SerializedTypes.TryGetValue(methodReferenceKey, out serializableAttribute);
    }

    /// <summary>
    /// Tries to add a serialized type to the queue.
    /// </summary>
    /// <param name="methodReferenceKey">Method reference key information.</param>
    /// <param name="serializableAttribute">Serializable attribute to be generated.</param>
    /// <param name="requiringType">The parent type that is implicitly adding the serialized type to the queue.</param>
    /// <returns>True if the element was added, false if it already exists.</returns>
    public bool TryAddSerializedType(
        MethodReferenceKey methodReferenceKey,
        SerializableAttribute serializableAttribute,
        ITypeSymbol? requiringType = null
    )
    {
        if (this.SerializedTypes.TryGetValue(methodReferenceKey, out var value))
        {
            if (requiringType != null)
            {
                if (serializableAttribute.GenerateParse && !value.GenerateParse)
                {
                    if (serializableAttribute.IsImplicitlyGenerated)
                    {
                        this.SerializedTypes[methodReferenceKey] = new SerializableAttribute(
                            value.Type,
                            value.IsStatic,
                            value.DeclaringType,
                            value.SerializedType,
                            value.AttributeSyntax,
                            value.TypeDeclarationSyntax,
                            value.ParsingMethodName,
                            value.SerializingMethodName,
                            value.CustomParsingMethodName,
                            value.CustomSerializingMethodName,
                            true,
                            value.GenerateSerialize,
                            value.IsImplicitlyGenerated
                        );
                    }
                    else
                    {
                        this.ReportDiagnostic(
                            DiagnosticMessages.CreateJsonSerializableTypeParsingDisabled(
                                value.AttributeSyntax.GetLocation(),
                                serializableAttribute.SerializedType.GetFullName(),
                                requiringType.GetFullName()
                            )
                        );
                    }
                }

                if (serializableAttribute.GenerateSerialize && !value.GenerateSerialize)
                {
                    if (serializableAttribute.IsImplicitlyGenerated)
                    {
                        this.SerializedTypes[methodReferenceKey] = new SerializableAttribute(
                            value.Type,
                            value.IsStatic,
                            value.DeclaringType,
                            value.SerializedType,
                            value.AttributeSyntax,
                            value.TypeDeclarationSyntax,
                            value.ParsingMethodName,
                            value.SerializingMethodName,
                            value.CustomParsingMethodName,
                            value.CustomSerializingMethodName,
                            value.GenerateParse,
                            true,
                            value.IsImplicitlyGenerated
                        );
                    }
                    else
                    {
                        this.ReportDiagnostic(
                            DiagnosticMessages.CreateJsonSerializableTypeSerializationDisabled(
                                value.AttributeSyntax.GetLocation(),
                                serializableAttribute.SerializedType.GetFullName(),
                                requiringType.GetFullName()
                            )
                        );
                    }
                }
            }

            return false;
        }

        this.SerializedTypes.Add(methodReferenceKey, serializableAttribute);
        this.PendingSerializedTypes.Enqueue((methodReferenceKey, serializableAttribute));
        return true;
    }

    /// <summary>
    /// Try to dequeue a serializable attribute from the pending list.
    /// </summary>
    /// <param name="result">Resulting pair to generate.</param>
    /// <returns>True if there are items in the queue. False if not.</returns>
    public bool TryDequeueSerializedType(
        out (MethodReferenceKey methodReferenceKey, SerializableAttribute serializableAttribute) result
    )
    {
        if (this.PendingSerializedTypes.Count == 0)
        {
            result = default;
            return false;
        }

        result = this.PendingSerializedTypes.Dequeue();
        return true;
    }
}
