// <copyright file="JsonSerializableCustomParseMethod.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Representation of a method with <c>JsonSerializableCustomParseAttribute</c> attribute on it.
/// </summary>
public sealed class JsonSerializableCustomParseMethod : IEquatable<JsonSerializableCustomParseMethod>
{
    private const string AttributeName =
        "global::KestrelToolbox.Serialization.DataAnnotations.JsonSerializableCustomParseAttribute";

    /// <summary>
    /// Gets the fully qualified method name for this attribute.
    /// </summary>
    public string FullMethodName { get; }

    /// <summary>
    /// Gets the fully qualified type name of the output type.
    /// </summary>
    public string ResultTypeFullName { get; }

    private JsonSerializableCustomParseMethod(string fullMethodName, string resultTypeFullName)
    {
        this.FullMethodName = fullMethodName;
        this.ResultTypeFullName = resultTypeFullName;
    }

    /// <summary>
    /// Tries to get a <see cref="JsonSerializableCustomParseMethod"/> from a type.
    /// </summary>
    /// <param name="symbol">Class or struct symbol to search properties for.</param>
    /// <param name="customParseAttribute">Resulting attribute if it exists.</param>
    /// <returns>True if found, false if not.</returns>
    public static bool TryGetMethodFromClass(
        ITypeSymbol symbol,
        [NotNullWhen(true)] out JsonSerializableCustomParseMethod? customParseAttribute
    )
    {
        foreach (var member in symbol.GetMembers())
        {
            if (member is not IMethodSymbol methodSymbol)
            {
                continue;
            }

            if (!Extensions.AttributeExists(methodSymbol, AttributeName))
            {
                continue;
            }

            if (methodSymbol.Parameters.Length != 3)
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            var valueParameter = methodSymbol.Parameters[0];
            var resultParameter = methodSymbol.Parameters[1];
            var errorsParameter = methodSymbol.Parameters[2];
            if (
                resultParameter.RefKind != RefKind.Out
                || errorsParameter.Type.GetFullName() != "string?"
                || errorsParameter.RefKind != RefKind.Out
            )
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            if (!symbol.Equals(resultParameter.Type, SymbolEqualityComparer.Default))
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            if (
                valueParameter.RefKind != RefKind.Ref
                || valueParameter.Type.GetFullName() != "global::System.Text.Json.Utf8JsonReader"
            )
            {
                throw new NotImplementedException("Need better error handling for invalid method.");
            }

            customParseAttribute = new JsonSerializableCustomParseMethod(
                methodSymbol.GetFullMethodName(),
                resultParameter.Type.GetFullName()
            );
            return true;
        }

        customParseAttribute = default;
        return false;
    }

    /// <summary>
    /// Tries to get a custom parse method from <paramref name="methodSymbol"/>.
    /// </summary>
    /// <param name="methodSymbol">Symbol to try.</param>
    /// <param name="resultParameterSymbol">Expected resulting variable type.</param>
    /// <param name="customParseAttribute">Output <see cref="JsonSerializableCustomParseMethod"/> or null if not found.</param>
    /// <returns>True if the method matches, false if not.</returns>
    public static bool TryGetMethodFromMethodSymbol(
        IMethodSymbol methodSymbol,
        ITypeSymbol resultParameterSymbol,
        [NotNullWhen(true)] out JsonSerializableCustomParseMethod? customParseAttribute
    )
    {
        if (methodSymbol.Parameters.Length != 3)
        {
            customParseAttribute = null;
            return false;
        }

        var readerParameter = methodSymbol.Parameters[0];
        var resultParameter = methodSymbol.Parameters[1];
        var errorsParameter = methodSymbol.Parameters[2];
        if (
            readerParameter.RefKind != RefKind.Ref
            || readerParameter.Type.GetFullName() != "global::System.Text.Json.Utf8JsonReader"
        )
        {
            customParseAttribute = null;
            return false;
        }

        if (resultParameter.RefKind != RefKind.Out || !resultParameterSymbol.CanCoerceTo(resultParameter.Type))
        {
            customParseAttribute = null;
            return false;
        }

        if (
            resultParameter.RefKind != RefKind.Out
            || errorsParameter.Type.GetFullName() != "string?"
            || errorsParameter.RefKind != RefKind.Out
        )
        {
            customParseAttribute = null;
            return false;
        }

        customParseAttribute = new JsonSerializableCustomParseMethod(
            methodSymbol.GetFullMethodName(),
            resultParameter.Type.GetFullName()
        );
        return true;
    }

    /// <inheritdoc />
    public bool Equals(JsonSerializableCustomParseMethod? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.FullMethodName == other.FullMethodName && this.ResultTypeFullName == other.ResultTypeFullName;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not JsonSerializableCustomParseMethod other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.FullMethodName, this.ResultTypeFullName);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(JsonSerializableCustomParseMethod? left, JsonSerializableCustomParseMethod? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="QueryStringSerializableCustomParseMethod"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(JsonSerializableCustomParseMethod? left, JsonSerializableCustomParseMethod? right)
    {
        return !Equals(left, right);
    }
}
