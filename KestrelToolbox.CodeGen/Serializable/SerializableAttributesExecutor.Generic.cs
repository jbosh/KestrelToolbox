// <copyright file="SerializableAttributesExecutor.Generic.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Executor for <see cref="SerializableAttributesGenerator"/>.
/// </summary>
public sealed partial class SerializableAttributesExecutor
{
    /// <summary>
    /// Generates code to validate a number value.
    /// </summary>
    /// <param name="builder">Builder to add code to.</param>
    /// <param name="inputValueName">Name of the variable to validate.</param>
    /// <param name="name">Name of the property associated with this validation. Null if it does not exist. This is used for error handling.</param>
    /// <param name="property">Property to get validation information from.</param>
    public static void ValidateNumberType(
        CodeBuilder builder,
        string inputValueName,
        string? name,
        PropertyMember property
    )
    {
        var range = property.Range;
        if (range != null)
        {
            builder.AppendLine($"if ({inputValueName} < {range.Min})");
            using (builder.AppendBlock())
            {
                ReturnError(builder, name, $"Was not in the range [{range.MinOriginal}..{range.MaxOriginal}]");
            }

            builder.AppendLine($"if ({inputValueName} > {range.Max})");
            using (builder.AppendBlock())
            {
                ReturnError(builder, name, $"Was not in the range [{range.MinOriginal}..{range.MaxOriginal}]");
            }
        }

        var validValues = property.ValidValues;
        if (validValues != null)
        {
            builder.AppendLine($"switch ({inputValueName})");
            using (builder.AppendBlock())
            {
                foreach (var value in validValues.Values)
                {
                    builder.AppendLine($"case {value}:");
                }

                using (builder.AppendBlock())
                {
                    builder.AppendLine("// Found our value.");
                    builder.AppendLine("break;");
                }

                builder.AppendLine("default:");
                using (builder.AppendBlock())
                {
                    ReturnError(builder, name, $"Must be one of: {string.Join(", ", validValues.Values)}.");
                }
            }

            builder.AppendLine();
        }
    }

    /// <summary>
    /// Generates code to validate a string value.
    /// </summary>
    /// <param name="builder">Builder to add code to.</param>
    /// <param name="inputValueName">Name of the variable to validate.</param>
    /// <param name="name">Name of the property associated with this validation. Null if it does not exist. This is used for error handling.</param>
    /// <param name="property">Property to get validation information from.</param>
    public static void ValidateStringType(
        CodeBuilder builder,
        string inputValueName,
        string? name,
        PropertyMember property
    )
    {
        var trimValue = property.TrimValue;
        if (trimValue is { OnParse: true })
        {
            if (trimValue.TrimChars == null)
            {
                builder.AppendLine($"{inputValueName} = {inputValueName}.Trim();");
            }
            else
            {
                builder.AppendLine($"{inputValueName} = {inputValueName}.Trim({trimValue.TrimChars});");
            }
        }

        var minLength = default(string?);
        var maxLength = default(string?);

        var stringLength = property.StringLength;
        if (stringLength != null)
        {
            minLength = stringLength.Min;
            maxLength = stringLength.Max;
        }

        if (minLength != null)
        {
            builder.AppendLine($"if ({inputValueName}.Length < {minLength})");
            using (builder.AppendBlock())
            {
                if (long.TryParse(minLength, out var minLengthInt))
                {
                    ReturnError(
                        builder,
                        name,
                        $"Must be longer than {minLengthInt - 1} character{(minLengthInt - 1 == 1 ? string.Empty : "s")}."
                    );
                }
                else
                {
                    ReturnError(builder, name, $"Must be longer than at least {minLength} characters.");
                }
            }
        }

        if (maxLength != null)
        {
            builder.AppendLine($"if ({inputValueName}.Length > {maxLength})");
            using (builder.AppendBlock())
            {
                ReturnError(builder, name, $"Must be shorter than {maxLength} characters.");
            }
        }

        var validValues = property.ValidValues;
        if (validValues != null)
        {
            builder.AppendLine($"switch ({inputValueName})");
            using (builder.AppendBlock())
            {
                foreach (var value in validValues.Values)
                {
                    builder.AppendLine($"case {value}:");
                }

                using (builder.AppendBlock())
                {
                    builder.AppendLine("// Found our value.");
                    builder.AppendLine("break;");
                }

                builder.AppendLine("default:");
                using (builder.AppendBlock())
                {
                    ReturnError(builder, name, $"Must be one of: {string.Join(", ", validValues.Values)}.");
                }
            }

            builder.AppendLine();
        }

        var emailAddress = property.EmailAddress;
        if (emailAddress)
        {
            builder.AppendLine(
                $"if (!global::KestrelToolbox.Serialization.DataAnnotations.EmailAddressAttribute.IsValid({inputValueName}))"
            );
            using (builder.AppendBlock())
            {
                ReturnError(builder, name, $"Is not a valid email address.");
            }
        }

        var nullOnEmpty = property.NullOnEmpty;
        if (nullOnEmpty)
        {
            builder.AppendLine($"if ({inputValueName}.Length == 0)");
            using (builder.AppendBlock())
            {
                builder.AppendLine($"{inputValueName} = null;");
            }
        }

        var allowOnlyWhiteSpace = property.AllowOnlyWhiteSpace;
        if (!allowOnlyWhiteSpace)
        {
            builder.AppendLine($"if (string.IsNullOrWhiteSpace({inputValueName}))");
            using (builder.AppendBlock())
            {
                ReturnError(builder, name, $"Must contain characters that are not whitespace.");
            }
        }
    }

    /// <summary>
    /// Finds an inherited dictionary of type <see cref="System.Collections.Generic.IDictionary{T,U}"/> from <paramref name="type"/>.
    /// </summary>
    /// <param name="type">Type to find interface from.</param>
    /// <returns>The <see cref="System.Collections.Generic.IDictionary{T,U}"/> type interface.</returns>
    public static CacheableTypeSymbol? FindGenericStringDictionary(CacheableTypeSymbol type)
    {
        foreach (var @interface in type.Interfaces)
        {
            if (
                !@interface.FullName.StartsWith(
                    "global::System.Collections.Generic.IDictionary<string, ",
                    StringComparison.Ordinal
                )
            )
            {
                continue;
            }

            return @interface;
        }

        return default;
    }

    /// <summary>
    /// Finds an inherited dictionary of type <see cref="System.Collections.Generic.IDictionary{T,U}"/> from <paramref name="type"/>.
    /// </summary>
    /// <param name="type">Type to find interface from.</param>
    /// <returns>The <see cref="System.Collections.Generic.IDictionary{T,U}"/> type interface.</returns>
    public static ITypeSymbol? FindGenericStringDictionary(ITypeSymbol type)
    {
        foreach (var @interface in type.Interfaces)
        {
            if (
                !@interface
                    .GetFullName()
                    .StartsWith("global::System.Collections.Generic.IDictionary<string, ", StringComparison.Ordinal)
            )
            {
                continue;
            }

            return @interface;
        }

        return default;
    }

    /// <summary>
    /// Gets the base type from a possibly nullable type.
    /// </summary>
    /// <param name="type">Serialized type.</param>
    /// <param name="baseType">Output type that has nullable removed.</param>
    /// <param name="isNullable">True if <paramref name="type"/> is nullable.</param>
    public static void GetNullableType(ITypeSymbol type, out ITypeSymbol baseType, out bool isNullable)
    {
        if (type.OriginalDefinition.GetFullName() == "global::System.Nullable<T>")
        {
            isNullable = true;
            baseType = (INamedTypeSymbol)((INamedTypeSymbol)type).TypeArguments[0];
        }
        else
        {
            baseType = type;
            isNullable = false;
        }
    }

    /// <summary>
    /// Returns default without setting error string.
    /// </summary>
    /// <param name="builder">Builder to append code to.</param>
    public static void ReturnError(CodeBuilder builder)
    {
        builder.AppendLine("result = default;");
        builder.AppendLine("return false;");
    }

    /// <summary>
    /// Returns default and sets the error.
    /// </summary>
    /// <param name="builder">Builder to append code to.</param>
    /// <param name="error">Error message.</param>
    public static void ReturnError(CodeBuilder builder, string error)
    {
        var errorConstant = SymbolDisplay.FormatPrimitive(error, true, false);
        builder.AppendLine($"error = {errorConstant};");
        ReturnError(builder);
    }

    /// <summary>
    /// Returns default and sets the error to be contain the name of the variable.
    /// </summary>
    /// <param name="builder">Builder to append code to.</param>
    /// <param name="name">Name of the variable or null if there isn't one.</param>
    /// <param name="error">Error message.</param>
    public static void ReturnError(CodeBuilder builder, string? name, string error)
    {
        if (name != null)
        {
            var firstChar = error[0];
            error = $"'{name}' {char.ToLowerInvariant(firstChar)}{error.Substring(1)}";
        }

        ReturnError(builder, error);
    }

    /// <summary>
    /// Appends information onto an already being returned error message. This is for recursive failures.
    /// </summary>
    /// <param name="builder">Builder to append code to.</param>
    /// <param name="name">Name of the variable or null if there isn't one.</param>
    /// <param name="errorName">Variable name of the error message to append.</param>
    public static void AppendError(CodeBuilder builder, string? name, string errorName = "error")
    {
        if (name != null)
        {
            builder.AppendLine($"error = $\"{name} > {{{errorName}}}\";");
        }

        ReturnError(builder);
    }

    /// <summary>
    /// Gets a value indicating whether the type is built-in.
    /// </summary>
    /// <param name="symbol">Symbol to check if it is a built-in type.</param>
    /// <returns>True if the type is built-in, false if not.</returns>
    public static bool IsBuiltInType(CacheableTypeSymbol symbol)
    {
        if (PropertyMember.IsParsedAsArray(symbol))
        {
            return true;
        }

        if (PropertyMember.IsParsedAsSet(symbol))
        {
            return true;
        }

        if (PropertyMember.IsPropertyTuple(symbol))
        {
            return true;
        }

        return IsBasicType(symbol);
    }

    /// <summary>
    /// Gets a value indicating whether the type is built-in.
    /// </summary>
    /// <param name="symbol">Symbol to check if it is a built-in type.</param>
    /// <returns>True if the type is built-in, false if not.</returns>
    public static bool IsBuiltInType(ITypeSymbol symbol)
    {
        if (PropertyMember.IsParsedAsArray(symbol))
        {
            return true;
        }

        if (PropertyMember.IsParsedAsSet(symbol))
        {
            return true;
        }

        if (PropertyMember.IsPropertyTuple(symbol))
        {
            return true;
        }

        return IsBasicType(symbol);
    }

    /// <summary>
    /// Gets a value indicating whether the type is a basic type that is not overridable. This means int, bool,
    /// char, etc...
    /// </summary>
    /// <param name="symbol">Symbol to check if it is a built-in type.</param>
    /// <returns>True if the type is basic, false if not.</returns>
    public static bool IsBasicType(CacheableTypeSymbol symbol) => IsBasicType(symbol.FullName);

    /// <summary>
    /// Gets a value indicating whether the type is a basic type that is not overridable. This means int, bool,
    /// char, etc...
    /// </summary>
    /// <param name="symbol">Symbol to check if it is a built-in type.</param>
    /// <returns>True if the type is basic, false if not.</returns>
    public static bool IsBasicType(ITypeSymbol symbol) => IsBasicType(symbol.GetFullName());

    /// <summary>
    /// Gets a value indicating whether the type is a basic type that is not overridable. This means int, bool,
    /// char, etc...
    /// </summary>
    /// <param name="fullName">Full name of the symbol.</param>
    /// <returns>True if the type is basic, false if not.</returns>
    public static bool IsBasicType(string fullName)
    {
        return fullName switch
        {
            "bool" => true,
            "global::System.Nullable<bool>" => true,
            "char" => true,
            "global::System.Nullable<char>" => true,
            "byte" => true,
            "global::System.Nullable<byte>" => true,
            "sbyte" => true,
            "global::System.Nullable<sbyte>" => true,
            "short" => true,
            "global::System.Nullable<short>" => true,
            "int" => true,
            "global::System.Nullable<int>" => true,
            "long" => true,
            "global::System.Nullable<long>" => true,
            "ushort" => true,
            "global::System.Nullable<ushort>" => true,
            "uint" => true,
            "global::System.Nullable<uint>" => true,
            "ulong" => true,
            "global::System.Nullable<ulong>" => true,
            "string" => true,
            "string?" => true,
            "float" => true,
            "global::System.Nullable<float>" => true,
            "double" => true,
            "global::System.Nullable<double>" => true,
            "decimal" => true,
            "global::System.Nullable<decimal>" => true,
            "global::System.Guid" => true,
            "global::System.Nullable<global::System.Guid>" => true,
            "global::System.DateOnly" => true,
            "global::System.Nullable<global::System.DateOnly>" => true,
            "global::System.DateTime" => true,
            "global::System.Nullable<global::System.DateTime>" => true,
            "global::System.DateTimeOffset" => true,
            "global::System.Nullable<global::System.DateTimeOffset>" => true,
            "global::System.TimeOnly" => true,
            "global::System.Nullable<global::System.TimeOnly>" => true,
            "global::System.TimeSpan" => true,
            "global::System.Nullable<global::System.TimeSpan>" => true,
            "global::System.Text.Json.Nodes.JsonNode" => true,
            "global::System.Text.Json.Nodes.JsonNode?" => true,
            "global::System.Text.Json.Nodes.JsonObject" => true,
            "global::System.Text.Json.Nodes.JsonObject?" => true,
            "global::System.Text.Json.Nodes.JsonArray" => true,
            "global::System.Text.Json.Nodes.JsonArray?" => true,
            "global::KestrelToolbox.Types.UUIDv1" => true,
            "global::System.Nullable<global::KestrelToolbox.Types.UUIDv1>?" => true,
            _ => false,
        };
    }

    /// <summary>
    /// Gets a serializable attribute generated method name.
    /// </summary>
    /// <param name="fullPublicName">Full name of the public class.</param>
    /// <param name="name">Name of the generated method.</param>
    /// <param name="visibility">Visibility of the method.</param>
    /// <returns>The fully qualified method name.</returns>
    public string GetMethodName(string fullPublicName, string name, SerializableVisibility visibility)
    {
        foreach (var c in name)
        {
            switch (c)
            {
                case '.':
                case ':':
                {
                    // Fully qualified name.
                    return name;
                }

                default:
                {
                    // Normal character
                    break;
                }
            }
        }

        if (visibility == SerializableVisibility.File)
        {
            return $"{this.fileLocalMethodClassName}.{name}";
        }

        return $"{fullPublicName}.{name}";
    }
}
