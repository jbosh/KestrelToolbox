// <copyright file="JsonSerializableSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// An instance of JsonSerializableSettingsAttribute that can be used on syntax or symbols.
/// </summary>
public sealed class JsonSerializableSettingsAttribute : IEquatable<JsonSerializableSettingsAttribute>
{
    private const string AttributeName =
        "global::KestrelToolbox.Serialization.DataAnnotations.JsonSerializableSettingsAttribute";

    /// <summary>
    /// Gets a value indicating whether the object should only contain known properties.
    /// </summary>
    public bool AllowExtraProperties { get; }

    /// <summary>
    /// Gets a value indicating whether fast parsing is enabled.
    /// </summary>
    public bool FastParsingAllPropertiesAreDefault { get; }

    private const bool DefaultAllowExtraProperties = true;
    private const bool DefaultFastParsingAllPropertiesAreDefault = false;

    /// <summary>
    /// Initializes a new instance of the <see cref="JsonSerializableSettingsAttribute"/> class.
    /// </summary>
    public JsonSerializableSettingsAttribute()
    {
        this.AllowExtraProperties = DefaultAllowExtraProperties;
        this.FastParsingAllPropertiesAreDefault = DefaultFastParsingAllPropertiesAreDefault;
    }

    private JsonSerializableSettingsAttribute(bool allowExtraProperties, bool fastParsingAllPropertiesAreDefault)
    {
        this.AllowExtraProperties = allowExtraProperties;
        this.FastParsingAllPropertiesAreDefault = fastParsingAllPropertiesAreDefault;
    }

    /// <summary>
    /// Try to get enum serialization attribute proper from a symbol.
    /// </summary>
    /// <param name="symbol">Enum symbol.</param>
    /// <param name="result">Resulting <see cref="JsonSerializableSettingsAttribute"/> if it's found.</param>
    /// <returns>True if JsonSerializableSettingsAttribute was found.</returns>
    public static bool TryGet(ITypeSymbol symbol, [NotNullWhen(true)] out JsonSerializableSettingsAttribute? result)
    {
        var allowExtraProperties = DefaultAllowExtraProperties;
        var fastParsingAllPropertiesAreDefault = DefaultFastParsingAllPropertiesAreDefault;

        result = default;
        foreach (var attribute in symbol.GetAttributes())
        {
            if (attribute.AttributeClass?.GetFullName() != AttributeName)
            {
                continue;
            }

            foreach (var argument in attribute.NamedArguments)
            {
                var key = argument.Key;
                switch (key)
                {
                    case "AllowExtraProperties":
                    {
                        var value = argument.Value;
                        if (value.Value is not bool boolValue)
                        {
                            // Compiler should handle this.
                            return false;
                        }

                        allowExtraProperties = boolValue;
                        break;
                    }

                    case "FastParsingAllPropertiesAreDefault":
                    {
                        var value = argument.Value;
                        if (value.Value is not bool boolValue)
                        {
                            // Compiler should handle this.
                            return false;
                        }

                        fastParsingAllPropertiesAreDefault = boolValue;
                        break;
                    }

                    default:
                    {
                        // Compiler should handle this.
                        return false;
                    }
                }
            }

            result = new JsonSerializableSettingsAttribute(allowExtraProperties, fastParsingAllPropertiesAreDefault);
            return true;
        }

        return false;
    }

    /// <inheritdoc />
    public bool Equals(JsonSerializableSettingsAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.AllowExtraProperties == other.AllowExtraProperties;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not JsonSerializableSettingsAttribute other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return this.AllowExtraProperties.GetHashCode();
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(JsonSerializableSettingsAttribute? left, JsonSerializableSettingsAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableSettingsAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(JsonSerializableSettingsAttribute? left, JsonSerializableSettingsAttribute? right)
    {
        return !Equals(left, right);
    }
}
