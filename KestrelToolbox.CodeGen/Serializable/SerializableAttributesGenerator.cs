﻿// <copyright file="SerializableAttributesGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for SerializableAttributes.
/// </summary>
public sealed class SerializableAttributesGenerator : IIncrementalGenerator
{
    /// <inheritdoc />
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        var foundClasses = context
            .SyntaxProvider.CreateSyntaxProvider(IsValidQuick, Gather)
            .Where(n => n != null)
            .Collect();
        var combinedClasses = foundClasses.SelectMany(Combine).Where(n => n != null);
        context.RegisterSourceOutput(combinedClasses, static (ctx, s) => s!.Execute(ctx));
    }

    private static bool IsValidQuick(SyntaxNode syntaxNode, CancellationToken cancellationToken)
    {
        if (syntaxNode.IsKind(SyntaxKind.ClassDeclaration) || syntaxNode.IsKind(SyntaxKind.StructDeclaration))
        {
            // Collect classes with Serializable attributes.
            var typeDeclaration = (TypeDeclarationSyntax)syntaxNode;
            if (typeDeclaration.AttributeLists.Count == 0)
            {
                return false;
            }

            return typeDeclaration.AttributeLists.Any(attributeList => attributeList.Attributes.Count != 0);
        }

        if (syntaxNode.IsKind(SyntaxKind.MethodDeclaration))
        {
            // Collect Route methods
            var methodDeclaration = (MethodDeclarationSyntax)syntaxNode;

            if (methodDeclaration.AttributeLists.Count == 0)
            {
                // Require RouteAttribute on the method.
                return false;
            }

            var parameters = methodDeclaration.ParameterList.Parameters;
            if (parameters.Count == 0)
            {
                // Require there be parameters.
                return false;
            }

            var parentKind = methodDeclaration.Parent?.Kind();
            if (parentKind is not SyntaxKind.ClassDeclaration and not SyntaxKind.StructDeclaration)
            {
                return false;
            }

            foreach (var parameter in parameters)
            {
                foreach (var attributeList in parameter.AttributeLists)
                {
                    if (attributeList.Attributes.Count != 0)
                    {
                        // Require at least one parameter has an attribute on it.
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static CollectedParser? Gather(GeneratorSyntaxContext context, CancellationToken cancellationToken)
    {
        var node = context.Node;
        if (node.IsKind(SyntaxKind.ClassDeclaration) || node.IsKind(SyntaxKind.StructDeclaration))
        {
            return GatherClasses(context, (TypeDeclarationSyntax)node, cancellationToken);
        }

        if (node.IsKind(SyntaxKind.MethodDeclaration))
        {
            return GatherRoutes(context, (MethodDeclarationSyntax)node, cancellationToken);
        }

        throw new NotImplementedException($"Unknown node kind: {node.Kind()}.");
    }

    private static CollectedParser GatherClasses(
        GeneratorSyntaxContext context,
        TypeDeclarationSyntax typeDeclaration,
        CancellationToken cancellationToken
    )
    {
        var semanticModel = context.SemanticModel;

        var parserGenContext = new SerializableAttributesGeneratorContext(semanticModel);

        var serializableSyntaxAttributes = SerializableAttribute
            .FilterAttributes(typeDeclaration, semanticModel, cancellationToken)
            .ToList();
        if (serializableSyntaxAttributes.Count != 0)
        {
            foreach (var serializableAttributeSyntax in serializableSyntaxAttributes)
            {
                cancellationToken.ThrowIfCancellationRequested();
                var serializedAttributes = SerializableAttribute.GetAttributes(
                    parserGenContext.ReportDiagnostic,
                    typeDeclaration,
                    serializableAttributeSyntax,
                    semanticModel,
                    cancellationToken
                );
                foreach (var serializableAttribute in serializedAttributes)
                {
                    var key = new MethodReferenceKey(
                        serializableAttribute.Type,
                        serializableAttribute.SerializedType.GetMethodKeyName()
                    );
                    if (!parserGenContext.TryAddSerializedType(key, serializableAttribute))
                    {
                        parserGenContext.ReportDiagnostic(
                            DiagnosticMessages.CreateSerializableAlreadySpecified(
                                serializableAttribute.GetLocation(),
                                serializableAttribute.SerializedType.GetFullName()
                            )
                        );
                    }
                }
            }
        }

        var resultTypes = FlushSerializedTypes(parserGenContext, cancellationToken);

        var serializeAnonymousAttributes = JsonSerializeAnonymousAttributeGenerator
            .FilterAttributes(typeDeclaration, semanticModel, cancellationToken)
            .ToList();
        if (serializeAnonymousAttributes.Count != 0)
        {
            foreach (var serializeAnonymousAttribute in serializeAnonymousAttributes)
            {
                cancellationToken.ThrowIfCancellationRequested();
                var generator = JsonSerializeAnonymousAttributeGenerator.GetAttributes(
                    serializeAnonymousAttribute,
                    semanticModel,
                    cancellationToken
                );
                if (generator != null)
                {
                    var key = new MethodReferenceKey(SerializableAttributeType.JsonAnonymous, string.Empty);
                    if (resultTypes.ContainsKey(key))
                    {
                        parserGenContext.ReportDiagnostic(
                            DiagnosticMessages.CreateSerializableAlreadySpecified(
                                generator.GetLocation(),
                                "JsonSerializeAnonymousAttribute"
                            )
                        );
                    }

                    resultTypes.Add(key, generator);
                }
            }
        }

        var classSymbol =
            semanticModel.GetDeclaredSymbol(typeDeclaration, cancellationToken)
            ?? throw new Exception($"Couldn't get symbol for {typeDeclaration}.");
        var cacheableClassSymbol = CacheableTypeDeclarationSymbol.FromTypeSymbol(
            classSymbol,
            typeDeclaration,
            cancellationToken
        );
        return new CollectedParser(cacheableClassSymbol, resultTypes, parserGenContext.Diagnostics);
    }

    private static CollectedParser? GatherRoutes(
        GeneratorSyntaxContext context,
        MethodDeclarationSyntax methodDeclaration,
        CancellationToken cancellationToken
    )
    {
        var semanticModel = context.SemanticModel;
        var parserGenContext = new SerializableAttributesGeneratorContext(semanticModel);

        if (methodDeclaration.Parent is not TypeDeclarationSyntax typeDeclaration)
        {
            return null;
        }

        if (!typeDeclaration.Modifiers.Any(m => m.IsKind(SyntaxKind.PartialKeyword)))
        {
            parserGenContext.ReportDiagnostic(
                DiagnosticMessages.CreateSerializableAttributeClassIsNotPartial(
                    typeDeclaration.GetLocation(),
                    typeDeclaration.Identifier.Text
                )
            );
        }

        var routeSerializableAttribute = SerializableAttribute.FilterRouteAttributes(
            parserGenContext,
            methodDeclaration,
            semanticModel,
            cancellationToken
        );
        if (routeSerializableAttribute == null)
        {
            return null;
        }

        var key = new MethodReferenceKey(
            routeSerializableAttribute.Type,
            routeSerializableAttribute.SerializedType.GetMethodKeyName()
        );
        if (!parserGenContext.TryAddSerializedType(key, routeSerializableAttribute))
        {
            parserGenContext.ReportDiagnostic(
                DiagnosticMessages.CreateSerializableAlreadySpecified(
                    routeSerializableAttribute.GetLocation(),
                    routeSerializableAttribute.SerializedType.GetFullName()
                )
            );
        }

        var resultTypes = FlushSerializedTypes(parserGenContext, cancellationToken);
        var classSymbol =
            semanticModel.GetDeclaredSymbol(typeDeclaration, cancellationToken)
            ?? throw new Exception($"Couldn't get symbol for {typeDeclaration}.");

        var cacheableClassSymbol = CacheableTypeDeclarationSymbol.FromTypeSymbol(
            classSymbol,
            methodDeclaration,
            cancellationToken
        );
        return new CollectedParser(cacheableClassSymbol, resultTypes, parserGenContext.Diagnostics);
    }

    private static Dictionary<MethodReferenceKey, ISerializableGenerator> FlushSerializedTypes(
        SerializableAttributesGeneratorContext context,
        CancellationToken cancellationToken
    )
    {
        var resultTypes = new Dictionary<MethodReferenceKey, ISerializableGenerator>();
        while (context.TryDequeueSerializedType(out var pendingAttribute))
        {
            cancellationToken.ThrowIfCancellationRequested();
            var (methodReferenceKey, serializableAttribute) = pendingAttribute;

            ISerializableGenerator? generator;
            switch (serializableAttribute.Type)
            {
                case SerializableAttributeType.Enum:
                {
                    generator = EnumSerializableGenerator.Prepare(context, serializableAttribute, cancellationToken);
                    break;
                }

                case SerializableAttributeType.QueryString:
                {
                    generator = QueryStringSerializableGenerator.Prepare(
                        context,
                        serializableAttribute,
                        cancellationToken
                    );
                    break;
                }

                case SerializableAttributeType.CommandLine:
                {
                    generator = CommandLineSerializableGenerator.Prepare(
                        context,
                        serializableAttribute,
                        cancellationToken
                    );
                    break;
                }

                case SerializableAttributeType.Json:
                {
                    generator = JsonSerializableGenerator.Prepare(context, serializableAttribute, cancellationToken);
                    break;
                }

                case SerializableAttributeType.JsonAnonymous:
                {
                    throw new NotSupportedException("Cannot flush an anonymous type.");
                }

                case SerializableAttributeType.Unknown:
                default:
                {
                    throw new NotImplementedException(
                        $"Not implemented type SerializableAttributeType.{serializableAttribute.Type}."
                    );
                }
            }

            if (generator == null)
            {
                continue;
            }

            resultTypes.Add(methodReferenceKey, generator);
        }

        return resultTypes;
    }

    private static IEnumerable<SerializableAttributesExecutor?> Combine(
        ImmutableArray<CollectedParser?> array,
        CancellationToken cancellationToken
    )
    {
        var set = new Dictionary<string, CollectedParser>();
        foreach (var item in array)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (item == null)
            {
                continue;
            }

            var fullName = item.TypeDeclarationSymbol.FullName;
            if (!set.TryGetValue(fullName, out var originalItem))
            {
                set.Add(fullName, item);
                continue;
            }

            foreach (var pair in item.Generators)
            {
                var methodReferenceKey = pair.Key;
                var generator = pair.Value;

                if (originalItem.Generators.TryGetValue(methodReferenceKey, out var originalGenerator))
                {
                    try
                    {
                        var combinedGenerator = originalGenerator.Combine(originalItem.Diagnostics.Add, generator);
                        originalItem.Generators[methodReferenceKey] = combinedGenerator;
                    }
                    catch (Exception ex)
                    {
                        originalItem.Diagnostics.Add(DiagnosticMessages.CreateGenericException(ex));
                    }
                }
                else
                {
                    originalItem.Generators.Add(methodReferenceKey, pair.Value);
                }

                originalItem.Diagnostics.AddRange(item.Diagnostics);
            }
        }

        foreach (var item in set)
        {
            var value = item.Value;
            var generators = value.Generators.Values.ToList();
            if (generators.Count != 0)
            {
                var methodReferences = generators[0].MethodReferences;

                foreach (var generator in generators)
                {
                    if (generator != generators[0])
                    {
                        foreach (var reference in generator.MethodReferences)
                        {
                            if (methodReferences.TryGetValue(reference.Key, out var methodReference))
                            {
                                if (methodReference.IsImplicitlyGenerated == reference.Value.IsImplicitlyGenerated)
                                {
                                    // Neither are implicitly generated, make sure they're equivalent. Set to max visibility.
                                    var comparison = reference.Value;
                                    if (comparison.ParsingType != methodReference.ParsingType)
                                    {
                                        value.Diagnostics.Add(
                                            DiagnosticMessages.CreateSerializableDifferentProperties(
                                                value.TypeDeclarationSymbol.GetLocation(),
                                                methodReference.ParsingType,
                                                nameof(comparison.ParsingType),
                                                methodReference.ParsingType,
                                                comparison.ParsingType
                                            )
                                        );
                                    }

                                    if (comparison.SerializingMethodName != methodReference.SerializingMethodName)
                                    {
                                        value.Diagnostics.Add(
                                            DiagnosticMessages.CreateSerializableDifferentProperties(
                                                value.TypeDeclarationSymbol.GetLocation(),
                                                methodReference.ParsingType,
                                                nameof(comparison.SerializingMethodName),
                                                methodReference.SerializingMethodName,
                                                comparison.SerializingMethodName
                                            )
                                        );
                                    }

                                    if (comparison.ParsingMethodName != methodReference.ParsingMethodName)
                                    {
                                        value.Diagnostics.Add(
                                            DiagnosticMessages.CreateSerializableDifferentProperties(
                                                value.TypeDeclarationSymbol.GetLocation(),
                                                methodReference.ParsingType,
                                                nameof(comparison.ParsingMethodName),
                                                methodReference.ParsingMethodName,
                                                comparison.ParsingMethodName
                                            )
                                        );
                                    }

                                    if (comparison.CustomParsingMethodName != methodReference.CustomParsingMethodName)
                                    {
                                        value.Diagnostics.Add(
                                            DiagnosticMessages.CreateSerializableDifferentProperties(
                                                value.TypeDeclarationSymbol.GetLocation(),
                                                methodReference.ParsingType,
                                                nameof(comparison.CustomParsingMethodName),
                                                methodReference.CustomParsingMethodName,
                                                comparison.CustomParsingMethodName
                                            )
                                        );
                                    }

                                    if (
                                        comparison.CustomSerializingMethodName
                                        != methodReference.CustomSerializingMethodName
                                    )
                                    {
                                        value.Diagnostics.Add(
                                            DiagnosticMessages.CreateSerializableDifferentProperties(
                                                value.TypeDeclarationSymbol.GetLocation(),
                                                methodReference.ParsingType,
                                                nameof(comparison.CustomSerializingMethodName),
                                                methodReference.CustomSerializingMethodName,
                                                comparison.CustomSerializingMethodName
                                            )
                                        );
                                    }

                                    methodReferences[reference.Key] = new MethodReference(
                                        methodReference.ParsingType,
                                        methodReference.ParsingMethodName,
                                        methodReference.SerializingMethodName,
                                        methodReference.Visibility.Max(comparison.Visibility),
                                        methodReference.IsImplicitlyGenerated
                                    );
                                }
                                else
                                {
                                    // The two are not equivalent. Set to the explicit one.
                                    methodReferences[reference.Key] = methodReference.IsImplicitlyGenerated
                                        ? reference.Value
                                        : methodReference;
                                }
                            }
                            else
                            {
                                methodReferences.Add(reference.Key, reference.Value);
                            }
                        }

                        generator.MethodReferences = methodReferences;
                    }

                    generator.PrepareToGenerate(value.Generators);
                }
            }

            yield return new SerializableAttributesExecutor(value.TypeDeclarationSymbol, generators, value.Diagnostics);
        }
    }

    private sealed class CollectedParser
    {
        public CacheableTypeDeclarationSymbol TypeDeclarationSymbol { get; }

        public Dictionary<MethodReferenceKey, ISerializableGenerator> Generators { get; }

        public List<Diagnostic> Diagnostics { get; }

        public CollectedParser(
            CacheableTypeDeclarationSymbol typeDeclarationSymbol,
            Dictionary<MethodReferenceKey, ISerializableGenerator> generators,
            List<Diagnostic> diagnostics
        )
        {
            this.TypeDeclarationSymbol = typeDeclarationSymbol;
            this.Generators = generators;
            this.Diagnostics = diagnostics;
        }
    }
}
