// <copyright file="JsonSerializableGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Serializable;

/// <summary>
/// Generator for JsonSerializable.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.OrderingRules",
    "SA1202:Elements should be ordered by access",
    Justification = "These classes are complicated."
)]
[SuppressMessage(
    "StyleCop.CSharp.OrderingRules",
    "SA1204:Static elements should appear before instance elements",
    Justification = "Static is interchangeable."
)]
public sealed partial class JsonSerializableGenerator : ISerializableGenerator, IEquatable<JsonSerializableGenerator>
{
    /// <inheritdoc />
    public Dictionary<MethodReferenceKey, MethodReference> MethodReferences { get; set; }

    /// <inheritdoc />
    public SerializableAttributesExecutor ParentExecutor { get; set; } = null!;

    private readonly CacheableSerializableAttribute serializableAttribute;
    private readonly JsonSerializableSettingsAttribute jsonSerializableSettingsAttribute;
    private readonly CacheableTypeSymbol serializedType;
    private readonly PropertyMember[] members;

    private JsonSerializableGenerator(
        CacheableSerializableAttribute serializableAttribute,
        CacheableTypeSymbol serializedType,
        JsonSerializableSettingsAttribute jsonSerializableSettingsAttribute,
        PropertyMember[] members,
        Dictionary<MethodReferenceKey, MethodReference> methodReferences
    )
    {
        this.serializedType = serializedType;
        this.serializableAttribute = serializableAttribute;
        this.jsonSerializableSettingsAttribute = jsonSerializableSettingsAttribute;
        this.members = members;
        this.MethodReferences = methodReferences;
        this.Visibility = serializableAttribute.Visibility;
    }

    /// <inheritdoc />
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Prepares a new instance of <see cref="JsonSerializableGenerator"/> using semantic model or null if there was an error.
    /// </summary>
    /// <param name="context">Context for compilation.</param>
    /// <param name="serializableAttribute"><see cref="Serializable.SerializableAttribute"/> that initiated parsing this json struct.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The <see cref="JsonSerializableGenerator"/> or null if there was an error.</returns>
    public static JsonSerializableGenerator? Prepare(
        SerializableAttributesGeneratorContext context,
        SerializableAttribute serializableAttribute,
        CancellationToken cancellationToken
    )
    {
        var serializedType = serializableAttribute.SerializedType;
        SerializableAttributesExecutor.GetNullableType(serializedType, out var baseType, out _);
#pragma warning disable IDE0010
        switch (baseType.TypeKind)
#pragma warning restore IDE0010
        {
            case TypeKind.Array:
            case TypeKind.Class:
            case TypeKind.Struct:
            case TypeKind.Enum:
            case TypeKind.Interface:
            {
                // Supported type.
                break;
            }

            case TypeKind.Error:
            {
                // Compiler handles this.
                return null;
            }

            default:
            {
                context.ReportDiagnostic(
                    DiagnosticMessages.CreateJsonSerializableTypeInvalid(
                        serializableAttribute.AttributeSyntax.GetLocation(),
                        baseType.GetFullName(),
                        baseType.TypeKind
                    )
                );
                return null;
            }
        }

        var members = PropertyMember.GetMembers(context, baseType).ToList();
        cancellationToken.ThrowIfCancellationRequested();

        _ = JsonSerializableSettingsAttribute.TryGet(baseType, out var jsonSerializableSettingsAttribute);
        jsonSerializableSettingsAttribute ??= new JsonSerializableSettingsAttribute();

        var methodReferences = new Dictionary<MethodReferenceKey, MethodReference>();
        var duplicateNames = new HashSet<string>();
        foreach (var member in members)
        {
            var names = member.member.Names;
            if (names.Count == 0)
            {
                continue;
            }

            if (names.Count != 1)
            {
                context.ReportDiagnostic(
                    DiagnosticMessages.CreateJsonSerializablePropertyHasMultipleNames(
                        serializableAttribute.AttributeSyntax.GetLocation(),
                        serializableAttribute.SerializedType.GetFullName(),
                        member.symbol.Name
                    )
                );
            }

            foreach (var name in names)
            {
                if (!duplicateNames.Add(name))
                {
                    context.ReportDiagnostic(
                        DiagnosticMessages.CreateJsonSerializableDuplicateName(
                            serializableAttribute.GetLocation(),
                            baseType.GetFullName(),
                            name
                        )
                    );

                    return null;
                }
            }

            if (
                member.member.CustomPropertyAttribute is
                { ParsingMethodName: not null, SerializingMethodName: not null }
            )
            {
                // Both custom parsing and serializing are set, don't generate info.
                // If only one is covered, go through the full process because I'm not sure what that scenario would be
                // and it seems better to over generate and over warn.
                continue;
            }

            var memberType = member.member.Type;
            if (!TryAddType(context, serializableAttribute, methodReferences, memberType, member.symbol.Type))
            {
                return null;
            }
        }

        var cacheableSerializableAttribute = CacheableSerializableAttribute.FromSerializableAttribute(
            context,
            serializableAttribute,
            cancellationToken
        );
        var cacheableSerializedType = CacheableTypeSymbol.FromITypeSymbol(
            context.SymbolCache,
            context.ReportDiagnostic,
            serializedType
        );

        if (!TryAddType(context, serializableAttribute, methodReferences, cacheableSerializedType, serializedType))
        {
            return null;
        }

        return new JsonSerializableGenerator(
            cacheableSerializableAttribute,
            cacheableSerializedType,
            jsonSerializableSettingsAttribute,
            members.Select(p => p.member).ToArray(),
            methodReferences
        );

        static bool TryAddType(
            SerializableAttributesGeneratorContext context,
            SerializableAttribute serializableAttribute,
            Dictionary<MethodReferenceKey, MethodReference> methodReferences,
            CacheableTypeSymbol type,
            ITypeSymbol typeSymbol
        )
        {
            if (!SerializableAttributesExecutor.IsBuiltInType(type))
            {
                var key = new MethodReferenceKey(SerializableAttributeType.Json, type.MethodKeyName);
                if (
                    !context.TryGetSerializedType(key, out var memberSerializableAttribute)
                    || (!memberSerializableAttribute.GenerateParse && serializableAttribute.GenerateParse)
                    || (!memberSerializableAttribute.GenerateSerialize && serializableAttribute.GenerateSerialize)
                )
                {
                    var attributeDisabledParsing = false;
                    var attributeDisabledSerialization = false;

                    if (memberSerializableAttribute != null)
                    {
                        attributeDisabledParsing =
                            !memberSerializableAttribute.GenerateParse && serializableAttribute.GenerateParse;
                        attributeDisabledSerialization =
                            !memberSerializableAttribute.GenerateSerialize && serializableAttribute.GenerateSerialize;
                    }

                    memberSerializableAttribute = Serializable.SerializableAttribute.FromProperty(
                        context.SymbolCache,
                        context.ReportDiagnostic,
                        key.Type,
                        serializableAttribute,
                        typeSymbol,
                        serializableAttribute.GenerateParse,
                        serializableAttribute.GenerateSerialize
                    );
                    if (
                        !context.TryAddSerializedType(
                            key,
                            memberSerializableAttribute,
                            serializableAttribute.SerializedType
                        )
                    )
                    {
                        // Type was already added and we failed to generate both parse and serialize.
                        if (attributeDisabledParsing)
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateJsonSerializableTypeParsingDisabled(
                                    serializableAttribute.AttributeSyntax.GetLocation(),
                                    memberSerializableAttribute.SerializedType.GetFullName(),
                                    serializableAttribute.SerializedType.GetFullName()
                                )
                            );
                        }

                        if (attributeDisabledSerialization)
                        {
                            context.ReportDiagnostic(
                                DiagnosticMessages.CreateJsonSerializableTypeSerializationDisabled(
                                    serializableAttribute.AttributeSyntax.GetLocation(),
                                    memberSerializableAttribute.SerializedType.GetFullName(),
                                    serializableAttribute.SerializedType.GetFullName()
                                )
                            );
                        }

                        return false;
                    }
                }

                if (methodReferences.ContainsKey(key))
                {
                    return true;
                }

                var parsingMethodName = memberSerializableAttribute.ParsingMethodName;
                var parsingType = type.NonNullableType.FullNameNonNullable;
                if (!type.NonNullableType.IsValueType)
                {
                    parsingType += "?";
                }

                var serializingMethodName = memberSerializableAttribute.SerializingMethodName;

                methodReferences.Add(
                    key,
                    new MethodReference(
                        parsingType,
                        parsingMethodName,
                        serializingMethodName,
                        memberSerializableAttribute.Visibility,
                        memberSerializableAttribute.IsImplicitlyGenerated
                    )
                );
            }

            if (type.ArrayElementType != null)
            {
                if (
                    !TryAddType(
                        context,
                        serializableAttribute,
                        methodReferences,
                        type.ArrayElementType,
                        ((IArrayTypeSymbol)typeSymbol).ElementType
                    )
                )
                {
                    return false;
                }
            }

            if (type.TypeArguments.Length != 0)
            {
                var namedTypeSymbol = (INamedTypeSymbol)typeSymbol;
                for (var index = 0; index < type.TypeArguments.Length; index++)
                {
                    var typeArgument = type.TypeArguments[index];
                    var typeArgumentSymbol = namedTypeSymbol.TypeArguments[index];
                    if (!TryAddType(context, serializableAttribute, methodReferences, typeArgument, typeArgumentSymbol))
                    {
                        return false;
                    }
                }
            }

            if (SerializableAttributesExecutor.FindGenericStringDictionary(type) != null)
            {
                var dictionaryType = SerializableAttributesExecutor.FindGenericStringDictionary(type)!;
                var dictionaryTypeSymbol = SerializableAttributesExecutor.FindGenericStringDictionary(typeSymbol)!;
                Debug.Assert(dictionaryType.FullName == dictionaryTypeSymbol.GetFullName(), "Types should match.");
                if (!TryAddType(context, serializableAttribute, methodReferences, dictionaryType, dictionaryTypeSymbol))
                {
                    return false;
                }
            }
            else if (PropertyMember.IsIEnumerable(type))
            {
                var enumerableType = type.FindInterface("global::System.Collections.Generic.IEnumerable<T>")!;
                var enumerableTypeSymbol =
                    FindInterface(typeSymbol, "global::System.Collections.Generic.IEnumerable<T>")
                    ?? FindInterface(typeSymbol, "global::System.Collections.Generic.IEnumerable<T>")!;

                Debug.Assert(enumerableType.FullName == enumerableTypeSymbol.GetFullName(), "Types should match.");

                var arrayElementType = enumerableTypeSymbol.TypeArguments[0];
                if (arrayElementType is INamedTypeSymbol namedArrayElementType)
                {
                    if (
                        namedArrayElementType.ConstructedFrom.GetFullName()
                            == "global::System.Collections.Generic.KeyValuePair<TKey, TValue>"
                        && namedArrayElementType.TypeArguments[0].GetFullName() == "string"
                    )
                    {
                        // Dictionary type.
                        if (
                            !TryAddType(
                                context,
                                serializableAttribute,
                                methodReferences,
                                enumerableType.TypeArguments[0].TypeArguments[1],
                                namedArrayElementType.TypeArguments[1]
                            )
                        )
                        {
                            return false;
                        }

                        return true;
                    }
                }

                if (
                    !TryAddType(
                        context,
                        serializableAttribute,
                        methodReferences,
                        enumerableType.TypeArguments[0],
                        arrayElementType
                    )
                )
                {
                    return false;
                }

                return true;
            }

            return true;

            static INamedTypeSymbol? FindInterface(ITypeSymbol symbol, string name)
            {
                if (symbol is INamedTypeSymbol namedTypeSymbol)
                {
                    if (namedTypeSymbol.ConstructedFrom.GetFullName() == name)
                    {
                        return namedTypeSymbol;
                    }
                }

                return symbol.AllInterfaces.FirstOrDefault(i => i.ConstructedFrom.GetFullName() == name);
            }
        }
    }

    /// <summary>
    /// Adds this type to anonymous serialization.
    /// </summary>
    /// <param name="builder">The builder to write to.</param>
    public void AddToAnonymousSerializer(CodeBuilder builder)
    {
        var type = this.serializedType.FullNameNonNullable;
        var methodName = this.serializableAttribute.SerializingMethodName;
        var visibility = this.serializableAttribute.Visibility;
        builder.AppendLine($"serializer.AddSerializer<{type}>({this.GetMethodName(methodName, visibility)});");
    }

    /// <inheritdoc />
    public ISerializableGenerator Combine(Action<Diagnostic> reportDiagnostic, ISerializableGenerator otherGenerator)
    {
        if (otherGenerator is not JsonSerializableGenerator other)
        {
            throw new NotImplementedException($"Should never combine mismatched {nameof(ISerializableGenerator)}.");
        }

        if (other.serializableAttribute.IsImplicitlyGenerated && this.serializableAttribute.IsImplicitlyGenerated)
        {
            // Both implicitly generated.
            var serializableAttribute = new CacheableSerializableAttribute(
                this.serializableAttribute.Type,
                this.serializableAttribute.IsStatic && other.serializableAttribute.IsStatic,
                this.serializableAttribute.DeclaringType,
                this.serializableAttribute.SerializedType,
                this.serializableAttribute.ParsingMethodName,
                this.serializableAttribute.SerializingMethodName,
                this.serializableAttribute.CustomParsingMethodName,
                this.serializableAttribute.CustomSerializingMethodName,
                true,
                this.serializableAttribute.IsSelfReference || other.serializableAttribute.IsSelfReference,
                this.serializableAttribute.GenerateParse || other.serializableAttribute.GenerateParse,
                this.serializableAttribute.GenerateSerialize || other.serializableAttribute.GenerateSerialize,
                this.serializableAttribute.CacheableLocation,
                this.serializableAttribute.AliasedTypedefType ?? other.serializableAttribute.AliasedTypedefType,
                this.serializableAttribute.Visibility.Max(other.serializableAttribute.Visibility)
            );

            return new JsonSerializableGenerator(
                serializableAttribute,
                this.serializedType,
                this.jsonSerializableSettingsAttribute,
                this.members,
                this.MethodReferences
            );
        }

        if (!this.serializableAttribute.IsImplicitlyGenerated && !other.serializableAttribute.IsImplicitlyGenerated)
        {
            // Neither is implicitly generated, just error. No need to validate they are equal.
            reportDiagnostic(
                DiagnosticMessages.CreateJsonSerializableDuplicateTypeDiffers(
                    this.serializableAttribute.GetLocation(),
                    this.serializableAttribute.SerializedType.FullName
                )
            );
            return this;
        }

        // One is implicitly generated, keep the non implicitly generated one and put up a warning if generation is not the same.
        var implicitAttribute = this.serializableAttribute.IsImplicitlyGenerated
            ? this.serializableAttribute
            : other.serializableAttribute;
        var explicitAttribute = !this.serializableAttribute.IsImplicitlyGenerated
            ? this.serializableAttribute
            : other.serializableAttribute;

        if (
            (implicitAttribute.GenerateParse && !explicitAttribute.GenerateParse)
            || (implicitAttribute.GenerateSerialize && !explicitAttribute.GenerateSerialize)
        )
        {
            // Generate parse and serialization is not the same.
            reportDiagnostic(
                DiagnosticMessages.CreateJsonSerializableSourceGenerationInvalid(
                    this.serializableAttribute.GetLocation(),
                    this.serializableAttribute.SerializedType.FullName,
                    implicitAttribute.GenerateParse && !explicitAttribute.GenerateParse ? "parsing" : "serializing"
                )
            );
        }

        return this.serializableAttribute.IsImplicitlyGenerated ? other : this;
    }

    /// <inheritdoc />
    public void PrepareToGenerate(Dictionary<MethodReferenceKey, ISerializableGenerator> generators)
    {
        foreach (var pair in generators)
        {
            var generator = pair.Value;

            if (generator is JsonSerializableGenerator jsonSerializableGenerator)
            {
                FixupJsonSerializableGenerator(jsonSerializableGenerator.MethodReferences, generators);
            }
        }

        static void FixupJsonSerializableGenerator(
            Dictionary<MethodReferenceKey, MethodReference> methodReferences,
            Dictionary<MethodReferenceKey, ISerializableGenerator> generators
        )
        {
            foreach (var methodReference in methodReferences)
            {
                var key = methodReference.Key;
                var value = methodReference.Value;
                if (!generators.TryGetValue(key, out var generator))
                {
                    continue;
                }

                if (generator is not JsonSerializableGenerator methodGenerator)
                {
                    continue;
                }

                value.CustomParsingMethodName = methodGenerator.serializableAttribute.CustomParsingMethodName;
                value.CustomSerializingMethodName = methodGenerator.serializableAttribute.CustomSerializingMethodName;
            }
        }
    }

    /// <inheritdoc />
    public bool Generate(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        if (this.serializableAttribute.IsSelfReference)
        {
            // Self reference attributes do not generate.
            return true;
        }

        try
        {
            var generateParse = this.serializableAttribute.GenerateParse;
            if (this.serializedType.TypeKind == TypeKind.Interface)
            {
                generateParse = false;
            }

            var visibility = this.serializableAttribute.Visibility switch
            {
                SerializableVisibility.Public => "public",
                SerializableVisibility.Private => "private",
                SerializableVisibility.File => "public",
                _ => "public",
            };
            if (generateParse)
            {
                var returnType =
                    $"{this.serializedType.FullNameNonNullable}{(this.serializedType.IsValueType ? string.Empty : "?")}";
                var notNullWhenTrue = !this.serializedType.IsValueType;
                this.GenerateJsonParseWrappers(builder, visibility, notNullWhenTrue, returnType);

                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"{visibility} static bool {this.serializableAttribute.ParsingMethodName}(ref global::System.Text.Json.Utf8JsonReader reader, {(notNullWhenTrue ? "[global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] " : string.Empty)}out {returnType} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)"
                );
                using (builder.AppendBlock())
                {
                    this.GenerateJsonParseMethodBody(reportDiagnostic, builder);
                }
            }

            var isGeneratingSelf = this.serializedType.FullName == this.serializableAttribute.DeclaringType.FullName;
            var generateInstanceSerialize = isGeneratingSelf && !this.serializableAttribute.IsStatic;
            if (this.serializableAttribute.GenerateSerialize || generateInstanceSerialize)
            {
                var serializedTypeFullName = this.serializedType.FullNameNonNullable;
                this.GenerateJsonSerializeWrappers(builder, visibility, serializedTypeFullName);

                builder.AppendAndSplitLines(
                    """
                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    """
                );
                builder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
                builder.AppendLine(
                    $"{visibility} static void {this.serializableAttribute.SerializingMethodName}(global::System.Text.Json.Utf8JsonWriter writer, {serializedTypeFullName} value)"
                );
                using (builder.AppendBlock())
                {
                    this.GenerateJsonSerializeMethodBody(reportDiagnostic, builder);
                }

                if (generateInstanceSerialize)
                {
                    this.GenerateJsonSerializeInstanceWrappers(builder, visibility);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            reportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            return false;
        }
    }

    private void GenerateJsonParseWrappers(
        CodeBuilder builder,
        string visibility,
        bool notNullWhenTrue,
        string serializedTypeFullName
    )
    {
        builder.AppendAndSplitLines(
            $$"""
            /// <summary>
            /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
            /// </summary>
            /// <param name="json">A raw json string to parse.</param>
            /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
            /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
            /// <returns>True if parsing was successful, false if it was not.</returns>
            /// <remarks>
            /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
            ///
            /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
            /// force initialization of value type fields.
            /// </remarks>
            [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
            {{visibility}} static bool {{this.serializableAttribute.ParsingMethodName}}(string json, {{(
                notNullWhenTrue ? "[global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] " : string.Empty
            )}}out {{serializedTypeFullName}} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
            {
                using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                return {{this.serializableAttribute.ParsingMethodName}}(rental.AsSpan(0, byteCount), out result, out error);
            }

            /// <summary>
            /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
            /// </summary>
            /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
            /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
            /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
            /// <returns>True if parsing was successful, false if it was not.</returns>
            /// <remarks>
            /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
            ///
            /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
            /// force initialization of value type fields.
            /// </remarks>
            [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
            {{visibility}} static bool {{this.serializableAttribute.ParsingMethodName}}(global::System.ReadOnlySpan<byte> utf8, {{(
                notNullWhenTrue ? "[global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] " : string.Empty
            )}}out {{serializedTypeFullName}} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
            {
                var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                return {{this.serializableAttribute.ParsingMethodName}}(ref reader, out result, out error);
            }

            /// <summary>
            /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
            /// </summary>
            /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
            /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
            /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
            /// <returns>True if parsing was successful, false if it was not.</returns>
            /// <remarks>
            /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
            ///
            /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
            /// force initialization of value type fields.
            /// </remarks>
            [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
            {{visibility}} static bool {{this.serializableAttribute.ParsingMethodName}}(global::System.Buffers.ReadOnlySequence<byte> utf8, {{(
                notNullWhenTrue ? "[global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] " : string.Empty
            )}}out {{serializedTypeFullName}} result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
            {
                var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                return {{this.serializableAttribute.ParsingMethodName}}(ref reader, out result, out error);
            }

            """
        );
    }

    private void GenerateJsonSerializeWrappers(CodeBuilder builder, string visibility, string serializedTypeFullName)
    {
        builder.AppendAndSplitLines(
            $$"""
            /// <summary>
            /// Asynchronously serialize an object to a stream.
            /// </summary>
            /// <param name="stream">The stream to write to.</param>
            /// <param name="value">The object to serialize.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            /// <param name="cancellationToken">Cancellation token.</param>
            /// <returns>void.</returns>
            {{visibility}} static async global::System.Threading.Tasks.Task {{this.serializableAttribute.SerializingMethodName}}Async(global::System.IO.Stream stream, {{serializedTypeFullName}} value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
            {
                await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                {{this.serializableAttribute.SerializingMethodName}}(writer, value);
                await writer.FlushAsync(cancellationToken);
            }

            /// <summary>
            /// Asynchronously serialize an object to a stream.
            /// </summary>
            /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
            /// <param name="value">The object to serialize.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            /// <param name="cancellationToken">Cancellation token.</param>
            /// <returns>void.</returns>
            {{visibility}} static async global::System.Threading.Tasks.Task {{this.serializableAttribute.SerializingMethodName}}Async(global::System.Buffers.IBufferWriter<byte> bufferWriter, {{serializedTypeFullName}} value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
            {
                await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                {{this.serializableAttribute.SerializingMethodName}}(writer, value);
                await writer.FlushAsync(cancellationToken);
            }

            /// <summary>
            /// Serialize an object to a string.
            /// </summary>
            /// <param name="value">The object to serialize.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            /// <returns>Json encoded string value.</returns>
            {{visibility}} static string {{this.serializableAttribute.SerializingMethodName}}({{serializedTypeFullName}} value, global::System.Text.Json.JsonWriterOptions options = default)
            {
                using var memoryStream = new global::System.IO.MemoryStream();
                {{this.serializableAttribute.SerializingMethodName}}(memoryStream, value, options);
                return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
            }

            /// <summary>
            /// Serialize an object to a stream.
            /// </summary>
            /// <param name="stream">The stream to write to.</param>
            /// <param name="value">The object to serialize.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            {{visibility}} static void {{this.serializableAttribute.SerializingMethodName}}(global::System.IO.Stream stream, {{serializedTypeFullName}} value, global::System.Text.Json.JsonWriterOptions options = default)
            {
                using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                {{this.serializableAttribute.SerializingMethodName}}(writer, value);
                writer.Flush();
            }

            /// <summary>
            /// Serialize an object to a stream.
            /// </summary>
            /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
            /// <param name="value">The object to serialize.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            {{visibility}} static void {{this.serializableAttribute.SerializingMethodName}}(global::System.Buffers.IBufferWriter<byte> bufferWriter, {{serializedTypeFullName}} value, global::System.Text.Json.JsonWriterOptions options = default)
            {
                using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                {{this.serializableAttribute.SerializingMethodName}}(writer, value);
                writer.Flush();
            }
            
            """
        );
    }

    private void GenerateJsonSerializeInstanceWrappers(CodeBuilder builder, string visibility)
    {
        builder.AppendAndSplitLines(
            $$""""
            /// <summary>
            /// Asynchronously serialize an object to a stream.
            /// </summary>
            /// <param name="stream">The stream to write to.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            /// <param name="cancellationToken">Cancellation token.</param>
            /// <returns>void.</returns>
            {{visibility}} global::System.Threading.Tasks.Task {{this.serializableAttribute.SerializingMethodName}}Async(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                => {{this.serializableAttribute.SerializingMethodName}}Async(stream, this, options);

            /// <summary>
            /// Asynchronously serialize an object to a stream.
            /// </summary>
            /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            /// <param name="cancellationToken">Cancellation token.</param>
            /// <returns>void.</returns>
            {{visibility}} global::System.Threading.Tasks.Task {{this.serializableAttribute.SerializingMethodName}}Async(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                => {{this.serializableAttribute.SerializingMethodName}}Async(bufferWriter, this, options);

            /// <summary>
            /// Serialize an object to a string.
            /// </summary>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            /// <returns>Json encoded string value.</returns>
            {{visibility}} string {{this.serializableAttribute.SerializingMethodName}}(global::System.Text.Json.JsonWriterOptions options = default)
                => {{this.serializableAttribute.SerializingMethodName}}(this, options);

            /// <summary>
            /// Serialize an object to a stream.
            /// </summary>
            /// <param name="stream">The stream to write to.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            {{visibility}} void {{this.serializableAttribute.SerializingMethodName}}(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                => {{this.serializableAttribute.SerializingMethodName}}(stream, this, options);

            /// <summary>
            /// Serialize an object to a stream.
            /// </summary>
            /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
            /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
            {{visibility}} void {{this.serializableAttribute.SerializingMethodName}}(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                => {{this.serializableAttribute.SerializingMethodName}}(bufferWriter, this, options);

            /// <summary>
            /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
            /// </summary>
            /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
            [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
            {{visibility}} void {{this.serializableAttribute.SerializingMethodName}}(global::System.Text.Json.Utf8JsonWriter writer)
                => {{this.serializableAttribute.SerializingMethodName}}(writer, this);
            """"
        );
    }

    private void GenerateJsonParseMethodBody(Action<Diagnostic> reportDiagnostic, CodeBuilder builder)
    {
        var serializedType = this.serializableAttribute.SerializedType;
        var baseType = serializedType.NonNullableType;

        builder.AppendLine("try");
        using (builder.AppendBlock())
        {
            builder.AppendLine(
                "if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())"
            );
            using (builder.AppendBlock())
            {
                SerializableAttributesExecutor.ReturnError(builder, "Cannot parse empty string.");
            }

            if (this.serializableAttribute.CustomParsingMethodName != null)
            {
                builder.AppendLine(
                    $"return {this.GetMethodName(this.serializableAttribute.CustomParsingMethodName, this.serializableAttribute.Visibility)}(ref reader, out result, out error);"
                );
            }
            else if (this.serializableAttribute.AliasedTypedefType != null)
            {
                var aliasedTypedefType = this.serializableAttribute.AliasedTypedefType;
                if (IsHandledAsJsonIndividualElement(aliasedTypedefType, true))
                {
                    var returnType = baseType.FullNameNonNullable;
                    var value = this.ParseIndividualJsonElement(
                        builder,
                        null,
                        PropertyMember.FromType(aliasedTypedefType),
                        aliasedTypedefType
                    );

                    builder.AppendLine($"result = ({returnType}){value};");
                    builder.AppendLine("error = default;");
                    builder.AppendLine("return true;");
                }
                else if (
                    this.MethodReferences.TryGetValue(
                        new MethodReferenceKey(SerializableAttributeType.Json, aliasedTypedefType.MethodKeyName),
                        out var aliasedMethodReference
                    )
                )
                {
                    // Try to inject nullable instead of failing below due to no names.
                    var value = builder.GetVariableName("output");
                    var returnType =
                        $"{aliasedTypedefType.FullNameNonNullable}{(aliasedTypedefType.IsValueType ? string.Empty : "?")}";
                    if (aliasedMethodReference.CustomParsingMethodName != null)
                    {
                        builder.AppendLine(
                            $"if (!{this.GetMethodName(aliasedMethodReference.CustomParsingMethodName, this.serializableAttribute.Visibility)}(ref reader, out {returnType} {value}, out error))"
                        );
                    }
                    else
                    {
                        builder.AppendLine(
                            $"if (!{this.GetMethodName(aliasedMethodReference.ParsingMethodName, aliasedMethodReference.Visibility)}(ref reader, out {returnType} {value}, out error))"
                        );
                    }

                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, null);
                    }

                    builder.AppendLine($"result = ({baseType.FullName}){value};");
                    builder.AppendLine("error = default;");
                    builder.AppendLine("return true;");
                }
                else
                {
                    var returnType =
                        $"{aliasedTypedefType.FullNameNonNullable}{(aliasedTypedefType.IsValueType ? string.Empty : "?")}";
                    var inputValueName = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!{this.serializableAttribute.ParsingMethodName}(ref reader, out {returnType} {inputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, "value");
                    }

                    builder.AppendLine($"result = new {this.serializedType.FullName}({inputValueName});");
                    builder.AppendLine("error = default;");
                    builder.AppendLine("return true;");
                }
            }
            else if (IsHandledAsJsonIndividualElement(serializedType, true))
            {
                var value = this.ParseIndividualJsonElement(
                    builder,
                    null,
                    PropertyMember.FromType(serializedType),
                    serializedType
                );

                builder.AppendLine($"result = {value};");
                builder.AppendLine("error = default;");
                builder.AppendLine("return true;");
            }
            else if (
                serializedType.IsValueType
                && serializedType.IsNullable
                && (this.members.Length == 0 || Array.TrueForAll(this.members, n => n.Names.Count == 0))
                && this.MethodReferences.TryGetValue(
                    new MethodReferenceKey(SerializableAttributeType.Json, baseType.MethodKeyName),
                    out var baseMethodReference
                )
            )
            {
                // Try to inject nullable instead of failing below due to no names.
                var returnType = $"{baseType.FullNameNonNullable}{(baseType.IsValueType ? string.Empty : "?")}";
                var value = builder.GetVariableName("output");
                if (baseMethodReference.CustomParsingMethodName != null)
                {
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(baseMethodReference.CustomParsingMethodName, this.serializableAttribute.Visibility)}(ref reader, out {returnType} {value}, out error))"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(baseMethodReference.ParsingMethodName, baseMethodReference.Visibility)}(ref reader, out {returnType} {value}, out error))"
                    );
                }

                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.AppendError(builder, null);
                }

                builder.AppendLine($"result = ({serializedType.FullName}){value};");
                builder.AppendLine("error = default;");
                builder.AppendLine("return true;");
            }
            else
            {
                // Setup variables and their defaults.
                var requiredProperties = new Dictionary<string, (PropertyMember member, string boolName)>();
                var initProperties = new HashSet<string>();

                var memberVariableNames = new string?[this.members.Length];
                for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
                {
                    var member = this.members[memberIndex];
                    if (member.Names.Count == 0)
                    {
                        memberVariableNames[memberIndex] = null;
                        continue;
                    }

                    var name = member.Names[0];
                    var memberVariableName = builder.GetVariableName("member");
                    memberVariableNames[memberIndex] = memberVariableName;

                    var propertyType = member.Type;
                    if (member.IsRequired)
                    {
                        builder.AppendLine($"{propertyType.FullName} {memberVariableName} = default!;");
                        _ = initProperties.Add(memberVariableName);
                    }
                    else if (member.DefaultValue != null)
                    {
                        builder.AppendLine($"{propertyType.FullName} {memberVariableName} = {member.DefaultValue};");
                        _ = initProperties.Add(memberVariableName);
                    }
                    else
                    {
                        if (member.IsInitOnly)
                        {
                            if (
                                propertyType is
                                {
                                    IsValueType: false,
                                    NullableAnnotation: NullableAnnotation.None or NullableAnnotation.NotAnnotated
                                }
                            )
                            {
                                reportDiagnostic(
                                    DiagnosticMessages.CreateJsonSerializableViolatesNullConstraints(
                                        member.GetLocation(),
                                        this.serializedType.FullName,
                                        member.PropertyName
                                    )
                                );

                                builder.AppendLine($"{propertyType.FullName} {memberVariableName} = default!;");
                            }
                            else
                            {
                                builder.AppendLine($"{propertyType.FullName} {memberVariableName} = default;");
                            }

                            _ = initProperties.Add(memberVariableName);
                        }
                        else if (this.jsonSerializableSettingsAttribute.FastParsingAllPropertiesAreDefault)
                        {
                            builder.AppendLine($"{propertyType.FullName} {memberVariableName} = default!;");
                            _ = initProperties.Add(memberVariableName);
                        }
                        else
                        {
                            builder.AppendLine(
                                $"var {memberVariableName} = default(global::KestrelToolbox.Internal.OptionalProperty<{propertyType.FullName}>);"
                            );
                        }
                    }

                    if (member.IsRequired)
                    {
                        var boolName = builder.GetVariableName("required");
                        builder.AppendLine($"var {boolName} = false;");
                        requiredProperties.Add(name, (member, boolName));
                    }
                }

                if (baseType.ArrayElementType != null)
                {
                    builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartArray)");
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            $"{serializedType.FriendlyName} requires an array."
                        );
                    }
                }
                else
                {
                    builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)");
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            $"'{serializedType.FriendlyName}' requires an object."
                        );
                    }
                }

                builder.AppendLine("while (reader.Read())");
                using (builder.AppendBlock())
                {
                    builder.AppendLine("if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)");
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine("if (!reader.IsFinalBlock)");
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine("reader.Read();");
                        }

                        // End the loop.
                        builder.AppendLine("break;");
                    }

                    var propertyName = builder.GetVariableName("propertyName");
                    builder.AppendLine($"var {propertyName} = reader.GetString()!;");

                    // Read through property. Utf8JsonReader should error if there is invalid json here.
                    builder.AppendLine("_ = reader.Read();");

                    builder.AppendLine($"switch ({propertyName})");
                    using (builder.AppendBlock())
                    {
                        for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
                        {
                            var member = this.members[memberIndex];
                            if (member.Names.Count == 0)
                            {
                                continue;
                            }

                            Debug.Assert(member.Names.Count == 1, "Too many names, should have been checked earlier.");

                            var name = member.Names[0];
                            var memberVariableName = memberVariableNames[memberIndex];

                            builder.AppendLine($"case \"{name}\":");
                            using (builder.AppendBlock())
                            {
                                if (requiredProperties.TryGetValue(name, out var requiredTuple))
                                {
                                    builder.AppendLine($"{requiredTuple.boolName} = true;");
                                }

                                var value = this.ParseIndividualJsonElement(builder, name, member, member.Type);
                                builder.AppendLine($"{memberVariableName} = {value};");
                                builder.AppendLine("break;");
                            }
                        }

                        builder.AppendLine("default:");
                        using (builder.AppendBlock())
                        {
                            if (!this.jsonSerializableSettingsAttribute.AllowExtraProperties)
                            {
                                builder.AppendLine($"error = $\"Unknown property received: {{{propertyName}}}\";");
                                builder.AppendLine("result = default;");
                                builder.AppendLine("return false;");
                            }
                            else
                            {
                                builder.AppendLine("switch (reader.TokenType)");
                                using (builder.AppendBlock())
                                {
                                    // Skip any StartObject or StartArray
                                    builder.AppendLine("case global::System.Text.Json.JsonTokenType.StartObject:");
                                    builder.AppendLine("case global::System.Text.Json.JsonTokenType.StartArray:");
                                    using (builder.AppendBlock())
                                    {
                                        builder.AppendLine("reader.Skip();");
                                        builder.AppendLine("break;");
                                    }

                                    builder.AppendLine("default:");
                                    using (builder.AppendBlock())
                                    {
                                        builder.AppendLine("// Process on next pass.");
                                        builder.AppendLine("break;");
                                    }
                                }

                                builder.AppendLine("break;");
                            }
                        }
                    }
                }

                // Loop through required properties and search for ones that have not been set.
                if (requiredProperties.Count != 0)
                {
                    var anyFailedName = builder.GetVariableName();
                    builder.AppendLine($"var {anyFailedName} = true;");
                    foreach (var pair in requiredProperties)
                    {
                        builder.AppendLine($"{anyFailedName} &= {pair.Value.boolName};");
                    }

                    builder.AppendLine($"if (!{anyFailedName})");
                    using (builder.AppendBlock())
                    {
                        var errorList = builder.GetVariableName();
                        builder.AppendLine($"var {errorList} = new global::System.Collections.Generic.List<string>();");
                        foreach (var pair in requiredProperties)
                        {
                            builder.AppendLine($"if (!{pair.Value.boolName})");
                            using (builder.AppendBlock())
                            {
                                builder.AppendLine($"{errorList}.Add(\"{pair.Value.member.Names[0]}\");");
                            }
                        }

                        builder.AppendLine(
                            $"error = $\"A required property was missing on {serializedType.FriendlyName}. Missing: {{string.Join(\", \", {errorList})}}.\";"
                        );
                        builder.AppendLine("result = default;");
                        builder.AppendLine("return false;");
                    }
                }

                if (memberVariableNames.Length == 0 || Array.TrueForAll(memberVariableNames, n => n == null))
                {
                    reportDiagnostic(
                        DiagnosticMessages.CreateJsonSerializableDidNotHaveAnyMembers(
                            this.serializableAttribute.GetLocation(),
                            serializedType.FullName
                        )
                    );
                }

                bool hasDefaultConstructor;
                if (serializedType.IsNamedType)
                {
                    hasDefaultConstructor = serializedType.HasDefaultConstructor;
                }
                else if (serializedType.ArrayElementType != null)
                {
                    hasDefaultConstructor = false;
                }
                else
                {
                    throw new NotImplementedException($"Unknown type {serializedType}.");
                }

                if (hasDefaultConstructor)
                {
                    builder.AppendLine($"result = new {baseType.FullNameNonNullable}()");
                    builder.PushBlock();
                    for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
                    {
                        var member = this.members[memberIndex];
                        var memberVariableName = memberVariableNames[memberIndex];
                        if (memberVariableName != null)
                        {
                            if (member.PropertyName == null)
                            {
                                throw new Exception("PropertyName on a member cannot be null.");
                            }

                            if (initProperties.Contains(memberVariableName))
                            {
                                builder.AppendLine($"{member.PropertyName} = {memberVariableName},");
                            }
                        }
                    }

                    builder.PopBlock(";");

                    for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
                    {
                        var member = this.members[memberIndex];
                        var memberVariableName = memberVariableNames[memberIndex];
                        if (memberVariableName != null)
                        {
                            if (member.PropertyName == null)
                            {
                                throw new Exception("PropertyName on a member cannot be null.");
                            }

                            if (!initProperties.Contains(memberVariableName))
                            {
                                using (builder.AppendBlock($"if ({memberVariableName}.HasValue)"))
                                {
                                    builder.AppendLine(
                                        $"result.{member.PropertyName} = ({member.Type.FullName}){memberVariableName};"
                                    );
                                }
                            }
                        }
                    }
                }
                else
                {
                    var outputName = builder.GetVariableName("output");
                    builder.AppendLine(
                        $"var {outputName} = ({baseType.FullNameNonNullable})global::System.Runtime.CompilerServices.RuntimeHelpers.GetUninitializedObject(typeof({baseType.FullNameNonNullable}));"
                    );
                    for (var memberIndex = 0; memberIndex < this.members.Length; memberIndex++)
                    {
                        var member = this.members[memberIndex];
                        var memberVariableName = memberVariableNames[memberIndex];
                        if (memberVariableName != null)
                        {
                            if (member.PropertyName == null)
                            {
                                throw new Exception("PropertyName on a member cannot be null.");
                            }

                            if (initProperties.Contains(memberVariableName))
                            {
                                builder.AppendLine($"{outputName}.{member.PropertyName} = {memberVariableName};");
                            }
                            else
                            {
                                using (builder.AppendBlock($"if ({memberVariableName}.HasValue)"))
                                {
                                    builder.AppendLine(
                                        $"{outputName}.{member.PropertyName} = ({member.Type.FullName}){memberVariableName};"
                                    );
                                }
                            }
                        }
                    }

                    builder.AppendLine($"result = {outputName};");
                }

                builder.AppendLine("error = default;");
                builder.AppendLine("return true;");
            }
        }

        using (builder.AppendBlock("catch (global::System.Text.Json.JsonException)"))
        {
            builder.AppendLine("result = default;");
            builder.AppendLine("error = \"Failed to parse json.\";");
            builder.AppendLine("return false;");
        }
    }

    private string ParseIndividualJsonElement(
        CodeBuilder builder,
        string? name,
        PropertyMember property,
        CacheableTypeSymbol elementType
    )
    {
        var baseElementType = elementType.NonNullableType;
        var isNullable = elementType.IsNullable;

        var outputValueName = builder.GetVariableName("output");
        builder.AppendLine($"{elementType.FullName} {outputValueName};");

        // Support null if type is nullable or it is annotated to be null.
        // Disable null support if it is the base type for this element. Root level non-null parsing is not supported.
        var supportsNull =
            isNullable
            || (
                !baseElementType.IsValueType
                && baseElementType.NullableAnnotation == NullableAnnotation.Annotated
                && elementType != this.serializedType
            );
        if (supportsNull)
        {
            builder.AppendLine("if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)");
            using (builder.AppendBlock())
            {
                builder.AppendLine($"{outputValueName} = null;");
            }

            builder.PushBlock("else");
        }

        if (property.CustomPropertyAttribute != null && property.CustomPropertyAttribute.ParsingMethodName != null)
        {
            var inputValueName = builder.GetVariableName("input");
            var returnType =
                $"{baseElementType.FullNameNonNullable}{(baseElementType.IsValueType ? string.Empty : "?")}";
            builder.AppendLine(
                $"if (!{property.CustomPropertyAttribute.ParsingMethodName}(ref reader, out {returnType} {inputValueName}, out error))"
            );
            using (builder.AppendBlock())
            {
                SerializableAttributesExecutor.AppendError(builder, name);
            }

            builder.AppendLine($"{outputValueName} = {inputValueName};");
            if (supportsNull)
            {
                builder.PopBlock();
            }

            return outputValueName;
        }

        switch (baseElementType.FullName)
        {
            case "byte[]":
            case "byte[]?":
            {
                // System.Byte[] is special because it uses base64
                var inputValueName = builder.GetVariableName("input");
                builder.AppendLine($"if (!reader.TryGetBytesFromBase64(out var {inputValueName}))");
                using (builder.AppendBlock())
                {
                    if (name == null)
                    {
                        SerializableAttributesExecutor.ReturnError(builder, "Was not of type base64 encoded string.");
                    }
                    else
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            "Was not of type base64 encoded string."
                        );
                    }
                }

                builder.AppendLine($"{outputValueName} = {inputValueName};");
                break;
            }

            case "bool":
            {
                var boolSerializableSettingsAttribute = property.BoolSerializableSettings;

                var allowFlexibleStrings = boolSerializableSettingsAttribute.AllowFlexibleStrings;
                var allowNumbers = boolSerializableSettingsAttribute.AllowNumbers;
                allowNumbers |= boolSerializableSettingsAttribute.SerializeAsNumbers;

                var inputValueName = builder.GetVariableName("input");
                if (allowFlexibleStrings || allowNumbers)
                {
                    builder.AppendLine(
                        $"if (!global::KestrelToolbox.Serialization.Helpers.JsonParsing.TryParseFlexibleBool(ref reader, out var {inputValueName}, {(allowFlexibleStrings ? "true" : "false")}, {(allowNumbers ? "true" : "false")}))"
                    );
                    using (builder.AppendBlock())
                    {
                        ReturnIncorrectTypeError(builder, name, supportsNull, baseElementType);
                    }
                }
                else
                {
                    // Simple parsing.
                    builder.AppendLine($"bool {inputValueName};");
                    builder.AppendLine("switch (reader.TokenType)");
                    using (builder.AppendBlock())
                    {
                        builder.AppendLine("case global::System.Text.Json.JsonTokenType.True:");
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine($"{inputValueName} = true;");
                            builder.AppendLine("break;");
                        }

                        builder.AppendLine("case global::System.Text.Json.JsonTokenType.False:");
                        using (builder.AppendBlock())
                        {
                            builder.AppendLine($"{inputValueName} = false;");
                            builder.AppendLine("break;");
                        }

                        builder.AppendLine("default:");
                        using (builder.AppendBlock())
                        {
                            SerializableAttributesExecutor.ReturnError(
                                builder,
                                name,
                                $"Was not of type {baseElementType.FriendlyName}."
                            );
                        }
                    }
                }

                builder.AppendLine($"{outputValueName} = {inputValueName};");
                break;
            }

            case "byte":
            case "byte?":
            case "decimal":
            case "decimal?":
            case "double":
            case "double?":
            case "float":
            case "float?":
            case "int":
            case "int?":
            case "long":
            case "long?":
            case "sbyte":
            case "sbyte?":
            case "short":
            case "short?":
            case "uint":
            case "uint?":
            case "ulong":
            case "ulong?":
            case "ushort":
            case "ushort?":
            {
                var methodNameType = baseElementType.FullName switch
                {
                    "byte" => "Byte",
                    "byte?" => "Byte",
                    "decimal" => "Decimal",
                    "decimal?" => "Decimal",
                    "double" => "Double",
                    "double?" => "Double",
                    "float" => "Single",
                    "float?" => "Single",
                    "int" => "Int32",
                    "int?" => "Int32",
                    "long" => "Int64",
                    "long?" => "Int64",
                    "sbyte" => "SByte",
                    "sbyte?" => "SByte",
                    "short" => "Int16",
                    "short?" => "Int16",
                    "uint" => "UInt32",
                    "uint?" => "UInt32",
                    "ulong" => "UInt64",
                    "ulong?" => "UInt64",
                    "ushort" => "UInt16",
                    "ushort?" => "UInt16",
                    _ => throw new NotImplementedException($"Unknown type {baseElementType.FullName}."),
                };
                var methodName = $"TryGet{methodNameType}";
                var value = ReadJsonToken(
                    builder,
                    baseElementType,
                    "JsonTokenType.Number",
                    methodName,
                    name,
                    supportsNull
                );

                SerializableAttributesExecutor.ValidateNumberType(builder, value, name, property);
                builder.AppendLine($"{outputValueName} = {value};");
                break;
            }

            case "string":
            case "string?":
            {
                if (property.AllowAnyString)
                {
                    var inputValueName = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!global::KestrelToolbox.Serialization.Helpers.JsonParsing.TryParseAnyString(ref reader, out var {inputValueName}))"
                    );
                    using (builder.AppendBlock())
                    {
                        ReturnIncorrectTypeError(builder, name, supportsNull, baseElementType);
                    }

                    builder.AppendLine($"{outputValueName} = {inputValueName};");
                }
                else
                {
                    var readValue = ReadJsonString(builder, name, supportsNull);
                    builder.AppendLine($"{outputValueName} = {readValue};");
                }

                SerializableAttributesExecutor.ValidateStringType(builder, outputValueName, name, property);
                break;
            }

            case "global::System.Guid":
            case "global::KestrelToolbox.Types.UUIDv1":
            {
                var inputValueName = ReadJsonToken(
                    builder,
                    baseElementType,
                    "JsonTokenType.String",
                    "TryGetGuid",
                    name,
                    supportsNull
                );

                builder.AppendLine($"{outputValueName} = {inputValueName};");
                break;
            }

            case "global::System.TimeOnly":
            case "global::System.DateTime":
            case "global::System.DateTimeOffset":
            {
                var inputValueName = ReadJsonString(builder, name, supportsNull, baseElementType.FriendlyName);

                var tempVariableName = builder.GetVariableName("token");
                builder.AppendLine(
                    $"if (!{baseElementType.FullName}.TryParse({inputValueName}, out var {tempVariableName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                builder.AppendLine($"{outputValueName} = {tempVariableName};");
                break;
            }

            case "global::System.DateOnly":
            {
                var inputValueName = ReadJsonString(builder, name, supportsNull, baseElementType.FriendlyName);

                var dateOnlySettings = property.DateOnlyParseSettings;
                var tempVariableName = builder.GetVariableName("token");
                if (dateOnlySettings != null)
                {
                    builder.AppendLine(
                        $"if (!global::System.DateOnly.TryParseExact({inputValueName}, {dateOnlySettings.Format}, out var {tempVariableName}))"
                    );
                }
                else
                {
                    builder.AppendLine(
                        $"if (!global::System.DateOnly.TryParse({inputValueName}, out var {tempVariableName}))"
                    );
                }

                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                builder.AppendLine($"{outputValueName} = {tempVariableName};");
                break;
            }

            case "global::System.TimeSpan":
            {
                builder.AppendLine("if (reader.TokenType == global::System.Text.Json.JsonTokenType.String)");
                using (builder.AppendBlock())
                {
                    var inputValueName = builder.GetVariableName("token");

                    // We know that GetString will return non-null because we check earlier for token type.
                    builder.AppendLine($"var {inputValueName} = reader.GetString()!;");
                    var tempVariableName = builder.GetVariableName("token");
                    builder.AppendLine(
                        $"if (!global::KestrelToolbox.Serialization.TimeSpanParser.TryParse({inputValueName}, out var {tempVariableName}))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            $"Was not of type {baseElementType.FriendlyName}."
                        );
                    }

                    builder.AppendLine($"{outputValueName} = {tempVariableName};");
                }

                builder.AppendLine("else if (reader.TokenType == global::System.Text.Json.JsonTokenType.Number)");
                using (builder.AppendBlock())
                {
                    var inputValueName = builder.GetVariableName("token");
                    builder.AppendLine($"var {inputValueName} = reader.GetDouble();");
                    var tempVariableName = builder.GetVariableName("token");
                    builder.AppendLine(
                        $"var {tempVariableName} = global::System.TimeSpan.FromSeconds({inputValueName});"
                    );

                    builder.AppendLine($"{outputValueName} = {tempVariableName};");
                }

                builder.AppendLine("else");
                using (builder.AppendBlock())
                {
                    ReturnIncorrectTypeError(builder, name, supportsNull, "string");
                }

                break;
            }

            case "global::System.Text.Json.Nodes.JsonNode":
            case "global::System.Text.Json.Nodes.JsonNode?":
            case "global::System.Text.Json.Nodes.JsonObject":
            case "global::System.Text.Json.Nodes.JsonObject?":
            case "global::System.Text.Json.Nodes.JsonArray":
            case "global::System.Text.Json.Nodes.JsonArray?":
            {
                var inputValueName = builder.GetVariableName("input");
                var methodNameType = elementType.FullName switch
                {
                    "global::System.Text.Json.Nodes.JsonNode" => "JsonNode",
                    "global::System.Text.Json.Nodes.JsonNode?" => "JsonNode",
                    "global::System.Text.Json.Nodes.JsonObject" => "JsonObject",
                    "global::System.Text.Json.Nodes.JsonObject?" => "JsonObject",
                    "global::System.Text.Json.Nodes.JsonArray" => "JsonArray",
                    "global::System.Text.Json.Nodes.JsonArray?" => "JsonArray",
                    _ => throw new NotImplementedException($"{elementType.FullName} not implemented."),
                };
                builder.AppendLine(
                    $"if (!global::KestrelToolbox.Serialization.Helpers.JsonParsing.TryParse{methodNameType}(ref reader, out var {inputValueName}))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        $"Was not of type {baseElementType.FriendlyName}."
                    );
                }

                builder.AppendLine($"{outputValueName} = {inputValueName};");
                break;
            }

            default:
            {
                if (
                    this.MethodReferences.TryGetValue(
                        new MethodReferenceKey(SerializableAttributeType.Json, baseElementType.MethodKeyName),
                        out var subObject
                    )
                    && subObject.CustomParsingMethodName != null
                )
                {
                    // Try non-nullable first, then nullable. We check for nullable above. Code gen should be identical.
                    // Compiler will take care of the magic here.
                    var inputValueName = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(subObject.CustomParsingMethodName, subObject.Visibility)}(ref reader, out {subObject.ParsingType} {inputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, name);
                    }

                    builder.AppendLine($"{outputValueName} = {inputValueName};");
                }
                else if (baseElementType.JsonSerializableCustomParseMethod != null)
                {
                    var inputValueName = builder.GetVariableName("input");
                    var customParseMethod = baseElementType.JsonSerializableCustomParseMethod;
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(customParseMethod.FullMethodName, SerializableVisibility.Private)}(ref reader, out {customParseMethod.ResultTypeFullName} {inputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, name);
                    }

                    builder.AppendLine($"{outputValueName} = {inputValueName};");
                }
                else if (
                    baseElementType.SelfSerializableAttributes?.Find(r =>
                        r.Type == SerializableAttributeType.Json && r.GenerateParse
                    ) != null
                )
                {
                    var selfSerializableAttribute = baseElementType.SelfSerializableAttributes!.Find(r =>
                        r.Type == SerializableAttributeType.Json
                    );
                    var returnType =
                        $"{baseElementType.FullNameNonNullable}{(baseElementType.IsValueType ? string.Empty : "?")}";

                    var inputValueName = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!{baseElementType.FullNameNonNullable}.{selfSerializableAttribute.ParsingMethodName}(ref reader, out {returnType} {inputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, name);
                    }

                    builder.AppendLine($"{outputValueName} = {inputValueName};");
                }
                else if (PropertyMember.IsPropertyTuple(baseElementType))
                {
                    builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.PropertyName)");
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(builder, name, "Was not a key/value pair.");
                    }

                    var inputKeyName = builder.GetVariableName("input");
                    builder.AppendLine($"var {inputKeyName} = reader.GetString()!;");
                    builder.AppendLine("if (!reader.Read())");
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(builder, name, "Was not a key/value pair.");
                    }

                    var inputValueName = this.ParseIndividualJsonElement(
                        builder,
                        null,
                        property,
                        baseElementType.TypeArguments[1]
                    );
                    builder.AppendLine(
                        $"{outputValueName} = new global::System.Collections.Generic.KeyValuePair<string, {baseElementType.TypeArguments[1].FullName}>({inputKeyName}, {inputValueName});"
                    );
                }
                else if (
                    PropertyMember.IsParsedAsArray(baseElementType) || PropertyMember.IsParsedAsSet(baseElementType)
                )
                {
                    WriteArray();
                }
                else if (SerializableAttributesExecutor.FindGenericStringDictionary(baseElementType) != null)
                {
                    WriteDictionary();
                }
                else if (baseElementType.TypeKind == TypeKind.Enum)
                {
                    WriteEnum();
                }
                else if (subObject != null)
                {
                    var inputValueName = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(subObject.ParsingMethodName, subObject.Visibility)}(ref reader, out {subObject.ParsingType} {inputValueName}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, name);
                    }

                    builder.AppendLine($"{outputValueName} = {inputValueName};");
                }
                else
                {
                    throw new NotImplementedException(
                        $"Type {baseElementType.FullName} didn't get added to method references."
                    );
                }

                break;
            }
        }

        if (supportsNull)
        {
            builder.PopBlock();
        }

        return outputValueName;

        void WriteArray()
        {
            bool isArray;
            CacheableTypeSymbol arrayElementType;
            if (baseElementType.ArrayElementType != null)
            {
                isArray = true;
                arrayElementType = baseElementType.ArrayElementType;
            }
            else
            {
                isArray = false;
                arrayElementType = baseElementType.TypeArguments[0];
            }

            var minLength = default(string);
            var maxLength = default(string);

            var arrayLength = property.ArrayLength;
            if (arrayLength != null)
            {
                minLength = arrayLength.Min;
                maxLength = arrayLength.Max;
            }

            builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartArray)");
            using (builder.AppendBlock())
            {
                SerializableAttributesExecutor.ReturnError(builder, name, "Was not an array.");
            }

            var inputValueName = builder.GetVariableName("input");
            var parsedAsSet = PropertyMember.IsParsedAsSet(baseElementType);
            if (parsedAsSet)
            {
                builder.AppendLine(
                    $"var {inputValueName} = new global::System.Collections.Generic.HashSet<{arrayElementType.FullName}>();"
                );
            }
            else
            {
                builder.AppendLine(
                    $"var {inputValueName} = new global::System.Collections.Generic.List<{arrayElementType.FullName}>();"
                );
            }

            builder.AppendLine("while (reader.Read())");
            using (builder.AppendBlock())
            {
                builder.AppendLine("if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndArray)");
                using (builder.AppendBlock())
                {
                    builder.AppendLine("break;");
                }

                if (
                    !SerializableAttributesExecutor.IsBasicType(arrayElementType)
                    && this.MethodReferences.TryGetValue(
                        new MethodReferenceKey(SerializableAttributeType.Json, arrayElementType.MethodKeyName),
                        out var subObject
                    )
                )
                {
                    var elementValue = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(subObject.ParsingMethodName, subObject.Visibility)}(ref reader, out {subObject.ParsingType} {elementValue}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, name);
                    }

                    builder.AppendLine($"{inputValueName}.Add({elementValue});");
                }
                else
                {
                    var elementValue = this.ParseIndividualJsonElement(builder, name, property, arrayElementType);
                    builder.AppendLine($"{inputValueName}.Add({elementValue});");
                }

                if (maxLength != null)
                {
                    builder.AppendLine($"if ({inputValueName}.Count > {maxLength})");
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            $"Must be shorter than {maxLength} elements."
                        );
                    }
                }
            }

            if (minLength != null)
            {
                builder.AppendLine($"if ({inputValueName}.Count < {minLength})");
                using (builder.AppendBlock())
                {
                    if (long.TryParse(minLength, out var minLengthInt))
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            $"Must be longer than {minLengthInt - 1} element{(minLengthInt - 1 == 1 ? string.Empty : "s")}."
                        );
                    }
                    else
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            $"Must be longer than at least {minLength} elements."
                        );
                    }
                }
            }

            if (isArray)
            {
                builder.AppendLine($"{outputValueName} = {inputValueName}.ToArray();");
            }
            else if (parsedAsSet && PropertyMember.IsParsedAsFrozenSet(baseElementType))
            {
                builder.AppendLine(
                    $"{outputValueName} = global::System.Collections.Frozen.FrozenSet.ToFrozenSet({inputValueName});"
                );
            }
            else
            {
                builder.AppendLine($"{outputValueName} = {inputValueName};");
            }
        }

        void WriteDictionary()
        {
            builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)");
            using (builder.AppendBlock())
            {
                SerializableAttributesExecutor.ReturnError(builder, name, "Was not a dictionary.");
            }

            var inputValueName = builder.GetVariableName("input");
            CacheableTypeSymbol arrayElementType;
            var isFrozen = PropertyMember.IsParsedAsFrozenDictionary(baseElementType);
            var hasTryAddMethod =
                isFrozen
                || baseElementType.ConstructedFromFullName
                    == "global::System.Collections.Generic.Dictionary<TKey, TValue>";
            if (isFrozen)
            {
                arrayElementType = baseElementType.TypeArguments[1];
                builder.AppendLine(
                    $"var {inputValueName} = new global::System.Collections.Generic.Dictionary<string, {arrayElementType.FullNameNonNullable}>();"
                );
            }
            else
            {
                var dictionaryInterface = SerializableAttributesExecutor.FindGenericStringDictionary(baseElementType)!;
                arrayElementType = dictionaryInterface.TypeArguments[1];
                builder.AppendLine($"var {inputValueName} = new {baseElementType.FullNameNonNullable}();");
            }

            builder.AppendLine("while (reader.Read())");
            using (builder.AppendBlock())
            {
                builder.AppendLine("if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)");
                using (builder.AppendBlock())
                {
                    builder.AppendLine("break;");
                }

                builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.PropertyName)");
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(
                        builder,
                        name,
                        "Has invalid dictionary. Expected property name."
                    );
                }

                var elementKey = builder.GetVariableName("key");
                builder.AppendLine($"var {elementKey} = reader.GetString()!;");
                builder.AppendLine("if (!reader.Read())");
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(builder, name, "Did not have valid value.");
                }

                string elementValue;
                if (
                    !SerializableAttributesExecutor.IsBasicType(arrayElementType)
                    && this.MethodReferences.TryGetValue(
                        new MethodReferenceKey(SerializableAttributeType.Json, arrayElementType.MethodKeyName),
                        out var subObject
                    )
                )
                {
                    elementValue = builder.GetVariableName("input");
                    builder.AppendLine(
                        $"if (!{this.GetMethodName(subObject.ParsingMethodName, subObject.Visibility)}(ref reader, out {subObject.ParsingType} {elementValue}, out error))"
                    );
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.AppendError(builder, name);
                    }
                }
                else
                {
                    elementValue = this.ParseIndividualJsonElement(builder, name, property, arrayElementType);
                }

                if (hasTryAddMethod)
                {
                    builder.AppendLine($"if (!{inputValueName}.TryAdd({elementKey}, {elementValue}))");
                }
                else
                {
                    builder.AppendLine(
                        $"if (!global::System.Collections.Generic.CollectionExtensions.TryAdd({inputValueName}, {elementKey}, {elementValue}))"
                    );
                }

                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.ReturnError(builder, name, "Has duplicate key.");
                }

                var minLength = default(string);
                var maxLength = default(string);

                var arrayLength = property.ArrayLength;
                if (arrayLength != null)
                {
                    minLength = arrayLength.Min;
                    maxLength = arrayLength.Max;
                }

                if (minLength != null)
                {
                    builder.AppendLine($"if ({inputValueName}.Length < {minLength})");
                    using (builder.AppendBlock())
                    {
                        if (long.TryParse(minLength, out var minLengthInt))
                        {
                            SerializableAttributesExecutor.ReturnError(
                                builder,
                                name,
                                $"Must be longer than {minLengthInt - 1} character{(minLengthInt - 1 == 1 ? string.Empty : "s")}."
                            );
                        }
                        else
                        {
                            SerializableAttributesExecutor.ReturnError(
                                builder,
                                name,
                                $"Must be longer than at least {minLength} characters."
                            );
                        }
                    }
                }

                if (maxLength != null)
                {
                    builder.AppendLine($"if ({inputValueName}.Length > {maxLength})");
                    using (builder.AppendBlock())
                    {
                        SerializableAttributesExecutor.ReturnError(
                            builder,
                            name,
                            $"Must be shorter than {maxLength} elements."
                        );
                    }
                }
            }

            if (isFrozen)
            {
                builder.AppendLine(
                    $"{outputValueName} = global::System.Collections.Frozen.FrozenDictionary.ToFrozenDictionary({inputValueName});"
                );
            }
            else
            {
                builder.AppendLine($"{outputValueName} = {inputValueName};");
            }
        }

        void WriteEnum()
        {
            if (elementType.FullName == this.serializableAttribute.SerializedType.FullName)
            {
                // If we're generating the parser method for enum, do it here.
                var enumSerializableSettings =
                    baseElementType.EnumSerializableSettingsAttribute ?? new EnumSerializableSettingsAttribute();
                Debug.Assert(
                    baseElementType.EnumMembers != null,
                    "Enum members should never be null for an enum type."
                );
                var enumMembers = baseElementType.EnumMembers!;

                if (enumSerializableSettings.SerializeAsNumbers)
                {
                    var methodNameType = baseElementType.EnumUnderlyingType switch
                    {
                        "byte" => "Byte",
                        "int" => "Int32",
                        "long" => "Int64",
                        "sbyte" => "SByte",
                        "short" => "Int16",
                        "uint" => "UInt32",
                        "ulong" => "UInt64",
                        "ushort" => "UInt16",
                        _ => throw new NotImplementedException(
                            $"Unknown enum type {baseElementType.EnumUnderlyingType}."
                        ),
                    };
                    var methodName = $"TryGet{methodNameType}";
                    var inputValueName = ReadJsonToken(
                        builder,
                        baseElementType,
                        "JsonTokenType.Number",
                        methodName,
                        baseElementType.FriendlyName,
                        supportsNull,
                        "number"
                    );
                    if (!enumSerializableSettings.AllowAnyNumber || enumSerializableSettings.InvalidValue != null)
                    {
                        builder.AppendLine($"switch ({inputValueName})");
                        using (builder.AppendBlock())
                        {
                            if (enumMembers.Length != 0)
                            {
                                foreach (var enumMember in enumMembers)
                                {
                                    builder.AppendLine($"case {enumMember.ConstantValue}:");
                                }

                                using (builder.AppendBlock())
                                {
                                    builder.AppendLine("// Valid value.");
                                    builder.AppendLine("break;");
                                }
                            }

                            builder.AppendLine("default:");
                            using (builder.AppendBlock())
                            {
                                if (enumSerializableSettings.InvalidValue != null)
                                {
                                    builder.AppendLine(
                                        $"{outputValueName} = ({elementType.FullName}){enumSerializableSettings.InvalidValue};"
                                    );
                                    builder.AppendLine("break;");
                                }
                                else
                                {
                                    SerializableAttributesExecutor.ReturnError(
                                        builder,
                                        baseElementType.FriendlyName,
                                        $"Must be one of: {string.Join(", ", enumMembers.OrderBy(v => v.ConstantValue).Select(v => v.ConstantValue))}."
                                    );
                                }
                            }
                        }
                    }

                    builder.AppendLine($"{outputValueName} = ({baseElementType.FullNameNonNullable}){inputValueName};");
                }
                else
                {
                    var inputValueName = ReadJsonString(builder, baseElementType.FriendlyName, supportsNull);
                    builder.AppendLine($"switch ({inputValueName})");
                    using (builder.AppendBlock())
                    {
                        var hasName = false;
                        var allNames = new List<string>();
                        foreach (var member in enumMembers)
                        {
                            if (member.Names.Count == 0)
                            {
                                continue;
                            }

                            foreach (var memberName in member.Names)
                            {
                                allNames.Add(memberName);
                                builder.AppendLine($"case \"{memberName}\":");
                                hasName = true;
                            }

                            if (hasName)
                            {
                                using (builder.AppendBlock())
                                {
                                    builder.AppendLine($"{outputValueName} = {member.FullName};");
                                    builder.AppendLine("break;");
                                }
                            }
                        }

                        builder.AppendLine("default:");
                        using (builder.AppendBlock())
                        {
                            if (enumSerializableSettings.InvalidValue == null)
                            {
                                allNames.Sort();
                                SerializableAttributesExecutor.ReturnError(
                                    builder,
                                    baseElementType.FriendlyName,
                                    $"Must be one of: {string.Join(", ", allNames.OrderBy(v => v))}."
                                );
                            }
                            else
                            {
                                builder.AppendLine(
                                    $"{outputValueName} = ({elementType.FullName}){enumSerializableSettings.InvalidValue};"
                                );
                                builder.AppendLine("break;");
                            }
                        }
                    }
                }
            }
            else if (
                this.MethodReferences.TryGetValue(
                    new MethodReferenceKey(SerializableAttributeType.Json, baseElementType.MethodKeyName),
                    out var serializedEnumAttribute
                )
            )
            {
                var tempVariableName = builder.GetVariableName("token");

                builder.AppendLine(
                    $"if (!{this.GetMethodName(serializedEnumAttribute.ParsingMethodName, serializedEnumAttribute.Visibility)}(ref reader, out {baseElementType.FullName} {tempVariableName}, out error))"
                );
                using (builder.AppendBlock())
                {
                    SerializableAttributesExecutor.AppendError(builder, name);
                }

                builder.AppendLine($"{outputValueName} = {tempVariableName};");
            }
            else
            {
                throw new NotImplementedException(
                    $"Enum {elementType.FullName} didn't get added to method references."
                );
            }
        }
    }

    private static string ReadJsonToken(
        CodeBuilder builder,
        CacheableTypeSymbol tokenType,
        string jsonTokenType,
        string method,
        string? name,
        bool supportsNull,
        string? friendlyName = null
    )
    {
        var outputValueName = builder.GetVariableName("token");
        builder.AppendLine($"if (reader.TokenType != global::System.Text.Json.{jsonTokenType})");
        using (builder.AppendBlock())
        {
            if (friendlyName != null)
            {
                ReturnIncorrectTypeError(builder, name, supportsNull, friendlyName);
            }
            else
            {
                ReturnIncorrectTypeError(builder, name, supportsNull, tokenType);
            }
        }

        builder.AppendLine($"if (!reader.{method}(out var {outputValueName}))");
        using (builder.AppendBlock())
        {
            if (friendlyName != null)
            {
                ReturnIncorrectTypeError(builder, name, supportsNull, friendlyName);
            }
            else
            {
                ReturnIncorrectTypeError(builder, name, supportsNull, tokenType);
            }
        }

        return outputValueName;
    }

    private static string ReadJsonString(
        CodeBuilder builder,
        string? name,
        bool supportsNull,
        string friendlyName = "string"
    )
    {
        builder.AppendLine("if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)");
        using (builder.AppendBlock())
        {
            ReturnIncorrectTypeError(builder, name, supportsNull, friendlyName);
        }

        var outputValueName = builder.GetVariableName("token");

        // We know that GetString will return non-null because we check earlier for token type.
        builder.AppendLine($"var {outputValueName} = reader.GetString()!;");
        return outputValueName;
    }

    private static void ReturnIncorrectTypeError(
        CodeBuilder builder,
        string? name,
        bool supportsNull,
        CacheableTypeSymbol type
    ) => ReturnIncorrectTypeError(builder, name, supportsNull, type.FriendlyName);

    private static void ReturnIncorrectTypeError(
        CodeBuilder builder,
        string? name,
        bool supportsNull,
        string friendlyName
    )
    {
        if (!supportsNull)
        {
            builder.AppendLine("if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)");
            using (builder.AppendBlock())
            {
                SerializableAttributesExecutor.ReturnError(
                    builder,
                    name,
                    $"Was null. Should be of type {friendlyName}."
                );
            }

            builder.PushBlock("else");
        }

        SerializableAttributesExecutor.ReturnError(builder, name, $"Was not of type {friendlyName}.");

        if (!supportsNull)
        {
            builder.PopBlock();
        }
    }

    /// <summary>
    /// Gets if a <see cref="Type"/> is parsed as an individual element.
    /// </summary>
    /// <param name="type">Type to parse as an individual element.</param>
    /// <param name="parsing">True if parsing, false if serializing.</param>
    /// <returns>True if the type supports parsing on its own.</returns>
    private static bool IsHandledAsJsonIndividualElement(CacheableTypeSymbol type, bool parsing)
    {
        var baseType = type.NonNullableType;

        if (baseType.TypeKind == TypeKind.Enum)
        {
            return true;
        }

        if (PropertyMember.IsParsedAsArray(type))
        {
            return true;
        }

        if (SerializableAttributesExecutor.FindGenericStringDictionary(type) != null)
        {
            return true;
        }

        if (PropertyMember.IsPropertyTuple(type))
        {
            return true;
        }

        if (parsing)
        {
            if (type.JsonSerializableCustomParseMethod != null)
            {
                return true;
            }
        }
        else
        {
            if (PropertyMember.IsIEnumerable(type))
            {
                return true;
            }

            if (type.JsonSerializableCustomSerializeMethod != null)
            {
                return true;
            }
        }

        return baseType.FullNameNonNullable switch
        {
            "bool" => true,
            "byte" => true,
            "sbyte" => true,
            "short" => true,
            "int" => true,
            "long" => true,
            "ushort" => true,
            "uint" => true,
            "ulong" => true,
            "string" => true,
            "byte[]" => true,
            "global::System.Guid" => true,
            "global::System.DateTime" => true,
            "global::System.TimeSpan" => true,
            "global::System.Text.Json.Nodes.JsonNode" => true,
            "global::System.Text.Json.Nodes.JsonObject" => true,
            "global::System.Text.Json.Nodes.JsonArray" => true,
            "global::KestrelToolbox.Types.UUIDv1" => true,
            _ => false,
        };
    }

    /// <inheritdoc />
    public bool Equals(JsonSerializableGenerator? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.serializableAttribute.Equals(other.serializableAttribute)
            && this.jsonSerializableSettingsAttribute.Equals(other.jsonSerializableSettingsAttribute)
            && this.serializedType.Equals(other.serializedType)
            && this.members.SequenceEqual(other.members)
            && this.MethodReferences.SequenceEqual(other.MethodReferences);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is JsonSerializableGenerator other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.serializableAttribute, this.jsonSerializableSettingsAttribute);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(JsonSerializableGenerator? left, JsonSerializableGenerator? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="JsonSerializableGenerator"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(JsonSerializableGenerator? left, JsonSerializableGenerator? right)
    {
        return !Equals(left, right);
    }

    private string GetMethodName(string name, SerializableVisibility visibility) =>
        this.ParentExecutor.GetMethodName(
            this.serializableAttribute.DeclaringType.FullNameNonNullable,
            name,
            visibility
        );
}
