// <copyright file="Generator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.CodeGen.Routes;
using KestrelToolbox.CodeGen.Serializable;
using KestrelToolbox.CodeGen.Typedefs;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// Generator to call all other generators so that the directories these populate in are the same.
/// </summary>
[Generator(LanguageNames.CSharp)]
public sealed class Generator : IIncrementalGenerator
{
    private readonly SerializableAttributesGenerator serializableAttributesGenerator = new();
    private readonly RouteAttributesGenerator routeAttributesGenerator = new();
    private readonly TypedefAttributesGenerator typedefAttributesGenerator = new();

    /// <inheritdoc />
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        this.serializableAttributesGenerator.Initialize(context);
        this.routeAttributesGenerator.Initialize(context);
        this.typedefAttributesGenerator.Initialize(context);
    }
}
