// <copyright file="MethodReference.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using KestrelToolbox.CodeGen.Serializable;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// Parsing and serializing method reference.
/// </summary>
/// <seealso cref="MethodReferenceKey"/>
public sealed class MethodReference : IEquatable<MethodReference>
{
    /// <summary>
    /// Gets the fully qualified name of the parsing method name. If it is not fully qualified, it will be static local.
    /// </summary>
    public string ParsingMethodName { get; }

    /// <summary>
    /// Gets the fully qualified name of the parsing type for output.
    /// </summary>
    public string ParsingType { get; }

    /// <summary>
    /// Gets the fully qualified name of the serializing method name. If it is not fully qualified, it will be static local.
    /// </summary>
    public string SerializingMethodName { get; }

    /// <summary>
    /// Gets or sets a custom parsing method name if it exists on the attribute associated with this attribute.
    /// </summary>
    public string? CustomParsingMethodName { get; set; }

    /// <summary>
    /// Gets or sets a custom serializing method name if it exists on the attribute associated with this attribute.
    /// </summary>
    public string? CustomSerializingMethodName { get; set; }

    /// <summary>
    /// Gets the visibility of the method.
    /// </summary>
    public SerializableVisibility Visibility { get; }

    /// <summary>
    /// Gets a value indicating whether this method reference was generated implicitly.
    /// </summary>
    public bool IsImplicitlyGenerated { get; }

    /// <inheritdoc />
    public override string ToString() => $"{this.Visibility} {this.ParsingType}";

    /// <summary>
    /// Initializes a new instance of the <see cref="MethodReference"/> class.
    /// </summary>
    /// <param name="parsingType">The fully qualified name of the parsing type for output.</param>
    /// <param name="parsingMethodName">The fully qualified name of the parsing method name. If it is not fully qualified, it will be static local.</param>
    /// <param name="serializingMethodName">The fully qualified name of the serializing method name. If it is not fully qualified, it will be static local.</param>
    /// <param name="visibility">The visibility of the method.</param>
    /// <param name="isImplicitlyGenerated">A value indicating whether this method reference was generated implicitly.</param>
    public MethodReference(
        string parsingType,
        string parsingMethodName,
        string serializingMethodName,
        SerializableVisibility visibility,
        bool isImplicitlyGenerated
    )
    {
        this.ParsingType = parsingType;
        this.ParsingMethodName = parsingMethodName;
        this.SerializingMethodName = serializingMethodName;
        this.Visibility = visibility;
        this.IsImplicitlyGenerated = isImplicitlyGenerated;
    }

    /// <inheritdoc />
    public bool Equals(MethodReference? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.ParsingMethodName == other.ParsingMethodName
            && this.ParsingType == other.ParsingType
            && this.SerializingMethodName == other.SerializingMethodName
            && this.CustomParsingMethodName == other.CustomParsingMethodName
            && this.CustomSerializingMethodName == other.CustomSerializingMethodName
            && this.Visibility == other.Visibility;
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is MethodReference other && this.Equals(other));
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(
            this.ParsingMethodName,
            this.ParsingType,
            this.SerializingMethodName,
            this.CustomParsingMethodName,
            this.CustomSerializingMethodName,
            this.Visibility
        );
    }

    /// <summary>
    /// Determines whether the specified object is equal to the current object.
    /// </summary>
    /// <param name="left">Object A to compare.</param>
    /// <param name="right">Object B to compare.</param>
    /// <returns>True if the objects are equal, false if not.</returns>
    public static bool operator ==(MethodReference? left, MethodReference? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Determines whether the specified object is equal to the current object.
    /// </summary>
    /// <param name="left">Object A to compare.</param>
    /// <param name="right">Object B to compare.</param>
    /// <returns>True if the objects are not equal, false if they are.</returns>
    public static bool operator !=(MethodReference? left, MethodReference? right)
    {
        return !Equals(left, right);
    }
}
