// <copyright file="RouteAttributesGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Routes;

/// <summary>
/// Generator for RouteAttributes.
/// </summary>
public sealed class RouteAttributesGenerator : IIncrementalGenerator
{
    /// <inheritdoc />
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        var foundMethods = context
            .SyntaxProvider.ForAttributeWithMetadataName(
                "KestrelToolbox.Web.RouteAttribute",
                IsValidQuick,
                GatherRoutes
            )
            .Where(n => n != null);
        var combinedClasses = foundMethods.Collect().SelectMany(Combine).Where(n => n != null);
        context.RegisterSourceOutput(combinedClasses, static (ctx, s) => s!.Execute(ctx));
    }

    private static bool IsValidQuick(SyntaxNode syntaxNode, CancellationToken cancellationToken)
    {
        if (!syntaxNode.IsKind(SyntaxKind.MethodDeclaration))
        {
            return false;
        }

        var methodDeclarationSyntax = (MethodDeclarationSyntax)syntaxNode;
        if (methodDeclarationSyntax.AttributeLists.Count == 0)
        {
            return false;
        }

        return true;
    }

    private static CollectedRoute? GatherRoutes(
        GeneratorAttributeSyntaxContext context,
        CancellationToken cancellationToken
    )
    {
        var semanticModel = context.SemanticModel;
        var routeContext = new RouteAttributesGeneratorContext(semanticModel);
        var methodDeclarationSyntax = (MethodDeclarationSyntax)context.TargetNode;
        var methodLocation = CacheableLocation.FromLocation(methodDeclarationSyntax.GetLocation());

        var methodSymbol = semanticModel.GetDeclaredSymbol(methodDeclarationSyntax, cancellationToken);
        if (methodSymbol == null)
        {
            return null;
        }

        var containingSymbol = methodSymbol.ContainingType;
        var classDeclarationSymbol = CacheableTypeDeclarationSymbol.FromTypeSymbol(
            containingSymbol,
            methodLocation,
            cancellationToken
        );

        var diagnostics = new List<Diagnostic>();
        var routeAttributes = new List<RouteAttribute>();
        var acceptContentTypes = new HashSet<string>();
        var authorizeAttributes = new List<AuthorizeAttribute>();
        foreach (var attributeListSyntax in methodDeclarationSyntax.AttributeLists)
        {
            if (attributeListSyntax.Attributes.Count == 0)
            {
                continue;
            }

            if (attributeListSyntax.Attributes.Count != 1)
            {
                throw new NotImplementedException(
                    $"Unknown scenario where there are {attributeListSyntax.Attributes.Count} attributes."
                );
            }

            var attributeSyntax = attributeListSyntax.Attributes[0];
            var info = semanticModel.GetSymbolInfo(attributeSyntax, cancellationToken)!;
            if (info.Symbol is not IMethodSymbol attributeSymbol)
            {
                continue;
            }

            var attributeTypeFullName = attributeSymbol.ContainingType.GetFullName();
            switch (attributeTypeFullName)
            {
                case "global::KestrelToolbox.Web.RouteAttribute":
                {
                    var routeAttribute = RouteAttribute.FromSyntax(routeContext, attributeSyntax, cancellationToken);
                    if (routeAttribute == null)
                    {
                        return new CollectedRoute(
                            classDeclarationSymbol,
                            new List<CacheableRouteAttribute>(),
                            diagnostics
                        );
                    }

                    routeAttributes.Add(routeAttribute);
                    break;
                }

                case "global::KestrelToolbox.Web.AcceptContentTypesAttribute":
                {
                    var types = AcceptContentTypesAttribute.FromSyntax(routeContext, attributeSyntax);
                    if (types == null)
                    {
                        return new CollectedRoute(
                            classDeclarationSymbol,
                            new List<CacheableRouteAttribute>(),
                            diagnostics
                        );
                    }

                    foreach (var type in types)
                    {
                        _ = acceptContentTypes.Add(type);
                    }

                    break;
                }

                case "global::Microsoft.AspNetCore.Authorization.AuthorizeAttribute":
                {
                    var authorize = AuthorizeAttribute.FromSyntax(routeContext, attributeSyntax, cancellationToken);
                    if (authorize == null)
                    {
                        return new CollectedRoute(
                            classDeclarationSymbol,
                            new List<CacheableRouteAttribute>(),
                            diagnostics
                        );
                    }

                    authorizeAttributes.Add(authorize);
                    break;
                }

                default:
                {
                    // Unknown, ignore.
                    break;
                }
            }
        }

        var parameterList = methodDeclarationSyntax.ParameterList.Parameters;
        if (parameterList.Count == 0)
        {
            // Compiler should take care of it.
            return new CollectedRoute(classDeclarationSymbol, new List<CacheableRouteAttribute>(), diagnostics);
        }

        {
            var parameter = parameterList[0];
            if (parameter.Type == null)
            {
                // Compiler should take care of it.
                return new CollectedRoute(classDeclarationSymbol, new List<CacheableRouteAttribute>(), diagnostics);
            }

            var type = semanticModel.GetTypeInfo(parameter.Type, cancellationToken);
            if (type.Type?.GetFullName() != "global::Microsoft.AspNetCore.Http.HttpContext")
            {
                diagnostics.Add(
                    DiagnosticMessages.CreateRouteParameterInvalid(
                        parameterList[0].GetLocation(),
                        0,
                        "Microsoft.AspNetCore.Http.HttpContext",
                        type.Type?.GetFullName()
                    )
                );
                return new CollectedRoute(classDeclarationSymbol, new List<CacheableRouteAttribute>(), diagnostics);
            }
        }

        var parameters = new List<CacheableRouteAttribute.CacheableRouteParameter>();
        foreach (var parameter in parameterList.Skip(1))
        {
            if (parameter.Type == null)
            {
                // Compiler should handle it.
                return new CollectedRoute(classDeclarationSymbol, new List<CacheableRouteAttribute>(), diagnostics);
            }

            var parameterTypeInfo = semanticModel.GetTypeInfo(parameter.Type, cancellationToken);
            var parameterType = parameterTypeInfo.Type!;
            var parameterName = parameter.Identifier.Text;
            var typeName = parameterType.GetFullName();

            var jsonBodyAttribute = default(CacheableJsonBodyAttribute);
            var queryParametersAttribute = default(CacheableQueryParametersAttribute);
            switch (typeName)
            {
                case "int":
                case "uint":
                case "long":
                case "ulong":
                case "bool":
                case "float":
                case "double":
                case "decimal":
                case "string":
                case "global::System.Guid":
                case "global::KestrelToolbox.Types.UUIDv1":
                {
                    foreach (var route in routeAttributes)
                    {
                        if (!route.RouteParameterNames.Contains(parameterName))
                        {
                            diagnostics.Add(
                                DiagnosticMessages.CreateUnreferencedRouteParameter(
                                    parameter.GetLocation(),
                                    parameterName
                                )
                            );

                            return new CollectedRoute(
                                classDeclarationSymbol,
                                new List<CacheableRouteAttribute>(),
                                diagnostics
                            );
                        }
                    }

                    break;
                }

                default:
                {
                    var foundValidAttribute = false;
                    foreach (var attributeList in parameter.AttributeLists)
                    {
                        foreach (var attributeSyntax in attributeList.Attributes)
                        {
                            var info = semanticModel.GetSymbolInfo(attributeSyntax, cancellationToken)!;
                            switch (info.Symbol?.GetFullName())
                            {
                                case "JsonBodyAttribute":
                                {
                                    if (parameters.Exists(p => p.JsonBodyAttribute != null))
                                    {
                                        diagnostics.Add(
                                            DiagnosticMessages.CreateMultipleJsonBodyRouteParameters(
                                                parameter.GetLocation()
                                            )
                                        );
                                    }

                                    jsonBodyAttribute = CacheableJsonBodyAttribute.FromSyntax(
                                        routeContext,
                                        parameter,
                                        attributeSyntax,
                                        cancellationToken
                                    );
                                    if (jsonBodyAttribute == null)
                                    {
                                        return new CollectedRoute(
                                            classDeclarationSymbol,
                                            new List<CacheableRouteAttribute>(),
                                            diagnostics
                                        );
                                    }

                                    foreach (var route in routeAttributes)
                                    {
                                        if (route.RouteParameterNames.Contains(parameterName))
                                        {
                                            diagnostics.Add(
                                                DiagnosticMessages.CreateJsonBodyParameterReferencedInRoutePattern(
                                                    parameter.GetLocation()
                                                )
                                            );
                                            return new CollectedRoute(
                                                classDeclarationSymbol,
                                                new List<CacheableRouteAttribute>(),
                                                diagnostics
                                            );
                                        }
                                    }

                                    foundValidAttribute = true;
                                    break;
                                }

                                case "QueryParametersAttribute":
                                {
                                    if (parameters.Exists(p => p.QueryParametersAttribute != null))
                                    {
                                        diagnostics.Add(
                                            DiagnosticMessages.CreateMultipleQueryParametersRouteParameters(
                                                parameter.GetLocation()
                                            )
                                        );
                                    }

                                    queryParametersAttribute = CacheableQueryParametersAttribute.FromSyntax(
                                        routeContext,
                                        parameter,
                                        attributeSyntax,
                                        cancellationToken
                                    );
                                    if (queryParametersAttribute == null)
                                    {
                                        return new CollectedRoute(
                                            classDeclarationSymbol,
                                            new List<CacheableRouteAttribute>(),
                                            diagnostics
                                        );
                                    }

                                    foreach (var route in routeAttributes)
                                    {
                                        if (route.RouteParameterNames.Contains(parameterName))
                                        {
                                            diagnostics.Add(
                                                DiagnosticMessages.CreateQueryParametersParameterReferencedInRoutePattern(
                                                    parameter.GetLocation()
                                                )
                                            );
                                            return new CollectedRoute(
                                                classDeclarationSymbol,
                                                new List<CacheableRouteAttribute>(),
                                                diagnostics
                                            );
                                        }
                                    }

                                    foundValidAttribute = true;
                                    break;
                                }

                                default:
                                {
                                    // Nothing to do.
                                    break;
                                }
                            }
                        }
                    }

                    if (!foundValidAttribute)
                    {
                        diagnostics.Add(
                            DiagnosticMessages.CreateInvalidRouteParameterType(
                                parameter.GetLocation(),
                                parameterName,
                                typeName
                            )
                        );

                        return new CollectedRoute(
                            classDeclarationSymbol,
                            new List<CacheableRouteAttribute>(),
                            diagnostics
                        );
                    }

                    break;
                }
            }

            var location = CacheableLocation.FromLocation(parameter.GetLocation());
            parameters.Add(
                new CacheableRouteAttribute.CacheableRouteParameter(
                    parameterName,
                    CacheableTypeSymbol.FromITypeSymbol(
                        routeContext.SymbolCache,
                        routeContext.ReportDiagnostic,
                        parameterType
                    ),
                    jsonBodyAttribute,
                    queryParametersAttribute,
                    location
                )
            );
        }

        if (acceptContentTypes.Count == 0)
        {
            var jsonParameter = parameters.Find(p => p.JsonBodyAttribute != null);
            var mimeType = jsonParameter?.JsonBodyAttribute?.MimeType;
            if (!string.IsNullOrEmpty(mimeType))
            {
                _ = acceptContentTypes.Add(mimeType!);
            }
        }

        if (methodDeclarationSyntax.Parent is ClassDeclarationSyntax classDeclarationSyntax)
        {
            if (!classDeclarationSyntax.Modifiers.Any(m => m.IsKind(SyntaxKind.PartialKeyword)))
            {
                diagnostics.Add(
                    DiagnosticMessages.CreateRouteContainingClassIsNotPartial(
                        classDeclarationSyntax.GetLocation(),
                        classDeclarationSyntax.Identifier.Text
                    )
                );

                return new CollectedRoute(classDeclarationSymbol, new List<CacheableRouteAttribute>(), diagnostics);
            }
        }

        var isStatic = methodSymbol.IsStatic;
        var methodName = methodSymbol.Name;
        var cacheableRouteAttribute = new CacheableRouteAttribute(
            isStatic,
            methodName,
            parameters,
            routeAttributes,
            acceptContentTypes,
            authorizeAttributes
        );
        var generators = new List<CacheableRouteAttribute> { cacheableRouteAttribute };
        return new CollectedRoute(classDeclarationSymbol, generators, diagnostics);
    }

    private static IEnumerable<RouteAttributesExecutor?> Combine(
        ImmutableArray<CollectedRoute?> array,
        CancellationToken cancellationToken
    )
    {
        var set = new Dictionary<string, CollectedRoute>();
        foreach (var item in array)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (item == null)
            {
                continue;
            }

            var fullName = item.TypeDeclarationSymbol.FullName;
            if (!set.TryGetValue(fullName, out var originalItem))
            {
                set.Add(fullName, item);
            }
            else
            {
                originalItem.Generators.AddRange(item.Generators);
                originalItem.Diagnostics.AddRange(item.Diagnostics);
            }
        }

        foreach (var item in set)
        {
            var value = item.Value;
            yield return new RouteAttributesExecutor(value.TypeDeclarationSymbol, value.Generators, value.Diagnostics);
        }
    }

    private sealed class CollectedRoute
    {
        public CacheableTypeDeclarationSymbol TypeDeclarationSymbol { get; }

        public List<CacheableRouteAttribute> Generators { get; }

        public List<Diagnostic> Diagnostics { get; }

        public CollectedRoute(
            CacheableTypeDeclarationSymbol typeDeclarationSymbol,
            List<CacheableRouteAttribute> generators,
            List<Diagnostic> diagnostics
        )
        {
            this.TypeDeclarationSymbol = typeDeclarationSymbol;
            this.Generators = generators;
            this.Diagnostics = diagnostics;
        }
    }
}
