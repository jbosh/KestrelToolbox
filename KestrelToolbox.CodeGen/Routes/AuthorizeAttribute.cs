// <copyright file="AuthorizeAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Routes;

/// <summary>
/// Representation of AuthorizeAttribute.
/// </summary>
public sealed class AuthorizeAttribute : IEquatable<AuthorizeAttribute>
{
    /// <summary>
    /// Gets the type of attribute.
    /// </summary>
    public CacheableTypeSymbol Type { get; }

    /// <summary>
    /// Gets the arguments given to the attribute.
    /// </summary>
    public List<CacheableArgument> Arguments { get; }

    private AuthorizeAttribute(CacheableTypeSymbol type, List<CacheableArgument> arguments)
    {
        this.Type = type;
        this.Arguments = arguments;
    }

    /// <summary>
    /// Gets a new instance of <see cref="AuthorizeAttribute"/> from attribute.
    /// </summary>
    /// <param name="context">Context for the request.</param>
    /// <param name="attributeSyntax">RouteAttribute.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The new <see cref="AuthorizeAttribute"/> or null if failure.</returns>
    public static AuthorizeAttribute? FromSyntax(
        RouteAttributesGeneratorContext context,
        AttributeSyntax attributeSyntax,
        CancellationToken cancellationToken
    )
    {
        var semanticModel = context.SemanticModel;
        var type = semanticModel.GetTypeInfo(attributeSyntax.Name, cancellationToken);
        if (type.Type == null)
        {
            return null;
        }

        var cacheableType = CacheableTypeSymbol.FromITypeSymbol(
            context.SymbolCache,
            context.ReportDiagnostic,
            type.Type
        );
        var arguments = attributeSyntax.ArgumentList?.Arguments;
        var cacheableArguments = new List<CacheableArgument>();
        if (arguments == null || arguments.Value.Count == 0)
        {
            return new AuthorizeAttribute(cacheableType, cacheableArguments);
        }

        for (var argumentIndex = 0; argumentIndex < arguments.Value.Count; argumentIndex++)
        {
            var argument = arguments.Value[argumentIndex];
            cacheableArguments.Add(CacheableArgument.FromArgumentSyntax(semanticModel, argument, cancellationToken));
        }

        return new AuthorizeAttribute(cacheableType, cacheableArguments);
    }

    /// <inheritdoc />
    public bool Equals(AuthorizeAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Type.Equals(other.Type) && this.Arguments.SequenceEqual(other.Arguments);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not AuthorizeAttribute other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Type, this.Arguments);
    }

    /// <summary>
    /// Compares two <see cref="AuthorizeAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(AuthorizeAttribute? left, AuthorizeAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="AuthorizeAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(AuthorizeAttribute? left, AuthorizeAttribute? right)
    {
        return !Equals(left, right);
    }
}
