// <copyright file="AcceptContentTypesAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Routes;

/// <summary>
/// Representation of AcceptContentTypesAttribute.
/// </summary>
public static class AcceptContentTypesAttribute
{
    /// <summary>
    /// Gets a <see cref="AcceptContentTypesAttribute"/> from <paramref name="attributeSyntax"/>.
    /// </summary>
    /// <param name="context">Context to report diagnostics and get semantic model.</param>
    /// <param name="attributeSyntax">Syntax containing attribute info.</param>
    /// <returns>Set of acceptable content types.</returns>
    public static HashSet<string>? FromSyntax(RouteAttributesGeneratorContext context, AttributeSyntax attributeSyntax)
    {
        var semanticModel = context.SemanticModel;
        var types = new HashSet<string>();

        var arguments = attributeSyntax.ArgumentList?.Arguments;
        if (arguments == null || arguments.Value.Count == 0)
        {
            return types;
        }

        for (var argumentIndex = 0; argumentIndex < arguments.Value.Count; argumentIndex++)
        {
            var argument = arguments.Value[argumentIndex];
            var value = semanticModel.GetConstantValue(argument.Expression);
            if (argument.NameColon != null || argument.NameEquals != null)
            {
                return null;
            }

            if (value.Value is not string stringValue)
            {
                return null;
            }

            _ = types.Add(stringValue);
        }

        return types;
    }
}
