// <copyright file="RouteAttributesGeneratorContext.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Routes;

/// <summary>
/// Context for generation during <see cref="RouteAttributesGenerator"/>.
/// </summary>
public sealed class RouteAttributesGeneratorContext
{
    /// <summary>
    /// Gets the semantic model.
    /// </summary>
    public SemanticModel SemanticModel { get; }

    /// <summary>
    /// Gets the set of symbols that have been added so far by fully qualified name.
    /// </summary>
    /// <remarks>This cache is used to prevent infinite recursion when fetching types.</remarks>
    public Dictionary<string, CacheableTypeSymbol> SymbolCache { get; } = new();

    /// <summary>
    /// Gets the list of diagnostics that have been added.
    /// </summary>
    public List<Diagnostic> Diagnostics { get; } = new();

    /// <summary>
    /// Initializes a new instance of the <see cref="RouteAttributesGeneratorContext"/> class.
    /// </summary>
    /// <param name="semanticModel">Semantic model used for parsing.</param>
    public RouteAttributesGeneratorContext(SemanticModel semanticModel)
    {
        this.SemanticModel = semanticModel;
    }

    /// <summary>
    /// Adds a diagnostic to be reported for compilation.
    /// </summary>
    /// <param name="diagnostic">Diagnostic.</param>
    public void ReportDiagnostic(Diagnostic diagnostic) => this.Diagnostics.Add(diagnostic);
}
