// <copyright file="RouteAttributesExecutor.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

// #define PRINT_BUILD_COUNT
using System;
using System.Collections.Generic;
using System.Linq;
using KestrelToolbox.CodeGen.Cacheables;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen.Routes;

/// <summary>
/// Executor to generate route attribute stuff.
/// </summary>
public sealed class RouteAttributesExecutor : IEquatable<RouteAttributesExecutor>
{
#if PRINT_BUILD_COUNT
    private static readonly Dictionary<string, int> BuildCounts = new();
#endif // PRINT_BUILD_COUNT

    private readonly List<Diagnostic> diagnostics;
    private readonly CacheableTypeDeclarationSymbol typeDeclarationSymbol;
    private readonly List<CacheableRouteAttribute> routes;

    /// <summary>
    /// Initializes a new instance of the <see cref="RouteAttributesExecutor"/> class.
    /// </summary>
    /// <param name="typeDeclarationSymbol">Overall class symbol for this <see cref="Serializable.SerializableAttributesExecutor"/>.</param>
    /// <param name="routes">Routes generate methods for.</param>
    /// <param name="diagnostics">Diagnostics for this <paramref name="typeDeclarationSymbol"/> type.</param>
    public RouteAttributesExecutor(
        CacheableTypeDeclarationSymbol typeDeclarationSymbol,
        List<CacheableRouteAttribute> routes,
        List<Diagnostic> diagnostics
    )
    {
        this.typeDeclarationSymbol = typeDeclarationSymbol;
        this.routes = routes;
        this.diagnostics = diagnostics;
    }

    /// <summary>
    /// Generate code for this class.
    /// </summary>
    /// <param name="context">Context of source generation.</param>
    public void Execute(SourceProductionContext context)
    {
        var cancellationToken = context.CancellationToken;
        if (this.diagnostics.Count != 0)
        {
            foreach (var diagnostic in this.diagnostics)
            {
                context.ReportDiagnostic(diagnostic);
            }
        }

        var classSymbol = this.typeDeclarationSymbol;

        var globalBuilder = new CodeBuilder();

#if PRINT_BUILD_COUNT
        lock (BuildCounts)
        {
            _ = BuildCounts.TryGetValue(classSymbol.FullName, out var buildCount);
            BuildCounts[classSymbol.FullName] = buildCount + 1;
            globalBuilder.AppendLine($"// Route build count: {buildCount}");
        }
#endif // PRINT_BUILD_COUNT

        if (classSymbol.Namespace.Length != 0)
        {
            globalBuilder.AppendLine($"namespace {classSymbol.Namespace};");
            globalBuilder.AppendLine();
        }

        globalBuilder.AppendLine("#nullable enable");

        var getRoutesXmlDoc = """
            /// <summary>
            /// Gets all routes that were tagged on this type using <see cref="global::KestrelToolbox.Web.RouteAttribute" />.
            /// </summary>
            /// <returns>
            /// The collection of <see cref="global::KestrelToolbox.Web.RouteInfo "/> elements.
            /// </returns>
            """;
        var staticBuilder = new CodeBuilder();
        staticBuilder.AppendAndSplitLines(getRoutesXmlDoc);
        staticBuilder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
        staticBuilder.AppendLine(
            "public static global::System.Collections.Generic.IEnumerable<global::KestrelToolbox.Web.RouteInfo> GetRoutes()"
        );
        staticBuilder.PushBlock();

        var instanceBuilder = new CodeBuilder();
        instanceBuilder.AppendAndSplitLines(getRoutesXmlDoc);
        instanceBuilder.AppendLine("[global::System.CodeDom.Compiler.GeneratedCode(\"KestrelToolbox\", \"2.0.0\")]");
        instanceBuilder.AppendLine(
            "public global::System.Collections.Generic.IEnumerable<global::KestrelToolbox.Web.RouteInfo> GetRoutes()"
        );
        instanceBuilder.PushBlock();

        // Build up class list so we can have the correct type.
        foreach (var containingType in classSymbol.ContainingTypes)
        {
            globalBuilder.AppendLine(containingType);
            globalBuilder.PushBlock();
        }

        var hasStaticRoutes = false;
        var hasInstanceRoutes = false;
        foreach (var route in this.routes)
        {
            cancellationToken.ThrowIfCancellationRequested();
            globalBuilder.ResetVariableNames();
            try
            {
                CodeBuilder builder;
                if (route.IsStatic)
                {
                    hasStaticRoutes = true;
                    builder = staticBuilder;
                }
                else
                {
                    hasInstanceRoutes = true;
                    builder = instanceBuilder;
                }

                string localMethodName;
                if (route.ShouldGenerate)
                {
                    localMethodName = builder.GetVariableName("method");
                }
                else if (route.IsStatic)
                {
                    localMethodName = route.MethodName;
                }
                else
                {
                    localMethodName = $"this.{route.MethodName}";
                }

                var authorizationPoliciesName = default(string);
                if (route.AuthorizeAttributes.Count != 0)
                {
                    authorizationPoliciesName = builder.GetVariableName("authorizationPolicies");
                    builder.AppendLine($"var {authorizationPoliciesName} = new[]");
                    using (builder.AppendBlock(postfix: ";"))
                    {
                        foreach (var authorization in route.AuthorizeAttributes)
                        {
                            var arguments = authorization
                                .Arguments.Where(a => a.NameEquals == null)
                                .Select(a => a.NameColon != null ? $"{a.NameColon}: {a.Value}" : $"{a.Value}")
                                .ToArray();
                            var properties = authorization
                                .Arguments.Where(a => a.NameEquals != null)
                                .Select(a => $"{a.NameEquals} = {a.Value}")
                                .ToArray();
                            var hasProperties = properties.Length != 0;

                            builder.AppendLine(
                                $"new {authorization.Type.FullNameNonNullable}({string.Join(", ", arguments)}){(hasProperties ? string.Empty : ",")}"
                            );
                            if (hasProperties)
                            {
                                using (builder.AppendBlock(postfix: ","))
                                {
                                    foreach (var property in properties)
                                    {
                                        builder.AppendLine($"{property},");
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (var attribute in route.Attributes)
                {
                    builder.AppendLine("yield return new global::KestrelToolbox.Web.RouteInfo()");
                    using (builder.AppendBlock(postfix: ";"))
                    {
                        builder.AppendLine($"Pattern = {attribute.Pattern.ToLiteral()},");
                        builder.AppendLine(
                            $"Methods = new[] {{{string.Join(", ", attribute.Methods.Select(m => m.ToLiteral()))}}},"
                        );
                        builder.AppendLine($"Callback = {localMethodName},");
                        if (authorizationPoliciesName != null)
                        {
                            builder.AppendLine($"AuthorizationPolicies = {authorizationPoliciesName}");
                        }
                    }
                }

                if (route.ShouldGenerate)
                {
                    if (!route.Generate(context, builder, localMethodName))
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                // Nothing to do, reporting should send true error to the user.
                context.ReportDiagnostic(DiagnosticMessages.CreateGenericException(ex));
            }
        }

        staticBuilder.PopBlock();
        instanceBuilder.PopBlock();

        if (hasStaticRoutes)
        {
            globalBuilder.Append(staticBuilder);
        }

        if (hasInstanceRoutes)
        {
            globalBuilder.Append(instanceBuilder);
        }

        for (var i = 0; i < classSymbol.ContainingTypes.Length; i++)
        {
            globalBuilder.PopBlock();
        }

        var contents = globalBuilder.ToString();
        var sourcePath = $"Routes/{classSymbol.FullName}.g.cs";
        sourcePath = sourcePath.Replace("global::", string.Empty).Replace('<', '(').Replace('>', ')');
        context.AddSource(sourcePath, contents);
    }

    /// <inheritdoc />
    public bool Equals(RouteAttributesExecutor? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.diagnostics.SequenceEqual(other.diagnostics)
            && this.typeDeclarationSymbol.Equals(other.typeDeclarationSymbol)
            && this.routes.SequenceEqual(other.routes);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not RouteAttributesExecutor other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.typeDeclarationSymbol, this.routes.AggregateHashCode());
    }

    /// <summary>
    /// Compares two <see cref="RouteAttributesExecutor"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(RouteAttributesExecutor? left, RouteAttributesExecutor? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="RouteAttributesExecutor"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(RouteAttributesExecutor? left, RouteAttributesExecutor? right)
    {
        return !Equals(left, right);
    }
}
