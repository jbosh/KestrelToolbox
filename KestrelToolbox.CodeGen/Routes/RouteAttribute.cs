// <copyright file="RouteAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace KestrelToolbox.CodeGen.Routes;

/// <summary>
/// Representation of RouteAttribute.
/// </summary>
public sealed class RouteAttribute : IEquatable<RouteAttribute>
{
    /// <summary>
    /// Gets URL that you want to call this method with.
    /// Some examples:
    /// - /hello - Matches any path that starts with exactly `/hello`
    /// - /hello:{name:alpha} - Matches any path that starts with `/hello/[a-zA-Z]+` and passes value to `name` parameter.
    /// - /hello/{*world} - Matches any path that matches `/hello/[^/]*` and passes the value to `world` parameter.
    /// - /hello/{**worlds} - Matches any path that matches `/hello/.*` and passes the value to `worlds` parameter.
    ///
    /// You can find more information about paths at https://learn.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-8.0#route-constraints.
    /// </summary>
    public string Pattern { get; }

    /// <summary>
    /// Gets a list of methods that this route will accept.
    /// </summary>
    public HashSet<string> Methods { get; }

    /// <summary>
    /// Gets the parameter names for this route's pattern.
    /// </summary>
    public HashSet<string> RouteParameterNames { get; }

    private static readonly char[] MethodSeparatorChars = new[] { ' ', ',' };

    /// <inheritdoc />
    public override string ToString() => this.Pattern;

    private RouteAttribute(string pattern, HashSet<string> methods, HashSet<string> routeParameterNames)
    {
        this.Pattern = pattern;
        this.Methods = methods;
        this.RouteParameterNames = routeParameterNames;
    }

    /// <summary>
    /// Gets a new instance of <see cref="RouteAttribute"/> from attribute.
    /// </summary>
    /// <param name="context">Context for the request.</param>
    /// <param name="attributeSyntax">RouteAttribute.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The new <see cref="RouteAttribute"/> or null if failure.</returns>
    public static RouteAttribute? FromSyntax(
        RouteAttributesGeneratorContext context,
        AttributeSyntax attributeSyntax,
        CancellationToken cancellationToken
    )
    {
        var semanticModel = context.SemanticModel;
        var pattern = default(string);
        var methods = new HashSet<string> { "GET" };
        var parameters = new HashSet<string>();

        var arguments = attributeSyntax.ArgumentList?.Arguments;
        if (arguments == null || arguments.Value.Count == 0)
        {
            // Compiler should take care of this.
            return null;
        }

        for (var argumentIndex = 0; argumentIndex < arguments.Value.Count; argumentIndex++)
        {
            var argument = arguments.Value[argumentIndex];
            var value = semanticModel.GetConstantValue(argument.Expression, cancellationToken);
            if (argument.NameColon != null)
            {
                var name = argument.NameColon.Name.Identifier.Text;
                switch (name)
                {
                    case "pattern":
                    {
                        // Pattern
                        if (value.Value is not string stringValue)
                        {
                            // Compiler should take care of this one.
                            return null;
                        }

                        pattern = stringValue;
                        parameters = GetParameterNamesFromPattern(pattern);
                        if (parameters == null)
                        {
                            return null;
                        }

                        break;
                    }

                    case "methods":
                    {
                        // Methods
                        if (value.Value is not string stringValue)
                        {
                            // Compiler should take care of this one.
                            return null;
                        }

                        methods = GetMethodsFromString(stringValue);
                        break;
                    }

                    default:
                    {
                        // Compiler should take care of this one.
                        return null;
                    }
                }
            }
            else
            {
                switch (argumentIndex)
                {
                    case 0:
                    {
                        // Pattern
                        if (value.Value is not string stringValue)
                        {
                            // Compiler should take care of this one.
                            return null;
                        }

                        pattern = stringValue;
                        parameters = GetParameterNamesFromPattern(pattern);
                        if (parameters == null)
                        {
                            return null;
                        }

                        break;
                    }

#pragma warning disable S2583 // Linter bug, I hope.
                    case 1:
#pragma warning restore S2583
                    {
                        // Methods
                        if (value.Value is not string stringValue)
                        {
                            // Compiler should take care of this one.
                            return null;
                        }

                        methods = GetMethodsFromString(stringValue);
                        break;
                    }

                    default:
                    {
                        // Compiler should take care of this one.
                        return null;
                    }
                }
            }
        }

        if (pattern == null)
        {
            // Compiler should take care of this one.
            return null;
        }

        return new RouteAttribute(pattern, methods, parameters);

        static HashSet<string> GetMethodsFromString(string methods)
        {
            var result = new HashSet<string>();
            foreach (
                var method in methods
                    .Split(MethodSeparatorChars, StringSplitOptions.RemoveEmptyEntries)
                    .Select(m => m.Trim().ToUpperInvariant())
            )
            {
                _ = result.Add(method.Trim());
            }

            return result;
        }

        HashSet<string>? GetParameterNamesFromPattern(string routePattern)
        {
            parameters.Clear();
            foreach (var match in Regex.Matches(routePattern, @"\{\**([^:\}]+)").Cast<Match>())
            {
                var parameterName = match.Groups[1].Value;
                if (!parameters.Add(parameterName))
                {
                    context.ReportDiagnostic(
                        DiagnosticMessages.CreateDuplicateParameterNameInRoute(
                            attributeSyntax.GetLocation(),
                            parameterName
                        )
                    );
                    return null;
                }
            }

            return parameters;
        }
    }

    /// <inheritdoc />
    public bool Equals(RouteAttribute? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return this.Pattern == other.Pattern
            && this.Methods.SetEquals(other.Methods)
            && this.RouteParameterNames.SetEquals(other.RouteParameterNames);
    }

    /// <inheritdoc />
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj is not RouteAttribute other)
        {
            return false;
        }

        return this.Equals(other);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        return HashCode.Combine(this.Pattern, this.Methods, this.RouteParameterNames);
    }

    /// <summary>
    /// Compares two <see cref="RouteAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are equivalent.</returns>
    public static bool operator ==(RouteAttribute? left, RouteAttribute? right)
    {
        return Equals(left, right);
    }

    /// <summary>
    /// Compares two <see cref="RouteAttribute"/> instances for equality.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two instance are not equivalent.</returns>
    public static bool operator !=(RouteAttribute? left, RouteAttribute? right)
    {
        return !Equals(left, right);
    }
}
