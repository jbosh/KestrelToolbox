// <copyright file="CodeBuilder.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Text;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// Class for building code like a <see cref="StringBuilder"/>.
/// </summary>
public partial class CodeBuilder
{
    /// <summary>
    /// Gets a value indicating whether this <see cref="CodeBuilder"/> is empty.
    /// </summary>
    public bool Empty => this.builder.Length == 0;

    /// <summary>
    /// Gets a unique variable name based on line number.
    /// </summary>
    /// <returns>The new variable name.</returns>
    public string GetVariableName() => $"var_{this.GetNextVariableNumber(string.Empty)}";

    /// <summary>
    /// Gets a unique variable name based on line number.
    /// </summary>
    /// <param name="name">Name to prefix variable with.</param>
    /// <returns>The new variable name.</returns>
    public string GetVariableName(string name) => $"{name}_{this.GetNextVariableNumber(name)}";

    /// <summary>
    /// Clears all variable names as if at the beginning of a new method.
    /// </summary>
    public void ResetVariableNames() => this.variableNumbers.Clear();

    private readonly StringBuilder builder = new();
    private readonly Dictionary<string, int> variableNumbers = new();
    private int indentation;
    private bool danglingIndent;

    /// <summary>
    /// Appends a raw value to the builder. Does not do anything with newlines.
    /// </summary>
    /// <param name="s">String to write.</param>
    public void Append(string s)
    {
        _ = this.builder.Append(s);
    }

    /// <summary>
    /// Appends another code builder to this builder.
    /// </summary>
    /// <param name="s">Other builder to append.</param>
    public void Append(CodeBuilder s)
    {
        if (s.builder.Length == 0)
        {
            // Split will always give at least one string.
            return;
        }

        foreach (var split in s.builder.ToString().Split('\n'))
        {
            this.AppendLine(split);
        }
    }

    /// <summary>
    /// Appends an empty new line.
    /// </summary>
    public void AppendLine()
    {
        this.AppendDanglingIndent();
        _ = this.builder.Append('\n');
    }

    /// <summary>
    /// Appends a new line at the current indentation level.
    /// </summary>
    /// <param name="s">String to write.</param>
    public void AppendLine(string s)
    {
        this.AppendDanglingIndent();
        if (!string.IsNullOrWhiteSpace(s))
        {
            this.WriteIndentation();
            _ = this.builder.Append(s);
        }

        _ = this.builder.Append('\n');
    }

    /// <summary>
    /// Appends a large block into separate lines.
    /// </summary>
    /// <param name="s">Large string with multiple lines to write.</param>
    public void AppendAndSplitLines(string s)
    {
        if (string.IsNullOrWhiteSpace(s))
        {
            return;
        }

        var lines = s.Split('\n');
        foreach (var line in lines)
        {
            this.AppendLine(line);
        }
    }

    /// <summary>
    /// Writes a code block and attaches it to the previous block.
    /// </summary>
    /// <param name="prefix">Line to write at the start of the block.</param>
    /// <param name="postfix">Line to write at the end of the block.</param>
    /// <returns>this.</returns>
    public CodeBlock AppendBlock(string? prefix = null, string? postfix = null)
    {
        if (prefix != null)
        {
            this.danglingIndent = false;
            this.AppendLine(prefix);
        }

        this.PushBlock();
        return new CodeBlock(this, postfix);
    }

    /// <summary>
    /// Appends a preprocessor strings.
    /// </summary>
    /// <param name="s">The string to append.</param>
    public void AppendPreprocessor(string s)
    {
        _ = this.builder.Append(s).Append('\n');
    }

    /// <summary>
    /// Write a brace and increase indentation by 1.
    /// </summary>
    public void PushBlock()
    {
        this.AppendLine("{");
        this.indentation++;
    }

    /// <summary>
    /// Write a brace and increase indentation by 1.
    /// </summary>
    /// <param name="line">Line write at start of block.</param>
    public void PushBlock(string line)
    {
        this.danglingIndent = false;
        this.AppendLine(line);
        this.PushBlock();
    }

    /// <summary>
    /// Decrease indentation by 1 and write a brace.
    /// </summary>
    /// <param name="suffix">Suffix to append after closing brace.</param>
    public void PopBlock(string? suffix = null)
    {
        this.indentation--;
        this.WriteIndentation();
        _ = this.builder.Append('}');
        if (suffix != null)
        {
            _ = this.builder.Append(suffix);
        }

        _ = this.builder.Append('\n');
        this.danglingIndent = true;
    }

    /// <summary>
    /// Pushes indentation by 1.
    /// </summary>
    public void PushIndent()
    {
        this.indentation++;
    }

    /// <summary>
    /// Pops indentation by 1.
    /// </summary>
    public void PopIndent()
    {
        this.indentation--;
    }

    /// <inheritdoc />
    public override string ToString()
    {
        return this.builder.ToString();
    }

    private void AppendDanglingIndent()
    {
        if (this.danglingIndent)
        {
            this.danglingIndent = false;
            this.AppendLine();
        }
    }

    private void WriteIndentation()
    {
        for (var i = 0; i < this.indentation; i++)
        {
            _ = this.builder.Append("    ");
        }
    }

    private int GetNextVariableNumber(string name)
    {
        if (!this.variableNumbers.TryGetValue(name, out var value))
        {
            value = -1;
        }

        value++;
        this.variableNumbers[name] = value;
        return value;
    }

    /// <summary>
    /// Structure for simplifying code block popping.
    /// </summary>
    public readonly struct CodeBlock : IDisposable
    {
        private readonly CodeBuilder builder;
        private readonly string? postfixLine;

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeBlock"/> struct.
        /// </summary>
        /// <param name="builder">The builder to use for this <see cref="CodeBlock"/>.</param>
        /// <param name="postfixLine">Line to write at the end of the block.</param>
        public CodeBlock(CodeBuilder builder, string? postfixLine = null)
        {
            this.builder = builder;
            this.postfixLine = postfixLine;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.builder.PopBlock(this.postfixLine);
        }
    }
}
