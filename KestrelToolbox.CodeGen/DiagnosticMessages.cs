// <copyright file="DiagnosticMessages.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Globalization;
using Microsoft.CodeAnalysis;

namespace KestrelToolbox.CodeGen;

/// <summary>
/// All of the <see cref="DiagnosticDescriptor"/>s that this library can generate.
/// </summary>
public static class DiagnosticMessages
{
    private static readonly DiagnosticDescriptor RouteUnreferencedParameter = new(
        "KTB0001",
        "RouteAttribute invalid",
        "Route parameter {0} is not referenced in the pattern",
        "Usage",
        DiagnosticSeverity.Error,
        true,
        "The route has a named parameter that does not appear in the route pattern."
    );

    private static readonly DiagnosticDescriptor SerializableCustomMethodNotFound = new(
        "KTB0002",
        "Serializable method not found",
        "{0} custom {1} method {2} was not found or had an invalid signature",
        "Usage",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor SerializableEnumTypeNotEnum = new(
        "KTB0003",
        "EnumSerializable type not an enum",
        "EnumSerializable type {0} is {1}. An enum is required.",
        "Usage",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableTypeInvalid = new(
        "KTB0004",
        "JsonSerializable type invalid",
        "JsonSerializable type {0} is {1}. An array, class, struct, enum, or interface is required.",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableTypeParsingDisabled = new(
        "KTB0005",
        "Serializable type serialization disabled",
        "Serializable type {0} has had required parsing generation disabled but is required by {1}",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableTypeSerializationDisabled = new(
        "KTB0006",
        "Serializable type parsing disabled",
        "Serializable type {0} has had required serialization generation disabled but is required by {1}",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor PropertyTypeUnknown = new(
        "KTB0007",
        "Property type invalid",
        "Property type {0} is unknown and could not be generated in {1}",
        "Usage",
        DiagnosticSeverity.Error,
        true,
        "Property type is unknown and requires custom parsing to handle."
    );

    private static readonly DiagnosticDescriptor GenericException = new(
        "KTB0008",
        "Generic exception thrown",
        "Generic exception: {0} at {1}",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor DuplicateParameterNameInRoute = new(
        "KTB0009",
        "Invalid RouteAttribute",
        "Duplicate parameter names in route pattern {0}",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor RouteParameterInvalid = new(
        "KTB0010",
        "Invalid RouteAttribute",
        "Parameter {0} is required to be type {1}. Was of type {2}.",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor UnreferencedRouteParameter = new(
        "KTB0011",
        "Invalid RouteAttribute",
        "Route parameter {0} is unreferenced in the pattern",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor MultipleJsonBodyRouteParameters = new(
        "KTB0012",
        "Invalid RouteAttribute",
        "Multiple parameters are JsonBodyAttribute",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor JsonBodyParameterReferencedInRoutePattern = new(
        "KTB0013",
        "Invalid RouteAttribute",
        "Parameter with JsonBodyAttribute was referenced by pattern",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor EnumSerializableDuplicateName = new(
        "KTB0014",
        "EnumSerializable duplicate name",
        "EnumSerializable type {0} contains duplicates of name {1}",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor InvalidRouteParameterType = new(
        "KTB0015",
        "Invalid RouteAttribute",
        "Route parameter {0} of type {1} is an unsupported type. Did you mean to specify JsonBodyAttribute or QueryParametersAttribute.",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor RouteContainingClassIsNotPartial = new(
        "KTB0015",
        "Invalid RouteAttribute",
        "Class {0} containing a RouteAttribute is not partial",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializablePropertyHasMultipleNames = new(
        "KTB0016",
        "JsonSerializable type invalid",
        "JsonSerializable type {0} has a property {1} that has multiple name attributes",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableDuplicateName = new(
        "KTB0017",
        "JsonSerializable duplicate name",
        "JsonSerializable type {0} contains duplicates of name {1}",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableDuplicateTypeDiffers = new(
        "KTB0018",
        "JsonSerializable duplicate type",
        "JsonSerializable type {0} duplicated and does not have matching settings",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableSourceGenerationInvalid = new(
        "KTB0019",
        "JsonSerializable invalid source generation",
        "JsonSerializable type {0} requires {1} but explicitly disables it",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableViolatesNullConstraints = new(
        "KTB0020",
        "JsonSerializable violates null constraints",
        "JsonSerializable type {0} has a property {1} that is not required and not nullable",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor JsonSerializableDidNotHaveAnyMembers = new(
        "KTB0021",
        "JsonSerializable had no members",
        "JsonSerializable on {0} did not have any members to serialize or parse",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor QueryStringSerializableTypeInvalid = new(
        "KTB0022",
        "QueryStringSerializable type invalid",
        "QueryStringSerializable type {0} is {1}. A class or struct is required.",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor QueryStringSerializablePropertyHasMultipleNames = new(
        "KTB0023",
        "JsonSerializable type invalid",
        "JsonSerializable type {0} has a property {1} that has multiple name attributes",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor QueryStringSerializableDuplicateName = new(
        "KTB0024",
        "QueryStringSerializable duplicate name",
        "QueryStringSerializable type {0} contains duplicates of name {1}",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor QueryStringSerializableDidNotHaveAnyMembers = new(
        "KTB0025",
        "QueryStringSerializable had no members",
        "QueryStringSerializable on {0} did not have any members to serialize or parse",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor SerializableMustBePublic = new(
        "KTB0026",
        "{0} must be public",
        "{0} type {1} is inaccessible publicly",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor SerializableAlreadySpecified = new(
        "KTB0027",
        "SerializableAttribute already specified for this type",
        "{0} was already specified",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor CommandLineSerializableTypeInvalid = new(
        "KTB0028",
        "CommandLineSerializable type invalid",
        "CommandLineSerializable type {0} is {1}. A class or struct is required.",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor CommandLineSerializableDuplicateName = new(
        "KTB0029",
        "CommandLineSerializable duplicate name",
        "QueryStringSerializable type {0} contains duplicate name {1}",
        "KestrelToolbox.Inaccessible",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor InvalidEmptyArgumentValueType = new(
        "KTB0030",
        "EmptyArgumentValue invalid parameter type",
        "EmptyArgumentValue expected parameter of type {0} but got {1}",
        "KestrelToolbox.Inaccessible",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor MultiplePositionalArgumentProperties = new(
        "KTB0031",
        "PositionalArgument multiple properties",
        "Multiple PositionalArgumentAttributes were applied to properties",
        "KestrelToolbox.Inaccessible",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor InvalidPositionalArgumentType = new(
        "KTB0032",
        "PositionalArgument invalid type",
        "PositionalArgumentAttributes was not a list or array of strings",
        "KestrelToolbox.Inaccessible",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor CommandLineSerializableViolatesNullConstraints = new(
        "KTB0033",
        "CommandLineSerializable violates null constraints",
        "CommandLineSerializable type {0} has a property {1} that is not required and not nullable",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor SerializableAttributeClassIsNotPartial = new(
        "KTB0034",
        "Invalid SerializableAttribute",
        "Class {0} with SerializableAttribute is not partial",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor TypedefClassIsNotPartial = new(
        "KTB0035",
        "Invalid TypedefAttribute",
        "Class {0} with TypedefAttribute is not partial",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor SerializableDifferentProperties = new(
        "KTB0036",
        "SerializableAttribute had different properties",
        "{0} had different property {1}: '{2}' vs. '{3}'.",
        "Build",
        DiagnosticSeverity.Warning,
        true
    );

    private static readonly DiagnosticDescriptor MultipleQueryParametersRouteParameters = new(
        "KTB0037",
        "Invalid RouteAttribute",
        "Multiple parameters are QueryParametersAttribute",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    private static readonly DiagnosticDescriptor QueryParametersParameterReferencedInRoutePattern = new(
        "KTB0018",
        "Invalid RouteAttribute",
        "Parameter with QueryParametersAttribute was referenced by pattern",
        "Build",
        DiagnosticSeverity.Error,
        true
    );

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="name">Name of the parameter.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateRouteUnreferencedParameter(Location? location, string name) =>
        Diagnostic.Create(RouteUnreferencedParameter, location, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="serializableAttributeType">The type of serializable attribute.</param>
    /// <param name="serializeType">Either parse or serialize.</param>
    /// <param name="methodName">Name of the method.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateSerializableCustomMethodNotFound(
        Location? location,
        string serializableAttributeType,
        string serializeType,
        string methodName
    ) =>
        Diagnostic.Create(
            SerializableCustomMethodNotFound,
            location,
            serializableAttributeType,
            serializeType,
            methodName
        );

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="typeKind">The TypeKind of the type specified.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateSerializableEnumTypeNotEnum(Location? location, string type, TypeKind typeKind) =>
        Diagnostic.Create(SerializableEnumTypeNotEnum, location, type, typeKind.ToString());

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="typeKind">The TypeKind of the type specified.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableTypeInvalid(Location? location, string type, TypeKind typeKind) =>
        Diagnostic.Create(JsonSerializableTypeInvalid, location, type, typeKind.ToString());

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="requiringType">The type that requires <paramref name="type"/>.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableTypeParsingDisabled(
        Location? location,
        string type,
        string requiringType
    ) => Diagnostic.Create(JsonSerializableTypeParsingDisabled, location, type, requiringType);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="requiringType">The type that requires <paramref name="type"/>.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableTypeSerializationDisabled(
        Location? location,
        string type,
        string requiringType
    ) => Diagnostic.Create(JsonSerializableTypeSerializationDisabled, location, type, requiringType);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="parserType">The parser type. This should be full strings like <c>QueryStringParser</c>.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreatePropertyTypeUnknown(Location? location, string type, string parserType) =>
        Diagnostic.Create(PropertyTypeUnknown, location, type, parserType);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="exception">The exception that was caught.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateGenericException(Exception exception) =>
        Diagnostic.Create(GenericException, null, exception.Message, exception.StackTrace);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="name">The name of the duplicate parameter.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateDuplicateParameterNameInRoute(Location? location, string name) =>
        Diagnostic.Create(DuplicateParameterNameInRoute, location, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="parameterIndex">Index of the invalid parameter.</param>
    /// <param name="expectedType">Expected type of the parameter.</param>
    /// <param name="actualType">Actual type the parameter is.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateRouteParameterInvalid(
        Location? location,
        int parameterIndex,
        string expectedType,
        string? actualType
    ) =>
        Diagnostic.Create(
            RouteParameterInvalid,
            location,
            parameterIndex.ToString(CultureInfo.InvariantCulture),
            expectedType,
            actualType
        );

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="name">The name unreferenced.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateUnreferencedRouteParameter(Location? location, string name) =>
        Diagnostic.Create(UnreferencedRouteParameter, location, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateMultipleJsonBodyRouteParameters(Location? location) =>
        Diagnostic.Create(MultipleJsonBodyRouteParameters, location);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonBodyParameterReferencedInRoutePattern(Location? location) =>
        Diagnostic.Create(JsonBodyParameterReferencedInRoutePattern, location);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <param name="name">The duplicate name.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateEnumSerializableDuplicateName(Location? location, string type, string name) =>
        Diagnostic.Create(EnumSerializableDuplicateName, location, type, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="actualType">Actual type the parameter is.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateInvalidRouteParameterType(
        Location? location,
        string parameterName,
        string? actualType
    ) => Diagnostic.Create(InvalidRouteParameterType, location, parameterName, actualType);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="className">Name of the class.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateRouteContainingClassIsNotPartial(Location? location, string className) =>
        Diagnostic.Create(RouteContainingClassIsNotPartial, location, className);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">Name of the class.</param>
    /// <param name="propertyName">Name of the property.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializablePropertyHasMultipleNames(
        Location? location,
        string type,
        string propertyName
    ) => Diagnostic.Create(JsonSerializablePropertyHasMultipleNames, location, type, propertyName);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <param name="name">The duplicate name.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableDuplicateName(Location? location, string type, string name) =>
        Diagnostic.Create(JsonSerializableDuplicateName, location, type, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableDuplicateTypeDiffers(Location? location, string type) =>
        Diagnostic.Create(JsonSerializableDuplicateTypeDiffers, location, type);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <param name="sourceGenerationType">The type of source generation. Usually 'parsing' or 'serialization'.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableSourceGenerationInvalid(
        Location? location,
        string type,
        string sourceGenerationType
    ) => Diagnostic.Create(JsonSerializableSourceGenerationInvalid, location, type, sourceGenerationType);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <param name="propertyName">The name of the property with invalid constraints.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableViolatesNullConstraints(
        Location? location,
        string type,
        string? propertyName
    ) => Diagnostic.Create(JsonSerializableViolatesNullConstraints, location, type, propertyName);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateJsonSerializableDidNotHaveAnyMembers(Location? location, string type) =>
        Diagnostic.Create(JsonSerializableDidNotHaveAnyMembers, location, type);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="typeKind">The TypeKind of the type specified.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateQueryStringSerializableTypeInvalid(
        Location? location,
        string type,
        TypeKind typeKind
    ) => Diagnostic.Create(QueryStringSerializableTypeInvalid, location, type, typeKind.ToString());

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">Name of the class.</param>
    /// <param name="propertyName">Name of the property.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateQueryStringSerializablePropertyHasMultipleNames(
        Location? location,
        string type,
        string propertyName
    ) => Diagnostic.Create(QueryStringSerializablePropertyHasMultipleNames, location, type, propertyName);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <param name="name">The duplicate name.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateQueryStringSerializableDuplicateName(Location? location, string type, string name) =>
        Diagnostic.Create(QueryStringSerializableDuplicateName, location, type, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateQueryStringSerializableDidNotHaveAnyMembers(Location? location, string type) =>
        Diagnostic.Create(QueryStringSerializableDidNotHaveAnyMembers, location, type);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="attributeType">Attribute type (JsonSerializableAttribute, EnumSerializableAttribute, etc...)</param>
    /// <param name="type">The enum type.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreatSerializableMustBePublic(Location? location, string attributeType, string type) =>
        Diagnostic.Create(
            new DiagnosticDescriptor(
                SerializableMustBePublic.Id,
                string.Format(
                    CultureInfo.InvariantCulture,
                    SerializableMustBePublic.Title.ToString(CultureInfo.InvariantCulture),
                    attributeType
                ),
                SerializableMustBePublic.MessageFormat,
                SerializableMustBePublic.Category,
                SerializableMustBePublic.DefaultSeverity,
                SerializableMustBePublic.IsEnabledByDefault
            ),
            location,
            attributeType,
            type
        );

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateSerializableAlreadySpecified(Location? location, string type) =>
        Diagnostic.Create(SerializableAlreadySpecified, location, type);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="typeKind">The TypeKind of the type specified.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateCommandLineSerializableTypeInvalid(
        Location? location,
        string type,
        TypeKind typeKind
    ) => Diagnostic.Create(CommandLineSerializableTypeInvalid, location, type, typeKind.ToString());

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The type specified.</param>
    /// <param name="name">The duplicate name.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateCommandLineSerializableDuplicateName(Location? location, string type, string name) =>
        Diagnostic.Create(CommandLineSerializableDuplicateName, location, type, name);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="expectedType">Expected type.</param>
    /// <param name="type">The type specified.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateInvalidEmptyArgumentValueType(
        Location? location,
        string expectedType,
        string type
    ) => Diagnostic.Create(InvalidEmptyArgumentValueType, location, expectedType, type);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateMultiplePositionalArgumentProperties(Location? location) =>
        Diagnostic.Create(MultiplePositionalArgumentProperties, location);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateInvalidPositionalArgumentType(Location? location) =>
        Diagnostic.Create(InvalidPositionalArgumentType, location);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="type">The enum type.</param>
    /// <param name="propertyName">The name of the property with invalid constraints.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateCommandLineSerializableViolatesNullConstraints(
        Location? location,
        string type,
        string? propertyName
    ) => Diagnostic.Create(CommandLineSerializableViolatesNullConstraints, location, type, propertyName);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="className">Name of the class.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateSerializableAttributeClassIsNotPartial(Location? location, string className) =>
        Diagnostic.Create(SerializableAttributeClassIsNotPartial, location, className);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="className">Name of the class.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateTypedefClassIsNotPartial(Location? location, string className) =>
        Diagnostic.Create(TypedefClassIsNotPartial, location, className);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <param name="className">Name of the class.</param>
    /// <param name="propertyName">Name of the property.</param>
    /// <param name="value0">Value of first property.</param>
    /// <param name="value1">Value of second property.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateSerializableDifferentProperties(
        Location? location,
        string className,
        string propertyName,
        string? value0,
        string? value1
    ) => Diagnostic.Create(SerializableDifferentProperties, location, className, propertyName, value0, value1);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateMultipleQueryParametersRouteParameters(Location? location) =>
        Diagnostic.Create(MultipleQueryParametersRouteParameters, location);

    /// <summary>
    /// Creates a new <see cref="DiagnosticDescriptor"/> for this error.
    /// </summary>
    /// <param name="location">An optional primary location of the diagnostic. If null, Location will return None.</param>
    /// <returns><see cref="Diagnostic"/>.</returns>
    public static Diagnostic CreateQueryParametersParameterReferencedInRoutePattern(Location? location) =>
        Diagnostic.Create(QueryParametersParameterReferencedInRoutePattern, location);
}
