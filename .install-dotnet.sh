#!/usr/bin/env bash

curl -SsfL https://dot.net/v1/dotnet-install.sh -o dotnet-install.sh || exit 1
chmod +x ./dotnet-install.sh || exit 1
./dotnet-install.sh --channel "8.0" --install-dir "/usr/share/dotnet/" || exit 1
