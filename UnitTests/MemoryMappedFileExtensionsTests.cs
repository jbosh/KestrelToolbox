// <copyright file="MemoryMappedFileExtensionsTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using KestrelToolbox.Extensions.MemoryMappedFileExtensions;

namespace UnitTests;

public class MemoryMappedFileExtensionsTests
{
    private readonly ITestOutputHelper testOutputHelper;

    public MemoryMappedFileExtensionsTests(ITestOutputHelper testOutputHelper)
    {
        this.testOutputHelper = testOutputHelper;
    }

    [Fact]
    public void Bool()
    {
        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        Assert.False(accessor.ReadBoolean(0));
        accessor.Write(0, true);
        var booleanValue = accessor.Read<bool>(0);
        Assert.True(booleanValue);
        Assert.True(accessor.ReadBoolean(0));
        var byteValue = accessor.Read<byte>(0);
        Assert.Equal(1, byteValue);
        Assert.Equal(1, accessor.ReadByte(0));
        byteValue = accessor.Read<byte>(1);
        Assert.Equal(0, byteValue);
    }

    [Fact]
    public void Int32Test()
    {
        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        Assert.Equal(0, accessor.ReadInt32(0));
        for (var i = 0; i < 8; i++)
        {
            Assert.Equal(0, accessor.ReadByte(i));
        }

        accessor.Write(1, 0x01020304);
        Assert.Equal(0x01020304, accessor.ReadInt32(1));
        {
            var bytes = accessor.ReadBytes(1, 4);
            Assert.Equal(4, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x04, bytes[0]);
            Assert.Equal(0x03, bytes[1]);
            Assert.Equal(0x02, bytes[2]);
            Assert.Equal(0x01, bytes[3]);

            Array.Clear(bytes, 0, bytes.Length);
            Assert.Equal(4, accessor.ReadBytes(1, bytes));
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x04, bytes[0]);
            Assert.Equal(0x03, bytes[1]);
            Assert.Equal(0x02, bytes[2]);
            Assert.Equal(0x01, bytes[3]);
        }
    }

    [Fact]
    public void Int64Test()
    {
        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        Assert.Equal(0, accessor.ReadInt64(0));
        for (var i = 0; i < 8; i++)
        {
            Assert.Equal(0, accessor.ReadByte(i));
        }

        accessor.Write(1, 0x0102030405060708);
        Assert.Equal(0x0102030405060708, accessor.ReadInt64(1));
        {
            var bytes = accessor.ReadBytes(1, 8);
            Assert.Equal(8, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x08, bytes[0]);
            Assert.Equal(0x07, bytes[1]);
            Assert.Equal(0x06, bytes[2]);
            Assert.Equal(0x05, bytes[3]);
            Assert.Equal(0x04, bytes[4]);
            Assert.Equal(0x03, bytes[5]);
            Assert.Equal(0x02, bytes[6]);
            Assert.Equal(0x01, bytes[7]);

            Array.Clear(bytes, 0, bytes.Length);
            Assert.Equal(8, accessor.ReadBytes(1, bytes));
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x08, bytes[0]);
            Assert.Equal(0x07, bytes[1]);
            Assert.Equal(0x06, bytes[2]);
            Assert.Equal(0x05, bytes[3]);
            Assert.Equal(0x04, bytes[4]);
            Assert.Equal(0x03, bytes[5]);
            Assert.Equal(0x02, bytes[6]);
            Assert.Equal(0x01, bytes[7]);
        }
    }

    [Fact]
    public void AssortedInts()
    {
        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        var clearArray = new byte[32];

        accessor.WriteBytes(0, clearArray);

        Assert.Equal(0, accessor.ReadInt64(0));
        for (var i = 0; i < 8; i++)
        {
            Assert.Equal(0, accessor.ReadByte(i));
        }

        // Int8
        accessor.Write(1, sbyte.MinValue);
        Assert.Equal(sbyte.MinValue, accessor.ReadSByte(1));
        {
            var bytes = accessor.ReadBytes(1, 1);
            var value = Assert.Single(bytes);

            Assert.Equal(sbyte.MinValue, (sbyte)value);
        }

        // UInt8
        accessor.Write(1, byte.MaxValue);
        Assert.Equal(byte.MaxValue, accessor.ReadByte(1));
        {
            var bytes = accessor.ReadBytes(1, 1);
            var value = Assert.Single(bytes);
            Assert.Equal(byte.MaxValue, value);
        }

        // Int16
        accessor.Write(1, (short)0x0102);
        Assert.Equal(0x0102, accessor.ReadInt16(1));
        {
            var bytes = accessor.ReadBytes(1, 2);
            Assert.Equal(2, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x02, bytes[0]);
            Assert.Equal(0x01, bytes[1]);
        }

        // Sanity check for clearing
        accessor.WriteBytes(0, clearArray);
        Assert.Equal(0, accessor.ReadInt64(0));
        Assert.Equal(0UL, accessor.ReadUInt64(0));

        // Int32
        accessor.Write(1, 0x01020304);
        Assert.Equal(0x01020304, accessor.ReadInt32(1));
        {
            var bytes = accessor.ReadBytes(1, 4);
            Assert.Equal(4, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x04, bytes[0]);
            Assert.Equal(0x03, bytes[1]);
            Assert.Equal(0x02, bytes[2]);
            Assert.Equal(0x01, bytes[3]);
        }

        // Int64
        accessor.WriteBytes(0, clearArray);
        accessor.Write(1, 0x0102030405060708);
        Assert.Equal(0x0102030405060708, accessor.ReadInt64(1));
        {
            var bytes = accessor.ReadBytes(1, 8);
            Assert.Equal(8, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x08, bytes[0]);
            Assert.Equal(0x07, bytes[1]);
            Assert.Equal(0x06, bytes[2]);
            Assert.Equal(0x05, bytes[3]);
            Assert.Equal(0x04, bytes[4]);
            Assert.Equal(0x03, bytes[5]);
            Assert.Equal(0x02, bytes[6]);
            Assert.Equal(0x01, bytes[7]);
        }

        // UInt16
        accessor.WriteBytes(0, clearArray);
        accessor.Write(1, (ushort)0x0102);
        Assert.Equal(0x0102, accessor.ReadUInt16(1));
        {
            var bytes = accessor.ReadBytes(1, 2);
            Assert.Equal(2, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x02, bytes[0]);
            Assert.Equal(0x01, bytes[1]);
        }

        // UInt32
        accessor.Write(1, 0x01020304U);
        Assert.Equal(0x01020304U, accessor.ReadUInt32(1));
        {
            var bytes = accessor.ReadBytes(1, 4);
            Assert.Equal(4, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x04, bytes[0]);
            Assert.Equal(0x03, bytes[1]);
            Assert.Equal(0x02, bytes[2]);
            Assert.Equal(0x01, bytes[3]);
        }

        // UInt64
        accessor.WriteBytes(0, clearArray);
        accessor.Write(1, 0x0102030405060708U);
        Assert.Equal(0x0102030405060708U, accessor.ReadUInt64(1));
        {
            var bytes = accessor.ReadBytes(1, 8);
            Assert.Equal(8, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x08, bytes[0]);
            Assert.Equal(0x07, bytes[1]);
            Assert.Equal(0x06, bytes[2]);
            Assert.Equal(0x05, bytes[3]);
            Assert.Equal(0x04, bytes[4]);
            Assert.Equal(0x03, bytes[5]);
            Assert.Equal(0x02, bytes[6]);
            Assert.Equal(0x01, bytes[7]);
        }

        // Char
        accessor.WriteBytes(0, clearArray);
        accessor.Write(1, (char)0x0102);
        Assert.Equal(0x0102, accessor.ReadChar(1));
        {
            var bytes = accessor.ReadBytes(1, 2);
            Assert.Equal(2, bytes.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            Assert.Equal(0x02, bytes[0]);
            Assert.Equal(0x01, bytes[1]);
        }
    }

    [Fact]
    public void Floats()
    {
        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        Assert.Equal(0, accessor.ReadInt64(0));
        for (var i = 0; i < 8; i++)
        {
            Assert.Equal(0, accessor.ReadByte(i));
        }

        var float0 = 280408200.00284770f;
        accessor.Write(1, float0);
        Assert.Equal(float0, accessor.ReadSingle(1));

        var float1 = 2974.97291f;
        accessor.Write(2, float1);
        Assert.Equal(float1, accessor.ReadSingle(2));
        Assert.NotEqual(float0, accessor.ReadSingle(1));
        Assert.NotEqual(float1, accessor.ReadSingle(4));

        var double0 = 280408200.00284770;
        accessor.Write(1, double0);
        Assert.Equal(double0, accessor.ReadDouble(1));
        Assert.NotEqual(double0, accessor.ReadDouble(8));

        var double1 = 2974.97291;
        accessor.Write(2, double1);
        Assert.Equal(double1, accessor.ReadDouble(2));
        Assert.NotEqual(double0, accessor.ReadDouble(1));
        Assert.NotEqual(double1, accessor.ReadDouble(8));

        var decimal0 = 2974.97291M;
        accessor.Write(2, decimal0);
        Assert.Equal(decimal0, accessor.ReadDecimal(2));
    }

    [Fact]
    public void Exceptions()
    {
        var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        var accessor = memoryMappedFile.CreateViewAccessorExtended();

        try
        {
            var buffer = new byte[8192];

            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadInt64(-1));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadInt64(4092));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadInt64(4096));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadInt64(4096));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.WriteBytes(0, buffer));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.WriteBytes(-1, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.WriteBytes(4098, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadBytes(-1, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadBytes(4098, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.WriteArray(0, buffer));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.WriteArray(-1, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.WriteArray(4098, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadArray(-1, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.ReadArray(4098, buffer.AsSpan(0, 32)));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() => accessor.Read<TestType>(4090));
            _ = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var t = default(TestType);
                accessor.Write(4090, ref t);
            });

            accessor.Dispose();
            memoryMappedFile.Dispose();

            _ = Assert.Throws<ObjectDisposedException>(() => accessor.ReadInt64(0));
        }
        finally
        {
#pragma warning disable S3966 // memoryMappedFile might be disposed multiple times.
            accessor.Dispose();
            memoryMappedFile.Dispose();
#pragma warning restore S3966
        }
    }

    [Fact]
    public void ByteArrays()
    {
        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, 4096);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        var sourceArray = new byte[8192];
        var rnd = new Random();
        rnd.NextBytes(sourceArray);

        var buffer = new byte[8192];
        accessor.WriteBytes(0, sourceArray.AsSpan(0, 4096));
        Assert.Equal(4096, accessor.ReadBytes(0, buffer));
        Assert.Equal(sourceArray.Take(4096), buffer.Take(4096));
        Assert.Equal(new byte[4096], buffer.Skip(4096));

        Assert.Equal(64, accessor.ReadBytes(64, buffer.AsSpan(0, 64)));
        Assert.Equal(sourceArray.Skip(64).Take(64), buffer.Take(64));

        Array.Clear(buffer, 0, buffer.Length);
        Assert.Equal(4, accessor.ReadBytes(4092, buffer));
        Assert.Equal(sourceArray.Skip(4092).Take(4), buffer.Take(4));
        Assert.Equal(new byte[8188], buffer.Skip(4));
    }

    [StructLayout(LayoutKind.Sequential)]
    private struct TestType
    {
        public bool Boolean;
        public int Int32;
        public long Int64;
    }

    [Fact]
    public void Generics()
    {
        var sizeofTestType = Marshal.SizeOf<TestType>();
        Assert.Equal(16, sizeofTestType); // Make sure it's easy to get alignment of MemoryMappedFile

        using var memoryMappedFile = MemoryMappedFile.CreateNew(null, sizeofTestType * 1024);
        using var accessor = memoryMappedFile.CreateViewAccessorExtended();

        Assert.Equal(accessor.Capacity, sizeofTestType * 1024); // Memory mapped file may have aligned up.
        var sourceArray = new TestType[1024];
        var rnd = Random.Shared;
        for (var i = 0; i < sourceArray.Length; i++)
        {
            var value = new TestType
            {
                Boolean = rnd.Next(2) == 0,
                Int32 = rnd.Next(),
                Int64 = rnd.NextInt64(),
            };

            sourceArray[i] = value;
        }

        var buffer = new TestType[2048];
        accessor.WriteArray(0, sourceArray);
        var amtRead = accessor.ReadArray(0, buffer);
        Assert.Equal(accessor.Capacity / sizeofTestType, amtRead);
        Assert.Equal(sourceArray.Take(amtRead), buffer.Take(amtRead));
        Assert.Equal(new TestType[buffer.Length - amtRead], buffer.Skip(amtRead));

        Assert.Equal(32, accessor.ReadArray(sizeofTestType * 32, buffer, 0, 32));
        Assert.Equal(sourceArray.Skip(32).Take(32), buffer.Take(32));

        Array.Clear(buffer, 0, buffer.Length);
        var expectedReadBytes = sizeofTestType * 4;
        Assert.Equal(4, accessor.ReadArray(accessor.Capacity - expectedReadBytes, buffer));
        Assert.Equal(sourceArray.Skip(1024 - 4).Take(4), buffer.Take(4));
        Assert.Equal(new TestType[2048 - 4], buffer.Skip(4));

        buffer[0] = default;
        accessor.Write(40, ref sourceArray[0]);
        buffer[0] = accessor.Read<TestType>(40);
        Assert.Equal(sourceArray[0], buffer[0]);
        buffer[0] = default;
        accessor.Read(40, out buffer[0]);
        Assert.Equal(sourceArray[0], buffer[0]);
    }

    [Benchmark]
    public void Benchmark()
    {
        for (var benchmarkIteration = 4; benchmarkIteration >= 0; benchmarkIteration--)
        {
            const int ArraySize = 1 * 1024 * 1024;
            using var memoryMappedFile = MemoryMappedFile.CreateNew(null, ArraySize);

            var sourceBuffer = new byte[ArraySize];
            var rnd = Random.Shared;
            rnd.NextBytes(sourceBuffer);

            using (var accessor = memoryMappedFile.CreateViewAccessor())
            {
                accessor.WriteArray(0, sourceBuffer, 0, sourceBuffer.Length);
            }

            const int IterationCount = 256 * 1024 * 1024;

            var toolboxTimer = Stopwatch.StartNew();
            using (var accessor = memoryMappedFile.CreateViewAccessorExtended())
            {
                for (var iteration = 0; iteration < IterationCount; iteration++)
                {
                    var offset = rnd.Next(ArraySize - 4);
                    _ = accessor.ReadInt32(offset);
                }
            }

            toolboxTimer.Stop();

            var dotnetTimer = Stopwatch.StartNew();
            using (var accessor = memoryMappedFile.CreateViewAccessor())
            {
                for (var iteration = 0; iteration < IterationCount; iteration++)
                {
                    var offset = rnd.Next(ArraySize - 4);
                    _ = accessor.ReadInt32(offset);
                }
            }

            dotnetTimer.Stop();

            if (benchmarkIteration == 0)
            {
                this.testOutputHelper.WriteLine($"Tests completed {IterationCount:#,#} iterations.");
                this.testOutputHelper.WriteLine($"Dotnet : {dotnetTimer.Elapsed}.");
                this.testOutputHelper.WriteLine(
                    $"Toolbox: {toolboxTimer.Elapsed} ({100.0 * toolboxTimer.Elapsed / dotnetTimer.Elapsed:0.00}% faster)."
                );

                Assert.True(toolboxTimer.ElapsedTicks < dotnetTimer.ElapsedTicks);
            }
        }
    }
}
