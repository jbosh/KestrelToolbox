// <copyright file="ValidValuesTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Serialization.DataAnnotations;

namespace UnitTests;

public class ValidValuesTests
{
    [Fact]
    public void Basic()
    {
        var validStrings = new ValidValuesAttribute("hello", "world");
        Assert.True(validStrings.Contains("hello"));
        Assert.True(validStrings.Contains("world"));
        Assert.False(validStrings.Contains("invalid"));
        Assert.False(validStrings.Contains(13));

        var validInts = new ValidValuesAttribute(22, 23);
        Assert.True(validInts.Contains(22));
        Assert.True(validInts.Contains(23));
        Assert.False(validInts.Contains(24));
        Assert.False(validInts.Contains("invalid"));

        Assert.Equal("Valid Values: hello, world", validStrings.ToString());
        Assert.Equal("Valid Values: 22, 23", validInts.ToString());
    }
}
