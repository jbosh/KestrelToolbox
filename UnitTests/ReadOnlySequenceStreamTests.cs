// <copyright file="ReadOnlySequenceStreamTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.IO;
using KestrelToolbox.Types;

namespace UnitTests;

public class ReadOnlySequenceStreamTests
{
    [Fact]
    public void Basic()
    {
        var sequence = CreateSequence(0, 4096, 128);
        using var stream = new ReadOnlySequenceStream(sequence);

        var count = 0;
        var buffer = new byte[61];
        while (true)
        {
            var amountRead = stream.Read(buffer);
            for (var i = 0; i < amountRead; i++)
            {
                Assert.Equal((byte)(count + i), buffer[i]);
            }

            count += amountRead;
            if (amountRead != buffer.Length)
            {
                break;
            }
        }

        var rnd = new Random(20850027);
        for (var iteration = 0; iteration < 256; iteration++)
        {
            var offset = rnd.Next((int)(stream.Length - buffer.Length));
            stream.Position = offset;

            var amountRead = stream.Read(buffer);
            Assert.Equal(amountRead, buffer.Length);

            for (var i = 0; i < amountRead; i++)
            {
                Assert.Equal((byte)(offset + i), buffer[i]);
            }
        }

        for (var iteration = 0; iteration < 256; iteration++)
        {
            var offset = rnd.Next((int)(stream.Length - buffer.Length));
            Assert.Equal(offset, stream.Seek(offset, SeekOrigin.Begin));

            var amountRead = stream.Read(buffer);
            Assert.Equal(amountRead, buffer.Length);

            for (var i = 0; i < amountRead; i++)
            {
                Assert.Equal((byte)(offset + i), buffer[i]);
            }
        }

        stream.Position = 0;
        for (var iteration = 0; iteration < 256; iteration++)
        {
            var previousPosition = stream.Position;
            var offset = rnd.Next(71);
            if (previousPosition + offset >= stream.Position)
            {
                previousPosition = 0;
                stream.Position = 0;
            }

            Assert.Equal(offset + stream.Position, stream.Seek(offset, SeekOrigin.Current));
            Assert.Equal(previousPosition + offset, stream.Position);

            var amountRead = stream.Read(buffer);
            for (var i = 0; i < amountRead; i++)
            {
                Assert.Equal((byte)(previousPosition + offset + i), buffer[i]);
            }
        }

        stream.Position = stream.Length - 1;
        Assert.Equal((byte)stream.Position, stream.ReadByte());
        Assert.Equal(-1, stream.ReadByte());

        stream.Position = stream.Length;
        Assert.Equal(-1, stream.ReadByte());
        Assert.Equal(0, stream.Read(buffer));
    }

    [Fact]
    public void Failure()
    {
        var sequence = CreateSequence(0, 4096, 128);
        using var stream = new ReadOnlySequenceStream(sequence);
        _ = Assert.Throws<ArgumentOutOfRangeException>(() => stream.Seek(-1, SeekOrigin.Begin));
        stream.Position = 32;
        Assert.Equal(12, stream.Seek(-20, SeekOrigin.Current));
        _ = Assert.Throws<ArgumentOutOfRangeException>(() => stream.Seek(-13, SeekOrigin.Begin));
        _ = Assert.Throws<ArgumentOutOfRangeException>(() => stream.Position = stream.Length + 1);
    }

    private static ReadOnlySequence<byte> CreateSequence(int start, int count, int segmentCount)
    {
        var segments = new List<ReadOnlySequenceSegment<byte>>((count / segmentCount) + 1);
        var lastSegment = default(Segment<byte>?);
        for (var i = 0; i < count; i += segmentCount)
        {
            var array = Enumerable.Range(start + i, Math.Min(count - i, segmentCount)).Select(b => (byte)b).ToArray();
            var segment = new Segment<byte>(array);
            lastSegment?.SetNext(segment);
            lastSegment = segment;
            segments.Add(segment);
        }

        return new ReadOnlySequence<byte>(segments[0], 0, segments[^1], segments[^1].Memory.Length);
    }

    private sealed class Segment<T> : ReadOnlySequenceSegment<T>
    {
        public Segment(ReadOnlyMemory<T> memory)
        {
            this.Memory = memory;
        }

        public void SetNext(Segment<T>? segment)
        {
            this.Next = segment;
            if (segment != null)
            {
                segment.RunningIndex = this.RunningIndex + this.Memory.Length;
            }
        }
    }
}
