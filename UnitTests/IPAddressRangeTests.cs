// <copyright file="IPAddressRangeTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Net;
using KestrelToolbox.Types;

namespace UnitTests;

public class IPAddressRangeTests
{
    [Fact]
    public void Basic()
    {
        Assert.True(IPAddressRange.TryParse("192.168.0.0/16", out var range));
        Assert.True(range.Contains(IPAddress.Parse("192.168.1.20")));
        Assert.True(range.Contains(IPAddress.Parse("192.168.255.255")));
        Assert.True(range.Contains(IPAddress.Parse("192.168.40.255")));
        Assert.False(range.Contains(IPAddress.Parse("192.167.255.255")));
        Assert.False(range.Contains(IPAddress.Parse("192.169.0.0")));

        Assert.False(IPAddressRange.TryParse("192.168.0.0/1622", out range));
        Assert.Equal(default, range);
        Assert.False(IPAddressRange.TryParse("192.a.0.0/1622", out range));
        Assert.Equal(default, range);

        Assert.True(IPAddressRange.TryParse("0.0.0.0/0", out range));
        Assert.True(range.Contains(IPAddress.Parse("1.1.1.1")));
        Assert.True(range.Contains(IPAddress.Parse("0.0.0.0")));

        Assert.True(IPAddressRange.TryParse("::/128", out range));
        Assert.False(range.Contains(IPAddress.Parse("1.1.1.1")));
        Assert.False(range.Contains(IPAddress.Parse("0.0.0.0")));

        // IPv4 translated addresses
        Assert.True(IPAddressRange.TryParse("::ffff:0:0/96", out range));
        Assert.True(range.Contains(IPAddress.Parse("::ffff:1.1.1.1")));
        Assert.True(range.Contains(IPAddress.Parse("::ffff:0.0.0.0")));
        Assert.NotNull(range.ToString());

        var exception = Assert.Throws<ArgumentException>(() => IPAddressRange.Parse("hello"));
        Assert.Equal("range", exception.ParamName);

        var range2 = IPAddressRange.Parse("::ffff:0:0/96");
        Assert.True(range == range2);
        Assert.False(range != range2);
        Assert.True(range.Equals(range2));
        Assert.Equal(range.GetHashCode(), range2.GetHashCode());
        Assert.False(range.Equals(null));

        range2 = IPAddressRange.Parse("192.168.0.0/16");
        Assert.False(range == range2);
        Assert.True(range != range2);
        Assert.False(range.Equals(range2));
        Assert.NotEqual(range.GetHashCode(), range2.GetHashCode());
        Assert.False(range.Equals(null));
    }

    [SuppressMessage("Style", "IDE0250:Make struct \'readonly\'", Justification = "Testing purity.")]
    private struct PureTest
    {
        public readonly IPAddressRange Range;

        public PureTest(string range)
        {
            this.Range = IPAddressRange.Parse(range);
        }
    }

    [Fact]
    public void Pure()
    {
        var test = new PureTest("0.0.0.0/0");
        Assert.True(test.Range.Contains(IPAddress.Parse("1.1.1.1")));
    }
}
