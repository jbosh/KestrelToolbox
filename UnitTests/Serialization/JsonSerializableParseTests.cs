// <copyright file="JsonSerializableParseTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections;
using System.Collections.Frozen;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO.Pipelines;
using System.Text.Json;
using System.Text.Json.Nodes;
using KestrelToolbox.Extensions.PipeReaderExtensions;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Serialization.DataAnnotations.Serialization;
using KestrelToolbox.Types;
using Newtonsoft.Json.Linq;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests.Serialization;

[SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Tests describe what things are.")]
public sealed partial class JsonSerializableParseTests
{
    [JsonSerializable(typeof(InvalidValueEnum))]
    public static partial class JsonParser { }

    [JsonSerializable(typeof(Int32Class))]
    [JsonSerializable(typeof(UInt32Class))]
    [JsonSerializable(typeof(IntegerTypesClass))]
    [JsonSerializable(typeof(DecimalTypesClass))]
    [JsonSerializable(typeof(StringClass))]
    [JsonSerializable(typeof(DateTimeClass))]
    [JsonSerializable(typeof(TimeOnlyClass))]
    [JsonSerializable(typeof(DateOnlyClass))]
    [JsonSerializable(typeof(TimeSpanClass))]
    [JsonSerializable(typeof(EnumClass))]
    [JsonSerializable(typeof(TestEnum), typeof(TestEnum?), ParsingMethodName = "TryParseEnum")]
    [JsonSerializable(typeof(EnumSerializationClass))]
    [JsonSerializable(typeof(TestNumberEnum), typeof(TestNumberEnum?), ParsingMethodName = "TryParseEnum")]
    [JsonSerializable(typeof(ArrayClass))]
    [JsonSerializable(typeof(SubArrayClass))]
    [JsonSerializable(typeof(List<AllowUnknownSubArrayClass>), typeof(AllowUnknownSubArrayClass))]
    [JsonSerializable(typeof(BytesClass))]
    [JsonSerializable(typeof(CustomObjectClass), SourceGenerationMode = SourceGenerationMode.Deserialize)]
    [JsonSerializable(typeof(CustomParseClass), SourceGenerationMode = SourceGenerationMode.Deserialize)]
    [JsonSerializable(typeof(NormalObjectClass), typeof(NormalReferenceObject), typeof(NormalValueObject))]
    [JsonSerializable(typeof(CustomInjectedParseClass), SourceGenerationMode = SourceGenerationMode.Deserialize)]
    [JsonSerializable(
        typeof(CustomInjectedParseClass.CustomValueClass),
        CustomParsingMethodName = "CustomInjectedParseClass.CustomValueClass.TryParseCustom",
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(
        typeof(CustomInjectedParseClass.CustomValueStruct),
        CustomParsingMethodName = "CustomInjectedParseClass.CustomValueStruct.TryParseCustom",
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(typeof(NormalObjectArraysClass))]
    [JsonSerializable(typeof(GuidObject))]
    [JsonSerializable(typeof(SystemTextJsonObjectClass))]
    [JsonSerializable(typeof(LinkedListObject))]
    [JsonSerializable(typeof(NullValuesObjectClass))]
    [JsonSerializable(typeof(ReadOnlySequenceObject))]
    [JsonSerializable(typeof(RequiredObject), typeof(RequiredObject.NestedObject))]
    [JsonSerializable(typeof(FriendlyNameObject), typeof(FriendlyNameObject.NestedObject))]
    [JsonSerializable(typeof(SpecialStringsObject))]
    [JsonSerializable(typeof(RangesClass), typeof(RangesClassNegative))]
    [JsonSerializable(typeof(JsonSerializableSettingsNoExtrasClass), typeof(JsonSerializableSettingsClass))]
    [JsonSerializable(typeof(EmptyFieldTypeClass), typeof(EmptyFieldTypeClass.Subclass))]
    [JsonSerializable(typeof(UUIDv1Class))]
    [JsonSerializable(
        typeof(JArray),
        CustomParsingMethodName = "NewtonsoftJObjectClass.TryParseJArray",
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(
        typeof(JObject),
        CustomParsingMethodName = "NewtonsoftJObjectClass.TryParseJObject",
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(
        typeof(JToken),
        CustomParsingMethodName = "NewtonsoftJObjectClass.TryParseJToken",
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(typeof(NewtonsoftJObjectClass), SourceGenerationMode = SourceGenerationMode.Deserialize)]
    [JsonSerializable(typeof(NoDefaultConstructorClass))]
    [JsonSerializable(typeof(ValueTypeStruct))]
    [JsonSerializable(typeof(AllowAnyStringClass), typeof(AllowAnyStringArrayClass))]
    [JsonSerializable(typeof(FlexibleBoolClass))]
    [JsonSerializable(typeof(DefaultNullableValuesClass))]
    [JsonSerializable(typeof(ListClass))]
    [JsonSerializable(typeof(HashSetClass))]
    [JsonSerializable(typeof(FrozenSetClass))]
    [JsonSerializable(typeof(RequiredPropertyInitClass))]
    [JsonSerializable(typeof(DefaultsClass))]
    [JsonSerializable(typeof(int), typeof(int?))]
    [JsonSerializable(typeof(string))]
    [JsonSerializable(
        typeof(Dictionary<string, string>),
        typeof(SortedDictionary<string, string>),
        typeof(Dictionary<string, int>),
        typeof(Dictionary<string, CustomParseClass>),
        typeof(IDictionaryClass<string, string>),
        typeof(FrozenDictionary<string, string>),
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(
        typeof(List<string>),
        typeof(string[]),
        typeof(List<int>),
        typeof(List<byte[]>),
        typeof(List<string[]>),
        typeof(int[]),
        typeof(List<CustomParseClass>),
        typeof(CustomParseClass[]),
        typeof(int?[]),
        SourceGenerationMode = SourceGenerationMode.Deserialize
    )]
    [JsonSerializable(typeof(Guid), typeof(UUIDv1), typeof(DateTime), typeof(byte[]))]
    [JsonSerializable(typeof(EmailAddressClass))]
    [JsonSerializable(typeof(TrimClass))]
    [JsonSerializable(typeof(NullOnEmptyClass))]
    [JsonSerializable(typeof(NullableStringClass))]
    [JsonSerializable(typeof(CustomParsingEnumClass))]
    [JsonSerializable(
        typeof(CustomParsingNumberEnum),
        ParsingMethodName = "TryParseNumber",
        CustomParsingMethodName = "CustomParsingEnumClass.TryParseNumberValue"
    )]
    [JsonSerializable(
        typeof(CustomParsingStringEnum),
        ParsingMethodName = "TryParseString",
        CustomParsingMethodName = "CustomParsingEnumSerialization.DoStringParsing"
    )]
    [JsonSerializable(typeof(InvalidValueEnumClass))]
    [JsonSerializable(typeof(CustomPropertyParsingClass))]
    [JsonSerializable(typeof(TypedefJsonNullable))]
    public static partial class JsonParser { }

    [JsonSerializable]
    public sealed partial class BoolClass
    {
        [Name("value")]
        public bool Value { get; set; }

        [Name("nullable")]
        public bool? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(true)]
        public bool Default { get; set; }
    }

    [Fact]
    public void Bool()
    {
        Assert.True(BoolClass.TryParseJson("""{"value": true, "nullable": false}""", out var result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.True(result.Default);
        Assert.False(result.Nullable);

        Assert.True(
            BoolClass.TryParseJson("""{"value": false, "nullable": true, "default": false}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.False(result.Default);
        Assert.True(result.Nullable);

        Assert.True(BoolClass.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.True(result.Default);
        Assert.Null(result.Nullable);

        Assert.False(BoolClass.TryParseJson("""{"value": "true"}""", out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseJson("""{"value": 1}""", out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseJson("""{"nullable": "true"}""", out result, out error));
        Assert.Equal("'nullable' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseJson("""{"nullable": 1}""", out result, out error));
        Assert.Equal("'nullable' was not of type bool.", error);
        Assert.Null(result);

        Assert.True(BoolClass.TryParseJson("""{"nullable2": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.True(result.Default);
        Assert.Null(result.Nullable);

        Assert.True(BoolClass.TryParseJson("""{"value678": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.True(result.Default);
        Assert.Null(result.Nullable);
    }

    public sealed class Int32Class
    {
        [Name("value")]
        [ValidRange(-20, 30)]
        public int Value { get; set; }

        [Name("nullable")]
        [ValidRange(-2000, 40)]
        public int? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(22)]
        [ValidRange(0, 2_000_000)]
        public int Default { get; set; }

        [Name("valid")]
        [ValidValues(22, 24, 2001, 100)]
        public int Valid { get; set; }
    }

    [Fact]
    public void Int32()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 14, "nullable": 32, "valid": 24}""",
                out Int32Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Equal(32, result.Nullable);
        Assert.Equal(24, result.Valid);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 12, "nullable": -1993, "default": 188372, "valid": 22}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(188372, result.Default);
        Assert.Equal(-1993, result.Nullable);
        Assert.Equal(22, result.Valid);

        Assert.True(JsonParser.TryParseJson("""{"value": -20}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-20, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);
        Assert.Equal(0, result.Valid);

        Assert.True(JsonParser.TryParseJson("""{"value": 30, "default": 2000000}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(30, result.Value);
        Assert.Equal(2000000, result.Default);
        Assert.Null(result.Nullable);
        Assert.Equal(0, result.Valid);

        Assert.True(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"value": "true"}""", out result, out error));
        Assert.Equal("'value' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 22.3}""", out result, out error));
        Assert.Equal("'value' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"nullable": "74"}""", out result, out error));
        Assert.Equal("'nullable' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"nullable": 74}""", out result, out error));
        Assert.Equal("'nullable' was not in the range [-2000..40]", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"nullable": 1001848720002704720027}""", out result, out error));
        Assert.Equal("'nullable' was not of type int32.", error);
        Assert.Null(result);

        Assert.True(JsonParser.TryParseJson("""{"nullable2": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value678": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"value": -22}""", out result, out error));
        Assert.Equal("'value' was not in the range [-20..30]", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"default": -1}""", out result, out error));
        Assert.Equal("'default' was not in the range [0..2000000]", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"valid": 23}""", out result, out error));
        Assert.Equal("'valid' must be one of: 22, 24, 100, 2001.", error);
        Assert.Null(result);
    }

    public sealed class UInt32Class
    {
        [Name("value")]
        [ValidRange(5, 30)]
        public uint Value { get; set; }

        [Name("nullable")]
        [ValidRange(30, 2000)]
        public uint? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(22)]
        [ValidRange(0, 2_000_000)]
        public uint Default { get; set; }

        [Name("valid")]
        [ValidValues(22, 24, 110)]
        public uint Valid { get; set; }
    }

    [Fact]
    public void UInt32()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 14, "nullable": 32, "valid": 24}""",
                out UInt32Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Equal(32U, result.Nullable);
        Assert.Equal(24U, result.Valid);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 12, "nullable": 1993, "default": 188372, "valid": 22}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12U, result.Value);
        Assert.Equal(188372U, result.Default);
        Assert.Equal(1993U, result.Nullable);
        Assert.Equal(22U, result.Valid);

        Assert.True(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"value": "true"}""", out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 22.3}""", out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"nullable": "74"}""", out result, out error));
        Assert.Equal("'nullable' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"nullable": 1001848720002704720027}""", out result, out error));
        Assert.Equal("'nullable' was not of type uint32.", error);
        Assert.Null(result);

        Assert.True(JsonParser.TryParseJson("""{"nullable2": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value678": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"value": -22}""", out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"default": -1}""", out result, out error));
        Assert.Equal("'default' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"valid": 23}""", out result, out error));
        Assert.Equal("'valid' must be one of: 22, 24, 110.", error);
        Assert.Null(result);
    }

    public sealed class IntegerTypesClass
    {
        [Name("byte")]
        public byte Byte { get; set; }

        [Name("ushort")]
        public ushort UInt16 { get; set; }

        [Name("uint")]
        public uint UInt32 { get; set; }

        [Name("ulong")]
        public ulong UInt64 { get; set; }

        [Name("sbyte")]
        public sbyte SByte { get; set; }

        [Name("short")]
        public short Int16 { get; set; }

        [Name("int")]
        public int Int32 { get; set; }

        [Name("long")]
        public long Int64 { get; set; }
    }

    [Fact]
    public void IntegerTypes()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"byte": 14, "ushort": 32, "uint": 24, "ulong": 38082007040}""",
                out IntegerTypesClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Byte);
        Assert.Equal(32, result.UInt16);
        Assert.Equal(24U, result.UInt32);
        Assert.Equal(38082007040UL, result.UInt64);

        Assert.False(JsonParser.TryParseJson("""{"byte": -2}""", out result, out error));
        Assert.Equal("'byte' was not of type uint8.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"ushort": -2}""", out result, out error));
        Assert.Equal("'ushort' was not of type uint16.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"uint": -2}""", out result, out error));
        Assert.Equal("'uint' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"ulong": -2}""", out result, out error));
        Assert.Equal("'ulong' was not of type uint64.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"byte": 256}""", out result, out error));
        Assert.Equal("'byte' was not of type uint8.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"ushort": 70000}""", out result, out error));
        Assert.Equal("'ushort' was not of type uint16.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"uint": 4668888664668}""", out result, out error));
        Assert.Equal("'uint' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"ulong": 69898989484684684684384349684}""", out result, out error));
        Assert.Equal("'ulong' was not of type uint64.", error);
        Assert.Null(result);

        Assert.True(
            JsonParser.TryParseJson(
                """{"sbyte": 14, "short": 32, "int": 24, "long": 38082007040}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.SByte);
        Assert.Equal(32, result.Int16);
        Assert.Equal(24, result.Int32);
        Assert.Equal(38082007040L, result.Int64);

        Assert.True(
            JsonParser.TryParseJson(
                """{"sbyte": -14, "short": -32, "int": -24, "long": -38082007040}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-14, result.SByte);
        Assert.Equal(-32, result.Int16);
        Assert.Equal(-24, result.Int32);
        Assert.Equal(-38082007040L, result.Int64);

        Assert.False(JsonParser.TryParseJson("""{"sbyte": -200}""", out result, out error));
        Assert.Equal("'sbyte' was not of type int8.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"short": -40000}""", out result, out error));
        Assert.Equal("'short' was not of type int16.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": -2147483649}""", out result, out error));
        Assert.Equal("'int' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"long": -984898116816838138138138}""", out result, out error));
        Assert.Equal("'long' was not of type int64.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"sbyte": 200}""", out result, out error));
        Assert.Equal("'sbyte' was not of type int8.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"short": 40000}""", out result, out error));
        Assert.Equal("'short' was not of type int16.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": 4668888664668}""", out result, out error));
        Assert.Equal("'int' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"long": 69898989484684684684384349684}""", out result, out error));
        Assert.Equal("'long' was not of type int64.", error);
        Assert.Null(result);
    }

    public sealed class DecimalTypesClass
    {
        [Name("float")]
        public float Float { get; set; }

        [Name("double")]
        public double Double { get; set; }

        [Name("decimal")]
        public decimal Decimal { get; set; }
    }

    [Fact]
    public void DecimalTypes()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"float": 2, "double": 409, "decimal": 8200}""",
                out DecimalTypesClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2.0f, result.Float);
        Assert.Equal(409.0, result.Double);
        Assert.Equal(8200, result.Decimal);

        Assert.True(
            JsonParser.TryParseJson(
                """{"float": 2.48, "double": 409.46684, "decimal": 820055.1668863313}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2.48f, result.Float);
        Assert.Equal(409.46684, result.Double);
        Assert.Equal(820055.1668863313M, result.Decimal);

        Assert.True(
            JsonParser.TryParseJson(
                """{"float": -2.48, "double": -409.46684, "decimal": -820055.1668863313}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-2.48f, result.Float);
        Assert.Equal(-409.46684, result.Double);
        Assert.Equal(-820055.1668863313M, result.Decimal);

        Assert.True(
            JsonParser.TryParseJson(
                """{"float": 9864684668338734.168616838, "double": 986834384436688631.16864, "decimal": 79228162514264337593543950335}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(9.864685E+15F, result.Float);
        Assert.Equal(9.8683438443668864E+17, result.Double);
        Assert.Equal(79228162514264337593543950335M, result.Decimal);

        Assert.True(
            JsonParser.TryParseJson(
                """{"float":9.864685E+15,"double":9.868343844366886E+17,"decimal":-79228162514264337593543950335}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(9.864685E+15F, result.Float);
        Assert.Equal(9.8683438443668864E+17, result.Double);
        Assert.Equal(-79228162514264337593543950335M, result.Decimal);

        Assert.True(
            JsonParser.TryParseJson("""{"decimal": 79228162514264337593543950334.499881384}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0.0f, result.Float);
        Assert.Equal(0.0, result.Double);
        Assert.Equal(79228162514264337593543950334.499881384M, result.Decimal);
    }

    public sealed class StringClass
    {
        [Name("value")]
        public string? Value { get; set; }

        [Name("valid")]
        [ValidValues("hello", "world", "invalid")]
        public string? Valid { get; set; }

        [Name("default")]
        [DefaultValue("some string")]
        public string Default { get; set; } = "unused default";

        [Name("limited")]
        [StringLength(2, 4)]
        public string? Limited { get; set; }

        [Name("limited2")]
        [StringLength(2, 4)]
        public string? Limited2 { get; set; }

        [Name("implicitMax")]
        [StringLength(Minimum = 2)]
        public string? ImplicitMax { get; set; }
    }

    [Fact]
    public void Strings()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "whatever", "valid": "invalid", "default": "8200", "limited": "he", "limited2": "he", "implicitMax": "boo"}""",
                out StringClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("whatever", result.Value);
        Assert.Equal("invalid", result.Valid);
        Assert.Equal("8200", result.Default);
        Assert.Equal("he", result.Limited);
        Assert.Equal("he", result.Limited2);
        Assert.Equal("boo", result.ImplicitMax);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "extra", "limited": "hell", "limited2": "hell"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("extra", result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Equal("hell", result.Limited);
        Assert.Equal("hell", result.Limited2);

        Assert.False(JsonParser.TryParseJson("""{"limited": "1"}""", out result, out error));
        Assert.Equal("'limited' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"limited": "12345"}""", out result, out error));
        Assert.Equal("'limited' must be shorter than 4 characters.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"limited2": "1"}""", out result, out error));
        Assert.Equal("'limited2' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"implicitMax": "1"}""", out result, out error));
        Assert.Equal("'implicitMax' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"limited2": "12345"}""", out result, out error));
        Assert.Equal("'limited2' must be shorter than 4 characters.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"valid": "22"}""", out result, out error));
        Assert.Equal("'valid' must be one of: \"hello\", \"invalid\", \"world\".", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 22}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);

        Assert.True(JsonParser.TryParseJson("""{"value": null}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Null(result.Limited);
        Assert.Null(result.Limited2);
    }

    public sealed class DateTimeClass
    {
        [Name("dateTime")]
        [DefaultValue(typeof(DateTime), "June 17, 1975")]
        public DateTime DateTime { get; set; }

        [Name("dateTimeOffset")]
        [DefaultValue(typeof(DateTimeOffset), "2024-05-25T04:13:22Z")]
        public DateTimeOffset DateTimeOffset { get; set; }

        [Name("dateTimeNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public DateTime? DateTimeNullable { get; set; }

        [Name("dateTimeOffsetNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public DateTimeOffset? DateTimeOffsetNullable { get; set; }
    }

    [Fact]
    public void DateTimeTest()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"dateTime":"2010-05-03T00:00:00.000Z","dateTimeOffset":"2021-07-08T15:55:34.000Z","dateTimeNullable":"2010-05-03T00:00:00.000Z","dateTimeOffsetNullable":"2021-07-08T15:55:34.000Z"}""",
                out DateTimeClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("2010-05-03T00:00:00.000Z", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTime.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
        Assert.Equal(DateTime.Parse("2010-05-03T00:00:00.000Z", CultureInfo.InvariantCulture), result.DateTimeNullable);
        Assert.Equal(
            DateTime.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
            result.DateTimeOffsetNullable
        );

        Assert.True(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("June 17, 1975", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTimeOffset.Parse("2024-05-25T04:13:22Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
        Assert.Null(result.DateTimeNullable);
        Assert.Null(result.DateTimeOffsetNullable);

        Assert.False(JsonParser.TryParseJson("""{"dateTime": "junuary"}""", out result, out error));
        Assert.Equal("'dateTime' was not of type DateTime.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"dateTime": 17}""", out result, out error));
        Assert.Equal("'dateTime' was not of type DateTime.", error);
        Assert.Null(result);
    }

    public sealed class DateOnlyClass
    {
        [Name("value")]
        [DateOnlyParseSettings(Format = "dd MMM yyyy")]
        public DateOnly Value { get; set; }

        [Name("value2")]
        public DateOnly Value2 { get; set; }

        [Name("default")]
        [DefaultValue(typeof(DateOnly), "2023-08-12")]
        public DateOnly Default { get; set; }

        [Name("nullable")]
        public DateOnly? Nullable { get; set; }
    }

    [Fact]
    public void DateOnlyTest()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "03 May 2010", "value2": "2021-07-08"}""",
                out DateOnlyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.ParseExact("03 May 2010", "dd MMM yyyy", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("2021-07-08", CultureInfo.InvariantCulture), result.Value2);
        Assert.Equal(DateOnly.ParseExact("2023-08-12", "o", CultureInfo.InvariantCulture), result.Default);

        Assert.True(
            JsonParser.TryParseJson("""{"value": "03 May 2010", "value2": "03 May 2010"}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.ParseExact("03 May 2010", "dd MMM yyyy", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("03 May 2010", CultureInfo.InvariantCulture), result.Value2);

        Assert.False(JsonParser.TryParseJson("""{"value": "junuary"}""", out result, out error));
        Assert.Equal("'value' was not of type DateOnly.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 17}""", out result, out error));
        Assert.Equal("'value' was not of type DateOnly.", error);
        Assert.Null(result);
    }

    public sealed class TimeOnlyClass
    {
        [Name("value")]
        public TimeOnly Value { get; set; }

        [Name("default")]
        [DefaultValue(typeof(TimeOnly), "11:03 am")]
        public TimeOnly Default { get; set; }
    }

    [Fact]
    public void TimeOnlyTest()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "14:35", "default": "8:02am"}""",
                out TimeOnlyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("14:35", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("8:02am", CultureInfo.InvariantCulture), result.Default);

        Assert.True(JsonParser.TryParseJson("""{"value": "10:45"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("10:45", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("11:03am", CultureInfo.InvariantCulture), result.Default);

        Assert.False(JsonParser.TryParseJson("""{"value": "junuary"}""", out result, out error));
        Assert.Equal("'value' was not of type TimeOnly.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 17}""", out result, out error));
        Assert.Equal("'value' was not of type TimeOnly.", error);
        Assert.Null(result);
    }

    public sealed class TimeSpanClass
    {
        [Name("value")]
        [SkipSerialization(Condition.WhenDefault)]
        public TimeSpan Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull | Condition.WhenDefault)]
        public TimeSpan? Nullable { get; set; }
    }

    [Fact]
    public void TimeSpanTest()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": "20s", "nullable": "30s"}""", out TimeSpanClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("20s"), result.Value);
        Assert.Equal(TimeSpanParser.Parse("30s"), result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value": 20, "nullable": 30.234}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("20"), result.Value);
        Assert.Equal(TimeSpanParser.Parse("30.234"), result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value": "14:32"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("14:32"), result.Value);
        Assert.Null(result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"nullable": "14d"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(default, result.Value);
        Assert.Equal(TimeSpanParser.Parse("14d"), result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"value": "May 3"}""", out result, out error));
        Assert.Equal("'value' was not of type TimeSpan.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    public sealed class EnumClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestEnum), "B")]
        public TestEnum Value { get; set; }

        [Name("nullable")]
        public TestEnum? Nullable { get; set; }
    }

    [Fact]
    public void Enums()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": "none","nullable":null}""", out EnumClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.None, result.Value);
        Assert.Null(result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value": "a","nullable":"a"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.A, result.Value);
        Assert.Equal(TestEnum.A, result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.B, result.Value);
        Assert.Null(result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value": "alt","nullable":"alt"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.B, result.Value);
        Assert.Equal(TestEnum.B, result.Nullable);

        Assert.True(
            JsonParser.TryParseJson("""{"value": "superLongName","nullable":"superLongName"}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.SuperLong, result.Value);
        Assert.Equal(TestEnum.SuperLong, result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"value": "May 3"}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonSerializableParseTests.TestEnum' must be one of: a, alt, b, none, superLongName.",
            error
        );
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 17}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonSerializableParseTests.TestEnum' was not of type string.",
            error
        );
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(SerializationType = EnumSerializableSettingsSerializationType.Numbers)]
    public enum TestNumberEnum
    {
        A = 0,
        B = 1,
        None = 17,
        SuperLong = 12345,
    }

    public sealed class EnumSerializationClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum Value { get; set; }

        [Name("nullable")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum? Nullable { get; set; }
    }

    [Fact]
    public void NumberEnums()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": 0,"nullable":1}""", out EnumSerializationClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.A, result.Value);
        Assert.Equal(TestNumberEnum.B, result.Nullable);

        Assert.True(JsonParser.TryParseJson("""{"value": 17}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.None, result.Value);

        Assert.True(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.B, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value": 1}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.B, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value": 12345}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.SuperLong, result.Value);

        Assert.False(JsonParser.TryParseJson("""{"value": "May 3"}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonSerializableParseTests.TestNumberEnum' was not of type number.",
            error
        );
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 14}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonSerializableParseTests.TestNumberEnum' must be one of: 0, 1, 17, 12345.",
            error
        );
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": "17"}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonSerializableParseTests.TestNumberEnum' was not of type number.",
            error
        );
        Assert.Null(result);
    }

    public sealed class ArrayClass
    {
        [Name("ints")]
        [ArrayLength(1, 4)]
        public int[]? Ints { get; set; }

        [Name("strings")]
        [ArrayLength(1, 4)]
        [StringLength(2, 5)]
        public string[]? Strings { get; set; }

        [Name("implicit")]
        [ArrayLength(Minimum = 1)]
        public int[]? ImplicitMax { get; set; }
    }

    [Fact]
    public void ArrayTest()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"ints": [2, 3, 4], "strings": ["hello", "world"], "implicit": [2]}""",
                out ArrayClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { 2, 3, 4 }, result.Ints);
        Assert.Equal(new[] { "hello", "world" }, result.Strings);
        Assert.Equal(new[] { 2 }, result.ImplicitMax);

        Assert.False(JsonParser.TryParseJson("""{"ints": [2, 3, 4, 5, 6]}""", out result, out error));
        Assert.Equal("'ints' must be shorter than 4 elements.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"strings": []}""", out result, out error));
        Assert.Equal("'strings' must be longer than 0 elements.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"strings": ["a"]}""", out result, out error));
        Assert.Equal("'strings' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"strings": ["1234567"]}""", out result, out error));
        Assert.Equal("'strings' must be shorter than 5 characters.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"strings": ["ab", "ab", "ab", "ab", "ab"]}""", out result, out error));
        Assert.Equal("'strings' must be shorter than 4 elements.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson("""{"strings": [2, 3, 4], "ints": ["hello", "world"]}""", out result, out error)
        );
        Assert.Equal("'strings' was not of type string.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"implicit": []}""", out result, out error));
        Assert.Equal("'implicit' must be longer than 0 elements.", error);
        Assert.Null(result);
    }

    public sealed class SubArrayClass
    {
        [Name("ints")]
        public int[][]? Ints { get; set; }
    }

    [Fact]
    public void SubArray()
    {
        Assert.True(JsonParser.TryParseJson("""{"ints": [[2, 3, 4]]}""", out SubArrayClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { new[] { 2, 3, 4 } }, result.Ints);
    }

    public sealed class AllowUnknownSubArrayClass
    {
        [Name("value")]
        public required int Value { get; set; }
    }

    [Fact]
    public void AllowUnknownSubArray()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """
                [
                    { "value":  16, "extra": [{"id": 0}] },
                    { "extra": [{"id": 0}], "value":  78 },
                    { "extra": [{"id": 0}], "value":  53, "extra2": [{"id": 0}] }
                ]
                """,
                out List<AllowUnknownSubArrayClass>? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(3, result.Count);
        Assert.Equal(16, result[0].Value);
        Assert.Equal(78, result[1].Value);
        Assert.Equal(53, result[2].Value);
    }

    public sealed class BytesClass
    {
        [Name("value")]
        public byte[]? Value { get; set; }

        [Name("values")]
        public byte[][]? Values { get; set; }
    }

    [Fact]
    public void Bytes()
    {
        Assert.True(JsonParser.TryParseJson("""{"value":"aGVsbG8="}""", out BytesClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello"u8.ToArray(), result.Value);
        Assert.Null(result.Values);

        Assert.True(JsonParser.TryParseJson("""{"values":["aGVsbG8=","d29ybGQ="]}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(new[] { "hello"u8.ToArray(), "world"u8.ToArray() }, result.Values);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value":"4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/","values":["4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/"]}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(
            Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F"),
            result.Value
        );
        Assert.Equal(
            new[] { Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F") },
            result.Values
        );

        Assert.False(JsonParser.TryParseJson("""{"value":"hello**person"}""", out result, out error));
        Assert.Equal("'value' was not of type base64 encoded string.", error);
        Assert.Null(result);
    }

    public sealed class CustomReferenceObject
    {
        public string String { get; private set; } = null!;

        public int Int { get; private set; }

        [JsonSerializableCustomParse]
        public static bool CustomTryParse(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out CustomReferenceObject? result,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = null;
                return false;
            }

            var span = reader.GetString();
            if (span == null)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = null;
                return false;
            }

            var split = span.IndexOf(',');
            if (split < 0)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = null;
                return false;
            }

            var i = int.Parse(span.Remove(split), CultureInfo.InvariantCulture);
            var s = span.Substring(split + 1);

            result = new CustomReferenceObject() { Int = i, String = s };

            error = null;
            return true;
        }
    }

    public struct CustomValueObject
    {
        public byte[] Bytes { get; private set; }

        [JsonSerializableCustomParse]
        public static bool CustomTryParse(
            ref Utf8JsonReader reader,
            out CustomValueObject result,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = default;
                return false;
            }

            var span = reader.ValueSpan;
            if (!Base64.TryDecodeFromUtf8(span, out var bytes))
            {
                result = default;
                error = $"Failed to parse {nameof(CustomValueObject)}, invalid base64 string.";
                return false;
            }

            result = new CustomValueObject() { Bytes = bytes };

            error = null;
            return true;
        }
    }

    public sealed class CustomObjectClass
    {
        [Name("reference")]
        public CustomReferenceObject? Reference { get; set; }

        [Name("value")]
        public CustomValueObject Value { get; set; }
    }

    [Fact]
    public void CustomObject()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "AQIDBA==", "reference": "22,howdy"}""",
                out CustomObjectClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);
        Assert.Equal(22, result.Reference.Int);
        Assert.Equal("howdy", result.Reference.String);

        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(JsonParser.TryParseJson("""{"value": "AQIDBA=="}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(JsonParser.TryParseJson("""{"reference": "44,hello there"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(44, result.Reference.Int);
        Assert.Equal("hello there", result.Reference.String);

        Assert.Null(result.Value.Bytes);

        Assert.False(JsonParser.TryParseJson("""{"reference": [22]}""", out result, out error));
        Assert.Equal("reference > Invalid CustomReferenceObject.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": "assd;;"}""", out result, out error));
        Assert.Equal("value > Failed to parse CustomValueObject, invalid base64 string.", error);
        Assert.Null(result);
    }

    public sealed partial class CustomParseClass
    {
        [Name("class")]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed partial class CustomValueClass
        {
            public int Value { get; set; }

            [JsonSerializableCustomParse]
            public static bool TryParseCustomValueClass(
                ref Utf8JsonReader reader,
                [NotNullWhen(true)] out CustomValueClass? value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType != JsonTokenType.Number)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueClass)}";
                    return false;
                }

                var readValue = reader.GetInt32();
                value = new CustomValueClass { Value = readValue };
                error = null;
                return true;
            }

            [JsonSerializableCustomSerialize]
            public static void SerializeCustomValueClass(Utf8JsonWriter writer, CustomValueClass value)
            {
                writer.WriteNumberValue(value.Value);
            }
        }

        public partial struct CustomValueStruct
        {
            public bool Value { get; set; }

            [JsonSerializableCustomParse]
            public static bool TryParseCustomValueStruct(
                ref Utf8JsonReader reader,
                out CustomValueStruct value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType is not JsonTokenType.True and not JsonTokenType.False)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueStruct)}";
                    return false;
                }

                var readValue = reader.GetBoolean();
                value = new CustomValueStruct { Value = readValue };
                error = null;
                return true;
            }

            [JsonSerializableCustomSerialize]
            public static void Serialize(Utf8JsonWriter writer, CustomValueStruct value)
            {
                writer.WriteBooleanValue(value.Value);
            }
        }
    }

    [Fact]
    public void CustomParser()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"class": 22, "struct": true}""", out CustomParseClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Class);
        Assert.Equal(22, result.Class.Value);
        Assert.True(result.Struct.Value);

        Assert.False(JsonParser.TryParseJson("""{"class": [22]}""", out result, out error));
        Assert.Equal("class > Invalid token type for CustomValueClass", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"struct": "assd"}""", out result, out error));
        Assert.Equal("struct > Invalid token type for CustomValueStruct", error);
        Assert.Null(result);
    }

    public sealed class CustomInjectedParseClass
    {
        [Name("class")]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed class CustomValueClass
        {
            public int Value { get; set; }

            public static bool TryParseCustom(
                ref Utf8JsonReader reader,
                [NotNullWhen(true)] out CustomValueClass? value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType != JsonTokenType.Number)
                {
                    value = null;
                    error = $"Invalid token type for {nameof(CustomValueClass)}";
                    return false;
                }

                var readValue = reader.GetInt32();
                value = new CustomValueClass { Value = readValue };
                error = null;
                return true;
            }
        }

        public struct CustomValueStruct
        {
            public bool Value { get; set; }

            public static bool TryParseCustom(
                ref Utf8JsonReader reader,
                out CustomValueStruct value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType is not JsonTokenType.False and not JsonTokenType.True)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueStruct)}";
                    return false;
                }

                var readValue = reader.TokenType == JsonTokenType.True;
                value = new CustomValueStruct { Value = readValue };
                error = null;
                return true;
            }
        }
    }

    [Fact]
    public void CustomInjectedParser()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"class": 22, "struct": true}""",
                out CustomInjectedParseClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Class);
        Assert.Equal(22, result.Class.Value);
        Assert.True(result.Struct.Value);

        Assert.False(JsonParser.TryParseJson("""{"class": [22]}""", out result, out error));
        Assert.Equal("class > Invalid token type for CustomValueClass", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"struct": "assd"}""", out result, out error));
        Assert.Equal("struct > Invalid token type for CustomValueStruct", error);
        Assert.Null(result);
    }

    public class NormalReferenceObject
    {
        [Name("string")]
        public required string String { get; set; }

        [Name("int")]
        public int Int { get; set; }
    }

    public struct NormalValueObject
    {
        [Name("bytes")]
        public required byte[] Bytes { get; set; }
    }

    public class NormalObjectClass
    {
        [Name("reference")]
        public NormalReferenceObject? Reference { get; set; }

        [Name("value")]
        public NormalValueObject Value { get; set; }
    }

    [Fact]
    public void NormalObject()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": {"bytes": "AQIDBA=="}, "reference": {"int": 22, "string": "howdy"}}""",
                out NormalObjectClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(22, result.Reference.Int);
        Assert.Equal("howdy", result.Reference.String);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);
    }

    public class NormalObjectArraysClass
    {
        [Name("references")]
        public NormalReferenceObject[]? References { get; set; }

        [Name("values")]
        public NormalValueObject[]? Values { get; set; }
    }

    [Fact]
    public void NormalObjectArrays()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"values": [{"bytes": "AQIDBA=="}], "references": [{"int": 22, "string": "howdy"}]}""",
                out NormalObjectArraysClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.References);
        Assert.NotNull(result.Values);
        var referenceObject = Assert.Single(result.References);
        Assert.Equal(22, referenceObject.Int);
        Assert.Equal("howdy", referenceObject.String);
        var referenceValues = Assert.Single(result.Values);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, referenceValues.Bytes);

        Assert.True(
            JsonParser.TryParseJson(
                """
                {"values": [
                    {"bytes": "AQIDBA=="},
                    {"bytes": "GBYNDDc="}],
                "references": [
                    {"int": 22, "string": "howdy"},
                    {"int": 13, "string": "partner"}
                ]}
                """,
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.References);
        Assert.NotNull(result.Values);
        Assert.Equal(2, result.References.Length);
        Assert.Equal(22, result.References[0].Int);
        Assert.Equal("howdy", result.References[0].String);
        Assert.Equal(13, result.References[1].Int);
        Assert.Equal("partner", result.References[1].String);
        Assert.Equal(2, result.Values.Length);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Values[0].Bytes);
        Assert.Equal(new byte[] { 24, 22, 13, 12, 55 }, result.Values[1].Bytes);
    }

    public class GuidObject
    {
        [Name("value")]
        public Guid Value { get; set; }
    }

    [Fact]
    public void GuidTest()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "9184400c-d7fe-422f-9d40-720c30a2b18f"}""",
                out GuidObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(Guid.Parse("9184400c-d7fe-422f-9d40-720c30a2b18f"), result.Value);

        Assert.False(
            JsonParser.TryParseJson("""{"value": "e847da22-bb0a-11ed-afa1-asdfasdfasdf"}""", out result, out error)
        );
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": true}""", out result, out error));
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 44.37}""", out result, out error));
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);
    }

    public class SystemTextJsonObjectClass
    {
        [Name("object")]
        public JsonObject? Object { get; set; }

        [Name("array")]
        public JsonArray? Array { get; set; }

        [Name("node")]
        public JsonNode? Node { get; set; }
    }

    [Fact]
    public void SystemTextJsonObject()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"object": {"hello": "world", "value": 12}, "array": [12, 13, "hello", true], "node": "hello"}""",
                out SystemTextJsonObjectClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Object);
        Assert.NotNull(result.Array);
        Assert.NotNull(result.Node);
        Assert.Equal("world", result.Object["hello"]?.GetValue<string>());
        Assert.Equal(12, result.Object["value"]?.GetValue<int>());
        Assert.Equal(12, result.Array[0]?.GetValue<int>());
        Assert.Equal(13, result.Array[1]?.GetValue<int>());
        Assert.Equal("hello", result.Array[2]?.GetValue<string>());
        Assert.Equal(true, result.Array[3]?.GetValue<bool>());
        Assert.Equal("hello", result.Node.GetValue<string>());

        Assert.True(JsonParser.TryParseJson("""{"node": 2}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Object);
        Assert.Null(result.Array);
        Assert.NotNull(result.Node);
        Assert.Equal(2, result.Node.GetValue<int>());

        Assert.True(JsonParser.TryParseJson("""{"node": 2.2}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Object);
        Assert.Null(result.Array);
        Assert.NotNull(result.Node);
        Assert.Equal(2.2, result.Node.GetValue<double>());

        Assert.True(JsonParser.TryParseJson("""{"node": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Object);
        Assert.Null(result.Array);
        Assert.NotNull(result.Node);
        Assert.True(result.Node.GetValue<bool>());

        Assert.False(JsonParser.TryParseJson("""{"object": [12, 13]}""", out result, out error));
        Assert.Equal("'object' was not of type object.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"array": {"12": 13}}""", out result, out error));
        Assert.Equal("'array' was not of type array.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"node": hello}""", out result, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(result);
    }

    public class LinkedListObject
    {
        [Name("child")]
        public LinkedListObject? Child { get; set; }

        [Name("value")]
        [Required]
        public int Value { get; set; }
    }

    [Fact]
    public void LinkedList()
    {
        var child = default(JsonObject);
        for (var i = 14; i >= 0; i--)
        {
            var node = new JsonObject { ["value"] = i, ["child"] = child };
            child = node;
        }

        var json = child!.ToString();
        Assert.True(JsonParser.TryParseJson(json, out LinkedListObject? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        {
            var node = result;
            var i = 0;
            while (node != null)
            {
                Assert.Equal(i, node.Value);
                i++;
                node = node.Child;
            }

            Assert.Equal(15, i);
        }
    }

    public class NullValuesObjectClass
    {
        [Name("string")]
        [StringLength(64)]
        public string? String { get; set; }

        [Name("int")]
        public int? Int { get; set; }
    }

    [Fact]
    public void NullValuesObject()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"string": null, "int": null}""",
                out NullValuesObjectClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.String);
        Assert.Null(result.Int);
    }

    public class ReadOnlySequenceObject
    {
        [Name("bigLongNameThatIsTooLong")]
        public string? String { get; set; }

        [Name("anotherNameThatIsWayTooLong")]
        public long Int { get; set; }

        [Name("bytes")]
        public byte[]? Bytes { get; set; }

        [Name("date")]
        public DateTime Date { get; set; }

        [Name("time")]
        public TimeSpan Time { get; set; }

        [Name("enum")]
        public TestEnum Enum { get; set; }
    }

    [Fact]
    public void ReadOnlySequence()
    {
        var json = """
            {
                "bigLongNameThatIsTooLong": "hello world how are you",
                "anotherNameThatIsWayTooLong": 6498463318,
                "bytes": "AAECAwQFBgcICQoLDA0ODw==",
                "date": "May 1, 2020",
                "time": "20:30",
                "enum": "superLongName"
            }
            """;

        Assert.True(
            JsonParser.TryParseJson(
                SequenceUtilities.SplitIntoSequence(json, 4),
                out ReadOnlySequenceObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello world how are you", result.String);
        Assert.Equal(6498463318, result.Int);
        Assert.Equal(Enumerable.Range(0, 16).Select(c => (byte)c).ToArray(), result.Bytes);
        Assert.Equal(result.Date, DateTime.Parse("May 1, 2020", CultureInfo.InvariantCulture));
        Assert.Equal(result.Time, TimeSpanParser.Parse("20:30"));
        Assert.Equal(TestEnum.SuperLong, result.Enum);
    }

    [JsonSerializableSettings(AllowExtraProperties = false)]
    public class RequiredObject
    {
        [Name("value")]
        public required string Value { get; set; }

        [Name("nested")]
        public required NestedObject Nested { get; set; }

        [Required]
        [Name("nullable")]
        public string? Nullable { get; set; }

        public class NestedObject
        {
            [Name("value")]
            public required string Value { get; set; }
        }
    }

    [Fact]
    public void Required()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "nested": {"value": "nested value"}, "nullable": null}""",
                out RequiredObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.NotNull(result.Nested);
        Assert.Equal("nested value", result.Nested.Value);
        Assert.Null(result.Nullable);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "nested": {"value": "nested value"}, "nullable": "real value"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.NotNull(result.Nested);
        Assert.Equal("nested value", result.Nested.Value);
        Assert.Equal("real value", result.Nullable);

        Assert.False(JsonParser.TryParseJson("""{"hello": "world"}""", out result, out error));
        Assert.Equal("Unknown property received: hello", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": "world", "nullable": "hello"}""", out result, out error));
        Assert.Equal(
            "A required property was missing on UnitTests.Serialization.JsonSerializableParseTests.RequiredObject. Missing: nested.",
            error
        );
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson("""{"value": "world", "nested": {}, "nullable": "hello"}""", out result, out error)
        );
        Assert.Equal(
            "nested > A required property was missing on UnitTests.Serialization.JsonSerializableParseTests.RequiredObject.NestedObject. Missing: value.",
            error
        );
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Equal(
            "A required property was missing on UnitTests.Serialization.JsonSerializableParseTests.RequiredObject. Missing: value, nested, nullable.",
            error
        );
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": null}""", out result, out error));
        Assert.Equal("'value' was null. Should be of type string.", error);
        Assert.Null(result);
    }

    [FriendlyName("SuperFriendlyName")]
    [JsonSerializableSettings(AllowExtraProperties = false)]
    public class FriendlyNameObject
    {
        [Name("value")]
        public required string Value { get; set; }

        [Name("nested")]
        public required NestedObject Nested { get; set; }

        [FriendlyName("Nested Object")]
        public class NestedObject
        {
            [Name("value")]
            public required string Value { get; set; }
        }
    }

    [Fact]
    public void FriendlyName()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "nested": {"value": "nested value"}}""",
                out FriendlyNameObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.NotNull(result.Nested);
        Assert.Equal("nested value", result.Nested.Value);

        Assert.False(JsonParser.TryParseJson("""{"hello": "world"}""", out result, out error));
        Assert.Equal("Unknown property received: hello", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": "world"}""", out result, out error));
        Assert.Equal("A required property was missing on SuperFriendlyName. Missing: nested.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": "world", "nested": {}}""", out result, out error));
        Assert.Equal("nested > A required property was missing on Nested Object. Missing: value.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Equal("A required property was missing on SuperFriendlyName. Missing: value, nested.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": null}""", out result, out error));
        Assert.Equal("'value' was null. Should be of type string.", error);
        Assert.Null(result);
    }

    public class SpecialStringsObject
    {
        [Required(AllowOnlyWhiteSpace = false)]
        [Name("value")]
        public string Value { get; set; } = null!;

        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue]
        [Name("value2")]
        public string Value2 { get; set; } = null!;

        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue(new[] { 'a', 'b', 'c' })]
        [Name("value3")]
        public string? Value3 { get; set; }
    }

    [Fact]
    public void SpecialStrings()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "value", "value3": "something"}""",
                out SpecialStringsObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.False(
            JsonParser.TryParseJson(
                """{"value": "  ", "value2": "value", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson(
                """{"value": "\t", "value2": "value", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "  value  ", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "\tvalue", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "\tva  lue", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("va  lue", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.False(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "    ", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value2' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "value", "value3": "aaasomethingbbb"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "value", "value3": "aaabbbccvcaabc"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("v", result.Value3);

        Assert.False(
            JsonParser.TryParseJson(
                """{"value": "world", "value2": "value", "value3": "aaabbbcccaabc"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value3' must contain characters that are not whitespace.", error);
        Assert.Null(result);
    }

    [Fact]
    public async Task TestPipe()
    {
        var json = """
            {
                "bigLongNameThatIsTooLong": "hello world how are you",
                "anotherNameThatIsWayTooLong": 6498463318,
                "bytes": "AAECAwQFBgcICQoLDA0ODw==",
                "date": "May 1, 2020",
                "time": "20:30",
                "enum": "superLongName"
            }
            """;

        var pipe = new Pipe(new PipeOptions(pauseWriterThreshold: 0));
        var sequence = SequenceUtilities.SplitIntoSequence(json, 4);
        foreach (var segment in sequence)
        {
            var memory = pipe.Writer.GetMemory(segment.Length);
            segment.CopyTo(memory);
            pipe.Writer.Advance(segment.Length);
        }

        await pipe.Writer.CompleteAsync();

        var readResult = await pipe.Reader.ReadToEndAsync();
        Assert.True(JsonParser.TryParseJson(readResult.Buffer, out ReadOnlySequenceObject? result, out var error));
        pipe.Reader.AdvanceTo(readResult.Buffer.End);
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello world how are you", result.String);
        Assert.Equal(6498463318, result.Int);
        Assert.Equal(Enumerable.Range(0, 16).Select(c => (byte)c).ToArray(), result.Bytes);
        Assert.Equal(result.Date, DateTime.Parse("May 1, 2020", CultureInfo.InvariantCulture));
        Assert.Equal(result.Time, TimeSpanParser.Parse("20:30"));
        Assert.Equal(TestEnum.SuperLong, result.Enum);
    }

    [Fact]
    public async Task TestPipeFailure()
    {
        var json = "hello";

        var pipe = new Pipe(new PipeOptions(pauseWriterThreshold: 0));
        var sequence = SequenceUtilities.SplitIntoSequence(json, 4);
        foreach (var segment in sequence)
        {
            var memory = pipe.Writer.GetMemory(segment.Length);
            segment.CopyTo(memory);
            pipe.Writer.Advance(segment.Length);
        }

        await pipe.Writer.CompleteAsync();

        var readResult = await pipe.Reader.ReadToEndAsync();
        Assert.False(JsonParser.TryParseJson(readResult.Buffer, out ReadOnlySequenceObject? result, out var error));
        pipe.Reader.AdvanceTo(readResult.Buffer.Start);
        Assert.Equal("Failed to parse json.", error);
        Assert.NotEqual(string.Empty, error);
        Assert.Null(result);

        var buffer = await pipe.Reader.ReadUTF8StringAsync();
        Assert.Equal(json, buffer);
    }

    public sealed class RangesClass
    {
        [Name("decimal")]
        [ValidRange(1, 100)]
        public decimal Decimal { get; set; }

        [Name("int")]
        [ValidRange(4, 90)]
        public int Int { get; set; }

        [Name("float")]
        [ValidRange(12.0f, 17.0f)]
        public float Float { get; set; }

        [Name("double")]
        [ValidRange(-12.0, 17.0)]
        public double Double { get; set; }
    }

    public sealed class RangesClassNegative
    {
        [Name("decimal")]
        [ValidRange(-201, -12)]
        public decimal Decimal { get; set; }

        [Name("int")]
        [ValidRange(-90, -12)]
        public int Int { get; set; }

        [Name("float")]
        [ValidRange(-17.0f, -1.0f)]
        public float Float { get; set; }

        [Name("double")]
        [ValidRange(-70.0, -42.0)]
        public double Double { get; set; }
    }

    [Fact]
    public void Ranges()
    {
        Assert.True(JsonParser.TryParseJson("""{"int": 20}""", out RangesClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0M, result.Decimal);
        Assert.Equal(20, result.Int);

        Assert.True(JsonParser.TryParseJson("""{"decimal": 35}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(35M, result.Decimal);
        Assert.Equal(0, result.Int);

        Assert.True(
            JsonParser.TryParseJson("""{"decimal": 1, "int": 4, "float": 12, "double": -12.0}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(1M, result.Decimal);
        Assert.Equal(4, result.Int);
        Assert.Equal(12.0f, result.Float);
        Assert.Equal(-12.0f, result.Double);

        Assert.True(
            JsonParser.TryParseJson(
                """{"decimal": 100, "int": 90, "float": 17, "double": 17.0}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(100M, result.Decimal);
        Assert.Equal(90, result.Int);
        Assert.Equal(17.0f, result.Float);
        Assert.Equal(17.0f, result.Double);

        Assert.True(
            JsonParser.TryParseJson(
                """{"decimal": -201, "int": -90, "float": -17.0, "double": -70}""",
                out RangesClassNegative? negativeResult,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(negativeResult);
        Assert.Equal(-201M, negativeResult.Decimal);
        Assert.Equal(-90, negativeResult.Int);
        Assert.Equal(-17.0f, negativeResult.Float);
        Assert.Equal(-70.0f, negativeResult.Double);

        Assert.True(
            JsonParser.TryParseJson(
                """{"decimal": -12, "int": -12, "float": -1, "double": -42.0}""",
                out negativeResult,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(negativeResult);
        Assert.Equal(-12M, negativeResult.Decimal);
        Assert.Equal(-12, negativeResult.Int);
        Assert.Equal(-1.0f, negativeResult.Float);
        Assert.Equal(-42.0f, negativeResult.Double);

        Assert.False(JsonParser.TryParseJson("""{"decimal": -11.99999}""", out negativeResult, out error));
        Assert.Equal("'decimal' was not in the range [-201..-12]", error);
        Assert.Null(negativeResult);

        Assert.False(JsonParser.TryParseJson("""{"float": -0.999}""", out negativeResult, out error));
        Assert.Equal("'float' was not in the range [-17..-1]", error);
        Assert.Null(negativeResult);

        Assert.False(JsonParser.TryParseJson("""{"double": -70.00000001}""", out negativeResult, out error));
        Assert.Equal("'double' was not in the range [-70..-42]", error);
        Assert.Null(negativeResult);

        Assert.False(JsonParser.TryParseJson("""{"decimal": 0}""", out result, out error));
        Assert.Equal("'decimal' was not in the range [1..100]", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"decimal": 101}""", out result, out error));
        Assert.Equal("'decimal' was not in the range [1..100]", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": 3}""", out result, out error));
        Assert.Equal("'int' was not in the range [4..90]", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": 91}""", out result, out error));
        Assert.Equal("'int' was not in the range [4..90]", error);
        Assert.Null(result);
    }

    public sealed class JsonSerializableSettingsClass
    {
        [Name("value")]
        public int Value { get; set; }
    }

    [JsonSerializableSettings(AllowExtraProperties = false)]
    public sealed class JsonSerializableSettingsNoExtrasClass
    {
        [Name("value")]
        public int Value { get; set; }
    }

    [Fact]
    public void JsonParserSettings()
    {
        Assert.False(
            JsonParser.TryParseJson(
                """{"value": 22, "extra": "something"}""",
                out JsonSerializableSettingsNoExtrasClass? resultNoExtras,
                out var error
            )
        );
        Assert.Equal("Unknown property received: extra", error);
        Assert.Null(resultNoExtras);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 22, "extra": "something"}""",
                out JsonSerializableSettingsClass? resultExtras,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(resultExtras);
        Assert.Equal(22, resultExtras.Value);
    }

    [JsonSerializableSettings(AllowExtraProperties = false)]
    public sealed class EmptyFieldTypeClass
    {
        [Required]
        [Name("value")]
        public int Value { get; set; }

        [Name("subclass")]
        public Subclass? SubValue { get; set; }

        [FriendlyName("Subclass")]
        [JsonSerializableSettings(AllowExtraProperties = false)]
        public sealed class Subclass
        {
#pragma warning disable CS0649, S3459, IDE0044
            private int a;
            private int b;
#pragma warning restore CS0649, S3459, IDE0044

            [Name("unused")]
            public int Unused { get; set; }

            public override string ToString() => $"{this.a}-${this.b}";
        }
    }

    [Fact]
    public void EmptyFieldType()
    {
        Assert.False(
            JsonParser.TryParseJson(
                """{"value": 22, "subclass": "hello"}""",
                out EmptyFieldTypeClass? result,
                out var error
            )
        );
        Assert.Equal("subclass > 'Subclass' requires an object.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson("""{"value": 22, "subclass": {"hello": "world"}}""", out result, out error)
        );
        Assert.Equal("subclass > Unknown property received: hello", error);
        Assert.Null(result);
    }

    [SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Spec correct.")]
    public sealed class UUIDv1Class
    {
        [Required]
        [Name("value")]
        public UUIDv1 Value { get; set; }
    }

    [Fact]
    public void UUIDv1Test()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": "e847da22-bb0a-11ed-afa1-0242ac120002"}""",
                out UUIDv1Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(UUIDv1.Parse("e847da22-bb0a-11ed-afa1-0242ac120002"), result.Value);

        Assert.False(
            JsonParser.TryParseJson("""{"value": "e847da22-bb0a-11ed-afa1-asdfasdfasdf"}""", out result, out error)
        );
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": true}""", out result, out error));
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": 44.37}""", out result, out error));
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S3604:Member initializer values should not be redundant",
        Justification = "Testing exactly this."
    )]
    [SuppressMessage(
        "Major Code Smell",
        "S1144:Unused private types or members should be removed",
        Justification = "Testing exactly this."
    )]
    public sealed class NoDefaultConstructorClass
    {
        [Name("value")]
        [DefaultValue(12)]
        public int Value { get; set; } = 17;

        [Name("default")]
        public int Default { get; set; }

        [Name("string")]
        public string? StringValue { get; set; } = "hello world";

        public NoDefaultConstructorClass(int value, string s)
        {
            this.Value = value;
            this.StringValue = s;
        }
    }

    [Fact]
    public void NoDefaultConstructor()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 2, "string": "rock"}""",
                out NoDefaultConstructorClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Equal("rock", result.StringValue);

        Assert.True(JsonParser.TryParseJson("""{"string": "rock"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Equal("rock", result.StringValue);

        Assert.True(JsonParser.TryParseJson("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Null(result.StringValue);

        Assert.False(JsonParser.TryParseJson("asdf", out result, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(result);
    }

    [JsonSerializable]
    public partial struct ValueTypeStruct
    {
        [Name("value")]
        [DefaultValue(12)]
        public int Value { get; set; }

        [Required]
        [Name("string")]
        public string? StringValue { get; set; }
    }

    [Fact]
    public void ValueType()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": 2, "string": "rock"}""", out ValueTypeStruct result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(2, result.Value);
        Assert.Equal("rock", result.StringValue);

        Assert.True(JsonParser.TryParseJson("""{"string": "rock"}""", out result, out error));
        Assert.Null(error);
        Assert.Equal(12, result.Value);
        Assert.Equal("rock", result.StringValue);

        Assert.True(JsonParser.TryParseJson("""{"string": null}""", out result, out error));
        Assert.Null(error);
        Assert.Equal(12, result.Value);
        Assert.Null(result.StringValue);

        Assert.False(JsonParser.TryParseJson("asdf", out result, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Equal(0, result.Value);
        Assert.Null(result.StringValue);

        Assert.True(ValueTypeStruct.TryParseJson("""{"value": 2, "string": "rock"}""", out result, out error));
        Assert.Null(error);
        Assert.Equal(2, result.Value);
        Assert.Equal("rock", result.StringValue);
    }

    public sealed class AllowAnyStringClass
    {
        [Name("value")]
        [AllowAnyString]
        public string? Value { get; set; }

        [Name("trailing")]
        public int Trailing { get; set; }
    }

    [Fact]
    public void AllowAnyString()
    {
        Assert.True(JsonParser.TryParseJson("""{"value": "easy"}""", out AllowAnyStringClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("easy", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(JsonParser.TryParseJson("""{"value": "null", "trailing": 33 }""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("null", result.Value);
        Assert.Equal(33, result.Trailing);

        Assert.True(JsonParser.TryParseJson("""{"value": null}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(JsonParser.TryParseJson("""{"value": 22}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("22", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(JsonParser.TryParseJson("""{"value": "true"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("true", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(JsonParser.TryParseJson("""{"value": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("true", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(JsonParser.TryParseJson("""{"value": false}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("false", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": 14.3365799813410704980910978570980928907092098651089710983}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("14.3365799813410704980910978570980928907092098651089710983", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.False(JsonParser.TryParseJson("""{"value": {}}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": []}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": {"string"}}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);
    }

    public sealed class AllowAnyStringArrayClass
    {
        [Name("value")]
        [AllowAnyString]
        public string?[]? Value { get; set; }
    }

    [Fact]
    public void AllowAnyStringArray()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": ["easy"]}""", out AllowAnyStringArrayClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "easy" }, result.Value);

        Assert.False(JsonParser.TryParseJson("""{"value": "null"}""", out result, out error));
        Assert.Equal("'value' was not an array.", error);
        Assert.Null(result);

        Assert.True(JsonParser.TryParseJson("""{"value": ["null"]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "null" }, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value": [null]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { default(string) }, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value": [22, "hello", null]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "22", "hello", null }, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value": ["true", "false"]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "true", "false" }, result.Value);

        Assert.True(
            JsonParser.TryParseJson(
                """{"value": [14.3365799813410704980910978570980928907092098651089710983]}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "14.3365799813410704980910978570980928907092098651089710983" }, result.Value);

        Assert.False(JsonParser.TryParseJson("""{"value": {}}""", out result, out error));
        Assert.Equal("'value' was not an array.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"value": {"string"}}""", out result, out error));
        Assert.Equal("'value' was not an array.", error);
        Assert.Null(result);
    }

    public sealed class FlexibleBoolClass
    {
        [BoolSerializableSettings(AllowNumbers = true)]
        [Name("int")]
        public bool Int { get; set; }

        [BoolSerializableSettings(AllowFlexibleStrings = true)]
        [Name("strings")]
        public bool Strings { get; set; }

        [BoolSerializableSettings(AllowFlexibleStrings = true, AllowNumbers = true)]
        [Name("both")]
        public bool Both { get; set; }

        [BoolSerializableSettings(SerializationType = BoolSerializableSettingsSerializationType.Numbers)]
        [Name("serialization")]
        public bool Serialization { get; set; }
    }

    [Fact]
    public void FlexibleBool()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"int": true, "strings": true, "both": true, "serialization": 1}""",
                out FlexibleBoolClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Int);
        Assert.True(result.Strings);
        Assert.True(result.Both);
        Assert.True(result.Serialization);

        Assert.True(
            JsonParser.TryParseJson(
                """{"int": false, "strings": false, "both": false, "serialization": 0}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Int);
        Assert.False(result.Strings);
        Assert.False(result.Both);
        Assert.False(result.Serialization);

        // Test int values
        Assert.True(JsonParser.TryParseJson("""{"int": 0}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Int);

        Assert.True(JsonParser.TryParseJson("""{"int": 1}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Int);

        Assert.False(JsonParser.TryParseJson("""{"int": "0"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": -15}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": 2}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": "1"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        // Test string values
        Assert.True(JsonParser.TryParseJson("""{"strings": "false"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Strings);

        Assert.True(JsonParser.TryParseJson("""{"strings": "true"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Strings);

        Assert.True(JsonParser.TryParseJson("""{"strings": "  false "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Strings);

        Assert.True(JsonParser.TryParseJson("""{"strings": " True  "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Strings);

        Assert.True(JsonParser.TryParseJson("""{"strings": "FALSE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Strings);

        Assert.True(JsonParser.TryParseJson("""{"strings": "TRUE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Strings);

        // Test both values
        Assert.True(JsonParser.TryParseJson("""{"both": 0}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": 1}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "0"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "1"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "false"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "true"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "  false "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": " True  "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "FALSE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(JsonParser.TryParseJson("""{"both": "TRUE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.False(JsonParser.TryParseJson("""{"int": "false"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": "01"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"int": "10"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"strings": "1"}""", out result, out error));
        Assert.Equal("'strings' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"strings": "0"}""", out result, out error));
        Assert.Equal("'strings' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"both": "hello"}""", out result, out error));
        Assert.Equal("'both' was not of type bool.", error);
        Assert.Null(result);
    }

    public sealed class DefaultNullableValuesClass
    {
        [Name("int")]
        [DefaultValue(22)]
        public int? IntValue { get; set; }
    }

    [Fact]
    public void DefaultNullableValue()
    {
        Assert.True(JsonParser.TryParseJson("""{}""", out DefaultNullableValuesClass? result, out var error));
        Assert.Null(error);
        Assert.Equal(22, result.IntValue);

        Assert.True(JsonParser.TryParseJson("""{"int": null}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.IntValue);
    }

    public sealed class ListClass
    {
        [Name("value")]
        public required List<int> List { get; set; }
    }

    [Fact]
    public void List()
    {
        Assert.True(JsonParser.TryParseJson("""{"value": [22, 22, 23, 24]}""", out ListClass? result, out var error));
        Assert.Null(error);
        Assert.Equal(new[] { 22, 22, 23, 24 }, result.List);
    }

    public sealed class HashSetClass
    {
        [Name("value")]
        public required HashSet<int> List { get; set; }
    }

    [Fact]
    public void HashSet()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": [22, 22, 23, 24]}""", out HashSetClass? result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(new HashSet<int> { 22, 23, 24 }, result.List);
    }

    public sealed class FrozenSetClass
    {
        [Name("value")]
        public required FrozenSet<int> List { get; set; }
    }

    [Fact]
    public void FrozenSet()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value": [22, 22, 23, 24]}""", out FrozenSetClass? result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(new HashSet<int> { 22, 23, 24 }, result.List);
    }

    [FriendlyName("Required Property Init")]
    public sealed class RequiredPropertyInitClass
    {
        [Name("username")]
        public required string Username { get; init; }

        [Name("password")]
        public required string Password { get; init; }

        [Name("remember")]
        public bool RememberMe { get; init; }

        [Name("coupon")]
        public string? Coupon { get; init; }
    }

    [Fact]
    public void RequiredPropertyInit()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"username":"tron","password":"hunter2","coupon":"1234"}""",
                out RequiredPropertyInitClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("tron", result.Username);
        Assert.Equal("hunter2", result.Password);
        Assert.False(result.RememberMe);
        Assert.Equal("1234", result.Coupon);

        Assert.False(JsonParser.TryParseJson("""{"username":"tron","coupon":"1234"}""", out result, out error));
        Assert.Equal("A required property was missing on Required Property Init. Missing: password.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"password":"hunter2","coupon":"1234"}""", out result, out error));
        Assert.Equal("A required property was missing on Required Property Init. Missing: username.", error);
        Assert.Null(result);

        Assert.True(JsonParser.TryParseJson("""{"username":"tron","password":"hunter2"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("tron", result.Username);
        Assert.Equal("hunter2", result.Password);
        Assert.False(result.RememberMe);
        Assert.Null(result.Coupon);
    }

    public sealed class DefaultsClass
    {
        [Name("s8")]
        [DefaultValue(sbyte.MinValue)]
        public sbyte S8 { get; set; }

        [Name("s16")]
        [DefaultValue(short.MinValue)]
        public short S16 { get; set; }

        [Name("s32")]
        [DefaultValue(int.MinValue)]
        public int S32 { get; set; }

        [Name("s64")]
        [DefaultValue(long.MinValue)]
        public long S64 { get; set; }

        [Name("u8")]
        [DefaultValue(byte.MaxValue)]
        public byte U8 { get; set; }

        [Name("u16")]
        [DefaultValue(ushort.MaxValue)]
        public ushort U16 { get; set; }

        [Name("u32")]
        [DefaultValue(uint.MaxValue)]
        public uint U32 { get; set; }

        [Name("u64")]
        [DefaultValue(ulong.MaxValue)]
        public ulong U64 { get; set; }

        [Name("f32")]
        [DefaultValue(float.MaxValue)]
        public float F32 { get; set; }

        [Name("f64")]
        [DefaultValue(double.MaxValue)]
        public double F64 { get; set; }

        [Name("decimal")]
        [DefaultValue(typeof(decimal), "79228162514264337593543950335")]
        public decimal Decimal { get; set; }

        [Name("timespan")]
        [DefaultValue(typeof(TimeSpan), "20:30")]
        public TimeSpan TimeSpan { get; set; }

        [Name("datetime")]
        [DefaultValue(typeof(DateTime), "1970-01-01T00:00:00Z")]
        public DateTime DateTime { get; set; }

        [Name("datetimeoffset")]
        [DefaultValue(typeof(DateTimeOffset), "1970-01-01T00:00:00Z")]
        public DateTimeOffset DateTimeOffset { get; set; }
    }

    [Fact]
    public void Defaults()
    {
        Assert.True(JsonParser.TryParseJson("""{}""", out DefaultsClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);

        Assert.Equal(sbyte.MinValue, result.S8);
        Assert.Equal(short.MinValue, result.S16);
        Assert.Equal(int.MinValue, result.S32);
        Assert.Equal(long.MinValue, result.S64);

        Assert.Equal(byte.MaxValue, result.U8);
        Assert.Equal(ushort.MaxValue, result.U16);
        Assert.Equal(uint.MaxValue, result.U32);
        Assert.Equal(ulong.MaxValue, result.U64);

        Assert.Equal(float.MaxValue, result.F32);
        Assert.Equal(double.MaxValue, result.F64);
        Assert.Equal(decimal.MaxValue, result.Decimal);

        Assert.Equal(TimeSpan.Parse("20:30", CultureInfo.InvariantCulture), result.TimeSpan);
        Assert.Equal(DateTime.UnixEpoch.ToLocalTime(), result.DateTime);
        Assert.Equal(DateTimeOffset.UnixEpoch, result.DateTimeOffset);

        Assert.True(
            JsonParser.TryParseJson(
                """
                {
                    "s8": -15,
                    "s16": -15,
                    "s32": -15,
                    "s64": -15,
                    "u8": 15,
                    "u16": 15,
                    "u32": 15,
                    "u64": 15,
                    "f32": 15.2,
                    "f64": 15.2,
                    "decimal": 15.2,
                    "timespan": "30",
                    "datetime": "1971-01-01T00:00:00Z",
                    "datetimeoffset": "1971-01-01T00:00:00Z"
                }
                """,
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);

        Assert.Equal(-15, result.S8);
        Assert.Equal(-15, result.S16);
        Assert.Equal(-15, result.S32);
        Assert.Equal(-15, result.S64);

        Assert.Equal(15U, result.U8);
        Assert.Equal(15U, result.U16);
        Assert.Equal(15U, result.U32);
        Assert.Equal(15U, result.U64);

        Assert.Equal(15.2f, result.F32);
        Assert.Equal(15.2, result.F64);
        Assert.Equal(15.2m, result.Decimal);

        Assert.Equal(TimeSpanParser.Parse("30"), result.TimeSpan);
        Assert.Equal(DateTime.Parse("1971-01-01T00:00:00Z", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTimeOffset.Parse("1971-01-01T00:00:00Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
    }

    [Fact]
    public void IndividualInts()
    {
        Assert.True(JsonParser.TryParseJson("12", out int int32, out var error));
        Assert.Null(error);
        Assert.Equal(12, int32);

        Assert.False(JsonParser.TryParseJson("h", out int32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Equal(0, int32);

        Assert.False(JsonParser.TryParseJson(string.Empty, out int32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Equal(0, int32);

        Assert.True(JsonParser.TryParseJson("12", out int? nullableInt32, out error));
        Assert.Null(error);
        Assert.Equal(12, nullableInt32);

        Assert.False(JsonParser.TryParseJson("h", out nullableInt32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(nullableInt32);

        Assert.False(JsonParser.TryParseJson(string.Empty, out nullableInt32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(nullableInt32);

        Assert.True(JsonParser.TryParseJson("null", out nullableInt32, out error));
        Assert.Null(error);
        Assert.Null(nullableInt32);
    }

    [Fact]
    public void IndividualStrings()
    {
        Assert.False(JsonParser.TryParseJson("null", out string? stringValue, out var error));
        Assert.Equal("Was null. Should be of type string.", error);
        Assert.Null(stringValue);

        Assert.True(JsonParser.TryParseJson("\"hello\"", out stringValue, out error));
        Assert.Null(error);
        Assert.Equal("hello", stringValue);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S101:Types should be named in PascalCase",
        Justification = "Test for exactly this."
    )]
    [SuppressMessage(
        "Naming",
        "CA1710:Identifiers should have correct suffix",
        Justification = "Consistency with other tests."
    )]
    public sealed class IDictionaryClass<TKey, TValue> : IDictionary<TKey, TValue>
        where TKey : notnull
    {
        private readonly Dictionary<TKey, TValue> dictionary = new();

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Add(KeyValuePair<TKey, TValue> item) => this.dictionary.Add(item.Key, item.Value);

        public void Clear() => this.dictionary.Clear();

        public bool Contains(KeyValuePair<TKey, TValue> item) => this.dictionary.Contains(item);

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => throw new NotSupportedException();

        public bool Remove(KeyValuePair<TKey, TValue> item) => this.dictionary.Remove(item.Key);

        public int Count => this.dictionary.Count;

        public bool IsReadOnly => false;

        public void Add(TKey key, TValue value) => this.dictionary.Add(key, value);

        public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

        public bool Remove(TKey key) => this.dictionary.Remove(key);

        public bool TryGetValue(TKey key, out TValue value) => this.dictionary.TryGetValue(key, out value!);

        public TValue this[TKey key]
        {
            get => this.dictionary[key];
            set => this.dictionary[key] = value;
        }

        public ICollection<TKey> Keys => this.dictionary.Keys;

        public ICollection<TValue> Values => this.dictionary.Values;
    }

    [Fact]
    public void IndividualDictionary()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out Dictionary<string, string>? strings,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } }, strings);

        Assert.False(
            JsonParser.TryParseJson(
                """{"hello": "world", "kestrel": "toolbox", "hello": "failure"}""",
                out strings,
                out error
            )
        );
        Assert.Equal("Has duplicate key.", error);
        Assert.Null(strings);

        Assert.True(
            JsonParser.TryParseJson(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out SortedDictionary<string, string>? sortedStrings,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(
            new SortedDictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } },
            sortedStrings
        );

        Assert.False(
            JsonParser.TryParseJson(
                """{"hello": "world", "kestrel": "toolbox", "hello": "failure"}""",
                out sortedStrings,
                out error
            )
        );
        Assert.Equal("Has duplicate key.", error);
        Assert.Null(sortedStrings);

        Assert.True(
            JsonParser.TryParseJson("""{"hello": 1, "kestrel": 32}""", out Dictionary<string, int>? ints, out error)
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, int> { { "hello", 1 }, { "kestrel", 32 } }, ints);

        Assert.True(
            JsonParser.TryParseJson(
                """{"first": {"class": 22, "struct": true}, "second": {"class": 11, "struct": false}}""",
                out Dictionary<string, CustomParseClass>? customParsed,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new Dictionary<string, CustomParseClass>
            {
                {
                    "first",
                    new CustomParseClass
                    {
                        Class = new CustomParseClass.CustomValueClass { Value = 22 },
                        Struct = new CustomParseClass.CustomValueStruct { Value = true },
                    }
                },
                {
                    "second",
                    new CustomParseClass
                    {
                        Class = new CustomParseClass.CustomValueClass { Value = 11 },
                        Struct = new CustomParseClass.CustomValueStruct { Value = false },
                    }
                },
            },
            customParsed
        );

        Assert.True(
            JsonParser.TryParseJson(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out IDictionaryClass<string, string>? expando,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } }, expando);

        Assert.True(
            JsonParser.TryParseJson(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out FrozenDictionary<string, string>? frozen,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } }, frozen);
    }

    [Fact]
    public void IndividualLists()
    {
        Assert.True(JsonParser.TryParseJson("""["a", "b", "c"]""", out List<string>? stringList, out var error));
        Assert.Null(error);
        Assert.Equal(new List<string> { "a", "b", "c" }, stringList);

        Assert.True(JsonParser.TryParseJson("""["a", "b", "c"]""", out string[]? stringArray, out error));
        Assert.Null(error);
        Assert.Equal(new[] { "a", "b", "c" }, stringArray);

        Assert.True(JsonParser.TryParseJson("""[4, 3, 2, 1]""", out List<int>? intList, out error));
        Assert.Null(error);
        Assert.Equal(new List<int> { 4, 3, 2, 1 }, intList);

        Assert.True(
            JsonParser.TryParseJson("""["aGVsbG8K", "d29ybGQK"]""", out List<byte[]>? byteArrayList, out error)
        );
        Assert.Null(error);
        Assert.Equal(
            new List<byte[]> { Convert.FromBase64String("aGVsbG8K"), Convert.FromBase64String("d29ybGQK") },
            byteArrayList
        );

        Assert.True(
            JsonParser.TryParseJson(
                """[["hello", "world"], ["goodbye", "world"]]""",
                out List<string[]>? stringArrayList,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new List<string[]> { new[] { "hello", "world" }, new[] { "goodbye", "world" } }, stringArrayList);

        Assert.True(JsonParser.TryParseJson("""[4, 1, 2, 3]""", out int[]? intArray, out error));
        Assert.Null(error);
        Assert.Equal(new[] { 4, 1, 2, 3 }, intArray);

        Assert.False(JsonParser.TryParseJson("""[4, null, 2, 3]""", out intArray, out error));
        Assert.Equal("Was null. Should be of type int32.", error);
        Assert.Null(intArray);

        Assert.True(
            JsonParser.TryParseJson(
                """[{"class": 22, "struct": false}, {"class": 11, "struct": true}]""",
                out List<CustomParseClass>? customList,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new List<CustomParseClass>
            {
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 22 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = false },
                },
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 11 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = true },
                },
            },
            customList
        );

        Assert.True(
            JsonParser.TryParseJson(
                """[{"class": 22, "struct": true}, {"class": 11, "struct": true}]""",
                out CustomParseClass[]? customArray,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new List<CustomParseClass>
            {
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 22 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = true },
                },
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 11 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = true },
                },
            },
            customArray
        );

        Assert.True(JsonParser.TryParseJson("[1, 2, null]", out int?[]? nullableIntArray, out error));
        Assert.Null(error);
        Assert.Equal(new int?[] { 1, 2, null }, nullableIntArray);
    }

    [Fact]
    public void IndividualGeneral()
    {
        Assert.True(JsonParser.TryParseJson("\"2aa36c42-2c81-4be8-8986-4f91733051b4\"", out Guid guid, out var error));
        Assert.Null(error);
        Assert.Equal(Guid.Parse("2aa36c42-2c81-4be8-8986-4f91733051b4"), guid);

        Assert.True(JsonParser.TryParseJson("\"9c7668aa-0477-11ee-be56-0242ac120002\"", out UUIDv1 uuid, out error));
        Assert.Null(error);
        Assert.Equal(UUIDv1.Parse("9c7668aa-0477-11ee-be56-0242ac120002"), uuid);

        Assert.True(JsonParser.TryParseJson("\"May 3, 2010\"", out DateTime dateTime, out error));
        Assert.Null(error);
        Assert.Equal(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), dateTime);

        Assert.True(JsonParser.TryParseEnum("\"superLongName\"", out TestEnum testEnum, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.SuperLong, testEnum);

        Assert.True(JsonParser.TryParseEnum("1", out TestNumberEnum testNumberEnum, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.B, testNumberEnum);

        Assert.True(JsonParser.TryParseJson("\"aGVsbG8=\"", out byte[]? bytes, out error));
        Assert.Null(error);
        Assert.Equal("hello"u8.ToArray(), bytes);
    }

    public class NewtonsoftJObjectClass
    {
        [Name("object")]
        public JObject? ObjectValue { get; set; }

        [Name("array")]
        [SkipSerialization(Condition.WhenNull)]
        public JArray? ArrayValue { get; set; }

        [Name("token")]
        public JToken? TokenValue { get; set; }

        public static bool TryParseJToken(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out JToken? value,
            [NotNullWhen(false)] out string? error
        )
        {
#pragma warning disable IDE0010
            switch (reader.TokenType)
#pragma warning restore IDE0010
            {
                case JsonTokenType.StartObject:
                {
                    using var doc = JsonDocument.ParseValue(ref reader);
                    var text = doc.RootElement.GetRawText();
                    value = JObject.Parse(text);
                    error = default;
                    return true;
                }

                case JsonTokenType.StartArray:
                {
                    using var doc = JsonDocument.ParseValue(ref reader);
                    var text = doc.RootElement.GetRawText();
                    value = JArray.Parse(text);
                    error = default;
                    return true;
                }

                case JsonTokenType.False:
                case JsonTokenType.True:
                case JsonTokenType.Number:
                case JsonTokenType.String:
                {
                    using var doc = JsonDocument.ParseValue(ref reader);
                    var text = doc.RootElement.GetRawText();
                    value = JToken.Parse(text);
                    error = default;
                    return true;
                }

                default:
                {
                    value = default;
                    error = "Unknown json type.";
                    return false;
                }
            }
        }

        public static bool TryParseJObject(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out JObject? value,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.StartObject)
            {
                value = default;
                error = "Was not an object.";
                return false;
            }

            using var doc = JsonDocument.ParseValue(ref reader);
            var text = doc.RootElement.GetRawText();
            value = JObject.Parse(text);
            error = default;
            return true;
        }

        public static bool TryParseJArray(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out JArray? value,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.StartArray)
            {
                value = default;
                error = "Was not an array.";
                return false;
            }

            using var doc = JsonDocument.ParseValue(ref reader);
            var text = doc.RootElement.GetRawText();
            value = JArray.Parse(text);
            error = default;
            return true;
        }
    }

    [Fact]
    public void NewtonsoftJObject()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"object":{"hello":"world","value":12},"array":[12,13,"hello",true],"token":"kestrel toolbox"}""",
                out NewtonsoftJObjectClass? value,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(value);
        Assert.Equal(new JObject { ["hello"] = "world", ["value"] = 12 }, value.ObjectValue);
        Assert.Equal(new JArray(12, 13, "hello", true), value.ArrayValue);
        Assert.Equal(new JValue("kestrel toolbox"), value.TokenValue);

        Assert.True(JsonParser.TryParseJson("""{"object":null,"token":null}""", out value, out error));
        Assert.Null(error);
        Assert.NotNull(value);
        Assert.Null(value.ObjectValue);
        Assert.Null(value.ArrayValue);
        Assert.Null(value.TokenValue);

        Assert.False(JsonParser.TryParseJson("""{"object":["array"]}""", out value, out error));
        Assert.Equal("object > Was not an object.", error);
        Assert.Null(value);

        Assert.False(JsonParser.TryParseJson("""{"array":{}}""", out value, out error));
        Assert.Equal("array > Was not an array.", error);
        Assert.Null(value);
    }

    [Fact]
    public void Comments()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """
                {
                    "value": 14,
                    // This is the first line of comment
                     "nullable": -1993,
                    "default":
                    // Second comment
                    188372,
                    "valid": 22,
                }
                """,
                out Int32Class? value,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new Int32Class
            {
                Value = 14,
                Default = 188372,
                Nullable = -1993,
                Valid = 22,
            },
            value
        );
    }

    public sealed class EmailAddressClass
    {
        [Name("value")]
        [EmailAddress]
        public required string Value { get; set; }
    }

    [Fact]
    public void EmailAddress()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value":"email@example.com"}""", out EmailAddressClass? result, out var error)
        );
        Assert.Null(error);
        Assert.Equal("email@example.com", result.Value);

        Assert.False(JsonParser.TryParseJson("""{"value":"notarealemail"}""", out result, out error));
        Assert.Equal("'value' is not a valid email address.", error);
        Assert.Null(result);

        Assert.True(EmailAddressAttribute.IsValid("email@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("email+test@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("email+test+2@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("email+test+2@192.168.1.15"));

        Assert.True(EmailAddressAttribute.IsValid("customer/department=shipping@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("!def!xyz%abc@example.com"));

        Assert.False(EmailAddressAttribute.IsValid("notarealemail"));
        Assert.False(EmailAddressAttribute.IsValid("this email has spacesss@example.com"));
        Assert.False(EmailAddressAttribute.IsValid("look!@my_ampersand@example.com"));

        // Controversial email addresses:
        Assert.False(EmailAddressAttribute.IsValid("Abc\\@def@example.com"));
    }

    public sealed class TrimClass
    {
        [Name("value")]
        [TrimValue]
        [StringLength(8)]
        public required string Value { get; set; }

        [Name("long")]
        [TrimValue(OnParse = false)]
        [StringLength(8)]
        public required string? Long { get; set; }
    }

    [Fact]
    public void Trim()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value":"    hello    ","long":" world "}""",
                out TrimClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal(" world ", result.Long);

        Assert.True(JsonParser.TryParseJson("""{"value":"01234567","long":"abcd"}""", out result, out error));
        Assert.Null(error);
        Assert.Equal("01234567", result.Value);
        Assert.Equal("abcd", result.Long);

        Assert.False(JsonParser.TryParseJson("""{"value":"    0123456    7","long":"short"}""", out result, out error));
        Assert.Equal("'value' must be shorter than 8 characters.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson("""{"value":"hello","long":"     whitespace     "}""", out result, out error)
        );
        Assert.Equal("'long' must be shorter than 8 characters.", error);
        Assert.Null(result);
    }

    public sealed class NullOnEmptyClass
    {
        [Name("value")]
        [NullOnEmpty(OnSerialize = false)]
        [TrimValue]
        public string? Value { get; set; }

        [Name("emptyOnly")]
        [NullOnEmpty]
        public string? EmptyOnly { get; set; }
    }

    [Fact]
    public void NullOnEmptyTest()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"value":"    hello    ","emptyOnly":"  world  "}""",
                out NullOnEmptyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal("  world  ", result.EmptyOnly);

        Assert.True(JsonParser.TryParseJson("""{"value":"  ","emptyOnly":" "}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(" ", result.EmptyOnly);

        Assert.True(JsonParser.TryParseJson("""{"value":"","emptyOnly":""}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Null(result.EmptyOnly);
    }

    public sealed class NullableStringClass
    {
        [Name("nullable")]
        [TrimValue]
        [StringLength(8)]
        public required string?[] Nullable { get; set; }

        [Name("exists")]
        [TrimValue]
        [StringLength(8)]
        public required string[] Exists { get; set; }

        [Name("notnull")]
        [AllowAnyString]
        public required string NotNull { get; set; }
    }

    [Fact]
    public void NullableString()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"nullable":["a", "b", null, "c"], "exists": ["a", "b", "c"], "notnull": "something"}""",
                out NullableStringClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal(new[] { "a", "b", null, "c" }, result.Nullable);
        Assert.Equal(new[] { "a", "b", "c" }, result.Exists);
        Assert.Equal("something", result.NotNull);

        Assert.True(
            JsonParser.TryParseJson(
                """{"nullable":["a", "b  ", null, "c"], "exists": ["   a", "b", "c"], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new[] { "a", "b", null, "c" }, result.Nullable);
        Assert.Equal(new[] { "a", "b", "c" }, result.Exists);
        Assert.Equal("something", result.NotNull);

        Assert.False(
            JsonParser.TryParseJson(
                """{"nullable":["a", "b", "c", "0123456789"], "exists": ["a", "b", "c"], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'nullable' must be shorter than 8 characters.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson(
                """{"nullable":["a", "b", "c"], "exists": ["a", "b", "c", null], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'exists' was null. Should be of type string.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson(
                """{"nullable":["a", "b", "c", null], "exists": ["a", "b", "c", null], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'exists' was null. Should be of type string.", error);
        Assert.Null(result);

        Assert.False(
            JsonParser.TryParseJson(
                """{"nullable":["a", "b", "c", null], "exists": ["a", "b", "c"], "notnull": null}""",
                out result,
                out error
            )
        );
        Assert.Equal("'notnull' was null. Should be of type string.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "Testing this feature."
    )]
    public enum CustomParsingStringEnum
    {
        A,
        B,
        Other,
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "Testing this feature."
    )]
    public enum CustomParsingNumberEnum
    {
        A,
        B,
        Other,
    }

    public sealed class CustomParsingEnumClass
    {
        [Name("string")]
        public CustomParsingStringEnum String { get; set; }

        [Name("number")]
        public CustomParsingNumberEnum Number { get; set; }

        public static bool TryParseNumberValue(
            ref Utf8JsonReader reader,
            out CustomParsingNumberEnum value,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.Number)
            {
                value = default;
                error = $"Invalid {nameof(CustomParsingNumberEnum)}.";
                return false;
            }

            var i = reader.GetInt32();
            if (i is < 0 or > 2)
            {
                value = CustomParsingNumberEnum.Other;
            }
            else
            {
                value = (CustomParsingNumberEnum)i;
            }

            error = default;
            return true;
        }
    }

    [Fact]
    public void TestCustomParsingStringEnum()
    {
        Assert.True(JsonParser.TryParseString("\"a\"", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(CustomParsingStringEnum.A, result);

        Assert.True(JsonParser.TryParseString("\"b\"", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingStringEnum.B, result);

        Assert.True(JsonParser.TryParseString("\"whatever\"", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingStringEnum.Other, result);

        Assert.False(JsonParser.TryParseString("72", out result, out error));
        Assert.Equal("CustomParsingStringEnum must be a string.", error);
        Assert.Equal(CustomParsingStringEnum.Other, result);

        Assert.False(JsonParser.TryParseString("true", out result, out error));
        Assert.Equal("CustomParsingStringEnum must be a string.", error);
        Assert.Equal(CustomParsingStringEnum.Other, result);
    }

    [Fact]
    public void TestCustomParsingNumberEnum()
    {
        Assert.True(JsonParser.TryParseNumber("0", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(CustomParsingNumberEnum.A, result);

        Assert.True(JsonParser.TryParseNumber("1", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingNumberEnum.B, result);

        Assert.True(JsonParser.TryParseNumber("72", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingNumberEnum.Other, result);

        Assert.False(JsonParser.TryParseNumber("\"a\"", out result, out error));
        Assert.Equal("Invalid CustomParsingNumberEnum.", error);
        Assert.Equal(CustomParsingNumberEnum.A, result);

        Assert.False(JsonParser.TryParseNumber("true", out result, out error));
        Assert.Equal("Invalid CustomParsingNumberEnum.", error);
        Assert.Equal(CustomParsingNumberEnum.A, result);
    }

    [Fact]
    public void TestCustomParsingEnumClass()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"string":"B", "number":1}""", out CustomParsingEnumClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(CustomParsingStringEnum.B, result.String);
        Assert.Equal(CustomParsingNumberEnum.B, result.Number);

        Assert.True(JsonParser.TryParseJson("""{"string":"whatever", "number":72}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(CustomParsingStringEnum.Other, result.String);
        Assert.Equal(CustomParsingNumberEnum.Other, result.Number);

        Assert.False(JsonParser.TryParseJson("""{"string":1, "number":1}""", out result, out error));
        Assert.Equal("string > CustomParsingStringEnum must be a string.", error);
        Assert.Null(result);

        Assert.False(JsonParser.TryParseJson("""{"string":"B", "number":"a"}""", out result, out error));
        Assert.Equal("number > Invalid CustomParsingNumberEnum.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(InvalidValue = Unknown)]
    public enum InvalidValueEnum
    {
        [Name("a")]
        A,

        [Name("unknown")]
        Unknown,
    }

    public sealed class InvalidValueEnumClass
    {
        [Name("value")]
        public InvalidValueEnum Value { get; set; }
    }

    [Fact]
    public void TestInvalidValueEnum()
    {
        Assert.True(JsonParser.TryParseJson("""{"value":"a"}""", out InvalidValueEnumClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.A, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value":"unknown"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.Unknown, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value":"asdf"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.Unknown, result.Value);
    }

    [JsonSerializable]
    public sealed partial class TypedefJsonNullable
    {
        [Typedef(typeof(string), TypedefFeatures.All)]
        public readonly partial struct TypedefString { }

        [Typedef(typeof(int), TypedefFeatures.All)]
        public readonly partial struct TypedefInt { }

        [Typedef(typeof(InvalidValueEnumClass), TypedefFeatures.JsonSerializable)]
        public readonly partial struct TypedefComplex { }

        [Typedef(typeof(CustomParseClass), TypedefFeatures.JsonSerializable)]
        public readonly partial struct TypedefCustom { }

        [Name("string")]
        public TypedefString? StringValue { get; set; }

        [Name("int")]
        public TypedefInt? IntValue { get; set; }

        [Name("complex")]
        public TypedefComplex? ComplexValue { get; set; }

        [Name("custom")]
        public TypedefCustom? CustomValue { get; set; }
    }

    [Fact]
    public void TestTypedefJsonNullable()
    {
        Assert.True(
            JsonParser.TryParseJson(
                """{"string": "hello", "int": 3, "complex": { "value": "a" }, "custom": {"class": 22, "struct": true}}""",
                out TypedefJsonNullable? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello", (string?)result.StringValue);
        Assert.Equal(3, (int?)result.IntValue);
        var complex = (InvalidValueEnumClass)Assert.NotNull(result.ComplexValue);
        Assert.Equal(InvalidValueEnum.A, complex.Value);
        var custom = (CustomParseClass)Assert.NotNull(result.CustomValue);
        Assert.NotNull(custom.Class);
        Assert.Equal(22, custom.Class.Value);
        Assert.True(custom.Struct.Value);

        Assert.True(
            JsonParser.TryParseJson(
                """{"string": null, "int": null, "complex": null, "custom": null}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Null(result.StringValue);
        Assert.Null(result.IntValue);
        Assert.Null(result.ComplexValue);
        Assert.Null(result.CustomValue);
    }

    public static class CustomParsingEnumSerialization
    {
        public static bool DoStringParsing(
            ref Utf8JsonReader reader,
            out CustomParsingStringEnum result,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = "CustomParsingStringEnum must be a string.";
                result = CustomParsingStringEnum.Other;
                return false;
            }

            var value = reader.GetString()!;
            result = value.ToLowerInvariant() switch
            {
                "a" => CustomParsingStringEnum.A,
                "b" => CustomParsingStringEnum.B,
                _ => CustomParsingStringEnum.Other,
            };

            error = default;
            return true;
        }
    }

    public sealed class CustomPropertyParsingClass
    {
        [Name("value")]
        [JsonSerializableCustomProperty(
            ParseMethod = "UnitTests.Serialization.JsonSerializableParseTests.CustomPropertyParsingClass.TryParseInt"
        )]
        public required int? Value { get; set; }

        public static bool TryParseInt(ref Utf8JsonReader reader, out int value, [NotNullWhen(false)] out string? error)
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = "Was not of type string.";
                value = default;
                return false;
            }

            var s = reader.GetString()!;
            if (!s.StartsWith("0x", StringComparison.Ordinal))
            {
                error = "Was not a hex string.";
                value = default;
                return false;
            }

            if (
                !int.TryParse(
                    s.AsSpan(2),
                    NumberStyles.HexNumber | NumberStyles.AllowHexSpecifier,
                    CultureInfo.InvariantCulture,
                    out value
                )
            )
            {
                error = "Was not parsable as hex.";
                value = default;
                return false;
            }

            error = null;
            return true;
        }
    }

    [Fact]
    public void CustomPropertyParsing()
    {
        Assert.True(
            JsonParser.TryParseJson("""{"value":"0xab"}""", out CustomPropertyParsingClass? result, out var error)
        );
        Assert.NotNull(result);
        Assert.Null(error);
        Assert.Equal(0xAB, result.Value);

        Assert.True(JsonParser.TryParseJson("""{"value":null}""", out result, out error));
        Assert.NotNull(result);
        Assert.Null(error);
        Assert.Null(result.Value);

        Assert.False(JsonParser.TryParseJson("""{"value":22}""", out result, out error));
        Assert.Null(result);
        Assert.Equal("value > Was not of type string.", error);

        Assert.False(JsonParser.TryParseJson("""{"value":"FF"}""", out result, out error));
        Assert.Null(result);
        Assert.Equal("value > Was not a hex string.", error);
    }
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
