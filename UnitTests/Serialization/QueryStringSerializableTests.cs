// <copyright file="QueryStringSerializableTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Frozen;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests.Serialization;

/// <summary>
/// Tests for <see cref="QueryStringSerializableAttribute"/>.
/// </summary>
[QueryStringSerializable(typeof(Int32Class))]
public partial class QueryStringSerializableTests { }

/// <summary>
/// Tests for <see cref="QueryStringSerializableAttribute"/>.
/// </summary>
[QueryStringSerializable(typeof(UInt32Class))]
[QueryStringSerializable(typeof(IntegerTypesClass))]
[QueryStringSerializable(typeof(DecimalTypesClass))]
[QueryStringSerializable(typeof(StringClass))]
[QueryStringSerializable(typeof(DateTimeClass))]
[QueryStringSerializable(typeof(DateOnlyClass))]
[QueryStringSerializable(typeof(TimeOnlyClass))]
[QueryStringSerializable(typeof(TimeSpanClass))]
[QueryStringSerializable(typeof(EnumClass))]
[QueryStringSerializable(typeof(NumberEnumClass))]
[QueryStringSerializable(typeof(ArrayClass))]
[QueryStringSerializable(typeof(ArrayWithAllTypesClass))]
[QueryStringSerializable(typeof(CustomIndividualObjectClass))]
[QueryStringSerializable(typeof(CustomArrayObjectClass))]
[QueryStringSerializable(typeof(NestedCustomParseClass))]
[QueryStringSerializable(typeof(GuidObject))]
[QueryStringSerializable(typeof(NullValuesObjectClass))]
[QueryStringSerializable(typeof(SpecialStringsObject))]
[QueryStringSerializable(typeof(RangesClass))]
[QueryStringSerializable(typeof(RangesClassNegative))]
[QueryStringSerializable(typeof(UUIDv1Class))]
[QueryStringSerializable(typeof(NoDefaultConstructorClass))]
[QueryStringSerializable(typeof(ValueTypeStruct), typeof(ValueTypeStruct?))]
[QueryStringSerializable(typeof(BytesClass))]
[QueryStringSerializable(typeof(DefaultNullableValuesClass))]
[QueryStringSerializable(typeof(ListClass))]
[QueryStringSerializable(typeof(HashSetClass))]
[QueryStringSerializable(typeof(FrozenSetClass))]
[QueryStringSerializable(typeof(DefaultsClass))]
[QueryStringSerializable(typeof(RequiredPropertyInitClass))]
[QueryStringSerializable(typeof(TrimClass))]
[QueryStringSerializable(typeof(NullOnEmptyClass))]
[QueryStringSerializable(typeof(EmailAddressClass))]
[QueryStringSerializable(typeof(InvalidValueEnumClass))]
[EnumSerializable(typeof(TestEnum))]
[EnumSerializable(typeof(TestNumberEnum))]
[EnumSerializable(typeof(InvalidValueEnum))]
[SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Tests describe what things are.")]
public partial class QueryStringSerializableTests
{
    [QueryStringSerializable(typeof(BoolClass))]
    public sealed partial class BoolClass
    {
        [Name("value")]
        [DefaultValue(true)]
        public bool Value { get; set; }

        [Name("nullable")]
        public bool? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(true)]
        public bool Default { get; set; }
    }

    [Fact]
    public void Bool()
    {
        Assert.True(
            BoolClass.TryParseQueryString(
                ParseToCollection("value=true&nullable=true&default=0"),
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.True(result.Nullable);
        Assert.False(result.Default);

        Assert.True(
            BoolClass.TryParseQueryString(
                ParseToCollection("value=false&nullable=0&default=true"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.False(result.Nullable);
        Assert.True(result.Default);

        Assert.True(BoolClass.TryParseQueryString(ParseToCollection("value=1&nullable=0"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.False(result.Nullable);
        Assert.True(result.Default);

        Assert.True(BoolClass.TryParseQueryString(ParseToCollection("nullable=0"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.False(result.Nullable);
        Assert.True(result.Default);

        Assert.True(BoolClass.TryParseQueryString(ParseToCollection("value=1&default=1"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.Null(result.Nullable);
        Assert.True(result.Default);

        Assert.True(BoolClass.TryParseQueryString(ParseToCollection("value=1"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.Null(result.Nullable);
        Assert.True(result.Default);

        Assert.False(BoolClass.TryParseQueryString(ParseToCollection("nullable"), out result, out error));
        Assert.Equal("'nullable' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseQueryString(ParseToCollection("default"), out result, out error));
        Assert.Equal("'default' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseQueryString(ParseToCollection("value=hello"), out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseQueryString(ParseToCollection("value=17"), out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(BoolClass.TryParseQueryString(ParseToCollection("value=007"), out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.True(
            BoolClass.TryParseQueryString(
                ParseToCollection("value=true&nullable=true&default=0"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.True(result.Nullable);
        Assert.False(result.Default);
    }

    public sealed class Int32Class
    {
        [Name("value")]
        [ValidRange(-20, 30)]
        public int Value { get; set; }

        [Name("nullable")]
        [ValidRange(-2000, 40)]
        public int? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(22)]
        [ValidRange(0, 2_000_000)]
        public int Default { get; set; }

        [Name("valid")]
        [ValidValues(22, 22, 24, 2001, 100)]
        public int Valid { get; set; }
    }

    [Fact]
    public void Int32Test()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=14&nullable=32&valid=24"),
                out Int32Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Equal(32, result.Nullable);
        Assert.Equal(24, result.Valid);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=12&nullable=-1993&default=188372&valid=22"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(188372, result.Default);
        Assert.Equal(-1993, result.Nullable);
        Assert.Equal(22, result.Valid);

        Assert.True(TryParseQueryString(ParseToCollection("value=-20"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-20, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);
        Assert.Equal(0, result.Valid);

        Assert.True(TryParseQueryString(ParseToCollection("value=30&default=2000000"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(30, result.Value);
        Assert.Equal(2000000, result.Default);
        Assert.Null(result.Nullable);
        Assert.Equal(0, result.Valid);

        Assert.True(TryParseQueryString(new QueryCollection(), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(TryParseQueryString(ParseToCollection("value=true"), out result, out error));
        Assert.Equal("'value' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=22.3"), out result, out error));
        Assert.Equal("'value' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("nullable=74"), out result, out error));
        Assert.Equal("'nullable' was not in the range [-2000..40]", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("nullable=1001848720002704720027"), out result, out error));
        Assert.Equal("'nullable' was not of type int32.", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(ParseToCollection("nullable2=true"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.True(TryParseQueryString(ParseToCollection("value678=true"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(TryParseQueryString(ParseToCollection("value=-22"), out result, out error));
        Assert.Equal("'value' was not in the range [-20..30]", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("default=-1"), out result, out error));
        Assert.Equal("'default' was not in the range [0..2000000]", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("valid=23"), out result, out error));
        Assert.Equal("'valid' must be one of: 22, 24, 100, 2001.", error);
        Assert.Null(result);
    }

    public sealed class UInt32Class
    {
        [Name("value")]
        [ValidRange(5, 30)]
        public uint Value { get; set; }

        [Name("nullable")]
        [ValidRange(30, 2000)]
        public uint? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(22)]
        [ValidRange(0, 2_000_000)]
        public uint Default { get; set; }

        [Name("valid")]
        [ValidValues(22, 24, 110)]
        public uint Valid { get; set; }
    }

    [Fact]
    public void UInt32Test()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=14&nullable=32&valid=24"),
                out UInt32Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Equal(32U, result.Nullable);
        Assert.Equal(24U, result.Valid);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=12&nullable=1993&default=188372&valid=22"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12U, result.Value);
        Assert.Equal(188372U, result.Default);
        Assert.Equal(1993U, result.Nullable);
        Assert.Equal(22U, result.Valid);

        Assert.True(TryParseQueryString(ParseToCollection("}"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(TryParseQueryString(ParseToCollection("value=true"), out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=22.3"), out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("nullable=1001848720002704720027"), out result, out error));
        Assert.Equal("'nullable' was not of type uint32.", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(ParseToCollection("nullable2=true"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.True(TryParseQueryString(ParseToCollection("value678=true"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(TryParseQueryString(ParseToCollection("value=-22"), out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("default=-1"), out result, out error));
        Assert.Equal("'default' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("valid=23"), out result, out error));
        Assert.Equal("'valid' must be one of: 22, 24, 110.", error);
        Assert.Null(result);
    }

    public sealed class IntegerTypesClass
    {
        [Name("byte")]
        public byte Byte { get; set; }

        [Name("ushort")]
        public ushort UInt16 { get; set; }

        [Name("uint")]
        public uint UInt32 { get; set; }

        [Name("ulong")]
        public ulong UInt64 { get; set; }

        [Name("sbyte")]
        public sbyte SByte { get; set; }

        [Name("short")]
        public short Int16 { get; set; }

        [Name("int")]
        public int Int32 { get; set; }

        [Name("long")]
        public long Int64 { get; set; }
    }

    [Fact]
    public void IntegerTypes()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("byte=14&ushort=32&uint=24&ulong=38082007040"),
                out IntegerTypesClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Byte);
        Assert.Equal(32, result.UInt16);
        Assert.Equal(24U, result.UInt32);
        Assert.Equal(38082007040UL, result.UInt64);

        Assert.False(TryParseQueryString(ParseToCollection("byte=-2"), out result, out error));
        Assert.Equal("'byte' was not of type uint8.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("ushort=-2"), out result, out error));
        Assert.Equal("'ushort' was not of type uint16.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("uint=-2"), out result, out error));
        Assert.Equal("'uint' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("ulong=-2"), out result, out error));
        Assert.Equal("'ulong' was not of type uint64.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("byte=256"), out result, out error));
        Assert.Equal("'byte' was not of type uint8.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("ushort=70000"), out result, out error));
        Assert.Equal("'ushort' was not of type uint16.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("uint=4668888664668"), out result, out error));
        Assert.Equal("'uint' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(ParseToCollection("ulong=69898989484684684684384349684"), out result, out error)
        );
        Assert.Equal("'ulong' was not of type uint64.", error);
        Assert.Null(result);

        Assert.True(
            TryParseQueryString(ParseToCollection("sbyte=14&short=32&int=24&long=38082007040"), out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.SByte);
        Assert.Equal(32, result.Int16);
        Assert.Equal(24, result.Int32);
        Assert.Equal(38082007040L, result.Int64);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("sbyte=-14&short=-32&int=-24&long=-38082007040"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-14, result.SByte);
        Assert.Equal(-32, result.Int16);
        Assert.Equal(-24, result.Int32);
        Assert.Equal(-38082007040L, result.Int64);

        Assert.False(TryParseQueryString(ParseToCollection("sbyte=-200"), out result, out error));
        Assert.Equal("'sbyte' was not of type int8.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("short=-40000"), out result, out error));
        Assert.Equal("'short' was not of type int16.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("int=-2147483649"), out result, out error));
        Assert.Equal("'int' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("long=-984898116816838138138138"), out result, out error));
        Assert.Equal("'long' was not of type int64.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("sbyte=200"), out result, out error));
        Assert.Equal("'sbyte' was not of type int8.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("short=40000"), out result, out error));
        Assert.Equal("'short' was not of type int16.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("int=4668888664668"), out result, out error));
        Assert.Equal("'int' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(ParseToCollection("long=69898989484684684684384349684"), out result, out error)
        );
        Assert.Equal("'long' was not of type int64.", error);
        Assert.Null(result);
    }

    public sealed class DecimalTypesClass
    {
        [Name("float")]
        public float Float { get; set; }

        [Name("double")]
        public double Double { get; set; }

        [Name("decimal")]
        public decimal Decimal { get; set; }
    }

    [Fact]
    public void DecimalTypes()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("float=2&double=409&decimal=8200"),
                out DecimalTypesClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2.0f, result.Float);
        Assert.Equal(409.0, result.Double);
        Assert.Equal(8200, result.Decimal);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("float=2.48&double=409.46684&decimal=820055.1668863313"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2.48f, result.Float);
        Assert.Equal(409.46684, result.Double);
        Assert.Equal(820055.1668863313M, result.Decimal);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("float=-2.48&double=-409.46684&decimal=-820055.1668863313"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-2.48f, result.Float);
        Assert.Equal(-409.46684, result.Double);
        Assert.Equal(-820055.1668863313M, result.Decimal);

        Assert.True(
            TryParseQueryString(
                ParseToCollection(
                    "float=9864684668338734.168616838&double=986834384436688631.16864&decimal=79228162514264337593543950335"
                ),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(9.864685E+15F, result.Float);
        Assert.Equal(9.8683438443668864E+17, result.Double);
        Assert.Equal(79228162514264337593543950335M, result.Decimal);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("decimal=79228162514264337593543950334.499881384"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0.0f, result.Float);
        Assert.Equal(0.0, result.Double);
        Assert.Equal(79228162514264337593543950334.499881384M, result.Decimal);
    }

    public sealed class StringClass
    {
        [Name("value")]
        public string? Value { get; set; }

        [Name("valid")]
        [ValidValues("hello", "world", "invalid")]
        public string? Valid { get; set; }

        [Name("default")]
        [DefaultValue("some string")]
        public string Default { get; set; } = null!;

        [Name("limited")]
        [StringLength(2, 4)]
        public string? Limited { get; set; }

        [Name("limited2")]
        [StringLength(2, 4)]
        public string? Limited2 { get; set; }
    }

    [Fact]
    public void Strings()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=whatever&valid=invalid&default=8200&limited=he&limited2=he&notEmpty=word"),
                out StringClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("whatever", result.Value);
        Assert.Equal("invalid", result.Valid);
        Assert.Equal("8200", result.Default);
        Assert.Equal("he", result.Limited);
        Assert.Equal("he", result.Limited2);

        Assert.True(
            TryParseQueryString(ParseToCollection("value=extra&limited=hell&limited2=hell"), out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("extra", result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Equal("hell", result.Limited);
        Assert.Equal("hell", result.Limited2);

        Assert.False(TryParseQueryString(ParseToCollection("limited=1"), out result, out error));
        Assert.Equal("'limited' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("limited=12345"), out result, out error));
        Assert.Equal("'limited' must be shorter than 4 characters.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("limited2=1"), out result, out error));
        Assert.Equal("'limited2' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("limited2=12345"), out result, out error));
        Assert.Equal("'limited2' must be shorter than 4 characters.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("valid=22"), out result, out error));
        Assert.Equal("'valid' must be one of: \"hello\", \"invalid\", \"world\".", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(ParseToCollection("value=22"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("22", result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Null(result.Limited);
        Assert.Null(result.Limited2);

        Assert.True(TryParseQueryString(ParseToCollection("value=null"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("null", result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Null(result.Limited);
        Assert.Null(result.Limited2);
    }

    public sealed class DateTimeClass
    {
        [Name("value")]
        public DateTime Value { get; set; }

        [Name("value2")]
        public DateTimeOffset Value2 { get; set; }

        [Name("defaultValue")]
        [DefaultValue(typeof(DateTime), "May 25, 1988")]
        public DateTime DefaultValue { get; set; }

        [Name("defaultValue2")]
        [DefaultValue(typeof(DateTimeOffset), "May 25, 1988")]
        public DateTimeOffset DefaultValue2 { get; set; }
    }

    [Fact]
    public void DateTimeTest()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=May%203%2C%202010&value2=2021-07-08T15%3A55%3A34Z"),
                out DateTimeClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateTimeOffset.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture), result.Value2);
        Assert.Equal(DateTimeOffset.Parse("May 25, 1988", CultureInfo.InvariantCulture), result.DefaultValue);
        Assert.Equal(DateTimeOffset.Parse("May 25, 1988", CultureInfo.InvariantCulture), result.DefaultValue2);

        Assert.False(TryParseQueryString(ParseToCollection("value=January"), out result, out error));
        Assert.Equal("'value' was not of type DateTime.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=17"), out result, out error));
        Assert.Equal("'value' was not of type DateTime.", error);
        Assert.Null(result);

        Assert.True(
            TryParseQueryString(
                ParseToCollection(
                    "value=May%203%2C%202010&value2=2021-07-08T15%3A55%3A34Z&defaultValue=May%203%2C%202010&defaultValue2=May%203%2C%202010"
                ),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateTimeOffset.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture), result.Value2);
        Assert.Equal(DateTimeOffset.Parse("May 3, 2010", CultureInfo.InvariantCulture), result.DefaultValue);
        Assert.Equal(DateTimeOffset.Parse("May 3, 2010", CultureInfo.InvariantCulture), result.DefaultValue2);
    }

    public sealed class DateOnlyClass
    {
        [Name("value")]
        [DateOnlyParseSettings(Format = "dd MMM yyyy")]
        public DateOnly Value { get; set; }

        [Name("value2")]
        [DefaultValue(typeof(DateOnly), "2023-10-14")]
        public DateOnly Value2 { get; set; }
    }

    [Fact]
    public void DateOnlyTest()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=03%20May%202010&value2=2021-07-08"),
                out DateOnlyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.ParseExact("03 May 2010", "dd MMM yyyy", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("2021-07-08", CultureInfo.InvariantCulture), result.Value2);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=03%20May%202010&value2=03%20May%202010"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.ParseExact("03 May 2010", "dd MMM yyyy", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("03 May 2010", CultureInfo.InvariantCulture), result.Value2);

        Assert.False(TryParseQueryString(ParseToCollection("value=junuary"), out result, out error));
        Assert.Equal("'value' was not of type DateOnly.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=17"), out result, out error));
        Assert.Equal("'value' was not of type DateOnly.", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(new QueryCollection(), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(default, result.Value);
        Assert.Equal(DateOnly.Parse("2023-10-14", CultureInfo.InvariantCulture), result.Value2);
    }

    public sealed class TimeOnlyClass
    {
        [Name("value")]
        public TimeOnly Value { get; set; }

        [Name("value2")]
        [DefaultValue(typeof(TimeOnly), "10:02")]
        public TimeOnly Value2 { get; set; }
    }

    [Fact]
    public void TimeOnlyTest()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=12%3A22am&value2=17%3A05pm"),
                out TimeOnlyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("12:22am", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("17:05pm", CultureInfo.InvariantCulture), result.Value2);

        Assert.False(TryParseQueryString(ParseToCollection("value=12"), out result, out error));
        Assert.Equal("'value' was not of type TimeOnly.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=17"), out result, out error));
        Assert.Equal("'value' was not of type TimeOnly.", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(new QueryCollection(), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(default, result.Value);
        Assert.Equal(TimeOnly.Parse("10:02", CultureInfo.InvariantCulture), result.Value2);
    }

    public sealed class TimeSpanClass
    {
        [Name("value")]
        [DefaultValue(typeof(TimeSpan), "10:04")]
        public TimeSpan Value { get; set; }
    }

    [Fact]
    public void TimeSpanTest()
    {
        Assert.True(TryParseQueryString(ParseToCollection("value=20s"), out TimeSpanClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("20s"), result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=14%3A32"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("14:32"), result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=14d"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("14d"), result.Value);

        Assert.False(TryParseQueryString(ParseToCollection("value=May%203"), out result, out error));
        Assert.Equal("'value' was not of type TimeSpan.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=hello"), out result, out error));
        Assert.Equal("'value' was not of type TimeSpan.", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(ParseToCollection(string.Empty), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("10:04"), result.Value);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    public sealed class EnumClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestEnum), "B")]
        public TestEnum Value { get; set; }
    }

    [Fact]
    public void Enums()
    {
        Assert.True(TryParseQueryString(ParseToCollection("value=none"), out EnumClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.None, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=a"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.A, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("}"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.B, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=alt"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.B, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=superLongName"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.SuperLong, result.Value);

        Assert.False(TryParseQueryString(ParseToCollection("value=May 3"), out result, out error));
        Assert.Equal("value > 'TestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=17"), out result, out error));
        Assert.Equal("value > 'TestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=1"), out result, out error));
        Assert.Equal("value > 'TestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(
        SerializationType = EnumSerializableSettingsSerializationType.Numbers,
        AllowAnyNumber = false
    )]
    public enum TestNumberEnum
    {
        A = 0,
        B = 1,
        None = 17,
        SuperLong = 12345,
    }

    public sealed class NumberEnumClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum Value { get; set; }

        [Name("nullable")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum? Nullable { get; set; }
    }

    [Fact]
    public void NumberEnums()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection("value=0&nullable=1"), out NumberEnumClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.A, result.Value);
        Assert.Equal(TestNumberEnum.B, result.Nullable);

        Assert.True(TryParseQueryString(ParseToCollection("value=17"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.None, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection(string.Empty), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.B, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=1"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.B, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=12345"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.SuperLong, result.Value);

        Assert.False(TryParseQueryString(ParseToCollection("value=May%203"), out result, out error));
        Assert.Equal("value > 'TestNumberEnum' must be an integer.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=14"), out result, out error));
        Assert.Equal("value > 'TestNumberEnum' must be one of: 0, 1, 17, 12345.", error);
        Assert.Null(result);
    }

    public sealed class ArrayClass
    {
        [Name("ints")]
        [ArrayLength(1, 4)]
        public int[]? Ints { get; set; }

        [Name("strings")]
        [ArrayLength(1, 4)]
        [StringLength(2, 5)]
        public string[]? Strings { get; set; }

        [Name("stringsNoMin")]
        [ArrayLength(1, 4)]
        public string[]? StringNoMin { get; set; }
    }

    [Fact]
    public void ArrayTest()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("ints=2&ints=3&ints=4&strings=hello&strings=world&stringsNoMin=longString"),
                out ArrayClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { 2, 3, 4 }, result.Ints);
        Assert.Equal(new[] { "hello", "world" }, result.Strings);
        Assert.Equal(new[] { "longString" }, result.StringNoMin);

        Assert.False(
            TryParseQueryString(ParseToCollection("ints=2&ints=3&ints=4&ints=5&ints=6"), out result, out error)
        );
        Assert.Equal("'ints' must be shorter than 4 elements.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("strings"), out result, out error));
        Assert.Equal("'strings' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("strings=a"), out result, out error));
        Assert.Equal("'strings' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("strings=1234567"), out result, out error));
        Assert.Equal("'strings' must be shorter than 5 characters.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(
                ParseToCollection("strings=ab&strings=ab&strings=ab&strings=ab&strings=ab"),
                out result,
                out error
            )
        );
        Assert.Equal("'strings' must be shorter than 4 elements.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(
                ParseToCollection("strings=2&strings=3&strings=4&ints=hello&ints=world"),
                out result,
                out error
            )
        );
        Assert.Equal("'ints' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(
                ParseToCollection(
                    "stringsNoMin=string0&stringsNoMin=string1&stringsNoMin=string2&stringsNoMin=string3&stringsNoMin=string4&stringsNoMin=string5"
                ),
                out result,
                out error
            )
        );
        Assert.Equal("'stringsNoMin' must be shorter than 4 elements.", error);
        Assert.Null(result);
    }

    public sealed class ArrayWithAllTypesClass
    {
        [Name("string")]
        public string[]? Strings { get; set; }

        [Name("sbyte")]
        public sbyte[]? SBytes { get; set; }

        [Name("short")]
        public short[]? Shorts { get; set; }

        [Name("ushort")]
        public ushort[]? UShorts { get; set; }

        [Name("int")]
        public int[]? Ints { get; set; }

        [Name("uint")]
        public uint[]? UInts { get; set; }

        [Name("long")]
        public long[]? Longs { get; set; }

        [Name("ulong")]
        public ulong[]? ULongs { get; set; }

        [Name("guid")]
        public Guid[]? Guids { get; set; }
    }

    [Fact]
    public void ArrayWithAllTypes()
    {
        var queryString = new QueryCollection(
            new Dictionary<string, StringValues>
            {
                { "string", new StringValues(new[] { "hello", "world" }) },
                { "sbyte", new StringValues(new[] { "2", "3" }) },
                { "short", new StringValues(new[] { "4", "5" }) },
                { "ushort", new StringValues(new[] { "6", "7" }) },
                { "int", new StringValues(new[] { "8", "9" }) },
                { "uint", new StringValues(new[] { "10", "11" }) },
                { "long", new StringValues(new[] { "234", "28702" }) },
                { "ulong", new StringValues(new[] { "2245", "9710" }) },
                {
                    "guid",
                    new StringValues(
                        new[] { "9184400c-d7fe-422f-9d40-720c30a2b18f", "13cec5ca-7a43-4d7a-bab3-5fbc59f465eb" }
                    )
                },
            }
        );

        Assert.True(TryParseQueryString(queryString, out ArrayWithAllTypesClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equivalent(
            new ArrayWithAllTypesClass
            {
                SBytes = new[] { (sbyte)2, (sbyte)3 },
                Shorts = new[] { (short)4, (short)5 },
                UShorts = new[] { (ushort)6, (ushort)7 },
                Ints = new[] { 8, 9 },
                UInts = new[] { 10U, 11U },
                Longs = new[] { 234L, 28702L },
                ULongs = new[] { 2245UL, 9710UL },
                Strings = new[] { "hello", "world" },
                Guids = new[]
                {
                    Guid.Parse("9184400c-d7fe-422f-9d40-720c30a2b18f"),
                    Guid.Parse("13cec5ca-7a43-4d7a-bab3-5fbc59f465eb"),
                },
            },
            result
        );
    }

    public sealed class CustomIndividualReferenceObject
    {
        public string StringValue { get; private set; } = null!;

        public int IntValue { get; private set; }

        [QueryStringSerializableCustomParse]
        public static bool TryQSParse(
            string value,
            [NotNullWhen(true)] out CustomIndividualReferenceObject? result,
            [NotNullWhen(false)] out string? error
        )
        {
            var span = value.AsSpan();
            var split = span.IndexOf(',');
            if (split < 0)
            {
                error = $"Invalid {nameof(CustomIndividualReferenceObject)}.";
                result = null;
                return false;
            }

            var i = int.Parse(span.Slice(0, split), CultureInfo.InvariantCulture);
            var s = span.Slice(split + 1);

            result = new CustomIndividualReferenceObject() { IntValue = i, StringValue = s.ToString() };

            error = null;
            return true;
        }
    }

    public struct CustomIndividualValueObject
    {
        public byte[] Bytes { get; private set; }

        [QueryStringSerializableCustomParse]
        public static bool TryParseInternal(
            string value,
            out CustomIndividualValueObject result,
            [NotNullWhen(false)] out string? error
        )
        {
            try
            {
                var bytes = Base64.DecodeFromString(value, Base64Encoding.Url);
                result = new CustomIndividualValueObject() { Bytes = bytes };

                error = null;
                return true;
            }
            catch
            {
                result = default;
                error = $"Failed to parse {nameof(CustomIndividualValueObject)}, invalid base64 string.";
                return false;
            }
        }
    }

    public sealed class CustomIndividualObjectClass
    {
        [Name("reference")]
        public CustomIndividualReferenceObject? Reference { get; set; }

        [Name("value")]
        public CustomIndividualValueObject Value { get; set; }
    }

    [Fact]
    public void CustomIndividualObjects()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=AQIDBA&reference=22%2Chowdy"),
                out CustomIndividualObjectClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);
        Assert.Equal(22, result.Reference.IntValue);
        Assert.Equal("howdy", result.Reference.StringValue);

        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(TryParseQueryString(ParseToCollection("value=AQIDBA"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(TryParseQueryString(ParseToCollection("reference=44,hello there"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(44, result.Reference.IntValue);
        Assert.Equal("hello there", result.Reference.StringValue);

        Assert.Null(result.Value.Bytes);

        Assert.False(TryParseQueryString(ParseToCollection("reference=22"), out result, out error));
        Assert.Equal("Invalid CustomIndividualReferenceObject.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=assd;;"), out result, out error));
        Assert.Equal("Failed to parse CustomIndividualValueObject, invalid base64 string.", error);
        Assert.Null(result);
    }

    public sealed class CustomArrayReferenceObject
    {
        public string StringValue { get; private set; } = null!;

        public int IntValue { get; private set; }

        [QueryStringSerializableCustomParse]
        public static bool TryParseInternal(
            StringValues values,
            [NotNullWhen(true)] out CustomArrayReferenceObject? result,
            [NotNullWhen(false)] out string? error
        )
        {
            var value = values[0]!;
            var span = value.AsSpan();
            var split = span.IndexOf(',');
            if (split < 0)
            {
                error = $"Invalid {nameof(CustomArrayReferenceObject)}.";
                result = null;
                return false;
            }

            var i = int.Parse(span.Slice(0, split), CultureInfo.InvariantCulture);
            var s = span.Slice(split + 1);

            result = new CustomArrayReferenceObject() { IntValue = i, StringValue = s.ToString() };

            error = null;
            return true;
        }
    }

    public struct CustomArrayValueObject
    {
        public byte[] Bytes { get; private set; }

        [QueryStringSerializableCustomParse]
        public static bool TryParseInternal(
            StringValues values,
            out CustomArrayValueObject result,
            [NotNullWhen(false)] out string? error
        )
        {
            try
            {
                var value = values[0]!;
                var bytes = Base64.DecodeFromString(value, Base64Encoding.Url);
                result = new CustomArrayValueObject() { Bytes = bytes };

                error = null;
                return true;
            }
            catch
            {
                result = default;
                error = $"Failed to parse {nameof(CustomArrayValueObject)}, invalid base64 string.";
                return false;
            }
        }
    }

    public sealed class CustomArrayObjectClass
    {
        [Name("reference")]
        public CustomArrayReferenceObject? Reference { get; set; }

        [Name("value")]
        public CustomArrayValueObject Value { get; set; }
    }

    [Fact]
    public void CustomArrayObjects()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=AQIDBA&reference=22%2Chowdy"),
                out CustomArrayObjectClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);
        Assert.Equal(22, result.Reference.IntValue);
        Assert.Equal("howdy", result.Reference.StringValue);

        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(TryParseQueryString(ParseToCollection("value=AQIDBA"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(TryParseQueryString(ParseToCollection("reference=44,hello there"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(44, result.Reference.IntValue);
        Assert.Equal("hello there", result.Reference.StringValue);

        Assert.Null(result.Value.Bytes);

        Assert.False(TryParseQueryString(ParseToCollection("reference=22"), out result, out error));
        Assert.Equal("Invalid CustomArrayReferenceObject.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=assd;;"), out result, out error));
        Assert.Equal("Failed to parse CustomArrayValueObject, invalid base64 string.", error);
        Assert.Null(result);
    }

    public sealed class NestedCustomParseClass
    {
        [Name("class")]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed class CustomValueClass
        {
            public int Value { get; private set; }

            [QueryStringSerializableCustomParse]
            public static bool TryParseInternal(
                string stringValue,
                [NotNullWhen(true)] out CustomValueClass? value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (!int.TryParse(stringValue, out var readValue))
                {
                    error = $"Invalid token type for {nameof(CustomValueClass)}";
                    value = default;
                    return false;
                }

                value = new CustomValueClass { Value = readValue };
                error = null;
                return true;
            }
        }

        public struct CustomValueStruct
        {
            public int Value { get; private set; }

            [QueryStringSerializableCustomParse]
            public static bool TryParseInternal(
                string stringValue,
                out CustomValueStruct value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (!int.TryParse(stringValue, out var readValue))
                {
                    error = $"Invalid token type for {nameof(CustomValueStruct)}";
                    value = default;
                    return false;
                }

                value = new CustomValueStruct { Value = readValue };
                error = null;
                return true;
            }
        }
    }

    [Fact]
    public void NestedCustomParser()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("class=22&struct=44"),
                out NestedCustomParseClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Class);
        Assert.Equal(22, result.Class.Value);
        Assert.Equal(44, result.Struct.Value);

        Assert.False(TryParseQueryString(ParseToCollection("class=asdf"), out result, out error));
        Assert.Equal("Invalid token type for CustomValueClass", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("struct=asdf"), out result, out error));
        Assert.Equal("Invalid token type for CustomValueStruct", error);
        Assert.Null(result);
    }

    public sealed class GuidObject
    {
        [Name("value")]
        public Guid Value { get; set; }
    }

    [Fact]
    public void GuidTest()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=9184400c-d7fe-422f-9d40-720c30a2b18f"),
                out GuidObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(Guid.Parse("9184400c-d7fe-422f-9d40-720c30a2b18f"), result.Value);

        Assert.False(
            TryParseQueryString(ParseToCollection("value=e847da22-bb0a-11ed-afa1-asdfasdfasdf"), out result, out error)
        );
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=true"), out result, out error));
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=44.37"), out result, out error));
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);
    }

    public sealed class NullValuesObjectClass
    {
        [Name("string")]
        [ArrayLength(64)]
        public string? StringValue { get; set; }

        [Name("int")]
        public int? IntValue { get; set; }
    }

    [Fact]
    public void NullValuesObject()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection(string.Empty), out NullValuesObjectClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.StringValue);
        Assert.Null(result.IntValue);

        Assert.True(TryParseQueryString(ParseToCollection("string"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(string.Empty, result.StringValue);
        Assert.Null(result.IntValue);
    }

    public sealed class SpecialStringsObject
    {
        [Required(AllowOnlyWhiteSpace = false)]
        [Name("value")]
        public string Value { get; set; } = null!;

        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue]
        [Name("value2")]
        public string Value2 { get; set; } = null!;

        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue('a', 'b', 'c')]
        [Name("value3")]
        public string? Value3 { get; set; }
    }

    [Fact]
    public void SpecialStrings()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=world&value2=value&value3=something"),
                out SpecialStringsObject? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.False(
            TryParseQueryString(ParseToCollection("value=  &value2=value&value3=something"), out result, out error)
        );
        Assert.Equal("'value' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(ParseToCollection("value=\t&value2=value&value3=something"), out result, out error)
        );
        Assert.Equal("'value' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=world&value2=  value  &value3=something"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            TryParseQueryString(ParseToCollection("value=world&value2=\tvalue&value3=something"), out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=world&value2=\tva  lue&value3=something"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("va  lue", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.False(
            TryParseQueryString(ParseToCollection("value=world&value2=    &value3=something"), out result, out error)
        );
        Assert.Equal("'value2' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=world&value2=value&value3=aaasomethingbbb"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=world&value2=value&value3=aaabbbccvcaabc"),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("v", result.Value3);

        Assert.False(
            TryParseQueryString(
                ParseToCollection("value=world&value2=value&value3=aaabbbcccaabc"),
                out result,
                out error
            )
        );
        Assert.Equal("'value3' must contain characters that are not whitespace.", error);
        Assert.Null(result);
    }

    public sealed class RangesClass
    {
        [Name("decimal")]
        [ValidRange(1, 100)]
        public decimal Decimal { get; set; }

        [Name("int")]
        [ValidRange(4, 90)]
        public int Int { get; set; }

        [Name("float")]
        [ValidRange(12.0f, 17.0f)]
        public float Float { get; set; }

        [Name("double")]
        [ValidRange(-12.0, 17.0)]
        public double Double { get; set; }
    }

    public sealed class RangesClassNegative
    {
        [Name("decimal")]
        [ValidRange(typeof(decimal), "-201", "-12")]
        public decimal Decimal { get; set; }

        [Name("int")]
        [ValidRange(-90, -12)]
        public int Int { get; set; }

        [Name("float")]
        [ValidRange(-17.0f, -1.0f)]
        public float Float { get; set; }

        [Name("double")]
        [ValidRange(-70.0, -42.0)]
        public double Double { get; set; }
    }

    [Fact]
    public void Ranges()
    {
        Assert.True(TryParseQueryString(ParseToCollection("int=20"), out RangesClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0M, result.Decimal);
        Assert.Equal(20, result.Int);

        Assert.True(TryParseQueryString(ParseToCollection("decimal=35"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(35M, result.Decimal);
        Assert.Equal(0, result.Int);

        Assert.True(
            TryParseQueryString(ParseToCollection("decimal=1&int=4&float=12&double=-12.0"), out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(1M, result.Decimal);
        Assert.Equal(4, result.Int);
        Assert.Equal(12.0f, result.Float);
        Assert.Equal(-12.0f, result.Double);

        Assert.True(
            TryParseQueryString(ParseToCollection("decimal=100&int=90&float=17&double=17.0"), out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(100M, result.Decimal);
        Assert.Equal(90, result.Int);
        Assert.Equal(17.0f, result.Float);
        Assert.Equal(17.0f, result.Double);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("decimal=-201&int=-90&float=-17.0&double=-70"),
                out RangesClassNegative? negativeResult,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(negativeResult);
        Assert.Equal(-201M, negativeResult.Decimal);
        Assert.Equal(-90, negativeResult.Int);
        Assert.Equal(-17.0f, negativeResult.Float);
        Assert.Equal(-70.0f, negativeResult.Double);

        Assert.True(
            TryParseQueryString(
                ParseToCollection("decimal=-12&int=-12&float=-1&double=-42.0"),
                out negativeResult,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(negativeResult);
        Assert.Equal(-12M, negativeResult.Decimal);
        Assert.Equal(-12, negativeResult.Int);
        Assert.Equal(-1.0f, negativeResult.Float);
        Assert.Equal(-42.0f, negativeResult.Double);

        Assert.False(TryParseQueryString(ParseToCollection("decimal=-11.99999"), out negativeResult, out error));
        Assert.Equal("'decimal' was not in the range [-201..-12]", error);
        Assert.Null(negativeResult);

        Assert.False(TryParseQueryString(ParseToCollection("float=-0.999"), out negativeResult, out error));
        Assert.Equal("'float' was not in the range [-17..-1]", error);
        Assert.Null(negativeResult);

        Assert.False(TryParseQueryString(ParseToCollection("double=-70.00000001"), out negativeResult, out error));
        Assert.Equal("'double' was not in the range [-70..-42]", error);
        Assert.Null(negativeResult);

        Assert.False(TryParseQueryString(ParseToCollection("decimal=0"), out result, out error));
        Assert.Equal("'decimal' was not in the range [1..100]", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("decimal=101"), out result, out error));
        Assert.Equal("'decimal' was not in the range [1..100]", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("int=3"), out result, out error));
        Assert.Equal("'int' was not in the range [4..90]", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("int=91"), out result, out error));
        Assert.Equal("'int' was not in the range [4..90]", error);
        Assert.Null(result);
    }

    [SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Spec correct.")]
    public sealed class UUIDv1Class
    {
        [Required]
        [Name("value")]
        public UUIDv1 Value { get; set; }
    }

    [Fact]
    public void UUIDv1Test()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=e847da22-bb0a-11ed-afa1-0242ac120002"),
                out UUIDv1Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(UUIDv1.Parse("e847da22-bb0a-11ed-afa1-0242ac120002"), result.Value);

        Assert.False(
            TryParseQueryString(ParseToCollection("value=e847da22-bb0a-11ed-afa1-asdfasdfasdf"), out result, out error)
        );
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=true"), out result, out error));
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("value=44.37"), out result, out error));
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S3604:Member initializer values should not be redundant",
        Justification = "Testing exactly this."
    )]
    [SuppressMessage(
        "Major Code Smell",
        "S1144:Unused private types or members should be removed",
        Justification = "Testing exactly this."
    )]
    public sealed class NoDefaultConstructorClass
    {
        [Name("value")]
        [DefaultValue(12)]
        public int Value { get; set; } = 17;

        [Name("default")]
        public int Default { get; set; }

        [Name("string")]
        public string? StringValue { get; set; } = "hello world";

        public NoDefaultConstructorClass(int value, string s)
        {
            this.Value = value;
            this.StringValue = s;
        }
    }

    [Fact]
    public void NoDefaultConstructor()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=2&string=rock"),
                out NoDefaultConstructorClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Equal("rock", result.StringValue);

        Assert.True(TryParseQueryString(ParseToCollection("string=rock"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Equal("rock", result.StringValue);

        Assert.True(TryParseQueryString(ParseToCollection(string.Empty), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Null(result.StringValue);
    }

    public struct ValueTypeStruct
    {
        [Name("value")]
        [DefaultValue(12)]
        public int Value { get; set; }

        [Required]
        [Name("string")]
        public string? StringValue { get; set; }
    }

    [Fact]
    public void ValueType()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection("value=2&string=rock"), out ValueTypeStruct result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(2, result.Value);
        Assert.Equal("rock", result.StringValue);

        Assert.True(TryParseQueryString(ParseToCollection("string=rock"), out result, out error));
        Assert.Null(error);
        Assert.Equal(12, result.Value);
        Assert.Equal("rock", result.StringValue);

        Assert.False(TryParseQueryString(ParseToCollection("asdf"), out result, out error));
        Assert.Equal("Required query parameter 'string' was missing.", error);
        Assert.Equal(0, result.Value);
        Assert.Null(result.StringValue);
    }

    [Fact]
    public void NullableValueType()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection("value=2&string=rock"), out ValueTypeStruct? result, out var error)
        );
        Assert.Null(error);
        _ = Assert.NotNull(result);
        Assert.Equal(2, result.Value.Value);
        Assert.Equal("rock", result.Value.StringValue);

        Assert.True(TryParseQueryString(ParseToCollection("string=rock"), out result, out error));
        Assert.Null(error);
        _ = Assert.NotNull(result);
        Assert.Equal(12, result.Value.Value);
        Assert.Equal("rock", result.Value.StringValue);

        Assert.False(TryParseQueryString(ParseToCollection("asdf"), out result, out error));
        Assert.Equal("Required query parameter 'string' was missing.", error);
        Assert.Null(result);
    }

    public sealed class BytesClass
    {
        [Name("value")]
        public byte[]? Value { get; set; }

        [Name("values")]
        public byte[][]? Values { get; set; }
    }

    [Fact]
    public void Bytes()
    {
        Assert.True(TryParseQueryString(ParseToCollection("value=aGVsbG8"), out BytesClass? result, out var error));
        Assert.Null(error);
        Assert.Equal("hello"u8.ToArray(), result.Value);
        Assert.Null(result.Values);

        Assert.True(TryParseQueryString(ParseToCollection("values=aGVsbG8&values=d29ybGQ"), out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(new[] { "hello"u8.ToArray(), "world"u8.ToArray() }, result.Values);

        Assert.True(
            TryParseQueryString(
                ParseToCollection(
                    "value=4rdHFh-HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ_&values=4rdHFh-HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ_"
                ),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(
            Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F"),
            result.Value
        );
        Assert.Equal(
            new[] { Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F") },
            result.Values
        );

        Assert.False(TryParseQueryString(ParseToCollection("value=hello**person"), out result, out error));
        Assert.Equal("'value' was not a url encoded base64 string.", error);
        Assert.Null(result);
    }

    public sealed class DefaultNullableValuesClass
    {
        [Name("int")]
        [DefaultValue(22)]
        public int? IntValue { get; set; }
    }

    [Fact]
    public void DefaultNullableValue()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection(string.Empty), out DefaultNullableValuesClass? result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(22, result.IntValue);
    }

    public sealed class ListClass
    {
        [Name("value")]
        public required List<int> List { get; set; }
    }

    [Fact]
    public void List()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection("value=22&value=23&value=22"), out ListClass? result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(new[] { 22, 23, 22 }, result.List);
    }

    public sealed class HashSetClass
    {
        [Name("value")]
        public required HashSet<int> Set { get; set; }
    }

    [Fact]
    public void HashSet()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=22&value=23&value=22"),
                out HashSetClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new HashSet<int> { 22, 23 }, result.Set);
    }

    public sealed class FrozenSetClass
    {
        [Name("value")]
        public required FrozenSet<int> Set { get; set; }
    }

    [Fact]
    public void FrozenSet()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=22&value=23&value=22"),
                out FrozenSetClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new HashSet<int> { 22, 23 }, result.Set);
    }

    public sealed class RequiredPropertyInitClass
    {
        [Name("username")]
        public required string Username { get; init; }

        [Name("password")]
        public required string Password { get; init; }

        [Name("remember")]
        public bool RememberMe { get; init; }

        [Name("coupon")]
        public string? Coupon { get; set; }
    }

    [Fact]
    public void RequiredPropertyInit()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("username=tron&password=hunter2&coupon=1234"),
                out RequiredPropertyInitClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("tron", result.Username);
        Assert.Equal("hunter2", result.Password);
        Assert.False(result.RememberMe);
        Assert.Equal("1234", result.Coupon);

        Assert.False(TryParseQueryString(ParseToCollection("username=tron&coupon=1234"), out result, out error));
        Assert.Equal("Required query parameter 'password' was missing.", error);
        Assert.Null(result);

        Assert.False(TryParseQueryString(ParseToCollection("password=hunter2&coupon=1234"), out result, out error));
        Assert.Equal("Required query parameter 'username' was missing.", error);
        Assert.Null(result);

        Assert.True(TryParseQueryString(ParseToCollection("username=tron&password=hunter2"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("tron", result.Username);
        Assert.Equal("hunter2", result.Password);
        Assert.False(result.RememberMe);
        Assert.Null(result.Coupon);
    }

    public sealed class DefaultsClass
    {
        [Name("s8")]
        [DefaultValue(sbyte.MinValue)]
        public sbyte S8 { get; set; }

        [Name("s16")]
        [DefaultValue(short.MinValue)]
        public short S16 { get; set; }

        [Name("s32")]
        [DefaultValue(int.MinValue)]
        public int S32 { get; set; }

        [Name("s64")]
        [DefaultValue(long.MinValue)]
        public long S64 { get; set; }

        [Name("u8")]
        [DefaultValue(byte.MaxValue)]
        public byte U8 { get; set; }

        [Name("u16")]
        [DefaultValue(ushort.MaxValue)]
        public ushort U16 { get; set; }

        [Name("u32")]
        [DefaultValue(uint.MaxValue)]
        public uint U32 { get; set; }

        [Name("u64")]
        [DefaultValue(ulong.MaxValue)]
        public ulong U64 { get; set; }

        [Name("f32")]
        [DefaultValue(float.MaxValue)]
        public float F32 { get; set; }

        [Name("f64")]
        [DefaultValue(double.MaxValue)]
        public double F64 { get; set; }

        [Name("decimal")]
        [DefaultValue(typeof(decimal), "79228162514264337593543950335")]
        public decimal Decimal { get; set; }

        [Name("timespan")]
        [DefaultValue(typeof(TimeSpan), "20:30")]
        public TimeSpan TimeSpan { get; set; }

        [Name("datetime")]
        [DefaultValue(typeof(DateTime), "1970-01-01T00:00:00Z")]
        public DateTime DateTime { get; set; }

        [Name("datetimeoffset")]
        [DefaultValue(typeof(DateTimeOffset), "1970-01-01T00:00:00Z")]
        public DateTimeOffset DateTimeOffset { get; set; }
    }

    [Fact]
    public void Defaults()
    {
        Assert.True(TryParseQueryString(ParseToCollection(string.Empty), out DefaultsClass? result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);

        Assert.Equal(sbyte.MinValue, result.S8);
        Assert.Equal(short.MinValue, result.S16);
        Assert.Equal(int.MinValue, result.S32);
        Assert.Equal(long.MinValue, result.S64);

        Assert.Equal(byte.MaxValue, result.U8);
        Assert.Equal(ushort.MaxValue, result.U16);
        Assert.Equal(uint.MaxValue, result.U32);
        Assert.Equal(ulong.MaxValue, result.U64);

        Assert.Equal(float.MaxValue, result.F32);
        Assert.Equal(double.MaxValue, result.F64);
        Assert.Equal(decimal.MaxValue, result.Decimal);

        Assert.Equal(TimeSpan.Parse("20:30", CultureInfo.InvariantCulture), result.TimeSpan);
        Assert.Equal(DateTime.UnixEpoch.ToLocalTime(), result.DateTime);
        Assert.Equal(DateTimeOffset.UnixEpoch, result.DateTimeOffset);

        Assert.True(
            TryParseQueryString(
                ParseToCollection(
                    "s8=-15&s16=-15&s32=-15&s64=-15&u8=15&u16=15&u32=15&u64=15&f32=15.2&f64=15.2&decimal=15.2&timespan=30&datetime=1971-01-01T00:00:00Z&datetimeoffset=1971-01-01T00:00:00Z"
                ),
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);

        Assert.Equal(-15, result.S8);
        Assert.Equal(-15, result.S16);
        Assert.Equal(-15, result.S32);
        Assert.Equal(-15, result.S64);

        Assert.Equal(15U, result.U8);
        Assert.Equal(15U, result.U16);
        Assert.Equal(15U, result.U32);
        Assert.Equal(15U, result.U64);

        Assert.Equal(15.2f, result.F32);
        Assert.Equal(15.2, result.F64);
        Assert.Equal(15.2m, result.Decimal);

        Assert.Equal(TimeSpanParser.Parse("30"), result.TimeSpan);
        Assert.Equal(DateTime.Parse("1971-01-01T00:00:00Z", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTimeOffset.Parse("1971-01-01T00:00:00Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
    }

    public sealed class EmailAddressClass
    {
        [Name("value")]
        [EmailAddress]
        public required string Value { get; set; }
    }

    [Fact]
    public void EmailAddress()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=email%40example.com"),
                out EmailAddressClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("email@example.com", result.Value);

        Assert.False(TryParseQueryString(ParseToCollection("value=notarealemail"), out result, out error));
        Assert.Equal("'value' is not a valid email address.", error);
        Assert.Null(result);
    }

    public sealed class TrimClass
    {
        [Name("value")]
        [TrimValue]
        [StringLength(8)]
        public required string Value { get; set; }

        [Name("long")]
        [TrimValue(OnParse = false)]
        [StringLength(8)]
        public required string? Long { get; set; }
    }

    [Fact]
    public void Trim()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=%20%20%20%20%20hello%20%20%20%20%20&long=%20world%20"),
                out TrimClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal(" world ", result.Long);

        Assert.True(TryParseQueryString(ParseToCollection("value=01234567&long=abcd"), out result, out error));
        Assert.Null(error);
        Assert.Equal("01234567", result.Value);
        Assert.Equal("abcd", result.Long);

        Assert.False(
            TryParseQueryString(
                ParseToCollection("value=%20%20%20%20%200123456%20%20%20%20%207&long=short"),
                out result,
                out error
            )
        );
        Assert.Equal("'value' must be shorter than 8 characters.", error);
        Assert.Null(result);

        Assert.False(
            TryParseQueryString(
                ParseToCollection("value=hello&%20%20%20%20%20&long=whitespace%20%20%20%20%20"),
                out result,
                out error
            )
        );
        Assert.Equal("'long' must be shorter than 8 characters.", error);
        Assert.Null(result);
    }

    public sealed class NullOnEmptyClass
    {
        [Name("value")]
        [NullOnEmpty(OnSerialize = false)]
        [TrimValue]
        public string? Value { get; set; }

        [Name("emptyOnly")]
        [NullOnEmpty]
        public string? EmptyOnly { get; set; }
    }

    [Fact]
    public void NullOnEmptyTest()
    {
        Assert.True(
            TryParseQueryString(
                ParseToCollection("value=%20%20hello%20&emptyOnly=%20%20world%20"),
                out NullOnEmptyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal("  world ", result.EmptyOnly);

        Assert.True(TryParseQueryString(ParseToCollection("value=%20%20&emptyOnly=%20"), out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(" ", result.EmptyOnly);

        Assert.True(TryParseQueryString(ParseToCollection("value=&emptyOnly="), out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Null(result.EmptyOnly);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(InvalidValue = Unknown)]
    public enum InvalidValueEnum
    {
        [Name("a")]
        A,

        [Name("unknown")]
        Unknown,
    }

    public sealed class InvalidValueEnumClass
    {
        [Name("value")]
        public InvalidValueEnum Value { get; set; }
    }

    [Fact]
    public void TestInvalidValueEnum()
    {
        Assert.True(
            TryParseQueryString(ParseToCollection("value=a"), out InvalidValueEnumClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.A, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=unknown"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.Unknown, result.Value);

        Assert.True(TryParseQueryString(ParseToCollection("value=asdf"), out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.Unknown, result.Value);
    }

    private static QueryCollection ParseToCollection(string queryString) => new(QueryHelpers.ParseQuery(queryString));
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
