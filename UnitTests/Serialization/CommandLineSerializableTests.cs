// <copyright file="CommandLineSerializableTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Frozen;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests.Serialization;

/// <summary>
/// Tests for <see cref="CommandLineSerializableAttribute"/>.
/// </summary>
[CommandLineSerializable(typeof(BoolClass))]
[CommandLineSerializable(typeof(Int32Class))]
[CommandLineSerializable(typeof(UInt32Class))]
[CommandLineSerializable(typeof(Int64Class))]
[CommandLineSerializable(typeof(SingleClass))]
[CommandLineSerializable(typeof(DoubleClass))]
[CommandLineSerializable(typeof(DecimalClass))]
[CommandLineSerializable(typeof(StringClass))]
[CommandLineSerializable(typeof(TimeSpanClass))]
[CommandLineSerializable(typeof(DateTimeClass))]
[CommandLineSerializable(typeof(DateOnlyClass))]
[CommandLineSerializable(typeof(TimeOnlyClass))]
[CommandLineSerializable(typeof(DateTimeOffsetClass))]
[CommandLineSerializable(typeof(EnumClass))]
[CommandLineSerializable(typeof(NumberEnumClass))]
[CommandLineSerializable(typeof(RequiredClass))]
[CommandLineSerializable(typeof(SpecialStringsClass))]
[CommandLineSerializable(typeof(NullOnEmptyClass))]
[CommandLineSerializable(typeof(ArraysClass))]
[CommandLineSerializable(typeof(ListClass))]
[CommandLineSerializable(typeof(HashSetClass))]
[CommandLineSerializable(typeof(FrozenSetClass))]
[CommandLineSerializable(typeof(RangesClass))]
[CommandLineSerializable(typeof(ValidValuesClass))]
[CommandLineSerializable(typeof(PositionalArgumentClass))]
[CommandLineSerializable(typeof(RequiredPositionalArgumentClass))]
[SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Tests describe what things are.")]
public partial class CommandLineSerializableTests
{
    public sealed class BoolClass
    {
        [Name("-value")]
        public bool Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(true)]
        public bool Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(null)]
        public bool? Nullable { get; set; }
    }

    [Fact]
    public void Bool()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "1", "-empty", "-nullable", "false" },
                out BoolClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.True(result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.False(result.Nullable.Value);

        Assert.True(
            TryParseCommandLine(new[] { "-value", "false", "-empty", "false", "-nullable" }, out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.False(result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type bool.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class Int32Class
    {
        [Name("-value")]
        [DefaultValue(17)]
        public int Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(37)]
        public int Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(5)]
        public int? Nullable { get; set; }
    }

    [Fact]
    public void Int32Test()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "1", "-empty", "-nullable", "12" },
                out Int32Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(1, result.Value);
        Assert.Equal(37, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(12, result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "4", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(17, result.Value);
        Assert.Equal(4, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(5, result.Nullable.Value);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type int32.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class UInt32Class
    {
        [Name("-value")]
        [DefaultValue(17)]
        public uint Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(37)]
        public uint Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(5)]
        public uint? Nullable { get; set; }
    }

    [Fact]
    public void UInt32Test()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "1", "-empty", "-nullable", "12" },
                out UInt32Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(1u, result.Value);
        Assert.Equal(37u, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(12u, result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "4", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(17u, result.Value);
        Assert.Equal(4u, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(5u, result.Nullable.Value);

        Assert.False(TryParseCommandLine(new[] { "-value", "-12" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type uint32.", error);

        Assert.False(TryParseCommandLine(new[] { "-value", "225998982989972" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type uint32.", error);
    }

    public sealed class Int64Class
    {
        [Name("-value")]
        [DefaultValue(17)]
        public long Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(37)]
        public long Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(5)]
        public long? Nullable { get; set; }
    }

    [Fact]
    public void Int64Test()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "-1", "-empty", "-nullable", "-12" },
                out Int64Class? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-1, result.Value);
        Assert.Equal(37, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(-12, result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "225998982989972", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(17, result.Value);
        Assert.Equal(225998982989972L, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(5, result.Nullable.Value);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type int64.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class SingleClass
    {
        [Name("-value")]
        [DefaultValue(17.2F)]
        public float Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(37.4F)]
        public float Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(5.7F)]
        public float? Nullable { get; set; }
    }

    [Fact]
    public void F32Test()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "-1.7", "-empty", "-nullable", "12.37" },
                out SingleClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-1.7, result.Value, 4);
        Assert.Equal(37.4, result.Empty, 4);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(12.37, result.Nullable.Value, 4);

        Assert.True(TryParseCommandLine(new[] { "-empty", "-4", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(17.2, result.Value, 4);
        Assert.Equal(-4.0, result.Empty, 4);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(5.7, result.Nullable.Value, 4);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type single.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class DoubleClass
    {
        [Name("-value")]
        [DefaultValue(17.2)]
        public double Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(37.4)]
        public double Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(5.7)]
        public double? Nullable { get; set; }
    }

    [Fact]
    public void F64Test()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "-1.7", "-empty", "-nullable", "12.37" },
                out DoubleClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-1.7, result.Value, 4);
        Assert.Equal(37.4, result.Empty, 4);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(12.37, result.Nullable.Value, 4);

        Assert.True(TryParseCommandLine(new[] { "-empty", "-4", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(17.2, result.Value, 4);
        Assert.Equal(-4, result.Empty, 4);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(5.7, result.Nullable.Value, 4);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type double.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class DecimalClass
    {
        [Name("-value")]
        [DefaultValue(typeof(decimal), "17.2")]
        public decimal Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(typeof(decimal), "37.4")]
        public decimal Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(typeof(decimal), "5.7")]
        public decimal? Nullable { get; set; }
    }

    [Fact]
    public void DecimalTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "-1.7", "-empty", "-nullable", "12.37" },
                out DecimalClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-1.7m, result.Value);
        Assert.Equal(37.4m, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(12.37m, result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "-4", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(17.2m, result.Value);
        Assert.Equal(-4m, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(5.7m, result.Nullable.Value);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type decimal.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class StringClass
    {
        [Name("-value")]
        [DefaultValue("hello")]
        public string? Value { get; set; }

        [Name("-empty")]
        public string? Empty { get; set; }

        [Name("-nullable")]
        public string? Nullable { get; set; }
    }

    [Fact]
    public void StringTest()
    {
        Assert.False(
            TryParseCommandLine(
                new[] { "-value", "-1.7", "-empty", "-nullable", "12.37" },
                out StringClass? result,
                out var error
            )
        );
        Assert.Null(result);
        Assert.Equal("Unknown argument given: 12.37", error);

        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "-1.7", "-empty", "something", "-nullable", "12.37" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal("-1.7", result.Value);
        Assert.Equal("something", result.Empty);
        Assert.Equal("12.37", result.Nullable);

        Assert.True(TryParseCommandLine(new[] { "-empty", "-4", "-nullable", "twenty" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello", result.Value);
        Assert.Equal("-4", result.Empty);
        Assert.Equal("twenty", result.Nullable);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type string.", error);
    }

    public sealed class TimeSpanClass
    {
        [Name("-value")]
        [DefaultValue(typeof(TimeSpan), "20:10")]
        public TimeSpan Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(typeof(TimeSpan), "37.4")]
        public TimeSpan Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(typeof(TimeSpan), "12d")]
        public TimeSpan? Nullable { get; set; }
    }

    [Fact]
    public void TimeSpanTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "-1.7", "-empty", "-nullable", "14d" },
                out TimeSpanClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpan.FromSeconds(-1.7), result.Value);
        Assert.Equal(TimeSpan.FromSeconds(37.4), result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(TimeSpan.FromDays(14), result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "-4", "-nullable" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new TimeSpan(20, 10, 0), result.Value);
        Assert.Equal(TimeSpan.FromSeconds(-4), result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(TimeSpan.FromDays(12), result.Nullable.Value);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type TimeSpan.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class DateTimeClass
    {
        [Name("-value")]
        [DefaultValue(typeof(DateTime), "May 23, 2020")]
        public DateTime Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(typeof(DateTime), "2021-06-25T14:26:59Z")]
        public DateTime Empty { get; set; }

        [Name("-nullable")]
        public DateTime? Nullable { get; set; }
    }

    [Fact]
    public void DateTimeTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "May 3, 2019", "-empty", "-nullable", "July 15, 1942" },
                out DateTimeClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("May 3, 2019", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateTime.Parse("2021-06-25T14:26:59Z", CultureInfo.InvariantCulture), result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(DateTime.Parse("July 15, 1942", CultureInfo.InvariantCulture), result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "May 3, 1988" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("May 23, 2020", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateTime.Parse("May 3, 1988", CultureInfo.InvariantCulture), result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type DateTime.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class DateOnlyClass
    {
        [Name("-value")]
        [DefaultValue(typeof(DateOnly), "May 23, 2020")]
        public DateOnly Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(typeof(DateOnly), "2021-06-25")]
        public DateOnly Empty { get; set; }

        [Name("-nullable")]
        public DateOnly? Nullable { get; set; }
    }

    [Fact]
    public void DateOnlyTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "May 3, 2019", "-empty", "-nullable", "July 15, 1942" },
                out DateOnlyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.Parse("May 3, 2019", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("2021-06-25", CultureInfo.InvariantCulture), result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(DateOnly.Parse("July 15, 1942", CultureInfo.InvariantCulture), result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "May 3, 1988" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.Parse("May 23, 2020", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("May 3, 1988", CultureInfo.InvariantCulture), result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type DateOnly.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class TimeOnlyClass
    {
        [Name("-value")]
        [DefaultValue(typeof(TimeOnly), "12:27")]
        public TimeOnly Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(typeof(TimeOnly), "15:03")]
        public TimeOnly Empty { get; set; }

        [Name("-nullable")]
        public TimeOnly? Nullable { get; set; }
    }

    [Fact]
    public void TimeOnlyTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "4:00 PM", "-empty", "-nullable", "7:23 AM" },
                out TimeOnlyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("4:00 PM", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("15:03", CultureInfo.InvariantCulture), result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(TimeOnly.Parse("7:23 AM", CultureInfo.InvariantCulture), result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "19:54" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("12:27", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("19:54", CultureInfo.InvariantCulture), result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type TimeOnly.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class DateTimeOffsetClass
    {
        [Name("-value")]
        [DefaultValue(typeof(DateTimeOffset), "May 23, 2020")]
        public DateTimeOffset Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(typeof(DateTimeOffset), "2021-06-25T14:26:59Z")]
        public DateTimeOffset Empty { get; set; }

        [Name("-nullable")]
        public DateTimeOffset? Nullable { get; set; }
    }

    [Fact]
    public void DateTimeOffsetTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "May 3, 2019", "-empty", "-nullable", "July 15, 1942" },
                out DateTimeOffsetClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTimeOffset.Parse("May 3, 2019", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateTimeOffset.Parse("2021-06-25T14:26:59Z", CultureInfo.InvariantCulture), result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(DateTimeOffset.Parse("July 15, 1942", CultureInfo.InvariantCulture), result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "May 3, 1988" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTimeOffset.Parse("May 23, 2020", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateTimeOffset.Parse("May 3, 1988", CultureInfo.InvariantCulture), result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value' was not of type DateTimeOffset.", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);
    }

    public sealed class EnumClass
    {
        public enum EnumValues
        {
            [Name("pig", "horse")]
            Animal,

            [Name("lucy", "joe")]
            Person,

            [Name("none")]
            None,
        }

        [Name("-value")]
        [DefaultValue(EnumValues.Animal)]
        public EnumValues Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(EnumValues.Person)]
        public EnumValues Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(EnumValues.None)]
        public EnumValues? Nullable { get; set; }
    }

    [Fact]
    public void Enums()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "joe", "-empty", "-nullable", "pig" },
                out EnumClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(EnumClass.EnumValues.Person, result.Value);
        Assert.Equal(EnumClass.EnumValues.Person, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(EnumClass.EnumValues.Animal, result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "horse" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(EnumClass.EnumValues.Animal, result.Value);
        Assert.Equal(EnumClass.EnumValues.Animal, result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.True(TryParseCommandLine(new[] { "-empty", "pig", "-value", "none" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(EnumClass.EnumValues.None, result.Value);
        Assert.Equal(EnumClass.EnumValues.Animal, result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("-value Missing argument", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);

        Assert.False(TryParseCommandLine(new[] { "-value", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("-value 'EnumValues' must be one of: horse, joe, lucy, none, pig.", error);
    }

    public sealed class NumberEnumClass
    {
        [EnumSerializableSettings(SerializationType = EnumSerializableSettingsSerializationType.Numbers)]
        public enum EnumValues
        {
            None = 0,
            Animal = 1,
            Person = 2,
        }

        [Name("-value")]
        [DefaultValue(EnumValues.Animal)]
        public EnumValues Value { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(EnumValues.Person)]
        public EnumValues Empty { get; set; }

        [Name("-nullable")]
        [EmptyArgumentValue(EnumValues.None)]
        public EnumValues? Nullable { get; set; }
    }

    [Fact]
    public void NumberEnums()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "2", "-empty", "-nullable", "1" },
                out NumberEnumClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(NumberEnumClass.EnumValues.Person, result.Value);
        Assert.Equal(NumberEnumClass.EnumValues.Person, result.Empty);
        Assert.True(result.Nullable.HasValue);
        Assert.Equal(NumberEnumClass.EnumValues.Animal, result.Nullable.Value);

        Assert.True(TryParseCommandLine(new[] { "-empty", "1" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(NumberEnumClass.EnumValues.Animal, result.Value);
        Assert.Equal(NumberEnumClass.EnumValues.Animal, result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.True(TryParseCommandLine(new[] { "-empty", "1", "-value", "0" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(NumberEnumClass.EnumValues.None, result.Value);
        Assert.Equal(NumberEnumClass.EnumValues.Animal, result.Empty);
        Assert.False(result.Nullable.HasValue);

        Assert.False(TryParseCommandLine(new[] { "-value", "73" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("-value 'EnumValues' must be one of: 0, 1, 2.", error);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("-value Missing argument", error);

        Assert.False(TryParseCommandLine(new[] { "-empty", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: hello", error);

        Assert.False(TryParseCommandLine(new[] { "-value", "hello" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("-value 'EnumValues' must be an integer.", error);
    }

    public sealed class RequiredClass
    {
        [Name("-value1")]
        [Required]
        public string Value1 { get; set; } = null!;

        [Name("-value2")]
        [Required]
        public string Value2 { get; set; } = null!;

        [Name("-value3")]
        [Required(AllowOnlyWhiteSpace = false)]
        public string Value3 { get; set; } = null!;
    }

    [Fact]
    public void Required()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value1", "joe", "-value2", "hello", "-value3", "arch" },
                out RequiredClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("joe", result.Value1);
        Assert.Equal("hello", result.Value2);
        Assert.Equal("arch", result.Value3);

        Assert.False(TryParseCommandLine(new[] { "-value" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("Unknown argument given: -value", error);

        Assert.False(TryParseCommandLine(new[] { "-value1" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-value1' was not of type string.", error);

        Assert.False(TryParseCommandLine(new[] { "-value1", "yo" }, out result, out error));
        Assert.Null(result);
        Assert.Equal(
            "A required property was missing on UnitTests.Serialization.CommandLineSerializableTests.RequiredClass. Missing: -value2, -value3.",
            error
        );

        Assert.False(
            TryParseCommandLine(new[] { "-value1", "joe", "-value2", "hello", "-value3", "  " }, out result, out error)
        );
        Assert.Null(result);
        Assert.Equal("'-value3' must contain characters that are not whitespace.", error);

        Assert.False(
            TryParseCommandLine(new[] { "-value1", "joe", "-value2", "hello", "-value3", "\t" }, out result, out error)
        );
        Assert.Null(result);
        Assert.Equal("'-value3' must contain characters that are not whitespace.", error);
    }

    public sealed class SpecialStringsClass
    {
        [Name("-value1")]
        public required string Value1 { get; set; }

        [Name("-value2")]
        [TrimValue]
        public required string Value2 { get; set; }

        [Name("-value3")]
        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue(new[] { 'a', 'b' })]
        public string Value3 { get; set; } = null!;
    }

    [Fact]
    public void SpecialStrings()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value1", "joe", "-value2", "hello", "-value3", "arch" },
                out SpecialStringsClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("joe", result.Value1);
        Assert.Equal("hello", result.Value2);
        Assert.Equal("rch", result.Value3);

        Assert.True(
            TryParseCommandLine(
                new[] { "-value1", "  joe ", "-value2", "     hello", "-value3", "arch" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("  joe ", result.Value1);
        Assert.Equal("hello", result.Value2);
        Assert.Equal("rch", result.Value3);

        Assert.True(
            TryParseCommandLine(
                new[] { "-value1", "  joe ", "-value2", "     hello", "-value3", "aaaabbbvvbbb" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("  joe ", result.Value1);
        Assert.Equal("hello", result.Value2);
        Assert.Equal("vv", result.Value3);

        Assert.False(
            TryParseCommandLine(
                new[] { "-value1", "joe", "-value2", "hello", "-value3", "aaaabbb" },
                out result,
                out error
            )
        );
        Assert.Equal("'-value3' must contain characters that are not whitespace.", error);
        Assert.Null(result);
    }

    public sealed class NullOnEmptyClass
    {
        [Name("-value")]
        [NullOnEmpty(OnSerialize = false)]
        [TrimValue]
        public string? Value { get; set; }

        [Name("-emptyOnly")]
        [NullOnEmpty]
        public string? EmptyOnly { get; set; }
    }

    [Fact]
    public void NullOnEmptyTest()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-value", "  hello ", "-emptyOnly", "  world " },
                out NullOnEmptyClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal("  world ", result.EmptyOnly);

        Assert.True(TryParseCommandLine(new[] { "-value", "  ", "-emptyOnly", " " }, out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(" ", result.EmptyOnly);

        Assert.True(TryParseCommandLine(new[] { "-value", "  ", "-emptyOnly", string.Empty }, out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Null(result.EmptyOnly);
    }

    public sealed class ArraysClass
    {
        [Name("-string")]
        [EmptyArgumentValue("2")]
        public string[]? Strings { get; set; }

        [Name("-int")]
        public int[]? Ints { get; set; }
    }

    [Fact]
    public void Arrays()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-int", "14", "-int", "-12", "-string", "hello" },
                out ArraysClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "hello" }, result.Strings);
        Assert.Equal(new[] { 14, -12 }, result.Ints);

        Assert.False(TryParseCommandLine(new[] { "-int", "yo" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' was not of type int32.", error);

        Assert.True(TryParseCommandLine(new[] { "-int", "4", "-string" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "2" }, result.Strings);
        Assert.Equal(new[] { 4 }, result.Ints);
    }

    public sealed class ListClass
    {
        [Name("-string")]
        [EmptyArgumentValue("2")]
        public List<string>? Strings { get; set; }

        [Name("-int")]
        public List<int>? Ints { get; set; }
    }

    [Fact]
    public void Lists()
    {
        Assert.True(
            TryParseCommandLine(
                new[]
                {
                    "-int",
                    "14",
                    "-int",
                    "-12",
                    "-int",
                    "-12",
                    "-string",
                    "hello",
                    "-string",
                    "world",
                    "-string",
                    "hello",
                },
                out ListClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "hello", "world", "hello" }, result.Strings);
        Assert.Equal(new[] { 14, -12, -12 }, result.Ints);

        Assert.False(TryParseCommandLine(new[] { "-int", "yo" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' was not of type int32.", error);

        Assert.True(TryParseCommandLine(new[] { "-int", "4", "-string" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "2" }, result.Strings);
        Assert.Equal(new[] { 4 }, result.Ints);
    }

    public sealed class HashSetClass
    {
        [Name("-string")]
        [EmptyArgumentValue("2")]
        public HashSet<string>? Strings { get; set; }

        [Name("-int")]
        public HashSet<int>? Ints { get; set; }
    }

    [Fact]
    public void HashSet()
    {
        Assert.True(
            TryParseCommandLine(
                new[]
                {
                    "-int",
                    "14",
                    "-int",
                    "-12",
                    "-int",
                    "-12",
                    "-string",
                    "hello",
                    "-string",
                    "world",
                    "-string",
                    "hello",
                },
                out HashSetClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new HashSet<string> { "hello", "world" }, result.Strings);
        Assert.Equal(new HashSet<int> { 14, -12 }, result.Ints);

        Assert.False(TryParseCommandLine(new[] { "-int", "yo" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' was not of type int32.", error);

        Assert.True(TryParseCommandLine(new[] { "-int", "4", "-string" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "2" }, result.Strings);
        Assert.Equal(new[] { 4 }, result.Ints);
    }

    public sealed class FrozenSetClass
    {
        [Name("-string")]
        [EmptyArgumentValue("2")]
        public FrozenSet<string>? Strings { get; set; }

        [Name("-int")]
        public FrozenSet<int>? Ints { get; set; }
    }

    [Fact]
    public void FrozenSet()
    {
        Assert.True(
            TryParseCommandLine(
                new[]
                {
                    "-int",
                    "14",
                    "-int",
                    "-12",
                    "-int",
                    "-12",
                    "-string",
                    "hello",
                    "-string",
                    "world",
                    "-string",
                    "hello",
                },
                out FrozenSetClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new HashSet<string> { "hello", "world" }.ToFrozenSet(), result.Strings);
        Assert.Equal(new HashSet<int> { 14, -12 }.ToFrozenSet(), result.Ints);

        Assert.False(TryParseCommandLine(new[] { "-int", "yo" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' was not of type int32.", error);

        Assert.True(TryParseCommandLine(new[] { "-int", "4", "-string" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "2" }, result.Strings);
        Assert.Equal(new[] { 4 }, result.Ints);
    }

    public sealed class RangesClass
    {
        [Name("-int")]
        [ValidRange(-12, 14)]
        public int Int32 { get; set; }

        [Name("-long")]
        [ValidRange(3802884792, 28977479799192)]
        public long Int64 { get; set; }

        [Name("-uint")]
        [ValidRange(0u, 14u)]
        public int UInt32 { get; set; }

        [Name("-ulong")]
        [ValidRange(12, 14)]
        public ulong UInt64 { get; set; }

        [Name("-decimal")]
        [ValidRange(typeof(decimal), "-12.7", "14.2")]
        public decimal Decimal { get; set; }

        [Name("-float")]
        [ValidRange(-12.2, 14.4)]
        public float Single { get; set; }

        [Name("-double")]
        [ValidRange(12.2, 14.4)]
        public double Double { get; set; }
    }

    [Fact]
    public void Ranges()
    {
        Assert.True(
            TryParseCommandLine(
                new[]
                {
                    "-int",
                    "-10",
                    "-long",
                    "28977479799192",
                    "-uint",
                    "0",
                    "-ulong",
                    "12",
                    "-decimal",
                    "-12.7",
                    "-float",
                    "12.2",
                    "-double",
                    "12.2",
                },
                out RangesClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-10, result.Int32);
        Assert.Equal(28977479799192L, result.Int64);
        Assert.Equal(0, result.UInt32);
        Assert.Equal(12UL, result.UInt64);
        Assert.Equal(-12.7m, result.Decimal);
        Assert.Equal(12.2, result.Single, 4);
        Assert.Equal(12.2, result.Double, 4);

        Assert.True(
            TryParseCommandLine(
                new[]
                {
                    "-int",
                    "14",
                    "-long",
                    "3802884792",
                    "-uint",
                    "14",
                    "-ulong",
                    "14",
                    "-decimal",
                    "14.2",
                    "-float",
                    "-12.2",
                    "-double",
                    "12.2",
                },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Int32);
        Assert.Equal(3802884792L, result.Int64);
        Assert.Equal(14, result.UInt32);
        Assert.Equal(14UL, result.UInt64);
        Assert.Equal(14.2m, result.Decimal);
        Assert.Equal(-12.2, result.Single, 4);
        Assert.Equal(12.2, result.Double, 4);

        Assert.False(TryParseCommandLine(new[] { "-int", "-14" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' was not in the range [-12..14]", error);

        Assert.False(TryParseCommandLine(new[] { "-int", "17" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' was not in the range [-12..14]", error);

        Assert.False(TryParseCommandLine(new[] { "-long", "-17" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-long' was not in the range [3802884792..28977479799192]", error);

        Assert.False(TryParseCommandLine(new[] { "-long", "17" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-long' was not in the range [3802884792..28977479799192]", error);

        Assert.False(TryParseCommandLine(new[] { "-long", "28977479799193" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-long' was not in the range [3802884792..28977479799192]", error);

        Assert.False(TryParseCommandLine(new[] { "-uint", "-1" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-uint' was not in the range [0..14]", error);

        Assert.False(TryParseCommandLine(new[] { "-uint", "15" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-uint' was not in the range [0..14]", error);

        Assert.False(TryParseCommandLine(new[] { "-ulong", "11" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-ulong' was not in the range [12..14]", error);

        Assert.False(TryParseCommandLine(new[] { "-ulong", "15" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-ulong' was not in the range [12..14]", error);

        Assert.False(TryParseCommandLine(new[] { "-decimal", "-12.7000001" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-decimal' was not in the range [-12.7..14.2]", error);

        Assert.False(TryParseCommandLine(new[] { "-decimal", "14.200001" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-decimal' was not in the range [-12.7..14.2]", error);

        Assert.False(TryParseCommandLine(new[] { "-float", "-12.2000009" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-float' was not in the range [-12.2..14.4]", error);

        Assert.False(TryParseCommandLine(new[] { "-float", "-14.4000001" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-float' was not in the range [-12.2..14.4]", error);

        Assert.False(TryParseCommandLine(new[] { "-double", "12.199999" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-double' was not in the range [12.2..14.4]", error);

        Assert.False(TryParseCommandLine(new[] { "-double", "-14.4000001" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-double' was not in the range [12.2..14.4]", error);
    }

    public sealed class ValidValuesClass
    {
        [Name("-int")]
        [ValidValues(10, 12, 14)]
        public int Int32 { get; set; }

        [Name("-string")]
        [ValidValues("hey", "there", "cat")]
        public string? String { get; set; }
    }

    [Fact]
    public void ValidValues()
    {
        Assert.True(
            TryParseCommandLine(new[] { "-int", "10", "-string", "hey" }, out ValidValuesClass? result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(10, result.Int32);
        Assert.Equal("hey", result.String);

        Assert.True(TryParseCommandLine(new[] { "-int", "14", "-string", "there" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Int32);
        Assert.Equal("there", result.String);

        Assert.False(TryParseCommandLine(new[] { "-int", "-14" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' must be one of: 10, 12, 14.", error);

        Assert.False(TryParseCommandLine(new[] { "-int", "13" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-int' must be one of: 10, 12, 14.", error);

        Assert.False(TryParseCommandLine(new[] { "-string", "catharsis" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-string' must be one of: \"cat\", \"hey\", \"there\".", error);

        Assert.False(TryParseCommandLine(new[] { "-string", "CAT" }, out result, out error));
        Assert.Null(result);
        Assert.Equal("'-string' must be one of: \"cat\", \"hey\", \"there\".", error);
    }

    public sealed class PositionalArgumentClass
    {
        [Name("-boolean")]
        public bool Boolean { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(true)]
        public bool Empty { get; set; }

        [PositionalArgument("extras")]
        public string[] Extras { get; set; } = null!;
    }

    [Fact]
    public void PositionalArguments()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-boolean", "1", "-empty", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" },
                out PositionalArgumentClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Equal(new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" }, result.Extras);

        Assert.True(
            TryParseCommandLine(
                new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "-boolean", "1", "-empty" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Equal(new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" }, result.Extras);

        Assert.True(
            TryParseCommandLine(
                new[] { "a", "b", "-boolean", "1", "c", "d", "e", "f", "g", "h", "-empty", "i", "j", "k" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Equal(new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" }, result.Extras);

        Assert.True(TryParseCommandLine(new[] { "-boolean", "1", "-empty" }, out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Empty(result.Extras);
    }

    public sealed class RequiredPositionalArgumentClass
    {
        [Name("-boolean")]
        public bool Boolean { get; set; }

        [Name("-empty")]
        [EmptyArgumentValue(true)]
        public bool Empty { get; set; }

        [PositionalArgument("extras")]
        public required string[] Extras { get; set; }
    }

    [Fact]
    public void RequiredPositionalArguments()
    {
        Assert.True(
            TryParseCommandLine(
                new[] { "-boolean", "1", "-empty", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" },
                out RequiredPositionalArgumentClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Equal(new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" }, result.Extras);

        Assert.True(
            TryParseCommandLine(
                new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "-boolean", "1", "-empty" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Equal(new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" }, result.Extras);

        Assert.True(
            TryParseCommandLine(
                new[] { "a", "b", "-boolean", "1", "c", "d", "e", "f", "g", "h", "-empty", "i", "j", "k" },
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Boolean);
        Assert.True(result.Empty);
        Assert.Equal(new[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k" }, result.Extras);

        Assert.False(TryParseCommandLine(new[] { "-boolean", "1", "-empty" }, out result, out error));
        Assert.Equal("Some required positional arguments were missing.", error);
        Assert.Null(result);
    }
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
