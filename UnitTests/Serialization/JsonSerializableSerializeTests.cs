// <copyright file="JsonSerializableSerializeTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Serialization.DataAnnotations.Serialization;
using KestrelToolbox.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests.Serialization;

[SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Tests describe what things are.")]
public sealed partial class JsonSerializableSerializeTests
{
    [JsonSerializable(typeof(string))]
    [JsonSerializable(typeof(StringClass))]
    [JsonSerializable(typeof(BoolClass))]
    [JsonSerializable(typeof(BoolAsIntsClass))]
    [JsonSerializable(typeof(Int8Class))]
    [JsonSerializable(typeof(Int16Class))]
    [JsonSerializable(typeof(Int32Class))]
    [JsonSerializable(typeof(Int64Class))]
    [JsonSerializable(typeof(UInt8Class))]
    [JsonSerializable(typeof(UInt16Class))]
    [JsonSerializable(typeof(UInt32Class))]
    [JsonSerializable(typeof(UInt64Class))]
    [JsonSerializable(typeof(F32Class))]
    [JsonSerializable(typeof(F64Class))]
    [JsonSerializable(typeof(DecimalClass))]
    [JsonSerializable(typeof(BoolSkipSerializationClass))]
    [JsonSerializable(typeof(Int32SkipSerializationClass))]
    [JsonSerializable(typeof(DecimalSkipSerializationClass))]
    [JsonSerializable(typeof(DateTimeOffsetClass))]
    [JsonSerializable(typeof(DateTimeClass))]
    [JsonSerializable(typeof(DateOnlyClass))]
    [JsonSerializable(typeof(TimeOnlyClass))]
    [JsonSerializable(typeof(TimeSpanClass))]
    [JsonSerializable(typeof(EnumClass))]
    [JsonSerializable(typeof(NumberEnumClass))]
    [JsonSerializable(typeof(ArrayClass))]
    [JsonSerializable(typeof(SubArrayClass))]
    [JsonSerializable(typeof(BytesClass))]
    [JsonSerializable(typeof(GuidClass))]
    [JsonSerializable(typeof(UUIDv1Class))]
    [JsonSerializable(typeof(DecimalTypesClass))]
    [JsonSerializable(typeof(ValueTypeStruct))]
    [JsonSerializable(typeof(NormalReferenceObject))]
    [JsonSerializable(typeof(NormalObjectClass))]
    [JsonSerializable(typeof(NormalObjectArraysClass))]
#pragma warning disable KTB0021
    [JsonSerializable(typeof(EmptyFieldTypeClass))]
#pragma warning restore KTB0021
    [JsonSerializable(typeof(LinkedListClass))]
    [JsonSerializable(typeof(ListClass))]
    [JsonSerializable(typeof(DictionaryClass))]
    [JsonSerializable(typeof(NewtonsoftJObjectClass), SourceGenerationMode = SourceGenerationMode.Serialize)]
    [JsonSerializable(
        typeof(JArray),
        CustomSerializingMethodName = "NewtonsoftJObjectClass.SerializeJToken",
        SourceGenerationMode = SourceGenerationMode.Serialize
    )]
    [JsonSerializable(
        typeof(JObject),
        CustomSerializingMethodName = "NewtonsoftJObjectClass.SerializeJToken",
        SourceGenerationMode = SourceGenerationMode.Serialize
    )]
    [JsonSerializable(
        typeof(JToken),
        CustomSerializingMethodName = "NewtonsoftJObjectClass.SerializeJToken",
        SourceGenerationMode = SourceGenerationMode.Serialize
    )]
    [JsonSerializable(typeof(SystemTextJsonObjectClass))]
    [JsonSerializable(typeof(SystemTextJsonObjectSkipSerializationClass))]
    [JsonSerializable(typeof(StreamObject))]
    [JsonSerializable(typeof(CustomSerializeClass), SourceGenerationMode = SourceGenerationMode.Serialize)]
    [JsonSerializable(typeof(InjectedCustomSerializeClass), SourceGenerationMode = SourceGenerationMode.Serialize)]
    [JsonSerializable(
        typeof(InjectedCustomSerializeClass.CustomValueClass),
        SourceGenerationMode = SourceGenerationMode.Serialize,
        CustomSerializingMethodName = "InjectedCustomSerializeClass.CustomValueClass.Serialize"
    )]
    [JsonSerializable(
        typeof(InjectedCustomSerializeClass.CustomValueStruct),
        SourceGenerationMode = SourceGenerationMode.Serialize,
        CustomSerializingMethodName = "InjectedCustomSerializeClass.CustomValueStruct.Serialize"
    )]
    [JsonSerializable(typeof(int), typeof(int?))]
    [JsonSerializable(
        typeof(Dictionary<string, string>),
        typeof(Dictionary<string, int>),
        typeof(IDictionaryClass<string, int>)
    )]
    [JsonSerializable(typeof(string[]), typeof(List<string>))]
    [JsonSerializable(typeof(IEnumerable<string>), typeof(IEnumerable<int>), typeof(IEnumerable<int?>))]
    [JsonSerializable(typeof(KeyValuePair<string, string>), typeof(KeyValuePair<string, int>))]
    [JsonSerializable(typeof(KeyValuePair<string, string>[]), typeof(KeyValuePair<string, int>[]))]
    [JsonSerializable(
        typeof(IEnumerable<KeyValuePair<string, string>>),
        typeof(IEnumerable<KeyValuePair<string, int>>)
    )]
    [JsonSerializable(typeof(TypedefJsonNullable))]
    [JsonSerializable(typeof(TrimClass))]
    [JsonSerializable(typeof(NullOnEmptyClass))]
    [JsonSerializable(typeof(SkipSerializationClass))]
    [JsonSerializable(typeof(InvalidValueEnum))]
    [JsonSerializable(typeof(ConcurrentBag<string>), SourceGenerationMode = SourceGenerationMode.Serialize)]
    [JsonSerializable(typeof(CustomPropertySerializingClass))]
    [JsonSerializeAnonymous]
    public static partial class JsonParser { }

    [Fact]
    public void AnonymousTypes()
    {
        Assert.Equal("""{"value":null}""", JsonParser.SerializeJsonAnonymous(new { value = default(string) }));
        Assert.Equal("""{"value":0}""", JsonParser.SerializeJsonAnonymous(new { value = 0 }));
        Assert.Equal("""{"value":null}""", JsonParser.SerializeJsonAnonymous(new { value = default(int?) }));
        Assert.Equal("""{"value":null}""", JsonParser.SerializeJsonAnonymous(new { value = default(decimal?) }));
        Assert.Equal("""{"value":true}""", JsonParser.SerializeJsonAnonymous(new { value = true }));
        Assert.Equal("""{"value":null}""", JsonParser.SerializeJsonAnonymous(new { value = default(bool?) }));
        Assert.Equal(
            """{"hello":"World","value":22,"boolean":true}""",
            JsonParser.SerializeJsonAnonymous(
                new
                {
                    hello = "World",
                    value = 22,
                    boolean = true,
                }
            )
        );

        Assert.Equal(
            """{"x":{"value":"big","nullable":"banana","default":"basketball","regularDefault":"bolo","nonNullable":"bogus","empty":"blogger"}}""",
            JsonParser.SerializeJsonAnonymous(
                new
                {
                    x = new StringClass
                    {
                        Value = "big",
                        Nullable = "banana",
                        Default = "basketball",
                        RegularDefault = "bolo",
                        NonNullable = "bogus",
                        Empty = "blogger",
                    },
                }
            )
        );
    }

    public sealed class StringClass
    {
        [Name("value")]
        public string? Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public string? Nullable { get; set; }

        [Name("default")]
        [DefaultValue("hello")]
        [SkipSerialization(Condition.WhenDefault)]
        public string? Default { get; set; }

        [Name("regularDefault")]
        [SkipSerialization(Condition.WhenDefault)]
        public string? RegularDefault { get; set; }

        [Name("nonNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public required string NonNullable { get; set; }

        [Name("empty")]
        [SkipSerialization(Condition.WhenEmpty)]
        public string? Empty { get; set; }
    }

    [Fact]
    public void StringTest()
    {
        var value = new StringClass
        {
            Value = "big",
            Nullable = "banana",
            Default = "basketball",
            RegularDefault = "bolo",
            NonNullable = "bogus",
            Empty = "blogger",
        };
        Assert.Equal(
            """{"value":"big","nullable":"banana","default":"basketball","regularDefault":"bolo","nonNullable":"bogus","empty":"blogger"}""",
            JsonParser.SerializeJson(value)
        );

        value = new StringClass
        {
            Value = null,
            Nullable = null,
            RegularDefault = null,
            Default = null,
            NonNullable = null!,
            Empty = null!,
        };
        Assert.Equal("""{"value":null,"default":null,"empty":null}""", JsonParser.SerializeJson(value));

        value = new StringClass
        {
            Value = string.Empty,
            Nullable = string.Empty,
            Default = string.Empty,
            RegularDefault = string.Empty,
            NonNullable = string.Empty,
            Empty = string.Empty,
        };
        Assert.Equal(
            """{"value":"","nullable":"","default":"","regularDefault":"","nonNullable":""}""",
            JsonParser.SerializeJson(value)
        );

        value = new StringClass
        {
            Value = "hello",
            Nullable = "hello",
            Default = "hello",
            RegularDefault = "hello",
            NonNullable = "hello",
            Empty = "hello",
        };
        Assert.Equal(
            """{"value":"hello","nullable":"hello","regularDefault":"hello","nonNullable":"hello","empty":"hello"}""",
            JsonParser.SerializeJson(value)
        );
    }

    public sealed class BoolClass
    {
        [Name("value")]
        public bool Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public bool? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(true)]
        [SkipSerialization(Condition.WhenDefault)]
        public bool Default { get; set; } = true;
    }

    [Fact]
    public void Bool()
    {
        var value = new BoolClass { Value = true, Nullable = false };

        Assert.Equal("""{"value":true,"nullable":false}""", JsonParser.SerializeJson(value));

        value = new BoolClass
        {
            Value = false,
            Nullable = true,
            Default = false,
        };

        Assert.Equal("""{"value":false,"nullable":true,"default":false}""", JsonParser.SerializeJson(value));

        value = new BoolClass
        {
            Value = false,
            Nullable = null,
            Default = true,
        };
        Assert.Equal("""{"value":false}""", JsonParser.SerializeJson(value));
    }

    public sealed class BoolAsIntsClass
    {
        [Name("value")]
        [BoolSerializableSettings(SerializationType = BoolSerializableSettingsSerializationType.Numbers)]
        public bool Value { get; set; }

        [Name("nullable")]
        [BoolSerializableSettings(SerializationType = BoolSerializableSettingsSerializationType.Numbers)]
        public bool? Nullable { get; set; }
    }

    [Fact]
    public void BoolAsInt()
    {
        var value = new BoolAsIntsClass { Value = true, Nullable = false };

        Assert.Equal("""{"value":1,"nullable":0}""", JsonParser.SerializeJson(value));

        value = new BoolAsIntsClass { Value = false, Nullable = true };

        Assert.Equal("""{"value":0,"nullable":1}""", JsonParser.SerializeJson(value));

        value = new BoolAsIntsClass { Value = false, Nullable = null };
        Assert.Equal("""{"value":0,"nullable":null}""", JsonParser.SerializeJson(value));
    }

    public sealed class Int8Class
    {
        [Name("value")]
        public sbyte Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public sbyte? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public sbyte Default { get; set; } = 30;
    }

    [Fact]
    public void Int8Test()
    {
        var value = new Int8Class { Value = 52, Nullable = 22 };

        Assert.Equal("""{"value":52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new Int8Class
        {
            Value = -32,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":-32,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new Int8Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class Int16Class
    {
        [Name("value")]
        public short Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public short? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public short Default { get; set; } = 30;
    }

    [Fact]
    public void Int16Test()
    {
        var value = new Int16Class { Value = 52, Nullable = 22 };

        Assert.Equal("""{"value":52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new Int16Class
        {
            Value = 17,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":17,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new Int16Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class Int32Class
    {
        [Name("value")]
        public int Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public int? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public int Default { get; set; } = 30;
    }

    [Fact]
    public void Int32Test()
    {
        var value = new Int32Class { Value = 52, Nullable = 22 };

        Assert.Equal("""{"value":52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new Int32Class
        {
            Value = 17,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":17,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new Int32Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class Int64Class
    {
        [Name("value")]
        public long Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public long? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public long Default { get; set; } = 30;
    }

    [Fact]
    public void Int64Test()
    {
        var value = new Int64Class { Value = -52, Nullable = 22 };

        Assert.Equal("""{"value":-52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new Int64Class
        {
            Value = 17,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":17,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new Int64Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class UInt8Class
    {
        [Name("value")]
        public byte Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public byte? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public byte Default { get; set; } = 30;
    }

    [Fact]
    public void UInt8Test()
    {
        var value = new UInt8Class { Value = 52, Nullable = 22 };

        Assert.Equal("""{"value":52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new UInt8Class
        {
            Value = 230,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":230,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new UInt8Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class UInt16Class
    {
        [Name("value")]
        public short Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public short? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public short Default { get; set; } = 30;
    }

    [Fact]
    public void UInt16Test()
    {
        var value = new UInt16Class { Value = 52, Nullable = 22 };

        Assert.Equal("""{"value":52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new UInt16Class
        {
            Value = 17,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":17,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new UInt16Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class UInt32Class
    {
        [Name("value")]
        public int Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public int? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public int Default { get; set; } = 30;
    }

    [Fact]
    public void UInt32Test()
    {
        var value = new UInt32Class { Value = 52, Nullable = 22 };

        Assert.Equal("""{"value":52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new UInt32Class
        {
            Value = 17,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":17,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new UInt32Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class UInt64Class
    {
        [Name("value")]
        public long Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public long? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30)]
        [SkipSerialization(Condition.WhenDefault)]
        public long Default { get; set; } = 30;
    }

    [Fact]
    public void UInt64Test()
    {
        var value = new UInt64Class { Value = -52, Nullable = 22 };

        Assert.Equal("""{"value":-52,"nullable":22}""", JsonParser.SerializeJson(value));

        value = new UInt64Class
        {
            Value = 17,
            Nullable = 0,
            Default = 0,
        };

        Assert.Equal("""{"value":17,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new UInt64Class
        {
            Value = 0,
            Nullable = null,
            Default = 0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class F32Class
    {
        [Name("value")]
        public float Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public float? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30.2f)]
        [SkipSerialization(Condition.WhenDefault)]
        public float Default { get; set; } = 30.2f;
    }

    [Fact]
    public void F32Test()
    {
        var value = new F32Class { Value = -52.67f, Nullable = 22.87f };

        Assert.Equal("""{"value":-52.67,"nullable":22.87}""", JsonParser.SerializeJson(value));

        value = new F32Class
        {
            Value = 17.2f,
            Nullable = 0.0f,
            Default = 0.0f,
        };

        Assert.Equal("""{"value":17.2,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new F32Class
        {
            Value = 0.0f,
            Nullable = null,
            Default = 0.0f,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class F64Class
    {
        [Name("value")]
        public double Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public double? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(30.2)]
        [SkipSerialization(Condition.WhenDefault)]
        public double Default { get; set; } = 30.2;
    }

    [Fact]
    public void F64Test()
    {
        var value = new F64Class { Value = -52.67, Nullable = 22.87 };

        Assert.Equal("""{"value":-52.67,"nullable":22.87}""", JsonParser.SerializeJson(value));

        value = new F64Class
        {
            Value = 17.2,
            Nullable = 0.0,
            Default = 0.0,
        };

        Assert.Equal("""{"value":17.2,"nullable":0,"default":0}""", JsonParser.SerializeJson(value));

        value = new F64Class
        {
            Value = 0.0,
            Nullable = null,
            Default = 0.0,
        };
        Assert.Equal("""{"value":0,"default":0}""", JsonParser.SerializeJson(value));
    }

    public sealed class DecimalClass
    {
        [Name("value")]
        public decimal Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public decimal? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(typeof(decimal), "30.2")]
        [SkipSerialization(Condition.WhenDefault)]
        public decimal Default { get; set; } = 30.2M;

        [Name("defaultInitializer")]
        [SkipSerialization(Condition.WhenDefault)]
        public decimal DefaultInitializer { get; set; } = 30.2M;
    }

    [Fact]
    public void DecimalTest()
    {
        var value = new DecimalClass { Value = -52.67M, Nullable = 22.87M };

        Assert.Equal("""{"value":-52.67,"nullable":22.87}""", JsonParser.SerializeJson(value));

        value = new DecimalClass
        {
            Value = 17.2M,
            Nullable = 0.0M,
            Default = 0.0M,
            DefaultInitializer = 0.0M,
        };

        Assert.Equal(
            """{"value":17.2,"nullable":0.0,"default":0.0,"defaultInitializer":0.0}""",
            JsonParser.SerializeJson(value)
        );

        value = new DecimalClass
        {
            Value = 0.0M,
            Nullable = null,
            Default = 0.0M,
        };
        Assert.Equal("""{"value":0.0,"default":0.0}""", JsonParser.SerializeJson(value));
    }

    public sealed class BoolSkipSerializationClass
    {
        [Name("default")]
        [SkipSerialization(Condition.WhenDefault)]
        [DefaultValue(true)]
        public bool? Default { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenDefault | Condition.WhenNull)]
        [DefaultValue(true)]
        public bool? Nullable { get; set; }
    }

    [Fact]
    public void BoolSkipSerialization()
    {
        var value = new BoolSkipSerializationClass { Default = true, Nullable = true };

        Assert.Equal("""{}""", JsonParser.SerializeJson(value));

        value = new BoolSkipSerializationClass { Default = null, Nullable = null };

        Assert.Equal("""{"default":null}""", JsonParser.SerializeJson(value));

        value = new BoolSkipSerializationClass { Default = null, Nullable = false };

        Assert.Equal("""{"default":null,"nullable":false}""", JsonParser.SerializeJson(value));
    }

    public sealed class Int32SkipSerializationClass
    {
        [Name("default")]
        [SkipSerialization(Condition.WhenDefault)]
        [DefaultValue(30)]
        public int? Default { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenDefault | Condition.WhenNull)]
        [DefaultValue(30)]
        public int? Nullable { get; set; }
    }

    [Fact]
    public void Int32SkipSerialization()
    {
        var value = new Int32SkipSerializationClass { Default = 30, Nullable = 30 };

        Assert.Equal("""{}""", JsonParser.SerializeJson(value));

        value = new Int32SkipSerializationClass { Default = null, Nullable = null };

        Assert.Equal("""{"default":null}""", JsonParser.SerializeJson(value));

        value = new Int32SkipSerializationClass { Default = null, Nullable = 22 };

        Assert.Equal("""{"default":null,"nullable":22}""", JsonParser.SerializeJson(value));
    }

    public sealed class DecimalSkipSerializationClass
    {
        [Name("default")]
        [SkipSerialization(Condition.WhenDefault)]
        [DefaultValue(30)]
        public decimal? Default { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenDefault | Condition.WhenNull)]
        [DefaultValue(30)]
        public decimal? Nullable { get; set; }
    }

    [Fact]
    public void DecimalSkipSerialization()
    {
        var value = new DecimalSkipSerializationClass { Default = 30, Nullable = 30 };

        Assert.Equal("""{}""", JsonParser.SerializeJson(value));

        value = new DecimalSkipSerializationClass { Default = null, Nullable = null };

        Assert.Equal("""{"default":null}""", JsonParser.SerializeJson(value));

        value = new DecimalSkipSerializationClass { Default = null, Nullable = 22 };

        Assert.Equal("""{"default":null,"nullable":22}""", JsonParser.SerializeJson(value));
    }

    public sealed class DateTimeOffsetClass
    {
        [Name("dateTime")]
        [SkipSerialization(Condition.WhenDefault)]
        public DateTime DateTime { get; set; }

        [Name("dateTimeOffset")]
        [SkipSerialization(Condition.WhenDefault)]
        public DateTimeOffset DateTimeOffset { get; set; }

        [Name("dateTimeNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public DateTime? DateTimeNullable { get; set; } = default(DateTime);

        [Name("dateTimeOffsetNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public DateTimeOffset? DateTimeOffsetNullable { get; set; } = default(DateTimeOffset);
    }

    [Fact]
    public void DateTimeOffsetTests()
    {
        var value = new DateTimeOffsetClass
        {
            DateTime = DateTime.SpecifyKind(
                DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture),
                DateTimeKind.Utc
            ),
            DateTimeOffset = DateTimeOffset.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
            DateTimeNullable = DateTime.SpecifyKind(
                DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture),
                DateTimeKind.Utc
            ),
            DateTimeOffsetNullable = DateTimeOffset.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
        };
        Assert.Equal(
            """{"dateTime":"2010-05-03T00:00:00.000Z","dateTimeOffset":"2021-07-08T15:55:34.000Z","dateTimeNullable":"2010-05-03T00:00:00.000Z","dateTimeOffsetNullable":"2021-07-08T15:55:34.000Z"}""",
            JsonParser.SerializeJson(value)
        );

        value = new DateTimeOffsetClass
        {
            DateTime = default,
            DateTimeOffset = default,
            DateTimeNullable = null,
            DateTimeOffsetNullable = null,
        };
        Assert.Equal("""{}""", JsonParser.SerializeJson(value));

        value = new DateTimeOffsetClass
        {
            DateTime = DateTime.SpecifyKind(
                DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture),
                DateTimeKind.Utc
            ),
            DateTimeOffset = DateTimeOffset.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
            DateTimeNullable = default(DateTime),
            DateTimeOffsetNullable = default(DateTimeOffset),
        };
        Assert.Equal(
            """{"dateTime":"2010-05-03T00:00:00.000Z","dateTimeOffset":"2021-07-08T15:55:34.000Z"}""",
            JsonParser.SerializeJson(value)
        );
    }

    public sealed class DateTimeClass
    {
        [Name("value")]
        public DateTime Value { get; set; }

        [Name("value2")]
        public DateTime? Value2 { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public DateTime? Nullable { get; set; }
    }

    [Fact]
    public void DateTimeTest()
    {
        var value = new DateTimeClass
        {
            Value = DateTime.SpecifyKind(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), DateTimeKind.Utc),
            Value2 = DateTime.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
            Nullable = DateTime.SpecifyKind(default, DateTimeKind.Utc), // Need to specify kind. DateTime defaults to local timezone.
        };
        Assert.Equal(
            """{"value":"2010-05-03T00:00:00.000Z","value2":"2021-07-08T15:55:34.000Z","nullable":"0001-01-01T00:00:00.000Z"}""",
            JsonParser.SerializeJson(value)
        );

        value = new DateTimeClass
        {
            Value = DateTime.SpecifyKind(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), DateTimeKind.Utc),
            Value2 = null,
            Nullable = null,
        };
        Assert.Equal("""{"value":"2010-05-03T00:00:00.000Z","value2":null}""", JsonParser.SerializeJson(value));

        value = new DateTimeClass
        {
            Value = DateTime.SpecifyKind(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), DateTimeKind.Utc),
            Value2 = null,
            Nullable = DateTime.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
        };
        Assert.Equal(
            """{"value":"2010-05-03T00:00:00.000Z","value2":null,"nullable":"2021-07-08T15:55:34.000Z"}""",
            JsonParser.SerializeJson(value)
        );
    }

    public sealed class DateOnlyClass
    {
        [Name("value")]
        public DateOnly Value { get; set; }

        [Name("value2")]
        [DateOnlyParseSettings(Format = "dd MMM yyyy")]
        public DateOnly? Value2 { get; set; }

        [Name("nullable")]
        [DateOnlyParseSettings(Format = "o")]
        [SkipSerialization(Condition.WhenNull)]
        public DateOnly? Nullable { get; set; }
    }

    [Fact]
    public void DateOnlyTest()
    {
        var value = new DateOnlyClass
        {
            Value = DateOnly.Parse("May 3, 2010", CultureInfo.InvariantCulture),
            Value2 = DateOnly.Parse("2021-07-08", CultureInfo.InvariantCulture),
            Nullable = default(DateOnly),
        };
        Assert.Equal(
            """{"value":"05/03/2010","value2":"08 Jul 2021","nullable":"0001-01-01"}""",
            JsonParser.SerializeJson(value)
        );

        value = new DateOnlyClass
        {
            Value = DateOnly.Parse("July 3, 2010", CultureInfo.InvariantCulture),
            Value2 = null,
            Nullable = null,
        };
        Assert.Equal("""{"value":"07/03/2010","value2":null}""", JsonParser.SerializeJson(value));

        value = new DateOnlyClass
        {
            Value = DateOnly.Parse("July 3, 2010", CultureInfo.InvariantCulture),
            Value2 = null,
            Nullable = DateOnly.Parse("2021-07-08", CultureInfo.InvariantCulture),
        };
        Assert.Equal(
            """{"value":"07/03/2010","value2":null,"nullable":"2021-07-08"}""",
            JsonParser.SerializeJson(value)
        );
    }

    public sealed class TimeOnlyClass
    {
        [Name("value")]
        public TimeOnly Value { get; set; }

        [Name("value2")]
        public TimeOnly? Value2 { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public TimeOnly? Nullable { get; set; }
    }

    [Fact]
    public void TimeOnlyTest()
    {
        var value = new TimeOnlyClass
        {
            Value = TimeOnly.Parse("11:24:27.0523 pm", CultureInfo.InvariantCulture),
            Value2 = TimeOnly.Parse("12:53pm", CultureInfo.InvariantCulture),
            Nullable = default(TimeOnly),
        };
        Assert.Equal(
            """{"value":"23:24:27.0523","value2":"12:53:00","nullable":"00:00:00"}""",
            JsonParser.SerializeJson(value)
        );

        value = new TimeOnlyClass
        {
            Value = TimeOnly.Parse("17:05", CultureInfo.InvariantCulture),
            Value2 = null,
            Nullable = null,
        };
        Assert.Equal("""{"value":"17:05:00","value2":null}""", JsonParser.SerializeJson(value));

        value = new TimeOnlyClass
        {
            Value = TimeOnly.Parse("22:52", CultureInfo.InvariantCulture),
            Nullable = TimeOnly.Parse("11:03", CultureInfo.InvariantCulture),
        };
        Assert.Equal("""{"value":"22:52:00","value2":null,"nullable":"11:03:00"}""", JsonParser.SerializeJson(value));
    }

    public sealed class TimeSpanClass
    {
        [Name("value")]
        [SkipSerialization(Condition.WhenDefault)]
        public TimeSpan Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull | Condition.WhenDefault)]
        public TimeSpan? Nullable { get; set; } = TimeSpan.Zero;
    }

    [Fact]
    public void TimeSpanTest()
    {
        var value = new TimeSpanClass { Value = TimeSpan.FromSeconds(20), Nullable = TimeSpan.FromMinutes(22.4) };
        Assert.Equal("""{"value":"00:00:20","nullable":"00:22:24"}""", JsonParser.SerializeJson(value));

        value = new TimeSpanClass { Value = TimeSpan.Zero, Nullable = TimeSpan.FromMinutes(22.4) };
        Assert.Equal("""{"nullable":"00:22:24"}""", JsonParser.SerializeJson(value));

        value = new TimeSpanClass { Value = TimeSpan.FromTicks(18759205), Nullable = null };
        Assert.Equal("""{"value":"00:00:01.8759205"}""", JsonParser.SerializeJson(value));

        value = new TimeSpanClass { Value = TimeSpan.Zero, Nullable = null };
        Assert.Equal("""{}""", JsonParser.SerializeJson(value));

        value = new TimeSpanClass { Value = TimeSpan.Zero, Nullable = TimeSpan.Zero };
        Assert.Equal("""{}""", JsonParser.SerializeJson(value));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,

        Unnamed,
    }

    public sealed class EnumClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestEnum), "B")]
        public TestEnum Value { get; set; }

        [Name("default")]
        [DefaultValue(typeof(TestEnum), "B")]
        [SkipSerialization(Condition.WhenDefault)]
        public TestEnum Default { get; set; }

        [Name("nullable")]
        [DefaultValue(typeof(TestEnum), "B")]
        [SkipSerialization(Condition.WhenNull | Condition.WhenDefault)]
        public TestEnum? Nullable { get; set; }
    }

    [Fact]
    public void Enums()
    {
        var value = new EnumClass
        {
            Value = TestEnum.B,
            Default = TestEnum.B,
            Nullable = TestEnum.B,
        };
        Assert.Equal("""{"value":"b"}""", JsonParser.SerializeJson(value));

        value = new EnumClass
        {
            Value = TestEnum.A,
            Default = TestEnum.A,
            Nullable = TestEnum.A,
        };
        Assert.Equal("""{"value":"a","default":"a","nullable":"a"}""", JsonParser.SerializeJson(value));

        value = new EnumClass
        {
            Value = TestEnum.Unnamed,
            Default = TestEnum.Unnamed,
            Nullable = TestEnum.Unnamed,
        };
        Assert.Equal("""{"value":"4","default":"4","nullable":"4"}""", JsonParser.SerializeJson(value));

        value = new EnumClass
        {
            Value = TestEnum.None,
            Default = TestEnum.SuperLong,
            Nullable = null,
        };
        Assert.Equal("""{"value":"none","default":"superLongName"}""", JsonParser.SerializeJson(value));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(SerializationType = EnumSerializableSettingsSerializationType.Numbers)]
    public enum TestNumberEnum
    {
        A = 0,
        B = 1,
        None = 17,
        SuperLong = 12345,
        Unnamed = -1,
    }

    public sealed class NumberEnumClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum Value { get; set; }

        [Name("default")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        [SkipSerialization(Condition.WhenDefault)]
        public TestNumberEnum Default { get; set; }

        [Name("nullable")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        [SkipSerialization(Condition.WhenNull | Condition.WhenDefault)]
        public TestNumberEnum? Nullable { get; set; }
    }

    [Fact]
    public void NumberEnums()
    {
        var value = new NumberEnumClass
        {
            Value = TestNumberEnum.B,
            Default = TestNumberEnum.B,
            Nullable = TestNumberEnum.B,
        };
        Assert.Equal("""{"value":1}""", JsonParser.SerializeJson(value));

        value = new NumberEnumClass
        {
            Value = TestNumberEnum.A,
            Default = TestNumberEnum.A,
            Nullable = TestNumberEnum.A,
        };
        Assert.Equal("""{"value":0,"default":0,"nullable":0}""", JsonParser.SerializeJson(value));

        value = new NumberEnumClass
        {
            Value = TestNumberEnum.Unnamed,
            Default = TestNumberEnum.Unnamed,
            Nullable = TestNumberEnum.Unnamed,
        };
        Assert.Equal("""{"value":-1,"default":-1,"nullable":-1}""", JsonParser.SerializeJson(value));

        value = new NumberEnumClass
        {
            Value = TestNumberEnum.None,
            Default = TestNumberEnum.SuperLong,
            Nullable = null,
        };
        Assert.Equal("""{"value":17,"default":12345}""", JsonParser.SerializeJson(value));
    }

    public sealed class ArrayClass
    {
        [Name("ints")]
        public int[]? Ints { get; set; }

        [Name("strings")]
        [SkipSerialization(Condition.WhenNull)]
        public string[]? Strings { get; set; }

        [Name("pairs")]
        [SkipSerialization(Condition.WhenNullOrEmpty)]
        public KeyValuePair<string, string>[]? Pairs { get; set; }
    }

    [Fact]
    public void ArrayTest()
    {
        var value = new ArrayClass
        {
            Ints = new[] { 2, 3, 4 },
            Strings = new[] { "hello", "world" },
            Pairs = new[] { new KeyValuePair<string, string>("a", "b"), new KeyValuePair<string, string>("c", "d") },
        };
        Assert.Equal(
            """{"ints":[2,3,4],"strings":["hello","world"],"pairs":{"a":"b","c":"d"}}""",
            JsonParser.SerializeJson(value)
        );

        value = new ArrayClass
        {
            Ints = null,
            Strings = null,
            Pairs = null,
        };
        Assert.Equal("""{"ints":null}""", JsonParser.SerializeJson(value));

        value = new ArrayClass
        {
            Ints = null,
            Strings = null,
            Pairs = Array.Empty<KeyValuePair<string, string>>(),
        };
        Assert.Equal("""{"ints":null}""", JsonParser.SerializeJson(value));
    }

    public sealed class SubArrayClass
    {
        [Name("ints")]
        public int[][]? Ints { get; set; }
    }

    [Fact]
    public void SubArray()
    {
        var value = new SubArrayClass { Ints = new[] { new[] { 2, 3, 4 } } };
        Assert.Equal("""{"ints":[[2,3,4]]}""", JsonParser.SerializeJson(value));
    }

    public sealed class BytesClass
    {
        [Name("value")]
        [SkipSerialization(Condition.WhenNull)]
        public byte[]? Value { get; set; }

        [Name("values")]
        public byte[][]? Values { get; set; }

        [Name("empty")]
        [SkipSerialization(Condition.WhenNullOrEmpty)]
        public byte[]? Empty { get; set; }
    }

    [Fact]
    public void Bytes()
    {
        var value = new BytesClass
        {
            Value = "hello"u8.ToArray(),
            Values = null,
            Empty = null,
        };
        Assert.Equal("""{"value":"aGVsbG8=","values":null}""", JsonParser.SerializeJson(value));

        value = new BytesClass
        {
            Value = null,
            Values = new[] { "hello"u8.ToArray(), "world"u8.ToArray() },
            Empty = Array.Empty<byte>(),
        };
        Assert.Equal("""{"values":["aGVsbG8=","d29ybGQ="]}""", JsonParser.SerializeJson(value));

        value = new BytesClass
        {
            Value = Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F"),
            Values = new[]
            {
                Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F"),
            },
            Empty = Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F"),
        };
        Assert.Equal(
            """{"value":"4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/","values":["4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/"],"empty":"4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/"}""",
            JsonParser.SerializeJson(value)
        );
    }

    public sealed class GuidClass
    {
        [Name("value")]
        [SkipSerialization(Condition.WhenDefault)]
        public Guid Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public Guid? Nullable { get; set; }
    }

    [Fact]
    public void GuidTest()
    {
        var value = new GuidClass() { Value = Guid.Parse("9184400c-d7fe-422f-9d40-720c30a2b18f"), Nullable = null };
        Assert.Equal("""{"value":"9184400c-d7fe-422f-9d40-720c30a2b18f"}""", JsonParser.SerializeJson(value));

        value = new GuidClass() { Value = Guid.Empty, Nullable = Guid.Parse("9184400c-d7fe-422f-9d40-720c30a2b18e") };
        Assert.Equal("""{"nullable":"9184400c-d7fe-422f-9d40-720c30a2b18e"}""", JsonParser.SerializeJson(value));
    }

    [SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Spec correct.")]
    public sealed class UUIDv1Class
    {
        [Name("value")]
        [SkipSerialization(Condition.WhenDefault)]
        public UUIDv1 Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull)]
        public UUIDv1? Nullable { get; set; }
    }

    [Fact]
    public void UUIDv1Test()
    {
        var value = new UUIDv1Class() { Value = UUIDv1.Parse("13b61f1e-3c12-11ef-9454-0242ac120002"), Nullable = null };
        Assert.Equal("""{"value":"13b61f1e-3c12-11ef-9454-0242ac120002"}""", JsonParser.SerializeJson(value));

        value = new UUIDv1Class()
        {
            Value = UUIDv1.Empty,
            Nullable = UUIDv1.Parse("13b61f1e-3c12-11ef-9454-0242ac120002"),
        };
        Assert.Equal("""{"nullable":"13b61f1e-3c12-11ef-9454-0242ac120002"}""", JsonParser.SerializeJson(value));
    }

    public sealed class DecimalTypesClass
    {
        [Name("float")]
        public float Float { get; set; }

        [Name("double")]
        public double Double { get; set; }

        [Name("decimal")]
        public decimal Decimal { get; set; }
    }

    [Fact]
    public void DecimalTypes()
    {
        var value = new DecimalTypesClass
        {
            Float = 2.0f,
            Double = 409.0,
            Decimal = 8200M,
        };
        Assert.Equal("""{"float":2,"double":409,"decimal":8200}""", JsonParser.SerializeJson(value));

        value = new DecimalTypesClass
        {
            Float = 2.48f,
            Double = 409.46684,
            Decimal = 820055.1668863313M,
        };
        Assert.Equal(
            """{"float":2.48,"double":409.46684,"decimal":820055.1668863313}""",
            JsonParser.SerializeJson(value)
        );

        value = new DecimalTypesClass
        {
            Float = -2.48f,
            Double = -409.46684,
            Decimal = -820055.1668863313M,
        };
        Assert.Equal(
            """{"float":-2.48,"double":-409.46684,"decimal":-820055.1668863313}""",
            JsonParser.SerializeJson(value)
        );

        value = new DecimalTypesClass
        {
            Float = 9864684668338734.168616838f,
            Double = 986834384436688631.16864,
            Decimal = -79228162514264337593543950335M,
        };
        Assert.Equal(
            """{"float":9.864685E+15,"double":9.868343844366886E+17,"decimal":-79228162514264337593543950335}""",
            JsonParser.SerializeJson(value)
        );

        value = new DecimalTypesClass
        {
            Float = 0,
            Double = 0,
            Decimal = 0,
        };
        Assert.Equal("""{"float":0,"double":0,"decimal":0}""", JsonParser.SerializeJson(value));
    }

    public struct ValueTypeStruct
    {
        [Name("value")]
        public int Value { get; set; }

        [Name("string")]
        public string? StringValue { get; set; }
    }

    [Fact]
    public void ValueType()
    {
        var value = new ValueTypeStruct { Value = 2, StringValue = "rock" };
        Assert.Equal("""{"value":2,"string":"rock"}""", JsonParser.SerializeJson(value));
    }

    public class NormalReferenceObject
    {
        [Name("string")]
        public string? StringValue { get; set; }

        [Name("int")]
        public int IntValue { get; set; }
    }

    public struct NormalValueObject
    {
        [Name("bytes")]
        public byte[]? Bytes { get; set; }
    }

    public class NormalObjectClass
    {
        [Name("value")]
        public NormalValueObject Value { get; set; }

        [Name("reference")]
        public NormalReferenceObject? Reference { get; set; }
    }

    [Fact]
    public void NormalObject()
    {
        var value = new NormalObjectClass
        {
            Value = new NormalValueObject { Bytes = new byte[] { 1, 2, 3, 4 } },
            Reference = new NormalReferenceObject { StringValue = "howdy", IntValue = 22 },
        };
        Assert.Equal(
            """{"value":{"bytes":"AQIDBA=="},"reference":{"string":"howdy","int":22}}""",
            JsonParser.SerializeJson(value)
        );
    }

    public class NormalObjectArraysClass
    {
        [Name("values")]
        public NormalValueObject[]? Values { get; set; }

        [Name("references")]
        public NormalReferenceObject[]? References { get; set; }
    }

    [Fact]
    public void NormalObjectArrays()
    {
        var value = new NormalObjectArraysClass()
        {
            Values = new[] { new NormalValueObject { Bytes = new byte[] { 1, 2, 3, 4 } } },
            References = new[]
            {
                new NormalReferenceObject { StringValue = "howdy", IntValue = 22 },
            },
        };
        Assert.Equal(
            """{"values":[{"bytes":"AQIDBA=="}],"references":[{"string":"howdy","int":22}]}""",
            JsonParser.SerializeJson(value)
        );

        value = new NormalObjectArraysClass()
        {
            Values = new[]
            {
                new NormalValueObject { Bytes = new byte[] { 1, 2, 3, 4 } },
                new NormalValueObject { Bytes = new byte[] { 24, 22, 13, 12, 55 } },
            },
            References = new[]
            {
                new NormalReferenceObject { StringValue = "howdy", IntValue = 22 },
                new NormalReferenceObject { StringValue = "partner", IntValue = 57 },
            },
        };
        Assert.Equal(
            """{"values":[{"bytes":"AQIDBA=="},{"bytes":"GBYNDDc="}],"references":[{"string":"howdy","int":22},{"string":"partner","int":57}]}""",
            JsonParser.SerializeJson(value)
        );
    }

    public sealed class EmptyFieldTypeClass
    {
        [Required]
        [Name("value")]
        public int Value { get; set; }

        [Name("subclass")]
        public Subclass? SubValue { get; set; }

        public sealed class Subclass
        {
#pragma warning disable CS0649, S3459, IDE0044
            private int a;
            private int b;
#pragma warning restore CS0649, S3459, IDE0044

            public override string ToString() => $"{this.a}-${this.b}";
        }
    }

    [Fact]
    public void EmptyFieldType()
    {
        var value = new EmptyFieldTypeClass { Value = 22 };
        Assert.Equal("""{"value":22,"subclass":null}""", JsonParser.SerializeJson(value));

        value = new EmptyFieldTypeClass { Value = 22, SubValue = new EmptyFieldTypeClass.Subclass() };
        Assert.Equal("""{"value":22,"subclass":{}}""", JsonParser.SerializeJson(value));
    }

    [Fact]
    public void JsonWriterSettings()
    {
        var x = new
        {
            s = "string",
            i = 37,
            d = 0.0,
            n = default(string),
        };

        Assert.Equal("""{"s":"string","i":37,"d":0,"n":null}""", JsonParser.SerializeJsonAnonymous(x));
        Assert.Equal(
            """
            {
              "s": "string",
              "i": 37,
              "d": 0,
              "n": null
            }
            """.ReplaceLineEndings("\n"),
            JsonParser.SerializeJsonAnonymous(x, new JsonWriterOptions { Indented = true }).ReplaceLineEndings("\n")
        );

        Assert.Equal(
            """
            {
              "value": "hello",
              "default": null,
              "nonNullable": "null",
              "empty": null
            }
            """,
            JsonParser
                .SerializeJson(
                    new StringClass { Value = "hello", NonNullable = "null" },
                    new JsonWriterOptions { Indented = true }
                )
                .ReplaceLineEndings("\n")
        );
    }

    public class LinkedListClass
    {
        [Name("value")]
        [Required]
        public int Value { get; set; }

        [Name("child")]
        public LinkedListClass? Child { get; set; }
    }

    [Fact]
    public void LinkedList()
    {
        var head = new LinkedListClass() { Value = -1 };

        for (var i = 0; i < 8; i++)
        {
            var node = new LinkedListClass() { Child = head, Value = i };
            head = node;
        }

        Assert.Equal(
            """{"value":7,"child":{"value":6,"child":{"value":5,"child":{"value":4,"child":{"value":3,"child":{"value":2,"child":{"value":1,"child":{"value":0,"child":{"value":-1,"child":null}}}}}}}}}""",
            JsonParser.SerializeJson(head)
        );
    }

    public sealed class ListClass
    {
        [Name("value")]
        [Required]
        public List<int> Value { get; set; } = null!;

        [Name("skip")]
        [SkipSerialization(Condition.WhenEmpty | Condition.WhenNull)]
        public List<string>? Skip { get; set; }
    }

    [Fact]
    public void List()
    {
        var value = new ListClass
        {
            Value = new List<int> { 22, 23, 24 },
            Skip = new List<string> { "a", "b", "c" },
        };
        Assert.Equal("""{"value":[22,23,24],"skip":["a","b","c"]}""", JsonParser.SerializeJson(value));

        value = new ListClass
        {
            Value = new List<int> { 22, 23, 24 },
            Skip = new List<string>(),
        };
        Assert.Equal("""{"value":[22,23,24]}""", JsonParser.SerializeJson(value));
    }

    public sealed class DictionaryClass
    {
        [Name("value")]
        public required Dictionary<string, string> Value { get; set; } = null!;

        [Name("skip")]
        [SkipSerialization(Condition.WhenNullOrEmpty)]
        public Dictionary<string, string>? Skip { get; set; }
    }

    [Fact]
    public void Dictionary()
    {
        var value = new DictionaryClass
        {
            Value = new Dictionary<string, string>(),
            Skip = new Dictionary<string, string>(),
        };
        Assert.Equal("""{"value":{}}""", JsonParser.SerializeJson(value));
        value = new DictionaryClass
        {
            Value = new Dictionary<string, string> { { "hello", "world" } },
            Skip = new Dictionary<string, string> { { "a", "b" } },
        };
        Assert.Equal("""{"value":{"hello":"world"},"skip":{"a":"b"}}""", JsonParser.SerializeJson(value));
    }

    public class NewtonsoftJObjectClass
    {
        [Name("object")]
        public JObject? ObjectValue { get; set; }

        [Name("array")]
        [SkipSerialization(Condition.WhenNull)]
        public JArray? ArrayValue { get; set; }

        [Name("token")]
        public JToken? TokenValue { get; set; }

        public static void SerializeJToken(Utf8JsonWriter writer, JToken value)
        {
            writer.WriteRawValue(value.ToString(Formatting.None));
        }
    }

    [Fact]
    public void NewtonsoftJObject()
    {
        var value = new NewtonsoftJObjectClass
        {
            ObjectValue = new JObject { ["hello"] = "world", ["value"] = 12 },
            ArrayValue = new JArray(12, 13, "hello", true),
            TokenValue = new JValue("kestrel toolbox"),
        };

        Assert.Equal(
            """{"object":{"hello":"world","value":12},"array":[12,13,"hello",true],"token":"kestrel toolbox"}""",
            JsonParser.SerializeJson(value)
        );

        value = new NewtonsoftJObjectClass
        {
            ObjectValue = null,
            ArrayValue = null,
            TokenValue = null,
        };
        Assert.Equal("""{"object":null,"token":null}""", JsonParser.SerializeJson(value));
    }

    public class SystemTextJsonObjectClass
    {
        [Name("object")]
        public JsonObject? ObjectValue { get; set; }

        [Name("array")]
        [SkipSerialization(Condition.WhenNull)]
        public JsonArray? ArrayValue { get; set; }

        [Name("node")]
        public JsonNode? NodeValue { get; set; }
    }

    [Fact]
    public void SystemTextJsonObject()
    {
        var value = new SystemTextJsonObjectClass
        {
            ObjectValue = new JsonObject { ["hello"] = "world", ["value"] = 12 },
            ArrayValue = new JsonArray(12, 13, "hello", true),
            NodeValue = JsonNode.Parse("\"kestrel toolbox\""),
        };
        Assert.Equal(
            """{"object":{"hello":"world","value":12},"array":[12,13,"hello",true],"node":"kestrel toolbox"}""",
            JsonParser.SerializeJson(value)
        );

        Assert.Equal(
            """
            {
              "object": {
                "hello": "world",
                "value": 12
              },
              "array": [
                12,
                13,
                "hello",
                true
              ],
              "node": "kestrel toolbox"
            }
            """.ReplaceLineEndings("\n"),
            JsonParser.SerializeJson(value, new JsonWriterOptions { Indented = true }).ReplaceLineEndings("\n")
        );

        value = new SystemTextJsonObjectClass
        {
            ObjectValue = null,
            ArrayValue = null,
            NodeValue = null,
        };
        Assert.Equal("""{"object":null,"node":null}""", JsonParser.SerializeJson(value));
    }

    public class SystemTextJsonObjectSkipSerializationClass
    {
        [Name("object")]
        [SkipSerialization(Condition.WhenNullOrEmpty)]
        public JsonObject? ObjectValue { get; set; }

        [Name("array")]
        [SkipSerialization(Condition.WhenNullOrEmpty)]
        public JsonArray? ArrayValue { get; set; }

        [Name("node")]
        [SkipSerialization(Condition.WhenNullOrEmpty)]
        public JsonNode? NodeValue { get; set; }
    }

    [Fact]
    public void SystemTextJsonSkipSerializationObject()
    {
        var value = new SystemTextJsonObjectSkipSerializationClass
        {
            ObjectValue = new JsonObject { ["hello"] = "world", ["value"] = 12 },
            ArrayValue = new JsonArray(12, 13, "hello", true),
            NodeValue = JsonNode.Parse("\"kestrel toolbox\""),
        };
        Assert.Equal(
            """{"object":{"hello":"world","value":12},"array":[12,13,"hello",true],"node":"kestrel toolbox"}""",
            JsonParser.SerializeJson(value)
        );

        value = new SystemTextJsonObjectSkipSerializationClass
        {
            ObjectValue = null,
            ArrayValue = null,
            NodeValue = null,
        };
        Assert.Equal("""{}""", JsonParser.SerializeJson(value));

        value = new SystemTextJsonObjectSkipSerializationClass
        {
            ObjectValue = new JsonObject(),
            ArrayValue = new JsonArray(),
            NodeValue = (JsonValue)string.Empty,
        };
        Assert.Equal("""{"node":""}""", JsonParser.SerializeJson(value));
    }

    public class StreamObject
    {
        [Name("bigLongNameThatIsTooLong")]
        public string? StringValue { get; set; }

        [Name("anotherNameThatIsWayTooLong")]
        public long IntValue { get; set; }

        [Name("bytes")]
        public byte[]? Bytes { get; set; }

        [Name("date")]
        public DateTime Date { get; set; }

        [Name("time")]
        public TimeSpan Time { get; set; }

        [Name("enum")]
        public TestEnum Enum { get; set; }
    }

    [Fact]
    public async Task TestStream()
    {
        var value = new StreamObject()
        {
            StringValue = "hello world how are you",
            IntValue = 6498463318,
            Bytes = Convert.FromBase64String("AAECAwQFBgcICQoLDA0ODw=="),
            Date = DateTimeOffset.Parse("2020-05-01T06:00:00.000Z", CultureInfo.InvariantCulture).UtcDateTime,
            Time = TimeSpanParser.Parse("20:30"),
            Enum = TestEnum.SuperLong,
        };

        using var stream = new MemoryStream();
        await JsonParser.SerializeJsonAsync(stream, value);

        var actual = Encoding.UTF8.GetString(stream.ToArray());
        Assert.Equal(
            """{"bigLongNameThatIsTooLong":"hello world how are you","anotherNameThatIsWayTooLong":6498463318,"bytes":"AAECAwQFBgcICQoLDA0ODw==","date":"2020-05-01T06:00:00.000Z","time":"20:30:00","enum":"superLongName"}""",
            actual
        );
    }

    public sealed class CustomSerializeClass
    {
        [Name("class")]
        [SkipSerialization(Condition.WhenNull)]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed class CustomValueClass
        {
            public int Value { get; set; }

            [JsonSerializableCustomParse]
            public static bool TryParseCustomValueClass(
                ref Utf8JsonReader reader,
                [NotNullWhen(true)] out CustomValueClass? value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType != JsonTokenType.Number)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueClass)}";
                    return false;
                }

                var readValue = reader.GetInt32();
                value = new CustomValueClass { Value = readValue };
                error = null;
                return true;
            }

            [JsonSerializableCustomSerialize]
            public static void Serialize(Utf8JsonWriter writer, CustomValueClass value)
            {
                writer.WriteNumberValue(value.Value);
            }
        }

        public struct CustomValueStruct
        {
            public bool Value { get; set; }

            [JsonSerializableCustomParse]
            public static bool TryParseCustomValueStruct(
                ref Utf8JsonReader reader,
                out CustomValueStruct value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType is not JsonTokenType.True and not JsonTokenType.False)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueStruct)}";
                    return false;
                }

                var readValue = reader.GetBoolean();
                value = new CustomValueStruct { Value = readValue };
                error = null;
                return true;
            }

            [JsonSerializableCustomSerialize]
            public static void Serialize(Utf8JsonWriter writer, CustomValueStruct value)
            {
                writer.WriteBooleanValue(value.Value);
            }
        }
    }

    [Fact]
    public void CustomSerializer()
    {
        var value = new CustomSerializeClass
        {
            Class = new CustomSerializeClass.CustomValueClass { Value = 22 },
            Struct = new CustomSerializeClass.CustomValueStruct { Value = true },
        };
        Assert.Equal("""{"class":22,"struct":true}""", JsonParser.SerializeJson(value));

        value = new CustomSerializeClass
        {
            Class = null,
            Struct = new CustomSerializeClass.CustomValueStruct { Value = false },
        };
        Assert.Equal("""{"struct":false}""", JsonParser.SerializeJson(value));
    }

    public sealed class InjectedCustomSerializeClass
    {
        [Name("class")]
        [SkipSerialization(Condition.WhenNull)]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed class CustomValueClass
        {
            public int Value { get; set; }

            public static void Serialize(Utf8JsonWriter writer, CustomValueClass value)
            {
                writer.WriteNumberValue(value.Value);
            }
        }

        public struct CustomValueStruct
        {
            public bool Value { get; set; }

            public static void Serialize(Utf8JsonWriter writer, CustomValueStruct value)
            {
                writer.WriteBooleanValue(value.Value);
            }
        }
    }

    [Fact]
    public void InjectedCustomSerializer()
    {
        var value = new InjectedCustomSerializeClass
        {
            Class = new InjectedCustomSerializeClass.CustomValueClass { Value = 22 },
            Struct = new InjectedCustomSerializeClass.CustomValueStruct { Value = true },
        };
        Assert.Equal("""{"class":22,"struct":true}""", JsonParser.SerializeJson(value));

        value = new InjectedCustomSerializeClass
        {
            Class = null,
            Struct = new InjectedCustomSerializeClass.CustomValueStruct { Value = false },
        };
        Assert.Equal("""{"struct":false}""", JsonParser.SerializeJson(value));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S101:Types should be named in PascalCase",
        Justification = "Test for exactly this."
    )]
    [SuppressMessage(
        "Naming",
        "CA1710:Identifiers should have correct suffix",
        Justification = "This is a test about it explicitly."
    )]
    public sealed class IDictionaryClass<TKey, TValue> : IDictionary<TKey, TValue>
        where TKey : notnull
    {
        private readonly Dictionary<TKey, TValue> dictionary = new();

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Add(KeyValuePair<TKey, TValue> item) => this.dictionary.Add(item.Key, item.Value);

        public void Clear() => this.dictionary.Clear();

        public bool Contains(KeyValuePair<TKey, TValue> item) => this.dictionary.Contains(item);

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => throw new NotSupportedException();

        public bool Remove(KeyValuePair<TKey, TValue> item) => this.dictionary.Remove(item.Key);

        public int Count => this.dictionary.Count;

        public bool IsReadOnly => false;

        public void Add(TKey key, TValue value) => this.dictionary.Add(key, value);

        public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

        public bool Remove(TKey key) => this.dictionary.Remove(key);

        public bool TryGetValue(TKey key, out TValue value) => this.dictionary.TryGetValue(key, out value!);

        public TValue this[TKey key]
        {
            get => this.dictionary[key];
            set => this.dictionary[key] = value;
        }

        public ICollection<TKey> Keys => this.dictionary.Keys;

        public ICollection<TValue> Values => this.dictionary.Values;
    }

    [Fact]
    public void IndividualInts()
    {
        Assert.Equal("12", JsonParser.SerializeJson(12));
        Assert.Equal("12", JsonParser.SerializeJson((int?)12));
        Assert.Equal("null", JsonParser.SerializeJson((int?)null));
    }

    [Fact]
    public void IndividualStrings()
    {
        Assert.Equal("null", JsonParser.SerializeJson((string)null!));
        Assert.Equal("\"hello\"", JsonParser.SerializeJson("hello"));
    }

    [Fact]
    public void IndividualDictionary()
    {
        Assert.Equal(
            """{"hello":"world","kestrel":"toolbox"}""",
            JsonParser.SerializeJson(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } })
        );

        Assert.Equal(
            """{"hello":1,"kestrel":32}""",
            JsonParser.SerializeJson(new Dictionary<string, int> { { "hello", 1 }, { "kestrel", 32 } })
        );

        Assert.Equal(
            """{"hello":1,"kestrel":32}""",
            JsonParser.SerializeJson(new IDictionaryClass<string, int> { { "hello", 1 }, { "kestrel", 32 } })
        );
    }

    [Fact]
    public void IndividualLists()
    {
        Assert.Equal("""["a","b","c"]""", JsonParser.SerializeJson(new[] { "a", "b", "c" }));
        Assert.Equal("""["a","b","c"]""", JsonParser.SerializeJson(new List<string> { "a", "b", "c" }));

        Assert.Equal("""[1,2,3]""", JsonParser.SerializeJson(new[] { 1, 2, 3 }));
        Assert.Equal("""[1,2,3]""", JsonParser.SerializeJson(new List<int> { 1, 2, 3 }));

        Assert.Equal("""[1,2,null,3]""", JsonParser.SerializeJson(new int?[] { 1, 2, null, 3 }));
        Assert.Equal("""[1,2,null,3]""", JsonParser.SerializeJson(new List<int?> { 1, 2, null, 3 }));
    }

    [Fact]
    public void IndividualEnumerable()
    {
        Assert.Equal(
            """["0","1","2"]""",
            JsonParser.SerializeJson(Enumerable.Range(0, 3).Select(i => i.ToString(CultureInfo.InvariantCulture)))
        );
        Assert.Equal("""[0,1,2]""", JsonParser.SerializeJson(Enumerable.Range(0, 3)));

        Assert.Equal("""[1,2,null,3]""", JsonParser.SerializeJson(new int?[] { 1, 2, null, 3 }.Select(i => i)));
        Assert.Equal("""[1,2,null,3]""", JsonParser.SerializeJson(new List<int?> { 1, 2, null, 3 }.Select(i => i)));

        Assert.Equal(
            """[{"hello":"world","kestrel":"toolbox","i":2}]""",
            JsonParser.SerializeJsonAnonymous(
                new[]
                {
                    new
                    {
                        hello = "world",
                        kestrel = "toolbox",
                        i = 2,
                    },
                }.Select(i => i)
            )
        );
    }

    [Fact]
    public void IndividualEnumerableKeyValuePair()
    {
        Assert.Equal(
            """{"hello":"world","kestrel":"toolbox"}""",
            JsonParser.SerializeJson(
                new KeyValuePair<string, string>[] { new("hello", "world"), new("kestrel", "toolbox") }
            )
        );

        Assert.Equal(
            """{"hello":1,"kestrel":2}""",
            JsonParser.SerializeJson(new KeyValuePair<string, int>[] { new("hello", 1), new("kestrel", 2) })
        );

        Assert.Equal(
            """{"hello":"world","kestrel":"toolbox"}""",
            JsonParser.SerializeJson(
                new KeyValuePair<string, string>[] { new("hello", "world"), new("kestrel", "toolbox") }.Select(p => p)
            )
        );

        Assert.Equal(
            """{"a":1,"b":2}""",
            JsonParser.SerializeJson(new KeyValuePair<string, int>[] { new("a", 1), new("b", 2) }.Select(p => p))
        );
    }

    [JsonSerializable]
    public sealed partial class TypedefJsonNullable
    {
        [Typedef(typeof(string), TypedefFeatures.All)]
        public readonly partial struct TypedefString { }

        [Typedef(typeof(int), TypedefFeatures.All)]
        public readonly partial struct TypedefInt { }

        [Typedef(typeof(InvalidValueEnumClass), TypedefFeatures.JsonSerializable)]
        public readonly partial struct TypedefComplex
        {
            /// <summary>
            /// Gets or sets some fake field.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <remarks>
            /// This field injects another struct member to test if nullable detection is working.
            /// </remarks>
            public int this[int index] => index;
        }

        [Typedef(typeof(CustomSerializeClass), TypedefFeatures.JsonSerializable)]
        public readonly partial struct TypedefCustom { }

        [Name("string")]
        public TypedefString? StringValue { get; set; }

        [Name("int")]
        public TypedefInt? IntValue { get; set; }

        [Name("complex")]
        public TypedefComplex? ComplexValue { get; set; }

        [Name("custom")]
        public TypedefCustom? CustomValue { get; set; }
    }

    [Fact]
    public void TestTypedefJsonNullable()
    {
        Assert.Equal(
            """{"string":"hello","int":3,"complex":{"value":"a"},"custom":{"class":22,"struct":true}}""",
            JsonParser.SerializeJson(
                new TypedefJsonNullable
                {
                    StringValue = (TypedefJsonNullable.TypedefString)"hello",
                    IntValue = (TypedefJsonNullable.TypedefInt)3,
                    ComplexValue = (TypedefJsonNullable.TypedefComplex)
                        new InvalidValueEnumClass { Value = InvalidValueEnum.A },
                    CustomValue = (TypedefJsonNullable.TypedefCustom)
                        new CustomSerializeClass
                        {
                            Class = new CustomSerializeClass.CustomValueClass { Value = 22 },
                            Struct = new CustomSerializeClass.CustomValueStruct { Value = true },
                        },
                }
            )
        );

        Assert.Equal(
            """{"string":null,"int":null,"complex":null,"custom":null}""",
            JsonParser.SerializeJson(new TypedefJsonNullable { })
        );
    }

    public sealed class TrimClass
    {
        [Name("value")]
        [TrimValue]
        public required string Value { get; set; }

        [Name("long")]
        [TrimValue(OnSerialize = false)]
        public required string Long { get; set; }

        [Name("null")]
        [TrimValue('a', 'b')]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public string? Null { get; set; }
    }

    [Fact]
    public void Trim()
    {
        Assert.Equal(
            """
            {"value":"hello","long":" whitespace "}
            """,
            JsonParser.SerializeJson(
                new TrimClass
                {
                    Value = "   hello   ",
                    Long = " whitespace ",
                    Null = null,
                }
            )
        );
    }

    public sealed class NullOnEmptyClass
    {
        [Name("value")]
        [NullOnEmpty(OnParse = false)]
        [TrimValue]
        public string? Value { get; set; }

        [Name("emptyOnly")]
        [NullOnEmpty]
        [SkipSerialization(Condition.WhenDefault)]
        public string? EmptyOnly { get; set; }

        [Name("skip")]
        [NullOnEmpty]
        [SkipSerialization(Condition.WhenNull)]
        public string? Skip { get; set; }
    }

    [Fact]
    public void NullOnEmptyTest()
    {
        Assert.Equal(
            """{"value":"hello","emptyOnly":"  world ","skip":"value"}""",
            JsonParser.SerializeJson(
                new NullOnEmptyClass
                {
                    Value = "  hello ",
                    EmptyOnly = "  world ",
                    Skip = "value",
                }
            )
        );
        Assert.Equal(
            """{"value":null,"emptyOnly":" "}""",
            JsonParser.SerializeJson(new NullOnEmptyClass { Value = " ", EmptyOnly = " " })
        );
        Assert.Equal(
            """{"value":null}""",
            JsonParser.SerializeJson(
                new NullOnEmptyClass
                {
                    Value = string.Empty,
                    EmptyOnly = string.Empty,
                    Skip = string.Empty,
                }
            )
        );
    }

    public sealed class SkipSerializationClass
    {
        [Name("default")]
        [SkipSerialization(Condition.WhenDefault)]
        [DefaultValue(30)]
        public int? DefaultInt { get; set; }

        [Name("null")]
        [SkipSerialization(Condition.WhenNull)]
        [DefaultValue(30)]
        public int? NullInt { get; set; }

        [Name("nullOrDefault")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        [DefaultValue(30)]
        public int? NullOrDefaultInt { get; set; }
    }

    [Fact]
    public void SkipSerialization()
    {
        var value = new SkipSerializationClass
        {
            DefaultInt = 30,
            NullInt = 30,
            NullOrDefaultInt = 30,
        };

        Assert.Equal("""{"null":30}""", JsonParser.SerializeJson(value));

        value = new SkipSerializationClass
        {
            DefaultInt = null,
            NullInt = null,
            NullOrDefaultInt = null,
        };

        Assert.Equal("""{"default":null}""", JsonParser.SerializeJson(value));

        value = new SkipSerializationClass
        {
            DefaultInt = 0,
            NullInt = 0,
            NullOrDefaultInt = 0,
        };

        Assert.Equal("""{"default":0,"null":0,"nullOrDefault":0}""", JsonParser.SerializeJson(value));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(InvalidValue = Unknown)]
    public enum InvalidValueEnum
    {
        [Name("a")]
        A,

        [Name("unknown")]
        Unknown,
    }

    public sealed class InvalidValueEnumClass
    {
        [Name("value")]
        public InvalidValueEnum Value { get; set; }
    }

    [Fact]
    public void TestInvalidValueEnum()
    {
        Assert.Equal("\"a\"", JsonParser.SerializeJson(InvalidValueEnum.A));
        Assert.Equal("\"unknown\"", JsonParser.SerializeJson(InvalidValueEnum.Unknown));
        Assert.Equal("\"unknown\"", JsonParser.SerializeJson((InvalidValueEnum)2000));
    }

    [Fact]
    public void ConcurrentBag()
    {
        var bag = new ConcurrentBag<string> { "hello", "world" };
        Assert.Equal($"[{string.Join(",", bag.Select(b => $"\"{b}\""))}]", JsonParser.SerializeJson(bag));
    }

    public sealed class CustomPropertySerializingClass
    {
        [Name("value")]
        [JsonSerializableCustomProperty(
            SerializeMethod = "UnitTests.Serialization.JsonSerializableSerializeTests.CustomPropertySerializingClass.SerializeInt"
        )]
        public required int? Value { get; set; }

        public static void SerializeInt(Utf8JsonWriter writer, int value)
        {
            writer.WriteStringValue($"0x{value:x}");
        }
    }

    [Fact]
    public void CustomPropertySerializing()
    {
        Assert.Equal(
            """{"value":"0xff"}""",
            JsonParser.SerializeJson(new CustomPropertySerializingClass { Value = 0xFF })
        );

        Assert.Equal(
            """{"value":null}""",
            JsonParser.SerializeJson(new CustomPropertySerializingClass { Value = null })
        );
    }

    [JsonSerializable]
    public sealed partial class SelfReferenceClass
    {
        [Name("value")]
        public string? Value { get; set; }
    }

    [Fact]
    public void SelfReferenceTest()
    {
        var value = new SelfReferenceClass { Value = "big" };
        Assert.Equal("""{"value":"big"}""", value.SerializeJson());
    }
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
