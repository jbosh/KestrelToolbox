// <copyright file="EnumSerializerTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Dynamic.Serialization.Helpers;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests.Serialization;

[EnumSerializable(typeof(RoslynLongTestEnum))]
[EnumSerializable(typeof(RoslynLongNumberTestEnum))]
public partial class EnumSerializerTests
{
    private readonly EnumSerializer enumSerializer;

    public EnumSerializerTests()
    {
        this.enumSerializer = new EnumSerializer();
        this.enumSerializer.AddParser<RoslynLongTestEnum>(TryParseEnum);
        this.enumSerializer.AddSerializer<RoslynLongTestEnum>(SerializeEnum);

        this.enumSerializer.AddParser<RoslynLongNumberTestEnum>(TryParseEnum);
        this.enumSerializer.AddSerializer<RoslynLongNumberTestEnum>(SerializeEnum);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    [Fact]
    public void StringParsing()
    {
        Assert.True(this.enumSerializer.TryParse("a", out TestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(TestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result, out error));
        Assert.Equal("'TestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(TestEnum.A, result);
    }

    [Fact]
    public void StringParsingWithoutError()
    {
        Assert.True(this.enumSerializer.TryParse("a", out TestEnum result));
        Assert.Equal(TestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result));
        Assert.Equal(TestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result));
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result));
        Assert.Equal(TestEnum.A, result);
    }

    [Fact]
    public void StringParsingNullable()
    {
        Assert.True(this.enumSerializer.TryParse("a", out TestEnum? result, out var error));
        Assert.Null(error);
        Assert.Equal(TestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result, out error));
        Assert.Equal("'TestEnum?' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Null(result);
    }

    [Fact]
    public void StringParsingNullableWithoutError()
    {
        Assert.True(this.enumSerializer.TryParse("a", out TestEnum? result));
        Assert.Equal(TestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result));
        Assert.Equal(TestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result));
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result));
        Assert.Null(result);
    }

    [Fact]
    public void StringSerializing()
    {
        Assert.Equal("a", this.enumSerializer.Serialize(TestEnum.A));
        Assert.Equal("b", this.enumSerializer.Serialize(TestEnum.B));
        Assert.Equal("none", this.enumSerializer.Serialize(TestEnum.None));
        Assert.Equal("superLongName", this.enumSerializer.Serialize(TestEnum.SuperLong));
        Assert.Equal("15", this.enumSerializer.Serialize((TestEnum)15));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(
        SerializationType = EnumSerializableSettingsSerializationType.Numbers,
        AllowAnyNumber = false
    )]
    public enum TestNumberEnum
    {
        A = 0,
        B = 1,
        None = 17,
        SuperLong = 12345,
    }

    [Fact]
    public void NumberParsing()
    {
        Assert.True(this.enumSerializer.TryParse("0", out TestNumberEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("1", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("17", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("12345", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("-12", out result, out error));
        Assert.Equal("'TestNumberEnum' must be one of: 0, 1, 17, 12345.", error);
        Assert.Equal(TestNumberEnum.A, result);
    }

    [Fact]
    public void NumberSerialization()
    {
        Assert.Equal("0", this.enumSerializer.Serialize(TestNumberEnum.A));
        Assert.Equal("1", this.enumSerializer.Serialize(TestNumberEnum.B));
        Assert.Equal("17", this.enumSerializer.Serialize(TestNumberEnum.None));
        Assert.Equal("12345", this.enumSerializer.Serialize(TestNumberEnum.SuperLong));
        Assert.Equal("15", this.enumSerializer.Serialize((TestNumberEnum)15));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(InvalidValue = Unknown)]
    public enum InvalidValueEnum
    {
        [Name("a")]
        A,

        [Name("unknown")]
        Unknown,
    }

    [Fact]
    public void InvalidValueParsing()
    {
        Assert.True(this.enumSerializer.TryParse("a", out InvalidValueEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(InvalidValueEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("unknown", out result, out error));
        Assert.Null(error);
        Assert.Equal(InvalidValueEnum.Unknown, result);

        Assert.True(this.enumSerializer.TryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(InvalidValueEnum.Unknown, result);
    }

    [Fact]
    public void InvalidValueSerialization()
    {
        Assert.Equal("a", this.enumSerializer.Serialize(InvalidValueEnum.A));
        Assert.Equal("unknown", this.enumSerializer.Serialize(InvalidValueEnum.Unknown));
        Assert.Equal("unknown", this.enumSerializer.Serialize((InvalidValueEnum)12));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings]
    public enum LongTestEnum : long
    {
        [Name("a")]
        A = -13,

        [Name("b", "alt")]
        B = 287914,

        [Name("none")]
        None = 2005750020805,

        [Name("superLongName")]
        SuperLong = long.MaxValue,
    }

    [Fact(Skip = "Long is unsupported.")]
    public void LongEnum()
    {
        Assert.True(this.enumSerializer.TryParse("a", out LongTestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result, out error));
        Assert.Equal("'LongTestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(0, (long)result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(
        SerializationType = EnumSerializableSettingsSerializationType.Numbers,
        AllowAnyNumber = false
    )]
    public enum LongNumberTestEnum : long
    {
        A = -13,
        B = 287914,
        None = 2005750020805,
        SuperLong = long.MaxValue,
    }

    [Fact(Skip = "Long is unsupported.")]
    public void LongNumberEnum()
    {
        Assert.True(this.enumSerializer.TryParse("-13", out LongNumberTestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("287914", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("2005750020805", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("9223372036854775807", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("45", out result, out error));
        Assert.Equal("'LongNumberTestEnum' must be one of: -13, 287914, 2005750020805, 9223372036854775807.", error);
        Assert.Equal(0, (long)result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestNamedEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    [Fact]
    public void NamedStringParsing()
    {
        Assert.True(this.enumSerializer.TryParse<TestNamedEnum>("a", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result, out error));
        Assert.Equal("'TestNamedEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(TestNamedEnum.A, result);
    }

    [Fact]
    public void NamedStringParsingWithoutError()
    {
        Assert.True(this.enumSerializer.TryParse<TestNamedEnum>("a", out var result));
        Assert.Equal(TestNamedEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result));
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result));
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result));
        Assert.Equal(TestNamedEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result));
        Assert.Equal(TestNamedEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result));
        Assert.Equal(TestNamedEnum.A, result);
    }

    [Fact]
    public void NamedStringSerializing()
    {
        Assert.Equal("a", this.enumSerializer.Serialize(TestNamedEnum.A));
        Assert.Equal("b", this.enumSerializer.Serialize(TestNamedEnum.B));
        Assert.Equal("none", this.enumSerializer.Serialize(TestNamedEnum.None));
        Assert.Equal("superLongName", this.enumSerializer.Serialize(TestNamedEnum.SuperLong));
        Assert.Equal("15", this.enumSerializer.Serialize((TestNamedEnum)15));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings]
    public enum RoslynLongTestEnum : long
    {
        [Name("a")]
        A = -13,

        [Name("b", "alt")]
        B = 287914,

        [Name("none")]
        None = 2005750020805,

        [Name("superLongName")]
        SuperLong = long.MaxValue,
    }

    [Fact]
    public void RoslynLongEnum()
    {
        Assert.True(this.enumSerializer.TryParse("a", out RoslynLongTestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(RoslynLongTestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongTestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongTestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongTestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongTestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("invalid", out result, out error));
        Assert.Equal("'RoslynLongTestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(0, (long)result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(
        SerializationType = EnumSerializableSettingsSerializationType.Numbers,
        AllowAnyNumber = false
    )]
    public enum RoslynLongNumberTestEnum : long
    {
        A = -13,
        B = 287914,
        None = 2005750020805,
        SuperLong = long.MaxValue,
    }

    [Fact]
    public void RoslynLongNumberEnum()
    {
        Assert.True(this.enumSerializer.TryParse("-13", out RoslynLongNumberTestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(RoslynLongNumberTestEnum.A, result);

        Assert.True(this.enumSerializer.TryParse("287914", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongNumberTestEnum.B, result);

        Assert.True(this.enumSerializer.TryParse("2005750020805", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongNumberTestEnum.None, result);

        Assert.True(this.enumSerializer.TryParse("9223372036854775807", out result, out error));
        Assert.Null(error);
        Assert.Equal(RoslynLongNumberTestEnum.SuperLong, result);

        Assert.False(this.enumSerializer.TryParse("45", out result, out error));
        Assert.Equal(
            "'RoslynLongNumberTestEnum' must be one of: -13, 287914, 2005750020805, 9223372036854775807.",
            error
        );
        Assert.Equal(0, (long)result);
    }
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
