// <copyright file="EnumSerializableTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;

namespace UnitTests.Serialization;

#if !DISABLE_ROSLYN_GENERATED_TESTS

[EnumSerializable(typeof(TestEnum), typeof(TestEnum?))]
public partial class EnumSerializableTests { }

[EnumSerializable(typeof(TestNumberEnum))]
[EnumSerializable(typeof(InvalidValueEnum))]
[EnumSerializable(typeof(LongTestEnum))]
[EnumSerializable(typeof(LongNumberTestEnum))]
[EnumSerializable(
    typeof(TestNamedEnum),
    ParsingMethodName = "TestNamedEnumTryParse",
    SerializingMethodName = "TestNamedEnumSerialize"
)]
public partial class EnumSerializableTests
{
    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    [Fact]
    public void StringParsing()
    {
        Assert.True(TryParseEnum("a", out TestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(TestEnum.A, result);

        Assert.True(TryParseEnum("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.None, result);

        Assert.True(TryParseEnum("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(TryParseEnum("invalid", out result, out error));
        Assert.Equal("'TestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(TestEnum.A, result);
    }

    [Fact]
    public void StringParsingWithoutError()
    {
        Assert.True(TryParseEnum("a", out TestEnum result));
        Assert.Equal(TestEnum.A, result);

        Assert.True(TryParseEnum("b", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("alt", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("none", out result));
        Assert.Equal(TestEnum.None, result);

        Assert.True(TryParseEnum("superLongName", out result));
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(TryParseEnum("invalid", out result));
        Assert.Equal(TestEnum.A, result);
    }

    [Fact]
    public void StringParsingNullable()
    {
        Assert.True(TryParseEnum("a", out TestEnum? result, out var error));
        Assert.Null(error);
        Assert.Equal(TestEnum.A, result);

        Assert.True(TryParseEnum("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.None, result);

        Assert.True(TryParseEnum("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(TryParseEnum("invalid", out result, out error));
        Assert.Equal("'TestEnum?' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Null(result);
    }

    [Fact]
    public void StringParsingNullableWithoutError()
    {
        Assert.True(TryParseEnum("a", out TestEnum? result));
        Assert.Equal(TestEnum.A, result);

        Assert.True(TryParseEnum("b", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("alt", out result));
        Assert.Equal(TestEnum.B, result);

        Assert.True(TryParseEnum("none", out result));
        Assert.Equal(TestEnum.None, result);

        Assert.True(TryParseEnum("superLongName", out result));
        Assert.Equal(TestEnum.SuperLong, result);

        Assert.False(TryParseEnum("invalid", out result));
        Assert.Null(result);
    }

    [Fact]
    public void StringSerializing()
    {
        Assert.Equal("a", SerializeEnum(TestEnum.A));
        Assert.Equal("b", SerializeEnum(TestEnum.B));
        Assert.Equal("none", SerializeEnum(TestEnum.None));
        Assert.Equal("superLongName", SerializeEnum(TestEnum.SuperLong));
        Assert.Equal("15", SerializeEnum((TestEnum)15));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(
        SerializationType = EnumSerializableSettingsSerializationType.Numbers,
        AllowAnyNumber = false
    )]
    public enum TestNumberEnum
    {
        A = 0,
        B = 1,
        None = 17,
        SuperLong = 12345,
    }

    [Fact]
    public void NumberParsing()
    {
        Assert.True(TryParseEnum("0", out TestNumberEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.A, result);

        Assert.True(TryParseEnum("1", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.B, result);

        Assert.True(TryParseEnum("17", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.None, result);

        Assert.True(TryParseEnum("12345", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.SuperLong, result);

        Assert.False(TryParseEnum("-12", out result, out error));
        Assert.Equal("'TestNumberEnum' must be one of: 0, 1, 17, 12345.", error);
        Assert.Equal(TestNumberEnum.A, result);
    }

    [Fact]
    public void NumberSerialization()
    {
        Assert.Equal("0", SerializeEnum(TestNumberEnum.A));
        Assert.Equal("1", SerializeEnum(TestNumberEnum.B));
        Assert.Equal("17", SerializeEnum(TestNumberEnum.None));
        Assert.Equal("12345", SerializeEnum(TestNumberEnum.SuperLong));
        Assert.Equal("15", SerializeEnum((TestNumberEnum)15));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(InvalidValue = Unknown)]
    public enum InvalidValueEnum
    {
        [Name("a")]
        A,

        [Name("unknown")]
        Unknown,
    }

    [Fact]
    public void InvalidValueParsing()
    {
        Assert.True(TryParseEnum("a", out InvalidValueEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(InvalidValueEnum.A, result);

        Assert.True(TryParseEnum("unknown", out result, out error));
        Assert.Null(error);
        Assert.Equal(InvalidValueEnum.Unknown, result);

        Assert.True(TryParseEnum("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(InvalidValueEnum.Unknown, result);
    }

    [Fact]
    public void InvalidValueSerialization()
    {
        Assert.Equal("a", SerializeEnum(InvalidValueEnum.A));
        Assert.Equal("unknown", SerializeEnum(InvalidValueEnum.Unknown));
        Assert.Equal("unknown", SerializeEnum((InvalidValueEnum)12));
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings]
    public enum LongTestEnum : long
    {
        [Name("a")]
        A = -13,

        [Name("b", "alt")]
        B = 287914,

        [Name("none")]
        None = 2005750020805,

        [Name("superLongName")]
        SuperLong = long.MaxValue,
    }

    [Fact]
    public void LongEnum()
    {
        Assert.True(TryParseEnum("a", out LongTestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.A, result);

        Assert.True(TryParseEnum("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.B, result);

        Assert.True(TryParseEnum("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.B, result);

        Assert.True(TryParseEnum("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.None, result);

        Assert.True(TryParseEnum("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongTestEnum.SuperLong, result);

        Assert.False(TryParseEnum("invalid", out result, out error));
        Assert.Equal("'LongTestEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(0, (long)result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(
        SerializationType = EnumSerializableSettingsSerializationType.Numbers,
        AllowAnyNumber = false
    )]
    public enum LongNumberTestEnum : long
    {
        A = -13,
        B = 287914,
        None = 2005750020805,
        SuperLong = long.MaxValue,
    }

    [Fact]
    public void LongNumberEnum()
    {
        Assert.True(TryParseEnum("-13", out LongNumberTestEnum result, out var error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.A, result);

        Assert.True(TryParseEnum("287914", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.B, result);

        Assert.True(TryParseEnum("2005750020805", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.None, result);

        Assert.True(TryParseEnum("9223372036854775807", out result, out error));
        Assert.Null(error);
        Assert.Equal(LongNumberTestEnum.SuperLong, result);

        Assert.False(TryParseEnum("45", out result, out error));
        Assert.Equal("'LongNumberTestEnum' must be one of: -13, 287914, 2005750020805, 9223372036854775807.", error);
        Assert.Equal(0, (long)result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestNamedEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    [Fact]
    public void NamedStringParsing()
    {
        Assert.True(TestNamedEnumTryParse("a", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.A, result);

        Assert.True(TestNamedEnumTryParse("b", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(TestNamedEnumTryParse("alt", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(TestNamedEnumTryParse("none", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.None, result);

        Assert.True(TestNamedEnumTryParse("superLongName", out result, out error));
        Assert.Null(error);
        Assert.Equal(TestNamedEnum.SuperLong, result);

        Assert.False(TestNamedEnumTryParse("invalid", out result, out error));
        Assert.Equal("'TestNamedEnum' must be one of: a, alt, b, none, superLongName.", error);
        Assert.Equal(TestNamedEnum.A, result);
    }

    [Fact]
    public void NamedStringParsingWithoutError()
    {
        Assert.True(TestNamedEnumTryParse("a", out var result));
        Assert.Equal(TestNamedEnum.A, result);

        Assert.True(TestNamedEnumTryParse("b", out result));
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(TestNamedEnumTryParse("alt", out result));
        Assert.Equal(TestNamedEnum.B, result);

        Assert.True(TestNamedEnumTryParse("none", out result));
        Assert.Equal(TestNamedEnum.None, result);

        Assert.True(TestNamedEnumTryParse("superLongName", out result));
        Assert.Equal(TestNamedEnum.SuperLong, result);

        Assert.False(TestNamedEnumTryParse("invalid", out result));
        Assert.Equal(TestNamedEnum.A, result);
    }

    [Fact]
    public void NamedStringSerializing()
    {
        Assert.Equal("a", TestNamedEnumSerialize(TestNamedEnum.A));
        Assert.Equal("b", TestNamedEnumSerialize(TestNamedEnum.B));
        Assert.Equal("none", TestNamedEnumSerialize(TestNamedEnum.None));
        Assert.Equal("superLongName", TestNamedEnumSerialize(TestNamedEnum.SuperLong));
        Assert.Equal("15", TestNamedEnumSerialize((TestNamedEnum)15));
    }
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
