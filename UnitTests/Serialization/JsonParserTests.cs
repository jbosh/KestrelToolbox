// <copyright file="JsonParserTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections;
using System.Collections.Frozen;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO.Pipelines;
using System.Reflection;
using System.Reflection.Emit;
using System.Text.Json;
using System.Text.Json.Nodes;
using KestrelToolbox.Dynamic.Serialization.Helpers;
using KestrelToolbox.Extensions.PipeReaderExtensions;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Serialization.DataAnnotations.Serialization;
using KestrelToolbox.Types;
using Newtonsoft.Json.Linq;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests.Serialization;

[SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Tests describe what things are.")]
public sealed partial class JsonParserTests
{
    private readonly JsonParser jsonParser;

    public JsonParserTests()
    {
        this.jsonParser = new();
        this.jsonParser.AddCustomParser<CustomParsingNumberEnum>(CustomParsingEnumClass.TryParseNumberValue);
        this.jsonParser.AddCustomParser<CustomParsingStringEnum>(CustomParsingEnumSerialization.DoStringParsing);
        this.jsonParser.AddCustomParser<CustomInjectedParseClass.CustomValueClass>(
            CustomInjectedParseClass.CustomValueClass.TryParse
        );
        this.jsonParser.AddCustomParser<CustomInjectedParseClass.CustomValueStruct>(
            CustomInjectedParseClass.CustomValueStruct.TryParse
        );
        this.jsonParser.AddCustomParser<JArray>(NewtonsoftJObjectClass.TryParseJArray);
        this.jsonParser.AddCustomParser<JObject>(NewtonsoftJObjectClass.TryParseJObject);
        this.jsonParser.AddCustomParser<JToken>(NewtonsoftJObjectClass.TryParseJToken);
    }

    private sealed class BoolClass
    {
        [Name("value")]
        public bool Value { get; set; }

        [Name("nullable")]
        public bool? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(true)]
        public bool Default { get; set; }
    }

    [Fact]
    public void Bool()
    {
        Assert.True(
            this.jsonParser.TryParse<BoolClass>("""{"value": true, "nullable": false}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Value);
        Assert.True(result.Default);
        Assert.False(result.Nullable);

        Assert.True(
            this.jsonParser.TryParse("""{"value": false, "nullable": true, "default": false}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.False(result.Default);
        Assert.True(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.True(result.Default);
        Assert.Null(result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": "true"}""", out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 1}""", out result, out error));
        Assert.Equal("'value' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": "true"}""", out result, out error));
        Assert.Equal("'nullable' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": 1}""", out result, out error));
        Assert.Equal("'nullable' was not of type bool.", error);
        Assert.Null(result);

        Assert.True(this.jsonParser.TryParse("""{"nullable2": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.True(result.Default);
        Assert.Null(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value678": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Value);
        Assert.True(result.Default);
        Assert.Null(result.Nullable);
    }

    private sealed class Int32Class
    {
        [Name("value")]
        [ValidRange(-20, 30)]
        public int Value { get; set; }

        [Name("nullable")]
        [ValidRange(-2000, 40)]
        public int? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(22)]
        [ValidRange(0, 2_000_000)]
        public int Default { get; set; }

        [Name("valid")]
        [ValidValues(22, 24, 2001, 100)]
        public int Valid { get; set; }
    }

    [Fact]
    public void Int32()
    {
        Assert.True(
            this.jsonParser.TryParse<Int32Class>(
                """{"value": 14, "nullable": 32, "valid": 24}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Equal(32, result.Nullable);
        Assert.Equal(24, result.Valid);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": 12, "nullable": -1993, "default": 188372, "valid": 22}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(188372, result.Default);
        Assert.Equal(-1993, result.Nullable);
        Assert.Equal(22, result.Valid);

        Assert.True(this.jsonParser.TryParse("""{"value": -20}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-20, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);
        Assert.Equal(0, result.Valid);

        Assert.True(this.jsonParser.TryParse("""{"value": 30, "default": 2000000}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(30, result.Value);
        Assert.Equal(2000000, result.Default);
        Assert.Null(result.Nullable);
        Assert.Equal(0, result.Valid);

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": "true"}""", out result, out error));
        Assert.Equal("'value' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 22.3}""", out result, out error));
        Assert.Equal("'value' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": "74"}""", out result, out error));
        Assert.Equal("'nullable' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": 74}""", out result, out error));
        Assert.Equal("'nullable' was not in the range [-2000..40]", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": 1001848720002704720027}""", out result, out error));
        Assert.Equal("'nullable' was not of type int32.", error);
        Assert.Null(result);

        Assert.True(this.jsonParser.TryParse("""{"nullable2": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value678": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0, result.Value);
        Assert.Equal(22, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": -22}""", out result, out error));
        Assert.Equal("'value' was not in the range [-20..30]", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"default": -1}""", out result, out error));
        Assert.Equal("'default' was not in the range [0..2000000]", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"valid": 23}""", out result, out error));
        Assert.Equal("'valid' must be one of: 22, 24, 100, 2001.", error);
        Assert.Null(result);
    }

    private sealed class UInt32Class
    {
        [Name("value")]
        [ValidRange(5, 30)]
        public uint Value { get; set; }

        [Name("nullable")]
        [ValidRange(30, 2000)]
        public uint? Nullable { get; set; }

        [Name("default")]
        [DefaultValue(22)]
        [ValidRange(0, 2_000_000)]
        public uint Default { get; set; }

        [Name("valid")]
        [ValidValues(22, 24, 110)]
        public uint Valid { get; set; }
    }

    [Fact]
    public void UInt32()
    {
        Assert.True(
            this.jsonParser.TryParse<UInt32Class>(
                """{"value": 14, "nullable": 32, "valid": 24}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Equal(32U, result.Nullable);
        Assert.Equal(24U, result.Valid);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": 12, "nullable": 1993, "default": 188372, "valid": 22}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12U, result.Value);
        Assert.Equal(188372U, result.Default);
        Assert.Equal(1993U, result.Nullable);
        Assert.Equal(22U, result.Valid);

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": "true"}""", out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 22.3}""", out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": "74"}""", out result, out error));
        Assert.Equal("'nullable' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"nullable": 1001848720002704720027}""", out result, out error));
        Assert.Equal("'nullable' was not of type uint32.", error);
        Assert.Null(result);

        Assert.True(this.jsonParser.TryParse("""{"nullable2": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value678": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0U, result.Value);
        Assert.Equal(22U, result.Default);
        Assert.Null(result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": -22}""", out result, out error));
        Assert.Equal("'value' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"default": -1}""", out result, out error));
        Assert.Equal("'default' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"valid": 23}""", out result, out error));
        Assert.Equal("'valid' must be one of: 22, 24, 110.", error);
        Assert.Null(result);
    }

    private sealed class IntegerTypesClass
    {
        [Name("byte")]
        public byte Byte { get; set; }

        [Name("ushort")]
        public ushort UInt16 { get; set; }

        [Name("uint")]
        public uint UInt32 { get; set; }

        [Name("ulong")]
        public ulong UInt64 { get; set; }

        [Name("sbyte")]
        public sbyte SByte { get; set; }

        [Name("short")]
        public short Int16 { get; set; }

        [Name("int")]
        public int Int32 { get; set; }

        [Name("long")]
        public long Int64 { get; set; }
    }

    [Fact]
    public void IntegerTypes()
    {
        Assert.True(
            this.jsonParser.TryParse<IntegerTypesClass>(
                """{"byte": 14, "ushort": 32, "uint": 24, "ulong": 38082007040}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.Byte);
        Assert.Equal(32, result.UInt16);
        Assert.Equal(24U, result.UInt32);
        Assert.Equal(38082007040UL, result.UInt64);

        Assert.False(this.jsonParser.TryParse("""{"byte": -2}""", out result, out error));
        Assert.Equal("'byte' was not of type uint8.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"ushort": -2}""", out result, out error));
        Assert.Equal("'ushort' was not of type uint16.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"uint": -2}""", out result, out error));
        Assert.Equal("'uint' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"ulong": -2}""", out result, out error));
        Assert.Equal("'ulong' was not of type uint64.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"byte": 256}""", out result, out error));
        Assert.Equal("'byte' was not of type uint8.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"ushort": 70000}""", out result, out error));
        Assert.Equal("'ushort' was not of type uint16.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"uint": 4668888664668}""", out result, out error));
        Assert.Equal("'uint' was not of type uint32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"ulong": 69898989484684684684384349684}""", out result, out error));
        Assert.Equal("'ulong' was not of type uint64.", error);
        Assert.Null(result);

        Assert.True(
            this.jsonParser.TryParse(
                """{"sbyte": 14, "short": 32, "int": 24, "long": 38082007040}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(14, result.SByte);
        Assert.Equal(32, result.Int16);
        Assert.Equal(24, result.Int32);
        Assert.Equal(38082007040L, result.Int64);

        Assert.True(
            this.jsonParser.TryParse(
                """{"sbyte": -14, "short": -32, "int": -24, "long": -38082007040}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-14, result.SByte);
        Assert.Equal(-32, result.Int16);
        Assert.Equal(-24, result.Int32);
        Assert.Equal(-38082007040L, result.Int64);

        Assert.False(this.jsonParser.TryParse("""{"sbyte": -200}""", out result, out error));
        Assert.Equal("'sbyte' was not of type int8.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"short": -40000}""", out result, out error));
        Assert.Equal("'short' was not of type int16.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": -2147483649}""", out result, out error));
        Assert.Equal("'int' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"long": -984898116816838138138138}""", out result, out error));
        Assert.Equal("'long' was not of type int64.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"sbyte": 200}""", out result, out error));
        Assert.Equal("'sbyte' was not of type int8.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"short": 40000}""", out result, out error));
        Assert.Equal("'short' was not of type int16.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": 4668888664668}""", out result, out error));
        Assert.Equal("'int' was not of type int32.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"long": 69898989484684684684384349684}""", out result, out error));
        Assert.Equal("'long' was not of type int64.", error);
        Assert.Null(result);
    }

    private sealed class DecimalTypesClass
    {
        [Name("float")]
        public float Float { get; set; }

        [Name("double")]
        public double Double { get; set; }

        [Name("decimal")]
        public decimal Decimal { get; set; }
    }

    [Fact]
    public void DecimalTypes()
    {
        Assert.True(
            this.jsonParser.TryParse<DecimalTypesClass>(
                """{"float": 2, "double": 409, "decimal": 8200}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2.0f, result.Float);
        Assert.Equal(409.0, result.Double);
        Assert.Equal(8200, result.Decimal);

        Assert.True(
            this.jsonParser.TryParse(
                """{"float": 2.48, "double": 409.46684, "decimal": 820055.1668863313}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2.48f, result.Float);
        Assert.Equal(409.46684, result.Double);
        Assert.Equal(820055.1668863313M, result.Decimal);

        Assert.True(
            this.jsonParser.TryParse(
                """{"float": -2.48, "double": -409.46684, "decimal": -820055.1668863313}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(-2.48f, result.Float);
        Assert.Equal(-409.46684, result.Double);
        Assert.Equal(-820055.1668863313M, result.Decimal);

        Assert.True(
            this.jsonParser.TryParse(
                """{"float": 9864684668338734.168616838, "double": 986834384436688631.16864, "decimal": 79228162514264337593543950335}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(9.864685E+15F, result.Float);
        Assert.Equal(9.8683438443668864E+17, result.Double);
        Assert.Equal(79228162514264337593543950335M, result.Decimal);

        Assert.True(
            this.jsonParser.TryParse(
                """{"float":9.864685E+15,"double":9.868343844366886E+17,"decimal":-79228162514264337593543950335}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(9.864685E+15F, result.Float);
        Assert.Equal(9.8683438443668864E+17, result.Double);
        Assert.Equal(-79228162514264337593543950335M, result.Decimal);

        Assert.True(
            this.jsonParser.TryParse("""{"decimal": 79228162514264337593543950334.499881384}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0.0f, result.Float);
        Assert.Equal(0.0, result.Double);
        Assert.Equal(79228162514264337593543950334.499881384M, result.Decimal);
    }

    private sealed class StringClass
    {
        [Name("value")]
        public string? Value { get; set; }

        [Name("valid")]
        [ValidValues("hello", "world", "invalid")]
        public string? Valid { get; set; }

        [Name("default")]
        [DefaultValue("some string")]
        public string Default { get; set; } = "unused default";

        [Name("limited")]
        [StringLength(2, 4)]
        public string? Limited { get; set; }

        [Name("limited2")]
        [StringLength(2, 4)]
        public string? Limited2 { get; set; }

        [Name("implicitMax")]
        [StringLength(Minimum = 2)]
        public string? ImplicitMax { get; set; }
    }

    [Fact]
    public void Strings()
    {
        Assert.True(
            this.jsonParser.TryParse<StringClass>(
                """{"value": "whatever", "valid": "invalid", "default": "8200", "limited": "he", "limited2": "he", "implicitMax": "boo"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("whatever", result.Value);
        Assert.Equal("invalid", result.Valid);
        Assert.Equal("8200", result.Default);
        Assert.Equal("he", result.Limited);
        Assert.Equal("he", result.Limited2);
        Assert.Equal("boo", result.ImplicitMax);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": "extra", "limited": "hell", "limited2": "hell"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("extra", result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Equal("hell", result.Limited);
        Assert.Equal("hell", result.Limited2);

        Assert.False(this.jsonParser.TryParse("""{"limited": "1"}""", out result, out error));
        Assert.Equal("'limited' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"limited": "12345"}""", out result, out error));
        Assert.Equal("'limited' must be shorter than 4 characters.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"limited2": "1"}""", out result, out error));
        Assert.Equal("'limited2' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"implicitMax": "1"}""", out result, out error));
        Assert.Equal("'implicitMax' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"limited2": "12345"}""", out result, out error));
        Assert.Equal("'limited2' must be shorter than 4 characters.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"valid": "22"}""", out result, out error));
        Assert.Equal("'valid' not an allowed value. It must be one of: hello, invalid, world.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 22}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);

        Assert.True(this.jsonParser.TryParse("""{"value": null}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Value);
        Assert.Null(result.Valid);
        Assert.Equal("some string", result.Default);
        Assert.Null(result.Limited);
        Assert.Null(result.Limited2);
    }

    public sealed class DateTimeClass
    {
        [Name("dateTime")]
        [DefaultValue(typeof(DateTime), "June 17, 1975")]
        public DateTime DateTime { get; set; }

        [Name("dateTimeOffset")]
        [DefaultValue(typeof(DateTimeOffset), "2024-05-25T04:13:22Z")]
        public DateTimeOffset DateTimeOffset { get; set; }

        [Name("dateTimeNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public DateTime? DateTimeNullable { get; set; }

        [Name("dateTimeOffsetNullable")]
        [SkipSerialization(Condition.WhenNullOrDefault)]
        public DateTimeOffset? DateTimeOffsetNullable { get; set; }
    }

    [Fact]
    public void DateTimeTest()
    {
        Assert.True(
            this.jsonParser.TryParse<DateTimeClass>(
                """{"dateTime":"2010-05-03T00:00:00.000Z","dateTimeOffset":"2021-07-08T15:55:34.000Z","dateTimeNullable":"2010-05-03T00:00:00.000Z","dateTimeOffsetNullable":"2021-07-08T15:55:34.000Z"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("2010-05-03T00:00:00.000Z", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTime.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
        Assert.Equal(DateTime.Parse("2010-05-03T00:00:00.000Z", CultureInfo.InvariantCulture), result.DateTimeNullable);
        Assert.Equal(
            DateTime.Parse("2021-07-08T15:55:34Z", CultureInfo.InvariantCulture),
            result.DateTimeOffsetNullable
        );

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateTime.Parse("June 17, 1975", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTimeOffset.Parse("2024-05-25T04:13:22Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
        Assert.Null(result.DateTimeNullable);
        Assert.Null(result.DateTimeOffsetNullable);

        Assert.False(this.jsonParser.TryParse("""{"dateTime": "junuary"}""", out result, out error));
        Assert.Equal("'dateTime' was not of type DateTime.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"dateTime": 17}""", out result, out error));
        Assert.Equal("'dateTime' was not of type DateTime.", error);
        Assert.Null(result);
    }

    private sealed class DateOnlyClass
    {
        [Name("value")]
        [DateOnlyParseSettings(Format = "dd MMM yyyy")]
        public DateOnly Value { get; set; }

        [Name("value2")]
        public DateOnly Value2 { get; set; }

        [Name("default")]
        [DefaultValue(typeof(DateOnly), "2023-08-12")]
        public DateOnly Default { get; set; }

        [Name("nullable")]
        public DateOnly? Nullable { get; set; }
    }

    [Fact]
    public void DateOnlyTest()
    {
        Assert.True(
            this.jsonParser.TryParse<DateOnlyClass>(
                """{"value": "03 May 2010", "value2": "2021-07-08"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.ParseExact("03 May 2010", "dd MMM yyyy", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("2021-07-08", CultureInfo.InvariantCulture), result.Value2);
        Assert.Equal(DateOnly.ParseExact("2023-08-12", "o", CultureInfo.InvariantCulture), result.Default);

        Assert.True(
            this.jsonParser.TryParse("""{"value": "03 May 2010", "value2": "03 May 2010"}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(DateOnly.ParseExact("03 May 2010", "dd MMM yyyy", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(DateOnly.Parse("03 May 2010", CultureInfo.InvariantCulture), result.Value2);

        Assert.False(this.jsonParser.TryParse("""{"value": "junuary"}""", out result, out error));
        Assert.Equal("'value' was not of type DateOnly.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 17}""", out result, out error));
        Assert.Equal("'value' was not of type DateOnly.", error);
        Assert.Null(result);
    }

    private sealed class TimeOnlyClass
    {
        [Name("value")]
        public TimeOnly Value { get; set; }

        [Name("default")]
        [DefaultValue(typeof(TimeOnly), "11:03 am")]
        public TimeOnly Default { get; set; }
    }

    [Fact]
    public void TimeOnlyTest()
    {
        Assert.True(
            this.jsonParser.TryParse<TimeOnlyClass>(
                """{"value": "14:35", "default": "8:02am"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("14:35", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("8:02am", CultureInfo.InvariantCulture), result.Default);

        Assert.True(this.jsonParser.TryParse("""{"value": "10:45"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeOnly.Parse("10:45", CultureInfo.InvariantCulture), result.Value);
        Assert.Equal(TimeOnly.Parse("11:03am", CultureInfo.InvariantCulture), result.Default);

        Assert.False(this.jsonParser.TryParse("""{"value": "junuary"}""", out result, out error));
        Assert.Equal("'value' was not of type TimeOnly.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 17}""", out result, out error));
        Assert.Equal("'value' was not of type TimeOnly.", error);
        Assert.Null(result);
    }

    private sealed class TimeSpanClass
    {
        [Name("value")]
        [SkipSerialization(Condition.WhenDefault)]
        public TimeSpan Value { get; set; }

        [Name("nullable")]
        [SkipSerialization(Condition.WhenNull | Condition.WhenDefault)]
        public TimeSpan? Nullable { get; set; }
    }

    [Fact]
    public void TimeSpanTest()
    {
        Assert.True(
            this.jsonParser.TryParse<TimeSpanClass>(
                """{"value": "20s", "nullable": "30s"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("20s"), result.Value);
        Assert.Equal(TimeSpanParser.Parse("30s"), result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value": 20, "nullable": 30.234}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("20"), result.Value);
        Assert.Equal(TimeSpanParser.Parse("30.234"), result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value": "14:32"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("14:32"), result.Value);
        Assert.Null(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"nullable": "14d"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(default, result.Value);
        Assert.Equal(TimeSpanParser.Parse("14d"), result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value": "14:32.072905702"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TimeSpanParser.Parse("14:32.072905702"), result.Value);
        Assert.Null(result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": "May 3"}""", out result, out error));
        Assert.Equal("'value' was not of type TimeSpan.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    public enum TestEnum
    {
        [Name("a")]
        A,

        [Name("b", "alt")]
        B,

        [Name("none")]
        None,

        [Name("superLongName")]
        SuperLong,
    }

    private sealed class EnumClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestEnum), "B")]
        public TestEnum Value { get; set; }

        [Name("nullable")]
        public TestEnum? Nullable { get; set; }
    }

    [Fact]
    public void Enums()
    {
        Assert.True(
            this.jsonParser.TryParse<EnumClass>("""{"value": "none","nullable":null}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.None, result.Value);
        Assert.Null(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value": "a","nullable":"a"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.A, result.Value);
        Assert.Equal(TestEnum.A, result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.B, result.Value);
        Assert.Null(result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value": "alt","nullable":"alt"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.B, result.Value);
        Assert.Equal(TestEnum.B, result.Nullable);

        Assert.True(
            this.jsonParser.TryParse("""{"value": "superLongName","nullable":"superLongName"}""", out result, out error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestEnum.SuperLong, result.Value);
        Assert.Equal(TestEnum.SuperLong, result.Nullable);

        Assert.False(this.jsonParser.TryParse("""{"value": "May 3"}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonParserTests+TestEnum' must be one of: a, alt, b, none, superLongName.",
            error
        );
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 17}""", out result, out error));
        Assert.Equal("value > 'UnitTests.Serialization.JsonParserTests+TestEnum' was not of type string.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(SerializationType = EnumSerializableSettingsSerializationType.Numbers)]
    public enum TestNumberEnum
    {
        A = 0,
        B = 1,
        None = 17,
        SuperLong = 12345,
    }

    private sealed class EnumSerializationClass
    {
        [Name("value")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum Value { get; set; }

        [Name("nullable")]
        [DefaultValue(typeof(TestNumberEnum), "B")]
        public TestNumberEnum? Nullable { get; set; }
    }

    [Fact]
    public void NumberEnums()
    {
        Assert.True(
            this.jsonParser.TryParse<EnumSerializationClass>(
                """{"value": 0,"nullable":1}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.A, result.Value);
        Assert.Equal(TestNumberEnum.B, result.Nullable);

        Assert.True(this.jsonParser.TryParse("""{"value": 17}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.None, result.Value);

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.B, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value": 1}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.B, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value": 12345}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(TestNumberEnum.SuperLong, result.Value);

        Assert.False(this.jsonParser.TryParse("""{"value": "May 3"}""", out result, out error));
        Assert.Equal("value > 'UnitTests.Serialization.JsonParserTests+TestNumberEnum' was not of type number.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 14}""", out result, out error));
        Assert.Equal(
            "value > 'UnitTests.Serialization.JsonParserTests+TestNumberEnum' must be one of: 0, 1, 17, 12345.",
            error
        );
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": "17"}""", out result, out error));
        Assert.Equal("value > 'UnitTests.Serialization.JsonParserTests+TestNumberEnum' was not of type number.", error);
        Assert.Null(result);
    }

    private sealed class ArrayClass
    {
        [Name("ints")]
        [ArrayLength(1, 4)]
        public int[]? Ints { get; set; }

        [Name("strings")]
        [ArrayLength(1, 4)]
        [StringLength(2, 5)]
        public string[]? Strings { get; set; }

        [Name("implicit")]
        [ArrayLength(Minimum = 1)]
        public int[]? ImplicitMax { get; set; }
    }

    [Fact]
    public void ArrayTest()
    {
        Assert.True(
            this.jsonParser.TryParse<ArrayClass>(
                """{"ints": [2, 3, 4], "strings": ["hello", "world"], "implicit": [2]}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { 2, 3, 4 }, result.Ints);
        Assert.Equal(new[] { "hello", "world" }, result.Strings);
        Assert.Equal(new[] { 2 }, result.ImplicitMax);

        Assert.False(this.jsonParser.TryParse("""{"ints": [2, 3, 4, 5, 6]}""", out result, out error));
        Assert.Equal("'ints' must be shorter than 4 elements.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"strings": []}""", out result, out error));
        Assert.Equal("'strings' must be longer than 0 elements.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"strings": ["a"]}""", out result, out error));
        Assert.Equal("'strings' must be longer than 1 character.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"strings": ["1234567"]}""", out result, out error));
        Assert.Equal("'strings' must be shorter than 5 characters.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse("""{"strings": ["ab", "ab", "ab", "ab", "ab"]}""", out result, out error)
        );
        Assert.Equal("'strings' must be shorter than 4 elements.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse("""{"strings": [2, 3, 4], "ints": ["hello", "world"]}""", out result, out error)
        );
        Assert.Equal("'strings' was not of type string.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"implicit": []}""", out result, out error));
        Assert.Equal("'implicit' must be longer than 0 elements.", error);
        Assert.Null(result);
    }

    private sealed class SubArrayClass
    {
        [Name("ints")]
        public int[][]? Ints { get; set; }
    }

    [Fact]
    public void SubArray()
    {
        Assert.True(
            this.jsonParser.TryParse<SubArrayClass>("""{"ints": [[2, 3, 4]]}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { new[] { 2, 3, 4 } }, result.Ints);
    }

    private sealed class AllowUnknownSubArrayClass
    {
        [Name("value")]
        public required int Value { get; set; }
    }

    [Fact]
    public void AllowUnknownSubArray()
    {
        Assert.True(
            this.jsonParser.TryParse<List<AllowUnknownSubArrayClass>>(
                """
                [
                    { "value":  16, "extra": [{"id": 0}] },
                    { "extra": [{"id": 0}], "value":  78 },
                    { "extra": [{"id": 0}], "value":  53, "extra2": [{"id": 0}] }
                ]
                """,
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(3, result.Count);
        Assert.Equal(16, result[0].Value);
        Assert.Equal(78, result[1].Value);
        Assert.Equal(53, result[2].Value);
    }

    private sealed class BytesClass
    {
        [Name("value")]
        public byte[]? Value { get; set; }

        [Name("values")]
        public byte[][]? Values { get; set; }
    }

    [Fact]
    public void Bytes()
    {
        Assert.True(this.jsonParser.TryParse<BytesClass>("""{"value":"aGVsbG8="}""", out var result, out var error));
        Assert.Null(error);
        Assert.Equal("hello"u8.ToArray(), result.Value);
        Assert.Null(result.Values);

        Assert.True(this.jsonParser.TryParse("""{"values":["aGVsbG8=","d29ybGQ="]}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(new[] { "hello"u8.ToArray(), "world"u8.ToArray() }, result.Values);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value":"4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/","values":["4rdHFh+HYoS8oLdVvbUzEVqB8Lvm7kSPnuwF0AAABYQ/"]}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(
            Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F"),
            result.Value
        );
        Assert.Equal(
            new[] { Convert.FromHexString("E2B747161F876284BCA0B755BDB533115A81F0BBE6EE448F9EEC05D0000005843F") },
            result.Values
        );

        Assert.False(this.jsonParser.TryParse("""{"value":"hello**person"}""", out result, out error));
        Assert.Equal("'value' was not of type base64 encoded string.", error);
        Assert.Null(result);
    }

    public sealed class CustomReferenceObject
    {
        public string String { get; private set; } = null!;

        public int Int { get; private set; }

        [JsonSerializableCustomParse]
        public static bool CustomTryParse(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out CustomReferenceObject? result,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = null;
                return false;
            }

            var span = reader.GetString();
            if (span == null)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = null;
                return false;
            }

            var split = span.IndexOf(',');
            if (split < 0)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = null;
                return false;
            }

            var i = int.Parse(span.Remove(split), CultureInfo.InvariantCulture);
            var s = span.Substring(split + 1);

            result = new CustomReferenceObject() { Int = i, String = s };

            error = null;
            return true;
        }
    }

    public struct CustomValueObject
    {
        public byte[] Bytes { get; private set; }

        [JsonSerializableCustomParse]
        public static bool CustomTryParse(
            ref Utf8JsonReader reader,
            out CustomValueObject result,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = $"Invalid {nameof(CustomReferenceObject)}.";
                result = default;
                return false;
            }

            var span = reader.ValueSpan;
            if (!Base64.TryDecodeFromUtf8(span, out var bytes))
            {
                result = default;
                error = $"Failed to parse {nameof(CustomValueObject)}, invalid base64 string.";
                return false;
            }

            result = new CustomValueObject() { Bytes = bytes };

            error = null;
            return true;
        }
    }

    private sealed class CustomObjectClass
    {
        [Name("reference")]
        public CustomReferenceObject? Reference { get; set; }

        [Name("value")]
        public CustomValueObject Value { get; set; }
    }

    [Fact]
    public void CustomObject()
    {
        Assert.True(
            this.jsonParser.TryParse<CustomObjectClass>(
                """{"value": "AQIDBA==", "reference": "22,howdy"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);
        Assert.Equal(22, result.Reference.Int);
        Assert.Equal("howdy", result.Reference.String);

        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(this.jsonParser.TryParse("""{"value": "AQIDBA=="}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Reference);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);

        Assert.True(this.jsonParser.TryParse("""{"reference": "44,hello there"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(44, result.Reference.Int);
        Assert.Equal("hello there", result.Reference.String);

        Assert.Null(result.Value.Bytes);

        Assert.False(this.jsonParser.TryParse("""{"reference": [22]}""", out result, out error));
        Assert.Equal("reference > Invalid CustomReferenceObject.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": "assd;;"}""", out result, out error));
        Assert.Equal("value > Failed to parse CustomValueObject, invalid base64 string.", error);
        Assert.Null(result);
    }

    private sealed class CustomParseClass
    {
        [Name("class")]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed class CustomValueClass
        {
            public int Value { get; set; }

            [SuppressMessage(
                "Major Code Smell",
                "S1144:Unused private types or members should be removed",
                Justification = "Used by reflection."
            )]
            [JsonSerializableCustomParse]
            public static bool TryParseCustomValueClass(
                ref Utf8JsonReader reader,
                [NotNullWhen(true)] out CustomValueClass? value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType != JsonTokenType.Number)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueClass)}";
                    return false;
                }

                var readValue = reader.GetInt32();
                value = new CustomValueClass { Value = readValue };
                error = null;
                return true;
            }
        }

        public struct CustomValueStruct
        {
            public int Value { get; set; }

            [SuppressMessage(
                "Major Code Smell",
                "S1144:Unused private types or members should be removed",
                Justification = "Used by reflection."
            )]
            [JsonSerializableCustomParse]
            public static bool TryParseCustomValueStruct(
                ref Utf8JsonReader reader,
                out CustomValueStruct value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType != JsonTokenType.Number)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueStruct)}";
                    return false;
                }

                var readValue = reader.GetInt32();
                value = new CustomValueStruct { Value = readValue };
                error = null;
                return true;
            }
        }
    }

    [Fact]
    public void CustomParser()
    {
        Assert.True(
            this.jsonParser.TryParse<CustomParseClass>("""{"class": 22, "struct": 44}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Class);
        Assert.Equal(22, result.Class.Value);
        Assert.Equal(44, result.Struct.Value);

        Assert.False(this.jsonParser.TryParse("""{"class": [22]}""", out result, out error));
        Assert.Equal("class > Invalid token type for CustomValueClass", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"struct": "assd"}""", out result, out error));
        Assert.Equal("struct > Invalid token type for CustomValueStruct", error);
        Assert.Null(result);
    }

    public sealed class CustomInjectedParseClass
    {
        [Name("class")]
        public CustomValueClass? Class { get; set; }

        [Name("struct")]
        public CustomValueStruct Struct { get; set; }

        public sealed class CustomValueClass
        {
            public int Value { get; set; }

            public static bool TryParse(
                ref Utf8JsonReader reader,
                [NotNullWhen(true)] out CustomValueClass? value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType != JsonTokenType.Number)
                {
                    value = null;
                    error = $"Invalid token type for {nameof(CustomValueClass)}";
                    return false;
                }

                var readValue = reader.GetInt32();
                value = new CustomValueClass { Value = readValue };
                error = null;
                return true;
            }
        }

        public struct CustomValueStruct
        {
            public bool Value { get; set; }

            public static bool TryParse(
                ref Utf8JsonReader reader,
                out CustomValueStruct value,
                [NotNullWhen(false)] out string? error
            )
            {
                if (reader.TokenType is not JsonTokenType.False and not JsonTokenType.True)
                {
                    value = default;
                    error = $"Invalid token type for {nameof(CustomValueStruct)}";
                    return false;
                }

                var readValue = reader.TokenType == JsonTokenType.True;
                value = new CustomValueStruct { Value = readValue };
                error = null;
                return true;
            }
        }
    }

    [Fact]
    public void CustomInjectedParser()
    {
        Assert.True(
            this.jsonParser.TryParse<CustomInjectedParseClass>(
                """{"class": 22, "struct": true}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Class);
        Assert.Equal(22, result.Class.Value);
        Assert.True(result.Struct.Value);

        Assert.False(this.jsonParser.TryParse("""{"class": [22]}""", out result, out error));
        Assert.Equal("class > Invalid token type for CustomValueClass", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"struct": "assd"}""", out result, out error));
        Assert.Equal("struct > Invalid token type for CustomValueStruct", error);
        Assert.Null(result);
    }

    public class NormalReferenceObject
    {
        [Name("string")]
        public string String { get; set; } = null!;

        [Name("int")]
        public int Int { get; set; }
    }

    public struct NormalValueObject
    {
        [Name("bytes")]
        public byte[] Bytes { get; set; }
    }

    public class NormalObjectClass
    {
        [Name("reference")]
        public NormalReferenceObject? Reference { get; set; }

        [Name("value")]
        public NormalValueObject Value { get; set; }
    }

    [Fact]
    public void NormalObject()
    {
        Assert.True(
            this.jsonParser.TryParse<NormalObjectClass>(
                """{"value": {"bytes": "AQIDBA=="}, "reference": {"int": 22, "string": "howdy"}}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Reference);
        Assert.Equal(22, result.Reference.Int);
        Assert.Equal("howdy", result.Reference.String);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Value.Bytes);
    }

    public class NormalObjectArraysClass
    {
        [Name("references")]
        public NormalReferenceObject[]? References { get; set; }

        [Name("values")]
        public NormalValueObject[]? Values { get; set; }
    }

    [Fact]
    public void NormalObjectArrays()
    {
        Assert.True(
            this.jsonParser.TryParse<NormalObjectArraysClass>(
                """{"values": [{"bytes": "AQIDBA=="}], "references": [{"int": 22, "string": "howdy"}]}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.References);
        Assert.NotNull(result.Values);
        var referenceObject = Assert.Single(result.References);
        Assert.Equal(22, referenceObject.Int);
        Assert.Equal("howdy", referenceObject.String);
        var referenceValues = Assert.Single(result.Values);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, referenceValues.Bytes);

        Assert.True(
            this.jsonParser.TryParse(
                """
                {"values": [
                    {"bytes": "AQIDBA=="},
                    {"bytes": "GBYNDDc="}],
                "references": [
                    {"int": 22, "string": "howdy"},
                    {"int": 13, "string": "partner"}
                ]}
                """,
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.References);
        Assert.NotNull(result.Values);
        Assert.Equal(2, result.References.Length);
        Assert.Equal(22, result.References[0].Int);
        Assert.Equal("howdy", result.References[0].String);
        Assert.Equal(13, result.References[1].Int);
        Assert.Equal("partner", result.References[1].String);
        Assert.Equal(2, result.Values.Length);
        Assert.Equal(new byte[] { 1, 2, 3, 4 }, result.Values[0].Bytes);
        Assert.Equal(new byte[] { 24, 22, 13, 12, 55 }, result.Values[1].Bytes);
    }

    public class GuidObject
    {
        [Name("value")]
        public Guid Value { get; set; }
    }

    [Fact]
    public void GuidTest()
    {
        Assert.True(
            this.jsonParser.TryParse<GuidObject>(
                """{"value": "9184400c-d7fe-422f-9d40-720c30a2b18f"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(Guid.Parse("9184400c-d7fe-422f-9d40-720c30a2b18f"), result.Value);

        Assert.False(
            this.jsonParser.TryParse("""{"value": "e847da22-bb0a-11ed-afa1-asdfasdfasdf"}""", out result, out error)
        );
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": true}""", out result, out error));
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 44.37}""", out result, out error));
        Assert.Equal("'value' was not of type Guid.", error);
        Assert.Null(result);
    }

    public class SystemTextJsonObjectClass
    {
        [Name("object")]
        public JsonObject? Object { get; set; }

        [Name("array")]
        public JsonArray? Array { get; set; }

        [Name("node")]
        public JsonNode? Node { get; set; }
    }

    [Fact]
    public void SystemTextJsonObject()
    {
        Assert.True(
            this.jsonParser.TryParse<SystemTextJsonObjectClass>(
                """{"object": {"hello": "world", "value": 12}, "array": [12, 13, "hello", true], "node": "hello"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.NotNull(result.Object);
        Assert.NotNull(result.Array);
        Assert.NotNull(result.Node);
        Assert.Equal("world", result.Object["hello"]?.GetValue<string>());
        Assert.Equal(12, result.Object["value"]?.GetValue<int>());
        Assert.Equal(12, result.Array[0]?.GetValue<int>());
        Assert.Equal(13, result.Array[1]?.GetValue<int>());
        Assert.Equal("hello", result.Array[2]?.GetValue<string>());
        Assert.Equal(true, result.Array[3]?.GetValue<bool>());
        Assert.Equal("hello", result.Node.GetValue<string>());

        Assert.True(this.jsonParser.TryParse("""{"node": 2}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Object);
        Assert.Null(result.Array);
        Assert.NotNull(result.Node);
        Assert.Equal(2, result.Node.GetValue<int>());

        Assert.True(this.jsonParser.TryParse("""{"node": 2.2}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Object);
        Assert.Null(result.Array);
        Assert.NotNull(result.Node);
        Assert.Equal(2.2, result.Node.GetValue<double>());

        Assert.True(this.jsonParser.TryParse("""{"node": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Object);
        Assert.Null(result.Array);
        Assert.NotNull(result.Node);
        Assert.True(result.Node.GetValue<bool>());

        Assert.False(this.jsonParser.TryParse("""{"object": [12, 13]}""", out result, out error));
        Assert.Equal("'object' was not of type object.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"array": {"12": 13}}""", out result, out error));
        Assert.Equal("'array' was not of type array.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"node": hello}""", out result, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(result);
    }

    public class LinkedListObject
    {
        [Name("child")]
        public LinkedListObject? Child { get; set; }

        [Name("value")]
        [Required]
        public int Value { get; set; }
    }

    [Fact]
    public void LinkedList()
    {
        var child = default(JsonObject);
        for (var i = 14; i >= 0; i--)
        {
            var node = new JsonObject { ["value"] = i, ["child"] = child };
            child = node;
        }

        var json = child!.ToString();
        Assert.True(this.jsonParser.TryParse<LinkedListObject>(json, out var result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        {
            var node = result;
            var i = 0;
            while (node != null)
            {
                Assert.Equal(i, node.Value);
                i++;
                node = node.Child;
            }

            Assert.Equal(15, i);
        }
    }

    public class NullValuesObjectClass
    {
        [Name("string")]
        [StringLength(64)]
        public string? String { get; set; }

        [Name("int")]
        public int? Int { get; set; }
    }

    [Fact]
    public void NullValuesObject()
    {
        Assert.True(
            this.jsonParser.TryParse<NullValuesObjectClass>(
                """{"string": null, "int": null}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.String);
        Assert.Null(result.Int);
    }

    public class ReadOnlySequenceObject
    {
        [Name("bigLongNameThatIsTooLong")]
        public string? String { get; set; }

        [Name("anotherNameThatIsWayTooLong")]
        public long Int { get; set; }

        [Name("bytes")]
        public byte[]? Bytes { get; set; }

        [Name("date")]
        public DateTime Date { get; set; }

        [Name("time")]
        public TimeSpan Time { get; set; }

        [Name("enum")]
        public TestEnum Enum { get; set; }
    }

    [Fact]
    public void ReadOnlySequence()
    {
        var json = """
            {
                "bigLongNameThatIsTooLong": "hello world how are you",
                "anotherNameThatIsWayTooLong": 6498463318,
                "bytes": "AAECAwQFBgcICQoLDA0ODw==",
                "date": "May 1, 2020",
                "time": "20:30",
                "enum": "superLongName"
            }
            """;

        Assert.True(
            this.jsonParser.TryParse<ReadOnlySequenceObject>(
                SequenceUtilities.SplitIntoSequence(json, 4),
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello world how are you", result.String);
        Assert.Equal(6498463318, result.Int);
        Assert.Equal(Enumerable.Range(0, 16).Select(c => (byte)c).ToArray(), result.Bytes);
        Assert.Equal(result.Date, DateTime.Parse("May 1, 2020", CultureInfo.InvariantCulture));
        Assert.Equal(result.Time, TimeSpanParser.Parse("20:30"));
        Assert.Equal(TestEnum.SuperLong, result.Enum);
    }

    [JsonSerializableSettings(AllowExtraProperties = false)]
    public class RequiredObject
    {
        [Name("value")]
        public required string Value { get; set; }

        [Name("nested")]
        public required NestedObject Nested { get; set; }

        public class NestedObject
        {
            [Name("value")]
            public required string Value { get; set; }
        }
    }

    [Fact]
    public void Required()
    {
        Assert.True(
            this.jsonParser.TryParse<RequiredObject>(
                """{"value": "world", "nested": {"value": "nested value"}}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.NotNull(result.Nested);
        Assert.Equal("nested value", result.Nested.Value);

        Assert.False(this.jsonParser.TryParse("""{"hello": "world"}""", out result, out error));
        Assert.Equal("Unknown property received: hello", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": "world"}""", out result, out error));
        Assert.Equal(
            "A required property was missing on UnitTests.Serialization.JsonParserTests+RequiredObject. Missing: nested.",
            error
        );
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": "world", "nested": {}}""", out result, out error));
        Assert.Equal(
            "nested > A required property was missing on UnitTests.Serialization.JsonParserTests+RequiredObject+NestedObject. Missing: value.",
            error
        );
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Equal(
            "A required property was missing on UnitTests.Serialization.JsonParserTests+RequiredObject. Missing: value, nested.",
            error
        );
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": null}""", out result, out error));
        Assert.Equal("'value' was null. Should be of type string.", error);
        Assert.Null(result);
    }

    [FriendlyName("SuperFriendlyName")]
    [JsonSerializableSettings(AllowExtraProperties = false)]
    public class FriendlyNameObject
    {
        [Name("value")]
        public required string Value { get; set; }

        [Name("nested")]
        public required NestedObject Nested { get; set; }

        [FriendlyName("Nested Object")]
        public class NestedObject
        {
            [Name("value")]
            public required string Value { get; set; }
        }
    }

    [Fact]
    public void FriendlyName()
    {
        Assert.True(
            this.jsonParser.TryParse<FriendlyNameObject>(
                """{"value": "world", "nested": {"value": "nested value"}}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.NotNull(result.Nested);
        Assert.Equal("nested value", result.Nested.Value);

        Assert.False(this.jsonParser.TryParse("""{"hello": "world"}""", out result, out error));
        Assert.Equal("Unknown property received: hello", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": "world"}""", out result, out error));
        Assert.Equal("A required property was missing on SuperFriendlyName. Missing: nested.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": "world", "nested": {}}""", out result, out error));
        Assert.Equal("nested > A required property was missing on Nested Object. Missing: value.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Equal("A required property was missing on SuperFriendlyName. Missing: value, nested.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": null}""", out result, out error));
        Assert.Equal("'value' was null. Should be of type string.", error);
        Assert.Null(result);
    }

    public class SpecialStringsObject
    {
        [Required(AllowOnlyWhiteSpace = false)]
        [Name("value")]
        public string Value { get; set; } = null!;

        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue]
        [Name("value2")]
        public string Value2 { get; set; } = null!;

        [Required(AllowOnlyWhiteSpace = false)]
        [TrimValue(new[] { 'a', 'b', 'c' })]
        [Name("value3")]
        public string? Value3 { get; set; }
    }

    [Fact]
    public void SpecialStrings()
    {
        Assert.True(
            this.jsonParser.TryParse<SpecialStringsObject>(
                """{"value": "world", "value2": "value", "value3": "something"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.False(
            this.jsonParser.TryParse(
                """{"value": "  ", "value2": "value", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse(
                """{"value": "\t", "value2": "value", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "  value  ", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "\tvalue", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "\tva  lue", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("va  lue", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.False(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "    ", "value3": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value2' must contain characters that are not whitespace.", error);
        Assert.Null(result);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "value", "value3": "aaasomethingbbb"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("something", result.Value3);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "value", "value3": "aaabbbccvcaabc"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("world", result.Value);
        Assert.Equal("value", result.Value2);
        Assert.Equal("v", result.Value3);

        Assert.False(
            this.jsonParser.TryParse(
                """{"value": "world", "value2": "value", "value3": "aaabbbcccaabc"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'value3' must contain characters that are not whitespace.", error);
        Assert.Null(result);
    }

    [Fact]
    public async Task TestPipe()
    {
        var json = """
            {
                "bigLongNameThatIsTooLong": "hello world how are you",
                "anotherNameThatIsWayTooLong": 6498463318,
                "bytes": "AAECAwQFBgcICQoLDA0ODw==",
                "date": "May 1, 2020",
                "time": "20:30",
                "enum": "superLongName"
            }
            """;

        var pipe = new Pipe(new PipeOptions(pauseWriterThreshold: 0));
        var sequence = SequenceUtilities.SplitIntoSequence(json, 4);
        foreach (var segment in sequence)
        {
            var memory = pipe.Writer.GetMemory(segment.Length);
            segment.CopyTo(memory);
            pipe.Writer.Advance(segment.Length);
        }

        await pipe.Writer.CompleteAsync();

        var readResult = await pipe.Reader.ReadToEndAsync();
        Assert.True(this.jsonParser.TryParse(readResult.Buffer, out ReadOnlySequenceObject? result, out var error));
        pipe.Reader.AdvanceTo(readResult.Buffer.End);
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("hello world how are you", result.String);
        Assert.Equal(6498463318, result.Int);
        Assert.Equal(Enumerable.Range(0, 16).Select(c => (byte)c).ToArray(), result.Bytes);
        Assert.Equal(result.Date, DateTime.Parse("May 1, 2020", CultureInfo.InvariantCulture));
        Assert.Equal(result.Time, TimeSpanParser.Parse("20:30"));
        Assert.Equal(TestEnum.SuperLong, result.Enum);
    }

    [Fact]
    public async Task TestPipeFailure()
    {
        var json = "hello";

        var pipe = new Pipe(new PipeOptions(pauseWriterThreshold: 0));
        var sequence = SequenceUtilities.SplitIntoSequence(json, 4);
        foreach (var segment in sequence)
        {
            var memory = pipe.Writer.GetMemory(segment.Length);
            segment.CopyTo(memory);
            pipe.Writer.Advance(segment.Length);
        }

        await pipe.Writer.CompleteAsync();

        var readResult = await pipe.Reader.ReadToEndAsync();
        Assert.False(this.jsonParser.TryParse(readResult.Buffer, out ReadOnlySequenceObject? result, out var error));
        pipe.Reader.AdvanceTo(readResult.Buffer.Start);
        Assert.Equal("Failed to parse json.", error);
        Assert.NotEqual(string.Empty, error);
        Assert.Null(result);

        var buffer = await pipe.Reader.ReadUTF8StringAsync();
        Assert.Equal(json, buffer);
    }

    private sealed class RangesClass
    {
        [Name("decimal")]
        [ValidRange(1, 100)]
        public decimal Decimal { get; set; }

        [Name("int")]
        [ValidRange(4, 90)]
        public int Int { get; set; }

        [Name("float")]
        [ValidRange(12.0f, 17.0f)]
        public float Float { get; set; }

        [Name("double")]
        [ValidRange(-12.0, 17.0)]
        public double Double { get; set; }
    }

    private sealed class RangesClassNegative
    {
        [Name("decimal")]
        [ValidRange(-201, -12)]
        public decimal Decimal { get; set; }

        [Name("int")]
        [ValidRange(-90, -12)]
        public int Int { get; set; }

        [Name("float")]
        [ValidRange(-17.0f, -1.0f)]
        public float Float { get; set; }

        [Name("double")]
        [ValidRange(-70.0, -42.0)]
        public double Double { get; set; }
    }

    [Fact]
    public void Ranges()
    {
        Assert.True(this.jsonParser.TryParse<RangesClass>("""{"int": 20}""", out var result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(0M, result.Decimal);
        Assert.Equal(20, result.Int);

        Assert.True(this.jsonParser.TryParse("""{"decimal": 35}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(35M, result.Decimal);
        Assert.Equal(0, result.Int);

        Assert.True(
            this.jsonParser.TryParse(
                """{"decimal": 1, "int": 4, "float": 12, "double": -12.0}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(1M, result.Decimal);
        Assert.Equal(4, result.Int);
        Assert.Equal(12.0f, result.Float);
        Assert.Equal(-12.0f, result.Double);

        Assert.True(
            this.jsonParser.TryParse(
                """{"decimal": 100, "int": 90, "float": 17, "double": 17.0}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(100M, result.Decimal);
        Assert.Equal(90, result.Int);
        Assert.Equal(17.0f, result.Float);
        Assert.Equal(17.0f, result.Double);

        Assert.True(
            this.jsonParser.TryParse<RangesClassNegative>(
                """{"decimal": -201, "int": -90, "float": -17.0, "double": -70}""",
                out var negativeResult,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(negativeResult);
        Assert.Equal(-201M, negativeResult.Decimal);
        Assert.Equal(-90, negativeResult.Int);
        Assert.Equal(-17.0f, negativeResult.Float);
        Assert.Equal(-70.0f, negativeResult.Double);

        Assert.True(
            this.jsonParser.TryParse(
                """{"decimal": -12, "int": -12, "float": -1, "double": -42.0}""",
                out negativeResult,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(negativeResult);
        Assert.Equal(-12M, negativeResult.Decimal);
        Assert.Equal(-12, negativeResult.Int);
        Assert.Equal(-1.0f, negativeResult.Float);
        Assert.Equal(-42.0f, negativeResult.Double);

        Assert.False(this.jsonParser.TryParse("""{"decimal": -11.99999}""", out negativeResult, out error));
        Assert.Equal("'decimal' was not in the range [-201..-12]", error);
        Assert.Null(negativeResult);

        Assert.False(this.jsonParser.TryParse("""{"float": -0.999}""", out negativeResult, out error));
        Assert.Equal("'float' was not in the range [-17..-1]", error);
        Assert.Null(negativeResult);

        Assert.False(this.jsonParser.TryParse("""{"double": -70.00000001}""", out negativeResult, out error));
        Assert.Equal("'double' was not in the range [-70..-42]", error);
        Assert.Null(negativeResult);

        Assert.False(this.jsonParser.TryParse("""{"decimal": 0}""", out result, out error));
        Assert.Equal("'decimal' was not in the range [1..100]", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"decimal": 101}""", out result, out error));
        Assert.Equal("'decimal' was not in the range [1..100]", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": 3}""", out result, out error));
        Assert.Equal("'int' was not in the range [4..90]", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": 91}""", out result, out error));
        Assert.Equal("'int' was not in the range [4..90]", error);
        Assert.Null(result);
    }

    private sealed class JsonParserSettingsClass
    {
        [Name("value")]
        public int Value { get; set; }
    }

    [JsonSerializableSettings(AllowExtraProperties = false)]
    private sealed class JsonParserSettingsNoExtrasClass
    {
        [Name("value")]
        public int Value { get; set; }
    }

    [Fact]
    public void JsonParserSettings()
    {
        var result = this.jsonParser.TryParse<JsonParserSettingsNoExtrasClass>(
            """{"value": 22, "extra": "something"}""",
            out var resultNoExtras,
            out var error
        );
        Assert.False(result);
        Assert.Equal("Unknown property received: extra", error);
        Assert.Null(resultNoExtras);

        Assert.True(
            this.jsonParser.TryParse<JsonParserSettingsClass>(
                """{"value": 22, "extra": "something"}""",
                out var resultExtras,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(resultExtras);
        Assert.Equal(22, resultExtras.Value);
    }

    [JsonSerializableSettings(AllowExtraProperties = false)]
    private sealed class EmptyFieldTypeClass
    {
        [Required]
        [Name("value")]
        public int Value { get; set; }

        [Name("subclass")]
        public Subclass? SubValue { get; set; }

        [FriendlyName("Subclass")]
        [JsonSerializableSettings(AllowExtraProperties = false)]
        public sealed class Subclass
        {
#pragma warning disable CS0649, S3459, IDE0044
            private int a;
            private int b;
#pragma warning restore CS0649, S3459, IDE0044

            [Name("unused")]
            public int Unused { get; set; }

            public override string ToString() => $"{this.a}-${this.b}";
        }
    }

    [Fact]
    public void EmptyFieldType()
    {
        Assert.False(
            this.jsonParser.TryParse<EmptyFieldTypeClass>(
                """{"value": 22, "subclass": "hello"}""",
                out var result,
                out var error
            )
        );
        Assert.Equal("subclass > 'Subclass' requires an object.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse("""{"value": 22, "subclass": {"hello": "world"}}""", out result, out error)
        );
        Assert.Equal("subclass > Unknown property received: hello", error);
        Assert.Null(result);
    }

    [SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Spec correct.")]
    public sealed class UUIDv1Class
    {
        [Required]
        [Name("value")]
        public UUIDv1 Value { get; set; }
    }

    [Fact]
    public void UUIDv1Test()
    {
        Assert.True(
            this.jsonParser.TryParse<UUIDv1Class>(
                """{"value": "e847da22-bb0a-11ed-afa1-0242ac120002"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(UUIDv1.Parse("e847da22-bb0a-11ed-afa1-0242ac120002"), result.Value);

        Assert.False(
            this.jsonParser.TryParse("""{"value": "e847da22-bb0a-11ed-afa1-asdfasdfasdf"}""", out result, out error)
        );
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": true}""", out result, out error));
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": 44.37}""", out result, out error));
        Assert.Equal("'value' was not of type UUIDv1.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S3604:Member initializer values should not be redundant",
        Justification = "Testing exactly this."
    )]
    [SuppressMessage(
        "Major Code Smell",
        "S1144:Unused private types or members should be removed",
        Justification = "Testing exactly this."
    )]
    private sealed class NoDefaultConstructorClass
    {
        [Name("value")]
        [DefaultValue(12)]
        public int Value { get; set; } = 17;

        [Name("default")]
        public int Default { get; set; }

        [Name("string")]
        public string? StringValue { get; set; } = "hello world";

        public NoDefaultConstructorClass(int value, string s)
        {
            this.Value = value;
            this.StringValue = s;
        }
    }

    [Fact]
    public void NoDefaultConstructor()
    {
        Assert.True(
            this.jsonParser.TryParse<NoDefaultConstructorClass>(
                """{"value": 2, "string": "rock"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(2, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Equal("rock", result.StringValue);

        Assert.True(this.jsonParser.TryParse("""{"string": "rock"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Equal("rock", result.StringValue);

        Assert.True(this.jsonParser.TryParse("""{}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(12, result.Value);
        Assert.Equal(0, result.Default);
        Assert.Null(result.StringValue);

        Assert.False(this.jsonParser.TryParse("asdf", out result, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(result);
    }

    private struct ValueTypeStruct
    {
        [Name("value")]
        [DefaultValue(12)]
        public int Value { get; set; }

        [Required]
        [Name("string")]
        public string? StringValue { get; set; }
    }

    [Fact]
    public void ValueType()
    {
        Assert.True(
            this.jsonParser.TryParse<ValueTypeStruct>(
                """{"value": 2, "string": "rock"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("rock", result.StringValue);
        Assert.Equal(2, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"string": "rock"}""", out result, out error));
        Assert.Null(error);
        Assert.Equal(12, result.Value);
        Assert.Equal("rock", result.StringValue);

        Assert.True(this.jsonParser.TryParse("""{"string": null}""", out result, out error));
        Assert.Null(error);
        Assert.Equal(12, result.Value);
        Assert.Null(result.StringValue);

        Assert.False(this.jsonParser.TryParse("asdf", out result, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Equal(0, result.Value);
        Assert.Null(result.StringValue);
    }

    private sealed class AllowAnyStringClass
    {
        [Name("value")]
        [AllowAnyString]
        public string? Value { get; set; }

        [Name("trailing")]
        public int Trailing { get; set; }
    }

    [Fact]
    public void AllowAnyString()
    {
        Assert.True(
            this.jsonParser.TryParse<AllowAnyStringClass>("""{"value": "easy"}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("easy", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(this.jsonParser.TryParse("""{"value": "null", "trailing": 33 }""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("null", result.Value);
        Assert.Equal(33, result.Trailing);

        Assert.True(this.jsonParser.TryParse("""{"value": null}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Null(result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(this.jsonParser.TryParse("""{"value": 22}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("22", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(this.jsonParser.TryParse("""{"value": "true"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("true", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(this.jsonParser.TryParse("""{"value": true}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("true", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(this.jsonParser.TryParse("""{"value": false}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("false", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": 14.3365799813410704980910978570980928907092098651089710983}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("14.3365799813410704980910978570980928907092098651089710983", result.Value);
        Assert.Equal(0, result.Trailing);

        Assert.False(this.jsonParser.TryParse("""{"value": {}}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": []}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": {"string"}}""", out result, out error));
        Assert.Equal("'value' was not of type string.", error);
        Assert.Null(result);
    }

    private sealed class AllowAnyStringArrayClass
    {
        [Name("value")]
        [AllowAnyString]
        public string?[]? Value { get; set; }
    }

    [Fact]
    public void AllowAnyStringArray()
    {
        Assert.True(
            this.jsonParser.TryParse<AllowAnyStringArrayClass>("""{"value": ["easy"]}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "easy" }, result.Value);

        Assert.False(this.jsonParser.TryParse("""{"value": "null"}""", out result, out error));
        Assert.Equal("'value' was not an array.", error);
        Assert.Null(result);

        Assert.True(this.jsonParser.TryParse("""{"value": ["null"]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "null" }, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value": [null]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { default(string) }, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value": [22, "hello", null]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "22", "hello", null }, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value": ["true", "false"]}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "true", "false" }, result.Value);

        Assert.True(
            this.jsonParser.TryParse(
                """{"value": [14.3365799813410704980910978570980928907092098651089710983]}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(new[] { "14.3365799813410704980910978570980928907092098651089710983" }, result.Value);

        Assert.False(this.jsonParser.TryParse("""{"value": {}}""", out result, out error));
        Assert.Equal("'value' was not an array.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"value": {"string"}}""", out result, out error));
        Assert.Equal("'value' was not an array.", error);
        Assert.Null(result);
    }

    private sealed class FlexibleBoolClass
    {
        [BoolSerializableSettings(AllowNumbers = true)]
        [Name("int")]
        public bool Int { get; set; }

        [BoolSerializableSettings(AllowFlexibleStrings = true)]
        [Name("strings")]
        public bool Strings { get; set; }

        [BoolSerializableSettings(AllowFlexibleStrings = true, AllowNumbers = true)]
        [Name("both")]
        public bool Both { get; set; }

        [BoolSerializableSettings(SerializationType = BoolSerializableSettingsSerializationType.Numbers)]
        [Name("serialization")]
        public bool Serialization { get; set; }
    }

    [Fact]
    public void FlexibleBool()
    {
        Assert.True(
            this.jsonParser.TryParse<FlexibleBoolClass>(
                """{"int": true, "strings": true, "both": true, "serialization": 1}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Int);
        Assert.True(result.Strings);
        Assert.True(result.Both);
        Assert.True(result.Serialization);

        Assert.True(
            this.jsonParser.TryParse(
                """{"int": false, "strings": false, "both": false, "serialization": 0}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Int);
        Assert.False(result.Strings);
        Assert.False(result.Both);
        Assert.False(result.Serialization);

        // Test int values
        Assert.True(this.jsonParser.TryParse("""{"int": 0}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Int);

        Assert.True(this.jsonParser.TryParse("""{"int": 1}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Int);

        Assert.False(this.jsonParser.TryParse("""{"int": "0"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": -15}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": 2}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": "1"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        // Test string values
        Assert.True(this.jsonParser.TryParse("""{"strings": "false"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Strings);

        Assert.True(this.jsonParser.TryParse("""{"strings": "true"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Strings);

        Assert.True(this.jsonParser.TryParse("""{"strings": "  false "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Strings);

        Assert.True(this.jsonParser.TryParse("""{"strings": " True  "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Strings);

        Assert.True(this.jsonParser.TryParse("""{"strings": "FALSE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Strings);

        Assert.True(this.jsonParser.TryParse("""{"strings": "TRUE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Strings);

        // Test both values
        Assert.True(this.jsonParser.TryParse("""{"both": 0}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": 1}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "0"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "1"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "false"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "true"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "  false "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": " True  "}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "FALSE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.False(result.Both);

        Assert.True(this.jsonParser.TryParse("""{"both": "TRUE"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.True(result.Both);

        Assert.False(this.jsonParser.TryParse("""{"int": "false"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": "01"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"int": "10"}""", out result, out error));
        Assert.Equal("'int' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"strings": "1"}""", out result, out error));
        Assert.Equal("'strings' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"strings": "0"}""", out result, out error));
        Assert.Equal("'strings' was not of type bool.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"both": "hello"}""", out result, out error));
        Assert.Equal("'both' was not of type bool.", error);
        Assert.Null(result);
    }

    private sealed class DefaultNullableValuesClass
    {
        [Name("int")]
        [DefaultValue(22)]
        public int? IntValue { get; set; }
    }

    [Fact]
    public void DefaultNullableValue()
    {
        Assert.True(this.jsonParser.TryParse<DefaultNullableValuesClass>("""{}""", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(22, result.IntValue);

        Assert.True(this.jsonParser.TryParse("""{"int": null}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.IntValue);
    }

    private sealed class ListClass
    {
        [Name("value")]
        [Required]
        public List<int> List { get; set; } = null!;
    }

    [Fact]
    public void List()
    {
        Assert.True(this.jsonParser.TryParse<ListClass>("""{"value": [22, 23, 24]}""", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(new[] { 22, 23, 24 }, result.List);
    }

    private sealed class HashSetClass
    {
        [Name("value")]
        public required HashSet<int> List { get; set; }
    }

    [Fact]
    public void HashSet()
    {
        Assert.True(
            this.jsonParser.TryParse<HashSetClass>("""{"value": [22, 22, 23, 24]}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(new HashSet<int> { 22, 23, 24 }, result.List);
    }

    private sealed class FrozenSetClass
    {
        [Name("value")]
        public required FrozenSet<int> List { get; set; }
    }

    [Fact]
    public void FrozenSet()
    {
        Assert.True(
            this.jsonParser.TryParse<FrozenSetClass>("""{"value": [22, 22, 23, 24]}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.Equal(new HashSet<int> { 22, 23, 24 }, result.List);
    }

    [FriendlyName("Required Property Init")]
    private sealed class RequiredPropertyInitClass
    {
        [Name("username")]
        public required string Username { get; init; }

        [Name("password")]
        public required string Password { get; init; }

        [Name("remember")]
        public bool RememberMe { get; init; }

        [Name("coupon")]
        public string? Coupon { get; init; }
    }

    [Fact]
    public void RequiredPropertyInit()
    {
        Assert.True(
            this.jsonParser.TryParse<RequiredPropertyInitClass>(
                """{"username":"tron","password":"hunter2","coupon":"1234"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("tron", result.Username);
        Assert.Equal("hunter2", result.Password);
        Assert.False(result.RememberMe);
        Assert.Equal("1234", result.Coupon);

        Assert.False(this.jsonParser.TryParse("""{"username":"tron","coupon":"1234"}""", out result, out error));
        Assert.Equal("A required property was missing on Required Property Init. Missing: password.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"password":"hunter2","coupon":"1234"}""", out result, out error));
        Assert.Equal("A required property was missing on Required Property Init. Missing: username.", error);
        Assert.Null(result);

        Assert.True(this.jsonParser.TryParse("""{"username":"tron","password":"hunter2"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal("tron", result.Username);
        Assert.Equal("hunter2", result.Password);
        Assert.False(result.RememberMe);
        Assert.Null(result.Coupon);
    }

    private sealed class MultipleNamesClass
    {
        [Name("hello")]
        [Name("world")]
        public int Value { get; set; }
    }

    private sealed class MissingSetter
    {
        [Name("value")]
        public int Value { get; }
    }

    private sealed class InvalidDefaultValue
    {
        [Name("value")]
        [DefaultValue(typeof(char), "j")]
        public object? Value { get; set; }
    }

    [Fact]
    public void Exceptions()
    {
        Exception ex = Assert.Throws<DuplicateNameException>(
            () => this.jsonParser.TryParse<MultipleNamesClass>("""{}""", out _, out _)
        );
        Assert.Equal("Using GetCustomAttribute cannot have multiple of same attribute.", ex.Message);

        ex = Assert.Throws<NullReferenceException>(
            () => this.jsonParser.TryParse<MissingSetter>("""{}""", out _, out _)
        );
        Assert.Equal(
            "UnitTests.Serialization.JsonParserTests+MissingSetter.Value requires a valid setter.",
            ex.Message
        );

        ex = Assert.Throws<NotSupportedException>(
            () => this.jsonParser.TryParse<InvalidDefaultValue>("""{}""", out _, out _)
        );
        Assert.Equal("Unknown type System.Object has DefaultValue.", ex.Message);
    }

    private sealed class DefaultsClass
    {
        [Name("s8")]
        [DefaultValue(sbyte.MinValue)]
        public sbyte S8 { get; set; }

        [Name("s16")]
        [DefaultValue(short.MinValue)]
        public short S16 { get; set; }

        [Name("s32")]
        [DefaultValue(int.MinValue)]
        public int S32 { get; set; }

        [Name("s64")]
        [DefaultValue(long.MinValue)]
        public long S64 { get; set; }

        [Name("u8")]
        [DefaultValue(byte.MaxValue)]
        public byte U8 { get; set; }

        [Name("u16")]
        [DefaultValue(ushort.MaxValue)]
        public ushort U16 { get; set; }

        [Name("u32")]
        [DefaultValue(uint.MaxValue)]
        public uint U32 { get; set; }

        [Name("u64")]
        [DefaultValue(ulong.MaxValue)]
        public ulong U64 { get; set; }

        [Name("f32")]
        [DefaultValue(float.MaxValue)]
        public float F32 { get; set; }

        [Name("f64")]
        [DefaultValue(double.MaxValue)]
        public double F64 { get; set; }

        [Name("decimal")]
        [DefaultValue(typeof(decimal), "79228162514264337593543950335")]
        public decimal Decimal { get; set; }

        [Name("timespan")]
        [DefaultValue(typeof(TimeSpan), "20:30")]
        public TimeSpan TimeSpan { get; set; }

        [Name("datetime")]
        [DefaultValue(typeof(DateTime), "1970-01-01T00:00:00Z")]
        public DateTime DateTime { get; set; }

        [Name("datetimeoffset")]
        [DefaultValue(typeof(DateTimeOffset), "1970-01-01T00:00:00Z")]
        public DateTimeOffset DateTimeOffset { get; set; }
    }

    [Fact]
    public void Defaults()
    {
        Assert.True(this.jsonParser.TryParse<DefaultsClass>("""{}""", out var result, out var error));
        Assert.Null(error);
        Assert.NotNull(result);

        Assert.Equal(sbyte.MinValue, result.S8);
        Assert.Equal(short.MinValue, result.S16);
        Assert.Equal(int.MinValue, result.S32);
        Assert.Equal(long.MinValue, result.S64);

        Assert.Equal(byte.MaxValue, result.U8);
        Assert.Equal(ushort.MaxValue, result.U16);
        Assert.Equal(uint.MaxValue, result.U32);
        Assert.Equal(ulong.MaxValue, result.U64);

        Assert.Equal(float.MaxValue, result.F32);
        Assert.Equal(double.MaxValue, result.F64);
        Assert.Equal(decimal.MaxValue, result.Decimal);

        Assert.Equal(TimeSpan.Parse("20:30", CultureInfo.InvariantCulture), result.TimeSpan);
        Assert.Equal(DateTime.UnixEpoch.ToLocalTime(), result.DateTime);
        Assert.Equal(DateTimeOffset.UnixEpoch, result.DateTimeOffset);

        Assert.True(
            this.jsonParser.TryParse(
                """
                {
                    "s8": -15,
                    "s16": -15,
                    "s32": -15,
                    "s64": -15,
                    "u8": 15,
                    "u16": 15,
                    "u32": 15,
                    "u64": 15,
                    "f32": 15.2,
                    "f64": 15.2,
                    "decimal": 15.2,
                    "timespan": "30",
                    "datetime": "1971-01-01T00:00:00Z",
                    "datetimeoffset": "1971-01-01T00:00:00Z"
                }
                """,
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);

        Assert.Equal(-15, result.S8);
        Assert.Equal(-15, result.S16);
        Assert.Equal(-15, result.S32);
        Assert.Equal(-15, result.S64);

        Assert.Equal(15U, result.U8);
        Assert.Equal(15U, result.U16);
        Assert.Equal(15U, result.U32);
        Assert.Equal(15U, result.U64);

        Assert.Equal(15.2f, result.F32);
        Assert.Equal(15.2, result.F64);
        Assert.Equal(15.2m, result.Decimal);

        Assert.Equal(TimeSpanParser.Parse("30"), result.TimeSpan);
        Assert.Equal(DateTime.Parse("1971-01-01T00:00:00Z", CultureInfo.InvariantCulture), result.DateTime);
        Assert.Equal(DateTimeOffset.Parse("1971-01-01T00:00:00Z", CultureInfo.InvariantCulture), result.DateTimeOffset);
    }

    [Fact]
    public void IndividualInts()
    {
        Assert.True(this.jsonParser.TryParse<int>("12", out var int32, out var error));
        Assert.Null(error);
        Assert.Equal(12, int32);

        Assert.False(this.jsonParser.TryParse("h", out int32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Equal(0, int32);

        Assert.False(this.jsonParser.TryParse(string.Empty, out int32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Equal(0, int32);

        Assert.True(this.jsonParser.TryParse<int?>("12", out var nullableInt32, out error));
        Assert.Null(error);
        Assert.Equal(12, nullableInt32);

        Assert.False(this.jsonParser.TryParse("h", out nullableInt32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(nullableInt32);

        Assert.False(this.jsonParser.TryParse(string.Empty, out nullableInt32, out error));
        Assert.Equal("Failed to parse json.", error);
        Assert.Null(nullableInt32);

        Assert.True(this.jsonParser.TryParse("null", out nullableInt32, out error));
        Assert.Null(error);
        Assert.Null(nullableInt32);
    }

    [Fact]
    public void IndividualStrings()
    {
        Assert.False(this.jsonParser.TryParse<string>("null", out var stringValue, out var error));
        Assert.Equal("Was null. Should be of type string.", error);
        Assert.Null(stringValue);

        Assert.True(this.jsonParser.TryParse("\"hello\"", out stringValue, out error));
        Assert.Null(error);
        Assert.Equal("hello", stringValue);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S101:Types should be named in PascalCase",
        Justification = "Test for exactly this."
    )]
    [SuppressMessage(
        "Naming",
        "CA1710:Identifiers should have correct suffix",
        Justification = "Consistency with other tests."
    )]
    private sealed class IDictionaryClass<TKey, TValue> : IDictionary<TKey, TValue>
        where TKey : notnull
    {
        private readonly Dictionary<TKey, TValue> dictionary = new();

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void Add(KeyValuePair<TKey, TValue> item) => this.dictionary.Add(item.Key, item.Value);

        public void Clear() => this.dictionary.Clear();

        public bool Contains(KeyValuePair<TKey, TValue> item) => this.dictionary.Contains(item);

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => throw new NotSupportedException();

        public bool Remove(KeyValuePair<TKey, TValue> item) => this.dictionary.Remove(item.Key);

        public int Count => this.dictionary.Count;

        public bool IsReadOnly => false;

        public void Add(TKey key, TValue value) => this.dictionary.Add(key, value);

        public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

        public bool Remove(TKey key) => this.dictionary.Remove(key);

        public bool TryGetValue(TKey key, out TValue value) => this.dictionary.TryGetValue(key, out value!);

        public TValue this[TKey key]
        {
            get => this.dictionary[key];
            set => this.dictionary[key] = value;
        }

        public ICollection<TKey> Keys => this.dictionary.Keys;

        public ICollection<TValue> Values => this.dictionary.Values;
    }

    [Fact]
    public void IndividualDictionary()
    {
        Assert.True(
            this.jsonParser.TryParse<Dictionary<string, string>>(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out var strings,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } }, strings);

        Assert.False(
            this.jsonParser.TryParse(
                """{"hello": "world", "kestrel": "toolbox", "hello": "failure"}""",
                out strings,
                out error
            )
        );
        Assert.Equal("Has duplicate key.", error);
        Assert.Null(strings);

        Assert.True(
            this.jsonParser.TryParse<SortedDictionary<string, string>>(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out var sortedStrings,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(
            new SortedDictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } },
            sortedStrings
        );

        Assert.False(
            this.jsonParser.TryParse(
                """{"hello": "world", "kestrel": "toolbox", "hello": "failure"}""",
                out sortedStrings,
                out error
            )
        );
        Assert.Equal("Has duplicate key.", error);
        Assert.Null(sortedStrings);

        Assert.True(
            this.jsonParser.TryParse<Dictionary<string, int>>(
                """{"hello": 1, "kestrel": 32}""",
                out var ints,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, int> { { "hello", 1 }, { "kestrel", 32 } }, ints);

        Assert.True(
            this.jsonParser.TryParse<Dictionary<string, CustomParseClass>>(
                """{"first": {"class": 22, "struct": 44}, "second": {"class": 11, "struct": 33}}""",
                out var customParsed,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new Dictionary<string, CustomParseClass>
            {
                {
                    "first",
                    new CustomParseClass
                    {
                        Class = new CustomParseClass.CustomValueClass { Value = 22 },
                        Struct = new CustomParseClass.CustomValueStruct { Value = 44 },
                    }
                },
                {
                    "second",
                    new CustomParseClass
                    {
                        Class = new CustomParseClass.CustomValueClass { Value = 11 },
                        Struct = new CustomParseClass.CustomValueStruct { Value = 33 },
                    }
                },
            },
            customParsed
        );

        Assert.True(
            this.jsonParser.TryParse<IDictionaryClass<string, string>>(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out var expando,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } }, expando);

        Assert.True(
            this.jsonParser.TryParse<FrozenDictionary<string, string>>(
                """{"hello": "world", "kestrel": "toolbox"}""",
                out var frozen,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new Dictionary<string, string> { { "hello", "world" }, { "kestrel", "toolbox" } }, frozen);
    }

    [Fact]
    public void IndividualLists()
    {
        Assert.True(this.jsonParser.TryParse<List<string>>("""["a", "b", "c"]""", out var stringList, out var error));
        Assert.Null(error);
        Assert.Equal(new List<string> { "a", "b", "c" }, stringList);

        Assert.True(this.jsonParser.TryParse<string[]>("""["a", "b", "c"]""", out var stringArray, out error));
        Assert.Null(error);
        Assert.Equal(new[] { "a", "b", "c" }, stringArray);

        Assert.True(this.jsonParser.TryParse<List<int>>("""[4, 3, 2, 1]""", out var intList, out error));
        Assert.Null(error);
        Assert.Equal(new List<int> { 4, 3, 2, 1 }, intList);

        Assert.True(this.jsonParser.TryParse<int[]>("""[4, 1, 2, 3]""", out var intArray, out error));
        Assert.Null(error);
        Assert.Equal(new[] { 4, 1, 2, 3 }, intArray);

        Assert.False(this.jsonParser.TryParse("""[4, null, 2, 3]""", out intArray, out error));
        Assert.Equal("Was null. Should be of type int32.", error);
        Assert.Null(intArray);

        Assert.True(
            this.jsonParser.TryParse<List<CustomParseClass>>(
                """[{"class": 22, "struct": 44}, {"class": 11, "struct": 33}]""",
                out var customList,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new List<CustomParseClass>
            {
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 22 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = 44 },
                },
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 11 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = 33 },
                },
            },
            customList
        );

        Assert.True(
            this.jsonParser.TryParse<CustomParseClass[]>(
                """[{"class": 22, "struct": 44}, {"class": 11, "struct": 33}]""",
                out var customArray,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new List<CustomParseClass>
            {
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 22 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = 44 },
                },
                new()
                {
                    Class = new CustomParseClass.CustomValueClass { Value = 11 },
                    Struct = new CustomParseClass.CustomValueStruct { Value = 33 },
                },
            },
            customArray
        );

        Assert.True(this.jsonParser.TryParse<int?[]>("[1, 2, null]", out var nullableIntArray, out error));
        Assert.Null(error);
        Assert.Equal(new int?[] { 1, 2, null }, nullableIntArray);
    }

    [Fact]
    public void IndividualGeneral()
    {
        Assert.True(
            this.jsonParser.TryParse<Guid>("\"2aa36c42-2c81-4be8-8986-4f91733051b4\"", out var guid, out var error)
        );
        Assert.Null(error);
        Assert.Equal(Guid.Parse("2aa36c42-2c81-4be8-8986-4f91733051b4"), guid);

        Assert.True(
            this.jsonParser.TryParse<UUIDv1>("\"9c7668aa-0477-11ee-be56-0242ac120002\"", out var uuid, out error)
        );
        Assert.Null(error);
        Assert.Equal(UUIDv1.Parse("9c7668aa-0477-11ee-be56-0242ac120002"), uuid);

        Assert.True(this.jsonParser.TryParse<DateTime>("\"May 3, 2010\"", out var dateTime, out error));
        Assert.Null(error);
        Assert.Equal(DateTime.Parse("May 3, 2010", CultureInfo.InvariantCulture), dateTime);

        Assert.True(this.jsonParser.TryParse<TestEnum>("\"superLongName\"", out var testEnum, out error));
        Assert.Null(error);
        Assert.Equal(TestEnum.SuperLong, testEnum);

        Assert.True(this.jsonParser.TryParse<TestNumberEnum>("1", out var testNumberEnum, out error));
        Assert.Null(error);
        Assert.Equal(TestNumberEnum.B, testNumberEnum);

        Assert.True(this.jsonParser.TryParse<byte[]>("\"aGVsbG8=\"", out var bytes, out error));
        Assert.Null(error);
        Assert.Equal("hello"u8.ToArray(), bytes);
    }

    public class NewtonsoftJObjectClass
    {
        [Name("object")]
        public JObject? ObjectValue { get; set; }

        [Name("array")]
        [SkipSerialization(Condition.WhenNull)]
        public JArray? ArrayValue { get; set; }

        [Name("token")]
        public JToken? TokenValue { get; set; }

        public static bool TryParseJToken(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out JToken? value,
            [NotNullWhen(false)] out string? error
        )
        {
#pragma warning disable IDE0010
            switch (reader.TokenType)
#pragma warning restore IDE0010
            {
                case JsonTokenType.StartObject:
                {
                    using var doc = JsonDocument.ParseValue(ref reader);
                    var text = doc.RootElement.GetRawText();
                    value = JObject.Parse(text);
                    error = default;
                    return true;
                }

                case JsonTokenType.StartArray:
                {
                    using var doc = JsonDocument.ParseValue(ref reader);
                    var text = doc.RootElement.GetRawText();
                    value = JArray.Parse(text);
                    error = default;
                    return true;
                }

                case JsonTokenType.False:
                case JsonTokenType.True:
                case JsonTokenType.Number:
                case JsonTokenType.String:
                {
                    using var doc = JsonDocument.ParseValue(ref reader);
                    var text = doc.RootElement.GetRawText();
                    value = JToken.Parse(text);
                    error = default;
                    return true;
                }

                default:
                {
                    value = default;
                    error = "Unknown json type.";
                    return false;
                }
            }
        }

        public static bool TryParseJObject(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out JObject? value,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.StartObject)
            {
                value = default;
                error = "Was not an object.";
                return false;
            }

            using var doc = JsonDocument.ParseValue(ref reader);
            var text = doc.RootElement.GetRawText();
            value = JObject.Parse(text);
            error = default;
            return true;
        }

        public static bool TryParseJArray(
            ref Utf8JsonReader reader,
            [NotNullWhen(true)] out JArray? value,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.StartArray)
            {
                value = default;
                error = "Was not an array.";
                return false;
            }

            using var doc = JsonDocument.ParseValue(ref reader);
            var text = doc.RootElement.GetRawText();
            value = JArray.Parse(text);
            error = default;
            return true;
        }
    }

    [Fact]
    public void NewtonsoftJObject()
    {
        Assert.True(
            this.jsonParser.TryParse(
                """{"object":{"hello":"world","value":12},"array":[12,13,"hello",true],"token":"kestrel toolbox"}""",
                out NewtonsoftJObjectClass? value,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(value);
        Assert.Equal(new JObject { ["hello"] = "world", ["value"] = 12 }, value.ObjectValue);
        Assert.Equal(new JArray(12, 13, "hello", true), value.ArrayValue);
        Assert.Equal(new JValue("kestrel toolbox"), value.TokenValue);

        Assert.True(this.jsonParser.TryParse("""{"object":null,"token":null}""", out value, out error));
        Assert.Null(error);
        Assert.NotNull(value);
        Assert.Null(value.ObjectValue);
        Assert.Null(value.ArrayValue);
        Assert.Null(value.TokenValue);

        Assert.False(this.jsonParser.TryParse("""{"object":["array"]}""", out value, out error));
        Assert.Equal("object > Was not an object.", error);
        Assert.Null(value);

        Assert.False(this.jsonParser.TryParse("""{"array":{}}""", out value, out error));
        Assert.Equal("array > Was not an array.", error);
        Assert.Null(value);
    }

    [Fact]
    public void Comments()
    {
        Assert.True(
            this.jsonParser.TryParse<Int32Class>(
                """
                {
                    "value": 14,
                    // This is the first line of comment
                     "nullable": -1993,
                    "default":
                    // Second comment
                    188372,
                    "valid": 22,
                }
                """,
                out var jObject,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equivalent(
            new Int32Class
            {
                Value = 14,
                Default = 188372,
                Nullable = -1993,
                Valid = 22,
            },
            jObject
        );
    }

    private sealed class EmailAddressClass
    {
        [Name("value")]
        [EmailAddress]
        public required string Value { get; set; }
    }

    [Fact]
    public void EmailAddress()
    {
        Assert.True(
            this.jsonParser.TryParse<EmailAddressClass>(
                """{"value":"email@example.com"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("email@example.com", result.Value);

        Assert.False(this.jsonParser.TryParse("""{"value":"notarealemail"}""", out result, out error));
        Assert.Equal("'value' is not a valid email address.", error);
        Assert.Null(result);

        Assert.True(EmailAddressAttribute.IsValid("email@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("email+test@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("email+test+2@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("email+test+2@192.168.1.15"));

        Assert.True(EmailAddressAttribute.IsValid("customer/department=shipping@example.com"));
        Assert.True(EmailAddressAttribute.IsValid("!def!xyz%abc@example.com"));

        Assert.False(EmailAddressAttribute.IsValid("notarealemail"));
        Assert.False(EmailAddressAttribute.IsValid("this email has spacesss@example.com"));
        Assert.False(EmailAddressAttribute.IsValid("look!@my_ampersand@example.com"));

        // Controversial email addresses:
        Assert.False(EmailAddressAttribute.IsValid("Abc\\@def@example.com"));
    }

    private sealed class TrimClass
    {
        [Name("value")]
        [TrimValue]
        [StringLength(8)]
        public required string Value { get; set; }

        [Name("long")]
        [TrimValue(OnParse = false)]
        [StringLength(8)]
        public required string? Long { get; set; }
    }

    [Fact]
    public void Trim()
    {
        Assert.True(
            this.jsonParser.TryParse<TrimClass>(
                """{"value":"    hello    ","long":" world "}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal(" world ", result.Long);

        Assert.True(this.jsonParser.TryParse("""{"value":"01234567","long":"abcd"}""", out result, out error));
        Assert.Null(error);
        Assert.Equal("01234567", result.Value);
        Assert.Equal("abcd", result.Long);

        Assert.False(
            this.jsonParser.TryParse("""{"value":"    0123456    7","long":"short"}""", out result, out error)
        );
        Assert.Equal("'value' must be shorter than 8 characters.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse("""{"value":"hello","long":"     whitespace     "}""", out result, out error)
        );
        Assert.Equal("'long' must be shorter than 8 characters.", error);
        Assert.Null(result);
    }

    private sealed class NullOnEmptyClass
    {
        [Name("value")]
        [NullOnEmpty(OnSerialize = false)]
        [TrimValue]
        public string? Value { get; set; }

        [Name("emptyOnly")]
        [NullOnEmpty]
        public string? EmptyOnly { get; set; }
    }

    [Fact]
    public void NullOnEmptyTest()
    {
        Assert.True(
            this.jsonParser.TryParse<NullOnEmptyClass>(
                """{"value":"    hello    ","emptyOnly":"  world  "}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal("hello", result.Value);
        Assert.Equal("  world  ", result.EmptyOnly);

        Assert.True(this.jsonParser.TryParse("""{"value":"  ","emptyOnly":" "}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Equal(" ", result.EmptyOnly);

        Assert.True(this.jsonParser.TryParse("""{"value":"","emptyOnly":""}""", out result, out error));
        Assert.Null(error);
        Assert.Null(result.Value);
        Assert.Null(result.EmptyOnly);
    }

    private sealed class NullableStringClass
    {
        [Name("nullable")]
        [TrimValue]
        [StringLength(8)]
        public required string?[] Nullable { get; set; }

        [Name("exists")]
        [TrimValue]
        [StringLength(8)]
        public required string[] Exists { get; set; }

        [Name("notnull")]
        [AllowAnyString]
        public required string NotNull { get; set; }
    }

    [Fact]
    public void NullableString()
    {
        Assert.True(
            this.jsonParser.TryParse<NullableStringClass>(
                """{"nullable":["a", "b", null, "c"], "exists": ["a", "b", "c"], "notnull": "something"}""",
                out var result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.Equal(new[] { "a", "b", null, "c" }, result.Nullable);
        Assert.Equal(new[] { "a", "b", "c" }, result.Exists);
        Assert.Equal("something", result.NotNull);

        Assert.True(
            this.jsonParser.TryParse(
                """{"nullable":["a", "b  ", null, "c"], "exists": ["   a", "b", "c"], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Null(error);
        Assert.Equal(new[] { "a", "b", null, "c" }, result.Nullable);
        Assert.Equal(new[] { "a", "b", "c" }, result.Exists);
        Assert.Equal("something", result.NotNull);

        Assert.False(
            this.jsonParser.TryParse(
                """{"nullable":["a", "b", "c", "0123456789"], "exists": ["a", "b", "c"], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'nullable' must be shorter than 8 characters.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse(
                """{"nullable":["a", "b", "c"], "exists": ["a", "b", "c", null], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'exists' was null. Should be of type string.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse(
                """{"nullable":["a", "b", "c", null], "exists": ["a", "b", "c", null], "notnull": "something"}""",
                out result,
                out error
            )
        );
        Assert.Equal("'exists' was null. Should be of type string.", error);
        Assert.Null(result);

        Assert.False(
            this.jsonParser.TryParse(
                """{"nullable":["a", "b", "c", null], "exists": ["a", "b", "c"], "notnull": null}""",
                out result,
                out error
            )
        );
        Assert.Equal("'notnull' was null. Should be of type string.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "Testing this feature."
    )]
    public enum CustomParsingStringEnum
    {
        A,
        B,
        Other,
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "Testing this feature."
    )]
    public enum CustomParsingNumberEnum
    {
        A,
        B,
        Other,
    }

    [JsonSerializable(
        typeof(CustomParsingNumberEnum),
        ParsingMethodName = "TryParseNumber",
        CustomParsingMethodName = "CustomParsingEnumClass.TryParseNumberValue"
    )]
    [JsonSerializable(
        typeof(CustomParsingStringEnum),
        ParsingMethodName = "TryParseString",
        CustomParsingMethodName = "CustomParsingEnumSerialization.DoStringParsing"
    )]
    [JsonSerializable]
    public sealed partial class CustomParsingEnumClass
    {
        [Name("string")]
        public CustomParsingStringEnum String { get; set; }

        [Name("number")]
        public CustomParsingNumberEnum Number { get; set; }

        public static bool TryParseNumberValue(
            ref Utf8JsonReader reader,
            out CustomParsingNumberEnum value,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.Number)
            {
                value = default;
                error = $"Invalid {nameof(CustomParsingNumberEnum)}.";
                return false;
            }

            var i = reader.GetInt32();
            if (i is < 0 or > 2)
            {
                value = CustomParsingNumberEnum.Other;
            }
            else
            {
                value = (CustomParsingNumberEnum)i;
            }

            error = default;
            return true;
        }
    }

    [Fact]
    public void TestCustomParsingStringEnum()
    {
        Assert.True(this.jsonParser.TryParse<CustomParsingStringEnum>("\"a\"", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(CustomParsingStringEnum.A, result);

        Assert.True(this.jsonParser.TryParse("\"b\"", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingStringEnum.B, result);

        Assert.True(this.jsonParser.TryParse("\"whatever\"", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingStringEnum.Other, result);

        Assert.False(this.jsonParser.TryParse("72", out result, out error));
        Assert.Equal("CustomParsingStringEnum must be a string.", error);
        Assert.Equal(CustomParsingStringEnum.Other, result);

        Assert.False(this.jsonParser.TryParse("true", out result, out error));
        Assert.Equal("CustomParsingStringEnum must be a string.", error);
        Assert.Equal(CustomParsingStringEnum.Other, result);
    }

    [Fact]
    public void TestCustomParsingNumberEnum()
    {
        Assert.True(this.jsonParser.TryParse<CustomParsingNumberEnum>("0", out var result, out var error));
        Assert.Null(error);
        Assert.Equal(CustomParsingNumberEnum.A, result);

        Assert.True(this.jsonParser.TryParse("1", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingNumberEnum.B, result);

        Assert.True(this.jsonParser.TryParse("72", out result, out error));
        Assert.Null(error);
        Assert.Equal(CustomParsingNumberEnum.Other, result);

        Assert.False(this.jsonParser.TryParse("\"a\"", out result, out error));
        Assert.Equal("Invalid CustomParsingNumberEnum.", error);
        Assert.Equal(CustomParsingNumberEnum.A, result);

        Assert.False(this.jsonParser.TryParse("true", out result, out error));
        Assert.Equal("Invalid CustomParsingNumberEnum.", error);
        Assert.Equal(CustomParsingNumberEnum.A, result);
    }

    [Fact]
    public void TestCustomParsingEnumClass()
    {
        Assert.True(
            this.jsonParser.TryParse(
                """{"string":"B", "number":1}""",
                out CustomParsingEnumClass? result,
                out var error
            )
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(CustomParsingStringEnum.B, result.String);
        Assert.Equal(CustomParsingNumberEnum.B, result.Number);

        Assert.True(this.jsonParser.TryParse("""{"string":"whatever", "number":72}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(CustomParsingStringEnum.Other, result.String);
        Assert.Equal(CustomParsingNumberEnum.Other, result.Number);

        Assert.False(this.jsonParser.TryParse("""{"string":1, "number":1}""", out result, out error));
        Assert.Equal("string > CustomParsingStringEnum must be a string.", error);
        Assert.Null(result);

        Assert.False(this.jsonParser.TryParse("""{"string":"B", "number":"a"}""", out result, out error));
        Assert.Equal("number > Invalid CustomParsingNumberEnum.", error);
        Assert.Null(result);
    }

    [SuppressMessage(
        "Minor Code Smell",
        "S2344:Enumeration type names should not have \"Flags\" or \"Enum\" suffixes",
        Justification = "I'm testing enums so it's an appropriate name."
    )]
    [EnumSerializableSettings(InvalidValue = Unknown)]
    private enum InvalidValueEnum
    {
        [Name("a")]
        A,

        [Name("unknown")]
        Unknown,
    }

    private sealed class InvalidValueEnumClass
    {
        [Name("value")]
        public InvalidValueEnum Value { get; set; }
    }

    [Fact]
    public void TestInvalidValueEnum()
    {
        Assert.True(
            this.jsonParser.TryParse<InvalidValueEnumClass>("""{"value":"a"}""", out var result, out var error)
        );
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.A, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value":"unknown"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.Unknown, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value":"asdf"}""", out result, out error));
        Assert.Null(error);
        Assert.NotNull(result);
        Assert.Equal(InvalidValueEnum.Unknown, result.Value);
    }

    public static class CustomParsingEnumSerialization
    {
        public static bool DoStringParsing(
            ref Utf8JsonReader reader,
            out CustomParsingStringEnum result,
            [NotNullWhen(false)] out string? error
        )
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = "CustomParsingStringEnum must be a string.";
                result = CustomParsingStringEnum.Other;
                return false;
            }

            var value = reader.GetString()!;
            result = value.ToLowerInvariant() switch
            {
                "a" => CustomParsingStringEnum.A,
                "b" => CustomParsingStringEnum.B,
                _ => CustomParsingStringEnum.Other,
            };

            error = default;
            return true;
        }
    }

    public sealed class CustomPropertyParsingClass
    {
        [Name("value")]
        [JsonSerializableCustomProperty(
            ParseMethod = "UnitTests.Serialization.JsonParserTests.CustomPropertyParsingClass.TryParseInt"
        )]
        public required int? Value { get; set; }

        public static bool TryParseInt(ref Utf8JsonReader reader, out int value, [NotNullWhen(false)] out string? error)
        {
            if (reader.TokenType != JsonTokenType.String)
            {
                error = "Was not of type string.";
                value = default;
                return false;
            }

            var s = reader.GetString()!;
            if (!s.StartsWith("0x", StringComparison.Ordinal))
            {
                error = "Was not a hex string.";
                value = default;
                return false;
            }

            if (
                !int.TryParse(
                    s.AsSpan(2),
                    NumberStyles.HexNumber | NumberStyles.AllowHexSpecifier,
                    CultureInfo.InvariantCulture,
                    out value
                )
            )
            {
                error = "Was not parsable as hex.";
                value = default;
                return false;
            }

            error = null;
            return true;
        }
    }

    [Fact]
    public void CustomPropertyParsing()
    {
        Assert.True(
            this.jsonParser.TryParse("""{"value":"0xab"}""", out CustomPropertyParsingClass? result, out var error)
        );
        Assert.NotNull(result);
        Assert.Null(error);
        Assert.Equal(0xAB, result.Value);

        Assert.True(this.jsonParser.TryParse("""{"value":null}""", out result, out error));
        Assert.NotNull(result);
        Assert.Null(error);
        Assert.Null(result.Value);

        Assert.False(this.jsonParser.TryParse("""{"value":22}""", out result, out error));
        Assert.Null(result);
        Assert.Equal("value > Was not of type string.", error);

        Assert.False(this.jsonParser.TryParse("""{"value":"FF"}""", out result, out error));
        Assert.Null(result);
        Assert.Equal("value > Was not a hex string.", error);
    }

    public class TypedefOuterClass
    {
        [Name("value")]
        public required TypedefClass Value { get; set; }
    }

    [Typedef(typeof(string), TypedefFeatures.All)]
    public sealed partial class TypedefClass { }

    [Fact]
    public void TestTypedefClass()
    {
        Assert.True(this.jsonParser.TryParse("\"hello\"", out TypedefClass? inner, out var error));
        Assert.NotNull(inner);
        Assert.Null(error);
        Assert.Equal("hello", (string)inner);

        Assert.True(this.jsonParser.TryParse("""{ "value": "hello" }""", out TypedefOuterClass? outer, out error));
        Assert.NotNull(outer);
        Assert.Null(error);
        Assert.Equal("hello", (string)outer.Value);
    }

    [Typedef(typeof(string), TypedefFeatures.All)]
    public readonly partial struct TypedefStruct { }

    [Fact]
    public void TestTypedefStruct()
    {
        var exception = Assert.Throws<NotSupportedException>(
            () => this.jsonParser.TryParse("\"hello\"", out TypedefStruct? _, out _)
        );
        Assert.Equal(
            $"Nullable complex types are not supported. {typeof(TypedefStruct?).FullName} was used.",
            exception.Message
        );
    }

    [Fact]
    public void LargeNumberEnum()
    {
        var assemblyName = new AssemblyName(Guid.NewGuid().ToString());
        var builder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
        var moduleBuilder = builder.DefineDynamicModule(assemblyName.Name!);
        var enumBuilder = moduleBuilder.DefineEnum("LargeEnum", TypeAttributes.Public, typeof(int));

        const int MaxValue = 100_000;
        for (var i = 0; i <= MaxValue; i++)
        {
            if (i % 32 == 0)
            {
                continue;
            }

            _ = enumBuilder.DefineLiteral($"Value_{i}", i);
        }

        var type = enumBuilder.CreateType();
        var attributeConstructor = typeof(EnumSerializableSettingsAttribute).GetConstructor(
            BindingFlags.Public | BindingFlags.Instance,
            Type.EmptyTypes
        )!;
        var attributeProperty = typeof(EnumSerializableSettingsAttribute).GetProperty(
            nameof(EnumSerializableSettingsAttribute.SerializationType)
        )!;

        var customAttributeBuilder = new CustomAttributeBuilder(
            attributeConstructor,
            Array.Empty<object>(),
            new[] { attributeProperty },
            new object[] { EnumSerializableSettingsSerializationType.Numbers },
            Array.Empty<FieldInfo>(),
            Array.Empty<object>()
        );
        enumBuilder.SetCustomAttribute(customAttributeBuilder);

        var method = typeof(JsonParser)
            .GetMethods()
            .First(m =>
                m is { Name: "TryParse", ContainsGenericParameters: true }
                && m.GetParameters()[0].ParameterType == typeof(string)
            );
        var tryParseDelegate = method.MakeGenericMethod(type);

        for (var i = 0; i <= MaxValue; i++)
        {
            var arguments = new object?[] { i.ToString(CultureInfo.InvariantCulture), null, null };
            var success = (bool)tryParseDelegate.Invoke(this.jsonParser, arguments)!;
            var result = arguments[1];
            var error = arguments[2];
            Assert.Equal(i % 32 != 0, success);
            if (success)
            {
                Assert.Null(error);
                Assert.NotNull(result);
                Assert.Equal(i, (int)result);
            }
            else
            {
                Assert.NotNull(error);
                Assert.NotNull(result);
                Assert.Equal(0, (int)result);
            }
        }
    }

    [Fact]
    public void LargeEnum()
    {
        var assemblyName = new AssemblyName(Guid.NewGuid().ToString());
        var builder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
        var moduleBuilder = builder.DefineDynamicModule(assemblyName.Name!);
        var enumBuilder = moduleBuilder.DefineEnum("LargeEnum", TypeAttributes.Public, typeof(int));

        var nameAttributeConstructor = typeof(NameAttribute).GetConstructor(
            BindingFlags.Public | BindingFlags.Instance,
            new[] { typeof(string) }
        )!;

        const int MaxValue = 10_000;
        for (var i = 0; i <= MaxValue; i++)
        {
            var literal = enumBuilder.DefineLiteral($"Value_{i}", i);
            literal.SetCustomAttribute(
                new CustomAttributeBuilder(nameAttributeConstructor, new object[] { $"Value_{i}" })
            );
        }

        var type = enumBuilder.CreateType();
        var attributeConstructor = typeof(EnumSerializableSettingsAttribute).GetConstructor(
            BindingFlags.Public | BindingFlags.Instance,
            Type.EmptyTypes
        )!;

        enumBuilder.SetCustomAttribute(new CustomAttributeBuilder(attributeConstructor, Array.Empty<object>()));

        var method = typeof(JsonParser)
            .GetMethods()
            .First(m =>
                m is { Name: "TryParse", ContainsGenericParameters: true }
                && m.GetParameters()[0].ParameterType == typeof(string)
            );
        var tryParseDelegate = method.MakeGenericMethod(type);

        for (var i = 0; i <= MaxValue; i++)
        {
            var arguments = new object?[] { $"\"Value_{i}\"", null, null };
            var success = (bool)tryParseDelegate.Invoke(this.jsonParser, arguments)!;
            var result = arguments[1];
            var error = arguments[2];

            Assert.True(success);
            Assert.Null(error);
            Assert.NotNull(result);
            Assert.Equal(i, (int)result);
        }

        // Failure.
        {
            var arguments = new object?[] { "\"Value_0000\"", null, null };
            var success = (bool)tryParseDelegate.Invoke(this.jsonParser, arguments)!;
            var result = arguments[1];
            var error = arguments[2];

            Assert.False(success);
            Assert.NotNull(error);
            Assert.NotNull(result);
            Assert.Equal(0, (int)result);
        }
    }
}
#endif // !DISABLE_ROSLYN_GENERATED_TESTS
