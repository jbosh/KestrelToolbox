// <copyright file="TimeSpanParserTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Text;
using KestrelToolbox.Serialization;

namespace UnitTests;

public class TimeSpanParserTests
{
    [Theory]
    [InlineData("20", "00:00:20")]
    [InlineData("20.2", "00:00:20.2000000")]
    [InlineData("40:20:20.2", "1.16:20:20.2000000")]
    [InlineData("10:64", "11:04:00")]
    [InlineData("12d", "12.00:00:00")]
    [InlineData("12.2d", "12.04:48:00")]
    [InlineData("12.2day", "12.04:48:00")]
    [InlineData("12.2 days", "12.04:48:00")]
    [InlineData("97h", "4.01:00:00")]
    [InlineData("97hr", "4.01:00:00")]
    [InlineData("97hrs", "4.01:00:00")]
    [InlineData("97m", "01:37:00")]
    [InlineData("97.2min", "01:37:12")]
    [InlineData("97s", "00:01:37")]
    [InlineData("4min", "00:04:00")]
    [InlineData("4000ms", "00:00:04")]
    [InlineData("12.27 min", "00:12:16.2000000")]
    [InlineData("2.0105 sec", "00:00:02.0105000")]
    [InlineData("2200us", "00:00:00.0022000")]
    [InlineData("3750000ns", "00:00:00.0037500")]
    [InlineData("500 ms", "00:00:00.5000000")]
    [InlineData("2700 ms", "00:00:02.7000000")]
    [InlineData("12.5 y", "4562.12:00:00")]
    [InlineData("12.5 yr", "4562.12:00:00")]
    [InlineData("12.5 yrs", "4562.12:00:00")]
    [InlineData("12.5 years", "4562.12:00:00")]
    [InlineData("0", "00:00:00")]
    [InlineData("00:00:01.8759205", "00:00:01.8759205")]
    public void TestSuccess(string value, string expected)
    {
        Assert.True(TimeSpanParser.TryParse(value, out var timeSpan));
        Assert.Equal(expected, timeSpan.ToString());

        Assert.True(TimeSpanParser.TryParse(Encoding.UTF8.GetBytes(value), out timeSpan));
        Assert.Equal(expected, timeSpan.ToString());
    }

    [Theory]
    [InlineData("10:a")]
    [InlineData("12b")]
    [InlineData("2asec")]
    [InlineData("10:12sec")]
    [InlineData("10:12 min")]
    [InlineData("5mm")]
    public void TestFailure(string value)
    {
        Assert.False(TimeSpanParser.TryParse(value, out var timeSpan));
        Assert.Equal(default, timeSpan);
        _ = Assert.Throws<FormatException>(() => TimeSpanParser.Parse(value));
    }
}
