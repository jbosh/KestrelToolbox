// <copyright file="ConsoleTestOutputHelper.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace UnitTests;

public class ConsoleTestOutputHelper : ITestOutputHelper
{
    public void WriteLine(string message) => Console.Out.WriteLine(message);

    public void WriteLine(string format, params object[] args) => Console.Out.WriteLine(format, args);
}
