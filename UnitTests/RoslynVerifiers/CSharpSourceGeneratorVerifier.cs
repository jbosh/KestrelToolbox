// <copyright file="CSharpSourceGeneratorVerifier.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Globalization;
using System.IO;
using System.Reflection;
using System.Reflection.Metadata;
using System.Runtime.Loader;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace UnitTests.RoslynVerifiers;

internal static class CSharpSourceGeneratorVerifier<TSourceGenerator>
    where TSourceGenerator : IIncrementalGenerator, new()
{
    public static Compilation VerifyGeneratorAsync(string source) =>
        VerifyGeneratorAsync(source, Array.Empty<GeneratedDiagnostic>(), Array.Empty<GeneratedSource>());

    public static Compilation VerifyGeneratorAsync(string source, params GeneratedSource[] generatedSources) =>
        VerifyGeneratorAsync(source, Array.Empty<GeneratedDiagnostic>(), generatedSources);

    public static Compilation VerifyGeneratorAsync(string source, params GeneratedDiagnostic[] expectedDiagnostics) =>
        VerifyGeneratorAsync(source, expectedDiagnostics, Array.Empty<GeneratedSource>());

    public static Compilation VerifyGeneratorAsync(
        string source,
        GeneratedDiagnostic expectedDiagnostics,
        params GeneratedSource[] expectedGeneratedSources
    ) => VerifyGeneratorAsync(source, new[] { expectedDiagnostics }, expectedGeneratedSources);

    public static Compilation VerifyGeneratorAsync(
        string source,
        GeneratedDiagnostic[] expectedDiagnostics,
        GeneratedSource[] expectedGeneratedSources
    )
    {
        var references = AppDomain
            .CurrentDomain.GetAssemblies()
            .Where(assembly => !assembly.IsDynamic && !string.IsNullOrWhiteSpace(assembly.Location))
            .Select(assembly => MetadataReference.CreateFromFile(assembly.Location))
            .Cast<MetadataReference>()
            .ToArray();

        return VerifyGeneratorAsync(source, expectedDiagnostics, expectedGeneratedSources, references);
    }

    public static Compilation VerifyGeneratorAsync(
        string source,
        GeneratedDiagnostic[] expectedDiagnostics,
        GeneratedSource[] expectedGeneratedSources,
        MetadataReference[] metadataReferences
    )
    {
        var compilation = CSharpCompilation.Create(
            Guid.NewGuid().ToString(),
            new[] { CSharpSyntaxTree.ParseText(source) },
            metadataReferences,
            new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary)
        );

        var generator = new TSourceGenerator();

#pragma warning disable S3220
        var driver = CSharpGeneratorDriver.Create(generator);
#pragma warning restore S3220

        _ = driver.RunGeneratorsAndUpdateCompilation(compilation, out var outputCompilation, out var actualDiagnostics);

        var actualGeneratedSources = outputCompilation
            .SyntaxTrees.Skip(1)
            .Select(t => new GeneratedSource(t.FilePath, t.GetText().ToString().Trim()))
            .ToArray();

        Assert.Equal(
            expectedDiagnostics,
            actualDiagnostics.Select(d => new GeneratedDiagnostic(
                d.Id,
                d.Severity,
                d.GetMessage(CultureInfo.InvariantCulture)
            ))
        );
        Assert.Equal(expectedGeneratedSources.Select(s => s.FilePath), actualGeneratedSources.Select(s => s.FilePath));
        for (var i = 0; i < expectedGeneratedSources.Length; i++)
        {
            var expectedSource = expectedGeneratedSources[i].Content;
            var actualSource = actualGeneratedSources[i].Content;
            Assert.Equal(expectedSource, actualSource);
        }

        return outputCompilation;
    }

    /// <summary>
    /// Compiles code into an assembly.
    /// </summary>
    /// <param name="compilation">Compilation unit to use.</param>
    /// <returns>The loaded assembly.</returns>
    /// <remarks>
    /// This code will fail on any errors or warnings.
    /// </remarks>
    public static Assembly LoadAssembly(Compilation compilation)
    {
        using var memoryStream = new MemoryStream();
        var emitResult = compilation.Emit(memoryStream);
        Assert.Empty(emitResult.Diagnostics);
        Assert.True(emitResult.Success);

        var assembly = Assembly.Load(memoryStream.ToArray());
        return assembly;
    }

    /// <summary>
    /// Compiles code into a <see cref="MetadataReference"/>.
    /// </summary>
    /// <param name="compilation">Compilation unit to use.</param>
    /// <returns>The loaded assembly.</returns>
    /// <remarks>
    /// This code will fail on any errors or warnings.
    /// </remarks>
    public static unsafe MetadataReference LoadMetadataReference(Compilation compilation)
    {
        var assembly = LoadAssembly(compilation);

        if (!assembly.TryGetRawMetadata(out var blob, out var length))
        {
            throw new Exception("Failed to load metadata.");
        }

        var moduleMetadata = ModuleMetadata.CreateFromMetadata((nint)blob, length);
        var assemblyMetadata = AssemblyMetadata.Create(moduleMetadata);
        return assemblyMetadata.GetReference();
    }

#if DEBUG
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Style",
        "IDE0051:Remove unused private members",
        Justification = "Debug only."
    )]
    private static string GenerateTestSnapshot(GeneratedSource[] sources)
    {
        var builder = new System.Text.StringBuilder();
        foreach (var source in sources)
        {
            _ = builder
                .AppendLine("new(")
                .AppendLine(CultureInfo.InvariantCulture, $"\"{source.FilePath}\",")
                .AppendLine("            // Language=C#")
                .AppendLine("            \"\"\"")
                .AppendLine(string.Join("\n", source.Content.Split('\n').Select(s => new string(' ', 12) + s)))
                .AppendLine("            \"\"\"")
                .AppendLine("),");
        }

        return builder.ToString();
    }
#endif // DEBUG
}

public record GeneratedDiagnostic(string Id, DiagnosticSeverity Severity, string Message);

public record GeneratedSource(string FilePath, string Content);

internal sealed class HotAssemblyLoadContext : AssemblyLoadContext
{
    public HotAssemblyLoadContext()
        : base("HotAssemblyLoadContext", isCollectible: true) { }

    /// <summary>
    /// Compiles code into an assembly.
    /// </summary>
    /// <param name="compilation">Compilation unit to use.</param>
    /// <returns>The loaded assembly.</returns>
    /// <remarks>
    /// This code will fail on any errors or warnings.
    /// </remarks>
    public Assembly LoadAssembly(Compilation compilation)
    {
        using var memoryStream = new MemoryStream();
        var emitResult = compilation.Emit(memoryStream);
        Assert.Empty(emitResult.Diagnostics);
        Assert.True(emitResult.Success);

        memoryStream.Position = 0;
        var assembly = this.LoadFromStream(memoryStream);
        return assembly;
    }

    /// <summary>
    /// Compiles code into a <see cref="MetadataReference"/>.
    /// </summary>
    /// <param name="compilation">Compilation unit to use.</param>
    /// <returns>The loaded assembly.</returns>
    /// <remarks>
    /// This code will fail on any errors or warnings.
    /// </remarks>
    public unsafe MetadataReference LoadMetadataReference(Compilation compilation)
    {
        var assembly = this.LoadAssembly(compilation);

        if (!assembly.TryGetRawMetadata(out var blob, out var length))
        {
            throw new Exception("Failed to load metadata.");
        }

        var moduleMetadata = ModuleMetadata.CreateFromMetadata((nint)blob, length);
        var assemblyMetadata = AssemblyMetadata.Create(moduleMetadata);
        return assemblyMetadata.GetReference();
    }

    /// <inheritdoc />
    protected override Assembly? Load(AssemblyName assemblyName) =>
        Default.Assemblies.FirstOrDefault(a => a.FullName == assemblyName.FullName);
}
