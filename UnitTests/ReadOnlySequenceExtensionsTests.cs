// <copyright file="ReadOnlySequenceExtensionsTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Extensions.ReadOnlySequenceExtensions;

namespace UnitTests;

public class ReadOnlySequenceExtensionsTests
{
    [Fact]
    public void Basic()
    {
        Assert.True(SequenceUtilities.CreateSequence("he", "llo").SequenceEqualsUTF8("hello"));
        Assert.True(SequenceUtilities.CreateSequence("hello").SequenceEqualsUTF8("hello"));
        Assert.True(SequenceUtilities.CreateSequence("he", "llo").SequenceEqualsASCII("hello"));
        Assert.True(SequenceUtilities.CreateSequence("hello").SequenceEqualsASCII("hello"));

        Assert.False(SequenceUtilities.CreateSequence("he", "llo").SequenceEqualsUTF8("hello2"));
        Assert.False(SequenceUtilities.CreateSequence("hello2").SequenceEqualsUTF8("hello"));
        Assert.False(SequenceUtilities.CreateSequence("he", "llo2").SequenceEqualsASCII("hello"));
        Assert.False(SequenceUtilities.CreateSequence("hello").SequenceEqualsASCII("hello2"));

        Assert.True(
            SequenceUtilities.CreateSequence("he", "llo2").Slice(0, "hello".Length).SequenceEqualsASCII("hello")
        );

        Assert.True("hello"u8.ToArray().AsSpan().SpanEqualsASCII("hello"));

        var value = SequenceUtilities.CreateSequence("he", "llo2").ToUTF8String();
        Assert.Equal("hello2", value);

        const string LoremIpsum =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc viverra pharetra feugiat. In dignissim elit eget pulvinar egestas. Morbi gravida dictum neque, et consequat risus placerat eget. Cras sed turpis sed dui hendrerit dapibus a et metus. Praesent pharetra lectus odio. Fusce elementum, orci sit amet dapibus gravida, erat magna fringilla lectus, eu volutpat erat diam ac urna. Nullam tempor quam in malesuada egestas. Aliquam sed pretium sapien, ut ullamcorper nunc. Etiam molestie mi a magna hendrerit, quis ornare est vehicula. Ut quis aliquet nisi. Morbi sem lorem, efficitur ac nisl et, vehicula ornare libero. Pellentesque dapibus nisl eget tempor venenatis. Etiam eget auctor metus. Mauris mollis malesuada laoreet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas";
        value = SequenceUtilities.SplitIntoSequence(LoremIpsum, 16).ToUTF8String();
        Assert.Equal(LoremIpsum, value);
    }
}
