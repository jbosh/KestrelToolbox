// <copyright file="JsonObjectMergeTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Text.Json.Nodes;
using KestrelToolbox.Extensions.JsonObjectExtensions;

namespace UnitTests;

public class JsonObjectMergeTests
{
    [Fact]
    public void ArrayMerge()
    {
        var a = new JsonObject { ["value"] = new JsonArray("a", "b", "c") };
        var b = new JsonObject { ["value"] = new JsonArray("d", "e", "f", "g") };

        a.Merge(b, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Merge });

        Assert.Equal(new JsonObject { ["value"] = new JsonArray("d", "e", "f", "g") }.ToJsonString(), a.ToJsonString());
    }

    [Fact]
    public void ArrayUnion()
    {
        var a = new JsonObject { ["value"] = new JsonArray("a", "b", "c") };
        var b = new JsonObject { ["value"] = new JsonArray("a", "b", "c", "d", "e", "f", "g") };

        a.Merge(b, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Union });

        Assert.Equal(
            new JsonObject { ["value"] = new JsonArray("a", "b", "c", "d", "e", "f", "g") }.ToJsonString(),
            a.ToJsonString()
        );
    }

    [Fact]
    public void ArrayConcat()
    {
        var a = new JsonObject { ["value"] = new JsonArray("a", "b", "c") };
        var b = new JsonObject { ["value"] = new JsonArray("a", "b", "c", "d", "e", "f", "g") };

        a.Merge(b, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Concat });

        Assert.Equal(
            new JsonObject
            {
                ["value"] = new JsonArray("a", "b", "c", "a", "b", "c", "d", "e", "f", "g"),
            }.ToJsonString(),
            a.ToJsonString()
        );
    }

    [Fact]
    public void ArrayReplace()
    {
        var a = new JsonObject { ["value"] = new JsonArray("a", "b", "c") };
        var b = new JsonObject { ["value"] = new JsonArray("d", "e", "f", "g") };

        a.Merge(b, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Replace });

        Assert.Equal(new JsonObject { ["value"] = new JsonArray("d", "e", "f", "g") }.ToJsonString(), a.ToJsonString());
    }

    [Fact]
    public void Objects()
    {
        var a = new JsonObject
        {
            ["value"] = new JsonObject
            {
                ["a"] = 22,
                ["b"] = new JsonObject { ["c"] = 45, ["d"] = "ddddd" },
                ["d"] = null,
            },
            ["other"] = new JsonArray("a", "b", 3),
        };
        var b = new JsonObject
        {
            ["value"] = new JsonObject
            {
                ["a"] = 55,
                ["b"] = new JsonObject { ["a"] = 22, ["b"] = "bbbbb" },
                ["c"] = "hello",
                ["e"] = null,
            },
            ["second"] = new JsonArray(14.4, "big string"),
        };

        a.Merge(b);

        Assert.Equal(
            new JsonObject
            {
                ["value"] = new JsonObject
                {
                    ["a"] = 55,
                    ["b"] = new JsonObject
                    {
                        ["c"] = 45,
                        ["d"] = "ddddd",
                        ["a"] = 22,
                        ["b"] = "bbbbb",
                    },
                    ["d"] = null,
                    ["c"] = "hello",
                },
                ["other"] = new JsonArray("a", "b", 3),
                ["second"] = new JsonArray(14.4, "big string"),
            }.ToJsonString(),
            a.ToJsonString()
        );
    }

    [Fact]
    public void KeepNull()
    {
        var a = new JsonObject
        {
            ["value"] = new JsonObject
            {
                ["a"] = 22,
                ["b"] = new JsonObject { ["c"] = 45, ["d"] = "ddddd" },
                ["d"] = null,
            },
            ["other"] = new JsonArray("a", "b", 3),
        };
        var b = new JsonObject
        {
            ["value"] = new JsonObject
            {
                ["a"] = 55,
                ["b"] = new JsonObject { ["a"] = 22, ["b"] = "bbbbb" },
                ["c"] = "hello",
                ["e"] = null,
            },
            ["second"] = new JsonArray(14.4, "big string"),
        };

        a.Merge(b, new JsonMergeSettings { MergeNullValueHandling = MergeNullValueHandling.Keep });

        Assert.Equal(
            new JsonObject
            {
                ["value"] = new JsonObject
                {
                    ["a"] = 55,
                    ["b"] = new JsonObject
                    {
                        ["c"] = 45,
                        ["d"] = "ddddd",
                        ["a"] = 22,
                        ["b"] = "bbbbb",
                    },
                    ["d"] = null,
                    ["c"] = "hello",
                    ["e"] = null,
                },
                ["other"] = new JsonArray("a", "b", 3),
                ["second"] = new JsonArray(14.4, "big string"),
            }.ToJsonString(),
            a.ToJsonString()
        );
    }
}
