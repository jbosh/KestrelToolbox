// <copyright file="AtomicULongTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Types.Atomic;

namespace UnitTests;

public class AtomicULongTests
{
    [Fact]
    public void Basic()
    {
        var value = new AtomicULong(22);
        Assert.Equal(22UL, value.Load());
        value = new AtomicULong();
        Assert.Equal(0UL, value.Load());
        value.Store(33);
        Assert.Equal(33UL, value.Load());
        Assert.Equal(33UL, value.Exchange(47));
        Assert.Equal(47UL, value.Load());

        Assert.False(value.CompareExchange(1, 86));

        var expected = 0UL;
        Assert.False(value.CompareExchange(ref expected, 86));
        Assert.Equal(47UL, expected);
        Assert.True(value.CompareExchange(ref expected, 86));
        Assert.Equal(47UL, expected);
        Assert.False(value.CompareExchange(ref expected, 113));
        Assert.Equal(86UL, expected);
        Assert.True(value.CompareExchange(ref expected, 0));
        Assert.Equal(86UL, expected);

        Assert.Equal(0UL, value.FetchAdd(66));
        Assert.Equal(66UL, value.FetchAdd(12));
        Assert.Equal(78UL, value.FetchSub(467));
        Assert.Equal(18446744073709551227, value.FetchSub(0xFFFFFFCA));
        Assert.Equal(18446744069414583985, value.Increment());
        Assert.Equal(18446744069414583986, value.Decrement());

        value.Store(0);
        Assert.Equal(0UL, value.Load());
        Assert.Equal(0UL, value.FetchOr(2));
        Assert.Equal(2UL, value.FetchOr(4));
        Assert.Equal(6UL, value.FetchOr(2));
        Assert.Equal(6UL, value.Load());

        Assert.Equal(6UL, value.FetchAnd(2));
        Assert.Equal(2UL, value.FetchAnd(2));
        Assert.Equal(2UL, value.Load());

        value.Store(ulong.MaxValue);
        Assert.Equal(ulong.MaxValue, value.Decrement());
        Assert.Equal(ulong.MaxValue - 1, value.Increment());
        Assert.Equal(ulong.MaxValue, value.Increment());
        Assert.Equal(ulong.MinValue, value.Load());
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringBasic()
    {
        var positiveValue = 1679UL;
        var negativeValue = 0xFFFFFFFFFFFFF41BUL;

        var positiveAtomic = new AtomicULong(positiveValue);
        var negativeAtomic = new AtomicULong(negativeValue);

        Assert.Equal(positiveValue.ToString(), positiveAtomic.ToString());
        Assert.Equal(negativeValue.ToString(), negativeAtomic.ToString());
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringFormat()
    {
        var positiveValue = 1679UL;
        var negativeValue = 0xFFFFFFFFFFFFF41BUL;

        var positiveAtomic = new AtomicULong(positiveValue);
        var negativeAtomic = new AtomicULong(negativeValue);

        var specifiers = new[] { "G", "C", "D8", "E2", "F", "N", "N0", "P", "X8" };

        foreach (var specifier in specifiers)
        {
            Assert.Equal(positiveValue.ToString(specifier), positiveAtomic.ToString(specifier));
            Assert.Equal(negativeValue.ToString(specifier), negativeAtomic.ToString(specifier));
        }
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringProvider()
    {
        var english = CultureInfo.CreateSpecificCulture("en-US");
        var french = CultureInfo.CreateSpecificCulture("fr-FR");
        var spanish = CultureInfo.CreateSpecificCulture("es-ES");

        var positiveAtomic = new AtomicULong(1679);
        var negativeAtomic = new AtomicULong(0xFFFFFFFFFFFFF41BUL);

        Assert.Equal("1679", positiveAtomic.ToString(english));
        Assert.Equal("1679", positiveAtomic.ToString(french));
        Assert.Equal("1679", positiveAtomic.ToString(spanish));

        Assert.Equal("18446744073709548571", negativeAtomic.ToString(english));
        Assert.Equal("18446744073709548571", negativeAtomic.ToString(french));
        Assert.Equal("18446744073709548571", negativeAtomic.ToString(spanish));
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringFormatAndProvider()
    {
        var positiveValue = 1679UL;
        var negativeValue = 0xFFFFF41BUL;

        var positiveAtomic = new AtomicULong(positiveValue);
        var negativeAtomic = new AtomicULong(negativeValue);

        var cultures = new[]
        {
            CultureInfo.CreateSpecificCulture("en-US"),
            CultureInfo.CreateSpecificCulture("fr-FR"),
            CultureInfo.CreateSpecificCulture("es-ES"),
        };

        var specifiers = new[] { "G", "C", "D8", "E2", "F", "N", "N0", "P", "X8" };
        foreach (var specifier in specifiers)
        {
            foreach (var culture in cultures)
            {
                Assert.Equal(positiveValue.ToString(specifier, culture), positiveAtomic.ToString(specifier, culture));
                Assert.Equal(negativeValue.ToString(specifier, culture), negativeAtomic.ToString(specifier, culture));
            }
        }
    }

    [Fact]
    [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global", Justification = "That's what we're testing.")]
    public void EqualsTest()
    {
        var a = new AtomicULong();
        var b = new AtomicULong();
        Assert.True(a.Equals(b));
        a.Store(2);
        Assert.False(a.Equals(b));
        b.Store(2);
        Assert.True(a.Equals(b));

        Assert.False(a.Equals(null));
        Assert.False(a.Equals(new AtomicBool()));

        Assert.Equal(a.Load().GetHashCode(), a.GetHashCode());
    }
}
