// <copyright file="WebHostBuilderConfigurationTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using KestrelToolbox.Extensions;
using KestrelToolbox.Extensions.PipeReaderExtensions;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Types;
using KestrelToolbox.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

#if !DISABLE_ROSLYN_GENERATED_TESTS
namespace UnitTests;

public partial class WebHostBuilderConfigurationTests
{
#if NET9_0_OR_GREATER
    // Multiple ports because multiple dotnet tests run simultaneously per version.
    private const int PortNumber = 8000;
#else
    private const int PortNumber = 8001;
#endif // NET9_0_OR_GREATER

    [Fact]
    public async Task TestRoutes()
    {
        var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber, UseDeveloperExceptionPage = true };
        configuration.AddRoutes(ServerRoutes.GetRoutes());

        using var server = configuration.CreateWebHost();
        await server.StartAsync();

        using var client = new HttpClient();

        var value = await client.GetStringAsync($"http://localhost:{PortNumber}/test");
        Assert.Equal("No Value", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test2");
        Assert.Equal("No Value", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/20");
        Assert.Equal("20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/hello");
        Assert.Equal("hello", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/20/whatever/20057500200700237");
        Assert.Equal("20,whatever,20057500200700237", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/boolean/true");
        Assert.Equal("bool:True", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/boolean/false");
        Assert.Equal("bool:False", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/float/20");
        Assert.Equal("float:20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/float/20.2");
        Assert.Equal("float:20.2", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/double/20");
        Assert.Equal("double:20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/double/20.2");
        Assert.Equal("double:20.2", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/decimal/20");
        Assert.Equal("decimal:20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/decimal/20.2");
        Assert.Equal("decimal:20.2", value);

        var guidValue = Guid.NewGuid();
        var uuidValue = UUIDv1.NewUUIDv1();
        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/guid/{guidValue}/{uuidValue}");
        Assert.Equal($"Guid Route: {guidValue} {uuidValue}", value);

        var exception = await Assert.ThrowsAsync<HttpRequestException>(
            () => client.GetStringAsync($"http://localhost:{PortNumber}/test/guid/{uuidValue}/{guidValue}")
        );
        Assert.Equal(HttpStatusCode.BadRequest, exception.StatusCode);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/wildcard/hello/world/how/are/you");
        Assert.Equal("hello/world/how/are/you", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/wildcard");
        Assert.Equal(string.Empty, value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/slugs/hello/world");
        Assert.Equal("hello, world", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/slugs/hello");
        Assert.Equal("hello, ", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/slugs");
        Assert.Equal("slugs", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/slugs/hello/world/goodbye");
        Assert.Equal("hello, world/goodbye", value);

        using (
            var response = await client.SendAsync(
                new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/not_found")
            )
        )
        {
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        using (var request = new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/test/stale"))
        {
            request.Headers.IfNoneMatch.Add(new EntityTagHeaderValue("\"1234\""));
            using var response = await client.SendAsync(request);

            var text = await response.Content.ReadAsStringAsync();
            Assert.Equal("Stale", text);
        }

        using (var request = new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/test/stale"))
        {
            request.Headers.IfNoneMatch.Add(new EntityTagHeaderValue("\"abcd\""));
            using var response = await client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NotModified, response.StatusCode);
        }

        using (var request = new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/test/stale"))
        {
            _ = request.Headers.TryAddWithoutValidation("If-None-Match", "abcd");
            using var response = await client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NotModified, response.StatusCode);
        }

        using (var request = new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/test/caching/true"))
        {
            using var response = await client.SendAsync(request);

            var cacheControl = response.Headers.CacheControl;
            Assert.NotNull(cacheControl);
            Assert.False(cacheControl.NoCache);
            Assert.Equal(TimeSpan.FromSeconds(30), cacheControl.MaxAge);
            Assert.True(cacheControl.Private);
            var text = await response.Content.ReadAsStringAsync();
            Assert.Equal("Content.", text);
        }

        using (
            var request = new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/test/caching/false")
        )
        {
            using var response = await client.SendAsync(request);

            var cacheControl = response.Headers.CacheControl;
            Assert.NotNull(cacheControl);
            Assert.False(cacheControl.NoCache);
            Assert.Equal(TimeSpan.FromSeconds(30), cacheControl.MaxAge);
            Assert.False(cacheControl.Private);
            var text = await response.Content.ReadAsStringAsync();
            Assert.Equal("Content.", text);
        }

        await server.StopAsync();
    }

    [Fact]
    public async Task TestRouteContentTypes()
    {
        var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber, UseDeveloperExceptionPage = true };
        configuration.AddRoutes(ServerRoutes.GetRoutes());

        using var server = configuration.CreateWebHost();
        await server.StartAsync();

        using var client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("custom/kestrel-toolbox"));

        await SendPost("single", new MediaTypeHeaderValue("text/plain"), HttpStatusCode.UnsupportedMediaType);
        await SendPost("single", new MediaTypeHeaderValue("custom/kestrel-toolbox"), HttpStatusCode.OK);
        await SendPost("single", new MediaTypeHeaderValue("CUSTOM/KESTREL-TOOLBOX"), HttpStatusCode.OK);
        await SendPost("single", new MediaTypeHeaderValue("custom/kestrel-toolbox", "utf8"), HttpStatusCode.OK);

        await SendPost("multiple", new MediaTypeHeaderValue("text/plain"), HttpStatusCode.UnsupportedMediaType);
        await SendPost("multiple", new MediaTypeHeaderValue("custom/kestrel-toolbox"), HttpStatusCode.OK);
        await SendPost("multiple", new MediaTypeHeaderValue("application/json"), HttpStatusCode.OK);

        await server.StopAsync();

        async Task SendPost(string url, MediaTypeHeaderValue contentType, HttpStatusCode expected)
        {
            var content = new StringContent("hello");
            content.Headers.ContentType = contentType;
            using var response = await client.PostAsync(
                $"http://localhost:{PortNumber}/test/content-type/{url}",
                content
            );
            Assert.Equal(expected, response.StatusCode);
        }
    }

    private static partial class ServerRoutes
    {
        [Route("/test")]
        [Route("/test2")]
        public static async Task TestNoValue(HttpContext context)
        {
            var response = context.Response;
            var dict = context.Request.RouteValues;
            Assert.Empty(dict);

            await response.WriteAsync("No Value");
        }

        [Route("test/{value:int}")]
        public static async Task TestSingle(HttpContext context, int value)
        {
            var response = context.Response;

            await response.WriteAsync($"{value}");
        }

        [Route("test/{value:alpha}")]
        public static async Task TestSingle(HttpContext context, string value)
        {
            var response = context.Response;

            await response.WriteAsync(value);
        }

        [Route("test/{value:int}/{value2:alpha}/{value3:long}")]
        public static async Task TestMultiple(HttpContext context, int value, string value2, long value3)
        {
            var response = context.Response;

            await response.WriteAsync($"{value},{value2},{value3}");
        }

        [Route("test/boolean/{value:bool}")]
        public static async Task TestBool(HttpContext context, bool value)
        {
            var response = context.Response;

            await response.WriteAsync($"bool:{value}");
        }

        [Route("/test/float/{value:float}")]
        public static async Task TestFloat(HttpContext context, float value)
        {
            var response = context.Response;

            await response.WriteAsync($"float:{value}");
        }

        [Route("/test/double/{value:double}")]
        public static async Task TestDouble(HttpContext context, double value)
        {
            var response = context.Response;

            await response.WriteAsync($"double:{value}");
        }

        [Route("/test/decimal/{value:decimal}")]
        public static async Task TestDecimal(HttpContext context, decimal value)
        {
            var response = context.Response;

            await response.WriteAsync($"decimal:{value}");
        }

        [Route("/test/guid/{guid:guid}/{uuid:guid}", methods: "GET")]
        public static async Task TestGuid(HttpContext context, Guid guid, UUIDv1 uuid)
        {
            var response = context.Response;

            await response.WriteAsync($"Guid Route: {guid} {uuid}");
        }

        [Route("/test/content-type/single", methods: "POST")]
        [AcceptContentTypes("custom/kestrel-toolbox")]
        public static async Task TestSingleContentType(HttpContext context)
        {
            var response = context.Response;

            await response.WriteAsync("success");
        }

        [Route("/test/content-type/multiple", methods: "POST")]
        [AcceptContentTypes("custom/kestrel-toolbox", "application/json")]
        public static Task TestMultipleContentType(HttpContext context)
        {
            var response = context.Response;

            return response.WriteAsync("success");
        }

        [Route("/test/stale")]
        public static async Task TestStaleExtensions(HttpContext context)
        {
            var request = context.Request;
            var response = context.Response;

            if (request.IsStale("abcd"))
            {
                await response.WriteAsync("Stale");
            }
            else
            {
                response.StatusCode = StatusCodes.Status304NotModified;
            }
        }

        [Route("/test/caching/{isPrivate:bool}")]
        public static async Task TestCachingExtensions(HttpContext context, bool isPrivate)
        {
            var response = context.Response;

            response.SetCacheControl(TimeSpan.FromSeconds(30), isPrivate);
            await response.WriteAsync("Content.");
        }

        [Route("test/wildcard/{**path}")]
        public static async Task TestWildcard(HttpContext context, string? path)
        {
            var response = context.Response;

            await response.WriteAsync(path ?? "null");
        }

        [Route("test/slugs/{slug0}/{*slug1}")]
        public static async Task TestSlugs(HttpContext context, string slug0, string slug1)
        {
            var response = context.Response;

            await response.WriteAsync($"{slug0}, {slug1}");
        }
    }

    [Fact]
    public async Task TestInstanceRoutes()
    {
        var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber, UseDeveloperExceptionPage = true };
        configuration.AddRoutes(new ServerInstanceRoutes().GetRoutes());

        using var server = configuration.CreateWebHost();
        await server.StartAsync();

        using var client = new HttpClient();

        var value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/20");
        Assert.Equal("20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/hello");
        Assert.Equal("hello", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/20/whatever/20057500200700237");
        Assert.Equal("20,whatever,20057500200700237", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/boolean/true");
        Assert.Equal("bool:True", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/boolean/false");
        Assert.Equal("bool:False", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/float/20");
        Assert.Equal("float:20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/float/20.2");
        Assert.Equal("float:20.2", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/double/20");
        Assert.Equal("double:20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/double/20.2");
        Assert.Equal("double:20.2", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/decimal/20");
        Assert.Equal("decimal:20", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/test/decimal/20.2");
        Assert.Equal("decimal:20.2", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/instance/get");
        Assert.Equal("0", value);

        value = await PostStringAsync(client, $"http://localhost:{PortNumber}/instance/set/14", null);
        Assert.Equal("14", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/instance/get");
        Assert.Equal("14", value);

        value = await PostStringAsync(client, $"http://localhost:{PortNumber}/instance/set", "48");
        Assert.Equal("48", value);

        value = await client.GetStringAsync($"http://localhost:{PortNumber}/instance/get");
        Assert.Equal("48", value);

        using (
            var response = await client.SendAsync(
                new HttpRequestMessage(HttpMethod.Get, $"http://localhost:{PortNumber}/not_found")
            )
        )
        {
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        await server.StopAsync();
    }

    private static async Task<string> PostStringAsync(
        HttpClient client,
        string url,
        string? value = null,
        string? contentType = null
    ) => (await PostStringAsyncWithStatus(client, url, value, contentType)).body;

    private static async Task<(int statusCode, string body)> PostStringAsyncWithStatus(
        HttpClient client,
        string url,
        string? value = default,
        string? contentType = default
    )
    {
        var request = new HttpRequestMessage(HttpMethod.Post, url);
        if (value != null)
        {
            request.Content = new StringContent(value);
            if (contentType != null)
            {
                request.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            }
        }

        var response = await client.SendAsync(request);
        var statusCode = (int)response.StatusCode;
        var body = await response.Content.ReadAsStringAsync();
        return (statusCode, body);
    }

    [SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Test class.")]
    private sealed partial class ServerInstanceRoutes
    {
        private int value;

        [Route("/test/{value:int}")]
        public async Task TestSingle(HttpContext context, int value)
        {
            var response = context.Response;

            await response.WriteAsync($"{value}");
        }

        [Route("/test/{value:alpha}")]
        public async Task TestSingle(HttpContext context, string value)
        {
            var response = context.Response;

            await response.WriteAsync(value);
        }

        [Route("/test/{value:int}/{value2:alpha}/{value3:long}")]
        public async Task TestMultiple(HttpContext context, int value, string value2, long value3)
        {
            var response = context.Response;

            await response.WriteAsync($"{value},{value2},{value3}");
        }

        [Route("/test/boolean/{value:bool}")]
        public async Task TestBool(HttpContext context, bool value)
        {
            var response = context.Response;

            await response.WriteAsync($"bool:{value}");
        }

        [Route("/test/float/{value:float}")]
        public async Task TestFloat(HttpContext context, float value)
        {
            var response = context.Response;

            await response.WriteAsync($"float:{value}");
        }

        [Route("/test/double/{value:double}")]
        public async Task TestDouble(HttpContext context, double value)
        {
            var response = context.Response;

            await response.WriteAsync($"double:{value}");
        }

        [Route("/test/decimal/{value:decimal}")]
        public async Task TestDecimal(HttpContext context, decimal value)
        {
            var response = context.Response;

            await response.WriteAsync($"decimal:{value}");
        }

        [Route("/instance/set/{value:int}", methods: "POST")]
        public Task TestSetInt(HttpContext context, int value)
        {
            var response = context.Response;

            this.value = value;
            return response.WriteAsync(value.ToString(CultureInfo.InvariantCulture));
        }

        [Route("/instance/set", methods: "POST")]
        public async Task TestSetIntPost(HttpContext context)
        {
            var request = context.Request;
            var response = context.Response;

            var readResult = await request.BodyReader.ReadToEndAsync();

            var buffer = readResult.Buffer;
            Assert.True(buffer.IsSingleSegment);
            Assert.True(readResult.IsCompleted);

            var s = Encoding.UTF8.GetString(buffer.FirstSpan);
            request.BodyReader.AdvanceTo(readResult.Buffer.End);
            if (!int.TryParse(s, out var value))
            {
                response.StatusCode = 400;
                return;
            }

            this.value = value;
            await response.WriteAsync(s);
        }

        [Route("/instance/get")]
        public async Task TestSingleInstance(HttpContext context)
        {
            var response = context.Response;

            await response.WriteAsync(this.value.ToString(CultureInfo.InvariantCulture));
        }
    }

    [Fact]
    public async Task TestJson()
    {
        var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber, UseDeveloperExceptionPage = true };
        configuration.AddRoutes(ServerJsonRoutes.GetRoutes());

        using var server = configuration.CreateWebHost();
        await server.StartAsync();

        using var client = new HttpClient();
        var result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/test",
            """{"value": 2}""",
            "application/json"
        );
        Assert.Equal("2", result);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/test",
            """{"value": 53, "delay": 1000}""",
            "application/json"
        );
        Assert.Equal("53", result);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/test",
            """{"value": 53, "delay": 1000, "exception": "hello"}""",
            "application/json"
        );
        Assert.StartsWith("System.Exception: ", result);

        var (status, body) = await PostStringAsyncWithStatus(
            client,
            $"http://localhost:{PortNumber}/testMime",
            """{"value": 2}""",
            "application/json"
        );
        Assert.Equal(StatusCodes.Status415UnsupportedMediaType, status);
        Assert.Equal("Unsupported content type.", body);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/testMime",
            """{"value": 2}""",
            "test/json"
        );
        Assert.Equal("2", result);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/testMime",
            """{"value": 53, "delay": 1000}""",
            "test/json"
        );
        Assert.Equal("53", result);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/testMime",
            """{"value": 53, "delay": 1000, "exception": "hello"}""",
            "test/json"
        );
        Assert.StartsWith("System.Exception: ", result);

        await server.StopAsync();
    }

    [Fact]
    public async Task TestJsonInstance()
    {
        var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber, UseDeveloperExceptionPage = true };
        configuration.AddRoutes(new ServerJsonRoutesInstance().GetRoutes());

        using var server = configuration.CreateWebHost();
        await server.StartAsync();

        using var client = new HttpClient();
        var result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/test",
            "{\"value\": 2}",
            "application/json"
        );
        Assert.Equal("2, 12", result);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/test",
            "{\"value\": 53, \"delay\": 1000}",
            "application/json"
        );
        Assert.Equal("53, 12", result);

        result = await PostStringAsync(
            client,
            $"http://localhost:{PortNumber}/test",
            "{\"value\": 53, \"delay\": 1000, \"exception\": \"world\"}",
            "application/json"
        );
        Assert.StartsWith("System.Exception: ", result);

        await server.StopAsync();
    }

    public class JsonPayload
    {
        [Name("value")]
        public int Value { get; set; }

        [Name("delay")]
        public int Delay { get; set; }

        [Name("exception")]
        public string? Exception { get; set; }
    }

    private static partial class ServerJsonRoutes
    {
        [Route("/test", methods: "POST")]
        private static async Task Test(HttpContext context, [JsonBody] JsonPayload json)
        {
            var response = context.Response;

            if (json.Delay != 0)
            {
                await Task.Delay(json.Delay);
            }

            if (json.Exception != null)
            {
                throw new Exception(json.Exception);
            }

            await response.WriteAsync($"{json.Value}");
        }

        [Route("/testMime", methods: "POST")]
        private static Task TestMime(HttpContext context, [JsonBody(MimeType = "test/json")] JsonPayload json) =>
            Test(context, json);
    }

    private sealed partial class ServerJsonRoutesInstance
    {
        private readonly int privateValue = 12;

        [Route("/test", methods: "POST")]
        public async Task Test(HttpContext context, [JsonBody] JsonPayload json)
        {
            var response = context.Response;

            if (json.Delay != 0)
            {
                await Task.Delay(json.Delay);
            }

            if (json.Exception != null)
            {
                throw new Exception(json.Exception);
            }

            await response.WriteAsync($"{json.Value}, {this.privateValue}");
        }
    }

    [QueryStringSerializable]
    [SuppressMessage(
        "StyleCop.CSharp.OrderingRules",
        "SA1202:Elements should be ordered by access",
        Justification = "Tests should be near their data."
    )]
    public partial class QueryStringPayload
    {
        [Name("int")]
        public int IntValue { get; set; }

        [Name("string")]
        [StringLength(1, int.MaxValue)]
        public required string StringValue { get; set; }
    }

    [SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "It's a test.")]
    private sealed partial class ServerQueryParameterRoutesInstance
    {
        [Route("/test", methods: "POST")]
        public async Task Test(HttpContext context, [QueryParameters] QueryStringPayload query)
        {
            var response = context.Response;

            await response.WriteAsync($"{query.IntValue}, {query.StringValue}");
        }
    }

    [Fact]
    public async Task TestQueryParameterInstance()
    {
        var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber, UseDeveloperExceptionPage = true };
        configuration.AddRoutes(new ServerQueryParameterRoutesInstance().GetRoutes());

        using var server = configuration.CreateWebHost();
        await server.StartAsync();

        using var client = new HttpClient();
        var result = await PostStringAsyncWithStatus(client, $"http://localhost:{PortNumber}/test?int=2&string=hello");
        Assert.Equal(200, result.statusCode);
        Assert.Equal("2, hello", result.body);

        result = await PostStringAsyncWithStatus(client, $"http://localhost:{PortNumber}/test?int=what&string=hello");
        Assert.Equal(400, result.statusCode);
        Assert.Equal("'int' was not of type int32.", result.body);

        result = await PostStringAsyncWithStatus(client, $"http://localhost:{PortNumber}/test?int=2");
        Assert.Equal(400, result.statusCode);
        Assert.Equal("Required query parameter 'string' was missing.", result.body);

        result = await PostStringAsyncWithStatus(client, $"http://localhost:{PortNumber}/test?int=2&string=");
        Assert.Equal(400, result.statusCode);
        Assert.Equal("'string' must be longer than 0 characters.", result.body);

        await server.StopAsync();
    }

    [Fact]
    public async Task TestAuthentication()
    {
        var configuration = new WebHostBuilderConfiguration
        {
            HttpPort = PortNumber,
            UseDeveloperExceptionPage = true,
            RequireAuthorization = true,
            ConfigureServices = services =>
            {
                // Add handler for getting scopes
                _ = services.AddSingleton<
                    IAuthorizationHandler,
                    AuthenticationRoutes.IsAuthorizedRequirement.HasScopeHandler
                >();
                _ = services.AddSingleton<
                    IAuthorizationHandler,
                    AuthenticationRoutes.IsAdministratorRequirement.HasScopeHandler
                >();

                _ = services
                    .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.Events.OnRedirectToLogin = context =>
                        {
                            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                            return Task.CompletedTask;
                        };

                        options.Events.OnRedirectToAccessDenied = context =>
                        {
                            context.Response.StatusCode = StatusCodes.Status403Forbidden;
                            return Task.CompletedTask;
                        };
                    });

                _ = services.AddAuthorization(options =>
                {
                    // Add policies.
                    options.AddPolicy(
                        "RegularPolicy",
                        policy => policy.Requirements.Add(new AuthenticationRoutes.IsAuthorizedRequirement())
                    );
                    options.AddPolicy(
                        "AdminPolicy",
                        policy => policy.Requirements.Add(new AuthenticationRoutes.IsAdministratorRequirement())
                    );
                });
            },
            ConfigureApp = app =>
            {
                _ = app.UseAuthentication();
                _ = app.UseAuthorization();
                _ = app.UseCookiePolicy(
                    new CookiePolicyOptions
                    {
                        Secure = CookieSecurePolicy.None,
                        MinimumSameSitePolicy = SameSiteMode.None,
                    }
                );
            },
        };

        configuration.AddRoutes(AuthenticationRoutes.GetRoutes());

        using var server = configuration.CreateWebHost();

        await server.StartAsync();

        using var client = new HttpClient(new HttpClientHandler { UseCookies = false });
        var regularToken = await GetAuth(client, $"http://localhost:{PortNumber}/authenticate/Regular");
        var adminToken = await GetAuth(client, $"http://localhost:{PortNumber}/authenticate/Admin");
        Assert.Equal(
            HttpStatusCode.OK,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/anonArea", regularToken)
        );
        Assert.Equal(
            HttpStatusCode.OK,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/anonArea", "invalid")
        );
        Assert.Equal(HttpStatusCode.OK, await GetStringAsync(client, $"http://localhost:{PortNumber}/anonArea", null));
        Assert.Equal(
            HttpStatusCode.OK,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/secureArea", regularToken)
        );
        Assert.Equal(
            HttpStatusCode.OK,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/secureArea", adminToken)
        );
        Assert.Equal(
            HttpStatusCode.Unauthorized,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/secureArea", "invalid")
        );
        Assert.Equal(
            HttpStatusCode.Unauthorized,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/secureArea", null)
        );
        Assert.Equal(
            HttpStatusCode.Unauthorized,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/adminArea", null)
        );
        Assert.Equal(
            HttpStatusCode.Forbidden,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/adminArea", regularToken)
        );
        Assert.Equal(
            HttpStatusCode.OK,
            await GetStringAsync(client, $"http://localhost:{PortNumber}/adminArea", adminToken)
        );

        await server.StopAsync();

        static async Task<string> GetAuth(HttpClient client, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var response = await client.SendAsync(request);
            _ = response.EnsureSuccessStatusCode();
            Assert.True(response.Headers.TryGetValues("Set-Cookie", out var cookieHeader));
            var auth = cookieHeader.First();
            return auth;
        }

        static async Task<HttpStatusCode> GetStringAsync(HttpClient client, string url, string? auth)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            if (auth != null)
            {
                _ = request.Headers.TryAddWithoutValidation("Cookie", auth);
            }

            var response = await client.SendAsync(request);
            var statusCode = response.StatusCode;
            return statusCode;
        }
    }

    private static partial class AuthenticationRoutes
    {
        [AllowAnonymous]
        [Route("/authenticate/{role}", methods: "GET")]
        private static async Task Authenticate(HttpContext context, string role)
        {
            var claims = new List<Claim> { new(ClaimTypes.Name, "Joe Email"), new(ClaimTypes.Role, role) };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties();

            await context.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties
            );
        }

        [Authorize(
            policy: "test",
            AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme,
            Policy = "RegularPolicy"
        )]
        [Route("/secureArea", methods: "GET")]
        private static async Task SecureArea(HttpContext context)
        {
            var response = context.Response;
            response.StatusCode = StatusCodes.Status200OK;
            await response.WriteAsync("Secure");
        }

        [AllowAnonymous]
        [Route("/anonArea", methods: "GET")]
        private static async Task AnonymousArea(HttpContext context)
        {
            var response = context.Response;
            response.StatusCode = StatusCodes.Status200OK;
            await response.WriteAsync("Anonymous");
        }

        [Authorize("AdminPolicy")]
        [Route("/adminArea", methods: "GET")]
        private static async Task AdminArea(HttpContext context)
        {
            var response = context.Response;
            response.StatusCode = StatusCodes.Status200OK;
            await response.WriteAsync("Admin");
        }

        /// <summary>
        /// Class to see if a user is authorized.
        /// </summary>
        public sealed class IsAuthorizedRequirement : IAuthorizationRequirement
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="IsAuthorizedRequirement" /> class.
            /// </summary>
            public IsAuthorizedRequirement() { }

            /// <summary>
            /// Authorization handler for scopes.
            /// </summary>
            public sealed class HasScopeHandler : AuthorizationHandler<IsAuthorizedRequirement>
            {
                /// <inheritdoc />
                protected override Task HandleRequirementAsync(
                    AuthorizationHandlerContext context,
                    IsAuthorizedRequirement requirement
                )
                {
                    var claim = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
                    if (claim != null)
                    {
                        switch (claim.Value)
                        {
                            case "Regular":
                            case "Admin":
                            {
                                context.Succeed(requirement);
                                break;
                            }

                            default:
                            {
                                // Failure.
                                break;
                            }
                        }
                    }

                    return Task.CompletedTask;
                }
            }
        }

        /// <summary>
        /// Class to see if a user is an admin.
        /// </summary>
        public sealed class IsAdministratorRequirement : IAuthorizationRequirement
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="IsAdministratorRequirement"/> class.
            /// </summary>
            public IsAdministratorRequirement() { }

            /// <summary>
            /// Authorization handler for scopes.
            /// </summary>
            public sealed class HasScopeHandler : AuthorizationHandler<IsAdministratorRequirement>
            {
                /// <inheritdoc />
                protected override Task HandleRequirementAsync(
                    AuthorizationHandlerContext context,
                    IsAdministratorRequirement requirement
                )
                {
                    if (context.User.Claims.Any(c => c is { Type: ClaimTypes.Role, Value: "Admin" }))
                    {
                        context.Succeed(requirement);
                    }

                    return Task.CompletedTask;
                }
            }
        }
    }
}

#endif // !DISABLE_ROSLYN_GENERATED_TESTS
