// <copyright file="BitManipulationTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Internal;

namespace UnitTests;

public class BitManipulationTests
{
    [Fact]
    public void Shorts()
    {
        for (var i = (long)ushort.MinValue; i <= ushort.MaxValue; i += 64)
        {
            TestValue((ushort)i);
        }

        for (var i = (long)short.MinValue; i <= short.MaxValue; i += 64)
        {
            TestValue((short)i);
        }
    }

    [Fact]
    public void Ints()
    {
        var bytes = new byte[8];
        var rnd = new Random(292866992);

        for (var i = 0; i < 65536; i++)
        {
            rnd.NextBytes(bytes);
            var l = BitConverter.ToInt32(bytes);
            TestValue(l);
        }

        for (var i = 0; i < 65536; i++)
        {
            rnd.NextBytes(bytes);
            var u = BitConverter.ToUInt32(bytes);
            TestValue(u);
        }
    }

    [Fact]
    public void Longs()
    {
        var bytes = new byte[8];
        var rnd = new Random(93793202);

        for (var i = 0; i < 65536; i++)
        {
            rnd.NextBytes(bytes);
            var l = BitConverter.ToInt64(bytes);
            TestValue(l);
        }

        for (var i = 0; i < 65536; i++)
        {
            rnd.NextBytes(bytes);
            var u = BitConverter.ToUInt64(bytes);
            TestValue(u);
        }
    }

    private static void TestValue(dynamic value)
    {
        var reversedCorrect = BitConverter.GetBytes(value);
        Array.Reverse(reversedCorrect);
        var reversedValue = BitConverter.GetBytes(BitManipulation.SwapEndian(value));

        Assert.Equal(reversedCorrect, reversedValue);
        if (BitConverter.IsLittleEndian)
        {
            Assert.Equal(reversedCorrect, BitConverter.GetBytes(BitManipulation.ConvertToBigEndian(value)));
            Assert.Equal(reversedCorrect, BitConverter.GetBytes(BitManipulation.ConvertFromBigEndian(value)));
        }
        else
        {
            Assert.Equal(reversedCorrect, BitConverter.GetBytes(BitManipulation.ConvertToLittleEndian(value)));
            Assert.Equal(reversedCorrect, BitConverter.GetBytes(BitManipulation.ConvertFromLittleEndian(value)));
        }
    }
}
