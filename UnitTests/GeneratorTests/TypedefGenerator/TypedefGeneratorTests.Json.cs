// <copyright file="TypedefGeneratorTests.Json.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Text.Json;
using KestrelToolbox.Types;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

namespace UnitTests.GeneratorTests.TypdefGenerator;

/// <summary>
/// Tests for <see cref="TypedefAttribute"/>.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.LayoutRules",
    "SA1515:Single-line comment should be preceded by blank line",
    Justification = "Comments for language."
)]
public sealed partial class TypedefGeneratorTests
{
    [Fact]
    public void TypedefFeaturesJsonSerializableInt()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(int), TypedefFeatures.JsonSerializable)]
            public partial class TestTypedef
            {
                /// <summary>
                /// Gets the value.
                /// </summary>
                public int GetValue() => this.value;
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type int32.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type int32.";
                                    result = default;
                                    return false;
                                }
                            }

                            if (!reader.TryGetInt32(out var token_0))
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type int32.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type int32.";
                                    result = default;
                                    return false;
                                }
                            }

                            output_0 = token_0;
                            result = (global::Tests.TestTypedef)output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestTypedef value)
                    {
                        writer.WriteNumberValue(value.value);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TestTypedef
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type int32.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type int32.";
                                    result = default;
                                    return false;
                                }
                            }

                            if (!reader.TryGetInt32(out var token_0))
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type int32.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type int32.";
                                    result = default;
                                    return false;
                                }
                            }

                            output_0 = token_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, int value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, int value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(int value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, int value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, int value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, int value)
                    {
                        writer.WriteNumberValue(value);
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef
                {
                    private readonly int value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(int value) => this.value = value;

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    public static explicit operator int(TestTypedef value) => value.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    public static explicit operator TestTypedef(int value) => new TestTypedef(value);

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        var assembly = Verifier.LoadAssembly(compilation);
        var testClassType = assembly.GetType("Tests.TestTypedef")!;
        var parseMethod = testClassType.GetMethod(
            "TryParseJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), testClassType.MakeByRefType(), typeof(string).MakeByRefType() }
        )!;
        var parameters = new object?[] { """13""", null, null };
        var invokeResult = parseMethod.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        dynamic value = parameters[1]!;
        Assert.Equal(13, value.GetValue());

        var serializeMethod = testClassType.GetMethod(
            "SerializeJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { testClassType, typeof(JsonWriterOptions) }
        )!;
        invokeResult = serializeMethod.Invoke(null, new object?[] { value, null });
        Assert.Equal("13", (string?)invokeResult);
    }

    [Fact]
    public void TypedefFeaturesJsonSerializableString()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;
            using System.Collections.Generic;
            using KestrelToolbox.Serialization;

            namespace Tests;

            #nullable enable
            [Typedef(typeof(string), TypedefFeatures.All)]
            public sealed partial class TestTypedef
            {
                /// <summary>
                /// Gets the value.
                /// </summary>
                public string GetValue() => this.value;
            }

            [JsonSerializable(typeof(List<TestTypedef?>))]
            public static partial class TestTypedefSerialization { }
            """;
        // Cannot compile out csharp strings, csharpier does not handle them nested within #if DOTNET_9_0.
#pragma warning disable CS0219, S1481 // Variable is assigned but its value is never used

        // Language=C#
        var dotnet8Source = """
            namespace Tests;

            #nullable enable
            public sealed partial class TestTypedef : global::System.IEquatable<TestTypedef>
            {
                private readonly string value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                public TestTypedef(string value) => this.value = value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static TestTypedef? From(string? value) => value is null ? null : new TestTypedef(value);

                /// <summary>
                /// Gets the aliased type of this typedef.
                /// </summary>
                /// <param name="value">Value to cast into the aliased type.</param>
                /// <returns>The aliased type.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator string?(TestTypedef? value) => value?.value;

                /// <summary>
                /// Gets a typedef from the aliased type.
                /// </summary>
                /// <param name="value">Value to cast into the typedef.</param>
                /// <returns>The typedef.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator TestTypedef?(string? value) => value == null ? null : new TestTypedef(value);

                /// <inheritdoc />
                public bool Equals(TestTypedef? other) => this.value.Equals(other?.value);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                public static bool operator==(TestTypedef? left, TestTypedef? right) => object.Equals(left, right);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                public static bool operator!=(TestTypedef? left, TestTypedef? right) => !object.Equals(left, right);

                /// <inheritdoc cref="string.CompareTo(object)" />
                public int CompareTo(object? value) =>
                    this.value.CompareTo(value);

                /// <inheritdoc cref="string.CompareTo(string)" />
                public int CompareTo(string? strB) =>
                    this.value.CompareTo(strB);

                /// <inheritdoc cref="string.EndsWith(string)" />
                public bool EndsWith(string value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.EndsWith(string,global::System.StringComparison)" />
                public bool EndsWith(string value, System.StringComparison comparisonType) =>
                    this.value.EndsWith(value, comparisonType);

                /// <inheritdoc cref="string.EndsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool EndsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.EndsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.EndsWith(char)" />
                public bool EndsWith(char value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.Equals(object)" />
                public override bool Equals(object? obj) =>
                    obj is global::Tests.TestTypedef other && this.Equals(other);

                /// <inheritdoc cref="string.Equals(string)" />
                public bool Equals(string? value) =>
                    this.value.Equals(value);

                /// <inheritdoc cref="string.Equals(string,global::System.StringComparison)" />
                public bool Equals(string? value, System.StringComparison comparisonType) =>
                    this.value.Equals(value, comparisonType);

                /// <inheritdoc cref="string.GetHashCode()" />
                public override int GetHashCode() =>
                    this.value.GetHashCode();

                /// <inheritdoc cref="string.GetHashCode(global::System.StringComparison)" />
                public int GetHashCode(System.StringComparison comparisonType) =>
                    this.value.GetHashCode(comparisonType);

                /// <inheritdoc cref="string.StartsWith(string)" />
                public bool StartsWith(string value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.StartsWith(string,global::System.StringComparison)" />
                public bool StartsWith(string value, System.StringComparison comparisonType) =>
                    this.value.StartsWith(value, comparisonType);

                /// <inheritdoc cref="string.StartsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool StartsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.StartsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.StartsWith(char)" />
                public bool StartsWith(char value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.Clone()" />
                public object Clone() =>
                    this.value.Clone();

                /// <inheritdoc cref="string.CopyTo(int,char[],int,int)" />
                public void CopyTo(int sourceIndex, char[] destination, int destinationIndex, int count) =>
                    this.value.CopyTo(sourceIndex, destination, destinationIndex, count);

                /// <inheritdoc cref="string.CopyTo(global::System.Span{char})" />
                public void CopyTo(System.Span<char> destination) =>
                    this.value.CopyTo(destination);

                /// <inheritdoc cref="string.TryCopyTo(global::System.Span{char})" />
                public bool TryCopyTo(System.Span<char> destination) =>
                    this.value.TryCopyTo(destination);

                /// <inheritdoc cref="string.ToCharArray()" />
                public char[] ToCharArray() =>
                    this.value.ToCharArray();

                /// <inheritdoc cref="string.ToCharArray(int,int)" />
                public char[] ToCharArray(int startIndex, int length) =>
                    this.value.ToCharArray(startIndex, length);

                /// <inheritdoc cref="string.GetPinnableReference()" />
                public char GetPinnableReference() =>
                    this.value.GetPinnableReference();

                /// <inheritdoc cref="string.ToString()" />
                public override string ToString() =>
                    this.value.ToString();

                /// <inheritdoc cref="string.ToString(global::System.IFormatProvider)" />
                public string ToString(System.IFormatProvider? provider) =>
                    this.value.ToString(provider);

                /// <inheritdoc cref="string.GetEnumerator()" />
                public global::System.CharEnumerator GetEnumerator() =>
                    this.value.GetEnumerator();

                /// <inheritdoc cref="string.EnumerateRunes()" />
                public global::System.Text.StringRuneEnumerator EnumerateRunes() =>
                    this.value.EnumerateRunes();

                /// <inheritdoc cref="string.GetTypeCode()" />
                public global::System.TypeCode GetTypeCode() =>
                    this.value.GetTypeCode();

                /// <inheritdoc cref="string.IsNormalized()" />
                public bool IsNormalized() =>
                    this.value.IsNormalized();

                /// <inheritdoc cref="string.IsNormalized(global::System.Text.NormalizationForm)" />
                public bool IsNormalized(System.Text.NormalizationForm normalizationForm) =>
                    this.value.IsNormalized(normalizationForm);

                /// <inheritdoc cref="string.Normalize()" />
                public string Normalize() =>
                    this.value.Normalize();

                /// <inheritdoc cref="string.Normalize(global::System.Text.NormalizationForm)" />
                public string Normalize(System.Text.NormalizationForm normalizationForm) =>
                    this.value.Normalize(normalizationForm);

                /// <inheritdoc cref="string.Insert(int,string)" />
                public string Insert(int startIndex, string value) =>
                    this.value.Insert(startIndex, value);

                /// <inheritdoc cref="string.PadLeft(int)" />
                public string PadLeft(int totalWidth) =>
                    this.value.PadLeft(totalWidth);

                /// <inheritdoc cref="string.PadLeft(int,char)" />
                public string PadLeft(int totalWidth, char paddingChar) =>
                    this.value.PadLeft(totalWidth, paddingChar);

                /// <inheritdoc cref="string.PadRight(int)" />
                public string PadRight(int totalWidth) =>
                    this.value.PadRight(totalWidth);

                /// <inheritdoc cref="string.PadRight(int,char)" />
                public string PadRight(int totalWidth, char paddingChar) =>
                    this.value.PadRight(totalWidth, paddingChar);

                /// <inheritdoc cref="string.Remove(int,int)" />
                public string Remove(int startIndex, int count) =>
                    this.value.Remove(startIndex, count);

                /// <inheritdoc cref="string.Remove(int)" />
                public string Remove(int startIndex) =>
                    this.value.Remove(startIndex);

                /// <inheritdoc cref="string.Replace(string,string,bool,global::System.Globalization.CultureInfo)" />
                public string Replace(string oldValue, string? newValue, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.Replace(oldValue, newValue, ignoreCase, culture);

                /// <inheritdoc cref="string.Replace(string,string,global::System.StringComparison)" />
                public string Replace(string oldValue, string? newValue, System.StringComparison comparisonType) =>
                    this.value.Replace(oldValue, newValue, comparisonType);

                /// <inheritdoc cref="string.Replace(char,char)" />
                public string Replace(char oldChar, char newChar) =>
                    this.value.Replace(oldChar, newChar);

                /// <inheritdoc cref="string.Replace(string,string)" />
                public string Replace(string oldValue, string? newValue) =>
                    this.value.Replace(oldValue, newValue);

                /// <inheritdoc cref="string.ReplaceLineEndings()" />
                public string ReplaceLineEndings() =>
                    this.value.ReplaceLineEndings();

                /// <inheritdoc cref="string.ReplaceLineEndings(string)" />
                public string ReplaceLineEndings(string replacementText) =>
                    this.value.ReplaceLineEndings(replacementText);

                /// <inheritdoc cref="string.Split(char,global::System.StringSplitOptions)" />
                public string[] Split(char separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char,int,global::System.StringSplitOptions)" />
                public string[] Split(char separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(char[])" />
                public string[] Split(params char[]? separator) =>
                    this.value.Split(separator);

                /// <inheritdoc cref="string.Split(char[],int)" />
                public string[] Split(char[]? separator, int count) =>
                    this.value.Split(separator, count);

                /// <inheritdoc cref="string.Split(char[],global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char[],int,global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string,int,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string[],global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string[],int,global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Substring(int)" />
                public string Substring(int startIndex) =>
                    this.value.Substring(startIndex);

                /// <inheritdoc cref="string.Substring(int,int)" />
                public string Substring(int startIndex, int length) =>
                    this.value.Substring(startIndex, length);

                /// <inheritdoc cref="string.ToLower()" />
                public string ToLower() =>
                    this.value.ToLower();

                /// <inheritdoc cref="string.ToLower(global::System.Globalization.CultureInfo)" />
                public string ToLower(System.Globalization.CultureInfo? culture) =>
                    this.value.ToLower(culture);

                /// <inheritdoc cref="string.ToLowerInvariant()" />
                public string ToLowerInvariant() =>
                    this.value.ToLowerInvariant();

                /// <inheritdoc cref="string.ToUpper()" />
                public string ToUpper() =>
                    this.value.ToUpper();

                /// <inheritdoc cref="string.ToUpper(global::System.Globalization.CultureInfo)" />
                public string ToUpper(System.Globalization.CultureInfo? culture) =>
                    this.value.ToUpper(culture);

                /// <inheritdoc cref="string.ToUpperInvariant()" />
                public string ToUpperInvariant() =>
                    this.value.ToUpperInvariant();

                /// <inheritdoc cref="string.Trim()" />
                public string Trim() =>
                    this.value.Trim();

                /// <inheritdoc cref="string.Trim(char)" />
                public string Trim(char trimChar) =>
                    this.value.Trim(trimChar);

                /// <inheritdoc cref="string.Trim(char[])" />
                public string Trim(params char[]? trimChars) =>
                    this.value.Trim(trimChars);

                /// <inheritdoc cref="string.TrimStart()" />
                public string TrimStart() =>
                    this.value.TrimStart();

                /// <inheritdoc cref="string.TrimStart(char)" />
                public string TrimStart(char trimChar) =>
                    this.value.TrimStart(trimChar);

                /// <inheritdoc cref="string.TrimStart(char[])" />
                public string TrimStart(params char[]? trimChars) =>
                    this.value.TrimStart(trimChars);

                /// <inheritdoc cref="string.TrimEnd()" />
                public string TrimEnd() =>
                    this.value.TrimEnd();

                /// <inheritdoc cref="string.TrimEnd(char)" />
                public string TrimEnd(char trimChar) =>
                    this.value.TrimEnd(trimChar);

                /// <inheritdoc cref="string.TrimEnd(char[])" />
                public string TrimEnd(params char[]? trimChars) =>
                    this.value.TrimEnd(trimChars);

                /// <inheritdoc cref="string.Contains(string)" />
                public bool Contains(string value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(string,global::System.StringComparison)" />
                public bool Contains(string value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.Contains(char)" />
                public bool Contains(char value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(char,global::System.StringComparison)" />
                public bool Contains(char value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char)" />
                public int IndexOf(char value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(char,int)" />
                public int IndexOf(char value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(char,global::System.StringComparison)" />
                public int IndexOf(char value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char,int,int)" />
                public int IndexOf(char value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOfAny(char[])" />
                public int IndexOfAny(char[] anyOf) =>
                    this.value.IndexOfAny(anyOf);

                /// <inheritdoc cref="string.IndexOfAny(char[],int)" />
                public int IndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.IndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.IndexOfAny(char[],int,int)" />
                public int IndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.IndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string)" />
                public int IndexOf(string value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(string,int)" />
                public int IndexOf(string value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(string,int,int)" />
                public int IndexOf(string value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string,global::System.StringComparison)" />
                public int IndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(char)" />
                public int LastIndexOf(char value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(char,int)" />
                public int LastIndexOf(char value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(char,int,int)" />
                public int LastIndexOf(char value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOfAny(char[])" />
                public int LastIndexOfAny(char[] anyOf) =>
                    this.value.LastIndexOfAny(anyOf);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.LastIndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int,int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.LastIndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string)" />
                public int LastIndexOf(string value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(string,int)" />
                public int LastIndexOf(string value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int)" />
                public int LastIndexOf(string value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string,global::System.StringComparison)" />
                public int LastIndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.this[int]" />
                public char this[int index]
                {
                    get => this.value[index];
                }

                /// <inheritdoc cref="string.Length" />
                public int Length
                {
                    get => this.value.Length;
                }

            }
            """;

        // Language=C#
        var dotnet9Source = """
            namespace Tests;

            #nullable enable
            public sealed partial class TestTypedef : global::System.IEquatable<TestTypedef>
            {
                private readonly string value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                public TestTypedef(string value) => this.value = value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static TestTypedef? From(string? value) => value is null ? null : new TestTypedef(value);

                /// <summary>
                /// Gets the aliased type of this typedef.
                /// </summary>
                /// <param name="value">Value to cast into the aliased type.</param>
                /// <returns>The aliased type.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator string?(TestTypedef? value) => value?.value;

                /// <summary>
                /// Gets a typedef from the aliased type.
                /// </summary>
                /// <param name="value">Value to cast into the typedef.</param>
                /// <returns>The typedef.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator TestTypedef?(string? value) => value == null ? null : new TestTypedef(value);

                /// <inheritdoc />
                public bool Equals(TestTypedef? other) => this.value.Equals(other?.value);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                public static bool operator==(TestTypedef? left, TestTypedef? right) => object.Equals(left, right);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                public static bool operator!=(TestTypedef? left, TestTypedef? right) => !object.Equals(left, right);

                /// <inheritdoc cref="string.CompareTo(object)" />
                public int CompareTo(object? value) =>
                    this.value.CompareTo(value);

                /// <inheritdoc cref="string.CompareTo(string)" />
                public int CompareTo(string? strB) =>
                    this.value.CompareTo(strB);

                /// <inheritdoc cref="string.EndsWith(string)" />
                public bool EndsWith(string value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.EndsWith(string,global::System.StringComparison)" />
                public bool EndsWith(string value, System.StringComparison comparisonType) =>
                    this.value.EndsWith(value, comparisonType);

                /// <inheritdoc cref="string.EndsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool EndsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.EndsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.EndsWith(char)" />
                public bool EndsWith(char value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.Equals(object)" />
                public override bool Equals(object? obj) =>
                    obj is global::Tests.TestTypedef other && this.Equals(other);

                /// <inheritdoc cref="string.Equals(string)" />
                public bool Equals(string? value) =>
                    this.value.Equals(value);

                /// <inheritdoc cref="string.Equals(string,global::System.StringComparison)" />
                public bool Equals(string? value, System.StringComparison comparisonType) =>
                    this.value.Equals(value, comparisonType);

                /// <inheritdoc cref="string.GetHashCode()" />
                public override int GetHashCode() =>
                    this.value.GetHashCode();

                /// <inheritdoc cref="string.GetHashCode(global::System.StringComparison)" />
                public int GetHashCode(System.StringComparison comparisonType) =>
                    this.value.GetHashCode(comparisonType);

                /// <inheritdoc cref="string.StartsWith(string)" />
                public bool StartsWith(string value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.StartsWith(string,global::System.StringComparison)" />
                public bool StartsWith(string value, System.StringComparison comparisonType) =>
                    this.value.StartsWith(value, comparisonType);

                /// <inheritdoc cref="string.StartsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool StartsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.StartsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.StartsWith(char)" />
                public bool StartsWith(char value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.Clone()" />
                public object Clone() =>
                    this.value.Clone();

                /// <inheritdoc cref="string.CopyTo(int,char[],int,int)" />
                public void CopyTo(int sourceIndex, char[] destination, int destinationIndex, int count) =>
                    this.value.CopyTo(sourceIndex, destination, destinationIndex, count);

                /// <inheritdoc cref="string.CopyTo(global::System.Span{char})" />
                public void CopyTo(System.Span<char> destination) =>
                    this.value.CopyTo(destination);

                /// <inheritdoc cref="string.TryCopyTo(global::System.Span{char})" />
                public bool TryCopyTo(System.Span<char> destination) =>
                    this.value.TryCopyTo(destination);

                /// <inheritdoc cref="string.ToCharArray()" />
                public char[] ToCharArray() =>
                    this.value.ToCharArray();

                /// <inheritdoc cref="string.ToCharArray(int,int)" />
                public char[] ToCharArray(int startIndex, int length) =>
                    this.value.ToCharArray(startIndex, length);

                /// <inheritdoc cref="string.GetPinnableReference()" />
                public char GetPinnableReference() =>
                    this.value.GetPinnableReference();

                /// <inheritdoc cref="string.ToString()" />
                public override string ToString() =>
                    this.value.ToString();

                /// <inheritdoc cref="string.ToString(global::System.IFormatProvider)" />
                public string ToString(System.IFormatProvider? provider) =>
                    this.value.ToString(provider);

                /// <inheritdoc cref="string.GetEnumerator()" />
                public global::System.CharEnumerator GetEnumerator() =>
                    this.value.GetEnumerator();

                /// <inheritdoc cref="string.EnumerateRunes()" />
                public global::System.Text.StringRuneEnumerator EnumerateRunes() =>
                    this.value.EnumerateRunes();

                /// <inheritdoc cref="string.GetTypeCode()" />
                public global::System.TypeCode GetTypeCode() =>
                    this.value.GetTypeCode();

                /// <inheritdoc cref="string.IsNormalized()" />
                public bool IsNormalized() =>
                    this.value.IsNormalized();

                /// <inheritdoc cref="string.IsNormalized(global::System.Text.NormalizationForm)" />
                public bool IsNormalized(System.Text.NormalizationForm normalizationForm) =>
                    this.value.IsNormalized(normalizationForm);

                /// <inheritdoc cref="string.Normalize()" />
                public string Normalize() =>
                    this.value.Normalize();

                /// <inheritdoc cref="string.Normalize(global::System.Text.NormalizationForm)" />
                public string Normalize(System.Text.NormalizationForm normalizationForm) =>
                    this.value.Normalize(normalizationForm);

                /// <inheritdoc cref="string.Insert(int,string)" />
                public string Insert(int startIndex, string value) =>
                    this.value.Insert(startIndex, value);

                /// <inheritdoc cref="string.PadLeft(int)" />
                public string PadLeft(int totalWidth) =>
                    this.value.PadLeft(totalWidth);

                /// <inheritdoc cref="string.PadLeft(int,char)" />
                public string PadLeft(int totalWidth, char paddingChar) =>
                    this.value.PadLeft(totalWidth, paddingChar);

                /// <inheritdoc cref="string.PadRight(int)" />
                public string PadRight(int totalWidth) =>
                    this.value.PadRight(totalWidth);

                /// <inheritdoc cref="string.PadRight(int,char)" />
                public string PadRight(int totalWidth, char paddingChar) =>
                    this.value.PadRight(totalWidth, paddingChar);

                /// <inheritdoc cref="string.Remove(int,int)" />
                public string Remove(int startIndex, int count) =>
                    this.value.Remove(startIndex, count);

                /// <inheritdoc cref="string.Remove(int)" />
                public string Remove(int startIndex) =>
                    this.value.Remove(startIndex);

                /// <inheritdoc cref="string.Replace(string,string,bool,global::System.Globalization.CultureInfo)" />
                public string Replace(string oldValue, string? newValue, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.Replace(oldValue, newValue, ignoreCase, culture);

                /// <inheritdoc cref="string.Replace(string,string,global::System.StringComparison)" />
                public string Replace(string oldValue, string? newValue, System.StringComparison comparisonType) =>
                    this.value.Replace(oldValue, newValue, comparisonType);

                /// <inheritdoc cref="string.Replace(char,char)" />
                public string Replace(char oldChar, char newChar) =>
                    this.value.Replace(oldChar, newChar);

                /// <inheritdoc cref="string.Replace(string,string)" />
                public string Replace(string oldValue, string? newValue) =>
                    this.value.Replace(oldValue, newValue);

                /// <inheritdoc cref="string.ReplaceLineEndings()" />
                public string ReplaceLineEndings() =>
                    this.value.ReplaceLineEndings();

                /// <inheritdoc cref="string.ReplaceLineEndings(string)" />
                public string ReplaceLineEndings(string replacementText) =>
                    this.value.ReplaceLineEndings(replacementText);

                /// <inheritdoc cref="string.Split(char,global::System.StringSplitOptions)" />
                public string[] Split(char separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char,int,global::System.StringSplitOptions)" />
                public string[] Split(char separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(char[])" />
                public string[] Split(params char[]? separator) =>
                    this.value.Split(separator);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.Split(global::System.ReadOnlySpan{char})" />
                public string[] Split(params System.ReadOnlySpan<char> separator) =>
                    this.value.Split(separator);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.Split(char[],int)" />
                public string[] Split(char[]? separator, int count) =>
                    this.value.Split(separator, count);

                /// <inheritdoc cref="string.Split(char[],global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char[],int,global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string,int,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string[],global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string[],int,global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Substring(int)" />
                public string Substring(int startIndex) =>
                    this.value.Substring(startIndex);

                /// <inheritdoc cref="string.Substring(int,int)" />
                public string Substring(int startIndex, int length) =>
                    this.value.Substring(startIndex, length);

                /// <inheritdoc cref="string.ToLower()" />
                public string ToLower() =>
                    this.value.ToLower();

                /// <inheritdoc cref="string.ToLower(global::System.Globalization.CultureInfo)" />
                public string ToLower(System.Globalization.CultureInfo? culture) =>
                    this.value.ToLower(culture);

                /// <inheritdoc cref="string.ToLowerInvariant()" />
                public string ToLowerInvariant() =>
                    this.value.ToLowerInvariant();

                /// <inheritdoc cref="string.ToUpper()" />
                public string ToUpper() =>
                    this.value.ToUpper();

                /// <inheritdoc cref="string.ToUpper(global::System.Globalization.CultureInfo)" />
                public string ToUpper(System.Globalization.CultureInfo? culture) =>
                    this.value.ToUpper(culture);

                /// <inheritdoc cref="string.ToUpperInvariant()" />
                public string ToUpperInvariant() =>
                    this.value.ToUpperInvariant();

                /// <inheritdoc cref="string.Trim()" />
                public string Trim() =>
                    this.value.Trim();

                /// <inheritdoc cref="string.Trim(char)" />
                public string Trim(char trimChar) =>
                    this.value.Trim(trimChar);

                /// <inheritdoc cref="string.Trim(char[])" />
                public string Trim(params char[]? trimChars) =>
                    this.value.Trim(trimChars);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.Trim(global::System.ReadOnlySpan{char})" />
                public string Trim(params System.ReadOnlySpan<char> trimChars) =>
                    this.value.Trim(trimChars);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.TrimStart()" />
                public string TrimStart() =>
                    this.value.TrimStart();

                /// <inheritdoc cref="string.TrimStart(char)" />
                public string TrimStart(char trimChar) =>
                    this.value.TrimStart(trimChar);

                /// <inheritdoc cref="string.TrimStart(char[])" />
                public string TrimStart(params char[]? trimChars) =>
                    this.value.TrimStart(trimChars);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.TrimStart(global::System.ReadOnlySpan{char})" />
                public string TrimStart(params System.ReadOnlySpan<char> trimChars) =>
                    this.value.TrimStart(trimChars);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.TrimEnd()" />
                public string TrimEnd() =>
                    this.value.TrimEnd();

                /// <inheritdoc cref="string.TrimEnd(char)" />
                public string TrimEnd(char trimChar) =>
                    this.value.TrimEnd(trimChar);

                /// <inheritdoc cref="string.TrimEnd(char[])" />
                public string TrimEnd(params char[]? trimChars) =>
                    this.value.TrimEnd(trimChars);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.TrimEnd(global::System.ReadOnlySpan{char})" />
                public string TrimEnd(params System.ReadOnlySpan<char> trimChars) =>
                    this.value.TrimEnd(trimChars);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.Contains(string)" />
                public bool Contains(string value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(string,global::System.StringComparison)" />
                public bool Contains(string value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.Contains(char)" />
                public bool Contains(char value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(char,global::System.StringComparison)" />
                public bool Contains(char value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char)" />
                public int IndexOf(char value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(char,int)" />
                public int IndexOf(char value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(char,global::System.StringComparison)" />
                public int IndexOf(char value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char,int,int)" />
                public int IndexOf(char value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOfAny(char[])" />
                public int IndexOfAny(char[] anyOf) =>
                    this.value.IndexOfAny(anyOf);

                /// <inheritdoc cref="string.IndexOfAny(char[],int)" />
                public int IndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.IndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.IndexOfAny(char[],int,int)" />
                public int IndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.IndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string)" />
                public int IndexOf(string value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(string,int)" />
                public int IndexOf(string value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(string,int,int)" />
                public int IndexOf(string value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string,global::System.StringComparison)" />
                public int IndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(char)" />
                public int LastIndexOf(char value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(char,int)" />
                public int LastIndexOf(char value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(char,int,int)" />
                public int LastIndexOf(char value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOfAny(char[])" />
                public int LastIndexOfAny(char[] anyOf) =>
                    this.value.LastIndexOfAny(anyOf);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.LastIndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int,int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.LastIndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string)" />
                public int LastIndexOf(string value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(string,int)" />
                public int LastIndexOf(string value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int)" />
                public int LastIndexOf(string value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string,global::System.StringComparison)" />
                public int LastIndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.this[int]" />
                public char this[int index]
                {
                    get => this.value[index];
                }

                /// <inheritdoc cref="string.Length" />
                public int Length
                {
                    get => this.value.Length;
                }

            }
            """;
#pragma warning restore CS0219, S1481 // Variable is assigned but its value is never used

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TestTypedef
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestTypedef? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            string output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type string.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type string.";
                                    result = default;
                                    return false;
                                }
                            }

                            var token_0 = reader.GetString()!;
                            output_0 = token_0;
                            result = (global::Tests.TestTypedef)output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestTypedef value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestTypedef value)
                    {
                        writer.WriteStringValue(value.value);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TestTypedef
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out string? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out string? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out string? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out string? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            string output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type string.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type string.";
                                    result = default;
                                    return false;
                                }
                            }

                            var token_0 = reader.GetString()!;
                            output_0 = token_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, string value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, string value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(string value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, string value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, string value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, string value)
                    {
                        writer.WriteStringValue(value);
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestTypedefSerialization.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public static partial class TestTypedefSerialization
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.List<global::Tests.TestTypedef?>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.List<global::Tests.TestTypedef?>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.List<global::Tests.TestTypedef?>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.List<global::Tests.TestTypedef?>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::System.Collections.Generic.List<global::Tests.TestTypedef?> output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartArray)
                            {
                                error = "Was not an array.";
                                result = default;
                                return false;
                            }

                            var input_0 = new global::System.Collections.Generic.List<global::Tests.TestTypedef?>();
                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndArray)
                                {
                                    break;
                                }

                                if (!global::Tests.TestTypedef.TryParseJson(ref reader, out global::Tests.TestTypedef? input_1, out error))
                                {
                                    result = default;
                                    return false;
                                }

                                input_0.Add(input_1);
                            }

                            output_0 = input_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Collections.Generic.List<global::Tests.TestTypedef?> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Generic.List<global::Tests.TestTypedef?> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Collections.Generic.List<global::Tests.TestTypedef?> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Collections.Generic.List<global::Tests.TestTypedef?> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Generic.List<global::Tests.TestTypedef?> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Collections.Generic.List<global::Tests.TestTypedef?> value)
                    {
                        writer.WriteStartArray();
                        foreach (var token_0 in value)
                        {
                            if (token_0 == null)
                            {
                                writer.WriteNullValue();
                            }
                            else
                            {
                                global::Tests.TestTypedef.SerializeJson(writer, token_0);
                            }
                        }

                        writer.WriteEndArray();
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
#if NET9_0_OR_GREATER
                dotnet9Source
#else
                dotnet8Source
#endif // NET9_0_OR_GREATER
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        var assembly = Verifier.LoadAssembly(compilation);
        var testClassType = assembly.GetType("Tests.TestTypedef")!;
        var parseMethod = testClassType.GetMethod(
            "TryParseJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), testClassType.MakeByRefType(), typeof(string).MakeByRefType() }
        )!;
        var parameters = new object?[] { "\"13\"", null, null };
        var invokeResult = parseMethod.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        dynamic value = parameters[1]!;
        Assert.Equal("13", value.GetValue());

        var serializeMethod = testClassType.GetMethod(
            "SerializeJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { testClassType, typeof(JsonWriterOptions) }
        )!;
        invokeResult = serializeMethod.Invoke(null, new object?[] { value, null });
        Assert.Equal("\"13\"", (string?)invokeResult);
    }

    [Fact]
    public void TypedefFeaturesJsonSerializableNullable()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;
            using System.Collections.Generic;
            using KestrelToolbox.CodeGen.Serializable;
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;
            using System.Text.Json;

            namespace Tests;

            #nullable enable

            public sealed class InvalidValueEnumClass
            {
                [Name("value")]
                public int Value { get; set; }
            }

            public sealed class CustomSerializeClass
            {
                [Name("class")]
                [SkipSerialization(Condition.WhenNull)]
                public CustomValueClass? Class { get; set; }

                [Name("struct")]
                public CustomValueStruct Struct { get; set; }

                public sealed class CustomValueClass
                {
                    public int Value { get; set; }

                    [JsonSerializableCustomParse]
                    public static bool TryParseCustomValueClass(
                        ref Utf8JsonReader reader,
                        [NotNullWhen(true)] out CustomValueClass? value,
                        [NotNullWhen(false)] out string? error
                    )
                    {
                        if (reader.TokenType != JsonTokenType.Number)
                        {
                            value = default;
                            error = $"Invalid token type for {nameof(CustomValueClass)}";
                            return false;
                        }

                        var readValue = reader.GetInt32();
                        value = new CustomValueClass { Value = readValue };
                        error = null;
                        return true;
                    }

                    [JsonSerializableCustomSerialize]
                    public static void Serialize(Utf8JsonWriter writer, CustomValueClass value)
                    {
                        writer.WriteNumberValue(value.Value);
                    }
                }

                public struct CustomValueStruct
                {
                    public bool Value { get; set; }

                    [JsonSerializableCustomParse]
                    public static bool TryParseCustomValueStruct(
                        ref Utf8JsonReader reader,
                        out CustomValueStruct value,
                        [NotNullWhen(false)] out string? error
                    )
                    {
                        if (reader.TokenType is not JsonTokenType.True and not JsonTokenType.False)
                        {
                            value = default;
                            error = $"Invalid token type for {nameof(CustomValueStruct)}";
                            return false;
                        }

                        var readValue = reader.GetBoolean();
                        value = new CustomValueStruct { Value = readValue };
                        error = null;
                        return true;
                    }

                    [JsonSerializableCustomSerialize]
                    public static void Serialize(Utf8JsonWriter writer, CustomValueStruct value)
                    {
                        writer.WriteBooleanValue(value.Value);
                    }
                }
            }

            [JsonSerializable]
            public sealed partial class TypedefJsonNullable
            {
                [Typedef(typeof(int), TypedefFeatures.All)]
                public readonly partial struct TypedefInt { }

                [Typedef(typeof(InvalidValueEnumClass), TypedefFeatures.JsonSerializable)]
                public readonly partial struct TypedefComplex { }

                [Typedef(typeof(CustomSerializeClass), TypedefFeatures.JsonSerializable)]
                public readonly partial struct TypedefCustom { }

                [Name("int")]
                public TypedefInt? IntValue { get; set; }

                [Name("complex")]
                public TypedefComplex? ComplexValue { get; set; }

                [Name("custom")]
                public TypedefCustom? CustomValue { get; set; }
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TypedefJsonNullable.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TypedefJsonNullable? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TypedefJsonNullable? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TypedefJsonNullable? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TypedefJsonNullable? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            var member_0 = default(global::KestrelToolbox.Internal.OptionalProperty<global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt>>);
                            var member_1 = default(global::KestrelToolbox.Internal.OptionalProperty<global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex>>);
                            var member_2 = default(global::KestrelToolbox.Internal.OptionalProperty<global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom>>);
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.TypedefJsonNullable' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "int":
                                    {
                                        global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> output_0;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_0 = null;
                                        }
                                        else
                                        {
                                            if (!global::Tests.TypedefJsonNullable.TypedefInt.TryParseJson(ref reader, out global::Tests.TypedefJsonNullable.TypedefInt input_0, out error))
                                            {
                                                error = $"int > {error}";
                                                result = default;
                                                return false;
                                            }

                                            output_0 = input_0;
                                        }

                                        member_0 = output_0;
                                        break;
                                    }

                                    case "complex":
                                    {
                                        global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> output_1;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_1 = null;
                                        }
                                        else
                                        {
                                            if (!global::Tests.TypedefJsonNullable.TypedefComplex.TryParseJson(ref reader, out global::Tests.TypedefJsonNullable.TypedefComplex input_1, out error))
                                            {
                                                error = $"complex > {error}";
                                                result = default;
                                                return false;
                                            }

                                            output_1 = input_1;
                                        }

                                        member_1 = output_1;
                                        break;
                                    }

                                    case "custom":
                                    {
                                        global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> output_2;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_2 = null;
                                        }
                                        else
                                        {
                                            if (!global::Tests.TypedefJsonNullable.TypedefCustom.TryParseJson(ref reader, out global::Tests.TypedefJsonNullable.TypedefCustom input_2, out error))
                                            {
                                                error = $"custom > {error}";
                                                result = default;
                                                return false;
                                            }

                                            output_2 = input_2;
                                        }

                                        member_2 = output_2;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            result = new global::Tests.TypedefJsonNullable()
                            {
                            };
                            if (member_0.HasValue)
                            {
                                result.IntValue = (global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt>)member_0;
                            }
                            if (member_1.HasValue)
                            {
                                result.ComplexValue = (global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex>)member_1;
                            }
                            if (member_2.HasValue)
                            {
                                result.CustomValue = (global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom>)member_2;
                            }

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TypedefJsonNullable value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TypedefJsonNullable value)
                    {
                        writer.WriteStartObject();
                        var member_3 = value.IntValue;
                        if (member_3.HasValue)
                        {
                            writer.WritePropertyName("int");
                            global::Tests.TypedefJsonNullable.TypedefInt.SerializeJson(writer, member_3.Value);
                        }
                        else
                        {
                            writer.WriteNull("int");
                        }

                        var member_4 = value.ComplexValue;
                        if (member_4.HasValue)
                        {
                            writer.WritePropertyName("complex");
                            global::Tests.TypedefJsonNullable.TypedefComplex.SerializeJson(writer, member_4.Value);
                        }
                        else
                        {
                            writer.WriteNull("complex");
                        }

                        var member_5 = value.CustomValue;
                        if (member_5.HasValue)
                        {
                            writer.WritePropertyName("custom");
                            global::Tests.TypedefJsonNullable.TypedefCustom.SerializeJson(writer, member_5.Value);
                        }
                        else
                        {
                            writer.WriteNull("custom");
                        }

                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            if (!global::Tests.TypedefJsonNullable.TypedefInt.TryParseJson(ref reader, out global::Tests.TypedefJsonNullable.TypedefInt output_0, out error))
                            {
                                result = default;
                                return false;
                            }

                            result = (global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt>)output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefInt> value)
                    {
                        if (value.HasValue)
                        {
                            global::Tests.TypedefJsonNullable.TypedefInt.SerializeJson(writer, value.Value);
                        }
                        else
                        {
                            writer.WriteNullValue();
                        }
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            if (!global::Tests.TypedefJsonNullable.TypedefComplex.TryParseJson(ref reader, out global::Tests.TypedefJsonNullable.TypedefComplex output_0, out error))
                            {
                                result = default;
                                return false;
                            }

                            result = (global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex>)output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefComplex> value)
                    {
                        if (value.HasValue)
                        {
                            global::Tests.TypedefJsonNullable.TypedefComplex.SerializeJson(writer, value.Value);
                        }
                        else
                        {
                            writer.WriteNullValue();
                        }
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            if (!global::Tests.TypedefJsonNullable.TypedefCustom.TryParseJson(ref reader, out global::Tests.TypedefJsonNullable.TypedefCustom output_0, out error))
                            {
                                result = default;
                                return false;
                            }

                            result = (global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom>)output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Nullable<global::Tests.TypedefJsonNullable.TypedefCustom> value)
                    {
                        if (value.HasValue)
                        {
                            global::Tests.TypedefJsonNullable.TypedefCustom.SerializeJson(writer, value.Value);
                        }
                        else
                        {
                            writer.WriteNullValue();
                        }
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TypedefJsonNullable.TypedefInt.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    public readonly partial struct TypedefInt
                    {
                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="json">A raw json string to parse.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(string json, out global::Tests.TypedefJsonNullable.TypedefInt result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                            var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                            return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::Tests.TypedefJsonNullable.TypedefInt result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                            return TryParseJson(ref reader, out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::Tests.TypedefJsonNullable.TypedefInt result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                            return TryParseJson(ref reader, out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::Tests.TypedefJsonNullable.TypedefInt result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            try
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                                {
                                    error = "Cannot parse empty string.";
                                    result = default;
                                    return false;
                                }

                                int output_0;
                                if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                {
                                    if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                    {
                                        error = "Was null. Should be of type int32.";
                                        result = default;
                                        return false;
                                    }
                                    else
                                    {
                                        error = "Was not of type int32.";
                                        result = default;
                                        return false;
                                    }
                                }

                                if (!reader.TryGetInt32(out var token_0))
                                {
                                    if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                    {
                                        error = "Was null. Should be of type int32.";
                                        result = default;
                                        return false;
                                    }
                                    else
                                    {
                                        error = "Was not of type int32.";
                                        result = default;
                                        return false;
                                    }
                                }

                                output_0 = token_0;
                                result = (global::Tests.TypedefJsonNullable.TypedefInt)output_0;
                                error = default;
                                return true;
                            }
                            catch (global::System.Text.Json.JsonException)
                            {
                                result = default;
                                error = "Failed to parse json.";
                                return false;
                            }
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable.TypedefInt value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        {
                            await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                            SerializeJson(writer, value);
                            await writer.FlushAsync(cancellationToken);
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable.TypedefInt value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        {
                            await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                            SerializeJson(writer, value);
                            await writer.FlushAsync(cancellationToken);
                        }

                        /// <summary>
                        /// Serialize an object to a string.
                        /// </summary>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <returns>Json encoded string value.</returns>
                        public static string SerializeJson(global::Tests.TypedefJsonNullable.TypedefInt value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var memoryStream = new global::System.IO.MemoryStream();
                            SerializeJson(memoryStream, value, options);
                            return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                        }

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable.TypedefInt value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                            SerializeJson(writer, value);
                            writer.Flush();
                        }

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable.TypedefInt value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                            SerializeJson(writer, value);
                            writer.Flush();
                        }

                        /// <summary>
                        /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                        /// </summary>
                        /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TypedefJsonNullable.TypedefInt value)
                        {
                            writer.WriteNumberValue(value.value);
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                            => SerializeJsonAsync(stream, this, options);

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                            => SerializeJsonAsync(bufferWriter, this, options);

                        /// <summary>
                        /// Serialize an object to a string.
                        /// </summary>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <returns>Json encoded string value.</returns>
                        public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(this, options);

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(stream, this, options);

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(bufferWriter, this, options);

                        /// <summary>
                        /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                        /// </summary>
                        /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                            => SerializeJson(writer, this);

                    }
                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefInt
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out int result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type int32.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type int32.";
                                    result = default;
                                    return false;
                                }
                            }

                            if (!reader.TryGetInt32(out var token_0))
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                {
                                    error = "Was null. Should be of type int32.";
                                    result = default;
                                    return false;
                                }
                                else
                                {
                                    error = "Was not of type int32.";
                                    result = default;
                                    return false;
                                }
                            }

                            output_0 = token_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, int value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, int value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(int value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, int value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, int value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, int value)
                    {
                        writer.WriteNumberValue(value);
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TypedefJsonNullable.TypedefComplex.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    public readonly partial struct TypedefComplex
                    {
                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="json">A raw json string to parse.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(string json, out global::Tests.TypedefJsonNullable.TypedefComplex result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                            var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                            return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::Tests.TypedefJsonNullable.TypedefComplex result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                            return TryParseJson(ref reader, out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::Tests.TypedefJsonNullable.TypedefComplex result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                            return TryParseJson(ref reader, out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::Tests.TypedefJsonNullable.TypedefComplex result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            try
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                                {
                                    error = "Cannot parse empty string.";
                                    result = default;
                                    return false;
                                }

                                if (!__KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefComplex.TryParseJson(ref reader, out global::Tests.InvalidValueEnumClass? output_0, out error))
                                {
                                    result = default;
                                    return false;
                                }

                                result = (global::Tests.TypedefJsonNullable.TypedefComplex)output_0;
                                error = default;
                                return true;
                            }
                            catch (global::System.Text.Json.JsonException)
                            {
                                result = default;
                                error = "Failed to parse json.";
                                return false;
                            }
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable.TypedefComplex value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        {
                            await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                            SerializeJson(writer, value);
                            await writer.FlushAsync(cancellationToken);
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable.TypedefComplex value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        {
                            await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                            SerializeJson(writer, value);
                            await writer.FlushAsync(cancellationToken);
                        }

                        /// <summary>
                        /// Serialize an object to a string.
                        /// </summary>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <returns>Json encoded string value.</returns>
                        public static string SerializeJson(global::Tests.TypedefJsonNullable.TypedefComplex value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var memoryStream = new global::System.IO.MemoryStream();
                            SerializeJson(memoryStream, value, options);
                            return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                        }

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable.TypedefComplex value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                            SerializeJson(writer, value);
                            writer.Flush();
                        }

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable.TypedefComplex value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                            SerializeJson(writer, value);
                            writer.Flush();
                        }

                        /// <summary>
                        /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                        /// </summary>
                        /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TypedefJsonNullable.TypedefComplex value)
                        {
                            __KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefComplex.SerializeJson(writer, (global::Tests.InvalidValueEnumClass)value);
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                            => SerializeJsonAsync(stream, this, options);

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                            => SerializeJsonAsync(bufferWriter, this, options);

                        /// <summary>
                        /// Serialize an object to a string.
                        /// </summary>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <returns>Json encoded string value.</returns>
                        public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(this, options);

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(stream, this, options);

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(bufferWriter, this, options);

                        /// <summary>
                        /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                        /// </summary>
                        /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                            => SerializeJson(writer, this);

                    }
                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefComplex
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.InvalidValueEnumClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.InvalidValueEnumClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.InvalidValueEnumClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.InvalidValueEnumClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            var member_0 = default(global::KestrelToolbox.Internal.OptionalProperty<int>);
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.InvalidValueEnumClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        int output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_0))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            result = new global::Tests.InvalidValueEnumClass()
                            {
                            };
                            if (member_0.HasValue)
                            {
                                result.Value = (int)member_0;
                            }

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.InvalidValueEnumClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.InvalidValueEnumClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.InvalidValueEnumClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.InvalidValueEnumClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.InvalidValueEnumClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.InvalidValueEnumClass value)
                    {
                        writer.WriteStartObject();
                        var member_1 = value.Value;
                        writer.WriteNumber("value", member_1);
                        writer.WriteEndObject();
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TypedefJsonNullable.TypedefCustom.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    public readonly partial struct TypedefCustom
                    {
                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="json">A raw json string to parse.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(string json, out global::Tests.TypedefJsonNullable.TypedefCustom result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                            var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                            return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::Tests.TypedefJsonNullable.TypedefCustom result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                            return TryParseJson(ref reader, out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::Tests.TypedefJsonNullable.TypedefCustom result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                            return TryParseJson(ref reader, out result, out error);
                        }

                        /// <summary>
                        /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                        /// </summary>
                        /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                        /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                        /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                        /// <returns>True if parsing was successful, false if it was not.</returns>
                        /// <remarks>
                        /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                        ///
                        /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                        /// force initialization of value type fields.
                        /// </remarks>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::Tests.TypedefJsonNullable.TypedefCustom result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                        {
                            try
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                                {
                                    error = "Cannot parse empty string.";
                                    result = default;
                                    return false;
                                }

                                if (!__KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefCustom.TryParseJson(ref reader, out global::Tests.CustomSerializeClass? output_0, out error))
                                {
                                    result = default;
                                    return false;
                                }

                                result = (global::Tests.TypedefJsonNullable.TypedefCustom)output_0;
                                error = default;
                                return true;
                            }
                            catch (global::System.Text.Json.JsonException)
                            {
                                result = default;
                                error = "Failed to parse json.";
                                return false;
                            }
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable.TypedefCustom value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        {
                            await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                            SerializeJson(writer, value);
                            await writer.FlushAsync(cancellationToken);
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable.TypedefCustom value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        {
                            await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                            SerializeJson(writer, value);
                            await writer.FlushAsync(cancellationToken);
                        }

                        /// <summary>
                        /// Serialize an object to a string.
                        /// </summary>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <returns>Json encoded string value.</returns>
                        public static string SerializeJson(global::Tests.TypedefJsonNullable.TypedefCustom value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var memoryStream = new global::System.IO.MemoryStream();
                            SerializeJson(memoryStream, value, options);
                            return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                        }

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TypedefJsonNullable.TypedefCustom value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                            SerializeJson(writer, value);
                            writer.Flush();
                        }

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TypedefJsonNullable.TypedefCustom value, global::System.Text.Json.JsonWriterOptions options = default)
                        {
                            using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                            SerializeJson(writer, value);
                            writer.Flush();
                        }

                        /// <summary>
                        /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                        /// </summary>
                        /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                        /// <param name="value">The object to serialize.</param>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TypedefJsonNullable.TypedefCustom value)
                        {
                            __KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefCustom.SerializeJson(writer, (global::Tests.CustomSerializeClass)value);
                        }

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                            => SerializeJsonAsync(stream, this, options);

                        /// <summary>
                        /// Asynchronously serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <param name="cancellationToken">Cancellation token.</param>
                        /// <returns>void.</returns>
                        public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                            => SerializeJsonAsync(bufferWriter, this, options);

                        /// <summary>
                        /// Serialize an object to a string.
                        /// </summary>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        /// <returns>Json encoded string value.</returns>
                        public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(this, options);

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="stream">The stream to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(stream, this, options);

                        /// <summary>
                        /// Serialize an object to a stream.
                        /// </summary>
                        /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                        /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                        public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                            => SerializeJson(bufferWriter, this, options);

                        /// <summary>
                        /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                        /// </summary>
                        /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                        [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                        public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                            => SerializeJson(writer, this);

                    }
                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TypedefJsonNullable_TypedefCustom
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            var member_0 = default(global::KestrelToolbox.Internal.OptionalProperty<global::Tests.CustomSerializeClass.CustomValueClass?>);
                            var member_1 = default(global::KestrelToolbox.Internal.OptionalProperty<global::Tests.CustomSerializeClass.CustomValueStruct>);
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.CustomSerializeClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "class":
                                    {
                                        global::Tests.CustomSerializeClass.CustomValueClass? output_0;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_0 = null;
                                        }
                                        else
                                        {
                                            if (!global::Tests.CustomSerializeClass.CustomValueClass.TryParseCustomValueClass(ref reader, out global::Tests.CustomSerializeClass.CustomValueClass? input_0, out error))
                                            {
                                                error = $"class > {error}";
                                                result = default;
                                                return false;
                                            }

                                            output_0 = input_0;
                                        }

                                        member_0 = output_0;
                                        break;
                                    }

                                    case "struct":
                                    {
                                        global::Tests.CustomSerializeClass.CustomValueStruct output_1;
                                        if (!global::Tests.CustomSerializeClass.CustomValueStruct.TryParseCustomValueStruct(ref reader, out global::Tests.CustomSerializeClass.CustomValueStruct input_1, out error))
                                        {
                                            error = $"struct > {error}";
                                            result = default;
                                            return false;
                                        }

                                        output_1 = input_1;
                                        member_1 = output_1;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            result = new global::Tests.CustomSerializeClass()
                            {
                            };
                            if (member_0.HasValue)
                            {
                                result.Class = (global::Tests.CustomSerializeClass.CustomValueClass?)member_0;
                            }
                            if (member_1.HasValue)
                            {
                                result.Struct = (global::Tests.CustomSerializeClass.CustomValueStruct)member_1;
                            }

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.CustomSerializeClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.CustomSerializeClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.CustomSerializeClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.CustomSerializeClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.CustomSerializeClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.CustomSerializeClass value)
                    {
                        writer.WriteStartObject();
                        var member_2 = value.Class;
                        if (member_2 != null)
                        {
                            writer.WritePropertyName("class");
                            global::Tests.CustomSerializeClass.CustomValueClass.Serialize(writer, member_2);
                        }
                        else
                        {
                            writer.WriteNull("class");
                        }

                        var member_3 = value.Struct;
                        writer.WritePropertyName("struct");
                        global::Tests.CustomSerializeClass.CustomValueStruct.Serialize(writer, member_3);
                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass.CustomValueClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass.CustomValueClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass.CustomValueClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.CustomSerializeClass.CustomValueClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::Tests.CustomSerializeClass.CustomValueClass? output_0;
                            if (!global::Tests.CustomSerializeClass.CustomValueClass.TryParseCustomValueClass(ref reader, out global::Tests.CustomSerializeClass.CustomValueClass? input_0, out error))
                            {
                                result = default;
                                return false;
                            }

                            output_0 = input_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.CustomSerializeClass.CustomValueClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.CustomSerializeClass.CustomValueClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.CustomSerializeClass.CustomValueClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.CustomSerializeClass.CustomValueClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.CustomSerializeClass.CustomValueClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.CustomSerializeClass.CustomValueClass value)
                    {
                        if (value != null)
                        {
                            global::Tests.CustomSerializeClass.CustomValueClass.Serialize(writer, value);
                        }
                        else
                        {
                            writer.WriteNullValue();
                        }
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, out global::Tests.CustomSerializeClass.CustomValueStruct result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, out global::Tests.CustomSerializeClass.CustomValueStruct result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, out global::Tests.CustomSerializeClass.CustomValueStruct result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, out global::Tests.CustomSerializeClass.CustomValueStruct result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::Tests.CustomSerializeClass.CustomValueStruct output_0;
                            if (!global::Tests.CustomSerializeClass.CustomValueStruct.TryParseCustomValueStruct(ref reader, out global::Tests.CustomSerializeClass.CustomValueStruct input_0, out error))
                            {
                                result = default;
                                return false;
                            }

                            output_0 = input_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.CustomSerializeClass.CustomValueStruct value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.CustomSerializeClass.CustomValueStruct value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.CustomSerializeClass.CustomValueStruct value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.CustomSerializeClass.CustomValueStruct value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.CustomSerializeClass.CustomValueStruct value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.CustomSerializeClass.CustomValueStruct value)
                    {
                        global::Tests.CustomSerializeClass.CustomValueStruct.Serialize(writer, value);
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TypedefJsonNullable.TypedefInt.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    public readonly partial struct TypedefInt : global::System.IEquatable<TypedefInt>
                    {
                        private readonly int value;

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TypedefInt"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        public TypedefInt(int value) => this.value = value;

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TypedefInt"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        /// <returns>New instance of the <see cref="TypedefInt"/> class.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static TypedefInt? From(int? value) => value is null ? null : new TypedefInt(value.Value);

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TypedefInt"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        /// <returns>New instance of the <see cref="TypedefInt"/> class.</returns>
                        public static TypedefInt From(int value) => new TypedefInt(value);

                        /// <summary>
                        /// Gets the aliased type of this typedef.
                        /// </summary>
                        /// <param name="value">Value to cast into the aliased type.</param>
                        /// <returns>The aliased type.</returns>
                        public static explicit operator int(TypedefInt value) => value.value;

                        /// <summary>
                        /// Gets a typedef from the aliased type.
                        /// </summary>
                        /// <param name="value">Value to cast into the typedef.</param>
                        /// <returns>The typedef.</returns>
                        public static explicit operator TypedefInt(int value) => new TypedefInt(value);

                        /// <inheritdoc />
                        public bool Equals(TypedefInt other) => this.value.Equals(other.value);

                        /// <summary>
                        /// Compares two aliased types for equality.
                        /// </summary>
                        /// <param name="left">Left hand side of operator.</param>
                        /// <param name="right">Right hand side of operator.</param>
                        /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                        public static bool operator==(TypedefInt left, TypedefInt right) => left.Equals(right);

                        /// <summary>
                        /// Compares two aliased types for equality.
                        /// </summary>
                        /// <param name="left">Left hand side of operator.</param>
                        /// <param name="right">Right hand side of operator.</param>
                        /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                        public static bool operator!=(TypedefInt left, TypedefInt right) => !left.Equals(right);

                        /// <inheritdoc cref="int.CompareTo(object)" />
                        public int CompareTo(object? value) =>
                            this.value.CompareTo(value);

                        /// <inheritdoc cref="int.CompareTo(int)" />
                        public int CompareTo(int value) =>
                            this.value.CompareTo(value);

                        /// <inheritdoc cref="int.Equals(object)" />
                        public override bool Equals(object? obj) =>
                            obj is global::Tests.TypedefJsonNullable.TypedefInt other && this.Equals(other);

                        /// <inheritdoc cref="int.Equals(int)" />
                        public bool Equals(int obj) =>
                            this.value.Equals(obj);

                        /// <inheritdoc cref="int.GetHashCode()" />
                        public override int GetHashCode() =>
                            this.value.GetHashCode();

                        /// <inheritdoc cref="int.ToString()" />
                        public override string ToString() =>
                            this.value.ToString();

                        /// <inheritdoc cref="int.ToString(string)" />
                        public string ToString(string? format) =>
                            this.value.ToString(format);

                        /// <inheritdoc cref="int.ToString(global::System.IFormatProvider)" />
                        public string ToString(System.IFormatProvider? provider) =>
                            this.value.ToString(provider);

                        /// <inheritdoc cref="int.ToString(string,global::System.IFormatProvider)" />
                        public string ToString(string? format, System.IFormatProvider? provider) =>
                            this.value.ToString(format, provider);

                        /// <inheritdoc cref="int.TryFormat(global::System.Span{char},out int,global::System.ReadOnlySpan{char},global::System.IFormatProvider)" />
                        public bool TryFormat(System.Span<char> destination, out int charsWritten, System.ReadOnlySpan<char> format, System.IFormatProvider? provider) =>
                            this.value.TryFormat(destination, out charsWritten, format, provider);

                        /// <inheritdoc cref="int.TryFormat(global::System.Span{byte},out int,global::System.ReadOnlySpan{char},global::System.IFormatProvider)" />
                        public bool TryFormat(System.Span<byte> utf8Destination, out int bytesWritten, System.ReadOnlySpan<char> format, System.IFormatProvider? provider) =>
                            this.value.TryFormat(utf8Destination, out bytesWritten, format, provider);

                        /// <inheritdoc cref="int.GetTypeCode()" />
                        public global::System.TypeCode GetTypeCode() =>
                            this.value.GetTypeCode();


                    }
                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TypedefJsonNullable.TypedefComplex.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    public readonly partial struct TypedefComplex
                    {
                        private readonly global::Tests.InvalidValueEnumClass value;

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TypedefComplex"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        public TypedefComplex(global::Tests.InvalidValueEnumClass value) => this.value = value;

                        /// <summary>
                        /// Gets the aliased type of this typedef.
                        /// </summary>
                        /// <param name="value">Value to cast into the aliased type.</param>
                        /// <returns>The aliased type.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static explicit operator global::Tests.InvalidValueEnumClass?(TypedefComplex? value) => value?.value;

                        /// <summary>
                        /// Gets a typedef from the aliased type.
                        /// </summary>
                        /// <param name="value">Value to cast into the typedef.</param>
                        /// <returns>The typedef.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static explicit operator TypedefComplex?(global::Tests.InvalidValueEnumClass? value) => value == null ? null : new TypedefComplex(value);

                    }
                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TypedefJsonNullable.TypedefCustom.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public sealed partial class TypedefJsonNullable
                {
                    public readonly partial struct TypedefCustom
                    {
                        private readonly global::Tests.CustomSerializeClass value;

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TypedefCustom"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        public TypedefCustom(global::Tests.CustomSerializeClass value) => this.value = value;

                        /// <summary>
                        /// Gets the aliased type of this typedef.
                        /// </summary>
                        /// <param name="value">Value to cast into the aliased type.</param>
                        /// <returns>The aliased type.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static explicit operator global::Tests.CustomSerializeClass?(TypedefCustom? value) => value?.value;

                        /// <summary>
                        /// Gets a typedef from the aliased type.
                        /// </summary>
                        /// <param name="value">Value to cast into the typedef.</param>
                        /// <returns>The typedef.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static explicit operator TypedefCustom?(global::Tests.CustomSerializeClass? value) => value == null ? null : new TypedefCustom(value);

                    }
                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }
}
