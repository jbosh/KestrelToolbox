// <copyright file="TypedefGeneratorTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text.Json;
using KestrelToolbox.Serialization;
using KestrelToolbox.Types;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

namespace UnitTests.GeneratorTests.TypdefGenerator;

/// <summary>
/// Tests for <see cref="TypedefAttribute"/>.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.LayoutRules",
    "SA1515:Single-line comment should be preceded by blank line",
    Justification = "Comments for language."
)]
public sealed partial class TypedefGeneratorTests
{
    public TypedefGeneratorTests()
    {
        // Use some of the types so the DLL loads.
        _ = new TypedefAttribute(typeof(int));
        _ = new JsonSerializableAttribute(typeof(int));
        _ = new Utf8JsonWriter(new MemoryStream());
    }

    [Fact]
    public void Int32Test()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(int))]
            public partial class TestTypedef {}
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef : global::System.IEquatable<TestTypedef>
                {
                    private readonly int value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(int value) => this.value = value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                    [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                    public static TestTypedef? From(int? value) => value is null ? null : new TestTypedef(value.Value);

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                    public static TestTypedef From(int value) => new TestTypedef(value);

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    public static explicit operator int(TestTypedef value) => value.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    public static explicit operator TestTypedef(int value) => new TestTypedef(value);

                    /// <inheritdoc />
                    public bool Equals(TestTypedef? other) => this.value.Equals(other?.value);

                    /// <summary>
                    /// Compares two aliased types for equality.
                    /// </summary>
                    /// <param name="left">Left hand side of operator.</param>
                    /// <param name="right">Right hand side of operator.</param>
                    /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                    public static bool operator==(TestTypedef? left, TestTypedef? right) => object.Equals(left, right);

                    /// <summary>
                    /// Compares two aliased types for equality.
                    /// </summary>
                    /// <param name="left">Left hand side of operator.</param>
                    /// <param name="right">Right hand side of operator.</param>
                    /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                    public static bool operator!=(TestTypedef? left, TestTypedef? right) => !object.Equals(left, right);

                    /// <inheritdoc cref="int.CompareTo(object)" />
                    public int CompareTo(object? value) =>
                        this.value.CompareTo(value);

                    /// <inheritdoc cref="int.CompareTo(int)" />
                    public int CompareTo(int value) =>
                        this.value.CompareTo(value);

                    /// <inheritdoc cref="int.Equals(object)" />
                    public override bool Equals(object? obj) =>
                        obj is global::Tests.TestTypedef other && this.Equals(other);

                    /// <inheritdoc cref="int.Equals(int)" />
                    public bool Equals(int obj) =>
                        this.value.Equals(obj);

                    /// <inheritdoc cref="int.GetHashCode()" />
                    public override int GetHashCode() =>
                        this.value.GetHashCode();

                    /// <inheritdoc cref="int.ToString()" />
                    public override string ToString() =>
                        this.value.ToString();

                    /// <inheritdoc cref="int.ToString(string)" />
                    public string ToString(string? format) =>
                        this.value.ToString(format);

                    /// <inheritdoc cref="int.ToString(global::System.IFormatProvider)" />
                    public string ToString(System.IFormatProvider? provider) =>
                        this.value.ToString(provider);

                    /// <inheritdoc cref="int.ToString(string,global::System.IFormatProvider)" />
                    public string ToString(string? format, System.IFormatProvider? provider) =>
                        this.value.ToString(format, provider);

                    /// <inheritdoc cref="int.TryFormat(global::System.Span{char},out int,global::System.ReadOnlySpan{char},global::System.IFormatProvider)" />
                    public bool TryFormat(System.Span<char> destination, out int charsWritten, System.ReadOnlySpan<char> format, System.IFormatProvider? provider) =>
                        this.value.TryFormat(destination, out charsWritten, format, provider);

                    /// <inheritdoc cref="int.TryFormat(global::System.Span{byte},out int,global::System.ReadOnlySpan{char},global::System.IFormatProvider)" />
                    public bool TryFormat(System.Span<byte> utf8Destination, out int bytesWritten, System.ReadOnlySpan<char> format, System.IFormatProvider? provider) =>
                        this.value.TryFormat(utf8Destination, out bytesWritten, format, provider);

                    /// <inheritdoc cref="int.GetTypeCode()" />
                    public global::System.TypeCode GetTypeCode() =>
                        this.value.GetTypeCode();


                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void ClassWithProperties()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            public sealed class BaseClass
            {
                public string Value { get; set; }

                public string Value2 { get; set; }

                public int Field;

                public int this[int index]
                {
                    get => index;
                    set => System.Console.WriteLine(index);
                }
            }

            [Typedef(typeof(BaseClass))]
            public partial class TestTypedef {}
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef : global::System.IEquatable<TestTypedef>
                {
                    private readonly global::Tests.BaseClass value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(global::Tests.BaseClass value) => this.value = value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                    [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                    public static TestTypedef? From(global::Tests.BaseClass? value) => value is null ? null : new TestTypedef(value);

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                    public static explicit operator global::Tests.BaseClass?(TestTypedef? value) => value?.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                    public static explicit operator TestTypedef?(global::Tests.BaseClass? value) => value == null ? null : new TestTypedef(value);

                    /// <inheritdoc />
                    public bool Equals(TestTypedef? other) => this.value.Equals(other?.value);

                    /// <summary>
                    /// Compares two aliased types for equality.
                    /// </summary>
                    /// <param name="left">Left hand side of operator.</param>
                    /// <param name="right">Right hand side of operator.</param>
                    /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                    public static bool operator==(TestTypedef? left, TestTypedef? right) => object.Equals(left, right);

                    /// <summary>
                    /// Compares two aliased types for equality.
                    /// </summary>
                    /// <param name="left">Left hand side of operator.</param>
                    /// <param name="right">Right hand side of operator.</param>
                    /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                    public static bool operator!=(TestTypedef? left, TestTypedef? right) => !object.Equals(left, right);

                    /// <inheritdoc cref="global::Tests.BaseClass.Value" />
                    public string Value
                    {
                        get => this.value.Value;
                        set => this.value.Value = value;
                    }

                    /// <inheritdoc cref="global::Tests.BaseClass.Value2" />
                    public string Value2
                    {
                        get => this.value.Value2;
                        set => this.value.Value2 = value;
                    }

                    /// <inheritdoc cref="global::Tests.BaseClass.this[int]" />
                    public int this[int index]
                    {
                        get => this.value[index];
                        set => this.value[index] = value;
                    }

                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void TypedefFeaturesNone()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(int), TypedefFeatures.None)]
            public partial class TestTypedef {}
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef
                {
                    private readonly int value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(int value) => this.value = value;

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    public static explicit operator int(TestTypedef value) => value.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    public static explicit operator TestTypedef(int value) => new TestTypedef(value);

                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void TypedefFeaturesAliasMethods()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(int), TypedefFeatures.AliasMethods)]
            public partial class TestTypedef {}
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef
                {
                    private readonly int value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(int value) => this.value = value;

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    public static explicit operator int(TestTypedef value) => value.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    public static explicit operator TestTypedef(int value) => new TestTypedef(value);

                    /// <inheritdoc cref="int.CompareTo(object)" />
                    public int CompareTo(object? value) =>
                        this.value.CompareTo(value);

                    /// <inheritdoc cref="int.CompareTo(int)" />
                    public int CompareTo(int value) =>
                        this.value.CompareTo(value);

                    /// <inheritdoc cref="int.Equals(object)" />
                    public override bool Equals(object? obj) =>
                        obj is global::Tests.TestTypedef other && this.Equals(other);

                    /// <inheritdoc cref="int.Equals(int)" />
                    public bool Equals(int obj) =>
                        this.value.Equals(obj);

                    /// <inheritdoc cref="int.GetHashCode()" />
                    public override int GetHashCode() =>
                        this.value.GetHashCode();

                    /// <inheritdoc cref="int.ToString()" />
                    public override string ToString() =>
                        this.value.ToString();

                    /// <inheritdoc cref="int.ToString(string)" />
                    public string ToString(string? format) =>
                        this.value.ToString(format);

                    /// <inheritdoc cref="int.ToString(global::System.IFormatProvider)" />
                    public string ToString(System.IFormatProvider? provider) =>
                        this.value.ToString(provider);

                    /// <inheritdoc cref="int.ToString(string,global::System.IFormatProvider)" />
                    public string ToString(string? format, System.IFormatProvider? provider) =>
                        this.value.ToString(format, provider);

                    /// <inheritdoc cref="int.TryFormat(global::System.Span{char},out int,global::System.ReadOnlySpan{char},global::System.IFormatProvider)" />
                    public bool TryFormat(System.Span<char> destination, out int charsWritten, System.ReadOnlySpan<char> format, System.IFormatProvider? provider) =>
                        this.value.TryFormat(destination, out charsWritten, format, provider);

                    /// <inheritdoc cref="int.TryFormat(global::System.Span{byte},out int,global::System.ReadOnlySpan{char},global::System.IFormatProvider)" />
                    public bool TryFormat(System.Span<byte> utf8Destination, out int bytesWritten, System.ReadOnlySpan<char> format, System.IFormatProvider? provider) =>
                        this.value.TryFormat(utf8Destination, out bytesWritten, format, provider);

                    /// <inheritdoc cref="int.GetTypeCode()" />
                    public global::System.TypeCode GetTypeCode() =>
                        this.value.GetTypeCode();


                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void TypedefFeaturesEquatable()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(int), TypedefFeatures.Equatable)]
            public partial class TestTypedef {}
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef : global::System.IEquatable<TestTypedef>
                {
                    private readonly int value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(int value) => this.value = value;

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    public static explicit operator int(TestTypedef value) => value.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    public static explicit operator TestTypedef(int value) => new TestTypedef(value);

                    /// <inheritdoc />
                    public bool Equals(TestTypedef? other) => this.value.Equals(other?.value);

                    /// <summary>
                    /// Compares two aliased types for equality.
                    /// </summary>
                    /// <param name="left">Left hand side of operator.</param>
                    /// <param name="right">Right hand side of operator.</param>
                    /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                    public static bool operator==(TestTypedef? left, TestTypedef? right) => object.Equals(left, right);

                    /// <summary>
                    /// Compares two aliased types for equality.
                    /// </summary>
                    /// <param name="left">Left hand side of operator.</param>
                    /// <param name="right">Right hand side of operator.</param>
                    /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                    public static bool operator!=(TestTypedef? left, TestTypedef? right) => !object.Equals(left, right);

                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void TypedefFeaturesFromMethods()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(int), TypedefFeatures.FromMethods)]
            public partial class TestTypedef {}
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestTypedef
                {
                    private readonly int value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    public TestTypedef(int value) => this.value = value;

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                    [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                    public static TestTypedef? From(int? value) => value is null ? null : new TestTypedef(value.Value);

                    /// <summary>
                    /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                    /// </summary>
                    /// <param name="value">Value to alias using this typedef.</param>
                    /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                    public static TestTypedef From(int value) => new TestTypedef(value);

                    /// <summary>
                    /// Gets the aliased type of this typedef.
                    /// </summary>
                    /// <param name="value">Value to cast into the aliased type.</param>
                    /// <returns>The aliased type.</returns>
                    public static explicit operator int(TestTypedef value) => value.value;

                    /// <summary>
                    /// Gets a typedef from the aliased type.
                    /// </summary>
                    /// <param name="value">Value to cast into the typedef.</param>
                    /// <returns>The typedef.</returns>
                    public static explicit operator TestTypedef(int value) => new TestTypedef(value);

                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void TypedefStruct()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            public static partial class ContainingClass
            {
                [Typedef(typeof(object))]
                public readonly partial struct TestTypedef {}
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.ContainingClass.TestTypedef.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public static partial class ContainingClass
                {
                    public readonly partial struct TestTypedef : global::System.IEquatable<TestTypedef>
                    {
                        private readonly object value;

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        public TestTypedef(object value) => this.value = value;

                        /// <summary>
                        /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                        /// </summary>
                        /// <param name="value">Value to alias using this typedef.</param>
                        /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static TestTypedef? From(object? value) => value is null ? null : new TestTypedef(value);

                        /// <summary>
                        /// Gets the aliased type of this typedef.
                        /// </summary>
                        /// <param name="value">Value to cast into the aliased type.</param>
                        /// <returns>The aliased type.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static explicit operator object?(TestTypedef? value) => value?.value;

                        /// <summary>
                        /// Gets a typedef from the aliased type.
                        /// </summary>
                        /// <param name="value">Value to cast into the typedef.</param>
                        /// <returns>The typedef.</returns>
                        [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                        public static explicit operator TestTypedef?(object? value) => value == null ? null : new TestTypedef(value);

                        /// <inheritdoc />
                        public bool Equals(TestTypedef other) => this.value.Equals(other.value);

                        /// <summary>
                        /// Compares two aliased types for equality.
                        /// </summary>
                        /// <param name="left">Left hand side of operator.</param>
                        /// <param name="right">Right hand side of operator.</param>
                        /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                        public static bool operator==(TestTypedef left, TestTypedef right) => left.Equals(right);

                        /// <summary>
                        /// Compares two aliased types for equality.
                        /// </summary>
                        /// <param name="left">Left hand side of operator.</param>
                        /// <param name="right">Right hand side of operator.</param>
                        /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                        public static bool operator!=(TestTypedef left, TestTypedef right) => !left.Equals(right);

                        /// <inheritdoc cref="object.GetType()" />
                        public global::System.Type GetType() =>
                            this.value.GetType();

                        /// <inheritdoc cref="object.ToString()" />
                        public string? ToString() =>
                            this.value.ToString();

                        /// <inheritdoc cref="object.Equals(object)" />
                        public bool Equals(object? obj) =>
                            this.value.Equals(obj);

                        /// <inheritdoc cref="object.GetHashCode()" />
                        public int GetHashCode() =>
                            this.value.GetHashCode();


                    }
                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }
}
