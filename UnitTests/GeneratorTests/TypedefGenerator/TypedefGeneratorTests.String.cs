// <copyright file="TypedefGeneratorTests.String.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Types;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

namespace UnitTests.GeneratorTests.TypdefGenerator;

/// <summary>
/// Tests for <see cref="TypedefAttribute"/>.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.LayoutRules",
    "SA1515:Single-line comment should be preceded by blank line",
    Justification = "Comments for language."
)]
public sealed partial class TypedefGeneratorTests
{
    [Fact]
    public void StringTest()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;

            namespace Tests;

            [Typedef(typeof(string))]
            public partial struct TestTypedef {}
            """;

        // Cannot compile out csharp strings, csharpier does not handle them nested within #if DOTNET_9_0.
#pragma warning disable CS0219, S1481 // Variable is assigned but its value is never used

        // Language=C#
        var dotnet8Source = """
            namespace Tests;

            #nullable enable
            public partial struct TestTypedef : global::System.IEquatable<TestTypedef>
            {
                private readonly string value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                public TestTypedef(string value) => this.value = value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static TestTypedef? From(string? value) => value is null ? null : new TestTypedef(value);

                /// <summary>
                /// Gets the aliased type of this typedef.
                /// </summary>
                /// <param name="value">Value to cast into the aliased type.</param>
                /// <returns>The aliased type.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator string?(TestTypedef? value) => value?.value;

                /// <summary>
                /// Gets a typedef from the aliased type.
                /// </summary>
                /// <param name="value">Value to cast into the typedef.</param>
                /// <returns>The typedef.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator TestTypedef?(string? value) => value == null ? null : new TestTypedef(value);

                /// <inheritdoc />
                public bool Equals(TestTypedef other) => this.value.Equals(other.value);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                public static bool operator==(TestTypedef left, TestTypedef right) => left.Equals(right);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                public static bool operator!=(TestTypedef left, TestTypedef right) => !left.Equals(right);

                /// <inheritdoc cref="string.CompareTo(object)" />
                public int CompareTo(object? value) =>
                    this.value.CompareTo(value);

                /// <inheritdoc cref="string.CompareTo(string)" />
                public int CompareTo(string? strB) =>
                    this.value.CompareTo(strB);

                /// <inheritdoc cref="string.EndsWith(string)" />
                public bool EndsWith(string value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.EndsWith(string,global::System.StringComparison)" />
                public bool EndsWith(string value, System.StringComparison comparisonType) =>
                    this.value.EndsWith(value, comparisonType);

                /// <inheritdoc cref="string.EndsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool EndsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.EndsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.EndsWith(char)" />
                public bool EndsWith(char value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.Equals(object)" />
                public override bool Equals(object? obj) =>
                    obj is global::Tests.TestTypedef other && this.Equals(other);

                /// <inheritdoc cref="string.Equals(string)" />
                public bool Equals(string? value) =>
                    this.value.Equals(value);

                /// <inheritdoc cref="string.Equals(string,global::System.StringComparison)" />
                public bool Equals(string? value, System.StringComparison comparisonType) =>
                    this.value.Equals(value, comparisonType);

                /// <inheritdoc cref="string.GetHashCode()" />
                public override int GetHashCode() =>
                    this.value.GetHashCode();

                /// <inheritdoc cref="string.GetHashCode(global::System.StringComparison)" />
                public int GetHashCode(System.StringComparison comparisonType) =>
                    this.value.GetHashCode(comparisonType);

                /// <inheritdoc cref="string.StartsWith(string)" />
                public bool StartsWith(string value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.StartsWith(string,global::System.StringComparison)" />
                public bool StartsWith(string value, System.StringComparison comparisonType) =>
                    this.value.StartsWith(value, comparisonType);

                /// <inheritdoc cref="string.StartsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool StartsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.StartsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.StartsWith(char)" />
                public bool StartsWith(char value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.Clone()" />
                public object Clone() =>
                    this.value.Clone();

                /// <inheritdoc cref="string.CopyTo(int,char[],int,int)" />
                public void CopyTo(int sourceIndex, char[] destination, int destinationIndex, int count) =>
                    this.value.CopyTo(sourceIndex, destination, destinationIndex, count);

                /// <inheritdoc cref="string.CopyTo(global::System.Span{char})" />
                public void CopyTo(System.Span<char> destination) =>
                    this.value.CopyTo(destination);

                /// <inheritdoc cref="string.TryCopyTo(global::System.Span{char})" />
                public bool TryCopyTo(System.Span<char> destination) =>
                    this.value.TryCopyTo(destination);

                /// <inheritdoc cref="string.ToCharArray()" />
                public char[] ToCharArray() =>
                    this.value.ToCharArray();

                /// <inheritdoc cref="string.ToCharArray(int,int)" />
                public char[] ToCharArray(int startIndex, int length) =>
                    this.value.ToCharArray(startIndex, length);

                /// <inheritdoc cref="string.GetPinnableReference()" />
                public char GetPinnableReference() =>
                    this.value.GetPinnableReference();

                /// <inheritdoc cref="string.ToString()" />
                public override string ToString() =>
                    this.value.ToString();

                /// <inheritdoc cref="string.ToString(global::System.IFormatProvider)" />
                public string ToString(System.IFormatProvider? provider) =>
                    this.value.ToString(provider);

                /// <inheritdoc cref="string.GetEnumerator()" />
                public global::System.CharEnumerator GetEnumerator() =>
                    this.value.GetEnumerator();

                /// <inheritdoc cref="string.EnumerateRunes()" />
                public global::System.Text.StringRuneEnumerator EnumerateRunes() =>
                    this.value.EnumerateRunes();

                /// <inheritdoc cref="string.GetTypeCode()" />
                public global::System.TypeCode GetTypeCode() =>
                    this.value.GetTypeCode();

                /// <inheritdoc cref="string.IsNormalized()" />
                public bool IsNormalized() =>
                    this.value.IsNormalized();

                /// <inheritdoc cref="string.IsNormalized(global::System.Text.NormalizationForm)" />
                public bool IsNormalized(System.Text.NormalizationForm normalizationForm) =>
                    this.value.IsNormalized(normalizationForm);

                /// <inheritdoc cref="string.Normalize()" />
                public string Normalize() =>
                    this.value.Normalize();

                /// <inheritdoc cref="string.Normalize(global::System.Text.NormalizationForm)" />
                public string Normalize(System.Text.NormalizationForm normalizationForm) =>
                    this.value.Normalize(normalizationForm);

                /// <inheritdoc cref="string.Insert(int,string)" />
                public string Insert(int startIndex, string value) =>
                    this.value.Insert(startIndex, value);

                /// <inheritdoc cref="string.PadLeft(int)" />
                public string PadLeft(int totalWidth) =>
                    this.value.PadLeft(totalWidth);

                /// <inheritdoc cref="string.PadLeft(int,char)" />
                public string PadLeft(int totalWidth, char paddingChar) =>
                    this.value.PadLeft(totalWidth, paddingChar);

                /// <inheritdoc cref="string.PadRight(int)" />
                public string PadRight(int totalWidth) =>
                    this.value.PadRight(totalWidth);

                /// <inheritdoc cref="string.PadRight(int,char)" />
                public string PadRight(int totalWidth, char paddingChar) =>
                    this.value.PadRight(totalWidth, paddingChar);

                /// <inheritdoc cref="string.Remove(int,int)" />
                public string Remove(int startIndex, int count) =>
                    this.value.Remove(startIndex, count);

                /// <inheritdoc cref="string.Remove(int)" />
                public string Remove(int startIndex) =>
                    this.value.Remove(startIndex);

                /// <inheritdoc cref="string.Replace(string,string,bool,global::System.Globalization.CultureInfo)" />
                public string Replace(string oldValue, string? newValue, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.Replace(oldValue, newValue, ignoreCase, culture);

                /// <inheritdoc cref="string.Replace(string,string,global::System.StringComparison)" />
                public string Replace(string oldValue, string? newValue, System.StringComparison comparisonType) =>
                    this.value.Replace(oldValue, newValue, comparisonType);

                /// <inheritdoc cref="string.Replace(char,char)" />
                public string Replace(char oldChar, char newChar) =>
                    this.value.Replace(oldChar, newChar);

                /// <inheritdoc cref="string.Replace(string,string)" />
                public string Replace(string oldValue, string? newValue) =>
                    this.value.Replace(oldValue, newValue);

                /// <inheritdoc cref="string.ReplaceLineEndings()" />
                public string ReplaceLineEndings() =>
                    this.value.ReplaceLineEndings();

                /// <inheritdoc cref="string.ReplaceLineEndings(string)" />
                public string ReplaceLineEndings(string replacementText) =>
                    this.value.ReplaceLineEndings(replacementText);

                /// <inheritdoc cref="string.Split(char,global::System.StringSplitOptions)" />
                public string[] Split(char separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char,int,global::System.StringSplitOptions)" />
                public string[] Split(char separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(char[])" />
                public string[] Split(params char[]? separator) =>
                    this.value.Split(separator);

                /// <inheritdoc cref="string.Split(char[],int)" />
                public string[] Split(char[]? separator, int count) =>
                    this.value.Split(separator, count);

                /// <inheritdoc cref="string.Split(char[],global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char[],int,global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string,int,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string[],global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string[],int,global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Substring(int)" />
                public string Substring(int startIndex) =>
                    this.value.Substring(startIndex);

                /// <inheritdoc cref="string.Substring(int,int)" />
                public string Substring(int startIndex, int length) =>
                    this.value.Substring(startIndex, length);

                /// <inheritdoc cref="string.ToLower()" />
                public string ToLower() =>
                    this.value.ToLower();

                /// <inheritdoc cref="string.ToLower(global::System.Globalization.CultureInfo)" />
                public string ToLower(System.Globalization.CultureInfo? culture) =>
                    this.value.ToLower(culture);

                /// <inheritdoc cref="string.ToLowerInvariant()" />
                public string ToLowerInvariant() =>
                    this.value.ToLowerInvariant();

                /// <inheritdoc cref="string.ToUpper()" />
                public string ToUpper() =>
                    this.value.ToUpper();

                /// <inheritdoc cref="string.ToUpper(global::System.Globalization.CultureInfo)" />
                public string ToUpper(System.Globalization.CultureInfo? culture) =>
                    this.value.ToUpper(culture);

                /// <inheritdoc cref="string.ToUpperInvariant()" />
                public string ToUpperInvariant() =>
                    this.value.ToUpperInvariant();

                /// <inheritdoc cref="string.Trim()" />
                public string Trim() =>
                    this.value.Trim();

                /// <inheritdoc cref="string.Trim(char)" />
                public string Trim(char trimChar) =>
                    this.value.Trim(trimChar);

                /// <inheritdoc cref="string.Trim(char[])" />
                public string Trim(params char[]? trimChars) =>
                    this.value.Trim(trimChars);

                /// <inheritdoc cref="string.TrimStart()" />
                public string TrimStart() =>
                    this.value.TrimStart();

                /// <inheritdoc cref="string.TrimStart(char)" />
                public string TrimStart(char trimChar) =>
                    this.value.TrimStart(trimChar);

                /// <inheritdoc cref="string.TrimStart(char[])" />
                public string TrimStart(params char[]? trimChars) =>
                    this.value.TrimStart(trimChars);

                /// <inheritdoc cref="string.TrimEnd()" />
                public string TrimEnd() =>
                    this.value.TrimEnd();

                /// <inheritdoc cref="string.TrimEnd(char)" />
                public string TrimEnd(char trimChar) =>
                    this.value.TrimEnd(trimChar);

                /// <inheritdoc cref="string.TrimEnd(char[])" />
                public string TrimEnd(params char[]? trimChars) =>
                    this.value.TrimEnd(trimChars);

                /// <inheritdoc cref="string.Contains(string)" />
                public bool Contains(string value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(string,global::System.StringComparison)" />
                public bool Contains(string value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.Contains(char)" />
                public bool Contains(char value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(char,global::System.StringComparison)" />
                public bool Contains(char value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char)" />
                public int IndexOf(char value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(char,int)" />
                public int IndexOf(char value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(char,global::System.StringComparison)" />
                public int IndexOf(char value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char,int,int)" />
                public int IndexOf(char value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOfAny(char[])" />
                public int IndexOfAny(char[] anyOf) =>
                    this.value.IndexOfAny(anyOf);

                /// <inheritdoc cref="string.IndexOfAny(char[],int)" />
                public int IndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.IndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.IndexOfAny(char[],int,int)" />
                public int IndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.IndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string)" />
                public int IndexOf(string value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(string,int)" />
                public int IndexOf(string value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(string,int,int)" />
                public int IndexOf(string value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string,global::System.StringComparison)" />
                public int IndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(char)" />
                public int LastIndexOf(char value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(char,int)" />
                public int LastIndexOf(char value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(char,int,int)" />
                public int LastIndexOf(char value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOfAny(char[])" />
                public int LastIndexOfAny(char[] anyOf) =>
                    this.value.LastIndexOfAny(anyOf);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.LastIndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int,int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.LastIndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string)" />
                public int LastIndexOf(string value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(string,int)" />
                public int LastIndexOf(string value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int)" />
                public int LastIndexOf(string value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string,global::System.StringComparison)" />
                public int LastIndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.this[int]" />
                public char this[int index]
                {
                    get => this.value[index];
                }

                /// <inheritdoc cref="string.Length" />
                public int Length
                {
                    get => this.value.Length;
                }

            }
            """;

        // Language=C#
        var dotnet9Source = """
            namespace Tests;

            #nullable enable
            public partial struct TestTypedef : global::System.IEquatable<TestTypedef>
            {
                private readonly string value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                public TestTypedef(string value) => this.value = value;

                /// <summary>
                /// Initializes a new instance of the <see cref="TestTypedef"/> class.
                /// </summary>
                /// <param name="value">Value to alias using this typedef.</param>
                /// <returns>New instance of the <see cref="TestTypedef"/> class.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static TestTypedef? From(string? value) => value is null ? null : new TestTypedef(value);

                /// <summary>
                /// Gets the aliased type of this typedef.
                /// </summary>
                /// <param name="value">Value to cast into the aliased type.</param>
                /// <returns>The aliased type.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator string?(TestTypedef? value) => value?.value;

                /// <summary>
                /// Gets a typedef from the aliased type.
                /// </summary>
                /// <param name="value">Value to cast into the typedef.</param>
                /// <returns>The typedef.</returns>
                [return: global::System.Diagnostics.CodeAnalysis.NotNullIfNotNull(nameof(value))]
                public static explicit operator TestTypedef?(string? value) => value == null ? null : new TestTypedef(value);

                /// <inheritdoc />
                public bool Equals(TestTypedef other) => this.value.Equals(other.value);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are equivalent.</returns>
                public static bool operator==(TestTypedef left, TestTypedef right) => left.Equals(right);

                /// <summary>
                /// Compares two aliased types for equality.
                /// </summary>
                /// <param name="left">Left hand side of operator.</param>
                /// <param name="right">Right hand side of operator.</param>
                /// <returns>Bool indicating whether <paramref name="left"/> and <paramref name="right"/> are not equivalent.</returns>
                public static bool operator!=(TestTypedef left, TestTypedef right) => !left.Equals(right);

                /// <inheritdoc cref="string.CompareTo(object)" />
                public int CompareTo(object? value) =>
                    this.value.CompareTo(value);

                /// <inheritdoc cref="string.CompareTo(string)" />
                public int CompareTo(string? strB) =>
                    this.value.CompareTo(strB);

                /// <inheritdoc cref="string.EndsWith(string)" />
                public bool EndsWith(string value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.EndsWith(string,global::System.StringComparison)" />
                public bool EndsWith(string value, System.StringComparison comparisonType) =>
                    this.value.EndsWith(value, comparisonType);

                /// <inheritdoc cref="string.EndsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool EndsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.EndsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.EndsWith(char)" />
                public bool EndsWith(char value) =>
                    this.value.EndsWith(value);

                /// <inheritdoc cref="string.Equals(object)" />
                public override bool Equals(object? obj) =>
                    obj is global::Tests.TestTypedef other && this.Equals(other);

                /// <inheritdoc cref="string.Equals(string)" />
                public bool Equals(string? value) =>
                    this.value.Equals(value);

                /// <inheritdoc cref="string.Equals(string,global::System.StringComparison)" />
                public bool Equals(string? value, System.StringComparison comparisonType) =>
                    this.value.Equals(value, comparisonType);

                /// <inheritdoc cref="string.GetHashCode()" />
                public override int GetHashCode() =>
                    this.value.GetHashCode();

                /// <inheritdoc cref="string.GetHashCode(global::System.StringComparison)" />
                public int GetHashCode(System.StringComparison comparisonType) =>
                    this.value.GetHashCode(comparisonType);

                /// <inheritdoc cref="string.StartsWith(string)" />
                public bool StartsWith(string value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.StartsWith(string,global::System.StringComparison)" />
                public bool StartsWith(string value, System.StringComparison comparisonType) =>
                    this.value.StartsWith(value, comparisonType);

                /// <inheritdoc cref="string.StartsWith(string,bool,global::System.Globalization.CultureInfo)" />
                public bool StartsWith(string value, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.StartsWith(value, ignoreCase, culture);

                /// <inheritdoc cref="string.StartsWith(char)" />
                public bool StartsWith(char value) =>
                    this.value.StartsWith(value);

                /// <inheritdoc cref="string.Clone()" />
                public object Clone() =>
                    this.value.Clone();

                /// <inheritdoc cref="string.CopyTo(int,char[],int,int)" />
                public void CopyTo(int sourceIndex, char[] destination, int destinationIndex, int count) =>
                    this.value.CopyTo(sourceIndex, destination, destinationIndex, count);

                /// <inheritdoc cref="string.CopyTo(global::System.Span{char})" />
                public void CopyTo(System.Span<char> destination) =>
                    this.value.CopyTo(destination);

                /// <inheritdoc cref="string.TryCopyTo(global::System.Span{char})" />
                public bool TryCopyTo(System.Span<char> destination) =>
                    this.value.TryCopyTo(destination);

                /// <inheritdoc cref="string.ToCharArray()" />
                public char[] ToCharArray() =>
                    this.value.ToCharArray();

                /// <inheritdoc cref="string.ToCharArray(int,int)" />
                public char[] ToCharArray(int startIndex, int length) =>
                    this.value.ToCharArray(startIndex, length);

                /// <inheritdoc cref="string.GetPinnableReference()" />
                public char GetPinnableReference() =>
                    this.value.GetPinnableReference();

                /// <inheritdoc cref="string.ToString()" />
                public override string ToString() =>
                    this.value.ToString();

                /// <inheritdoc cref="string.ToString(global::System.IFormatProvider)" />
                public string ToString(System.IFormatProvider? provider) =>
                    this.value.ToString(provider);

                /// <inheritdoc cref="string.GetEnumerator()" />
                public global::System.CharEnumerator GetEnumerator() =>
                    this.value.GetEnumerator();

                /// <inheritdoc cref="string.EnumerateRunes()" />
                public global::System.Text.StringRuneEnumerator EnumerateRunes() =>
                    this.value.EnumerateRunes();

                /// <inheritdoc cref="string.GetTypeCode()" />
                public global::System.TypeCode GetTypeCode() =>
                    this.value.GetTypeCode();

                /// <inheritdoc cref="string.IsNormalized()" />
                public bool IsNormalized() =>
                    this.value.IsNormalized();

                /// <inheritdoc cref="string.IsNormalized(global::System.Text.NormalizationForm)" />
                public bool IsNormalized(System.Text.NormalizationForm normalizationForm) =>
                    this.value.IsNormalized(normalizationForm);

                /// <inheritdoc cref="string.Normalize()" />
                public string Normalize() =>
                    this.value.Normalize();

                /// <inheritdoc cref="string.Normalize(global::System.Text.NormalizationForm)" />
                public string Normalize(System.Text.NormalizationForm normalizationForm) =>
                    this.value.Normalize(normalizationForm);

                /// <inheritdoc cref="string.Insert(int,string)" />
                public string Insert(int startIndex, string value) =>
                    this.value.Insert(startIndex, value);

                /// <inheritdoc cref="string.PadLeft(int)" />
                public string PadLeft(int totalWidth) =>
                    this.value.PadLeft(totalWidth);

                /// <inheritdoc cref="string.PadLeft(int,char)" />
                public string PadLeft(int totalWidth, char paddingChar) =>
                    this.value.PadLeft(totalWidth, paddingChar);

                /// <inheritdoc cref="string.PadRight(int)" />
                public string PadRight(int totalWidth) =>
                    this.value.PadRight(totalWidth);

                /// <inheritdoc cref="string.PadRight(int,char)" />
                public string PadRight(int totalWidth, char paddingChar) =>
                    this.value.PadRight(totalWidth, paddingChar);

                /// <inheritdoc cref="string.Remove(int,int)" />
                public string Remove(int startIndex, int count) =>
                    this.value.Remove(startIndex, count);

                /// <inheritdoc cref="string.Remove(int)" />
                public string Remove(int startIndex) =>
                    this.value.Remove(startIndex);

                /// <inheritdoc cref="string.Replace(string,string,bool,global::System.Globalization.CultureInfo)" />
                public string Replace(string oldValue, string? newValue, bool ignoreCase, System.Globalization.CultureInfo? culture) =>
                    this.value.Replace(oldValue, newValue, ignoreCase, culture);

                /// <inheritdoc cref="string.Replace(string,string,global::System.StringComparison)" />
                public string Replace(string oldValue, string? newValue, System.StringComparison comparisonType) =>
                    this.value.Replace(oldValue, newValue, comparisonType);

                /// <inheritdoc cref="string.Replace(char,char)" />
                public string Replace(char oldChar, char newChar) =>
                    this.value.Replace(oldChar, newChar);

                /// <inheritdoc cref="string.Replace(string,string)" />
                public string Replace(string oldValue, string? newValue) =>
                    this.value.Replace(oldValue, newValue);

                /// <inheritdoc cref="string.ReplaceLineEndings()" />
                public string ReplaceLineEndings() =>
                    this.value.ReplaceLineEndings();

                /// <inheritdoc cref="string.ReplaceLineEndings(string)" />
                public string ReplaceLineEndings(string replacementText) =>
                    this.value.ReplaceLineEndings(replacementText);

                /// <inheritdoc cref="string.Split(char,global::System.StringSplitOptions)" />
                public string[] Split(char separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char,int,global::System.StringSplitOptions)" />
                public string[] Split(char separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(char[])" />
                public string[] Split(params char[]? separator) =>
                    this.value.Split(separator);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.Split(global::System.ReadOnlySpan{char})" />
                public string[] Split(params System.ReadOnlySpan<char> separator) =>
                    this.value.Split(separator);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.Split(char[],int)" />
                public string[] Split(char[]? separator, int count) =>
                    this.value.Split(separator, count);

                /// <inheritdoc cref="string.Split(char[],global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(char[],int,global::System.StringSplitOptions)" />
                public string[] Split(char[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string,int,global::System.StringSplitOptions)" />
                public string[] Split(string? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Split(string[],global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, System.StringSplitOptions options) =>
                    this.value.Split(separator, options);

                /// <inheritdoc cref="string.Split(string[],int,global::System.StringSplitOptions)" />
                public string[] Split(string[]? separator, int count, System.StringSplitOptions options) =>
                    this.value.Split(separator, count, options);

                /// <inheritdoc cref="string.Substring(int)" />
                public string Substring(int startIndex) =>
                    this.value.Substring(startIndex);

                /// <inheritdoc cref="string.Substring(int,int)" />
                public string Substring(int startIndex, int length) =>
                    this.value.Substring(startIndex, length);

                /// <inheritdoc cref="string.ToLower()" />
                public string ToLower() =>
                    this.value.ToLower();

                /// <inheritdoc cref="string.ToLower(global::System.Globalization.CultureInfo)" />
                public string ToLower(System.Globalization.CultureInfo? culture) =>
                    this.value.ToLower(culture);

                /// <inheritdoc cref="string.ToLowerInvariant()" />
                public string ToLowerInvariant() =>
                    this.value.ToLowerInvariant();

                /// <inheritdoc cref="string.ToUpper()" />
                public string ToUpper() =>
                    this.value.ToUpper();

                /// <inheritdoc cref="string.ToUpper(global::System.Globalization.CultureInfo)" />
                public string ToUpper(System.Globalization.CultureInfo? culture) =>
                    this.value.ToUpper(culture);

                /// <inheritdoc cref="string.ToUpperInvariant()" />
                public string ToUpperInvariant() =>
                    this.value.ToUpperInvariant();

                /// <inheritdoc cref="string.Trim()" />
                public string Trim() =>
                    this.value.Trim();

                /// <inheritdoc cref="string.Trim(char)" />
                public string Trim(char trimChar) =>
                    this.value.Trim(trimChar);

                /// <inheritdoc cref="string.Trim(char[])" />
                public string Trim(params char[]? trimChars) =>
                    this.value.Trim(trimChars);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.Trim(global::System.ReadOnlySpan{char})" />
                public string Trim(params System.ReadOnlySpan<char> trimChars) =>
                    this.value.Trim(trimChars);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.TrimStart()" />
                public string TrimStart() =>
                    this.value.TrimStart();

                /// <inheritdoc cref="string.TrimStart(char)" />
                public string TrimStart(char trimChar) =>
                    this.value.TrimStart(trimChar);

                /// <inheritdoc cref="string.TrimStart(char[])" />
                public string TrimStart(params char[]? trimChars) =>
                    this.value.TrimStart(trimChars);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.TrimStart(global::System.ReadOnlySpan{char})" />
                public string TrimStart(params System.ReadOnlySpan<char> trimChars) =>
                    this.value.TrimStart(trimChars);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.TrimEnd()" />
                public string TrimEnd() =>
                    this.value.TrimEnd();

                /// <inheritdoc cref="string.TrimEnd(char)" />
                public string TrimEnd(char trimChar) =>
                    this.value.TrimEnd(trimChar);

                /// <inheritdoc cref="string.TrimEnd(char[])" />
                public string TrimEnd(params char[]? trimChars) =>
                    this.value.TrimEnd(trimChars);

                #if NET9_0_OR_GREATER
                /// <inheritdoc cref="string.TrimEnd(global::System.ReadOnlySpan{char})" />
                public string TrimEnd(params System.ReadOnlySpan<char> trimChars) =>
                    this.value.TrimEnd(trimChars);
                #endif // NET9_0_OR_GREATER

                /// <inheritdoc cref="string.Contains(string)" />
                public bool Contains(string value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(string,global::System.StringComparison)" />
                public bool Contains(string value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.Contains(char)" />
                public bool Contains(char value) =>
                    this.value.Contains(value);

                /// <inheritdoc cref="string.Contains(char,global::System.StringComparison)" />
                public bool Contains(char value, System.StringComparison comparisonType) =>
                    this.value.Contains(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char)" />
                public int IndexOf(char value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(char,int)" />
                public int IndexOf(char value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(char,global::System.StringComparison)" />
                public int IndexOf(char value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(char,int,int)" />
                public int IndexOf(char value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOfAny(char[])" />
                public int IndexOfAny(char[] anyOf) =>
                    this.value.IndexOfAny(anyOf);

                /// <inheritdoc cref="string.IndexOfAny(char[],int)" />
                public int IndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.IndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.IndexOfAny(char[],int,int)" />
                public int IndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.IndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string)" />
                public int IndexOf(string value) =>
                    this.value.IndexOf(value);

                /// <inheritdoc cref="string.IndexOf(string,int)" />
                public int IndexOf(string value, int startIndex) =>
                    this.value.IndexOf(value, startIndex);

                /// <inheritdoc cref="string.IndexOf(string,int,int)" />
                public int IndexOf(string value, int startIndex, int count) =>
                    this.value.IndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.IndexOf(string,global::System.StringComparison)" />
                public int IndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.IndexOf(string,int,int,global::System.StringComparison)" />
                public int IndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.IndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(char)" />
                public int LastIndexOf(char value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(char,int)" />
                public int LastIndexOf(char value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(char,int,int)" />
                public int LastIndexOf(char value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOfAny(char[])" />
                public int LastIndexOfAny(char[] anyOf) =>
                    this.value.LastIndexOfAny(anyOf);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex) =>
                    this.value.LastIndexOfAny(anyOf, startIndex);

                /// <inheritdoc cref="string.LastIndexOfAny(char[],int,int)" />
                public int LastIndexOfAny(char[] anyOf, int startIndex, int count) =>
                    this.value.LastIndexOfAny(anyOf, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string)" />
                public int LastIndexOf(string value) =>
                    this.value.LastIndexOf(value);

                /// <inheritdoc cref="string.LastIndexOf(string,int)" />
                public int LastIndexOf(string value, int startIndex) =>
                    this.value.LastIndexOf(value, startIndex);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int)" />
                public int LastIndexOf(string value, int startIndex, int count) =>
                    this.value.LastIndexOf(value, startIndex, count);

                /// <inheritdoc cref="string.LastIndexOf(string,global::System.StringComparison)" />
                public int LastIndexOf(string value, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, comparisonType);

                /// <inheritdoc cref="string.LastIndexOf(string,int,int,global::System.StringComparison)" />
                public int LastIndexOf(string value, int startIndex, int count, System.StringComparison comparisonType) =>
                    this.value.LastIndexOf(value, startIndex, count, comparisonType);

                /// <inheritdoc cref="string.this[int]" />
                public char this[int index]
                {
                    get => this.value[index];
                }

                /// <inheritdoc cref="string.Length" />
                public int Length
                {
                    get => this.value.Length;
                }

            }
            """;
#pragma warning restore CS0219, S1481 // Variable is assigned but its value is never used

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Typedef/Tests.TestTypedef.g.cs",
#if NET9_0_OR_GREATER
                dotnet9Source
#else
                dotnet8Source
#endif // NET9_0_OR_GREATER

            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }
}
