// <copyright file="RouteGeneratorTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Pipelines;
using System.Text.Json;
using KestrelToolbox.Serialization;
using KestrelToolbox.Web;
using Microsoft.AspNetCore.Http;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

namespace UnitTests.GeneratorTests.RouteGenerator;

/// <summary>
/// Tests for <see cref="RouteAttribute"/>.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.LayoutRules",
    "SA1515:Single-line comment should be preceded by blank line",
    Justification = "Comments for language."
)]
public sealed class RouteGeneratorTests
{
    public RouteGeneratorTests()
    {
        // Use some of the types so the DLL loads.
        _ = new RouteAttribute("Hello");
        _ = new QueryString("?test=value");
        _ = new ReadOnlySequence<byte>(ReadOnlyMemory<byte>.Empty);
        _ = new JsonSerializableAttribute(typeof(int));
        _ = new Utf8JsonWriter(new MemoryStream());
        _ = new HeaderDictionary();
        _ = new Pipe();
    }

    [Fact]
    public void JsonBody()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization.DataAnnotations;
            using KestrelToolbox.Web;
            using Microsoft.AspNetCore.Http;
            using System.Threading.Tasks;

            public partial class JsonPayload
            {
                [Name("value")]
                public required int Value { get; set; }
            }

            public static partial class ServerRoutes
            {
                [Route("/test")]
                public static Task TestNoValue(HttpContext context, [JsonBody] JsonPayload payload)
                {
                    return Task.CompletedTask;
                }

                [Route("/test", methods: "PUT, POST")]
                public static Task TestNoValuePost(HttpContext context, [JsonBody] JsonPayload payload)
                {
                    return Task.CompletedTask;
                }
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/ServerRoutes.g.cs",
                // Language=C#
                """
                #nullable enable
                public static partial class ServerRoutes
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::JsonPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::JsonPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::JsonPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::JsonPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int member_0 = default!;
                            var required_0 = false;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'JsonPayload' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        required_0 = true;
                                        int output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_0))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("value");
                                }

                                error = $"A required property was missing on JsonPayload. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::JsonPayload()
                            {
                                Value = member_0,
                            };

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Routes/ServerRoutes.g.cs",
                // Language=C#
                """
                #nullable enable
                public static partial class ServerRoutes
                {
                    /// <summary>
                    /// Gets all routes that were tagged on this type using <see cref="global::KestrelToolbox.Web.RouteAttribute" />.
                    /// </summary>
                    /// <returns>
                    /// The collection of <see cref="global::KestrelToolbox.Web.RouteInfo "/> elements.
                    /// </returns>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static global::System.Collections.Generic.IEnumerable<global::KestrelToolbox.Web.RouteInfo> GetRoutes()
                    {
                        yield return new global::KestrelToolbox.Web.RouteInfo()
                        {
                            Pattern = "/test",
                            Methods = new[] {"GET"},
                            Callback = method_0,
                        };

                        static async global::System.Threading.Tasks.Task method_0(global::Microsoft.AspNetCore.Http.HttpContext context)
                        {
                            var request = context.Request;
                            var response = context.Response;
                            if (!global::KestrelToolbox.Internal.WebHelpers.IsAcceptableContentType(request.Headers.ContentType, "application/json"))
                            {
                                response.StatusCode = 415;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, "Unsupported content type.");
                                return;
                            }

                            var routeValues = request.RouteValues;
                            var readResult = await global::KestrelToolbox.Extensions.PipeReaderExtensions.PipeReaderExtensions.ReadToEndAsync(request.BodyReader);
                            if (!TryParseJson(readResult.Buffer, out global::JsonPayload? param0, out var jsonErrors))
                            {
                                request.BodyReader.AdvanceTo(readResult.Buffer.End);
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, jsonErrors);
                                return;
                            }

                            request.BodyReader.AdvanceTo(readResult.Buffer.End);
                            await TestNoValue(context, param0);
                        }

                        yield return new global::KestrelToolbox.Web.RouteInfo()
                        {
                            Pattern = "/test",
                            Methods = new[] {"PUT", "POST"},
                            Callback = method_1,
                        };

                        static async global::System.Threading.Tasks.Task method_1(global::Microsoft.AspNetCore.Http.HttpContext context)
                        {
                            var request = context.Request;
                            var response = context.Response;
                            if (!global::KestrelToolbox.Internal.WebHelpers.IsAcceptableContentType(request.Headers.ContentType, "application/json"))
                            {
                                response.StatusCode = 415;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, "Unsupported content type.");
                                return;
                            }

                            var routeValues = request.RouteValues;
                            var readResult = await global::KestrelToolbox.Extensions.PipeReaderExtensions.PipeReaderExtensions.ReadToEndAsync(request.BodyReader);
                            if (!TryParseJson(readResult.Buffer, out global::JsonPayload? param0, out var jsonErrors))
                            {
                                request.BodyReader.AdvanceTo(readResult.Buffer.End);
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, jsonErrors);
                                return;
                            }

                            request.BodyReader.AdvanceTo(readResult.Buffer.End);
                            await TestNoValuePost(context, param0);
                        }
                    }

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        _ = Verifier.LoadAssembly(compilation);
    }

    [Fact]
    public void QueryParameters()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;
            using KestrelToolbox.Web;
            using Microsoft.AspNetCore.Http;
            using System.Threading.Tasks;

            [QueryStringSerializable]
            public partial class QueryPayload
            {
                [Name("value")]
                public required int Value { get; set; }
            }

            public static partial class ServerRoutes
            {
                [Route("/test")]
                public static Task TestNoValue(HttpContext context, [JsonBody] QueryPayload json, [QueryParameters] QueryPayload payload)
                {
                    return Task.CompletedTask;
                }

                [Route("/test", methods: "PUT, POST")]
                public static Task TestNoValuePost(HttpContext context, [QueryParameters] QueryPayload payload)
                {
                    return Task.CompletedTask;
                }
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/QueryPayload.g.cs",
                // Language=C#
                """
                #nullable enable
                public partial class QueryPayload
                {
                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="queryCollection">Query string collection.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseQueryString(global::Microsoft.AspNetCore.Http.IQueryCollection queryCollection, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::QueryPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        int member_0;
                        if (queryCollection.TryGetValue("value", out var inputValues_0))
                        {
                            if (inputValues_0.Count == 0)
                            {
                                error = "'value' must contain values.";
                                result = default;
                                return false;
                            }

                            if (inputValues_0.Count != 1)
                            {
                                error = "'value' cannot have multiple values.";
                                result = default;
                                return false;
                            }

                            var token_0 = inputValues_0[0] ?? string.Empty;
                            if (!int.TryParse(token_0, out var output_0))
                            {
                                error = "'value' was not of type int32.";
                                result = default;
                                return false;
                            }

                            member_0 = output_0;
                        }
                        else
                        {
                            error = "Required query parameter 'value' was missing.";
                            result = default;
                            return false;
                        }

                        result = new global::QueryPayload()
                        {
                            Value = member_0,
                        };

                        error = default;
                        return true;
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/ServerRoutes.g.cs",
                // Language=C#
                """
                #nullable enable
                public static partial class ServerRoutes
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::QueryPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::QueryPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::QueryPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    private static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::QueryPayload? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int member_0 = default!;
                            var required_0 = false;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'QueryPayload' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        required_0 = true;
                                        int output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_0))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("value");
                                }

                                error = $"A required property was missing on QueryPayload. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::QueryPayload()
                            {
                                Value = member_0,
                            };

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Routes/ServerRoutes.g.cs",
                // Language=C#
                """
                #nullable enable
                public static partial class ServerRoutes
                {
                    /// <summary>
                    /// Gets all routes that were tagged on this type using <see cref="global::KestrelToolbox.Web.RouteAttribute" />.
                    /// </summary>
                    /// <returns>
                    /// The collection of <see cref="global::KestrelToolbox.Web.RouteInfo "/> elements.
                    /// </returns>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static global::System.Collections.Generic.IEnumerable<global::KestrelToolbox.Web.RouteInfo> GetRoutes()
                    {
                        yield return new global::KestrelToolbox.Web.RouteInfo()
                        {
                            Pattern = "/test",
                            Methods = new[] {"GET"},
                            Callback = method_0,
                        };

                        static async global::System.Threading.Tasks.Task method_0(global::Microsoft.AspNetCore.Http.HttpContext context)
                        {
                            var request = context.Request;
                            var response = context.Response;
                            if (!global::KestrelToolbox.Internal.WebHelpers.IsAcceptableContentType(request.Headers.ContentType, "application/json"))
                            {
                                response.StatusCode = 415;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, "Unsupported content type.");
                                return;
                            }

                            var routeValues = request.RouteValues;
                            var readResult = await global::KestrelToolbox.Extensions.PipeReaderExtensions.PipeReaderExtensions.ReadToEndAsync(request.BodyReader);
                            if (!TryParseJson(readResult.Buffer, out global::QueryPayload? param0, out var jsonErrors))
                            {
                                request.BodyReader.AdvanceTo(readResult.Buffer.End);
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, jsonErrors);
                                return;
                            }

                            request.BodyReader.AdvanceTo(readResult.Buffer.End);
                            if (!global::QueryPayload.TryParseQueryString(context.Request.Query, out global::QueryPayload? param1, out var queryErrors))
                            {
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, queryErrors);
                                return;
                            }

                            await TestNoValue(context, param0, param1);
                        }

                        yield return new global::KestrelToolbox.Web.RouteInfo()
                        {
                            Pattern = "/test",
                            Methods = new[] {"PUT", "POST"},
                            Callback = method_1,
                        };

                        static async global::System.Threading.Tasks.Task method_1(global::Microsoft.AspNetCore.Http.HttpContext context)
                        {
                            var request = context.Request;
                            var response = context.Response;
                            var routeValues = request.RouteValues;
                            if (!global::QueryPayload.TryParseQueryString(context.Request.Query, out global::QueryPayload? param0, out var queryErrors))
                            {
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, queryErrors);
                                return;
                            }

                            await TestNoValuePost(context, param0);
                        }
                    }

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        _ = Verifier.LoadAssembly(compilation);
    }

    [Fact]
    public void RoutePathTypes()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Types;
            using KestrelToolbox.Web;
            using Microsoft.AspNetCore.Http;
            using System;
            using System.Threading.Tasks;

            public static partial class ServerRoutes
            {
                [Route("/test/{guid:guid}/{uuid:guid}")]
                public static Task TestNoValue(HttpContext context, Guid guid, UUIDv1 uuid)
                {
                    return Task.CompletedTask;
                }
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/Routes/ServerRoutes.g.cs",
                // Language=C#
                """
                #nullable enable
                public static partial class ServerRoutes
                {
                    /// <summary>
                    /// Gets all routes that were tagged on this type using <see cref="global::KestrelToolbox.Web.RouteAttribute" />.
                    /// </summary>
                    /// <returns>
                    /// The collection of <see cref="global::KestrelToolbox.Web.RouteInfo "/> elements.
                    /// </returns>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static global::System.Collections.Generic.IEnumerable<global::KestrelToolbox.Web.RouteInfo> GetRoutes()
                    {
                        yield return new global::KestrelToolbox.Web.RouteInfo()
                        {
                            Pattern = "/test/{guid:guid}/{uuid:guid}",
                            Methods = new[] {"GET"},
                            Callback = method_0,
                        };

                        static async global::System.Threading.Tasks.Task method_0(global::Microsoft.AspNetCore.Http.HttpContext context)
                        {
                            var request = context.Request;
                            var response = context.Response;
                            var routeValues = request.RouteValues;
                            if (!global::System.Guid.TryParse((string?)routeValues["guid"] ?? string.Empty, out var param0))
                            {
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, "guid was not a valid Guid.");
                                return;
                            }

                            if (!global::KestrelToolbox.Types.UUIDv1.TryParse((string?)routeValues["uuid"] ?? string.Empty, out var param1))
                            {
                                response.StatusCode = 400;
                                await global::Microsoft.AspNetCore.Http.HttpResponseWritingExtensions.WriteAsync(response, "uuid was not a valid UUIDv1.");
                                return;
                            }

                            await TestNoValue(context, param0, param1);
                        }
                    }

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        _ = Verifier.LoadAssembly(compilation);
    }
}
