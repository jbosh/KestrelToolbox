// <copyright file="JsonSerializableGeneratorTests.CrossClassCompilation.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

#pragma warning disable SA1515 // Single line comments for `Language=C#`
namespace UnitTests.GeneratorTests.JsonSerializableGenerator;

public partial class JsonSerializableGeneratorTests
{
    [Fact]
    public void CrossClassCompilation()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;
            using System.Text.Json;
            using System.Diagnostics.CodeAnalysis;

            namespace Tests;
            #nullable enable

            [JsonSerializable(typeof(TestClass))]
            public partial class TestClass
            {
                [Name("value")]
                public required TestSubClass Value { get; set; }

                [Name("default")]
                public required TestDefaultSubClass DefaultValue { get; set; }

                [Name("nullable")]
                public TestDefaultSubClass? NullableValue { get; set; }
            }

            [JsonSerializable(
                CustomParsingMethodName = "TryParse",
                CustomSerializingMethodName = "Serialize"
            )]
            public partial class TestSubClass
            {
                public required int Value { get; set; }

                public static bool TryParse(
                    ref Utf8JsonReader reader,
                    [NotNullWhen(true)] out TestSubClass? value,
                    [NotNullWhen(false)] out string? error
                )
                {
                    if (reader.TokenType != JsonTokenType.Number)
                    {
                        value = null;
                        error = $"Invalid token type for {nameof(TestSubClass)}";
                        return false;
                    }

                    var readValue = reader.GetInt32();
                    value = new TestSubClass { Value = readValue };
                    error = null;
                    return true;
                }

                public static void Serialize(Utf8JsonWriter writer, TestSubClass value)
                {
                    writer.WriteNumberValue(value.Value);
                }
            }

            [JsonSerializable]
            public partial class TestDefaultSubClass
            {
                [Name("value")]
                public required int Value { get; set; }
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestClass.g.cs",
                // language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::Tests.TestSubClass member_0 = default!;
                            var required_0 = false;
                            global::Tests.TestDefaultSubClass member_1 = default!;
                            var required_1 = false;
                            var member_2 = default(global::KestrelToolbox.Internal.OptionalProperty<global::Tests.TestDefaultSubClass?>);
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.TestClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        required_0 = true;
                                        global::Tests.TestSubClass output_0;
                                        if (!global::Tests.TestSubClass.TryParse(ref reader, out global::Tests.TestSubClass? input_0, out error))
                                        {
                                            error = $"value > {error}";
                                            result = default;
                                            return false;
                                        }

                                        output_0 = input_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    case "default":
                                    {
                                        required_1 = true;
                                        global::Tests.TestDefaultSubClass output_1;
                                        if (!global::Tests.TestDefaultSubClass.TryParseJson(ref reader, out global::Tests.TestDefaultSubClass? input_1, out error))
                                        {
                                            error = $"default > {error}";
                                            result = default;
                                            return false;
                                        }

                                        output_1 = input_1;
                                        member_1 = output_1;
                                        break;
                                    }

                                    case "nullable":
                                    {
                                        global::Tests.TestDefaultSubClass? output_2;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_2 = null;
                                        }
                                        else
                                        {
                                            if (!global::Tests.TestDefaultSubClass.TryParseJson(ref reader, out global::Tests.TestDefaultSubClass? input_2, out error))
                                            {
                                                error = $"nullable > {error}";
                                                result = default;
                                                return false;
                                            }

                                            output_2 = input_2;
                                        }

                                        member_2 = output_2;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            var_0 &= required_1;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("value");
                                }

                                if (!required_1)
                                {
                                    var_1.Add("default");
                                }

                                error = $"A required property was missing on Tests.TestClass. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::Tests.TestClass()
                            {
                                Value = member_0,
                                DefaultValue = member_1,
                            };
                            if (member_2.HasValue)
                            {
                                result.NullableValue = (global::Tests.TestDefaultSubClass?)member_2;
                            }

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestClass value)
                    {
                        writer.WriteStartObject();
                        var member_3 = value.Value;
                        writer.WritePropertyName("value");
                        global::Tests.TestSubClass.Serialize(writer, member_3);
                        var member_4 = value.DefaultValue;
                        writer.WritePropertyName("default");
                        global::Tests.TestDefaultSubClass.SerializeJson(writer, member_4);
                        var member_5 = value.NullableValue;
                        if (member_5 != null)
                        {
                            writer.WritePropertyName("nullable");
                            global::Tests.TestDefaultSubClass.SerializeJson(writer, member_5);
                        }
                        else
                        {
                            writer.WriteNull("nullable");
                        }

                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestSubClass.g.cs",
                // language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestSubClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            return global::Tests.TestSubClass.TryParse(ref reader, out result, out error);
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestSubClass value)
                    {
                        Serialize(writer, value);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }
                """
            ),
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestDefaultSubClass.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestDefaultSubClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestDefaultSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestDefaultSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestDefaultSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestDefaultSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int member_0 = default!;
                            var required_0 = false;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.TestDefaultSubClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        required_0 = true;
                                        int output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_0))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("value");
                                }

                                error = $"A required property was missing on Tests.TestDefaultSubClass. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::Tests.TestDefaultSubClass()
                            {
                                Value = member_0,
                            };

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestDefaultSubClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestDefaultSubClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestDefaultSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestDefaultSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestDefaultSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestDefaultSubClass value)
                    {
                        writer.WriteStartObject();
                        var member_1 = value.Value;
                        writer.WriteNumber("value", member_1);
                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        var assembly = Verifier.LoadAssembly(compilation);
        var testClassType = assembly.GetType("Tests.TestClass")!;
        var method = testClassType.GetMethod(
            "TryParseJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), testClassType.MakeByRefType(), typeof(string).MakeByRefType() }
        )!;
        var parameters = new object?[] { """{ "value": 33, "default": { "value": 14 } }""", null, null };
        var invokeResult = method.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        dynamic value = parameters[1]!;
        Assert.Equal(33, value.Value.Value);
        Assert.Equal(14, value.DefaultValue.Value);
    }
}
