// <copyright file="JsonSerializableGeneratorTests.NullableBytes.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

#pragma warning disable SA1515 // Single line comments for `Language=C#`
namespace UnitTests.GeneratorTests.JsonSerializableGenerator;

public partial class JsonSerializableGeneratorTests
{
    [Fact]
    public void NullableBytes()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;

            namespace Tests;
            #nullable enable

            [JsonSerializable]
            public partial class TestClass
            {
                [Name("required")]
                public required byte[] Required { get; set; }

                [Name("nullable")]
                public byte[]? Nullable { get; set; }
            }

            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestClass.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            byte[] member_0 = default!;
                            var required_0 = false;
                            var member_1 = default(global::KestrelToolbox.Internal.OptionalProperty<byte[]?>);
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.TestClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "required":
                                    {
                                        required_0 = true;
                                        byte[] output_0;
                                        if (!reader.TryGetBytesFromBase64(out var input_0))
                                        {
                                            error = "'required' was not of type base64 encoded string.";
                                            result = default;
                                            return false;
                                        }

                                        output_0 = input_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    case "nullable":
                                    {
                                        byte[]? output_1;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_1 = null;
                                        }
                                        else
                                        {
                                            if (!reader.TryGetBytesFromBase64(out var input_1))
                                            {
                                                error = "'nullable' was not of type base64 encoded string.";
                                                result = default;
                                                return false;
                                            }

                                            output_1 = input_1;
                                        }

                                        member_1 = output_1;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("required");
                                }

                                error = $"A required property was missing on Tests.TestClass. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::Tests.TestClass()
                            {
                                Required = member_0,
                            };
                            if (member_1.HasValue)
                            {
                                result.Nullable = (byte[]?)member_1;
                            }

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestClass value)
                    {
                        writer.WriteStartObject();
                        var member_2 = value.Required;
                        writer.WriteBase64String("required", member_2);
                        var member_3 = value.Nullable;
                        if (member_3 != null)
                        {
                            writer.WriteBase64String("nullable", member_3);
                        }
                        else
                        {
                            writer.WriteNull("nullable");
                        }

                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out byte[]? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out byte[]? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out byte[]? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out byte[]? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            byte[] output_0;
                            if (!reader.TryGetBytesFromBase64(out var input_0))
                            {
                                error = "Was not of type base64 encoded string.";
                                result = default;
                                return false;
                            }

                            output_0 = input_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, byte[] value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, byte[] value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(byte[] value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, byte[] value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, byte[] value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, byte[] value)
                    {
                        writer.WriteBase64StringValue(value);
                    }

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        var assembly = Verifier.LoadAssembly(compilation);
        var testClassType = assembly.GetType("Tests.TestClass")!;
        var method = testClassType.GetMethod(
            "TryParseJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), testClassType.MakeByRefType(), typeof(string).MakeByRefType() }
        )!;
        var parameters = new object?[] { """{ "required": "aGVsbG8=", "nullable": "aGVsbG8=" }""", null, null };
        var invokeResult = method.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        dynamic value = parameters[1]!;
        Assert.Equal("hello"u8.ToArray(), value.Required);
        Assert.Equal("hello"u8.ToArray(), value.Nullable);

        parameters = new object?[] { """{ "required": "aGVsbG8=", "nullable": null }""", null, null };
        invokeResult = method.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        value = parameters[1]!;
        Assert.Equal("hello"u8.ToArray(), value.Required);
        Assert.Null(value.Nullable);
    }
}
