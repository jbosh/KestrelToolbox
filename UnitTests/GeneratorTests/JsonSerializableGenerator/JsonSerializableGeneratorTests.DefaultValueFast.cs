// <copyright file="JsonSerializableGeneratorTests.DefaultValueFast.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

#pragma warning disable SA1515 // Single line comments for `Language=C#`
namespace UnitTests.GeneratorTests.JsonSerializableGenerator;

public partial class JsonSerializableGeneratorTests
{
    [Fact]
    public void DefaultValueFast()
    {
        // language=c#
        var code = """
            using System;
            using System.Collections.Generic;
            using System.Collections.Frozen;
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;

            namespace Tests;
            #nullable enable

            [JsonSerializable]
            [JsonSerializableSettingsAttribute(FastParsingAllPropertiesAreDefault = true)]
            public partial class TestClass
            {
                private const string InitValue = "init";

                [Name("string")]
                public string StringValue { get; set; } = "DefaultValue";

                [Name("int")]
                public int IntValue { get; set; } = 47;

                [Name("dictionary")]
                public Dictionary<string, string> DictionaryValue { get; set; } = new(capacity: 15);

                [Name("set")]
                public FrozenSet<string> HashSetValue { get; set; } = new HashSet<string>()
                {
                    "hello",
                    "world",
                    InitValue,
                }.ToFrozenSet();

                [Name("subclass")]
                public TestSubClass TestSubClassValue { get; set; } = new TestSubClass(14);

                [Name("frozen")]
                public FrozenDictionary<string, string> FrozenValue { get; set; } = FrozenDictionary<string, string>.Empty;

                [Name("array")]
                public int[] ArrayValue { get; set; } = new int[3] { 1, 2, 3 };

                [Name("list")]
                public List<int> ListValue { get; set; } = new List<int>(2) { 1, 2 };

                [Name("string2")]
                public string? StringValue2 { get; set; } = default(string);

                [Name("timespan")]
                public TimeSpan TimeSpanValue { get; set; } = TimeSpan.FromMinutes(1);
            }

            public class TestSubClass
            {
                public TestSubClass() { }

                public TestSubClass(int value)
                {
                    this.Value = value;
                }

                [Name("value")]
                public int Value { get; set; } = 47;
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestClass.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            string member_0 = default!;
                            int member_1 = default!;
                            global::System.Collections.Generic.Dictionary<string, string> member_2 = default!;
                            global::System.Collections.Frozen.FrozenSet<string> member_3 = default!;
                            global::Tests.TestSubClass member_4 = default!;
                            global::System.Collections.Frozen.FrozenDictionary<string, string> member_5 = default!;
                            int[] member_6 = default!;
                            global::System.Collections.Generic.List<int> member_7 = default!;
                            string? member_8 = default!;
                            global::System.TimeSpan member_9 = default!;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.TestClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "string":
                                    {
                                        string output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'string' was null. Should be of type string.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'string' was not of type string.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        var token_0 = reader.GetString()!;
                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    case "int":
                                    {
                                        int output_1;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'int' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'int' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_1))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'int' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'int' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_1 = token_1;
                                        member_1 = output_1;
                                        break;
                                    }

                                    case "dictionary":
                                    {
                                        global::System.Collections.Generic.Dictionary<string, string> output_2;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                                        {
                                            error = "'dictionary' was not a dictionary.";
                                            result = default;
                                            return false;
                                        }

                                        var input_0 = new global::System.Collections.Generic.Dictionary<string, string>();
                                        while (reader.Read())
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                            {
                                                break;
                                            }

                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.PropertyName)
                                            {
                                                error = "'dictionary' has invalid dictionary. Expected property name.";
                                                result = default;
                                                return false;
                                            }

                                            var key_0 = reader.GetString()!;
                                            if (!reader.Read())
                                            {
                                                error = "'dictionary' did not have valid value.";
                                                result = default;
                                                return false;
                                            }

                                            string output_3;
                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'dictionary' was null. Should be of type string.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'dictionary' was not of type string.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            var token_2 = reader.GetString()!;
                                            output_3 = token_2;
                                            if (!input_0.TryAdd(key_0, output_3))
                                            {
                                                error = "'dictionary' has duplicate key.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_2 = input_0;
                                        member_2 = output_2;
                                        break;
                                    }

                                    case "set":
                                    {
                                        global::System.Collections.Frozen.FrozenSet<string> output_4;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartArray)
                                        {
                                            error = "'set' was not an array.";
                                            result = default;
                                            return false;
                                        }

                                        var input_1 = new global::System.Collections.Generic.HashSet<string>();
                                        while (reader.Read())
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndArray)
                                            {
                                                break;
                                            }

                                            string output_5;
                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'set' was null. Should be of type string.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'set' was not of type string.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            var token_3 = reader.GetString()!;
                                            output_5 = token_3;
                                            input_1.Add(output_5);
                                        }

                                        output_4 = global::System.Collections.Frozen.FrozenSet.ToFrozenSet(input_1);
                                        member_3 = output_4;
                                        break;
                                    }

                                    case "subclass":
                                    {
                                        global::Tests.TestSubClass output_6;
                                        if (!__KestrelToolbox_Serializable__File_global__Tests_TestClass.TryParseJson(ref reader, out global::Tests.TestSubClass? input_2, out error))
                                        {
                                            error = $"subclass > {error}";
                                            result = default;
                                            return false;
                                        }

                                        output_6 = input_2;
                                        member_4 = output_6;
                                        break;
                                    }

                                    case "frozen":
                                    {
                                        global::System.Collections.Frozen.FrozenDictionary<string, string> output_7;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                                        {
                                            error = "'frozen' was not a dictionary.";
                                            result = default;
                                            return false;
                                        }

                                        var input_3 = new global::System.Collections.Generic.Dictionary<string, string>();
                                        while (reader.Read())
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                            {
                                                break;
                                            }

                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.PropertyName)
                                            {
                                                error = "'frozen' has invalid dictionary. Expected property name.";
                                                result = default;
                                                return false;
                                            }

                                            var key_1 = reader.GetString()!;
                                            if (!reader.Read())
                                            {
                                                error = "'frozen' did not have valid value.";
                                                result = default;
                                                return false;
                                            }

                                            string output_8;
                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'frozen' was null. Should be of type string.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'frozen' was not of type string.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            var token_4 = reader.GetString()!;
                                            output_8 = token_4;
                                            if (!input_3.TryAdd(key_1, output_8))
                                            {
                                                error = "'frozen' has duplicate key.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_7 = global::System.Collections.Frozen.FrozenDictionary.ToFrozenDictionary(input_3);
                                        member_5 = output_7;
                                        break;
                                    }

                                    case "array":
                                    {
                                        int[] output_9;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartArray)
                                        {
                                            error = "'array' was not an array.";
                                            result = default;
                                            return false;
                                        }

                                        var input_4 = new global::System.Collections.Generic.List<int>();
                                        while (reader.Read())
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndArray)
                                            {
                                                break;
                                            }

                                            int output_10;
                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'array' was null. Should be of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'array' was not of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            if (!reader.TryGetInt32(out var token_5))
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'array' was null. Should be of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'array' was not of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            output_10 = token_5;
                                            input_4.Add(output_10);
                                        }

                                        output_9 = input_4.ToArray();
                                        member_6 = output_9;
                                        break;
                                    }

                                    case "list":
                                    {
                                        global::System.Collections.Generic.List<int> output_11;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartArray)
                                        {
                                            error = "'list' was not an array.";
                                            result = default;
                                            return false;
                                        }

                                        var input_5 = new global::System.Collections.Generic.List<int>();
                                        while (reader.Read())
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndArray)
                                            {
                                                break;
                                            }

                                            int output_12;
                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'list' was null. Should be of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'list' was not of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            if (!reader.TryGetInt32(out var token_6))
                                            {
                                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                                {
                                                    error = "'list' was null. Should be of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                                else
                                                {
                                                    error = "'list' was not of type int32.";
                                                    result = default;
                                                    return false;
                                                }
                                            }

                                            output_12 = token_6;
                                            input_5.Add(output_12);
                                        }

                                        output_11 = input_5;
                                        member_7 = output_11;
                                        break;
                                    }

                                    case "string2":
                                    {
                                        string? output_13;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                        {
                                            output_13 = null;
                                        }
                                        else
                                        {
                                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                            {
                                                error = "'string2' was not of type string.";
                                                result = default;
                                                return false;
                                            }

                                            var token_7 = reader.GetString()!;
                                            output_13 = token_7;
                                        }

                                        member_8 = output_13;
                                        break;
                                    }

                                    case "timespan":
                                    {
                                        global::System.TimeSpan output_14;
                                        if (reader.TokenType == global::System.Text.Json.JsonTokenType.String)
                                        {
                                            var token_8 = reader.GetString()!;
                                            if (!global::KestrelToolbox.Serialization.TimeSpanParser.TryParse(token_8, out var token_9))
                                            {
                                                error = "'timespan' was not of type TimeSpan.";
                                                result = default;
                                                return false;
                                            }

                                            output_14 = token_9;
                                        }

                                        else if (reader.TokenType == global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            var token_10 = reader.GetDouble();
                                            var token_11 = global::System.TimeSpan.FromSeconds(token_10);
                                            output_14 = token_11;
                                        }

                                        else
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'timespan' was null. Should be of type string.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'timespan' was not of type string.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        member_9 = output_14;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            result = new global::Tests.TestClass()
                            {
                                StringValue = member_0,
                                IntValue = member_1,
                                DictionaryValue = member_2,
                                HashSetValue = member_3,
                                TestSubClassValue = member_4,
                                FrozenValue = member_5,
                                ArrayValue = member_6,
                                ListValue = member_7,
                                StringValue2 = member_8,
                                TimeSpanValue = member_9,
                            };

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestClass value)
                    {
                        writer.WriteStartObject();
                        var member_10 = value.StringValue;
                        writer.WriteString("string", member_10);
                        var member_11 = value.IntValue;
                        writer.WriteNumber("int", member_11);
                        var member_12 = value.DictionaryValue;
                        writer.WriteStartObject("dictionary");
                        foreach (var token_12 in member_12)
                        {
                            writer.WritePropertyName(token_12.Key);
                            var token_13 = token_12.Value;
                            writer.WriteStringValue(token_13);
                        }

                        writer.WriteEndObject();
                        var member_13 = value.HashSetValue;
                        writer.WriteStartArray("set");
                        foreach (var token_14 in member_13)
                        {
                            writer.WriteStringValue(token_14);
                        }

                        writer.WriteEndArray();
                        var member_14 = value.TestSubClassValue;
                        writer.WritePropertyName("subclass");
                        __KestrelToolbox_Serializable__File_global__Tests_TestClass.SerializeJson(writer, member_14);
                        var member_15 = value.FrozenValue;
                        writer.WriteStartObject("frozen");
                        foreach (var token_15 in member_15)
                        {
                            writer.WritePropertyName(token_15.Key);
                            var token_16 = token_15.Value;
                            writer.WriteStringValue(token_16);
                        }

                        writer.WriteEndObject();
                        var member_16 = value.ArrayValue;
                        writer.WriteStartArray("array");
                        foreach (var token_17 in member_16)
                        {
                            writer.WriteNumberValue(token_17);
                        }

                        writer.WriteEndArray();
                        var member_17 = value.ListValue;
                        writer.WriteStartArray("list");
                        foreach (var token_18 in member_17)
                        {
                            writer.WriteNumberValue(token_18);
                        }

                        writer.WriteEndArray();
                        var member_18 = value.StringValue2;
                        writer.WriteString("string2", member_18);
                        var member_19 = value.TimeSpanValue;
                        writer.WriteString("timespan", member_19.ToString());
                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Tests_TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.Dictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.Dictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.Dictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Generic.Dictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::System.Collections.Generic.Dictionary<string, string> output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "Was not a dictionary.";
                                result = default;
                                return false;
                            }

                            var input_0 = new global::System.Collections.Generic.Dictionary<string, string>();
                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    break;
                                }

                                if (reader.TokenType != global::System.Text.Json.JsonTokenType.PropertyName)
                                {
                                    error = "Has invalid dictionary. Expected property name.";
                                    result = default;
                                    return false;
                                }

                                var key_0 = reader.GetString()!;
                                if (!reader.Read())
                                {
                                    error = "Did not have valid value.";
                                    result = default;
                                    return false;
                                }

                                string output_1;
                                if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                {
                                    if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                    {
                                        error = "Was null. Should be of type string.";
                                        result = default;
                                        return false;
                                    }
                                    else
                                    {
                                        error = "Was not of type string.";
                                        result = default;
                                        return false;
                                    }
                                }

                                var token_0 = reader.GetString()!;
                                output_1 = token_0;
                                if (!input_0.TryAdd(key_0, output_1))
                                {
                                    error = "Has duplicate key.";
                                    result = default;
                                    return false;
                                }
                            }

                            output_0 = input_0;
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Collections.Generic.Dictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Generic.Dictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Collections.Generic.Dictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Collections.Generic.Dictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Generic.Dictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Collections.Generic.Dictionary<string, string> value)
                    {
                        writer.WriteStartObject();
                        foreach (var token_1 in value)
                        {
                            writer.WritePropertyName(token_1.Key);
                            var token_2 = token_1.Value;
                            writer.WriteStringValue(token_2);
                        }

                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Collections.Generic.IDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Generic.IDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Collections.Generic.IDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Collections.Generic.IDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Generic.IDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Collections.Generic.IDictionary<string, string> value)
                    {
                        writer.WriteStartObject();
                        foreach (var token_0 in value)
                        {
                            writer.WritePropertyName(token_0.Key);
                            var token_1 = token_0.Value;
                            writer.WriteStringValue(token_1);
                        }

                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Tests.TestSubClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            var member_0 = default(global::KestrelToolbox.Internal.OptionalProperty<int>);
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Tests.TestSubClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        int output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_0))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            result = new global::Tests.TestSubClass()
                            {
                            };
                            if (member_0.HasValue)
                            {
                                result.Value = (int)member_0;
                            }

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Tests.TestSubClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Tests.TestSubClass value)
                    {
                        writer.WriteStartObject();
                        var member_1 = value.Value;
                        writer.WriteNumber("value", member_1);
                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Frozen.FrozenDictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Frozen.FrozenDictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Frozen.FrozenDictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::System.Collections.Frozen.FrozenDictionary<string, string>? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::System.Collections.Frozen.FrozenDictionary<string, string> output_0;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "Was not a dictionary.";
                                result = default;
                                return false;
                            }

                            var input_0 = new global::System.Collections.Generic.Dictionary<string, string>();
                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    break;
                                }

                                if (reader.TokenType != global::System.Text.Json.JsonTokenType.PropertyName)
                                {
                                    error = "Has invalid dictionary. Expected property name.";
                                    result = default;
                                    return false;
                                }

                                var key_0 = reader.GetString()!;
                                if (!reader.Read())
                                {
                                    error = "Did not have valid value.";
                                    result = default;
                                    return false;
                                }

                                string output_1;
                                if (reader.TokenType != global::System.Text.Json.JsonTokenType.String)
                                {
                                    if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                    {
                                        error = "Was null. Should be of type string.";
                                        result = default;
                                        return false;
                                    }
                                    else
                                    {
                                        error = "Was not of type string.";
                                        result = default;
                                        return false;
                                    }
                                }

                                var token_0 = reader.GetString()!;
                                output_1 = token_0;
                                if (!input_0.TryAdd(key_0, output_1))
                                {
                                    error = "Has duplicate key.";
                                    result = default;
                                    return false;
                                }
                            }

                            output_0 = global::System.Collections.Frozen.FrozenDictionary.ToFrozenDictionary(input_0);
                            result = output_0;
                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Collections.Frozen.FrozenDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Frozen.FrozenDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::System.Collections.Frozen.FrozenDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::System.Collections.Frozen.FrozenDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Collections.Frozen.FrozenDictionary<string, string> value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::System.Collections.Frozen.FrozenDictionary<string, string> value)
                    {
                        writer.WriteStartObject();
                        foreach (var token_1 in value)
                        {
                            writer.WritePropertyName(token_1.Key);
                            var token_2 = token_1.Value;
                            writer.WriteStringValue(token_2);
                        }

                        writer.WriteEndObject();
                    }

                }
                """
            ),
        };

        var compilation = Verifier.VerifyGeneratorAsync(code, sources);
        var assembly = Verifier.LoadAssembly(compilation);
        var testClassType = assembly.GetType("Tests.TestClass")!;
        var method = testClassType.GetMethod(
            "TryParseJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), testClassType.MakeByRefType(), typeof(string).MakeByRefType() }
        )!;
        var parameters = new object?[] { """{}""", null, null };
        var invokeResult = method.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        dynamic value = parameters[1]!;
        Assert.Null((int[])value.ArrayValue);
        Assert.Null(value.DictionaryValue);
        Assert.Null(value.FrozenValue);
        Assert.Null(value.HashSetValue);
        Assert.Equal(0, value.IntValue);
        Assert.Null(value.ListValue);
        Assert.Null(value.StringValue);
        Assert.Null(value.StringValue2);
        Assert.Null(value.TestSubClassValue);
    }
}
