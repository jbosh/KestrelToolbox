// <copyright file="JsonSerializableGeneratorTests.MultipleAssemblies.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection;
using Microsoft.CodeAnalysis;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

#pragma warning disable SA1515 // Single line comments for `Language=C#`
namespace UnitTests.GeneratorTests.JsonSerializableGenerator;

public partial class JsonSerializableGeneratorTests
{
    [Fact]
    public void MultipleAssemblies()
    {
        // language=c#
        var code0 = """
            using KestrelToolbox.Serialization.DataAnnotations;

            namespace Assembly0;

            public partial class TestClass
            {
                [Name("value")]
                public required int Value { get; set; }
            }
            """;

        var assemblyContext = new HotAssemblyLoadContext();
        var sources0 = Array.Empty<GeneratedSource>();

        var compilation0 = Verifier.VerifyGeneratorAsync(code0, sources0);
        var assemblyReference0 = assemblyContext.LoadMetadataReference(compilation0);

        // language=c#
        var code1 = """
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;

            namespace Assembly1;

            [JsonSerializable]
            public partial class TestClass
            {
                [Name("value")]
                public required Assembly0.TestClass Value { get; set; }
            }
            """;

        var sources1 = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Assembly1.TestClass.g.cs",
                // language=C#
                """
                namespace Assembly1;

                #nullable enable
                public partial class TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly1.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly1.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly1.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly1.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            global::Assembly0.TestClass member_0 = default!;
                            var required_0 = false;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Assembly1.TestClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        required_0 = true;
                                        global::Assembly0.TestClass output_0;
                                        if (!__KestrelToolbox_Serializable__File_global__Assembly1_TestClass.TryParseJson(ref reader, out global::Assembly0.TestClass? input_0, out error))
                                        {
                                            error = $"value > {error}";
                                            result = default;
                                            return false;
                                        }

                                        output_0 = input_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("value");
                                }

                                error = $"A required property was missing on Assembly1.TestClass. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::Assembly1.TestClass()
                            {
                                Value = member_0,
                            };

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Assembly1.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Assembly1.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Assembly1.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Assembly1.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Assembly1.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Assembly1.TestClass value)
                    {
                        writer.WriteStartObject();
                        var member_1 = value.Value;
                        writer.WritePropertyName("value");
                        __KestrelToolbox_Serializable__File_global__Assembly1_TestClass.SerializeJson(writer, member_1);
                        writer.WriteEndObject();
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(stream, this, options);

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                        => SerializeJsonAsync(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public string SerializeJson(global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.IO.Stream stream, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(stream, this, options);

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::System.Text.Json.JsonWriterOptions options = default)
                        => SerializeJson(bufferWriter, this, options);

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer)
                        => SerializeJson(writer, this);

                }

                [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                file static class __KestrelToolbox_Serializable__File_global__Assembly1_TestClass
                {
                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="json">A raw json string to parse.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(string json, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly0.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        using var rental = new global::KestrelToolbox.Types.ArrayPoolRental(global::System.Text.Encoding.UTF8.GetMaxByteCount(json.Length));
                        var byteCount = global::System.Text.Encoding.UTF8.GetBytes(json, rental.Array);
                        return TryParseJson(rental.AsSpan(0, byteCount), out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.ReadOnlySpan<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly0.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(global::System.Buffers.ReadOnlySequence<byte> utf8, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly0.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        var reader = new global::System.Text.Json.Utf8JsonReader(utf8, new() { AllowTrailingCommas = true, CommentHandling = global::System.Text.Json.JsonCommentHandling.Skip });
                        return TryParseJson(ref reader, out result, out error);
                    }

                    /// <summary>
                    /// Try to parse a json string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="reader">A <see cref="global::System.Text.Json.Utf8JsonReader"/> to read from.</param>
                    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseJson(ref global::System.Text.Json.Utf8JsonReader reader, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::Assembly0.TestClass? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        try
                        {
                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.None && !reader.Read())
                            {
                                error = "Cannot parse empty string.";
                                result = default;
                                return false;
                            }

                            int member_0 = default!;
                            var required_0 = false;
                            if (reader.TokenType != global::System.Text.Json.JsonTokenType.StartObject)
                            {
                                error = "'Assembly0.TestClass' requires an object.";
                                result = default;
                                return false;
                            }

                            while (reader.Read())
                            {
                                if (reader.TokenType == global::System.Text.Json.JsonTokenType.EndObject)
                                {
                                    if (!reader.IsFinalBlock)
                                    {
                                        reader.Read();
                                    }

                                    break;
                                }

                                var propertyName_0 = reader.GetString()!;
                                _ = reader.Read();
                                switch (propertyName_0)
                                {
                                    case "value":
                                    {
                                        required_0 = true;
                                        int output_0;
                                        if (reader.TokenType != global::System.Text.Json.JsonTokenType.Number)
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        if (!reader.TryGetInt32(out var token_0))
                                        {
                                            if (reader.TokenType == global::System.Text.Json.JsonTokenType.Null)
                                            {
                                                error = "'value' was null. Should be of type int32.";
                                                result = default;
                                                return false;
                                            }
                                            else
                                            {
                                                error = "'value' was not of type int32.";
                                                result = default;
                                                return false;
                                            }
                                        }

                                        output_0 = token_0;
                                        member_0 = output_0;
                                        break;
                                    }

                                    default:
                                    {
                                        switch (reader.TokenType)
                                        {
                                            case global::System.Text.Json.JsonTokenType.StartObject:
                                            case global::System.Text.Json.JsonTokenType.StartArray:
                                            {
                                                reader.Skip();
                                                break;
                                            }

                                            default:
                                            {
                                                // Process on next pass.
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            var var_0 = true;
                            var_0 &= required_0;
                            if (!var_0)
                            {
                                var var_1 = new global::System.Collections.Generic.List<string>();
                                if (!required_0)
                                {
                                    var_1.Add("value");
                                }

                                error = $"A required property was missing on Assembly0.TestClass. Missing: {string.Join(", ", var_1)}.";
                                result = default;
                                return false;
                            }

                            result = new global::Assembly0.TestClass()
                            {
                                Value = member_0,
                            };

                            error = default;
                            return true;
                        }
                        catch (global::System.Text.Json.JsonException)
                        {
                            result = default;
                            error = "Failed to parse json.";
                            return false;
                        }
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.IO.Stream stream, global::Assembly0.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Asynchronously serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <param name="cancellationToken">Cancellation token.</param>
                    /// <returns>void.</returns>
                    public static async global::System.Threading.Tasks.Task SerializeJsonAsync(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Assembly0.TestClass value, global::System.Text.Json.JsonWriterOptions options = default, global::System.Threading.CancellationToken cancellationToken = default)
                    {
                        await using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        await writer.FlushAsync(cancellationToken);
                    }

                    /// <summary>
                    /// Serialize an object to a string.
                    /// </summary>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    /// <returns>Json encoded string value.</returns>
                    public static string SerializeJson(global::Assembly0.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var memoryStream = new global::System.IO.MemoryStream();
                        SerializeJson(memoryStream, value, options);
                        return global::System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="stream">The stream to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.IO.Stream stream, global::Assembly0.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(stream, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a stream.
                    /// </summary>
                    /// <param name="bufferWriter">The <see cref="global::System.Buffers.IBufferWriter{T}"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    /// <param name="options">Options to initialize <see cref="global::System.Text.Json.Utf8JsonWriter"/> with.</param>
                    public static void SerializeJson(global::System.Buffers.IBufferWriter<byte> bufferWriter, global::Assembly0.TestClass value, global::System.Text.Json.JsonWriterOptions options = default)
                    {
                        using var writer = new global::System.Text.Json.Utf8JsonWriter(bufferWriter, options);
                        SerializeJson(writer, value);
                        writer.Flush();
                    }

                    /// <summary>
                    /// Serialize an object to a <see cref="global::System.Text.Json.Utf8JsonWriter"/>.
                    /// </summary>
                    /// <param name="writer">The <see cref="global::System.Text.Json.Utf8JsonWriter"/> to write to.</param>
                    /// <param name="value">The object to serialize.</param>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static void SerializeJson(global::System.Text.Json.Utf8JsonWriter writer, global::Assembly0.TestClass value)
                    {
                        writer.WriteStartObject();
                        var member_1 = value.Value;
                        writer.WriteNumber("value", member_1);
                        writer.WriteEndObject();
                    }

                }
                """
            ),
        };

        var references = AppDomain
            .CurrentDomain.GetAssemblies()
            .Where(assembly => !assembly.IsDynamic && !string.IsNullOrWhiteSpace(assembly.Location))
            .Select(assembly => MetadataReference.CreateFromFile(assembly.Location))
            .Concat(new[] { assemblyReference0 })
            .ToArray();

        var compilation1 = Verifier.VerifyGeneratorAsync(
            code1,
            Array.Empty<GeneratedDiagnostic>(),
            sources1,
            references
        );
        var assembly1 = assemblyContext.LoadAssembly(compilation1);

        var testClassType = assembly1.GetType("Assembly1.TestClass")!;
        var method = testClassType.GetMethod(
            "TryParseJson",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), testClassType.MakeByRefType(), typeof(string).MakeByRefType() }
        );
        Assert.NotNull(method);
        var parameters = new object?[] { """{ "value": { "value": 42 } }""", null, null };
        var invokeResult = method.Invoke(null, parameters);
        Assert.True((bool)invokeResult!);
        Assert.NotNull(parameters[1]);
        Assert.Null(parameters[2]);

        dynamic value = parameters[1]!;
        Assert.Equal(42, value.Value.Value);
    }
}
