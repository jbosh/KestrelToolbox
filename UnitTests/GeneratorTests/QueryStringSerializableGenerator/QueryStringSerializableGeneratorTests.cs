// <copyright file="QueryStringSerializableGeneratorTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Serialization;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

#pragma warning disable SA1515 // Single line comments for `Language=C#`
namespace UnitTests.GeneratorTests.QueryStringSerializableGenerator;

public class QueryStringSerializableGeneratorTests
{
    public QueryStringSerializableGeneratorTests()
    {
        // Use some of the types so the DLLs load.
        _ = new QueryStringSerializableAttribute(typeof(int));
    }

    [Fact]
    public void Duplicates()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;

            [QueryStringSerializable(typeof(FirstInstance), typeof(SecondInstance))]
            public static class All { }
            public sealed class FirstInstance
            {
                [Name("int")]
                public int? IntValue { get; set; }

                [Name("string")]
                public string? StringValue { get; set; }
            }

            public sealed class SecondInstance
            {
                [Name("int")]
                public int? IntValue { get; set; }

                [Name("string")]
                public string? StringValue { get; set; }
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/All.g.cs",
                // Language=C#
                """
                #nullable enable
                public static class All
                {
                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="queryCollection">Query string collection.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseQueryString(global::Microsoft.AspNetCore.Http.IQueryCollection queryCollection, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::FirstInstance? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        global::System.Nullable<int> member_0;
                        if (queryCollection.TryGetValue("int", out var inputValues_0))
                        {
                            if (inputValues_0.Count == 0)
                            {
                                error = "'int' must contain values.";
                                result = default;
                                return false;
                            }

                            if (inputValues_0.Count != 1)
                            {
                                error = "'int' cannot have multiple values.";
                                result = default;
                                return false;
                            }

                            var token_0 = inputValues_0[0] ?? string.Empty;
                            if (!int.TryParse(token_0, out var output_0))
                            {
                                error = "'int' was not of type int32.";
                                result = default;
                                return false;
                            }

                            member_0 = output_0;
                        }
                        else
                        {
                            member_0 = default;
                        }

                        string? member_1;
                        if (queryCollection.TryGetValue("string", out var inputValues_1))
                        {
                            if (inputValues_1.Count == 0)
                            {
                                error = "'string' must contain values.";
                                result = default;
                                return false;
                            }

                            if (inputValues_1.Count != 1)
                            {
                                error = "'string' cannot have multiple values.";
                                result = default;
                                return false;
                            }

                            var token_1 = inputValues_1[0] ?? string.Empty;

                            var output_1 = token_1;
                            member_1 = output_1;
                        }
                        else
                        {
                            member_1 = default;
                        }

                        result = new global::FirstInstance()
                        {
                            IntValue = member_0,
                            StringValue = member_1,
                        };

                        error = default;
                        return true;
                    }

                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="queryCollection">Query string collection.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    /// <remarks>
                    /// If <paramref name="result"/> does not have a default constructor, everything will be initialized to null or zero.
                    ///
                    /// Any usage of <see cref="global::KestrelToolbox.Serialization.DataAnnotations.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
                    /// force initialization of value type fields.
                    /// </remarks>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseQueryString(global::Microsoft.AspNetCore.Http.IQueryCollection queryCollection, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out global::SecondInstance? result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        global::System.Nullable<int> member_0;
                        if (queryCollection.TryGetValue("int", out var inputValues_0))
                        {
                            if (inputValues_0.Count == 0)
                            {
                                error = "'int' must contain values.";
                                result = default;
                                return false;
                            }

                            if (inputValues_0.Count != 1)
                            {
                                error = "'int' cannot have multiple values.";
                                result = default;
                                return false;
                            }

                            var token_0 = inputValues_0[0] ?? string.Empty;
                            if (!int.TryParse(token_0, out var output_0))
                            {
                                error = "'int' was not of type int32.";
                                result = default;
                                return false;
                            }

                            member_0 = output_0;
                        }
                        else
                        {
                            member_0 = default;
                        }

                        string? member_1;
                        if (queryCollection.TryGetValue("string", out var inputValues_1))
                        {
                            if (inputValues_1.Count == 0)
                            {
                                error = "'string' must contain values.";
                                result = default;
                                return false;
                            }

                            if (inputValues_1.Count != 1)
                            {
                                error = "'string' cannot have multiple values.";
                                result = default;
                                return false;
                            }

                            var token_1 = inputValues_1[0] ?? string.Empty;

                            var output_1 = token_1;
                            member_1 = output_1;
                        }
                        else
                        {
                            member_1 = default;
                        }

                        result = new global::SecondInstance()
                        {
                            IntValue = member_0,
                            StringValue = member_1,
                        };

                        error = default;
                        return true;
                    }

                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }
}
