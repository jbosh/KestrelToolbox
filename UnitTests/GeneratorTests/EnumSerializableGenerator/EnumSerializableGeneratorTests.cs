// <copyright file="EnumSerializableGeneratorTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Serialization;
using Microsoft.CodeAnalysis;
using UnitTests.RoslynVerifiers;
using Verifier = UnitTests.RoslynVerifiers.CSharpSourceGeneratorVerifier<KestrelToolbox.CodeGen.Generator>;

namespace UnitTests.GeneratorTests.EnumSerializableGenerator;

[SuppressMessage(
    "StyleCop.CSharp.LayoutRules",
    "SA1515:Single-line comment should be preceded by blank line",
    Justification = "Language comments are super nice."
)]
public class EnumSerializableGeneratorTests
{
    public EnumSerializableGeneratorTests()
    {
        // Use some of the types so the DLL loads.
        _ = new EnumSerializableAttribute(typeof(int));
    }

    [Fact]
    public void Empty()
    {
        // language=c#
        var code = """
            namespace Tests;

            public enum TestEnum
            {
            }
            """;

        _ = Verifier.VerifyGeneratorAsync(code);
    }

    [Fact]
    public void Basic()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization;
            using KestrelToolbox.Serialization.DataAnnotations;

            namespace Tests;

            public enum TestEnum
            {
                [Name("hello")]
                Hello = 0,

                Unnamed,
            }

            [EnumSerializable(typeof(TestEnum))]
            public partial class TestEnumClass
            {
            }
            """;

        var sources = new GeneratedSource[]
        {
            new(
                "KestrelToolbox.CodeGen/KestrelToolbox.CodeGen.Generator/SerializableAttribute/Tests.TestEnumClass.g.cs",
                // Language=C#
                """
                namespace Tests;

                #nullable enable
                public partial class TestEnumClass
                {
                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="value">Enum string value.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseEnum(string value, out global::Tests.TestEnum result) => TryParseEnum(value, out result, out _);

                    /// <summary>
                    /// Try to parse a string into <paramref name="result"/> using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="value">Enum string value.</param>
                    /// <param name="result">The output of the parsing. This value is default when parsing fails.</param>
                    /// <param name="error">An error message for when parsing fails. This val,ue is null when parsing succeeds.</param>
                    /// <returns>True if parsing was successful, false if it was not.</returns>
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static bool TryParseEnum(string value, out global::Tests.TestEnum result, [global::System.Diagnostics.CodeAnalysis.NotNullWhen(false)] out string? error)
                    {
                        switch (value)
                        {
                            case "hello":
                            {
                                result = global::Tests.TestEnum.Hello;
                                break;
                            }

                            default:
                            {
                                error = "'TestEnum' must be one of: hello.";
                                result = default;
                                return false;
                            }
                        }

                        error = default;
                        return true;
                    }

                    /// <summary>
                    /// Serializes an enum value to the equivalent string format using attributes in <see cref="global::KestrelToolbox.Serialization.DataAnnotations"/>.
                    /// </summary>
                    /// <param name="value">The value to serialize to a string.</param>
                    /// <returns>The string version of <paramref name="value" />.</returns>);
                    [global::System.CodeDom.Compiler.GeneratedCode("KestrelToolbox", "2.0.0")]
                    public static string SerializeEnum(global::Tests.TestEnum value)
                    {
                        return value switch
                        {
                            global::Tests.TestEnum.Hello => "hello",
                            _ => value.ToString(),
                        };
                    }

                }
                """
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, sources);
    }

    [Fact]
    public void ClassOrStructRequired()
    {
        // language=c#
        var code = """
            using KestrelToolbox.Serialization;

            namespace Tests;

            [EnumSerializable(typeof(TestEnumClass))]
            public partial class TestEnumClass
            {
            }
            """;

        var diagnostics = new GeneratedDiagnostic[]
        {
            new(
                "KTB0003",
                DiagnosticSeverity.Error,
                "EnumSerializable type global::Tests.TestEnumClass is Class. An enum is required."
            ),
        };

        _ = Verifier.VerifyGeneratorAsync(code, diagnostics);
    }
}
