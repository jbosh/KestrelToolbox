// <copyright file="ArrayPoolRentalTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Types;

namespace UnitTests;

public class ArrayPoolRentalTests
{
    [Fact]
    public void BasicBytes()
    {
        using var rental = new ArrayPoolRental(4096, true, true);
        Assert.True(rental.Array.Length >= 4096);
        Assert.Equal(rental.Array.Length, rental.Length);

        var span = rental.AsSpan();
        var memory = rental.AsMemory();
        Assert.Equal(span.Length, memory.Length);

        for (var i = 0; i < span.Length; i++)
        {
            span[i] = (byte)i;
            Assert.Equal((byte)i, memory.Span[i]);
            Assert.Equal((byte)i, rental.Array[i]);
        }

        var shortMemory = rental.AsMemory(16);
        var shortSpan = rental.AsSpan(16);
        Assert.Equal(memory.Length - 16, shortMemory.Length);
        Assert.Equal(span.Length - 16, shortSpan.Length);

        span = span.Slice(16);
        for (var i = 0; i < span.Length; i++)
        {
            Assert.Equal(span[i], shortMemory.Span[i]);
            Assert.Equal(span[i], shortSpan[i]);
        }

        span = rental.AsSpan().Slice(16, 32);
        shortMemory = rental.AsMemory(16, 32);
        shortSpan = rental.AsSpan(16, 32);
        Assert.Equal(32, shortMemory.Length);
        Assert.Equal(32, shortSpan.Length);

        for (var i = 0; i < span.Length; i++)
        {
            Assert.Equal(span[i], shortMemory.Span[i]);
            Assert.Equal(span[i], shortSpan[i]);
        }

        var arrayBefore = rental.Array;
        rental.Resize(16);
        Assert.Equal(arrayBefore, rental.Array);

        rental.ResizeAndKeepContents(16);
        Assert.Equal(arrayBefore, rental.Array);

        var oldSize = rental.Array.Length;
        var newSize = rental.Array.Length * 2;
        rental.ResizeAndKeepContents(newSize);
        Assert.NotEqual(arrayBefore, rental.Array);
        Assert.True(rental.Array.Length >= newSize);

        for (var i = 0; i < oldSize; i++)
        {
            Assert.Equal((byte)i, rental.Array[i]);
        }

        for (var i = oldSize; i < rental.Array.Length; i++)
        {
            Assert.Equal(0, rental.Array[i]);
        }

        rental.Resize(rental.Array.Length * 2);
        for (var i = 0; i < rental.Array.Length; i++)
        {
            Assert.Equal(0, rental.Array[i]);
        }
    }

    [Fact]
    public void Basic()
    {
        using var rental = new ArrayPoolRental<int>(4096, true, true);
        Assert.True(rental.Array.Length >= 4096);
        Assert.Equal(rental.Array.Length, rental.Length);

        var span = rental.AsSpan();
        var memory = rental.AsMemory();
        Assert.Equal(span.Length, memory.Length);

        for (var i = 0; i < span.Length; i++)
        {
            span[i] = i;
            Assert.Equal(i, memory.Span[i]);
            Assert.Equal(i, rental.Array[i]);
        }

        var shortMemory = rental.AsMemory(16);
        var shortSpan = rental.AsSpan(16);
        Assert.Equal(memory.Length - 16, shortMemory.Length);
        Assert.Equal(span.Length - 16, shortSpan.Length);

        span = span.Slice(16);
        for (var i = 0; i < span.Length; i++)
        {
            Assert.Equal(span[i], shortMemory.Span[i]);
            Assert.Equal(span[i], shortSpan[i]);
        }

        span = rental.AsSpan().Slice(16, 32);
        shortMemory = rental.AsMemory(16, 32);
        shortSpan = rental.AsSpan(16, 32);
        Assert.Equal(32, shortMemory.Length);
        Assert.Equal(32, shortSpan.Length);

        for (var i = 0; i < span.Length; i++)
        {
            Assert.Equal(span[i], shortMemory.Span[i]);
            Assert.Equal(span[i], shortSpan[i]);
        }

        var arrayBefore = rental.Array;
        rental.Resize(16);
        Assert.Equal(arrayBefore, rental.Array);

        rental.ResizeAndKeepContents(16);
        Assert.Equal(arrayBefore, rental.Array);

        var oldSize = rental.Array.Length;
        var newSize = rental.Array.Length * 2;
        rental.ResizeAndKeepContents(newSize);
        Assert.NotEqual(arrayBefore, rental.Array);
        Assert.True(rental.Array.Length >= newSize);

        for (var i = 0; i < oldSize; i++)
        {
            Assert.Equal(i, rental.Array[i]);
        }

        for (var i = oldSize; i < rental.Array.Length; i++)
        {
            Assert.Equal(0, rental.Array[i]);
        }

        rental.Resize(rental.Array.Length * 2);
        for (var i = 0; i < rental.Array.Length; i++)
        {
            Assert.Equal(0, rental.Array[i]);
        }
    }

    [Fact]
    public void Constructors()
    {
        using (var rental = new ArrayPoolRental(1024))
        {
            Assert.False(rental.ClearContentsOnInitialization);
            Assert.False(rental.ClearContentsOnDispose);
        }

        using (var rental = new ArrayPoolRental(1024, true))
        {
            Assert.True(rental.ClearContentsOnInitialization);
            Assert.False(rental.ClearContentsOnDispose);
        }

        using (var rental = new ArrayPoolRental(1024, true, true))
        {
            Assert.True(rental.ClearContentsOnInitialization);
            Assert.True(rental.ClearContentsOnDispose);
        }

        using (var rental = new ArrayPoolRental<byte>(1024))
        {
            Assert.False(rental.ClearContentsOnInitialization);
            Assert.False(rental.ClearContentsOnDispose);
        }

        using (var rental = new ArrayPoolRental<byte>(1024, true))
        {
            Assert.True(rental.ClearContentsOnInitialization);
            Assert.False(rental.ClearContentsOnDispose);
        }

        using (var rental = new ArrayPoolRental<byte>(1024, true, true))
        {
            Assert.True(rental.ClearContentsOnInitialization);
            Assert.True(rental.ClearContentsOnDispose);
        }
    }
}
