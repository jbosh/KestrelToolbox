// <copyright file="TypedefTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Types;

namespace UnitTests;

#if !DISABLE_ROSLYN_GENERATED_TESTS

public sealed partial class TypedefTests
{
    [Typedef(typeof(int))]
    public sealed partial class Int32Alias { }

    [Fact]
    public void Int32Test()
    {
        var alias = new Int32Alias(15);
        Assert.Equal(15, (int)alias);
        Assert.Equal(15.GetHashCode(), alias.GetHashCode());
        Assert.Equal("15", alias.ToString(CultureInfo.InvariantCulture));

        var alias2 = new Int32Alias(15);
        Assert.True(alias == alias2);
        Assert.False(alias != alias2);

        alias2 = new Int32Alias(13);
        Assert.False(alias == alias2);
        Assert.True(alias != alias2);
    }

    [Typedef(typeof(int))]
    public readonly partial struct Int32StructAlias { }

    [Fact]
    public void Int32StructTest()
    {
        var alias = new Int32StructAlias(15);
        Assert.Equal(15, (int)alias);
        Assert.Equal(15.GetHashCode(), alias.GetHashCode());
        Assert.Equal("15", alias.ToString(CultureInfo.InvariantCulture));

        var alias2 = new Int32StructAlias(15);
        Assert.True(alias == alias2);
        Assert.False(alias != alias2);

        alias2 = new Int32StructAlias(13);
        Assert.False(alias == alias2);
        Assert.True(alias != alias2);
    }

    [Typedef(typeof(string))]
    public sealed partial class StringAlias { }

    [Fact]
    public void StringTest()
    {
        var alias = new StringAlias("hello");
        Assert.Equal("hello", (string)alias);
        Assert.Equal("hello".GetHashCode(), alias.GetHashCode());
        Assert.Equal("hello", alias.ToString(CultureInfo.InvariantCulture));
        Assert.Equal(-1, alias.CompareTo("world"));
        Assert.Equal("lo", alias.Substring(3));

        var nullAlias = StringAlias.From(null);
        Assert.Null(nullAlias);
        nullAlias = StringAlias.From("world");
        Assert.NotNull(nullAlias);
        Assert.Equal("world", (string)nullAlias);

        var alias2 = new StringAlias("hello");
        Assert.True(alias == alias2);
        Assert.False(alias != alias2);

        alias2 = new StringAlias("world");
        Assert.False(alias == alias2);
        Assert.True(alias != alias2);
    }

    public sealed class PropertyClass : IEquatable<PropertyClass>
    {
        public string? Value0 { get; set; }

        public string? Value1 { get; set; }

        private string? field;

        public string? GetField() => this.field;

        public string? this[int index]
        {
            get =>
                index switch
                {
                    0 => this.Value0,
                    1 => this.Value1,
                    _ => this.field,
                };
            set
            {
                switch (index)
                {
                    case 0:
                        this.Value0 = value;
                        break;
                    case 1:
                        this.Value1 = value;
                        break;
                    default:
                        this.field = value;
                        break;
                }
            }
        }

        /// <inheritdoc />
        public bool Equals(PropertyClass? other)
        {
            if (other is null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.field == other.field && this.Value0 == other.Value0 && this.Value1 == other.Value1;
        }

        /// <inheritdoc />
        public override bool Equals(object? obj)
        {
            return ReferenceEquals(this, obj) || (obj is PropertyClass other && this.Equals(other));
        }

        /// <inheritdoc />
        [SuppressMessage(
            "Minor Bug",
            "S2328:\"GetHashCode\" should not reference mutable fields",
            Justification = "It's a test."
        )]
        public override int GetHashCode()
        {
            return HashCode.Combine(this.field, this.Value0, this.Value1);
        }
    }

    [Typedef(typeof(PropertyClass))]
    public sealed partial class PropertyClassAlias { }

    [Fact]
    public void PropertyClassTest()
    {
        var alias = new PropertyClassAlias(new PropertyClass())
        {
            [0] = "Yo",
            [1] = "hello",
            [2] = "world",
        };
        Assert.Equal("world", alias.GetField());
        Assert.Equal("Yo", alias.Value0);
        Assert.Equal("hello", alias.Value1);
        Assert.Equal("Yo", alias[0]);
        Assert.Equal("hello", alias[1]);
        Assert.Equal("world", alias[2]);

        var expected = new PropertyClass
        {
            [0] = "Yo",
            [1] = "hello",
            [2] = "world",
        };
        Assert.Equal(new PropertyClassAlias(expected), alias);
        Assert.Equal(expected, (PropertyClass)alias);
        Assert.Equal(expected.GetHashCode(), alias.GetHashCode());
    }

    [Typedef(typeof(PropertyClass))]
    public readonly partial struct PropertyStructAlias { }

    [Fact]
    public void PropertyStructTest()
    {
        var alias = new PropertyStructAlias(new PropertyClass())
        {
            [0] = "Yo",
            [1] = "hello",
            [2] = "world",
        };
        Assert.Equal("world", alias.GetField());
        Assert.Equal("Yo", alias.Value0);
        Assert.Equal("hello", alias.Value1);
        Assert.Equal("Yo", alias[0]);
        Assert.Equal("hello", alias[1]);
        Assert.Equal("world", alias[2]);

        var expected = new PropertyClass
        {
            [0] = "Yo",
            [1] = "hello",
            [2] = "world",
        };
        Assert.Equal(new PropertyStructAlias(expected), alias);
        Assert.Equal(expected, (PropertyClass)alias);
        Assert.Equal(expected.GetHashCode(), alias.GetHashCode());
    }
}
#endif // DISABLE_ROSLYN_GENERATED_TESTS
