// <copyright file="MutexSlimWithValueTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Types;

namespace UnitTests;

public class MutexSlimWithValueTests
{
    private readonly ITestOutputHelper testOutputHelper;

    public MutexSlimWithValueTests(ITestOutputHelper testOutputHelper)
    {
        this.testOutputHelper = testOutputHelper;
    }

    private sealed class TestState : IDisposable
    {
        public MutexSlim<long> Mutex { get; } = new(0);

        public void Dispose()
        {
            this.Mutex.Dispose();
        }
    }

    [Fact]
    public void Basic()
    {
        using var mutex = new MutexSlim<int>(0);

        using var locker = mutex.Lock();
        Assert.Equal(0, locker.Value);
    }

    [Fact]
    public void ReferenceType()
    {
        using var mutex = new MutexSlim<string?>(null);

        using (var locker = mutex.Lock())
        {
            Assert.Null(locker.Value);
            locker.Value = "hello";
        }

        using (var locker = mutex.Lock())
        {
            Assert.Equal("hello", locker.Value);
        }
    }

    [Fact]
    public void Exceptions()
    {
        var mutex = new MutexSlim<int>(13);

        using var locker = mutex.Lock();
        Assert.Equal(13, locker.Value);
        _ = Assert.Throws<InvalidOperationException>(mutex.Dispose);
    }

    [Theory]
    [InlineData(2, 20_000_000)]
    [InlineData(8, 2_000_000)]
    [InlineData(16, 1_000_000)]
    public void TestThreads(int numThreads, int iterations)
    {
        using var state = new TestState();
        var threads = new Thread[numThreads];
        for (var i = 0; i < threads.Length; i++)
        {
            // ReSharper disable once AccessToDisposedClosure
            threads[i] = new Thread(() => Run(state, iterations));
            threads[i].Start();
        }

        foreach (var thread in threads)
        {
            thread.Join();
        }

        using (var locker = state.Mutex.Lock())
        {
            Assert.Equal(iterations * threads.Length, locker.Value);
        }

        static void Run(TestState state, int iterations)
        {
            for (var i = 0; i < iterations; i++)
            {
                _ = Thread.Yield();
                using var locker = state.Mutex.Lock();
                locker.Value++;
            }
        }
    }

    [Fact]
    [SuppressMessage("ReSharper", "MethodSupportsCancellation", Justification = "Not correct usage.")]
    public async Task Cancellation()
    {
        const int Iterations = 2_000_000;
        using var state = new TestState();
        using var cancellationToken = new CancellationTokenSource();
        var tasks = new Task[8];
        for (var i = 0; i < tasks.Length; i++)
        {
            // ReSharper disable once AccessToDisposedClosure
            tasks[i] = Task.Run(() => Run(state, Iterations, cancellationToken.Token));
        }

        cancellationToken.CancelAfter(500);

        try
        {
            await Task.WhenAll(tasks);
        }
        catch (OperationCanceledException)
        {
            // Success!
        }

        using (var locker = await state.Mutex.LockAsync())
        {
            Assert.True(locker.Value <= Iterations * tasks.Length);
        }

        static void Run(TestState state, int iterations, CancellationToken cancellationToken)
        {
            for (var i = 0; i < iterations; i++)
            {
                _ = Thread.Yield();
                using var locker = state.Mutex.Lock(cancellationToken);
                locker.Value++;
            }
        }
    }

    private sealed class TestStateDotNet : IDisposable
    {
        public SemaphoreSlim Semaphore { get; } = new(1, 1);

        public long Counter { get; set; }

        public void Dispose()
        {
            this.Semaphore.Dispose();
        }
    }

    [Benchmark]
    public void TestUncontendedPerformance()
    {
        const int Iterations = 100_000_000;
        const int NumThreads = 1;
        this.TestPerformance(NumThreads, Iterations);
    }

    [Benchmark]
    public void TestContendedPerformance()
    {
        const int Iterations = 300_000;
        const int NumThreads = 8;
        this.TestPerformance(NumThreads, Iterations);
    }

    private void TestPerformance(int numThreads, int numIterations)
    {
        for (var benchmarkIteration = 4; benchmarkIteration >= 0; benchmarkIteration--)
        {
            var dotnetTimer = Stopwatch.StartNew();
            using (var state = new TestStateDotNet())
            {
                var threads = new Thread[numThreads];
                for (var i = 0; i < threads.Length; i++)
                {
                    threads[i] = new Thread(() => Run(state, numIterations).Wait());
                    threads[i].Start();
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                Assert.Equal(numIterations * threads.Length, state.Counter);

                static async Task Run(TestStateDotNet state, int iterations)
                {
                    for (var i = 0; i < iterations; i++)
                    {
                        try
                        {
                            await state.Semaphore.WaitAsync();
                            state.Counter++;
                        }
                        finally
                        {
                            _ = state.Semaphore.Release();
                        }
                    }
                }
            }

            dotnetTimer.Stop();

            var lockerTimer = Stopwatch.StartNew();
            using (var state = new TestState())
            {
                var threads = new Thread[numThreads];
                for (var i = 0; i < threads.Length; i++)
                {
                    threads[i] = new Thread(() => Run(state, numIterations).Wait());
                    threads[i].Start();
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                using (var locker = state.Mutex.Lock())
                {
                    Assert.Equal(numIterations * threads.Length, locker.Value);
                }

                static async Task Run(TestState state, int iterations)
                {
                    for (var i = 0; i < iterations; i++)
                    {
                        using var locker = await state.Mutex.LockAsync();
                        locker.Value++;
                    }
                }
            }

            lockerTimer.Stop();

            if (benchmarkIteration == 0)
            {
                this.testOutputHelper.WriteLine(
                    $"Locker   : {lockerTimer.Elapsed:g} ({100.0 - (lockerTimer.Elapsed / dotnetTimer.Elapsed * 100.0):0.00}% faster)"
                );
                this.testOutputHelper.WriteLine($"Dotnet   : {dotnetTimer.Elapsed:g}");
                Assert.True(lockerTimer.Elapsed < dotnetTimer.Elapsed, "KestrelToolbox was not faster than dotnet.");
            }
        }
    }
}
