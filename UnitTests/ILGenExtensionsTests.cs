// <copyright file="ILGenExtensionsTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection.Emit;
using KestrelToolbox.Internal.ILGenExtensions;

namespace UnitTests;

public class ILGenExtensionsTests
{
    [Fact]
    public void Basic()
    {
        TestEmit(decimal.MaxValue);
        TestEmit(decimal.MinValue);
        TestEmit(int.MinValue);
        TestEmit(int.MaxValue);
        TestEmit(uint.MinValue);
        TestEmit(uint.MaxValue);
        TestEmit(long.MinValue);
        TestEmit(long.MaxValue);
        TestEmit(ulong.MinValue);
        TestEmit(ulong.MaxValue);

        TestEmit(0UL);
        TestEmit(1L);
        TestEmit(-1L);
        TestEmit(-2L);
        TestEmit(-20L);
        TestEmit(300820072UL);
        TestEmit((ulong)int.MaxValue);

        TestEmit(-1);
        TestEmit(-20);
        TestEmit(ulong.MaxValue - 20UL);
        TestEmit(ulong.MinValue + 47UL);
        TestEmit(-2m);
        TestEmit(20m);
        TestEmit(decimal.MaxValue - 20m);

        TestEmit(20.0);
        TestEmit(20.0f);
        TestEmit(20.0000000001);
        TestEmit(20.0000000001f);
    }

    private static void TestEmit(dynamic value)
    {
        var dynamicMethod = new DynamicMethod(string.Empty, value.GetType(), Type.EmptyTypes);

        var ilGen = dynamicMethod.GetILGenerator();
        ILGenExtensions.EmitLdc(ilGen, value);

        ilGen.Emit(OpCodes.Ret);

        var result = dynamicMethod.Invoke(null, null)!;
        Assert.Equal(value, result);
    }
}
