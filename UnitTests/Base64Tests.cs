// <copyright file="Base64Tests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.IO;
using System.Text;
using KestrelToolbox.Serialization;
using KestrelToolbox.Types;

namespace UnitTests;

public class Base64Tests
{
    private const int RndSeed = 275002;
    private const string ShortLoremIpsum = "Lorem ipsum";

    private const string LongLoremIpsum = """
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam eget nisi eu est scelerisque commodo.Proin et dictum nisi.Sed ultrices nibh a risus molestie congue. Sed eu tincidunt elit, at bibendum elit.Phasellus placerat arcu nec consequat ullamcorper. Nullam odio ante, imperdiet id ultrices a, sodales nec augue.Pellentesque et lorem efficitur, mollis ante id, porta neque. Donec luctus dolor erat, non lacinia lectus sagittis eu.Praesent eget quam quis nunc efficitur hendrerit.Duis quis enim interdum, volutpat enim ut, laoreet lacus. Sed condimentum orci in volutpat rhoncus. Integer ac scelerisque libero.

        Curabitur vestibulum nunc in nisi porta posuere.Curabitur sollicitudin tincidunt diam sed auctor.Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam at molestie sapien.Fusce placerat enim sit amet ex elementum, sed ullamcorper libero vulputate. Proin sapien purus, pulvinar id hendrerit ut, luctus in tortor.Donec scelerisque turpis ac suscipit tincidunt. Donec bibendum sapien sed venenatis pretium. Praesent mollis ultrices nisi auctor rhoncus. Sed quis scelerisque orci, eget semper nisi.In nisl nisl, volutpat vitae ipsum quis, lobortis faucibus orci.Phasellus at tortor fringilla, convallis ante eget, pharetra magna.Sed nunc erat, accumsan hendrerit mollis eu, fringilla commodo sapien.

        Cras vulputate semper orci in finibus.Sed et massa pellentesque, sagittis lorem at, dictum felis.Aenean nec enim commodo, elementum nisi et, scelerisque felis.Aliquam lorem ligula, finibus at ipsum vitae, mollis hendrerit justo.Aliquam nulla tortor, consectetur in euismod at, convallis et erat.Sed eu pharetra dui. Nam sem nulla, molestie id aliquet ut, sodales sit amet nibh.Nunc quis hendrerit erat, vitae lacinia ante.Sed interdum justo ut ultricies fringilla. Fusce pharetra a ex eget pulvinar. In nec molestie dui. Donec orci ligula, ornare vel semper non, convallis ut ex.In at ante semper, interdum augue sed, aliquet justo.Aenean hendrerit, felis convallis faucibus congue, arcu justo sollicitudin nibh, a placerat felis mauris sed enim.Duis lobortis, sem in luctus gravida, diam metus laoreet massa, ac vestibulum magna nunc vitae nunc. Donec congue in sapien in
        """;

    [Fact]
    public void Miscellaneous()
    {
        Assert.Equal(string.Empty, Base64.EncodeToString(Array.Empty<byte>()));
        Assert.Equal(0, Base64.EncodeToUtf8(Array.Empty<byte>(), Array.Empty<byte>()));

        var outOfRangeException = Assert.Throws<ArgumentOutOfRangeException>(
            () => Base64.EncodeToUtf8(new byte[1], Array.Empty<byte>())
        );
        Assert.Equal(
            "output must be longer than 4 bytes. Please use GetMaxEncodedToUtf8Length. (Parameter 'output')",
            outOfRangeException.Message
        );
        Assert.Equal("output", outOfRangeException.ParamName);

        Assert.Equal(Array.Empty<byte>(), Base64.DecodeFromString(string.Empty));

        var invalidDataException = Assert.Throws<InvalidDataException>(() => Base64.DecodeFromString("abc"));
        Assert.Equal("Invalid padding.", invalidDataException.Message);

        Assert.True(Base64.TryDecodeFromString(string.Empty, Array.Empty<byte>(), out var writtenBytes));
        Assert.Equal(0, writtenBytes);

        Assert.True(Base64.TryDecodeFromUtf8(Array.Empty<byte>(), Array.Empty<byte>(), out writtenBytes));
        Assert.Equal(0, writtenBytes);

        Assert.False(Base64.TryDecodeFromUtf8("abc"u8, Array.Empty<byte>(), out writtenBytes));
        Assert.Equal(0, writtenBytes);
    }

    [Fact]
    public void EdgeOfArray()
    {
        var bytes = Convert.FromBase64String(string.Concat(Enumerable.Range(0, 16).Select(_ => "+/+/")));

        var encoded = Base64.EncodeToString(bytes, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(DotNetConvertToBase64Url(bytes), encoded);
        var decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(bytes, decoded);

        encoded = Base64.EncodeToString(bytes, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Convert.ToBase64String(bytes), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(bytes, decoded);
    }

    [Fact]
    public void Ranges()
    {
        var linearBytes = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();

        var encoded = Base64.EncodeToString(linearBytes, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Convert.ToBase64String(linearBytes), encoded);
        var decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(linearBytes, decoded);

        encoded = Base64.EncodeToString(linearBytes, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(DotNetConvertToBase64Url(linearBytes), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(linearBytes, decoded);

        var duplicatedBytes = Enumerable
            .Range(0, 256)
            .SelectMany(i => Enumerable.Range(0, 3).Select(_ => (byte)i))
            .ToArray();

        encoded = Base64.EncodeToString(duplicatedBytes, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Convert.ToBase64String(duplicatedBytes), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(duplicatedBytes, decoded);

        encoded = Base64.EncodeToString(duplicatedBytes, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(DotNetConvertToBase64Url(duplicatedBytes), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(duplicatedBytes, decoded);
    }

    [Fact]
    public void StandardBasic()
    {
        var encoded = EncodeUtf8ToString(ShortLoremIpsum, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Convert.ToBase64String(Encoding.UTF8.GetBytes(ShortLoremIpsum)), encoded);
        var decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Encoding.UTF8.GetBytes(ShortLoremIpsum), decoded);

        encoded = EncodeUtf8ToString(ShortLoremIpsum, Base64Encoding.Standard);
        Assert.Equal(Convert.ToBase64String(Encoding.UTF8.GetBytes(ShortLoremIpsum)).TrimEnd('='), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard);
        Assert.Equal(Encoding.UTF8.GetBytes(ShortLoremIpsum), decoded);

        encoded = EncodeUtf8ToString(LongLoremIpsum, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Convert.ToBase64String(Encoding.UTF8.GetBytes(LongLoremIpsum)), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard | Base64Encoding.Padding);
        Assert.Equal(Encoding.UTF8.GetBytes(LongLoremIpsum), decoded);

        encoded = EncodeUtf8ToString(LongLoremIpsum, Base64Encoding.Standard);
        Assert.Equal(Convert.ToBase64String(Encoding.UTF8.GetBytes(LongLoremIpsum)).TrimEnd('='), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard);
        Assert.Equal(Encoding.UTF8.GetBytes(LongLoremIpsum), decoded);
    }

    [Fact]
    public void StandardRandom()
    {
        var rnd = new Random(RndSeed);
        for (var i = 32; i < 4096; i++)
        {
            using var rental = new ArrayPoolRental(i);
            var span = rental.Array.AsSpan(0, i);
            rnd.NextBytes(span);

            var encoded = Base64.EncodeToString(span, Base64Encoding.Standard | Base64Encoding.Padding);
            Assert.Equal(Convert.ToBase64String(span), encoded);
            var decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard | Base64Encoding.Padding);
            Assert.Equal(span.ToArray(), decoded);

            encoded = Base64.EncodeToString(span, Base64Encoding.Standard);
            Assert.Equal(Convert.ToBase64String(span).TrimEnd('='), encoded);
            decoded = Base64.DecodeFromString(encoded, Base64Encoding.Standard);
            Assert.Equal(span.ToArray(), decoded);
        }
    }

    [Fact]
    public void UrlBasic()
    {
        var encoded = EncodeUtf8ToString(ShortLoremIpsum, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(DotNetConvertToBase64Url(ShortLoremIpsum), encoded);
        var decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(Encoding.UTF8.GetBytes(ShortLoremIpsum), decoded);

        encoded = EncodeUtf8ToString(ShortLoremIpsum, Base64Encoding.Url);
        Assert.Equal(DotNetConvertToBase64Url(ShortLoremIpsum).TrimEnd('='), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url);
        Assert.Equal(Encoding.UTF8.GetBytes(ShortLoremIpsum), decoded);

        encoded = EncodeUtf8ToString(LongLoremIpsum, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(DotNetConvertToBase64Url(LongLoremIpsum), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url | Base64Encoding.Padding);
        Assert.Equal(Encoding.UTF8.GetBytes(LongLoremIpsum), decoded);

        encoded = EncodeUtf8ToString(LongLoremIpsum, Base64Encoding.Url);
        Assert.Equal(DotNetConvertToBase64Url(LongLoremIpsum).TrimEnd('='), encoded);
        decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url);
        Assert.Equal(Encoding.UTF8.GetBytes(LongLoremIpsum), decoded);
    }

    [Fact]
    public void UrlRandom()
    {
        var rnd = new Random(RndSeed);
        for (var i = 32; i < 4096; i++)
        {
            using var rental = new ArrayPoolRental(i);
            var span = rental.Array.AsSpan(0, i);
            rnd.NextBytes(span);

            var encoded = Base64.EncodeToString(span, Base64Encoding.Url | Base64Encoding.Padding);
            Assert.Equal(DotNetConvertToBase64Url(span), encoded);
            var decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url | Base64Encoding.Padding);
            Assert.Equal(span.ToArray(), decoded);

            encoded = Base64.EncodeToString(span, Base64Encoding.Url);
            Assert.Equal(DotNetConvertToBase64Url(span).TrimEnd('='), encoded);
            decoded = Base64.DecodeFromString(encoded, Base64Encoding.Url);
            Assert.Equal(span.ToArray(), decoded);
        }
    }

    [Fact]
    public void LowCharacterCounts()
    {
        var arr = new byte[1];
        for (var i = 0; i < byte.MaxValue; i++)
        {
            arr[0] = (byte)i;

            var encoded = Base64.EncodeToString(arr, Base64Encoding.Standard | Base64Encoding.Padding);
            Assert.Equal(Convert.ToBase64String(arr), encoded);

            encoded = Base64.EncodeToString(arr, Base64Encoding.Standard);
            Assert.Equal(Convert.ToBase64String(arr).TrimEnd('='), encoded);

            encoded = Base64.EncodeToString(arr, Base64Encoding.Url | Base64Encoding.Padding);
            Assert.Equal(DotNetConvertToBase64Url(arr), encoded);

            encoded = Base64.EncodeToString(arr, Base64Encoding.Url);
            Assert.Equal(DotNetConvertToBase64Url(arr).TrimEnd('='), encoded);
        }

        arr = new byte[2];
        for (var i = 0; i < byte.MaxValue; i++)
        {
            for (var j = 0; j < byte.MaxValue; j++)
            {
                arr[0] = (byte)i;
                arr[1] = (byte)j;

                var encoded = Base64.EncodeToString(arr, Base64Encoding.Standard | Base64Encoding.Padding);
                Assert.Equal(Convert.ToBase64String(arr), encoded);

                encoded = Base64.EncodeToString(arr, Base64Encoding.Standard);
                Assert.Equal(Convert.ToBase64String(arr).TrimEnd('='), encoded);

                encoded = Base64.EncodeToString(arr, Base64Encoding.Url | Base64Encoding.Padding);
                Assert.Equal(DotNetConvertToBase64Url(arr), encoded);

                encoded = Base64.EncodeToString(arr, Base64Encoding.Url);
                Assert.Equal(DotNetConvertToBase64Url(arr).TrimEnd('='), encoded);
            }
        }

        arr = new byte[3];
        for (var i = 0; i < byte.MaxValue; i++)
        {
            for (var j = 0; j < byte.MaxValue; j++)
            {
                for (var k = 0; k < byte.MaxValue; k++)
                {
                    arr[0] = (byte)i;
                    arr[1] = (byte)j;
                    arr[2] = (byte)k;

                    var encoded = Base64.EncodeToString(arr, Base64Encoding.Standard | Base64Encoding.Padding);
                    Assert.Equal(Convert.ToBase64String(arr), encoded);

                    encoded = Base64.EncodeToString(arr, Base64Encoding.Standard);
                    Assert.Equal(Convert.ToBase64String(arr).TrimEnd('='), encoded);

                    encoded = Base64.EncodeToString(arr, Base64Encoding.Url | Base64Encoding.Padding);
                    Assert.Equal(DotNetConvertToBase64Url(arr), encoded);

                    encoded = Base64.EncodeToString(arr, Base64Encoding.Url);
                    Assert.Equal(DotNetConvertToBase64Url(arr).TrimEnd('='), encoded);
                }
            }
        }
    }

    [Fact]
    public void BadInputs()
    {
        using var decodedRental = new ArrayPoolRental(128 * 1024);
        var decoded = decodedRental.Array;

        // Padding tests
        var success = Base64.TryDecodeFromString(
            "bG9yZW0gaXBzdW0=",
            decoded,
            out var decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.True(success);
        Assert.Equal(11, decodedBytes);
        success = Base64.TryDecodeFromString(
            "bG9yZW0gaXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "bG9yZW0gaXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Url | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString("bG9yZW0gaXBzdW0", decoded, out decodedBytes, Base64Encoding.Url);
        Assert.True(success);
        Assert.Equal(11, decodedBytes);

        // Invalid characters
        success = Base64.TryDecodeFromString(
            "bG9yZW0g\naXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "bG9yZW0gaXBzdW0\n",
            decoded,
            out decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "\nbG9yZW0gaXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "-bG9yZW0gaXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "bG9yZW0gaXBz_dW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Standard | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "+bG9yZW0gaXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Url | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);
        success = Base64.TryDecodeFromString(
            "bG9yZW/0gaXBzdW0",
            decoded,
            out decodedBytes,
            Base64Encoding.Url | Base64Encoding.Padding
        );
        Assert.False(success);
        Assert.Equal(0, decodedBytes);

        // Decode 16 bytes into 15 bytes
        Assert.False(
            Base64.TryDecodeFromString(
                "YTc5YmQwOWE4MjQ5OGVhMGE0MWUxOGQ0ZDg1NzExNDEK",
                new byte[15],
                out decodedBytes,
                Base64Encoding.Url
            )
        );
        Assert.Equal(0, decodedBytes);
    }

    [Fact]
    public void MaxEncodedBytes()
    {
        Assert.Equal(4, Base64.GetMaxEncodedToUtf8Length(1));
        Assert.Equal(4, Base64.GetMaxEncodedToUtf8Length(2));
        Assert.Equal(4, Base64.GetMaxEncodedToUtf8Length(3));
        Assert.Equal(8, Base64.GetMaxEncodedToUtf8Length(4));
        Assert.Equal(8, Base64.GetMaxEncodedToUtf8Length(5));
        Assert.Equal(8, Base64.GetMaxEncodedToUtf8Length(6));
        Assert.Equal(12, Base64.GetMaxEncodedToUtf8Length(7));
        Assert.Equal(12, Base64.GetMaxEncodedToUtf8Length(8));
        Assert.Equal(12, Base64.GetMaxEncodedToUtf8Length(9));

        Assert.Equal(2, Base64.GetMaxEncodedToUtf8Length(1, Base64Encoding.Standard));
        Assert.Equal(3, Base64.GetMaxEncodedToUtf8Length(2, Base64Encoding.Standard));
        Assert.Equal(4, Base64.GetMaxEncodedToUtf8Length(3, Base64Encoding.Standard));
        Assert.Equal(6, Base64.GetMaxEncodedToUtf8Length(4, Base64Encoding.Standard));
        Assert.Equal(7, Base64.GetMaxEncodedToUtf8Length(5, Base64Encoding.Standard));
        Assert.Equal(8, Base64.GetMaxEncodedToUtf8Length(6, Base64Encoding.Standard));
        Assert.Equal(10, Base64.GetMaxEncodedToUtf8Length(7, Base64Encoding.Standard));
        Assert.Equal(11, Base64.GetMaxEncodedToUtf8Length(8, Base64Encoding.Standard));
        Assert.Equal(12, Base64.GetMaxEncodedToUtf8Length(9, Base64Encoding.Standard));
    }

    [Fact]
    public void GetMaxDecodedBytes()
    {
        Assert.Equal(0, Base64.GetMaxDecodedFromUtf8Length(1));
        Assert.Equal(1, Base64.GetMaxDecodedFromUtf8Length(2));
        Assert.Equal(2, Base64.GetMaxDecodedFromUtf8Length(3));
        Assert.Equal(3, Base64.GetMaxDecodedFromUtf8Length(4));
        Assert.Equal(3, Base64.GetMaxDecodedFromUtf8Length(5));
        Assert.Equal(4, Base64.GetMaxDecodedFromUtf8Length(6));
        Assert.Equal(5, Base64.GetMaxDecodedFromUtf8Length(7));
        Assert.Equal(6, Base64.GetMaxDecodedFromUtf8Length(8));
        Assert.Equal(6, Base64.GetMaxDecodedFromUtf8Length(9));

        Assert.Equal(0, Base64.GetMaxDecodedFromUtf8Length(1, Base64Encoding.Standard));
        Assert.Equal(1, Base64.GetMaxDecodedFromUtf8Length(2, Base64Encoding.Standard));
        Assert.Equal(2, Base64.GetMaxDecodedFromUtf8Length(3, Base64Encoding.Standard));
        Assert.Equal(3, Base64.GetMaxDecodedFromUtf8Length(4, Base64Encoding.Standard));
        Assert.Equal(3, Base64.GetMaxDecodedFromUtf8Length(5, Base64Encoding.Standard));
        Assert.Equal(4, Base64.GetMaxDecodedFromUtf8Length(6, Base64Encoding.Standard));
        Assert.Equal(5, Base64.GetMaxDecodedFromUtf8Length(7, Base64Encoding.Standard));
        Assert.Equal(6, Base64.GetMaxDecodedFromUtf8Length(8, Base64Encoding.Standard));
        Assert.Equal(6, Base64.GetMaxDecodedFromUtf8Length(9, Base64Encoding.Standard));
    }

    private static string DotNetConvertToBase64Url(string text)
    {
        var bytes = Encoding.UTF8.GetBytes(text);
        return DotNetConvertToBase64Url(bytes);
    }

    private static string DotNetConvertToBase64Url(Span<byte> bytes)
    {
        var base64 = Convert.ToBase64String(bytes);
        return base64.Replace('+', '-').Replace('/', '_');
    }

    private static string EncodeUtf8ToString(string input, Base64Encoding encoding = Base64Encoding.Default)
    {
        var maxByteCount = Encoding.UTF8.GetMaxByteCount(input.Length);
        using var rental = new ArrayPoolRental(maxByteCount);
        var byteCount = Encoding.UTF8.GetBytes(input, rental.Array);
        return Base64.EncodeToString(rental.Array.AsSpan(0, byteCount), encoding);
    }
}
