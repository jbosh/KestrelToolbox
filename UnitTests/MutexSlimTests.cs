// <copyright file="MutexSlimTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Types;

namespace UnitTests;

public class MutexSlimTests
{
    private readonly ITestOutputHelper testOutputHelper;

    public MutexSlimTests(ITestOutputHelper testOutputHelper)
    {
        this.testOutputHelper = testOutputHelper;
    }

    private sealed class TestState : IDisposable
    {
        public MutexSlim Mutex { get; } = new();

        public long Counter { get; set; }

        public void Dispose()
        {
            this.Mutex.Dispose();
        }
    }

    [Fact]
    public void Basic()
    {
        using var mutex = new MutexSlim();

        using (mutex.Lock())
        {
            // Testing out lock `using` block.
        }

        Assert.True(mutex.TryWait());
        Assert.False(mutex.TryWait());
        mutex.Release();
    }

    [Fact]
    public void Exceptions()
    {
        var mutex = new MutexSlim();

        _ = Assert.Throws<InvalidOperationException>(mutex.Release);
        _ = Assert.Throws<InvalidOperationException>(mutex.Dispose);
    }

    [Theory]
    [InlineData(2, 20_000_000)]
    [InlineData(8, 2_000_000)]
    [InlineData(16, 1_000_000)]
    public void TestThreads(int numThreads, int iterations)
    {
        using var state = new TestState();
        var threads = new Thread[numThreads];
        for (var i = 0; i < threads.Length; i++)
        {
            // ReSharper disable once AccessToDisposedClosure
            threads[i] = new Thread(() => Run(state, iterations));
            threads[i].Start();
        }

        foreach (var thread in threads)
        {
            thread.Join();
        }

        Assert.Equal(iterations * threads.Length, state.Counter);

        static void Run(TestState state, int iterations)
        {
            for (var i = 0; i < iterations; i++)
            {
                _ = Thread.Yield();
                using (state.Mutex.Lock())
                {
                    state.Counter++;
                }
            }
        }
    }

    [Fact]
    public async Task Cancellation()
    {
        const int Iterations = 2_000_000;
        using var state = new TestState();
        using var cancellationToken = new CancellationTokenSource();
        var tasks = new Task[8];
        for (var i = 0; i < tasks.Length; i++)
        {
            // ReSharper disable once AccessToDisposedClosure
            tasks[i] = Task.Run(() => Run(state, Iterations, cancellationToken.Token));
        }

        cancellationToken.CancelAfter(500);

        try
        {
            await Task.WhenAll(tasks);
        }
        catch (OperationCanceledException)
        {
            // Success!
        }

        Assert.True(state.Counter <= Iterations * tasks.Length);

        static void Run(TestState state, int iterations, CancellationToken cancellationToken)
        {
            for (var i = 0; i < iterations; i++)
            {
                _ = Thread.Yield();
                using (state.Mutex.Lock(cancellationToken))
                {
                    state.Counter++;
                }
            }
        }
    }

    private sealed class TestStateDotNet : IDisposable
    {
        public SemaphoreSlim Semaphore { get; } = new(1, 1);

        public long Counter { get; set; }

        public void Dispose()
        {
            this.Semaphore.Dispose();
        }
    }

    [Benchmark]
    public void TestUncontendedPerformance()
    {
        const int Iterations = 100_000_000;
        const int NumThreads = 1;
        this.TestPerformance(NumThreads, Iterations);
    }

    [Benchmark]
    public void TestContendedPerformance()
    {
        const int Iterations = 300_000;
        const int NumThreads = 8;
        this.TestPerformance(NumThreads, Iterations);
    }

    private void TestPerformance(int numThreads, int numIterations)
    {
        for (var benchmarkIteration = 4; benchmarkIteration >= 0; benchmarkIteration--)
        {
            var dotnetTimer = Stopwatch.StartNew();
            using (var state = new TestStateDotNet())
            {
                var threads = new Thread[numThreads];
                for (var i = 0; i < threads.Length; i++)
                {
                    threads[i] = new Thread(() => Run(state, numIterations).Wait());
                    threads[i].Start();
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                Assert.Equal(numIterations * threads.Length, state.Counter);

                static async Task Run(TestStateDotNet state, int iterations)
                {
                    for (var i = 0; i < iterations; i++)
                    {
                        try
                        {
                            await state.Semaphore.WaitAsync();
                            state.Counter++;
                        }
                        finally
                        {
                            _ = state.Semaphore.Release();
                        }
                    }
                }
            }

            dotnetTimer.Stop();

            var lockerTimer = Stopwatch.StartNew();
            using (var state = new TestState())
            {
                var threads = new Thread[numThreads];
                for (var i = 0; i < threads.Length; i++)
                {
                    threads[i] = new Thread(() => Run(state, numIterations).Wait());
                    threads[i].Start();
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                Assert.Equal(numIterations * threads.Length, state.Counter);

                static async Task Run(TestState state, int iterations)
                {
                    for (var i = 0; i < iterations; i++)
                    {
                        using (await state.Mutex.LockAsync())
                        {
                            state.Counter++;
                        }
                    }
                }
            }

            lockerTimer.Stop();

            var noLockerPerformance = Stopwatch.StartNew();
            using (var state = new TestState())
            {
                var threads = new Thread[numThreads];
                for (var i = 0; i < threads.Length; i++)
                {
                    threads[i] = new Thread(() => Run(state, numIterations).Wait());
                    threads[i].Start();
                }

                foreach (var thread in threads)
                {
                    thread.Join();
                }

                Assert.Equal(numIterations * threads.Length, state.Counter);

                static async Task Run(TestState state, int iterations)
                {
                    for (var i = 0; i < iterations; i++)
                    {
                        try
                        {
                            await state.Mutex.WaitAsync();
                            state.Counter++;
                        }
                        finally
                        {
                            state.Mutex.Release();
                        }
                    }
                }
            }

            noLockerPerformance.Stop();

            if (benchmarkIteration == 0)
            {
                this.testOutputHelper.WriteLine(
                    $"Locker   : {lockerTimer.Elapsed:g} ({100.0 - (lockerTimer.Elapsed / dotnetTimer.Elapsed * 100.0):0.00}% faster)"
                );
                this.testOutputHelper.WriteLine(
                    $"No locker: {noLockerPerformance.Elapsed:g} ({100.0 - (noLockerPerformance.Elapsed / dotnetTimer.Elapsed * 100.0):0.00}% faster)"
                );
                this.testOutputHelper.WriteLine($"Dotnet   : {dotnetTimer.Elapsed:g}");

                Assert.True(lockerTimer.Elapsed < dotnetTimer.Elapsed, "KestrelToolbox was not faster than dotnet.");
            }
        }
    }
}
