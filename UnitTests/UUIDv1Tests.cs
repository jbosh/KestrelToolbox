// <copyright file="UUIDv1Tests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Types;

namespace UnitTests;

[SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Spec correct.")]
public class UUIDv1Tests
{
    [Fact]
    public async Task Basic()
    {
        var id = UUIDv1.NewUUIDv1();
        var time = (DateTimeOffset)id;
        Assert.True(time - DateTime.UtcNow <= TimeSpan.FromSeconds(30));
        Assert.Equal(1, id.Version);

        var id2 = UUIDv1.NewUUIDv1();
        Assert.NotEqual(id, id2);

        await Task.Delay(200);
        id2 = UUIDv1.NewUUIDv1();
        Assert.True(id < id2);
        Assert.True(id2 > id);
        Assert.True(id <= id2);
        Assert.True(id2 >= id);
        Assert.Equal(-1, id.CompareTo(id2));
        Assert.Equal(1, id2.CompareTo(id));
        Assert.Equal(0, id2.CompareTo(id2));
        Assert.True(id2.Equals(id2));

        id2 = UUIDv1.NewMinUUIDv1(id);
        Assert.Equal(id.ToDateTimeOffset(), id2.ToDateTimeOffset());
        Assert.True(id > id2);
        Assert.True(id2 < id);
        Assert.True(id >= id2);
        Assert.True(id2 <= id);
        Assert.Equal(1, id.CompareTo(id2));
        Assert.Equal(-1, id2.CompareTo(id));
        Assert.Equal(0, id2.CompareTo(id2));
        Assert.True(id2.Equals(id2));

        id2 = UUIDv1.Parse(id.ToString());
        Assert.Equal(id, id2);
        Assert.True(id.Equals(id2));

        Assert.True(UUIDv1.TryParse(id.ToString(), out var id3));
        Assert.Equal(id, id3);

        Assert.True(UUIDv1.TryParse(id.ToString().AsSpan(), out id2));
        Assert.Equal(id, id3);
        Assert.Equal(id2, id3);
    }

    [Fact]
    public void VersionNumber()
    {
        var guid = Guid.NewGuid();
        var uuid = (UUIDv1)guid;
        Assert.Equal(4, uuid.Version);
    }

    [Theory]
    [InlineData("2ee6558b-b1ca-11ec-83f1-6c67d142c9d9", "2022-04-01T14:44:07.0000011+00:00")]
    [InlineData("180c0111-0011-11bf-acc4-123456789012", "1981-06-29 07:00:00.7547153Z")]
    [InlineData("c8f0cc80-dd98-11d6-802b-73c555e65ed6", "2002-10-12 04:12:13Z")]
    public void Parsing(string test, string expectedDateString)
    {
        var guid = Guid.Parse(test);
        var id = UUIDv1.Parse(test);

        Assert.True(UUIDv1.TryParse(test, out var id2));
        Assert.Equal(id, id2);

        var expected = DateTimeOffset.Parse(expectedDateString, CultureInfo.InvariantCulture);
        var date = (DateTimeOffset)id;

        Assert.Equal(test, id.ToString());
        Assert.Equal(guid.ToString(), id.ToString());
        Assert.Equal((UUIDv1)guid, id);
        Assert.Equal(expected, date);

        var expectedBytes = guid.ToByteArray();
        var bytes = id.ToByteArray();

        Assert.Equal(expectedBytes, bytes);
        Assert.Equal(id, new UUIDv1(bytes));
        Assert.Equal(id, new UUIDv1(bytes, 0));
        Assert.Equal(id, new UUIDv1(bytes.AsMemory()));
    }

    [Fact]
    public void ConstructionFromInts()
    {
        var a = 403439889;
        var b = (short)17;
        var c = (short)4543;
        var d = (byte)172;
        var e = (byte)196;
        var f = (byte)18;
        var g = (byte)52;
        var h = (byte)86;
        var i = (byte)120;
        var j = (byte)144;
        var k = (byte)18;
        var guid = new Guid(a, b, c, d, e, f, g, h, i, j, k);
        var uuid = new UUIDv1(a, b, c, d, e, f, g, h, i, j, k);

        Assert.Equal(guid, (Guid)uuid);

        guid = new Guid(a, b, c, new[] { d, e, f, g, h, i, j, k });
        uuid = new UUIDv1(a, b, c, new[] { d, e, f, g, h, i, j, k });

        Assert.Equal(guid, (Guid)uuid);

        var ex = Assert.Throws<ArgumentException>(() => new UUIDv1(a, b, c, new[] { d, e }));
        Assert.Equal("d must be exactly 8 bytes. (Parameter 'd')", ex.Message);
        Assert.Equal("d", ex.ParamName);
    }

    [Theory]
    [InlineData("c8f0cc80-dd98-11d6-802b", "Unrecognized Guid format.")]
    [InlineData(
        "c8f0cc80-dd98-11d6-*&99a-73c555e65ed6",
        "Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)."
    )]
    [InlineData("hello world", "Unrecognized Guid format.")]
    [InlineData(
        "c8f0cc80 dd98 11d6 802b 73c555e65ed6",
        "Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)."
    )]
    [InlineData("e3ecb1c8-339c-4556-8e16-45ddfcbf3ec6", "UUIDv1 requires version 1. Was version 4.")]
    public void Failure(string test, string errorMessage)
    {
        Assert.False(UUIDv1.TryParse(test, out var id));
        Assert.False(UUIDv1.TryParse(test.AsSpan(), out id));
        Assert.Equal(UUIDv1.Empty, id);
        var ex = Assert.Throws<FormatException>(() => UUIDv1.Parse(test));
        Assert.Equal(errorMessage, ex.Message);
    }

    [Fact]
    public void FailureNull()
    {
        Assert.False(UUIDv1.TryParse(null, out var id));
        Assert.Equal(id, UUIDv1.Empty);
    }

    [Theory]
    [InlineData("1981-06-29 07:00:00Z")]
    [InlineData("1967-05-19 14:12:30Z")]
    public void DateTimeOffsetTest(string expectedDateString)
    {
        var expected = DateTimeOffset.Parse(expectedDateString, CultureInfo.InvariantCulture);
        var id = UUIDv1.NewUUIDv1(expected);
        var actual = id.ToDateTimeOffset();
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void Exceptions()
    {
        var ex = Assert.Throws<ArgumentOutOfRangeException>(() => new UUIDv1(Array.Empty<byte>()));
        Assert.Equal("0", ex.ActualValue);
        Assert.Equal(
            "buffer must be larger than 16 bytes. (Parameter 'buffer')\nActual value was 0.",
            ex.Message?.ReplaceLineEndings("\n")
        );
        Assert.Equal("buffer", ex.ParamName);
    }

    [Theory]
    [InlineData("00112233-4455-6677-8899-AABBCCDDEEFF", "33221100-5544-7766-8899-AABBCCDDEEFF")]
    [InlineData("2ee6558b-b1ca-11ec-83f1-6c67d142c9d9", "8b55e62e-cab1-ec11-83f1-6c67d142c9d9")]
    [InlineData("180c0111-0011-11bf-acc4-123456789012", "11010c18-1100-bf11-acc4-123456789012")]
    [InlineData("c8f0cc80-dd98-11d6-802b-73c555e65ed6", "80ccf0c8-98dd-d611-802b-73c555e65ed6")]
    public void Endian(string nativeString, string expectedString)
    {
        // Use guid to parse because we don't care about version in these tests and UUIDv1 will throw.
        var native = (UUIDv1)Guid.Parse(nativeString);
        var expected = (UUIDv1)Guid.Parse(expectedString);

        var actual = UUIDv1.SwapEndian(native);
        Assert.Equal(expected, actual);

        Span<byte> bytes = stackalloc byte[UUIDv1.SizeInBytes];
        Assert.True(expected.TryWriteBytesBigEndian(bytes));
        var bigEndianBytes = expected.ToByteArrayBigEndian();
        actual = UUIDv1.CreateFromBytesBigEndian(bytes);
        Assert.Equal(expected, actual);
        Assert.Equal(bytes.ToArray(), bigEndianBytes);

        Assert.True(expected.TryWriteBytesLittleEndian(bytes));
        var littleEndianBytes = expected.ToByteArrayLittleEndian();
        actual = UUIDv1.CreateFromBytesLittleEndian(bytes);
        Assert.Equal(expected, actual);
        Assert.Equal(bytes.ToArray(), littleEndianBytes);

        Assert.True(expected.TryWriteBytes(bytes));
        var nativeEndianBytes = expected.ToByteArray();
        actual = UUIDv1.CreateFromBytes(bytes);
        Assert.Equal(expected, actual);
        Assert.Equal(bytes.ToArray(), nativeEndianBytes);

        if (BitConverter.IsLittleEndian)
        {
            Assert.Equal(littleEndianBytes, nativeEndianBytes);
            Assert.NotEqual(bigEndianBytes, nativeEndianBytes);
        }
        else
        {
            Assert.Equal(bigEndianBytes, nativeEndianBytes);
            Assert.NotEqual(littleEndianBytes, nativeEndianBytes);
        }
    }

    [SuppressMessage("Style", "IDE0250:Make struct \'readonly\'", Justification = "Testing purity.")]
    private struct PureTest
    {
        public readonly UUIDv1 Value;

        public PureTest(UUIDv1 value)
        {
            this.Value = value;
        }
    }

    [Fact]
    public void Pure()
    {
        var test = new PureTest(UUIDv1.NewUUIDv1());
        Assert.True(test.Value.TryWriteBytes(new byte[16]));
    }
}
