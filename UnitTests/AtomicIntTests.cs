// <copyright file="AtomicIntTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Types.Atomic;

namespace UnitTests;

public class AtomicIntTests
{
    [Fact]
    public void Basic()
    {
        var value = new AtomicInt(22);
        Assert.Equal(22, value.Load());
        value = new AtomicInt();
        Assert.Equal(0, value.Load());
        value.Store(33);
        Assert.Equal(33, value.Load());
        Assert.Equal(33, value.Exchange(47));
        Assert.Equal(47, value.Load());

        Assert.False(value.CompareExchange(1, 86));

        var expected = 0;
        Assert.False(value.CompareExchange(ref expected, 86));
        Assert.Equal(47, expected);
        Assert.True(value.CompareExchange(ref expected, 86));
        Assert.Equal(47, expected);
        Assert.False(value.CompareExchange(ref expected, 113));
        Assert.Equal(86, expected);
        Assert.True(value.CompareExchange(ref expected, 0));
        Assert.Equal(86, expected);

        Assert.Equal(0, value.FetchAdd(66));
        Assert.Equal(66, value.FetchAdd(12));
        Assert.Equal(78, value.FetchSub(467));
        Assert.Equal(-389, value.FetchSub(-54));
        Assert.Equal(-335, value.Increment());
        Assert.Equal(-334, value.Decrement());

        value.Store(0);
        Assert.Equal(0, value.Load());
        Assert.Equal(0, value.FetchOr(2));
        Assert.Equal(2, value.FetchOr(4));
        Assert.Equal(6, value.FetchOr(2));
        Assert.Equal(6, value.Load());

        Assert.Equal(6, value.FetchAnd(2));
        Assert.Equal(2, value.FetchAnd(2));
        Assert.Equal(2, value.Load());

        value.Store(int.MaxValue);
        Assert.Equal(int.MaxValue, value.Decrement());
        Assert.Equal(int.MaxValue - 1, value.Increment());
        Assert.Equal(int.MaxValue, value.Increment());
        Assert.Equal(int.MinValue, value.Load());
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringBasic()
    {
        var positiveValue = 1679;
        var negativeValue = -3045;

        var positiveAtomic = new AtomicInt(positiveValue);
        var negativeAtomic = new AtomicInt(negativeValue);

        Assert.Equal(positiveValue.ToString(), positiveAtomic.ToString());
        Assert.Equal(negativeValue.ToString(), negativeAtomic.ToString());
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringFormat()
    {
        var positiveValue = 1679;
        var negativeValue = -3045;

        var positiveAtomic = new AtomicInt(positiveValue);
        var negativeAtomic = new AtomicInt(negativeValue);

        var specifiers = new[] { "G", "C", "D8", "E2", "F", "N", "N0", "P", "X8" };

        foreach (var specifier in specifiers)
        {
            Assert.Equal(positiveValue.ToString(specifier), positiveAtomic.ToString(specifier));
            Assert.Equal(negativeValue.ToString(specifier), negativeAtomic.ToString(specifier));
        }
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringProvider()
    {
        var english = CultureInfo.CreateSpecificCulture("en-US");
        var french = CultureInfo.CreateSpecificCulture("fr-FR");
        var spanish = CultureInfo.CreateSpecificCulture("es-ES");

        var positiveAtomic = new AtomicInt(1679);
        var negativeAtomic = new AtomicInt(-3045);

        Assert.Equal("1679", positiveAtomic.ToString(english));
        Assert.Equal("1679", positiveAtomic.ToString(french));
        Assert.Equal("1679", positiveAtomic.ToString(spanish));

        Assert.Equal("-3045", negativeAtomic.ToString(english));
        Assert.Equal("-3045", negativeAtomic.ToString(french));
        Assert.Equal("-3045", negativeAtomic.ToString(spanish));
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringFormatAndProvider()
    {
        var positiveValue = 1679;
        var negativeValue = -3045;

        var positiveAtomic = new AtomicInt(positiveValue);
        var negativeAtomic = new AtomicInt(negativeValue);

        var cultures = new[]
        {
            CultureInfo.CreateSpecificCulture("en-US"),
            CultureInfo.CreateSpecificCulture("fr-FR"),
            CultureInfo.CreateSpecificCulture("es-ES"),
        };

        var specifiers = new[] { "G", "C", "D8", "E2", "F", "N", "N0", "P", "X8" };
        foreach (var specifier in specifiers)
        {
            foreach (var culture in cultures)
            {
                Assert.Equal(positiveValue.ToString(specifier, culture), positiveAtomic.ToString(specifier, culture));
                Assert.Equal(negativeValue.ToString(specifier, culture), negativeAtomic.ToString(specifier, culture));
            }
        }
    }

    [Fact]
    [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global", Justification = "That's what we're testing.")]
    public void EqualsTest()
    {
        var a = new AtomicInt();
        var b = new AtomicInt();
        Assert.True(a.Equals(b));
        a.Store(2);
        Assert.False(a.Equals(b));
        b.Store(2);
        Assert.True(a.Equals(b));

        Assert.False(a.Equals(null));
        Assert.False(a.Equals(new AtomicBool()));

        Assert.Equal(a.Load().GetHashCode(), a.GetHashCode());
    }
}
