// <copyright file="BenchmarkAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace UnitTests;

/// <summary>
/// Attribute to signify a test is a benchmark and should only run when the debugger is attached.
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class BenchmarkAttribute : FactAttribute
{
    public BenchmarkAttribute()
    {
        if (!Debugger.IsAttached)
        {
            this.Skip = "Only running in interactive mode.";
        }
    }
}
