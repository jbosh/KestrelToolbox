// <copyright file="StringExtensionsTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Extensions.StringExtensions;

namespace UnitTests;

public class StringExtensionsTests
{
    [Fact]
    public void Basic()
    {
        TestString(string.Empty, ':');
        TestString(string.Empty, new[] { ':', '+' });
        TestString(string.Empty, ':', StringSplitOptions.RemoveEmptyEntries);
        TestString(string.Empty, new[] { ':', '+' }, StringSplitOptions.RemoveEmptyEntries);

        TestString("h/m//s", '/');
        TestString("h/m//s", '/', StringSplitOptions.RemoveEmptyEntries);
        TestString("h/m//s", '/', StringSplitOptions.RemoveEmptyEntries);

        TestString("h:m//s", new[] { '/', ':' });
        TestString("h:m//s", new[] { '/', ':' }, StringSplitOptions.RemoveEmptyEntries);

        TestString("h:m+asdfa+:/asdf+s", new[] { '/', ':', '+' });
        TestString("h:m+asdfa+:/asdf+s", new[] { '/', ':', '+' }, StringSplitOptions.RemoveEmptyEntries);

        for (var count = 0; count < 4; count++)
        {
            TestString("h:m+asdfa+:/asdf+s", new[] { '/', ':', '+' }, count);
            TestString("h:m+asdfa+:/asdf+s", new[] { '/', ':', '+' }, count, StringSplitOptions.RemoveEmptyEntries);

            TestString("h/m/asdfa///asdf/s", '/', count);
            TestString("h/m/asdfa///asdf/s", '/', count, StringSplitOptions.RemoveEmptyEntries);
        }
    }

    private static void TestString(string s, char separator, StringSplitOptions options = StringSplitOptions.None) =>
        TestString(s, separator, int.MaxValue, options);

    private static void TestString(
        string s,
        char separator,
        int count,
        StringSplitOptions options = StringSplitOptions.None
    )
    {
        var result = s.SplitAndEnumerate(separator, count, options).Select(m => new string(m.Span)).ToArray();
        var expected = s.Split(separator, count, options);
        Assert.Equal(expected, result);
    }

    private static void TestString(string s, char[] separator, StringSplitOptions options = StringSplitOptions.None) =>
        TestString(s, separator, int.MaxValue, options);

    private static void TestString(
        string s,
        char[] separator,
        int count,
        StringSplitOptions options = StringSplitOptions.None
    )
    {
        var result = s.SplitAndEnumerate(separator, count, options).Select(m => new string(m.Span)).ToArray();
        var expected = s.Split(separator, count, options);
        Assert.Equal(expected, result);
    }
}
