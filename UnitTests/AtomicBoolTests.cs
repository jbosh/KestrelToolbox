// <copyright file="AtomicBoolTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Types.Atomic;

namespace UnitTests;

public class AtomicBoolTests
{
    [Fact]
    public void Basic()
    {
        var value = new AtomicBool(true);
        Assert.True(value.Load());
        value = new AtomicBool();
        Assert.False(value.Load());
        value.Store(true);
        Assert.True(value.Load());
        Assert.True(value.Exchange(false));
        Assert.False(value.Load());
        value.Store(false);
        Assert.False(value.Load());
        Assert.False(value.Exchange(true));
        Assert.True(value.Load());

        Assert.False(value.CompareExchange(false, true));
        Assert.True(value.CompareExchange(true, false));
        Assert.False(value.CompareExchange(true, false));
        Assert.True(value.CompareExchange(false, true));
        Assert.True(value.CompareExchange(true, false));

        var expected = true;
        Assert.False(value.CompareExchange(ref expected, false));
        Assert.False(expected);
        Assert.True(value.CompareExchange(ref expected, true));
        Assert.False(expected);
        Assert.False(value.CompareExchange(ref expected, false));
        Assert.True(expected);
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringBasic()
    {
        var positiveValue = true;
        var negativeValue = false;

        var positiveAtomic = new AtomicBool(positiveValue);
        var negativeAtomic = new AtomicBool(negativeValue);

        Assert.Equal(positiveValue.ToString(), positiveAtomic.ToString());
        Assert.Equal(negativeValue.ToString(), negativeAtomic.ToString());

        Assert.Equal(positiveValue.ToString(null), positiveAtomic.ToString(null));
        Assert.Equal(negativeValue.ToString(null), negativeAtomic.ToString(null));
    }

    [Fact]
    [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global", Justification = "That's what we're testing.")]
    public void EqualsTest()
    {
        var a = new AtomicBool();
        var b = new AtomicBool();
        Assert.True(a.Equals(b));
        a.Store(true);
        Assert.False(a.Equals(b));
        b.Store(true);
        Assert.True(a.Equals(b));

        Assert.False(a.Equals(null));
        Assert.False(a.Equals(new AtomicInt()));

        Assert.Equal(a.Load().GetHashCode(), a.GetHashCode());
    }
}
