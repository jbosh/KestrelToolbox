// <copyright file="SimpleSpinLockTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Internal;

namespace UnitTests;

[SuppressMessage(
    "Major Code Smell",
    "S2925:\"Thread.Sleep\" should not be used in tests",
    Justification = "Lock tests need some waits to simulate."
)]
public sealed class SimpleSpinLockTests
{
    private sealed class LockedInt
    {
        private SimpleSpinLock spinLock;

        public int Value { get; set; }

        public LockedInt()
            : this(0) { }

        public LockedInt(int value)
        {
            this.spinLock = default;
            this.Value = value;
        }

        public void Increment()
        {
            this.spinLock.Enter();
            this.Value++;
            this.spinLock.Exit();
        }
    }

    [Fact]
    public void Basic()
    {
        const int k_iterations = 100_000;
        var value = new LockedInt();

        var threads = new Thread[8];
        for (var i = 0; i < threads.Length; i++)
        {
            threads[i] = new Thread(() => Increment(value));
            threads[i].Start();
        }

        for (var i = 0; i < threads.Length; i++)
        {
            threads[i].Join();
        }

        Assert.Equal(k_iterations * threads.Length, value.Value);

        static void Increment(LockedInt value)
        {
            for (var iteration = 0; iteration < k_iterations; iteration++)
            {
                value.Increment();
                Thread.Sleep(0);
            }
        }
    }
}
