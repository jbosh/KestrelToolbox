// <copyright file="AtomicUIntTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using KestrelToolbox.Types.Atomic;

namespace UnitTests;

public class AtomicUIntTests
{
    [Fact]
    public void Basic()
    {
        var value = new AtomicUInt(22);
        Assert.Equal(22U, value.Load());
        value = new AtomicUInt();
        Assert.Equal(0U, value.Load());
        value.Store(33);
        Assert.Equal(33U, value.Load());
        Assert.Equal(33U, value.Exchange(47));
        Assert.Equal(47U, value.Load());

        Assert.False(value.CompareExchange(1, 86));

        var expected = 0U;
        Assert.False(value.CompareExchange(ref expected, 86));
        Assert.Equal(47U, expected);
        Assert.True(value.CompareExchange(ref expected, 86));
        Assert.Equal(47U, expected);
        Assert.False(value.CompareExchange(ref expected, 113));
        Assert.Equal(86U, expected);
        Assert.True(value.CompareExchange(ref expected, 0));
        Assert.Equal(86U, expected);

        Assert.Equal(0U, value.FetchAdd(66));
        Assert.Equal(66U, value.FetchAdd(12));
        Assert.Equal(78U, value.FetchSub(467));
        Assert.Equal(0xFFFFFE7B, value.FetchSub(0xFFFFFFCA));
        Assert.Equal(0xFFFFFEB1, value.Increment());
        Assert.Equal(0xFFFFFEB2, value.Decrement());

        value.Store(0);
        Assert.Equal(0U, value.Load());
        Assert.Equal(0U, value.FetchOr(2));
        Assert.Equal(2U, value.FetchOr(4));
        Assert.Equal(6U, value.FetchOr(2));
        Assert.Equal(6U, value.Load());

        Assert.Equal(6U, value.FetchAnd(2));
        Assert.Equal(2U, value.FetchAnd(2));
        Assert.Equal(2U, value.Load());

        value.Store(uint.MaxValue);
        Assert.Equal(uint.MaxValue, value.Decrement());
        Assert.Equal(uint.MaxValue - 1, value.Increment());
        Assert.Equal(uint.MaxValue, value.Increment());
        Assert.Equal(uint.MinValue, value.Load());
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringBasic()
    {
        var positiveValue = 1679U;
        var negativeValue = 0xFFFFF41BU;

        var positiveAtomic = new AtomicUInt(positiveValue);
        var negativeAtomic = new AtomicUInt(negativeValue);

        Assert.Equal(positiveValue.ToString(), positiveAtomic.ToString());
        Assert.Equal(negativeValue.ToString(), negativeAtomic.ToString());
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringFormat()
    {
        var positiveValue = 1679U;
        var negativeValue = 0xFFFFF41BU;

        var positiveAtomic = new AtomicUInt(positiveValue);
        var negativeAtomic = new AtomicUInt(negativeValue);

        var specifiers = new[] { "G", "C", "D8", "E2", "F", "N", "N0", "P", "X8" };

        foreach (var specifier in specifiers)
        {
            Assert.Equal(positiveValue.ToString(specifier), positiveAtomic.ToString(specifier));
            Assert.Equal(negativeValue.ToString(specifier), negativeAtomic.ToString(specifier));
        }
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringProvider()
    {
        var english = CultureInfo.CreateSpecificCulture("en-US");
        var french = CultureInfo.CreateSpecificCulture("fr-FR");
        var spanish = CultureInfo.CreateSpecificCulture("es-ES");

        var positiveAtomic = new AtomicUInt(1679);
        var negativeAtomic = new AtomicUInt(0xFFFFF41BU);

        Assert.Equal("1679", positiveAtomic.ToString(english));
        Assert.Equal("1679", positiveAtomic.ToString(french));
        Assert.Equal("1679", positiveAtomic.ToString(spanish));

        Assert.Equal("4294964251", negativeAtomic.ToString(english));
        Assert.Equal("4294964251", negativeAtomic.ToString(french));
        Assert.Equal("4294964251", negativeAtomic.ToString(spanish));
    }

    [Fact]
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Testing exactly this.")]
    public void ToStringFormatAndProvider()
    {
        var positiveValue = 1679U;
        var negativeValue = 0xFFFFF41BU;

        var positiveAtomic = new AtomicUInt(positiveValue);
        var negativeAtomic = new AtomicUInt(negativeValue);

        var cultures = new[]
        {
            CultureInfo.CreateSpecificCulture("en-US"),
            CultureInfo.CreateSpecificCulture("fr-FR"),
            CultureInfo.CreateSpecificCulture("es-ES"),
        };

        var specifiers = new[] { "G", "C", "D8", "E2", "F", "N", "N0", "P", "X8" };
        foreach (var specifier in specifiers)
        {
            foreach (var culture in cultures)
            {
                Assert.Equal(positiveValue.ToString(specifier, culture), positiveAtomic.ToString(specifier, culture));
                Assert.Equal(negativeValue.ToString(specifier, culture), negativeAtomic.ToString(specifier, culture));
            }
        }
    }

    [Fact]
    [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global", Justification = "That's what we're testing.")]
    public void EqualsTest()
    {
        var a = new AtomicUInt();
        var b = new AtomicUInt();
        Assert.True(a.Equals(b));
        a.Store(2);
        Assert.False(a.Equals(b));
        b.Store(2);
        Assert.True(a.Equals(b));

        Assert.False(a.Equals(null));
        Assert.False(a.Equals(new AtomicBool()));

        Assert.Equal(a.Load().GetHashCode(), a.GetHashCode());
    }
}
