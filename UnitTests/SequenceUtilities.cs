// <copyright file="SequenceUtilities.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.Text;

namespace UnitTests;

public static class SequenceUtilities
{
    public static ReadOnlySequence<byte> CreateSequence(params string[] strings)
    {
        var segments = new List<ReadOnlySequenceSegment<byte>>(strings.Length);
        var lastSegment = default(Segment<byte>?);
        for (var i = 0; i < strings.Length; i++)
        {
            var array = Encoding.ASCII.GetBytes(strings[i]);
            var segment = new Segment<byte>(array);
            lastSegment?.SetNext(segment);
            lastSegment = segment;
            segments.Add(segment);
        }

        return new ReadOnlySequence<byte>(segments[0], 0, segments[^1], segments[^1].Memory.Length);
    }

    public static ReadOnlySequence<byte> SplitIntoSequence(string s, int sizePerString)
    {
        var strings = new List<string>();
        for (var i = 0; i < s.Length; i += sizePerString)
        {
            var end = Math.Min(s.Length, i + sizePerString);
            strings.Add(s.Substring(i, end - i));
        }

        return CreateSequence(strings.ToArray());
    }

    private sealed class Segment<T> : ReadOnlySequenceSegment<T>
    {
        public Segment(ReadOnlyMemory<T> memory)
        {
            this.Memory = memory;
        }

        public void SetNext(Segment<T>? segment)
        {
            this.Next = segment;
            if (segment != null)
            {
                segment.RunningIndex = this.RunningIndex + this.Memory.Length;
            }
        }
    }
}
