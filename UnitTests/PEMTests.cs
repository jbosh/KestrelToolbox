// <copyright file="PEMTests.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using KestrelToolbox.Utilities;

namespace UnitTests;

public class PEMTests
{
    private const int RSAKeyLength = 2048;

    private const string RSAKey = """
        -----BEGIN RSA PRIVATE KEY-----
        MIIEpAIBAAKCAQEArm3paCNVtl3me+UdcXpGsYiVUfB1sZ+31rSlYzjJ7RmQcM5o
        k0Ehx0PSKRpa1D00IjioTzwwgM+z4NF8A7Wv1O1txCztM47JWzYgAsSKX5aPtuzM
        7Z5HkbbjzdCZ5ox1vha4tGe13LRlmug9o8HaToyYzrAkD819844HUy+Ow6rurxbm
        Me8ULuZXA/BAuFA+IAhZWlM8nxvGqqDADPGYXpPSXhjUrX/VAXiUlbQgobWkuC4e
        haNw8muPs4rSk513M1XxMork48PX2plsqB8trJ0mVOjbsdL41EBgnaGDiU53hrTZ
        cgrh05JsJsvsYoYGGLSDGZjs9mSRaxgN3yLkuwIDAQABAoIBAQCnshXnP6lk9evk
        C0b+VT9n6Vcdet+8EEDvU6d5uBwyLhS2z0qs375409RWHfda+yiHCg+OdeBvxsA6
        EUx4SJGuVUHljAfUPJ+LaILoBkq+UYMDq6/tdvF5vupOchG0HDAKu8+B6f9nG6xf
        fI3eRT37rKIwfBl/c+F2aYLTT/KiW7mr5qHiJmvMnqXuMrbRKTqAxNIEY9hOjlne
        jpOad65ySFkVIEfcS6c+Z4KAe1bpEImQk6z33LsPcRIgru2txsWu5JIlUHzu/jBP
        HPiVQ1qqiDzSc/Rohy9loK/brtQ2EraEWEYCiGJ8AYiUhBYH+HWld6X14P6yAP7P
        sAN3xFghAoGBAOAsIXmg9Pee3qxSnuuZgb7qCJT//L9U0d8DDZJtfI61XCGIxquw
        xVveJmI7UZ7w6gjZEZwrhpaSamJVyspDtLS7dc4WoaMyUAW1qqi9YA5/BCWBUTsw
        zMSNVqf5WOyiZHVMLRfw5JGtOWX6+FZg9h/FwNqxhGCsdsMrF2v/c7arAoGBAMcx
        zFfzioMiSAXpn/Cpg7v5RYWr52lDB4iNJrHCytM7j5wCHlkoAoh4vlKdF6nu8iPj
        tKfY/ZFj0bsT8/C+X5LB4PQi/Y8BvuigvpyDPalSa+CuVtSFafn+UoV3WxCFeodW
        cbmBUZalYfj8hqRWEkSaZi9h+DntSORDzVgLjsoxAoGAR1QjbqUVbTkC10gOwx58
        Fzg2dNqjHGorwU8iYWYvDOWOJCl2NakwkxOzmOwRA4baoOgY311GHOEHsrRjq0ox
        1JQVXvb6hYf7yGVOoRRo3RPzSM/F/hSAA0aJvTbs4ZamamBLtON0hxncOqPJowqF
        JgTxYWS8n1ohC+bJNBb7RbMCgYEAwufpKI32lEVw78gfZZ6Ydc98xUTlU8vwjR6A
        JCOTzohTBrXT+Cz7/YkXwUJRK0fQymk+FZZu1lozl7f4UzIT1ke602eBbvKoWHsg
        x4hrR0iMofY0pBI/FgxRtqKJCmtw7wgYkwkESkFc0gJVxGsryNDC984G0rdpQWFO
        hb4ZY6ECgYBLmCBXhaUozFO5NsO/9Pga4SXycSzwzJ8/rzf0/tDne1lyqbRaCzvD
        ki2mAvjeulFmSLqnR27OgeY6X9HP85kp6xs5OaX9gIRNzKWBc9GYK+lgM8vZMdXS
        NiUUTKT1rvctFJhaWisJFS6xiMpc0WYkqdec52f7I6sozLowyCy1MQ==
        -----END RSA PRIVATE KEY-----
        """;

    private const int Pkcs8KeyLength = 1024;

    private const string Pkcs8Key = """
        -----BEGIN PRIVATE KEY-----
        MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAL9sSNe2hUJGuUWh
        b91QweBwGlWtMdCz712oun32vFbMwtY4gNDR9Ra71M5qxFGjyPdww0S0aHBgmO7+
        4n3AgDf7Dj+KNMZFycVlbMB/h2CxB/MJhkIMv1PIP3BYbaLeoJSHBQ+meY8OfQK1
        ctd+tB7TjeM82jNqzBlDV9zkHrE3AgMBAAECgYARrgmoSsZSEcAo6zkuwre1aAU2
        ppF0Sjowd1YKLlDE35KbQIq9PrcZS23jRr8tHyh/mh2e6d6/ilx1rnryzGXLHIuX
        pEgssPRuGhAozGLrkv+8ltlUa6YjaM2/iXaLs8mUjJndFYdcnngE7HMZ9gFRX4+W
        ncMjCrlJC2yaxZvWaQJBAP7bzmcgQl5rHGVoK57aglcFT9VtX05JXToI0sz8UjeL
        qDsDyqjE425MNHRVgBmKJNJvL/Zt0FgMW3suhGoeVvsCQQDAR7/vMb2rsGkINjJ7
        GWTufPdtMkALHVB5Gjjrp+iOKL6HMqOKsSdh6IKdT/26tgkGBw0MaGCgSkLOTxAP
        0en1AkBXC9xDfNEuToc0W5AKgbdi4X2S9/hFVOlymPDm8odiUWD0RydA3X6QfZas
        5UcLchOBjFhlYrx2VkIooIq5dANhAkA1yBvkcLpHupk2RcqK6gh0OJoQffyx3yoy
        JWaEa7u9f6IDOOPLmE+WsC1QiR5ESkRt3i0FKnDUhND42d55PlK9AkBPEU3PZ50o
        yqvywDhKtnxm1HgcGk43KdQR5u2tzVsgK3L+NXKQgDgdy8F21fWTZqAYp9odKph8
        n7RpaHLrKMQc
        -----END PRIVATE KEY-----
        """;

    [Fact]
    public void PrivateKeys()
    {
        using (var rsa = PEM.ReadPrivateKey(RSAKey))
        {
            Assert.Equal(RSAKeyLength, rsa.KeySize);
            Assert.Equal("RSA", rsa.KeyExchangeAlgorithm);
        }

        using (var rsa = PEM.ReadPrivateKey(Pkcs8Key))
        {
            Assert.Equal(Pkcs8KeyLength, rsa.KeySize);
            Assert.Equal("RSA", rsa.KeyExchangeAlgorithm);
        }
    }

    [Fact]
    public void Certificates()
    {
        const string Cert = """
            -----BEGIN CERTIFICATE-----
            MIICUTCCAfugAwIBAgIBADANBgkqhkiG9w0BAQQFADBXMQswCQYDVQQGEwJDTjEL
            MAkGA1UECBMCUE4xCzAJBgNVBAcTAkNOMQswCQYDVQQKEwJPTjELMAkGA1UECxMC
            VU4xFDASBgNVBAMTC0hlcm9uZyBZYW5nMB4XDTA1MDcxNTIxMTk0N1oXDTA1MDgx
            NDIxMTk0N1owVzELMAkGA1UEBhMCQ04xCzAJBgNVBAgTAlBOMQswCQYDVQQHEwJD
            TjELMAkGA1UEChMCT04xCzAJBgNVBAsTAlVOMRQwEgYDVQQDEwtIZXJvbmcgWWFu
            ZzBcMA0GCSqGSIb3DQEBAQUAA0sAMEgCQQCp5hnG7ogBhtlynpOS21cBewKE/B7j
            V14qeyslnr26xZUsSVko36ZnhiaO/zbMOoRcKK9vEcgMtcLFuQTWDl3RAgMBAAGj
            gbEwga4wHQYDVR0OBBYEFFXI70krXeQDxZgbaCQoR4jUDncEMH8GA1UdIwR4MHaA
            FFXI70krXeQDxZgbaCQoR4jUDncEoVukWTBXMQswCQYDVQQGEwJDTjELMAkGA1UE
            CBMCUE4xCzAJBgNVBAcTAkNOMQswCQYDVQQKEwJPTjELMAkGA1UECxMCVU4xFDAS
            BgNVBAMTC0hlcm9uZyBZYW5nggEAMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEE
            BQADQQA/ugzBrjjK9jcWnDVfGHlk3icNRq0oV7Ri32z/+HQX67aRfgZu7KWdI+Ju
            Wm7DCfrPNGVwFWUQOmsPue9rZBgO
            -----END CERTIFICATE-----
            -----BEGIN CERTIFICATE-----
            MIICZjCCAc+gAwIBAgIUWTa9MFAed5y1MDh3/0kZc30tJm8wDQYJKoZIhvcNAQEL
            BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
            GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMTA5MDEyMTAxMzRaFw0zMTA4
            MzAyMTAxMzRaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
            HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwgZ8wDQYJKoZIhvcNAQEB
            BQADgY0AMIGJAoGBAJP9ljDE4BG8owq/g+5YPdAa7bLq1OLohNu+CjMuZKS1VfY6
            DPo1r4QUlA1A64Wf+w9qleYNzyOXLcJuf3teiRmZwLsazZJuxpbq33Fil553Y7C9
            o4r/ND0e4XUDNE124e/Rmdn3ng3U9sxGIoT+9V+cQEvfZ0hMUnD/BjU08CExAgMB
            AAGjUzBRMB0GA1UdDgQWBBSi+X+Tl0vsLuanqwCbA4qTYIM0ozAfBgNVHSMEGDAW
            gBSi+X+Tl0vsLuanqwCbA4qTYIM0ozAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3
            DQEBCwUAA4GBAHc1tbqeUc0Gs3utFeiQurBGdQEiUeS1amUOAoJYfJcu5gntqbKY
            /r3qV48YiYsi244P1d3SQ3medMo/qxONNofz8HgcjizPT8aU0YfKkWMKDRNQjDwH
            zO3r0rCp1P6j1ShlLYFgu8sEc9fN2MWTSlah+ZMsIgMUrAXkTIV5rgxN
            -----END CERTIFICATE-----
            """;

        var certs = PEM.ReadCerts(Cert).ToArray();
        Assert.Equal(2, certs.Length);
        Assert.Equal("00", certs[0].SerialNumber);
        Assert.Equal("5936BD30501E779CB5303877FF4919737D2D266F", certs[1].SerialNumber);
    }

    [Fact]
    public void KeyCertPair()
    {
        const string Cert = """
            -----BEGIN CERTIFICATE-----
            MIIFazCCA1OgAwIBAgIUMf85CPtme3bElYsc4/zmheTGNJMwDQYJKoZIhvcNAQEL
            BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
            GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMTA5MDIxOTI0NDVaFw0yMjA5
            MDIxOTI0NDVaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
            HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggIiMA0GCSqGSIb3DQEB
            AQUAA4ICDwAwggIKAoICAQC6MeLNtO4OgcIB5/1ItLYGES4CDDiCUYhf/Ku5nuMu
            q5OGqrC52poIxiJW3Kn7KlBj3ZclkegC8DY/j8vjH7OHDSm+Fkh8TN4YQT8xBoJb
            qykbsKqoPWovYaJ4rrqEq5EVo9NL4GYYMdu4DN+xAbT1d223FwdijkICpU9fzMfm
            YF/QrWUxGEMpHGYul3SVUQYOej2DXQj2waeDmJNp5Um00dP8AWyfzbbkgobxOJZI
            1tU2nFju4lYWAN8JxFLL4vA/Kf+ZGMvlK2M7Jqu+Z9HFzqPUv77wb7iSmPPPMmJp
            XmZ1rfjtPaqIqlCftOBqbmwcs9GYTjwFzI/1ax6+qZFSWSUvRSuzyhqtx007BVdl
            U+mkH0lkBDiC27biPyORdj13KVDn5FcFmgOZgRaCost/Nz3tOA70+eCuKhr3zgea
            BqErpMUGVY9aUQKKIm6dgQMjpLfTaYfqsf3wq4MY0Bz/XYsXjd4VjoZAiJbsjeC0
            7Uo+QNhCzdwSTdIGZFWAF8gtS7MYphPVPStFeZzZSu8Y0PJqzrf3wLqIBZmPC6n9
            yAny760/DWq7T6bfDSqElDICpNDy0tNJyjkch6x6yq9OABteeS6LgmvLAud/XuVf
            JwvpnqmcJMuKRJOv0SZ/6XIxvsG1pw+d5AQm6wDO0JopVUa+zKyXy8rxJurGJ7dr
            CwIDAQABo1MwUTAdBgNVHQ4EFgQUh7+jbzx/lFG94eGWJs1RBb/oVdQwHwYDVR0j
            BBgwFoAUh7+jbzx/lFG94eGWJs1RBb/oVdQwDwYDVR0TAQH/BAUwAwEB/zANBgkq
            hkiG9w0BAQsFAAOCAgEABUjeIvgJVjVk3vNRzv6tYQZVwFfpiZlVm9/SRYgBWI/G
            JvBqZ7YOmwOhgMUhRRZBV0To1MZoVfE1cebO2NjyxYtblFtDXzG+N2ARtgoIJa+m
            VKU/QQxC/AEsC/g5QIee3PSaMN/SyB0aFy1X4BvWcPpiTi3bvEEmISDYctAicnvu
            0QJqeTIKWytd/onVcn5kL6UCSk/JiNlzpFUs7mm5h9E9ujtl2EBPzADgW1//sNTo
            ap6VORNkgQGaRmdu08kAKZ5289UCp3MNCO4hybl/uDGPQom90nhcWePhnNnMXuMH
            vUWTRLTnB2o02K9ak/7ESwf2jGezPJr61Yu4TUn7eswJ1kQdiolspHPuGKSXt/N7
            niTfy0294o+0p9pWRpvWg7nkKM014vTndVV3ud5C6t4ysdmrZLgzdFpFpKDOj4o7
            gTKWqGjjqOxvcrHU9/Op/xMVjQJb9CkGnHbtED9kYcSD/0NuVDPxHZNavSBMwGdN
            yiL3TA2ff1OsAOBGh/KGA7Zadf7PEIfkPQWPXAig7xc2BIU2swz0JCka20XIBCtZ
            9+svDLDw29bJP/IVLGfEPrXuoUJRT6GdglO7fqLaqDvldVGAVZgKlwW/ZP3m9yEM
            RKJXQxst/RE31JQUlESHto6Qtsjx21jed48MAJ22m2g7GN4JsuJmFdg7aFtasQs=
            -----END CERTIFICATE-----
            """;
        const string Key = """
            -----BEGIN ENCRYPTED PRIVATE KEY-----
            MIIJnDBOBgkqhkiG9w0BBQ0wQTApBgkqhkiG9w0BBQwwHAQI54mo3IFG5YwCAggA
            MAwGCCqGSIb3DQIJBQAwFAYIKoZIhvcNAwcECJiOUa05WJTFBIIJSGOm10afMd7E
            uScNA0SJO5uqcK418DPoZ6Kzso041nlXseGvRaxaoGEZAQW59Vylnx60+y+8hl1j
            Thm8+7Wy7faO2+lsv+1MoHker7jhFaxynk3Sj8K2MoojCqwGgCc86KrRLBmFe+6G
            HEV3odAz1EnejYgGoHjp+edjAWvQWJDD7D9j4z9I5LnJRlI/mPy904FtKSll3y+F
            gtGy0ecYLEvT3gV/0rkeb5w8RmrqmKC6Am4RLD9VIzJA+x2RsPjgp1Me9VUazGZr
            yVPipzxDs6/vA/5kEParP8fFQrKjNaNp9j626ZDKVGmjMupYgqH51+Un+aOOBim+
            WJLqyzHI2psv7TLW6tIM9BNWWlFsBmgl2GP4uVTHa0+5u+aEpo7WzirasUuLAyiH
            xkIoKCJ7HW+Y36uPJD0Z+Fbgo/6dMfgSeVJcQiyhku36x7Um4Jzh1O+XS//66BHh
            aefU8nXM8w6Hl+922+qnAjNxV5yTDQitsczQWQdSyVj11fnTAgQhN9r8EZdwMuBD
            pIVhXL1vS2vYk7ZuFegLo03grikTBTka9pWj6HxHpNDON2YRYq9onBnrunF9lRgG
            z2IGUw00E//Ev5OM0xPPhsjx0I5Shqj6Km6VEu0FeA77x8dVT5lv8uykCZFYEd67
            tvkEiYX993aP+abLvdlyGD2jLfP/bRVMEDiDUQq8+E46/z/WMMCewAbmBZYVmGtY
            t3HuSiLjgzdps7Hewe1C83HqOJmFXkbIOMX44Z/ulpkuqqR6e8gFmnrCGYz0urtg
            TM9eKbEjKwHfOVytokA2AQswVY4+zWM7sOC72dlNN+u4MbfMvon79wmelo46LbOA
            +nJ4Orqdl1bNChzuToMDuyyRO/cYwaxDhCJwl+467k3HhNn+RcXNceCJSsU757RS
            TXlHs1LxzFKFyimLcs2O4prQXxHDMPBS0INCR0zik/Ny/FgVGLtX68hNTIBIMuOZ
            k9S7paPBO4UQIVmhTTM8O+jVkCQirIwk5zL25ZxlEOB9dQTONmc/kRmVEyWCF7v2
            Mr70WUF3EJ3yd5veTp3BTg8AIeyUst6M0HlXYYwM2sQ73HlawOSQrMJAr4q/+hUi
            rYd9ljwKHVh560NbFqNgVOsScq2vzO0+BvUE2GrCSCs5bEjgyhKM85PB3C5Lpp3j
            ZwNUbkY7VdtmNSlbaKQn6tdyIxMzdx3MgNLjfHJHLNBOvVSZcHcBZbgg0SqFCDUP
            U7xkae6/1uTvGvXHqwfZO7o/cDIR27+7/aXJ6cJnxf24A68vG09amaPURlt8XnX9
            W4MR4yRcVZjjqRYqQ1rqfRtZjJAEgNuuUW2bxnlSQy7UoEOEQ45hwXYBa1KqIryT
            s/w8jidPmhZ/aVpxoWRrlCuHL9lzhu79Zp4aEa2bx6xyjQWFzl856mpviPpfdCDg
            tBN3xsgr0P+SUoqOWixoyjK2YEej6kgFH2QSiJVOslC+OdSxnpzzFQqlTVTSFWE0
            LlbDj0B4ABhBjG09+W2L+7cReCJy8D5umJz3/6wEzpd9QH7NIlD6VwPAOKyQLVDb
            hrHVnvacM31cLFsAUQz6SnpXY/Be4+QI2aUSAZJgOwAwX6VYlmjLkar2zCuBLUmB
            NdcqkAgP4l0Y7ouNglS9euiPLy9PyWMEcTNozjCvP18KI3B+jnoW4zDia0BqWRtv
            PVOnj4vKRBjoAuXDbpVowSYZ4mseMsXYRjDzCgXKlpCSAqmWKEPqdHefLCSiqWj7
            3o9uUf9a5PUbWzT3tIgn4bagq+aunobXEGtxCm3HirPh3RyyFNdGbGvWG3mSvfqm
            x8vgvaxIxeEubBlrLtdC4C/s7S+wnuQEB7a8vZpOnfGkpxCTqjfsZ9Q5hpRJ5GZe
            ooztndd6tptbOl5WTtO2YgAsFQ7TNWqOy4q15ylgz62HXX46DQvD8rB0/3icEh5+
            nfcpcLMHFOhAdUbLuFihlWcrPX2Jvj2djrSY/xVSZjTGm5UXKsGC6OG1eYu5xsy0
            +gwm4u9qFaJ3I/tcHCBFlvwaw28evc4R0dlDC9gxeOdeZPM+1IAN/jYQBIZvj5W6
            SGfFaCKZi3bRwJtUzyYZMvRAVStDV6HV51ZoxesEId6w6A/8pKLD1d7PEtcp/pbX
            80hJGZ7p9i8tAp4O1gioA4J20R2dkqcVr3opbrx+AC5hqqZRBZg1chDqk1hNcEwy
            /zo/lL7ClxWwK5SXCZmHgcp5ldn6vNbEXOKpJ+srIL4rrpshKW9YEqqSlNHQa4qs
            z/BxD8ZAxFEiWwsFqC4ZPw3uqpy3X21mYVv3RzWWHXP+mfdXWK4gY7asrhL+SS2t
            rd7ig1BmgbH/gZJmG3cHqW4jZYSY+ae80Ac1d454rEUelWZPeL1tXnP/INrT5SIH
            Brw8W+tOuyXMnXG5JPfATOEEGCcLHZ0qiieyQ/Oj0MT2llPZXtVmAn5wSBjt+56x
            RDZ9Mr0ADV1BaFz1LdQd1VwnecZMKcuFIwv8pCMarXlq5cTvohv+9usnqutawCF5
            Kbk0RArivnttj35krAxBa4tZn/r3QrRLFhyMOXV61RIrTqkrewMMO2W4gdQOsOsV
            fv5zpdycNacUc2cGQvTQiJHF7kFgwtMnYew0olJ8oXpSnw8x5sK8Yxa4Rl0P2q/I
            nOXX0JrvB44UMoBgIKICqu7aI7goXN3o4lBoRSnmNnRClxvaw8RgQ7/nX4l40hjU
            Gs/brWruXuJJn3ibpg2lI+K7q2UVwT5RL+T9kXBE7v9MkD/lnp04n74K5BmfB7Su
            MgHGBCjKToI8VWKyQoFWz/wG/HcB8IdncF48B/o0tgraQYXcwHdapmX/Tl3dn3Hz
            AbrWf9/RfnZbdFNXus+vt/+ATC+vY6SUKvDIcQhTjzSU7Apo6Wd3QxzV7l2tHD7M
            571aDcK3K2PADJV5KVrxBuWGHWUAKfzD8UBRnqDgBwBF0fh0gdTwaqTP+eHHCyz7
            zcpyYCBMdusGSpgj1mlzXrCfLF2cJo0iDVUARTvAczz4xaQCfWRi/mB09rfioSsS
            DZtMSjIyfdNBEr80AQaWYefogiobasmt2rwJl4JfPKL9OncGYOI9zv6y590g/x1v
            DIlymNfDIDoqoT0yq+L25rlKg96b+k8016mZLlALafCzTG/VoX5EULxqR4FdF899
            1w0hcWVL65q9BL4afgw9wA==
            -----END ENCRYPTED PRIVATE KEY-----
            """;

        using var rsa = PEM.ReadPrivateKey(Key, "password");
        Assert.Equal(4096, rsa.KeySize);
        Assert.Equal("RSA", rsa.KeyExchangeAlgorithm);

        using var cert = PEM.ReadCertWithKey(Cert, Key, "password");
        Assert.True(cert.HasPrivateKey);
        Assert.Equal("31FF3908FB667B76C4958B1CE3FCE685E4C63493", cert.SerialNumber);
    }

    [Fact]
    public void SelfSigned()
    {
        using var cert = PEM.CreateSelfSignedServerCertificate("name", "domain.com", TimeSpan.FromDays(90), 4096);
        Assert.True(cert.HasPrivateKey);

        var privateKey = cert.GetRSAPrivateKey();
        Assert.NotNull(privateKey);
        Assert.Equal(4096, privateKey.KeySize);

        // OSX was weird, so verify that we can actually use the certificate. This test is likely superfluous.
        var bytes = new byte[64];
        Random.Shared.NextBytes(bytes);
        var encryptedBytes = privateKey.Encrypt(bytes, RSAEncryptionPadding.Pkcs1);
        var decryptedBytes = privateKey.Decrypt(encryptedBytes, RSAEncryptionPadding.Pkcs1);
        Assert.Equal(bytes, decryptedBytes);
    }
}
