# Web
Generating [IWebHost](https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.hosting.iwebhost?view=aspnetcore-8.0) can be done in one of several ways. Adding [Routes](./routes.md) is how these services interact externally.

## General Initialization
The easiest way to configure a host is through `WebHostBuilderConfiguration`. It's a set of common configuration settings that can then be used to initialize a host. [Custom Initialization](#Custom_Initialization) explains how to do a completely custom solution.

```csharp
public static class Routes
{
    [JsonSerializable]
    public class GetStorageBody
    {
        [Name("value")]
        public string Value { get; set; }
    }

    // Parse the body as json. Return 400 on invalid body and return 412 when the request body type is not application/json.
    [Route("/api/v1/storage/{id:guid}", methods: "POST")]
    public static async Task GetStorage(HttpContext context, Guid id, [JsonBody] body)
    {
        // Do stuff here.
    }
}

public static void Main()
{
    var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber };
    configuration.AddRoutes(Routes.GetRoutes());

    using var server = configuration.CreateWebHost();
    await server.RunAsync();
}
```

## WebHostBuilderConfiguration
A helper class to help configure [IWebHost](https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.hosting.iwebhost?view=aspnetcore-8.0) with common features.

#### HttpPort
Integer value for port number. If it is unspecified, the Kestrel default (usually 5000) is used.

#### HttpsPort
Integer value for port number of https connections. If unspecified, https is disabled.

#### SelectCertificate
A callback for selecting https certificate. This value must be set if `HttpsPort` is set.

Using [PEM.CreateSelfSignedServerCertificate](../utilities/pem.md#CreateSelfSignedServerCertificate) can be helpful for local debugging.

#### EnableCompression
A value indicating whether compression is enabled. This value defaults to `true` and will enable br and gz compression on supported mime types. Those mime types default to `ResponseCompressionDefaults.MimeTypes`. `CompressionMimeTypes`

#### CompressionMimeTypes
A set of mime types that will have compression enabled. The default is `ResponseCompressionDefaults.MimeTypes`.

#### MinimumLogLevel
Kestrel built-in logging level. Defaults to `LogLevel.Warning`.

#### UseDeveloperExceptionPage
A value indicating whether built-in developer exception page. This setting should not be enabled in production builds. This value is ignored if `ExceptionHandler` is set.

#### ExceptionHandler
A callback for handling exceptions. If this value is set, `UseDeveloperExceptionPage` is ignored.

#### ConfigureKestrel
Callback for `IWebHostBuilder.ConfigureKestrel`. This occurs after `ConfigureHttps`.

#### ConfigureServices
Callback for `IWebHostBuilder.ConfigureServices`. This callback will occur after `AddCompression`.

#### ConfigureApp
Callback for `IWebHostBuilder.Configure`. This callback occurs after everything except for adding routes.

#### RequireAuthorization
A value indicating whether routes require authorization by default.

If this value is true, `AllowAnonymousAttribute` is required to access the route anonymously or `AuthorizeAttribute` can be used to access using authentication policies provided. The default authentication provider will be used if neither are provided.

#### UseWebSockets
Enabled websocket support.

#### Routes
A list of routes to add to the host.

Use the shorthand `AddRoutes(IEnumerable<RouteInfo>)` to add items to this list.

### CreateWebHost()
Creates a new `IWebHost` using the configuration settings.

## <a name="Custom_Initialization"></a>Custom Initialization
[WebHostBuilderConfiguration.CreateWebHost](../../KestrelToolbox/Web/WebHostBuilderConfiguration.cs) has complete code for how [IWebHost](https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.hosting.iwebhost?view=aspnetcore-8.0) is generated. For custom solutions that require more flexibility than `ConfigureKestrel`, `ConfigureServices`, and `ConfigureApp` offer it may be helpful to use this code as a guide.

There's an extension method `KestrelToolbox.Web.Extensions.WebHostBuilderConfigurationExtensions.UseRouteInfo` that takes `IEnumerable<RouteInfo>` and will set up routes from [IWebHostBuilder](https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.hosting.iwebhostbuilder?view=aspnetcore-8.0).


## StaticFiles
Serving static files can be handled automatically, this includes compression and sidecar files (.gz and .br files existing alongside a file).

```csharp
StaticFiles staticFiles = new StaticFiles("/var/lib/files/")
    {
        // Set the max age to 1yr.
        MaxAge = TimeSpan.FromDays(365),

        // Make sure cache control is public so any CDN will cache the content.
        CacheControlPrivate = false,
    };

[Route("/js/{*path}")]
public static async Task GetJS(HttpContext context, string path)
{
    var served = await staticFiles.ServeFileAsync(context, path);
    if (!served)
    {
        // Couldn't find the file, return 404. It's also possible to do whatever here, nothing will have been sent to the client.
        response.StatusCode = StatusCodes.Status404NotFound;
        return;
    }
}
```
