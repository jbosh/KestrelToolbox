# Routes
Routes are similar to [Controllers](https://learn.microsoft.com/en-us/aspnet/core/mvc/controllers/routing?view=aspnetcore-8.0) in basic Kestrel. These are where the action happens in an `IWebHost` and the logic will happen for handling requests. These are generated at compile time in the method `GetRoutes()` which works on either a static class or an instance class.

```csharp
[Route("/test", methods: "POST, PUT")]
[Authorize("admin")]
public static async Task Test(HttpContext context, [QueryParameters] QueryStringPayload query, [JsonBody] BodyPayload body)
{
    // ... do the work.
}
```

Any of these routes will generate `GetRoutes()` that can be applied to `IWebHost`.

```csharp
var configuration = new WebHostBuilderConfiguration { HttpPort = PortNumber };
configuration.AddRoutes(Routes.GetRoutes());
```

or if using `WebHostBuilder` directly:

```csharp
webHostBuilder.Configure(app =>
{
    // Extension method from KestrelToolbox.Web.Extensions.WebHostBuilderConfigurationExtensions
    app.UseRouteInfo(Routes.GetRoutes());
});
```

## Parameters
Routing can be done using standard [.net routing](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-8.0#route-constraints). An example below:

```csharp
[AllowAnonymous]
[Route("/authenticate/{role}", methods: "POST")]
public async Task Authenticate(HttpContext context, string role)
{
    // Action goes here.
}

// Types will automatically be filled out and parsed.
[Route("/get/{fileID:guid}/{version:int}")]
public async Task Authenticate(HttpContext context, Guid fileID, int version)
{
    // Action goes here.
}

// It's also possible to use slugs.
[Route("/blog/{*post}")]
public async Task Authenticate(HttpContext context, string post)
{
    // Action goes here.
}
```

## JsonBodyAttribute
Using the [JsonSerializable](../serialization/json-serializable-attribute.md) you can set up a parameter to a `Route` to use `JsonBody` attribute, and it will automatically be filled out or the route will return 400 with the error in the event of parsing failure. 415 will be returned if the content type is not `application/json`.

```csharp
[JsonSerializable]
public partial class LoginPayload
{
    [Name("username")]
    public required string Username { get; set; }

    [Name("password")]
    public required string Password { get; set; }
}

[Route("/test", methods: "POST")]
public static async Task Login(HttpContext context, [JsonBody] LoginPayload body)
{
    // ...
}
```

## QueryParametersAttribute
Query strings can also be processed before any user logic using [QueryStringSerializable](../serialization/query-string-serializable-attribute.md). These parameters will be parsed and validated before the method is called. In the event of parsing errors, a 400 will be returned with error information.

```csharp
[QueryStringSerializable]
public partial class GetFilePayload
{
    [Name("filename")]
    public required string Filename { get; set; }

    [Name("version")]
    public int Version { get; set; }
}

[Route("/test", methods: "GET")]
public static async Task GetFile(HttpContext context, [QueryParameters] GetFilePayload body)
{
    // ...
}
```

## Auth
Authorization and authentication can be performed before entering routes using `AuthorizeAttribute`. This is standard .net core authorizations. The examples below use [WebHostBuilderConfiguration](./web.md) but can be extended to `WebHostBuilder` directly.

```csharp
// Initialization
new WebHostBuilderConfiguration
{
    ConfigureApp = ConfigureApp,
    ConfigureServices = ConfigureServices,
}

public static void ConfigureApp(IApplicationBuilder app)
{
    _ = app.UseAuthentication();
    _ = app.UseAuthorization();
}

public static void ConfigureServices(IServicesCollection services)
{
    services.AddAuthentication("MyScheme").AddJwtBearer(...);

    services.AddSingleton<IAuthorizationHandler, IsLoggedInRequirement.HasScopeHandler>();

    services.AddAuthorization(options =>
    {
       options.AddPolicy("logged-in", policy => policy.Requirements.Add(new IsLoggedInRequirement()));
    });

    public class IsLoggedInRequirement : IAuthorizationRequirement
    {
       public class HasScopeHandler : AuthorizationHandler<IsLoggedInRequirement>
        {
            protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsLoggedInRequirement requirement)
            {
                // Check if there is a user of any kind from `JwtBearer`.
                if (context.User != null)
                {
                    // We've gotten a valid token, policy is successful.
                    context.Succeed(requirement);
                }

                return Task.CompletedTask;
            }
        }
    }
}

[Route("/user/getData")]
[Authorize("logged-in")]
public static Task GetData(HttpContext context)
{
    // User will already be logged in at this point.
}
```
