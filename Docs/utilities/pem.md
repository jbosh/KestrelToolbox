# PEM
PEM is a class that tries to read [openssl](https://www.openssl.org/) generated files in PEM format or similar. This includes certs and private keys for certs.

Supported types:
- `RSA PRIVATE KEY`
- `PRIVATE KEY`
- `ENCRYPTED PRIVATE KEY`
- `CERTIFICATE`

## <a name="CreateSelfSignedServerCertificate"></a>CreateSelfSignedServerCertificate
Creates a self-signed RSA `X509Certificate2`.

#### certificateName
Common name of the certificate. The certificate is valid only if the request hostname matches the certificate common name.

#### domain
The domain name for Subject Alternative Name for this certificate. This can and probably should be identical to `certificateName`.

#### expiration
Amount of time that the certificate is valid for.

#### keySizeInBits
The default for this is 2048 bits. This is an RSA key and is self-signed. 4096 is going to soon become a more recommended value.
