// <copyright file="ReflectionHelpers.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Data;
using System.Reflection;

namespace KestrelToolbox.Dynamic.Internal;

/// <summary>
/// Helpers to make reflection easier.
/// </summary>
internal static class ReflectionHelpers
{
    /// <summary>
    /// Gets if a custom attribute exists on a provider.
    /// </summary>
    /// <param name="info">Object to search.</param>
    /// <param name="inherit">When true, look up the hierarchy chain for the inherited custom attribute.</param>
    /// <typeparam name="T">Attribute to search for.</typeparam>
    /// <returns>True if the attribute exists, false if not..</returns>
    public static bool HasCustomAttribute<T>(ICustomAttributeProvider info, bool inherit = false)
        where T : class
    {
        var attributes = info.GetCustomAttributes(typeof(T), inherit);
        return attributes.Length != 0;
    }

    /// <summary>
    /// Gets a custom attribute and verifies that only 1 exists.
    /// </summary>
    /// <param name="info">Object to search.</param>
    /// <param name="inherit">When true, look up the hierarchy chain for the inherited custom attribute.</param>
    /// <typeparam name="T">Attribute to search for.</typeparam>
    /// <returns>{T} if attribute exists. Null if not.</returns>
    /// <exception cref="DuplicateNameException">Throws if multiple attributes exist.</exception>
    public static T? GetCustomAttribute<T>(ICustomAttributeProvider info, bool inherit = false)
        where T : class
    {
        var attributes = info.GetCustomAttributes(typeof(T), inherit);
        if (attributes.Length == 0)
        {
            return null;
        }

        if (attributes.Length != 1)
        {
            throw new DuplicateNameException("Using GetCustomAttribute cannot have multiple of same attribute.");
        }

        return (T?)attributes[0];
    }

    /// <summary>
    /// Gets a custom attributes.
    /// </summary>
    /// <param name="info">Object to search.</param>
    /// <param name="inherit">When true, look up the hierarchy chain for the inherited custom attribute.</param>
    /// <typeparam name="T">Attribute to search for.</typeparam>
    /// <returns>{T} if attributes exist. Null if not.</returns>
    public static T[] GetCustomAttributes<T>(ICustomAttributeProvider info, bool inherit = false)
        where T : class
    {
        var attributes = info.GetCustomAttributes(typeof(T), inherit);
        if (attributes.Length == 0)
        {
            return Array.Empty<T>();
        }

        return (T[])attributes;
    }

    /// <summary>
    /// Searches for the public members with the specified name.
    /// </summary>
    /// <param name="enumType">Enum type to search for.</param>
    /// <param name="enumName">Name of the enum.</param>
    /// <returns>Info about the member if it exists, null if none do.</returns>
    public static MemberInfo? GetEnumMemberInfo(Type enumType, string enumName)
    {
        var members = enumType.GetMember(enumName);
        return Array.Find(members, m => m.DeclaringType == enumType);
    }

    /// <summary>
    /// Finds an interface based on type.
    /// </summary>
    /// <param name="type">Type to search for interface of.</param>
    /// <param name="interfaceType">Type of interface that should exist. This should be an open generic type.</param>
    /// <returns>The interface's full type if found.</returns>
    public static Type? FindGenericInterface(Type type, Type interfaceType)
    {
        return Array.Find(type.GetInterfaces(), i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceType);
    }

    /// <summary>
    /// Finds an interface based on type.
    /// </summary>
    /// <param name="type">Type to search for interface of.</param>
    /// <param name="interfaceType">Type of interface that should exist.</param>
    /// <returns>The interface's full type if found.</returns>
    public static Type? FindInterface(Type type, Type interfaceType)
    {
        return Array.Find(type.GetInterfaces(), i => i.IsGenericType && i == interfaceType);
    }
}
