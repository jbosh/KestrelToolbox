// <copyright file="ParsingIntSwitchGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection.Emit;
using KestrelToolbox.Internal.ILGenExtensions;

namespace KestrelToolbox.Dynamic.Internal;

/// <summary>
/// Generator int switches.
/// </summary>
internal static class ParsingIntSwitchGenerator
{
    /// <summary>
    /// Range of ints.
    /// </summary>
    [SuppressMessage(
        "Minor Code Smell",
        "S1210:\"Equals\" and the comparison operators should be overridden when implementing \"IComparable\"",
        Justification = "Internal class with very specific use."
    )]
    public sealed class Range : IComparable<Range>
    {
        /// <summary>
        /// Gets or sets the start value of the range.
        /// </summary>
        public int StartValue { get; set; }

        /// <summary>
        /// Gets or sets the end value of the range.
        /// </summary>
        public int EndValue { get; set; }

        /// <summary>
        /// Gets the count of the range (end - start + 1).
        /// </summary>
        public int Count => this.EndValue - this.StartValue + 1;

        /// <inheritdoc />
        public override string ToString() => $"start: {this.StartValue}, end: {this.EndValue}";

        /// <summary>
        /// Initializes a new instance of the <see cref="Range"/> class.
        /// </summary>
        /// <param name="startValue">Initial value for this range.</param>
        public Range(int startValue)
        {
            this.StartValue = startValue;
            this.EndValue = startValue;
        }

        /// <inheritdoc />
        public int CompareTo(Range? other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }

            return other is null ? 1 : this.StartValue.CompareTo(other.StartValue);
        }
    }

    /// <summary>
    /// Generates ranges based on collection of strings.
    /// </summary>
    /// <param name="values">Collection to turn into ranges.</param>
    /// <returns>List of ranges.</returns>
    public static List<Range> GenerateRanges(IEnumerable<int> values)
    {
        var sorted = values.OrderBy(v => v);
        var result = new List<Range>();
        var range = default(Range);
        foreach (var value in sorted)
        {
            if (range != null && value == range.EndValue + 1)
            {
                range.EndValue = value;
            }
            else
            {
                range = new Range(value);
                result.Add(range);
            }
        }

        return result;
    }

    /// <summary>
    /// Generates IL for this switch.
    /// </summary>
    /// <param name="ilGen">IL generator to add instructions to.</param>
    /// <param name="ranges">Ranges to process.</param>
    /// <param name="start">Start of the range (should be 0).</param>
    /// <param name="end">End of range (should be ranges.Count - 1).</param>
    /// <param name="ldValue">Generate op codes to load integer value.</param>
    /// <param name="onSuccess">Generate op codes to store the string at its definition. Use null if there is no need to anything other than validate that the value exists.</param>
    /// <param name="successLabel">Label for successfully finding the int.</param>
    /// <param name="failureLabel">Label for failing to find int.</param>
    public static void RecurseSwitch(
        ILGenerator ilGen,
        List<Range> ranges,
        int start,
        int end,
        Action ldValue,
        Action<int>? onSuccess,
        Label successLabel,
        Label failureLabel
    )
    {
        Debug.Assert(start <= end, "Start was too big.");

        if (start == end)
        {
            var range = ranges[start];
            if (onSuccess != null)
            {
                if (range.Count == 1)
                {
                    onSuccess(range.StartValue);
                }
                else
                {
                    var labels = new Label[range.Count];
                    for (var i = 0; i < labels.Length; i++)
                    {
                        labels[i] = ilGen.DefineLabel();
                    }

                    ldValue();
                    if (range.StartValue != 0)
                    {
                        ilGen.EmitLdc(-range.StartValue);
                        ilGen.Emit(OpCodes.Add);
                    }

                    ilGen.Emit(OpCodes.Switch, labels);
                    ilGen.Emit(OpCodes.Br, failureLabel);

                    for (var i = 0; i < labels.Length; i++)
                    {
                        ilGen.MarkLabel(labels[i]);
                        onSuccess(range.StartValue + i);
                        ilGen.Emit(OpCodes.Br, successLabel);
                    }
                }
            }
            else
            {
                if (range.Count == 1)
                {
                    ldValue();
                    ilGen.EmitLdc(range.StartValue);
                    ilGen.Emit(OpCodes.Beq, successLabel);
                    ilGen.Emit(OpCodes.Br, failureLabel);
                }
                else
                {
                    ldValue();
                    ilGen.EmitLdc(range.StartValue);
                    ilGen.Emit(OpCodes.Blt, failureLabel);
                    ldValue();
                    ilGen.EmitLdc(range.EndValue);
                    ilGen.Emit(OpCodes.Bgt, failureLabel);
                    ilGen.Emit(OpCodes.Br, successLabel);
                }
            }

            return;
        }

        var mid = start + ((end - start) / 2);

        var midValue = ranges[mid].EndValue;
        var leftLabel = ilGen.DefineLabel();
        var rightLabel = ilGen.DefineLabel();
        ldValue();
        ilGen.EmitLdc(midValue);
        ilGen.Emit(OpCodes.Ble, leftLabel);
        ilGen.Emit(OpCodes.Br, rightLabel);

        ilGen.MarkLabel(leftLabel);
        RecurseSwitch(ilGen, ranges, start, mid, ldValue, onSuccess, successLabel, failureLabel);

        ilGen.MarkLabel(rightLabel);
        RecurseSwitch(ilGen, ranges, mid + 1, end, ldValue, onSuccess, successLabel, failureLabel);
    }
}
