// <copyright file="EnumSerializer.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using KestrelToolbox.Dynamic.Internal;
using KestrelToolbox.Internal.ILGenExtensions;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;

#pragma warning disable S125 // Remove commented out code
#pragma warning disable S3011 // Accessing private members

namespace KestrelToolbox.Dynamic.Serialization.Helpers;

/// <summary>
/// Class for parsing enums and validating that their values are valid dynamically at runtime.
/// </summary>
public class EnumSerializer
{
    /// <summary>
    /// Delegate for compiled parsers.
    /// </summary>
    /// <typeparam name="T">The type to parse.</typeparam>
    /// <param name="value">The value of the enum to parse.</param>
    /// <param name="result">Resulting <typeparamref name="T"/> value.</param>
    /// <param name="error">Error message on failure.</param>
    /// <returns>True on success, false on failure.</returns>
    public delegate bool TryParseDelegate<T>(
        string value,
        [NotNullWhen(true)] out T result,
        [NotNullWhen(false)] out string? error
    );

    /// <summary>
    /// Delegate for compiled serializers.
    /// </summary>
    /// <typeparam name="T">The type to serialize.</typeparam>
    /// <param name="value">The value of the enum to parse.</param>
    /// <returns>The serialized value.</returns>
    public delegate string SerializeDelegate<in T>(T value);

    private readonly struct CompiledParser
    {
        private readonly Delegate @delegate;

        public MethodInfo Method { get; }

        public CompiledParser(Delegate @delegate, MethodInfo method)
        {
            this.Method = method;
            this.@delegate = @delegate;
        }

        public TryParseDelegate<T> GetDelegate<T>()
        {
            return (TryParseDelegate<T>)this.@delegate;
        }
    }

    private readonly struct CompiledSerializer
    {
        private readonly Delegate @delegate;

        public MethodInfo Method { get; }

        public CompiledSerializer(Delegate @delegate, MethodInfo method)
        {
            this.Method = method;
            this.@delegate = @delegate;
        }

        public SerializeDelegate<T> GetDelegate<T>()
            where T : Enum
        {
            return (SerializeDelegate<T>)this.@delegate;
        }
    }

#if NET9_0_OR_GREATER
    private readonly System.Threading.Lock compiledParserLock = new();
    private readonly System.Threading.Lock compiledSerializersLock = new();
#else
    private readonly object compiledParserLock = new();
    private readonly object compiledSerializersLock = new();
#endif // NET9_0_OR_GREATER

    private Dictionary<Type, CompiledParser> compiledParsers = new();
    private Dictionary<Type, CompiledSerializer> compiledSerializers = new();

    /// <summary>
    /// Adds a custom parsing callback for <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type to add custom parser for.</typeparam>
    /// <param name="callback">Callback to call when this type is encountered.</param>
    public void AddParser<T>(TryParseDelegate<T> callback)
    {
        var compiledParser = new CompiledParser(callback, callback.Method);
        if (!this.TryAddCompiledParser(typeof(T), compiledParser))
        {
            throw new ArgumentException($"{typeof(T)} has already been added as a parser.");
        }
    }

    /// <summary>
    /// Adds a custom serializer callback for <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type to add custom serializer for.</typeparam>
    /// <param name="callback">Callback to call when this type is encountered.</param>
    public void AddSerializer<T>(SerializeDelegate<T> callback)
    {
        var compiledSerializer = new CompiledSerializer(callback, callback.Method);
        if (!this.TryAddCompiledSerializer(typeof(T), compiledSerializer))
        {
            throw new ArgumentException($"{typeof(T)} has already been added as a serializer.");
        }
    }

    /// <summary>
    /// Try to parse a string value into an enum using the built in parser. This should be the equivalent to <c>EnumParser.TryParse&lt;T&gt;($"\"{value}\"")</c>
    /// but without having to instantiate a parser and string.
    /// </summary>
    /// <param name="value">Raw string value to parse. Should not contain leading or trailing quotes.</param>
    /// <param name="result">Resulting value, or default if false.</param>
    /// <typeparam name="T">Enum to parse to.</typeparam>
    /// <returns>True if parsing succeeded, false if not.</returns>
    /// <remarks>
    /// This will not use any custom parsing functionality that exists and is only to assist with using the built-in enum parsing
    /// when a user is using <see cref="NameAttribute"/> and <see cref="EnumSerializableAttribute"/>.
    /// </remarks>
    public bool TryParse<T>(string value, [NotNullWhen(true)] out T? result)
    {
        return this.TryParse(value, out result, out _);
    }

    /// <summary>
    /// Try to parse a string value into an enum using the built in parser. This should be the equivalent to <c>EnumParser.TryParse&lt;T&gt;($"\"{value}\"")</c>
    /// but without having to instantiate a parser and string.
    /// </summary>
    /// <param name="value">Raw string value to parse. Should not contain leading or trailing quotes.</param>
    /// <param name="result">Resulting value, or default if false.</param>
    /// <param name="error">Error if one should occur.</param>
    /// <typeparam name="T">Enum to parse to.</typeparam>
    /// <returns>True if parsing succeeded, false if not.</returns>
    /// <remarks>
    /// This will not use any custom parsing functionality that exists and is only to assist with using the built-in enum parsing
    /// when a user is using <see cref="NameAttribute"/> and <see cref="EnumSerializableSettingsAttribute"/>.
    /// </remarks>
    public bool TryParse<T>(string value, [NotNullWhen(true)] out T? result, [NotNullWhen(false)] out string? error)
    {
        if (!this.compiledParsers.TryGetValue(typeof(T), out var compiledParser))
        {
            compiledParser = this.CompileParser(typeof(T));
        }

        return compiledParser.GetDelegate<T>()(value, out result, out error);
    }

    /// <summary>
    /// Serializes an enum value <typeparamref name="T"/> to a string using built in serializer.
    /// </summary>
    /// <param name="value">Value to serialize.</param>
    /// <typeparam name="T">Type of enum to serialize.</typeparam>
    /// <returns>The serialized string.</returns>
    /// <remarks>
    /// This will not use any custom parsing functionality that exists and is only to assist with using the built-in enum parsing
    /// when a user is using <see cref="NameAttribute"/> and <see cref="EnumSerializableSettingsAttribute"/>.
    /// </remarks>
    public string Serialize<T>(T value)
        where T : Enum
    {
        if (!this.compiledSerializers.TryGetValue(typeof(T), out var compiledSerializer))
        {
            compiledSerializer = this.CompileSerializer(typeof(T));
        }

        return compiledSerializer.GetDelegate<T>()(value);
    }

    private static void ReturnError(ILGenerator ilGen, string message)
    {
        // error = message
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldstr, message);
        ilGen.Emit(OpCodes.Stind_Ref);

        // return false
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Ret);
    }

    private static void GetNullableType(Type type, out Type baseType, out bool isNullable)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
            isNullable = true;
            baseType = type.GenericTypeArguments[0];
        }
        else
        {
            baseType = type;
            isNullable = false;
        }
    }

    private CompiledParser CompileParser(Type type)
    {
        if (this.compiledParsers.TryGetValue(type, out var compiledParser))
        {
            return compiledParser;
        }

        GetNullableType(type, out var baseType, out var isNullable);
        if (!baseType.IsEnum)
        {
            throw new ArgumentException($"Enum type is required. {baseType} is unsupported.");
        }

        if (baseType.GetEnumUnderlyingType() != typeof(int))
        {
            throw new NotImplementedException(
                "Cannot JIT enum that is not base type 'int'. Please use roslyn generated type for this."
            );
        }

        var typeFullName = $"{baseType.Name}{(isNullable ? "?" : string.Empty)}";

        var dynamicMethod = new DynamicMethod(
            string.Empty,
            typeof(bool),
            new[] { typeof(string), type.MakeByRefType(), typeof(string).MakeByRefType() }
        );

        var ilGen = dynamicMethod.GetILGenerator();

        var result = ilGen.DeclareLocal(typeof(int));

        // Store result = 0 because we're always returning an int.
        ilGen.Emit(OpCodes.Ldarg_1);
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Stind_Ref);

        var enumSerializationAttribute = ReflectionHelpers.GetCustomAttribute<EnumSerializableSettingsAttribute>(
            baseType
        );
        var useNumbers =
            enumSerializationAttribute?.SerializationType == EnumSerializableSettingsSerializationType.Numbers;
        var invalidValue = enumSerializationAttribute?.InvalidValue;

        if (invalidValue != null)
        {
            if (invalidValue.GetType() != baseType)
            {
                throw new Exception(
                    $"'{typeFullName}' has invalid default value. Must be of type {typeFullName} not {invalidValue.GetType().FullName}."
                );
            }
        }

        if (useNumbers)
        {
            if (invalidValue != null)
            {
                throw new Exception($"'{typeFullName}' has both UseNumbers and InvalidValue set.");
            }

            var isIntLabel = ilGen.DefineLabel();

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdloca(result);
            ilGen.Emit(
                OpCodes.Call,
                typeof(int).GetMethod("TryParse", new[] { typeof(string), typeof(int).MakeByRefType() })!
            );
            ilGen.Emit(OpCodes.Brtrue, isIntLabel);

            ReturnError(ilGen, $"'{typeFullName}' must be an integer.");

            ilGen.MarkLabel(isIntLabel);

            var successLabel = ilGen.DefineLabel();
            var failureLabel = ilGen.DefineLabel();
            var enumValues = Enum.GetValues(baseType);
            var enumValuesForError = enumValues.Cast<int>().ToList();
            var ranges = ParsingIntSwitchGenerator.GenerateRanges(enumValues.Cast<int>());
            if (enumValues.Length <= 4 || ranges.Count == 1)
            {
                foreach (var enumValue in enumValues)
                {
                    // if (attribute == value)
                    ilGen.EmitLdloc(result);
                    ilGen.EmitLdc((int)enumValue);
                    ilGen.Emit(OpCodes.Beq, successLabel); // found it
                }
            }
            else
            {
                ParsingIntSwitchGenerator.RecurseSwitch(
                    ilGen,
                    ranges,
                    0,
                    ranges.Count - 1,
                    () => ilGen.EmitLdloc(result),
                    null,
                    successLabel,
                    failureLabel
                );
            }

            ilGen.MarkLabel(failureLabel);
            ReturnError(
                ilGen,
                $"'{typeFullName}' must be one of: {string.Join(", ", enumValuesForError.OrderBy(v => v))}."
            );

            ilGen.MarkLabel(successLabel);
        }
        else
        {
            var enumValues = new Dictionary<string, object>();
            var successLabel = ilGen.DefineLabel();
            var failureLabel = ilGen.DefineLabel();

            foreach (var member in baseType.GetMembers())
            {
                var enumInfo = member as FieldInfo;
                if (enumInfo == null || enumInfo.FieldType != baseType)
                {
                    continue;
                }

                var enumNameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(enumInfo);
                if (enumNameAttributes.Length == 0)
                {
                    continue;
                }

                var enumValue = enumInfo.GetValue(null)!;

                foreach (var enumNameAttribute in enumNameAttributes)
                {
                    foreach (var enumName in enumNameAttribute.Names)
                    {
                        if (!enumValues.TryAdd(enumName, enumValue))
                        {
                            throw new Exception($"Duplicate name {enumName} on {typeFullName}.");
                        }
                    }
                }
            }

            if (enumValues.Count <= 4)
            {
                foreach (var (enumName, enumValue) in enumValues)
                {
                    var nextNameLabel = ilGen.DefineLabel();

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.EmitLdc(enumName);
                    ilGen.EmitLdc((int)StringComparison.Ordinal);
                    ilGen.Emit(
                        OpCodes.Call,
                        typeof(string).GetMethod(
                            "Equals",
                            BindingFlags.Public | BindingFlags.Static,
                            new[] { typeof(string), typeof(string), typeof(StringComparison) }
                        )!
                    );
                    ilGen.Emit(OpCodes.Brfalse, nextNameLabel);

                    ilGen.EmitLdc((int)enumValue);
                    ilGen.EmitStloc(result);
                    ilGen.Emit(OpCodes.Br, successLabel);

                    ilGen.MarkLabel(nextNameLabel);
                }
            }
            else
            {
                var ranges = ParsingStringSwitchGenerator.GenerateRanges(enumValues.Keys);
                ParsingStringSwitchGenerator.RecurseSwitch(
                    ilGen,
                    ranges,
                    0,
                    ranges.Count - 1,
                    () => ilGen.Emit(OpCodes.Ldarg_0),
                    s =>
                    {
                        ilGen.EmitLdc((int)enumValues[s]);
                        ilGen.EmitStloc(result);
                    },
                    successLabel,
                    failureLabel
                );
            }

            ilGen.MarkLabel(failureLabel);
            if (invalidValue != null)
            {
                ilGen.EmitLdc((int)invalidValue);
                ilGen.EmitStloc(result);
            }
            else
            {
                ReturnError(
                    ilGen,
                    $"'{typeFullName}' must be one of: {string.Join(", ", enumValues.Select(p => p.Key).OrderBy(v => v))}."
                );
            }

            ilGen.MarkLabel(successLabel);
        }

        // resultOut = result
        if (isNullable)
        {
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.EmitLdloc(result);
            ilGen.Emit(OpCodes.Newobj, type.GetConstructor(new[] { baseType })!);
            ilGen.Emit(OpCodes.Stobj, type);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.EmitLdloc(result);
            ilGen.Emit(OpCodes.Stind_Ref);
        }

        // error = null
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldnull);
        ilGen.Emit(OpCodes.Stind_Ref);

        ilGen.EmitLdc(true);
        ilGen.Emit(OpCodes.Ret);

        var methodDelegate = dynamicMethod.CreateDelegate(typeof(TryParseDelegate<>).MakeGenericType(type));
        compiledParser = new CompiledParser(methodDelegate, dynamicMethod);

        _ = this.TryAddCompiledParser(type, compiledParser);

        return compiledParser;
    }

    private CompiledSerializer CompileSerializer(Type type)
    {
        if (this.compiledSerializers.TryGetValue(type, out var compiledSerializer))
        {
            return compiledSerializer;
        }

        if (!type.IsEnum)
        {
            throw new ArgumentException($"Enum type is required. {type} is unsupported.");
        }

        if (type.GetEnumUnderlyingType() != typeof(int))
        {
            throw new NotImplementedException(
                "Cannot JIT enum that is not base type 'int'. Please use roslyn generated type for this."
            );
        }

        var dynamicMethod = new DynamicMethod(string.Empty, typeof(string), new[] { type });

        var ilGen = dynamicMethod.GetILGenerator();

        var enumSerializationAttribute = ReflectionHelpers.GetCustomAttribute<EnumSerializableSettingsAttribute>(type);

        var invalidValue = enumSerializationAttribute?.InvalidValue;
        if (invalidValue != null)
        {
            if (invalidValue.GetType() != type)
            {
                throw new Exception(
                    $"'{type.FullName}' has invalid default value. Must be of type {type.FullName} not {invalidValue.GetType().FullName}."
                );
            }
        }

        var useNumbers =
            enumSerializationAttribute?.SerializationType == EnumSerializableSettingsSerializationType.Numbers;
        if (useNumbers)
        {
            if (invalidValue != null)
            {
                throw new Exception($"'{type.FullName}' has both UseNumbers and InvalidValue set.");
            }

            ilGen.Emit(OpCodes.Ldarga_S, 0);
            ilGen.Emit(OpCodes.Call, typeof(int).GetMethod("ToString", Type.EmptyTypes)!);
        }
        else
        {
            var enumValues = new Dictionary<int, string>();
            var invalidValueString = default(string);
            var result = ilGen.DeclareLocal(typeof(string));
            var successLabel = ilGen.DefineLabel();
            var failureLabel = ilGen.DefineLabel();

            foreach (var member in type.GetMembers())
            {
                var enumInfo = member as FieldInfo;
                if (enumInfo == null || enumInfo.FieldType != type)
                {
                    continue;
                }

                var enumNameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(enumInfo);
                if (enumNameAttributes.Length == 0)
                {
                    continue;
                }

                var enumValue = enumInfo.GetValue(null)!;

                foreach (var enumNameAttribute in enumNameAttributes)
                {
                    if (enumNameAttribute.Names.Count == 0)
                    {
                        continue;
                    }

                    if (!enumValues.TryAdd((int)enumValue, enumNameAttribute.Names[0]))
                    {
                        throw new Exception($"Duplicate name {enumNameAttribute.Names[0]} on {type.FullName}.");
                    }

                    if (invalidValue != null && invalidValueString == null)
                    {
                        if (invalidValue.Equals(enumValue))
                        {
                            invalidValueString = enumNameAttribute.Names[0];
                        }
                    }
                }
            }

            if (enumValues.Count <= 4)
            {
                foreach (var (enumValue, enumName) in enumValues)
                {
                    var nextNameLabel = ilGen.DefineLabel();

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.EmitLdc(enumValue);
                    ilGen.Emit(OpCodes.Bne_Un, nextNameLabel);

                    ilGen.EmitLdc(enumName);
                    ilGen.EmitStloc(result);
                    ilGen.Emit(OpCodes.Br, successLabel);

                    ilGen.MarkLabel(nextNameLabel);
                }
            }
            else
            {
                var ranges = ParsingIntSwitchGenerator.GenerateRanges(enumValues.Keys);
                ParsingIntSwitchGenerator.RecurseSwitch(
                    ilGen,
                    ranges,
                    0,
                    ranges.Count - 1,
                    () => ilGen.Emit(OpCodes.Ldarg_0),
                    i =>
                    {
                        ilGen.EmitLdc(enumValues[i]);
                        ilGen.EmitStloc(result);
                    },
                    successLabel,
                    failureLabel
                );
            }

            ilGen.MarkLabel(failureLabel);
            if (invalidValue != null)
            {
                if (invalidValueString == null)
                {
                    throw new InvalidDataException($"{type.FullName} default value was not found.");
                }

                ilGen.EmitLdc(invalidValueString);
                ilGen.EmitStloc(result);
            }
            else
            {
                ilGen.Emit(OpCodes.Ldarga_S, 0);
                ilGen.Emit(OpCodes.Callvirt, typeof(int).GetMethod("ToString", Type.EmptyTypes)!);
                ilGen.EmitStloc(result);
            }

            ilGen.MarkLabel(successLabel);
            ilGen.EmitLdloc(result);
        }

        ilGen.Emit(OpCodes.Ret);

        var methodDelegate = dynamicMethod.CreateDelegate(typeof(SerializeDelegate<>).MakeGenericType(type));
        compiledSerializer = new CompiledSerializer(methodDelegate, dynamicMethod);

        _ = this.TryAddCompiledSerializer(type, compiledSerializer);

        return compiledSerializer;
    }

    private bool TryAddCompiledParser(Type type, CompiledParser compiledParser)
    {
        // Lock so we can modify CompiledSerializers
        lock (this.compiledParserLock)
        {
            // Clone CompiledParsers and add to that. We cannot modify active CompiledParsers. GC will collect old one when no more threads
            // are using it.
            var newCompiledParsers = this.compiledParsers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledParsers.TryAdd(type, compiledParser);

            this.compiledParsers = newCompiledParsers;
            return added;
        }
    }

    private bool TryAddCompiledSerializer(Type type, CompiledSerializer compiledSerializer)
    {
        // Lock so we can modify CompiledSerializers
        lock (this.compiledSerializersLock)
        {
            // Clone CompiledSerializers and add to that. We cannot modify active CompiledParsers. GC will collect old one when no more threads
            // are using it.
            var newCompiledSerializers = this.compiledSerializers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledSerializers.TryAdd(type, compiledSerializer);

            this.compiledSerializers = newCompiledSerializers;
            return added;
        }
    }
}
