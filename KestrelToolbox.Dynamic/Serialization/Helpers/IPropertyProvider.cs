// <copyright file="IPropertyProvider.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Reflection;

namespace KestrelToolbox.Dynamic.Serialization.Helpers;

/// <summary>
/// Interface for providing attributes and states of fields/properties.
/// </summary>
internal interface IPropertyProvider : ICustomAttributeProvider
{
    /// <summary>
    /// Gets the <see cref="NullabilityState"/> if available.
    /// </summary>
    public NullabilityState NullabilityState { get; }

    /// <summary>
    /// Returns a custom attributes defined on this member, identified by type, or null if there are no custom attributes of that type.
    /// </summary>
    /// <param name="inherit">When true, look up the hierarchy chain for the inherited custom attribute.</param>
    /// <typeparam name="T">Type of custom custom attribute.</typeparam>
    /// <returns>The custom attribute or null.</returns>
    /// <exception cref="Exception">Multiple attributes were found.</exception>
    /// <exception cref="TypeLoadException">The custom attribute type cannot be loaded.</exception>
    public T? GetCustomAttribute<T>(bool inherit = false)
    {
        var arr = this.GetCustomAttributes(typeof(T), inherit);
        return arr.Length switch
        {
            0 => default,
            1 => (T)arr[0],
            _ => throw new Exception("Multiple attributes found."),
        };
    }
}

/// <summary>
/// Provider for properties.
/// </summary>
internal sealed class PropertyProvider : IPropertyProvider
{
    /// <inheritdoc />
    public NullabilityState NullabilityState { get; }

    /// <summary>
    /// Gets info for the property.
    /// </summary>
    public PropertyInfo PropertyInfo { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="PropertyProvider"/> class.
    /// </summary>
    /// <param name="propertyInfo">Info for the property.</param>
    public PropertyProvider(PropertyInfo propertyInfo)
    {
        this.PropertyInfo = propertyInfo;
        var context = new NullabilityInfoContext();
        var nullabilityInfo = context.Create(propertyInfo);

        // You can mismatch ReadState and WriteState by using [AllowNull] and [MaybeNull].
        // [AllowNull] = ReadState becoming nullable.
        // [MaybeNull] = WriteState becoming nullable.
        // I'm choosing to check both for setting NullabilityState because it's probably better to
        // allow nulls in the event a user isn't using #nullable (which sets both). If neither of them
        // is NullabilityState.Nullable, use ReadState because the user will be reading from this value
        // and that is likely what they want. In practice, it means the state is not nullable though.
        var isNullable =
            nullabilityInfo.WriteState == NullabilityState.Nullable
            || nullabilityInfo.ReadState == NullabilityState.Nullable;
        if (isNullable)
        {
            this.NullabilityState = NullabilityState.Nullable;
        }
        else
        {
            this.NullabilityState = nullabilityInfo.ReadState;
        }
    }

    /// <inheritdoc />
    public object[] GetCustomAttributes(bool inherit) => this.PropertyInfo.GetCustomAttributes(inherit);

    /// <inheritdoc />
    public object[] GetCustomAttributes(Type attributeType, bool inherit) =>
        this.PropertyInfo.GetCustomAttributes(attributeType, inherit);

    /// <inheritdoc />
    public bool IsDefined(Type attributeType, bool inherit) => this.PropertyInfo.IsDefined(attributeType, inherit);
}

/// <summary>
/// Property provider for array children.
/// </summary>
internal sealed class ArrayChildProvider : IPropertyProvider
{
    /// <inheritdoc />
    public NullabilityState NullabilityState { get; } = NullabilityState.Unknown;

    private readonly ICustomAttributeProvider provider;

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayChildProvider"/> class.
    /// </summary>
    /// <param name="provider">Provider information from the parent.</param>
    public ArrayChildProvider(ICustomAttributeProvider provider)
    {
        this.provider = provider;

        if (provider is PropertyProvider propertyProvider)
        {
            var context = new NullabilityInfoContext();
            var nullabilityInfo = context.Create(propertyProvider.PropertyInfo);
            this.NullabilityState = nullabilityInfo.ElementType?.WriteState ?? NullabilityState.Unknown;

            // Check to see that read and write state are the same. We're writing to the class, so WriteState is correct. It'd just
            // be nice to see the scenario where they do not match.
            Debug.Assert(
                nullabilityInfo.WriteState == nullabilityInfo.ReadState,
                "Nullability info should be the same."
            );
        }
    }

    /// <inheritdoc />
    public object[] GetCustomAttributes(bool inherit) => throw new NotSupportedException();

    /// <inheritdoc />
    public object[] GetCustomAttributes(Type attributeType, bool inherit)
    {
        if (attributeType == typeof(MinLengthAttribute) || attributeType == typeof(MaxLengthAttribute))
        {
            return Array.Empty<object>();
        }

        return this.provider.GetCustomAttributes(attributeType, inherit);
    }

    /// <inheritdoc />
    public bool IsDefined(Type attributeType, bool inherit) => throw new NotSupportedException();
}

/// <summary>
/// Property provider when there are no attributes.
/// </summary>
internal sealed class NoAttributesProvider : IPropertyProvider
{
    /// <inheritdoc />
    public NullabilityState NullabilityState => NullabilityState.Unknown;

    /// <summary>
    /// Static instance so that these do not need to be instantiated.
    /// </summary>
    public static readonly NoAttributesProvider Default = new();

    /// <inheritdoc />
    public object[] GetCustomAttributes(bool inherit)
    {
        return Array.Empty<object>();
    }

    /// <inheritdoc />
    public object[] GetCustomAttributes(Type attributeType, bool inherit)
    {
        return Array.Empty<object>();
    }

    /// <inheritdoc />
    public bool IsDefined(Type attributeType, bool inherit)
    {
        return false;
    }
}
