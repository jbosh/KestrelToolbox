// <copyright file="QueryStringSerializer.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Frozen;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using KestrelToolbox.Dynamic.Internal;
using KestrelToolbox.Internal.ILGenExtensions;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Serialization.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using EmailAddressAttribute = KestrelToolbox.Serialization.DataAnnotations.EmailAddressAttribute;
using RequiredAttribute = KestrelToolbox.Serialization.DataAnnotations.RequiredAttribute;
using StringLengthAttribute = KestrelToolbox.Serialization.DataAnnotations.StringLengthAttribute;

#pragma warning disable S125 // Remove commented out code
#pragma warning disable S3011 // Accessing private members

namespace KestrelToolbox.Dynamic.Serialization.Helpers;

/// <summary>
/// Class for parsing query strings and validating that their values are valid.
/// </summary>
public class QueryStringSerializer
{
    /// <summary>
    /// Delegate for compiled parsers.
    /// </summary>
    /// <typeparam name="T">The type to parse.</typeparam>
    /// <param name="value">The value of the enum to parse.</param>
    /// <param name="result">Resulting <typeparamref name="T"/> value.</param>
    /// <param name="error">Error message on failure.</param>
    /// <returns>True on success, false on failure.</returns>
    public delegate bool TryParseDelegate<T>(
        IQueryCollection value,
        [NotNullWhen(true)] out T result,
        [NotNullWhen(false)] out string? error
    );

    private readonly struct CompiledParser
    {
        private readonly Delegate @delegate;

        public MethodInfo Method { get; }

        public CompiledParser(Delegate @delegate, MethodInfo method)
        {
            this.Method = method;
            this.@delegate = @delegate;
        }

        public TryParseDelegate<T> GetDelegate<T>()
        {
            return (TryParseDelegate<T>)this.@delegate;
        }
    }

#if NET9_0_OR_GREATER
    private readonly System.Threading.Lock compiledParserLock = new();
#else
    private readonly object compiledParserLock = new();
#endif // NET9_0_OR_GREATER

    private Dictionary<Type, CompiledParser> compiledParsers = new();

    /// <summary>
    /// Adds a custom parsing callback for <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type to add custom parser for.</typeparam>
    /// <param name="callback">Callback to call when this type is encountered.</param>
    public void AddParser<T>(TryParseDelegate<T> callback)
    {
        var compiledParser = new CompiledParser(callback, callback.Method);
        if (!this.TryAddCompiledParser(typeof(T), compiledParser))
        {
            throw new ArgumentException($"{typeof(T)} has already been added as a parser.");
        }
    }

    /// <summary>
    /// Try to parse a query string into <typeparamref name="T"/> using attributes in <see cref="KestrelToolbox.Serialization.DataAnnotations"/>.
    /// </summary>
    /// <param name="queryString">Raw query string to parse.</param>
    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
    /// <typeparam name="T">The type marked up with attributes from <see cref="KestrelToolbox.Serialization.DataAnnotations"/> to emit.</typeparam>
    /// <returns>True if parsing was successful, false if it was not.</returns>
    public bool TryParse<T>(
        string queryString,
        [NotNullWhen(true)] out T? result,
        [NotNullWhen(false)] out string? error
    ) => this.TryParse(new QueryCollection(QueryHelpers.ParseQuery(queryString)), out result, out error);

    /// <summary>
    /// Try to parse a query string into <typeparamref name="T"/> using attributes in <see cref="KestrelToolbox.Serialization.DataAnnotations"/>.
    /// </summary>
    /// <param name="query">Parsed query string collection to parse.</param>
    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
    /// <typeparam name="T">The type marked up with attributes from <see cref="KestrelToolbox.Serialization.DataAnnotations"/> to emit.</typeparam>
    /// <returns>True if parsing was successful, false if it was not.</returns>
    public bool TryParse<T>(
        IQueryCollection query,
        [NotNullWhen(true)] out T? result,
        [NotNullWhen(false)] out string? error
    )
    {
        if (!this.compiledParsers.TryGetValue(typeof(T), out var compiledParser))
        {
            compiledParser = this.CompileParser(typeof(T));
        }

        return compiledParser.GetDelegate<T>()(query, out result, out error);
    }

    private static bool IsListType(Type type)
    {
        return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
    }

    private static bool IsSetType(Type type)
    {
        return type.IsGenericType
            && (
                type.GetGenericTypeDefinition() == typeof(HashSet<>)
                || type.GetGenericTypeDefinition() == typeof(FrozenSet<>)
            );
    }

    private static bool TryGetCustomParse(Type type, Type parameterType, [NotNullWhen(true)] out MethodInfo? methodInfo)
    {
        foreach (var method in type.GetMethods())
        {
            var attribute = method.GetCustomAttribute<QueryStringSerializableCustomParseAttribute>();
            if (attribute == null)
            {
                continue;
            }

            var parameters = method.GetParameters();
            if (parameters.Length != 3)
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} requires 3 parameters."
                );
            }

            if (parameters[1].ParameterType != type.MakeByRefType())
            {
                continue;
            }

            if (method.ReturnType != typeof(bool))
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} had invalid return type {method.ReturnType}, needed bool."
                );
            }

            if (parameters[0].ParameterType != typeof(string) && parameters[0].ParameterType != typeof(StringValues))
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} requires first parameter to be ref string or {nameof(StringValues)}."
                );
            }

            if (parameters[0].ParameterType != parameterType)
            {
                methodInfo = default;
                return false;
            }

            if (parameters[2].ParameterType != typeof(string).MakeByRefType())
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} requires first parameter to be out string."
                );
            }

            methodInfo = method;
            return true;
        }

        methodInfo = default;
        return false;
    }

    private static void ReturnError(ILGenerator ilGen, string message)
    {
        // error = message
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldstr, message);
        ilGen.Emit(OpCodes.Stind_Ref);

        // return false
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Ret);
    }

    private static LocalBuilder ParseIndividualElement(
        ILGenerator ilGen,
        LocalBuilder stringValue,
        string name,
        Type baseElementType,
        ICustomAttributeProvider property,
        LocalBuilder? incomingValue = null
    )
    {
        LocalBuilder value;
        GetNullableType(baseElementType, out var elementType, out _);
        switch (elementType.FullName)
        {
            case "System.Byte[]":
            {
                // System.Byte[] is special because it uses base64
                value = incomingValue ?? ilGen.DeclareLocal(elementType);

                var method = typeof(QueryStringParser).GetMethod(
                    nameof(QueryStringParser.TryParseBase64String),
                    BindingFlags.Public | BindingFlags.Static,
                    null,
                    new[] { typeof(string), baseElementType.MakeByRefType() },
                    null
                )!;
                ilGen.EmitLdloc(stringValue);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, method);

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                ReturnError(ilGen, $"'{name}' was not a url encoded base64 string.");

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.Boolean":
            {
                value = incomingValue ?? ilGen.DeclareLocal(elementType);

                var method = typeof(QueryStringParser).GetMethod(
                    nameof(QueryStringParser.TryParseBool),
                    BindingFlags.Public | BindingFlags.Static,
                    null,
                    new[] { typeof(string), elementType.MakeByRefType() },
                    null
                )!;
                ilGen.EmitLdloc(stringValue);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, method);

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                ReturnError(ilGen, $"'{name}' was not of type {GetFriendlyName(elementType)}.");

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.SByte":
            case "System.Int16":
            case "System.Int32":
            case "System.Int64":
            case "System.Byte":
            case "System.UInt16":
            case "System.UInt32":
            case "System.UInt64":
            case "System.Single":
            case "System.Double":
            case "System.Decimal":
            {
                value = incomingValue ?? ilGen.DeclareLocal(elementType);
                ParseIntegerInternal(ilGen, stringValue, property, elementType, name, value);
                break;
            }

            case "System.String":
            {
                value = incomingValue ?? ilGen.DeclareLocal(elementType);
                ilGen.EmitLdloc(stringValue);
                ilGen.EmitStloc(value);

                var trimAttribute = ReflectionHelpers.GetCustomAttribute<TrimValueAttribute>(property);
                if (trimAttribute is { OnParse: true })
                {
                    // attribute.Trim()
                    ilGen.EmitLdloc(value);
                    if (trimAttribute.TrimChars == null)
                    {
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("Trim", Type.EmptyTypes)!);
                    }
                    else
                    {
                        ilGen.Emit(OpCodes.Ldstr, new string(trimAttribute.TrimChars));
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("ToCharArray", Type.EmptyTypes)!);
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("Trim", new[] { typeof(char[]) })!);
                    }

                    ilGen.EmitStloc(value);
                }

                var minLengthAttribute = ReflectionHelpers.GetCustomAttribute<MinLengthAttribute>(property);
                if (minLengthAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (value.Length < minLengthAttribute.Length)
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                    ilGen.EmitLdc(minLengthAttribute.Length);
                    ilGen.Emit(OpCodes.Bge, successLabel);

                    ReturnError(
                        ilGen,
                        $"'{name}' must be longer than {minLengthAttribute.Length - 1} character{(minLengthAttribute.Length - 1 == 1 ? string.Empty : "s")}."
                    );
                    ilGen.MarkLabel(successLabel);
                }

                var maxLengthAttribute = ReflectionHelpers.GetCustomAttribute<MaxLengthAttribute>(property);
                if (maxLengthAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (value.Length > maxLengthAttribute.Length)
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                    ilGen.EmitLdc(maxLengthAttribute.Length);
                    ilGen.Emit(OpCodes.Ble, successLabel);

                    ReturnError(ilGen, $"'{name}' must be shorter than {maxLengthAttribute.Length} characters.");
                    ilGen.MarkLabel(successLabel);
                }

                var stringLengthAttribute = ReflectionHelpers.GetCustomAttribute<StringLengthAttribute>(property);
                if (stringLengthAttribute != null)
                {
                    if (stringLengthAttribute.Minimum > 0)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (value.Length < stringLengthAttribute.MinimumLength)
                        ilGen.EmitLdloc(value);
                        ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                        ilGen.EmitLdc(stringLengthAttribute.Minimum);
                        ilGen.Emit(OpCodes.Bge, successLabel);

                        ReturnError(
                            ilGen,
                            $"'{name}' must be longer than {stringLengthAttribute.Minimum - 1} character{(stringLengthAttribute.Minimum - 1 == 1 ? string.Empty : "s")}."
                        );
                        ilGen.MarkLabel(successLabel);
                    }

                    if (stringLengthAttribute.Maximum < int.MaxValue)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (value.Length > stringLengthAttribute.MaximumLength)
                        ilGen.EmitLdloc(value);
                        ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                        ilGen.EmitLdc(stringLengthAttribute.Maximum);
                        ilGen.Emit(OpCodes.Ble, successLabel);

                        ReturnError(
                            ilGen,
                            $"'{name}' must be shorter than {stringLengthAttribute.Maximum} characters."
                        );
                        ilGen.MarkLabel(successLabel);
                    }
                }

                var validValuesAttribute = ReflectionHelpers.GetCustomAttribute<ValidValuesAttribute>(property);
                if (validValuesAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();
                    foreach (var attribute in validValuesAttribute.Values)
                    {
                        // if (attribute != value)
                        ilGen.EmitLdloc(value);
                        ilGen.Emit(OpCodes.Ldstr, attribute);
                        ilGen.EmitLdc((int)StringComparison.Ordinal);
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(string).GetMethod(
                                "Equals",
                                BindingFlags.Public | BindingFlags.Static,
                                new[] { typeof(string), typeof(string), typeof(StringComparison) }
                            )!
                        );
                        ilGen.Emit(OpCodes.Brtrue, successLabel);
                    }

                    ReturnError(
                        ilGen,
                        $"'{name}' must be one of: {string.Join(", ", validValuesAttribute.Values.OrderBy(v => v).Select(v => $"\"{v}\""))}."
                    );

                    ilGen.MarkLabel(successLabel);
                }

                var emailAddressAttribute = ReflectionHelpers.GetCustomAttribute<EmailAddressAttribute>(property);
                if (emailAddressAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (!EmailAddressAttribute.IsValid(value))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(
                        OpCodes.Call,
                        typeof(EmailAddressAttribute).GetMethod(
                            "IsValid",
                            BindingFlags.Public | BindingFlags.Static,
                            new[] { typeof(string) }
                        )!
                    );
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    ReturnError(ilGen, $"'{name}' is not a valid email address.");

                    ilGen.MarkLabel(successLabel);
                }

                var nullOnEmptyAttribute = ReflectionHelpers.GetCustomAttribute<NullOnEmptyAttribute>(property);
                if (nullOnEmptyAttribute is { OnParse: true })
                {
                    var isNonNullLabel = ilGen.DefineLabel();

                    // if (string.IsNullOrWhitespace(attribute))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("IsNullOrEmpty", new[] { typeof(string) })!);
                    ilGen.Emit(OpCodes.Brfalse, isNonNullLabel);

                    ilGen.EmitLdc(null);
                    ilGen.EmitStloc(value);

                    ilGen.MarkLabel(isNonNullLabel);
                }

                var requiredAttribute = ReflectionHelpers.GetCustomAttribute<RequiredAttribute>(property);
                if (requiredAttribute is { AllowOnlyWhiteSpace: false })
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (string.IsNullOrWhitespace(attribute))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("IsNullOrWhiteSpace", new[] { typeof(string) })!);
                    ilGen.Emit(OpCodes.Brfalse, successLabel);

                    ReturnError(ilGen, $"'{name}' must contain characters that are not whitespace.");

                    ilGen.MarkLabel(successLabel);
                }

                break;
            }

            case "System.Guid":
            case "KestrelToolbox.Types.UUIDv1":
            case "System.TimeOnly":
            case "System.DateTime":
            case "System.DateTimeOffset":
            {
                value = incomingValue ?? ilGen.DeclareLocal(elementType);

                var method = elementType.GetMethod(
                    "TryParse",
                    BindingFlags.Public | BindingFlags.Static,
                    null,
                    new[] { typeof(string), elementType.MakeByRefType() },
                    null
                )!;
                ilGen.EmitLdloc(stringValue);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, method);

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                ReturnError(ilGen, $"'{name}' was not of type {GetFriendlyName(elementType)}.");

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.DateOnly":
            {
                var dateOnlySettingsAttributes = ReflectionHelpers.GetCustomAttribute<DateOnlyParseSettingsAttribute>(
                    property
                );
                var format = dateOnlySettingsAttributes?.Format;

                value = incomingValue ?? ilGen.DeclareLocal(elementType);

                if (format != null)
                {
                    var method = elementType.GetMethod(
                        "TryParseExact",
                        BindingFlags.Public | BindingFlags.Static,
                        null,
                        new[] { typeof(string), typeof(string), elementType.MakeByRefType() },
                        null
                    )!;
                    ilGen.EmitLdloc(stringValue);
                    ilGen.EmitLdc(format);
                    ilGen.EmitLdloca(value);
                    ilGen.Emit(OpCodes.Call, method);
                }
                else
                {
                    var method = elementType.GetMethod(
                        "TryParse",
                        BindingFlags.Public | BindingFlags.Static,
                        null,
                        new[] { typeof(string), elementType.MakeByRefType() },
                        null
                    )!;
                    ilGen.EmitLdloc(stringValue);
                    ilGen.EmitLdloca(value);
                    ilGen.Emit(OpCodes.Call, method);
                }

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                ReturnError(ilGen, $"'{name}' was not of type {GetFriendlyName(elementType)}.");

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.TimeSpan":
            {
                value = incomingValue ?? ilGen.DeclareLocal(elementType);

                var method = typeof(TimeSpanParser).GetMethod(
                    "TryParse",
                    BindingFlags.Public | BindingFlags.Static,
                    null,
                    new[] { typeof(string), elementType.MakeByRefType() },
                    null
                )!;
                ilGen.EmitLdloc(stringValue);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, method);

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                ReturnError(ilGen, $"'{name}' was not of type {GetFriendlyName(elementType)}.");

                ilGen.MarkLabel(successLabel);
                break;
            }

            default:
            {
                if (TryGetCustomParse(baseElementType, typeof(string), out var tryParseMethod))
                {
                    value = incomingValue ?? ilGen.DeclareLocal(elementType);
                    ilGen.EmitLdloc(stringValue);
                    ilGen.EmitLdloca(value);
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.Emit(OpCodes.Call, tryParseMethod);

                    var successLabel = ilGen.DefineLabel();
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    // return false, everything should be correct at this point
                    ilGen.EmitLdc(0);
                    ilGen.Emit(OpCodes.Ret);

                    ilGen.MarkLabel(successLabel);
                }
                else if (elementType.IsEnum)
                {
                    value = incomingValue ?? ilGen.DeclareLocal(elementType);

                    var enumSerializationAttribute =
                        ReflectionHelpers.GetCustomAttribute<EnumSerializableSettingsAttribute>(elementType);
                    var useNumbers =
                        enumSerializationAttribute?.SerializationType
                        == EnumSerializableSettingsSerializationType.Numbers;
                    var invalidValue = enumSerializationAttribute?.InvalidValue;

                    if (invalidValue != null)
                    {
                        if (invalidValue.GetType() != elementType)
                        {
                            throw new Exception(
                                $"'{elementType.FullName}' has invalid default value. Must be of type {elementType.FullName} not {invalidValue.GetType().FullName}."
                            );
                        }
                    }

                    if (useNumbers)
                    {
                        if (invalidValue != null)
                        {
                            throw new Exception($"'{elementType.FullName}' has both UseNumbers and InvalidValue set.");
                        }

                        var isIntLabel = ilGen.DefineLabel();

                        ilGen.EmitLdloc(stringValue);
                        ilGen.EmitLdloca(value);
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(int).GetMethod("TryParse", new[] { typeof(string), typeof(int).MakeByRefType() })!
                        );
                        ilGen.Emit(OpCodes.Brtrue, isIntLabel);

                        ReturnError(ilGen, $"{name} > '{baseElementType.Name}' must be an integer.");

                        ilGen.MarkLabel(isIntLabel);

                        var successLabel = ilGen.DefineLabel();
                        var failureLabel = ilGen.DefineLabel();
                        var enumValues = Enum.GetValues(elementType);
                        var enumValuesForError = enumValues.Cast<int>().ToList();
                        var ranges = ParsingIntSwitchGenerator.GenerateRanges(enumValues.Cast<int>());
                        if (enumValues.Length <= 4 || ranges.Count == 1)
                        {
                            foreach (var enumValue in enumValues)
                            {
                                // if (attribute == value)
                                ilGen.EmitLdloc(value);
                                ilGen.EmitLdc((int)enumValue);
                                ilGen.Emit(OpCodes.Beq, successLabel); // found it
                            }
                        }
                        else
                        {
                            ParsingIntSwitchGenerator.RecurseSwitch(
                                ilGen,
                                ranges,
                                0,
                                ranges.Count - 1,
                                () => ilGen.EmitLdloc(value),
                                null,
                                successLabel,
                                failureLabel
                            );
                        }

                        ilGen.MarkLabel(failureLabel);
                        ReturnError(
                            ilGen,
                            $"{name} > '{baseElementType.Name}' must be one of: {string.Join(", ", enumValuesForError.OrderBy(v => v))}."
                        );

                        ilGen.MarkLabel(successLabel);
                    }
                    else
                    {
                        var enumValues = new Dictionary<string, object>();
                        var successLabel = ilGen.DefineLabel();
                        var failureLabel = ilGen.DefineLabel();

                        foreach (var member in elementType.GetMembers())
                        {
                            var enumInfo = member as FieldInfo;
                            if (enumInfo == null || enumInfo.FieldType != elementType)
                            {
                                continue;
                            }

                            var enumNameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(enumInfo);
                            if (enumNameAttributes.Length == 0)
                            {
                                continue;
                            }

                            var enumValue = enumInfo.GetValue(null)!;

                            foreach (var enumNameAttribute in enumNameAttributes)
                            {
                                foreach (var enumName in enumNameAttribute.Names)
                                {
                                    if (!enumValues.TryAdd(enumName, enumValue))
                                    {
                                        throw new Exception($"Duplicate name {enumName} on {elementType.FullName}.");
                                    }
                                }
                            }
                        }

                        if (enumValues.Count <= 4)
                        {
                            foreach (var (enumName, enumValue) in enumValues)
                            {
                                var nextNameLabel = ilGen.DefineLabel();

                                ilGen.EmitLdloc(stringValue);
                                ilGen.EmitLdc(enumName);
                                ilGen.EmitLdc((int)StringComparison.Ordinal);
                                ilGen.Emit(
                                    OpCodes.Call,
                                    typeof(string).GetMethod(
                                        "Equals",
                                        BindingFlags.Public | BindingFlags.Static,
                                        new[] { typeof(string), typeof(string), typeof(StringComparison) }
                                    )!
                                );
                                ilGen.Emit(OpCodes.Brfalse, nextNameLabel);

                                ilGen.EmitLdc((int)enumValue);
                                ilGen.EmitStloc(value);
                                ilGen.Emit(OpCodes.Br, successLabel);

                                ilGen.MarkLabel(nextNameLabel);
                            }
                        }
                        else
                        {
                            var ranges = ParsingStringSwitchGenerator.GenerateRanges(enumValues.Keys);
                            ParsingStringSwitchGenerator.RecurseSwitch(
                                ilGen,
                                ranges,
                                0,
                                ranges.Count - 1,
                                () => ilGen.EmitLdloc(stringValue),
                                s =>
                                {
                                    ilGen.EmitLdc((int)enumValues[s]);
                                    ilGen.EmitStloc(value);
                                },
                                successLabel,
                                failureLabel
                            );
                        }

                        ilGen.MarkLabel(failureLabel);
                        if (invalidValue != null)
                        {
                            ilGen.EmitLdc((int)invalidValue);
                            ilGen.EmitStloc(value);
                        }
                        else
                        {
                            ReturnError(
                                ilGen,
                                $"{name} > '{baseElementType.Name}' must be one of: {string.Join(", ", enumValues.Select(p => p.Key).OrderBy(v => v))}."
                            );
                        }

                        ilGen.MarkLabel(successLabel);
                    }
                }
                else
                {
                    throw new DataException($"Unknown type {elementType.FullName}.");
                }

                break;
            }
        }

        return value;
    }

    private static void GetNullableType(Type type, out Type baseType, out bool isNullable)
    {
        if (type.FullName!.StartsWith("System.Nullable`1", StringComparison.Ordinal))
        {
            isNullable = true;
            baseType = type.GenericTypeArguments[0];
        }
        else
        {
            baseType = type;
            isNullable = false;
        }
    }

    private static void ParseIntegerInternal(
        ILGenerator ilGen,
        LocalBuilder stringValue,
        ICustomAttributeProvider property,
        Type propertyType,
        string name,
        LocalBuilder value
    )
    {
        var tryParseMethod = propertyType.GetMethod(
            "TryParse",
            BindingFlags.Public | BindingFlags.Static,
            new[] { typeof(string), propertyType.MakeByRefType() }
        )!;
        ilGen.EmitLdloc(stringValue);
        ilGen.EmitLdloca(value);
        ilGen.Emit(OpCodes.Call, tryParseMethod);

        var tryParseSuccessLabel = ilGen.DefineLabel();
        ilGen.Emit(OpCodes.Brtrue, tryParseSuccessLabel);

        ReturnError(ilGen, $"'{name}' was not of type {GetFriendlyName(propertyType)}.");

        ilGen.MarkLabel(tryParseSuccessLabel);

        var rangeAttribute = ReflectionHelpers.GetCustomAttribute<ValidRangeAttribute>(property);
        if (rangeAttribute != null)
        {
            dynamic min,
                max;
            try
            {
                min = Convert.ChangeType(rangeAttribute.Minimum, propertyType, CultureInfo.InvariantCulture);
                max = Convert.ChangeType(rangeAttribute.Maximum, propertyType, CultureInfo.InvariantCulture);
            }
            catch
            {
                throw new InvalidProgramException(
                    $"'{name}' with type {propertyType.FullName} had invalid type in attribute Range."
                );
            }

            var isUnsigned =
                propertyType == typeof(byte)
                || propertyType == typeof(ushort)
                || propertyType == typeof(uint)
                || propertyType == typeof(ulong);

            var successLabel0 = ilGen.DefineLabel();
            var successLabel1 = ilGen.DefineLabel();

            // if (value < min)
            ilGen.EmitLdloc(value);
            ILGenExtensions.EmitLdc(ilGen, min);
            if (propertyType.FullName == "System.Decimal")
            {
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(decimal).GetMethod("op_GreaterThanOrEqual", new[] { typeof(decimal), typeof(decimal) })!
                );
                ilGen.Emit(OpCodes.Brtrue, successLabel0);
            }
            else
            {
                ilGen.Emit(isUnsigned ? OpCodes.Bge_Un : OpCodes.Bge, successLabel0);
            }

            ReturnError(ilGen, $"'{name}' was not in the range [{min}..{max}]");

            ilGen.MarkLabel(successLabel0);

            // if (value > max)
            ilGen.EmitLdloc(value);
            ILGenExtensions.EmitLdc(ilGen, max);
            if (propertyType.FullName == "System.Decimal")
            {
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(decimal).GetMethod("op_LessThanOrEqual", new[] { typeof(decimal), typeof(decimal) })!
                );
                ilGen.Emit(OpCodes.Brtrue, successLabel1);
            }
            else
            {
                ilGen.Emit(isUnsigned ? OpCodes.Ble_Un : OpCodes.Ble, successLabel1);
            }

            ReturnError(ilGen, $"'{name}' was not in the range [{min}..{max}]");
            ilGen.MarkLabel(successLabel1);
        }

        var validValuesAttribute = ReflectionHelpers.GetCustomAttribute<ValidValuesAttribute>(property);
        if (validValuesAttribute != null)
        {
            if (validValuesAttribute.ValuesAsInts == null)
            {
                throw new InvalidProgramException(
                    $"'{name}' with type {propertyType} had invalid attribute ValidValues."
                );
            }

            var successLabel = ilGen.DefineLabel();
            foreach (var attribute in validValuesAttribute.ValuesAsInts!)
            {
                // if (attribute == value)
                ilGen.EmitLdloc(value);
                ilGen.EmitLdc(attribute);
                ilGen.Emit(OpCodes.Beq, successLabel); // found it
            }

            ReturnError(
                ilGen,
                $"'{name}' must be one of: {string.Join(", ", validValuesAttribute.ValuesAsInts.OrderBy(v => v))}."
            );

            ilGen.MarkLabel(successLabel);
        }
    }

    private static LocalBuilder GetSingleStringValue(ILGenerator ilGen, LocalBuilder stringValues, string propertyName)
    {
        var stringValue = ilGen.DeclareLocal(typeof(string));
        var testArrayLabel = ilGen.DefineLabel();
        var successLabel = ilGen.DefineLabel();

        ilGen.EmitLdloca(stringValues);
        ilGen.Emit(OpCodes.Call, typeof(StringValues).GetMethod("get_Count", Type.EmptyTypes)!);
        ilGen.Emit(OpCodes.Dup);
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Bne_Un, testArrayLabel);

        ilGen.Emit(OpCodes.Pop);
        ReturnError(ilGen, $"'{propertyName}' must contain values.");

        ilGen.MarkLabel(testArrayLabel);
        ilGen.EmitLdc(1);
        ilGen.Emit(OpCodes.Beq, successLabel);
        ReturnError(ilGen, $"'{propertyName}' cannot have multiple values.");

        ilGen.MarkLabel(successLabel);
        ilGen.EmitLdloca(stringValues);
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Call, typeof(StringValues).GetMethod("get_Item", new[] { typeof(int) })!);

        ilGen.EmitStloc(stringValue);
        return stringValue;
    }

    private static string GetFriendlyName(Type type)
    {
        var friendlyNameAttribute = type.GetCustomAttribute<FriendlyNameAttribute>();
        if (friendlyNameAttribute != null)
        {
            return friendlyNameAttribute.Name;
        }

        return type.FullName switch
        {
            "System.Boolean" => "bool",
            "System.Byte" => "uint8",
            "System.SByte" => "int8",
            "System.Int16" => "int16",
            "System.Int32" => "int32",
            "System.Int64" => "int64",
            "System.UInt16" => "uint16",
            "System.UInt32" => "uint32",
            "System.UInt64" => "uint64",
            "System.String" => "string",
            "System.Guid" => "Guid",
            "System.DateOnly" => "DateOnly",
            "System.DateTime" => "DateTime",
            "System.TimeOnly" => "TimeOnly",
            "System.TimeSpan" => "TimeSpan",
            "Newtonsoft.Json.Linq.JObject" => "object",
            "Newtonsoft.Json.Linq.JArray" => "array",
            "KestrelToolbox.Types.UUIDv1" => "UUIDv1",
            _ => type.FullName ?? type.Name,
        };
    }

    private CompiledParser CompileParser(Type type)
    {
        if (this.compiledParsers.TryGetValue(type, out var compiledParser))
        {
            return compiledParser;
        }

        GetNullableType(type, out var baseType, out var isNullable);
        var dynamicMethod = new DynamicMethod(
            string.Empty,
            typeof(bool),
            new[] { typeof(IQueryCollection), type.MakeByRefType(), typeof(string).MakeByRefType() }
        );

        var ilGen = dynamicMethod.GetILGenerator();

        var result = ilGen.DeclareLocal(baseType);

        Action ldResult;

        var constructor = baseType.GetConstructor(Type.EmptyTypes);
        if (constructor != null)
        {
            ilGen.Emit(OpCodes.Newobj, constructor);
            ilGen.EmitStloc(result);

            ldResult = () => ilGen.EmitLdloc(result);
        }
        else if (!baseType.IsValueType)
        {
            // Call RuntimeHelpers.GetUninitializedObject because no default constructor.
            ilGen.Emit(OpCodes.Ldtoken, baseType);
            ilGen.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle", new[] { typeof(RuntimeTypeHandle) })!);
            ilGen.Emit(
                OpCodes.Call,
                typeof(RuntimeHelpers).GetMethod("GetUninitializedObject", new[] { typeof(Type) })!
            );
            ilGen.Emit(OpCodes.Castclass, type);
            ilGen.EmitStloc(result);

            ldResult = () => ilGen.EmitLdloc(result);
        }
        else
        {
            ilGen.EmitLdloca(result);
            ilGen.Emit(OpCodes.Initobj, baseType);

            ldResult = () => ilGen.EmitLdloca(result);
        }

        // Store result = null because we're always returning an object.
        if (type.IsValueType)
        {
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.Emit(OpCodes.Initobj, type);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Stind_Ref);
        }

        var stringValues = ilGen.DeclareLocal(typeof(StringValues));
        var propertyNames = new HashSet<string>();

        // Loop through properties and fetch the value out of query collection (if it exists).
        // Setup required parameters and setup defaults too.
        var nextPropertyLabel = default(Label?);
        foreach (var property in baseType.GetProperties())
        {
            var nameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(property);
            if (nameAttributes.Length == 0)
            {
                continue;
            }

            if (nameAttributes.Length != 1 || nameAttributes[0].Names.Count != 1)
            {
                throw new Exception(
                    $"Name attribute on {baseType.FullName}.{type.Name} cannot have multiple names [Name({string.Join(", ", nameAttributes.SelectMany(n => n.Names))})]."
                );
            }

            if (property.SetMethod == null)
            {
                throw new NullReferenceException(
                    $"{property.DeclaringType!.FullName}.{property.Name} requires a valid setter."
                );
            }

            if (nextPropertyLabel.HasValue)
            {
                ilGen.MarkLabel(nextPropertyLabel.Value);
            }

            nextPropertyLabel = ilGen.DefineLabel();

            var onTryGetValueLabel = ilGen.DefineLabel();
            var name = nameAttributes[0].Names[0];

            if (!propertyNames.Add(name))
            {
                throw new DuplicateNameException($"{name} is on multiple properties.");
            }

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldstr, name);
            ilGen.EmitLdloca(stringValues);
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(IQueryCollection).GetMethod(
                    "TryGetValue",
                    new[] { typeof(string), typeof(StringValues).MakeByRefType() }
                )!
            );

            // On parsed successfully, go to parsing piece. Got to default/required logic otherwise..
            ilGen.Emit(OpCodes.Brtrue, onTryGetValueLabel);

            // If we're required and didn't parse, error.
            var defaultValueAttribute = ReflectionHelpers.GetCustomAttribute<DefaultValueAttribute>(property);

            if (
                ReflectionHelpers.HasCustomAttribute<RequiredAttribute>(property)
                || ReflectionHelpers.HasCustomAttribute<RequiredMemberAttribute>(property)
            )
            {
                // Error because we're required.
                ReturnError(ilGen, $"Required query parameter '{name}' was missing.");
            }
            else if (defaultValueAttribute != null)
            {
                // Setup any defaults (if they exist).
                GetNullableType(property.PropertyType, out var localPropertyType, out var localPropertyIsNullable);

                ldResult();
                switch (localPropertyType.FullName)
                {
                    case "System.Boolean":
                        ilGen.EmitLdc(Convert.ToBoolean(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Byte":
                        ilGen.EmitLdc(Convert.ToByte(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Int16":
                        ilGen.EmitLdc(Convert.ToInt16(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Int32":
                        ilGen.EmitLdc(Convert.ToInt32(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Int64":
                        ilGen.EmitLdc(Convert.ToInt64(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.SByte":
                        ilGen.EmitLdc(Convert.ToSByte(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.UInt16":
                        ilGen.EmitLdc(Convert.ToUInt16(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.UInt32":
                        ilGen.EmitLdc(Convert.ToUInt32(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.UInt64":
                        ilGen.EmitLdc(Convert.ToUInt64(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Single":
                        ilGen.EmitLdc(Convert.ToSingle(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Double":
                        ilGen.EmitLdc(Convert.ToDouble(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.Decimal":
                        ilGen.EmitLdc(Convert.ToDecimal(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                        break;
                    case "System.String":
                        ilGen.Emit(OpCodes.Ldstr, (string)defaultValueAttribute.Value);
                        break;

                    case "System.TimeSpan":
                    {
                        if (defaultValueAttribute.Value is not TimeSpan timespan)
                        {
                            throw new InvalidCastException(
                                $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeSpan."
                            );
                        }

                        ilGen.EmitLdc(timespan.Ticks);
                        ilGen.Emit(OpCodes.Newobj, typeof(TimeSpan).GetConstructor(new[] { typeof(long) })!);
                        break;
                    }

                    case "System.DateTime":
                    {
                        if (defaultValueAttribute.Value is not DateTime dateTime)
                        {
                            throw new InvalidCastException(
                                $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeSpan."
                            );
                        }

                        ilGen.EmitLdc(dateTime.ToBinary());
                        ilGen.Emit(OpCodes.Call, typeof(DateTime).GetMethod("FromBinary", new[] { typeof(long) })!);
                        break;
                    }

                    case "System.DateOnly":
                    {
                        if (defaultValueAttribute.Value is not DateOnly dateOnly)
                        {
                            throw new InvalidCastException(
                                $"{property.DeclaringType!.FullName}.{property.Name} is not a DateOnly."
                            );
                        }

                        ilGen.EmitLdc(dateOnly.DayNumber);
                        ilGen.Emit(OpCodes.Call, typeof(DateOnly).GetMethod("FromDayNumber", new[] { typeof(int) })!);
                        break;
                    }

                    case "System.TimeOnly":
                    {
                        if (defaultValueAttribute.Value is not TimeOnly timeOnly)
                        {
                            throw new InvalidCastException(
                                $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeOnly."
                            );
                        }

                        ilGen.EmitLdc(timeOnly.Ticks);
                        ilGen.Emit(OpCodes.Newobj, typeof(TimeOnly).GetConstructor(new[] { typeof(long) })!);
                        break;
                    }

                    case "System.DateTimeOffset":
                    {
                        if (defaultValueAttribute.Value is not DateTimeOffset dateTimeOffset)
                        {
                            throw new InvalidCastException(
                                $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeSpan."
                            );
                        }

                        ilGen.EmitLdc(dateTimeOffset.Ticks);
                        ilGen.EmitLdc(dateTimeOffset.Offset.Ticks);
                        ilGen.Emit(OpCodes.Call, typeof(TimeSpan).GetMethod("FromTicks", new[] { typeof(long) })!);
                        ilGen.Emit(
                            OpCodes.Newobj,
                            typeof(DateTimeOffset).GetConstructor(new[] { typeof(long), typeof(TimeSpan) })!
                        );
                        break;
                    }

                    default:
                    {
                        if (property.PropertyType.IsEnum || localPropertyType.IsEnum)
                        {
                            ilGen.EmitLdc((int)defaultValueAttribute.Value);
                        }
                        else
                        {
                            throw new NotSupportedException(
                                $"Unknown type {property.PropertyType.FullName} has DefaultValue."
                            );
                        }

                        break;
                    }
                }

                if (localPropertyIsNullable)
                {
                    ilGen.Emit(OpCodes.Newobj, property.PropertyType.GetConstructor(new[] { localPropertyType })!);
                }

                ilGen.Emit(OpCodes.Callvirt, property.SetMethod!);

                // Parsing failed, move onto next one.
                ilGen.Emit(OpCodes.Br, nextPropertyLabel.Value);
            }
            else
            {
                // Parsing failed, move onto next one.
                ilGen.Emit(OpCodes.Br, nextPropertyLabel.Value);
            }

            // We got a string successfully, get the value and set it.
            ilGen.MarkLabel(onTryGetValueLabel);
            if (property.PropertyType.FullName == "System.Byte[]")
            {
                var stringValue = GetSingleStringValue(ilGen, stringValues, name);
                var value = ParseIndividualElement(ilGen, stringValue, name, property.PropertyType, property);
                SetValue(property, value);
            }
            else if (TryGetCustomParse(property.PropertyType, typeof(StringValues), out var tryParseMethod))
            {
                var value = ilGen.DeclareLocal(property.PropertyType);
                ilGen.EmitLdloc(stringValues);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Ldarg_2);
                ilGen.Emit(OpCodes.Call, tryParseMethod);

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                // return false, everything should be correct at this point
                ilGen.EmitLdc(0);
                ilGen.Emit(OpCodes.Ret);

                ilGen.MarkLabel(successLabel);
                SetValue(property, value);
            }
            else if (
                property.PropertyType.IsArray
                || IsListType(property.PropertyType)
                || IsSetType(property.PropertyType)
            )
            {
                var elementType = property.PropertyType;

                var isArray = elementType.IsArray;
                var arrayElementType = isArray ? elementType.GetElementType()! : elementType.GenericTypeArguments[0];
                var arrayType = IsSetType(elementType) ? typeof(List<>).MakeGenericType(arrayElementType) : elementType;

                // var count = StringValues.Count;
                // var array = new type[count];
                var stringValuesCount = ilGen.DeclareLocal(typeof(int));
                ilGen.EmitLdloca(stringValues);
                ilGen.Emit(OpCodes.Call, typeof(StringValues).GetMethod("get_Count", Type.EmptyTypes)!);
                ilGen.EmitStloc(stringValuesCount);

                var arrayLengthAttribute = ReflectionHelpers.GetCustomAttribute<ArrayLengthAttribute>(property);
                if (arrayLengthAttribute is { Minimum: > 0 })
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (list.Count < minLengthAttribute.Length)
                    ilGen.EmitLdloc(stringValuesCount);
                    ilGen.EmitLdc(arrayLengthAttribute.Minimum);
                    ilGen.Emit(OpCodes.Bge, successLabel);

                    ReturnError(
                        ilGen,
                        $"'{name}' must be longer than {arrayLengthAttribute.Minimum - 1} element{(arrayLengthAttribute.Minimum - 1 == 1 ? string.Empty : "s")}."
                    );
                    ilGen.MarkLabel(successLabel);
                }

                if (arrayLengthAttribute is { Maximum: < int.MaxValue })
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (list.Count > maxLengthAttribute.Length)
                    ilGen.EmitLdloc(stringValuesCount);
                    ilGen.EmitLdc(arrayLengthAttribute.Maximum);
                    ilGen.Emit(OpCodes.Ble, successLabel);

                    ReturnError(ilGen, $"'{name}' must be shorter than {arrayLengthAttribute.Maximum} elements.");

                    ilGen.MarkLabel(successLabel);
                }

                var array = ilGen.DeclareLocal(arrayType);
                ilGen.EmitLdloc(stringValuesCount);
                if (isArray)
                {
                    ilGen.Emit(OpCodes.Newarr, arrayElementType);
                }
                else
                {
                    ilGen.Emit(OpCodes.Newobj, arrayType.GetConstructor(new[] { typeof(int) })!);
                }

                ilGen.EmitStloc(array);

                var arrayLoopLogic = ilGen.DefineLabel();
                var arrayLoopStart = ilGen.DefineLabel();

                var arrayLoopValue = ilGen.DeclareLocal(arrayElementType);
                var arrayLoopIndex = ilGen.DeclareLocal(typeof(int));
                var stringValue = ilGen.DeclareLocal(typeof(string));

                // var index = 0;
                ilGen.EmitLdc(0);
                ilGen.EmitStloc(arrayLoopIndex);
                ilGen.Emit(OpCodes.Br, arrayLoopLogic);

                ilGen.MarkLabel(arrayLoopStart);

                // var stringValue = stringValues[index];
                ilGen.EmitLdloca(stringValues);
                ilGen.EmitLdloc(arrayLoopIndex);
                ilGen.Emit(OpCodes.Call, typeof(StringValues).GetMethod("get_Item", new[] { typeof(int) })!);
                ilGen.EmitStloc(stringValue);

                _ = ParseIndividualElement(
                    ilGen,
                    stringValue,
                    name,
                    arrayElementType,
                    new ArrayChildProvider(property),
                    arrayLoopValue
                );

                if (isArray)
                {
                    // array[index] = arrayValue;
                    ilGen.EmitLdloc(array);
                    ilGen.EmitLdloc(arrayLoopIndex);
                    ilGen.EmitLdloc(arrayLoopValue);
                    ilGen.EmitStelem(arrayElementType);
                }
                else
                {
                    // array.Add(arrayValue)
                    ilGen.EmitLdloc(array);
                    ilGen.EmitLdloc(arrayLoopValue);
                    ilGen.Emit(OpCodes.Callvirt, arrayType.GetMethod("Add", new[] { arrayElementType })!);
                }

                // index++
                ilGen.EmitLdloc(arrayLoopIndex);
                ilGen.EmitLdc(1);
                ilGen.Emit(OpCodes.Add);
                ilGen.EmitStloc(arrayLoopIndex);

                // if (index < stringValues.count)
                ilGen.MarkLabel(arrayLoopLogic);
                ilGen.EmitLdloc(arrayLoopIndex);
                ilGen.EmitLdloc(stringValuesCount);
                ilGen.Emit(OpCodes.Clt);
                ilGen.Emit(OpCodes.Brtrue, arrayLoopStart);

                if (elementType.IsGenericType && elementType.GetGenericTypeDefinition() == typeof(HashSet<>))
                {
                    var toSetMethod = typeof(Enumerable)
                        .GetMethods(BindingFlags.Public | BindingFlags.Static)
                        .Where(m =>
                            m.Name == "ToHashSet" && m.ContainsGenericParameters && m.GetGenericArguments().Length == 1
                        )
                        .Select(m => m.MakeGenericMethod(arrayElementType))
                        .First(m =>
                        {
                            var parameters = m.GetParameters();
                            if (parameters.Length != 1)
                            {
                                return false;
                            }

                            return parameters[0].ParameterType
                                == typeof(IEnumerable<>).MakeGenericType(arrayElementType);
                        });

                    var finalType = ilGen.DeclareLocal(elementType);
                    ilGen.EmitLdloc(array);
                    ilGen.Emit(OpCodes.Call, toSetMethod);
                    ilGen.EmitStloc(finalType);
                    SetValue(property, finalType);
                }
                else if (elementType.IsGenericType && elementType.GetGenericTypeDefinition() == typeof(FrozenSet<>))
                {
                    var toSetMethod = typeof(FrozenSet)
                        .GetMethods(BindingFlags.Public | BindingFlags.Static)
                        .Where(m =>
                            m.Name == "ToFrozenSet"
                            && m.ContainsGenericParameters
                            && m.GetGenericArguments().Length == 1
                        )
                        .Select(m => m.MakeGenericMethod(arrayElementType))
                        .First(m =>
                        {
                            var parameters = m.GetParameters();
                            if (parameters.Length != 2)
                            {
                                return false;
                            }

                            return parameters[0].ParameterType
                                == typeof(IEnumerable<>).MakeGenericType(arrayElementType);
                        });

                    var finalType = ilGen.DeclareLocal(elementType);
                    ilGen.EmitLdloc(array);
                    ilGen.Emit(OpCodes.Ldnull);
                    ilGen.Emit(OpCodes.Call, toSetMethod);
                    ilGen.EmitStloc(finalType);
                    SetValue(property, finalType);
                }
                else
                {
                    SetValue(property, array);
                }
            }
#pragma warning disable S1871 // Duplicate branch. Annoying to do.
            else
            {
                var stringValue = GetSingleStringValue(ilGen, stringValues, name);
                var value = ParseIndividualElement(ilGen, stringValue, name, property.PropertyType, property);
                SetValue(property, value);
            }
#pragma warning restore S1871
        }

        if (nextPropertyLabel.HasValue)
        {
            ilGen.MarkLabel(nextPropertyLabel.Value);
        }

        // resultOut = result
        ilGen.Emit(OpCodes.Ldarg_1);
        ilGen.EmitLdloc(result);
        if (type.IsValueType)
        {
            if (isNullable)
            {
                ilGen.Emit(OpCodes.Newobj, type.GetConstructor(new[] { baseType })!);
                ilGen.Emit(OpCodes.Stobj, type);
            }
            else
            {
                ilGen.Emit(OpCodes.Stobj, type);
            }
        }
        else
        {
            ilGen.Emit(OpCodes.Stind_Ref);
        }

        // error = null
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldnull);
        ilGen.Emit(OpCodes.Stind_Ref);

        ilGen.EmitLdc(true);
        ilGen.Emit(OpCodes.Ret);

        var methodDelegate = dynamicMethod.CreateDelegate(typeof(TryParseDelegate<>).MakeGenericType(type));
        compiledParser = new CompiledParser(methodDelegate, dynamicMethod);

        // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
        _ = this.TryAddCompiledParser(type, compiledParser);

        return compiledParser;

        void SetValue(PropertyInfo property, LocalBuilder value)
        {
            GetNullableType(property.PropertyType, out var localPropertyType, out var localPropertyIsNullable);

            ldResult();
            ilGen.EmitLdloc(value);
            if (localPropertyIsNullable)
            {
                ilGen.Emit(OpCodes.Newobj, property.PropertyType.GetConstructor(new[] { localPropertyType })!);
            }

            ilGen.Emit(OpCodes.Callvirt, property.SetMethod!);
        }
    }

    private bool TryAddCompiledParser(Type type, CompiledParser compiledParser)
    {
        // Lock so we can modify CompiledSerializers
        lock (this.compiledParserLock)
        {
            // Clone CompiledParsers and add to that. We cannot modify active CompiledParsers. GC will collect old one when no more threads
            // are using it.
            var newCompiledParsers = this.compiledParsers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledParsers.TryAdd(type, compiledParser);

            this.compiledParsers = newCompiledParsers;
            return added;
        }
    }
}
