// <copyright file="JsonParser.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Buffers;
using System.Buffers.Text;
using System.Collections.Frozen;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using KestrelToolbox.Dynamic.Internal;
using KestrelToolbox.Extensions.ReadOnlySequenceExtensions;
using KestrelToolbox.Internal.ILGenExtensions;
using KestrelToolbox.Serialization;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Serialization.DataAnnotations.Serialization;
using KestrelToolbox.Types;
using EmailAddressAttribute = KestrelToolbox.Serialization.DataAnnotations.EmailAddressAttribute;
using RequiredAttribute = KestrelToolbox.Serialization.DataAnnotations.RequiredAttribute;
using StringLengthAttribute = KestrelToolbox.Serialization.DataAnnotations.StringLengthAttribute;

#pragma warning disable S125 // Remove commented out code
#pragma warning disable S3011 // Accessing private members

namespace KestrelToolbox.Dynamic.Serialization.Helpers;

/// <summary>
/// Class for parsing json and validating that its values are valid dynamically at runtime.
/// </summary>
public class JsonParser
{
    /// <summary>
    /// Delegate for compiled parsers.
    /// </summary>
    /// <typeparam name="T">The type to parse.</typeparam>
    /// <param name="reader">The reader from which to parse values.</param>
    /// <param name="result">Resulting <typeparamref name="T"/> value.</param>
    /// <param name="error">Error message on failure.</param>
    /// <returns>True on success, false on failure.</returns>
    public delegate bool TryParseDelegate<T>(
        ref Utf8JsonReader reader,
        [NotNullWhen(true)] out T? result,
        [NotNullWhen(false)] out string? error
    );

    private readonly struct CompiledParser
    {
        private readonly Delegate @delegate;

        public MethodInfo Method { get; }

        public CompiledParser(Delegate @delegate, MethodInfo method)
        {
            this.Method = method;
            this.@delegate = @delegate;
        }

        public TryParseDelegate<T> GetDelegate<T>()
        {
            return (TryParseDelegate<T>)this.@delegate;
        }
    }

#if NET9_0_OR_GREATER
    private readonly System.Threading.Lock compiledParserLock = new();
#else
    private readonly object compiledParserLock = new();
#endif // NET9_0_OR_GREATER
    private Dictionary<Type, CompiledParser> compiledParsers = new();
    private Dictionary<Type, CompiledParser> compiledCustomParsers = new();
    private Dictionary<Type, CompiledParser> compiledEnumParsers = new();

    /// <summary>
    /// Adds a custom parsing callback for <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type to add custom parser for.</typeparam>
    /// <param name="callback">Callback to call when this type is encountered.</param>
    public void AddParser<T>(TryParseDelegate<T> callback)
    {
        var compiledParser = new CompiledParser(callback, callback.Method);
        if (!this.TryAddCompiledParser(typeof(T), compiledParser))
        {
            throw new ArgumentException($"{typeof(T)} has already been added.");
        }
    }

    /// <summary>
    /// Adds a custom parsing callback for <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type to add custom parser for.</typeparam>
    /// <param name="callback">Callback to call when this type is encountered.</param>
    /// <remarks>
    /// This differs from <see cref="AddParser{T}"/> in that you will already be mid-parse by the time <paramref name="callback"/>
    /// is called.
    /// </remarks>
    public void AddCustomParser<T>(TryParseDelegate<T> callback)
    {
        var compiledParser = new CompiledParser(callback, callback.Method);
        if (!this.TryAddCustomCompiledParser(typeof(T), compiledParser))
        {
            throw new ArgumentException($"{typeof(T)} has already been added.");
        }
    }

    private static readonly JsonReaderOptions DefaultReaderOptions = new()
    {
        AllowTrailingCommas = true,
        CommentHandling = JsonCommentHandling.Skip,
    };

    /// <summary>
    /// Try to parse a json string into <typeparamref name="T"/> using attributes in <see cref="KestrelToolbox.Serialization.DataAnnotations"/>.
    /// </summary>
    /// <param name="json">A raw json string to parse.</param>
    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
    /// <typeparam name="T">The type marked up with attributes from <see cref="KestrelToolbox.Serialization.DataAnnotations"/> to emit.</typeparam>
    /// <returns>True if parsing was successful, false if it was not.</returns>
    /// <remarks>
    /// If <typeparamref name="T"/> does not have a default constructor, it will be initialized to null.
    ///
    /// Any usage of <see cref="System.ComponentModel.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
    /// force initialization of value type fields.
    /// </remarks>
    public bool TryParse<T>(string json, [NotNullWhen(true)] out T? result, [NotNullWhen(false)] out string? error)
    {
        using var rental = new ArrayPoolRental(Encoding.UTF8.GetMaxByteCount(json.Length));
        var byteCount = Encoding.UTF8.GetBytes(json, rental.Array);
        return this.TryParse(rental.AsSpan(0, byteCount), out result, out error);
    }

    /// <summary>
    /// Try to parse a json string into <typeparamref name="T"/> using attributes in <see cref="KestrelToolbox.Serialization.DataAnnotations"/>.
    /// </summary>
    /// <param name="utf8">Span of bytes that is utf8 encoded string.</param>
    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
    /// <typeparam name="T">The type marked up with attributes from <see cref="KestrelToolbox.Serialization.DataAnnotations"/> to emit.</typeparam>
    /// <returns>True if parsing was successful, false if it was not.</returns>
    /// <remarks>
    /// If <typeparamref name="T"/> does not have a default constructor, it will be initialized to null.
    ///
    /// Any usage of <see cref="System.ComponentModel.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
    /// force initialization of value type fields.
    /// </remarks>
    public bool TryParse<T>(
        ReadOnlySpan<byte> utf8,
        [NotNullWhen(true)] out T? result,
        [NotNullWhen(false)] out string? error
    )
    {
        var reader = new Utf8JsonReader(utf8, DefaultReaderOptions);
        var success = this.TryParse(ref reader, out result, out error);
        return success;
    }

    /// <summary>
    /// Try to parse a json string into <typeparamref name="T"/> using attributes in <see cref="KestrelToolbox.Serialization.DataAnnotations"/>.
    /// </summary>
    /// <param name="utf8">Bytes that are a utf8 encoded string.</param>
    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
    /// <typeparam name="T">The type marked up with attributes from <see cref="KestrelToolbox.Serialization.DataAnnotations"/> to emit.</typeparam>
    /// <returns>True if parsing was successful, false if it was not.</returns>
    /// <remarks>
    /// If <typeparamref name="T"/> does not have a default constructor, it will be initialized to null.
    ///
    /// Any usage of <see cref="System.ComponentModel.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
    /// force initialization of value type fields.
    /// </remarks>
    public bool TryParse<T>(
        ReadOnlySequence<byte> utf8,
        [NotNullWhen(true)] out T? result,
        [NotNullWhen(false)] out string? error
    )
    {
        var reader = new Utf8JsonReader(utf8, DefaultReaderOptions);
        var success = this.TryParse(ref reader, out result, out error);
        return success;
    }

    /// <summary>
    /// Try to parse a json string into <typeparamref name="T"/> using attributes in <see cref="KestrelToolbox.Serialization.DataAnnotations"/>.
    /// </summary>
    /// <typeparam name="T">The type marked up with attributes from <see cref="KestrelToolbox.Serialization.DataAnnotations"/> to emit.</typeparam>
    /// <param name="reader">A raw json string to parse.</param>
    /// <param name="result">The output of the parsing. This value is null when parsing fails.</param>
    /// <param name="error">An error message for when parsing fails. This value is null when parsing succeeds.</param>
    /// <returns>True if parsing was successful, false if it was not.</returns>
    /// <remarks>
    /// If <typeparamref name="T"/> does not have a default constructor, it will be initialized to null.
    ///
    /// Any usage of <see cref="System.ComponentModel.DefaultValueAttribute"/> will take precedence over default constructor and can be a good way to
    /// force initialization of value type fields.
    /// </remarks>
    public bool TryParse<T>(
        ref Utf8JsonReader reader,
        [NotNullWhen(true)] out T? result,
        [NotNullWhen(false)] out string? error
    )
    {
        if (!this.compiledParsers.TryGetValue(typeof(T), out var compiledParser))
        {
            compiledParser = this.Compile(typeof(T));
        }

        try
        {
            return compiledParser.GetDelegate<T>()(ref reader, out result, out error);
        }
        catch (JsonException)
        {
            result = default;
            error = "Failed to parse json.";
            return false;
        }
    }

    private static void ReturnIncorrectTypeError(ILGenerator ilGen, string? name, Type type) =>
        ReturnIncorrectTypeError(ilGen, name, GetFriendlyName(type));

    private static void ReturnIncorrectTypeError(ILGenerator ilGen, string? name, string friendlyName)
    {
        var isNullLabel = ilGen.DefineLabel();

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
        ilGen.EmitLdc((int)JsonTokenType.Null);
        ilGen.Emit(OpCodes.Beq, isNullLabel);

        if (name == null)
        {
            ReturnError(ilGen, $"Was not of type {friendlyName}.");
        }
        else
        {
            ReturnError(ilGen, $"'{name}' was not of type {friendlyName}.");
        }

        ilGen.MarkLabel(isNullLabel);
        if (name == null)
        {
            ReturnError(ilGen, $"Was null. Should be of type {friendlyName}.");
        }
        else
        {
            ReturnError(ilGen, $"'{name}' was null. Should be of type {friendlyName}.");
        }
    }

    private static void ReturnError(ILGenerator ilGen, string message)
    {
        // error = message
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldstr, message);
        ilGen.Emit(OpCodes.Stind_Ref);

        // return false
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Ret);
    }

    private static void ReturnError(ILGenerator ilGen, LocalBuilder message)
    {
        // error = message
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.EmitLdloc(message);
        ilGen.Emit(OpCodes.Stind_Ref);

        // return false
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Ret);
    }

    private static LocalBuilder ReadString(ILGenerator ilGen, string? name)
    {
        var correctTokenTypeLabel = ilGen.DefineLabel();
        var value = ilGen.DeclareLocal(typeof(string));

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
        ilGen.EmitLdc((int)JsonTokenType.String);
        ilGen.Emit(OpCodes.Beq, correctTokenTypeLabel);

        ReturnIncorrectTypeError(ilGen, name, typeof(string));

        ilGen.MarkLabel(correctTokenTypeLabel);
        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("GetString", Type.EmptyTypes)!);
        ilGen.EmitStloc(value);

        return value;
    }

    private static (LocalBuilder span, LocalBuilder sequence, LocalBuilder isSequence) ReadStringSpan(
        ILGenerator ilGen,
        string? name,
        Type elementType
    )
    {
        var correctTokenTypeLabel = ilGen.DefineLabel();
        var valueSpan = ilGen.DeclareLocal(typeof(ReadOnlySpan<byte>));
        var valueSequence = ilGen.DeclareLocal(typeof(ReadOnlySequence<byte>));
        var valueIsSequence = ilGen.DeclareLocal(typeof(bool));

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
        ilGen.EmitLdc((int)JsonTokenType.String);
        ilGen.Emit(OpCodes.Beq, correctTokenTypeLabel);

        ReturnIncorrectTypeError(ilGen, name, elementType);

        var isSequenceLabel = ilGen.DefineLabel();
        var endStringReadLabel = ilGen.DefineLabel();

        ilGen.MarkLabel(correctTokenTypeLabel);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_HasValueSequence", Type.EmptyTypes)!);
        ilGen.Emit(OpCodes.Dup);
        ilGen.EmitStloc(valueIsSequence);

        ilGen.Emit(OpCodes.Brtrue, isSequenceLabel);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_ValueSpan", Type.EmptyTypes)!);
        ilGen.EmitStloc(valueSpan);
        ilGen.Emit(OpCodes.Br, endStringReadLabel);

        ilGen.MarkLabel(isSequenceLabel);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_ValueSequence", Type.EmptyTypes)!);
        ilGen.EmitStloc(valueSequence);
        ilGen.MarkLabel(endStringReadLabel);

        return (valueSpan, valueSequence, valueIsSequence);
    }

    private static LocalBuilder TryReadToken(
        ILGenerator ilGen,
        Type tokenType,
        JsonTokenType jsonTokenType,
        string method,
        string? name,
        string? friendlyName = null
    )
    {
        var correctTokenTypeLabel = ilGen.DefineLabel();
        var successLabel = ilGen.DefineLabel();
        var value = ilGen.DeclareLocal(tokenType);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
        ilGen.EmitLdc((int)jsonTokenType);
        ilGen.Emit(OpCodes.Beq, correctTokenTypeLabel);

        if (friendlyName != null)
        {
            ReturnIncorrectTypeError(ilGen, name, friendlyName);
        }
        else
        {
            ReturnIncorrectTypeError(ilGen, name, tokenType);
        }

        ilGen.MarkLabel(correctTokenTypeLabel);
        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.EmitLdloca(value);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod(method, new[] { tokenType.MakeByRefType() })!);
        ilGen.Emit(OpCodes.Brtrue, successLabel);

        if (friendlyName != null)
        {
            ReturnIncorrectTypeError(ilGen, name, friendlyName);
        }
        else
        {
            ReturnIncorrectTypeError(ilGen, name, tokenType);
        }

        ilGen.MarkLabel(successLabel);

        return value;
    }

    private static void GetNullableType(Type type, out Type baseType, out bool isNullable)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
            isNullable = true;
            baseType = type.GenericTypeArguments[0];
        }
        else
        {
            baseType = type;
            isNullable = false;
        }
    }

    private static bool IsNullableType(Type type)
    {
        GetNullableType(type, out _, out var isNullable);
        return isNullable;
    }

    private static LocalBuilder ParseIntegerInternal(
        ILGenerator ilGen,
        IPropertyProvider property,
        Type propertyType,
        JsonTokenType tokenType,
        string methodName,
        string? name
    )
    {
        var value = TryReadToken(ilGen, propertyType, tokenType, methodName, name);

        var rangeAttribute = ReflectionHelpers.GetCustomAttribute<ValidRangeAttribute>(property);
        if (rangeAttribute != null)
        {
            dynamic min,
                max;
            try
            {
                min = Convert.ChangeType(rangeAttribute.Minimum, propertyType, CultureInfo.InvariantCulture);
                max = Convert.ChangeType(rangeAttribute.Maximum, propertyType, CultureInfo.InvariantCulture);
            }
            catch
            {
                throw new InvalidProgramException(
                    $"'{name}' with type {propertyType.FullName} had invalid type in attribute Range."
                );
            }

            var isUnsigned =
                propertyType == typeof(byte)
                || propertyType == typeof(ushort)
                || propertyType == typeof(uint)
                || propertyType == typeof(ulong);

            var successLabel0 = ilGen.DefineLabel();
            var successLabel1 = ilGen.DefineLabel();

            // if (value < min)
            ilGen.EmitLdloc(value);
            ILGenExtensions.EmitLdc(ilGen, min);
            if (propertyType.FullName == "System.Decimal")
            {
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(decimal).GetMethod("op_GreaterThanOrEqual", new[] { typeof(decimal), typeof(decimal) })!
                );
                ilGen.Emit(OpCodes.Brtrue, successLabel0);
            }
            else
            {
                ilGen.Emit(isUnsigned ? OpCodes.Bge_Un : OpCodes.Bge, successLabel0);
            }

            ReturnError(ilGen, $"'{name}' was not in the range [{min}..{max}]");

            ilGen.MarkLabel(successLabel0);

            // if (value > max)
            ilGen.EmitLdloc(value);
            ILGenExtensions.EmitLdc(ilGen, max);
            if (propertyType.FullName == "System.Decimal")
            {
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(decimal).GetMethod("op_LessThanOrEqual", new[] { typeof(decimal), typeof(decimal) })!
                );
                ilGen.Emit(OpCodes.Brtrue, successLabel1);
            }
            else
            {
                ilGen.Emit(isUnsigned ? OpCodes.Ble_Un : OpCodes.Ble, successLabel1);
            }

            ReturnError(ilGen, $"'{name}' was not in the range [{min}..{max}]");
            ilGen.MarkLabel(successLabel1);
        }

        var validValuesAttribute = ReflectionHelpers.GetCustomAttribute<ValidValuesAttribute>(property);
        if (validValuesAttribute != null)
        {
            if (validValuesAttribute.ValuesAsInts == null)
            {
                throw new InvalidProgramException(
                    $"'{name}' with type {propertyType} had invalid attribute ValidValues."
                );
            }

            var successLabel = ilGen.DefineLabel();
            foreach (var attribute in validValuesAttribute.ValuesAsInts!)
            {
                // if (attribute == value)
                ilGen.EmitLdloc(value);
                ilGen.EmitLdc(attribute);
                ilGen.Emit(OpCodes.Beq, successLabel); // found it
            }

            ReturnError(
                ilGen,
                $"'{name}' must be one of: {string.Join(", ", validValuesAttribute.ValuesAsInts.OrderBy(v => v))}."
            );

            ilGen.MarkLabel(successLabel);
        }

        return value;
    }

    private static void CompareStringSpans(
        ILGenerator ilGen,
        string stringA,
        LocalBuilder spanB,
        LocalBuilder sequenceB,
        LocalBuilder isBSequence,
        Label failureLabel
    )
    {
        var isSequence = ilGen.DefineLabel();
        var endComparison = ilGen.DefineLabel();

        ilGen.EmitLdloc(isBSequence);
        ilGen.Emit(OpCodes.Brtrue, isSequence);
        {
            var stringABytes = Encoding.UTF8.GetBytes(stringA);
            ilGen.EmitLdc(stringABytes.Length);
            ilGen.EmitLdloca(spanB);
            ilGen.Emit(OpCodes.Call, typeof(ReadOnlySpan<byte>).GetMethod("get_Length", Type.EmptyTypes)!);
            ilGen.Emit(OpCodes.Bne_Un, failureLabel);

            var bPtr = ilGen.DeclareLocal(typeof(byte).MakeByRefType());
            ilGen.EmitLdloca(spanB);
            ilGen.Emit(OpCodes.Call, typeof(ReadOnlySpan<byte>).GetMethod("GetPinnableReference", Type.EmptyTypes)!);
            ilGen.EmitStloc(bPtr);

            var length = stringABytes.Length;
            var length4 = stringABytes.Length & ~0x3;
            var length8 = stringABytes.Length & ~0x7;
            if (IntPtr.Size == 8)
            {
                for (var i = 0; i < length8; i += 8)
                {
                    // if (*namePtr != *name) goto nextProperty
                    ilGen.EmitLdloc(bPtr);
                    ilGen.Emit(OpCodes.Ldind_I8);
                    ilGen.EmitLdc(BitConverter.ToInt64(stringABytes, i));
                    ilGen.Emit(OpCodes.Bne_Un, failureLabel);

                    // namePtr += 8
                    ilGen.EmitLdloc(bPtr);
                    ilGen.EmitLdc(8);
                    ilGen.Emit(OpCodes.Add);
                    ilGen.EmitStloc(bPtr);
                }
            }
            else
            {
                length8 = 0;
            }

            for (var i = length8; i < length4; i += 4)
            {
                // if (*namePtr != *name) goto nextProperty
                ilGen.EmitLdloc(bPtr);
                ilGen.Emit(OpCodes.Ldind_I4);
                ilGen.EmitLdc(BitConverter.ToInt32(stringABytes, i));
                ilGen.Emit(OpCodes.Bne_Un, failureLabel);

                // namePtr += 4
                ilGen.EmitLdloc(bPtr);
                ilGen.EmitLdc(4);
                ilGen.Emit(OpCodes.Add);
                ilGen.EmitStloc(bPtr);
            }

            for (var i = length4; i < length; i++)
            {
                // if (*namePtr != *name) goto nextProperty
                ilGen.EmitLdloc(bPtr);
                ilGen.Emit(OpCodes.Ldind_I1);
                ilGen.EmitLdc(stringABytes[i]);
                ilGen.Emit(OpCodes.Bne_Un, failureLabel);

                // namePtr += 1
                ilGen.EmitLdloc(bPtr);
                ilGen.EmitLdc(1);
                ilGen.Emit(OpCodes.Add);
                ilGen.EmitStloc(bPtr);
            }

            ilGen.Emit(OpCodes.Br, endComparison);
        }

        ilGen.MarkLabel(isSequence);
        {
            ilGen.EmitLdloc(sequenceB);
            ilGen.Emit(OpCodes.Ldstr, stringA);
            ilGen.Emit(
                OpCodes.Call,
                typeof(ReadOnlySequenceExtensions).GetMethod(
                    "SequenceEqualsUTF8",
                    new[] { typeof(ReadOnlySequence<byte>), typeof(string) }
                )!
            );
            ilGen.Emit(OpCodes.Brfalse, failureLabel);
        }

        ilGen.MarkLabel(endComparison);
    }

    private static bool TryParseTimeOnly(ReadOnlySequence<byte> source, out TimeOnly value)
    {
        if (source.IsSingleSegment)
        {
            return TryParseTimeOnly(source.FirstSpan, out value);
        }
        else
        {
            // Using a rental and copying is super inefficient, but should hopefully not happen a ton in practice
            if (source.Length > 512)
            {
                value = default;
                return false;
            }

            Span<byte> sourceSpan = stackalloc byte[(int)source.Length];
            source.CopyTo(sourceSpan);

            return TryParseTimeOnly(sourceSpan, out value);
        }
    }

    private static bool TryParseTimeOnly(ReadOnlySpan<byte> source, out TimeOnly value)
    {
        var charCount = Encoding.UTF8.GetMaxCharCount(source.Length);
        if (charCount > 256)
        {
            // arbitrary limit that shouldn't have valid dates so we don't blow stack.
            value = default;
            return false;
        }

        Span<char> chars = stackalloc char[charCount];
        var charsWritten = Encoding.UTF8.GetChars(source, chars);
        return TimeOnly.TryParse(chars.Slice(0, charsWritten), CultureInfo.InvariantCulture, out value);
    }

    private static bool TryParseDateTime(ReadOnlySequence<byte> source, out DateTime value)
    {
        if (source.IsSingleSegment)
        {
            return TryParseDateTime(source.FirstSpan, out value);
        }
        else
        {
            // Using a rental and copying is super inefficient, but should hopefully not happen a ton in practice
            if (source.Length > 512)
            {
                value = default;
                return false;
            }

            Span<byte> sourceSpan = stackalloc byte[(int)source.Length];
            source.CopyTo(sourceSpan);

            return TryParseDateTime(sourceSpan, out value);
        }
    }

    private static bool TryParseDateTime(ReadOnlySpan<byte> source, out DateTime value)
    {
        if (Utf8Parser.TryParse(source, out value, out var unused, 'O'))
        {
            return true;
        }

        if (Utf8Parser.TryParse(source, out value, out unused, 'G'))
        {
            return true;
        }

        if (Utf8Parser.TryParse(source, out value, out unused, 'R'))
        {
            return true;
        }

        if (Utf8Parser.TryParse(source, out value, out unused, 'l'))
        {
            return true;
        }

        var charCount = Encoding.UTF8.GetMaxCharCount(source.Length);
        if (charCount > 256)
        {
            // arbitrary limit that shouldn't have valid dates so we don't blow stack.
            return false;
        }

        Span<char> chars = stackalloc char[charCount];
        var charsWritten = Encoding.UTF8.GetChars(source, chars);
        return DateTime.TryParse(chars.Slice(0, charsWritten), CultureInfo.InvariantCulture, out value);
    }

    private static bool TryParseDateTimeOffset(ReadOnlySequence<byte> source, out DateTimeOffset value)
    {
        if (source.IsSingleSegment)
        {
            return TryParseDateTimeOffset(source.FirstSpan, out value);
        }
        else
        {
            // Using a rental and copying is super inefficient, but should hopefully not happen a ton in practice
            if (source.Length > 512)
            {
                value = default;
                return false;
            }

            Span<byte> sourceSpan = stackalloc byte[(int)source.Length];
            source.CopyTo(sourceSpan);

            return TryParseDateTimeOffset(sourceSpan, out value);
        }
    }

    private static bool TryParseDateTimeOffset(ReadOnlySpan<byte> source, out DateTimeOffset value)
    {
        if (Utf8Parser.TryParse(source, out value, out var unused, 'O'))
        {
            return true;
        }

        if (Utf8Parser.TryParse(source, out value, out unused, 'G'))
        {
            return true;
        }

        if (Utf8Parser.TryParse(source, out value, out unused, 'R'))
        {
            return true;
        }

        if (Utf8Parser.TryParse(source, out value, out unused, 'l'))
        {
            return true;
        }

        var charCount = Encoding.UTF8.GetMaxCharCount(source.Length);
        if (charCount > 256)
        {
            // arbitrary limit that shouldn't have valid dates so we don't blow stack.
            return false;
        }

        Span<char> chars = stackalloc char[charCount];
        var charsWritten = Encoding.UTF8.GetChars(source, chars);
        return DateTimeOffset.TryParse(chars.Slice(0, charsWritten), CultureInfo.InvariantCulture, out value);
    }

    private static bool TryParseDateOnly(ReadOnlySequence<byte> source, out DateOnly value)
    {
        if (source.IsSingleSegment)
        {
            return TryParseDateOnly(source.FirstSpan, out value);
        }
        else
        {
            // Using a rental and copying is super inefficient, but should hopefully not happen a ton in practice
            if (source.Length > 512)
            {
                value = default;
                return false;
            }

            Span<byte> sourceSpan = stackalloc byte[(int)source.Length];
            source.CopyTo(sourceSpan);

            return TryParseDateOnly(sourceSpan, out value);
        }
    }

    private static bool TryParseDateOnly(ReadOnlySpan<byte> source, out DateOnly value)
    {
        var charCount = Encoding.UTF8.GetMaxCharCount(source.Length);
        if (charCount > 256)
        {
            // arbitrary limit that shouldn't have valid dates so we don't blow stack.
            value = default;
            return false;
        }

        Span<char> chars = stackalloc char[charCount];
        var charsWritten = Encoding.UTF8.GetChars(source, chars);
        return DateOnly.TryParse(chars.Slice(0, charsWritten), CultureInfo.InvariantCulture, out value);
    }

    private static bool TryParseExactDateOnly(ReadOnlySequence<byte> source, string format, out DateOnly value)
    {
        if (source.IsSingleSegment)
        {
            return TryParseExactDateOnly(source.FirstSpan, format, out value);
        }
        else
        {
            // Using a rental and copying is super inefficient, but should hopefully not happen a ton in practice
            if (source.Length > 512)
            {
                value = default;
                return false;
            }

            Span<byte> sourceSpan = stackalloc byte[(int)source.Length];
            source.CopyTo(sourceSpan);

            return TryParseExactDateOnly(sourceSpan, format, out value);
        }
    }

    private static bool TryParseExactDateOnly(ReadOnlySpan<byte> source, string format, out DateOnly value)
    {
        var charCount = Encoding.UTF8.GetMaxCharCount(source.Length);
        if (charCount > 256)
        {
            // arbitrary limit that shouldn't have valid dates so we don't blow stack.
            value = default;
            return false;
        }

        Span<char> chars = stackalloc char[charCount];
        var charsWritten = Encoding.UTF8.GetChars(source, chars);
        return DateOnly.TryParseExact(
            chars.Slice(0, charsWritten),
            format,
            CultureInfo.InvariantCulture,
            DateTimeStyles.None,
            out value
        );
    }

    private static bool TryParseJsonNode(ref Utf8JsonReader reader, [NotNullWhen(true)] out JsonNode? value)
    {
        value = JsonNode.Parse(ref reader);
        return value != null;
    }

    private static bool TryParseJsonObject(ref Utf8JsonReader reader, [NotNullWhen(true)] out JsonObject? value)
    {
        if (reader.TokenType != JsonTokenType.StartObject)
        {
            value = default;
            return false;
        }

        var node = JsonNode.Parse(ref reader);
        if (node == null)
        {
            value = default;
            return false;
        }

        value = node.AsObject();
        return true;
    }

    private static bool TryParseJsonArray(ref Utf8JsonReader reader, [NotNullWhen(true)] out JsonArray? value)
    {
        if (reader.TokenType != JsonTokenType.StartArray)
        {
            value = default;
            return false;
        }

        var node = JsonNode.Parse(ref reader);
        if (node == null)
        {
            value = default;
            return false;
        }

        value = node.AsArray();
        return true;
    }

    private static readonly UTF8Encoding Utf8Encoding = new(
        encoderShouldEmitUTF8Identifier: false,
        throwOnInvalidBytes: true
    );

    private static bool TryParseString(ref Utf8JsonReader reader, out string? value)
    {
#pragma warning disable IDE0010
        switch (reader.TokenType)
#pragma warning restore IDE0010
        {
            case JsonTokenType.String:
            {
                value = reader.GetString();
                return true;
            }

            case JsonTokenType.False:
            {
                value = "false";
                return true;
            }

            case JsonTokenType.True:
            {
                value = "true";
                return true;
            }

            case JsonTokenType.Number:
            {
                if (reader.HasValueSequence)
                {
                    var sequence = reader.ValueSequence;
                    value = Utf8Encoding.GetString(sequence);
                }
                else
                {
                    var span = reader.ValueSpan;
                    Debug.Assert(!reader.ValueIsEscaped, "Numbers should not be escaped.");
                    value = Utf8Encoding.GetString(span);
                }

                return true;
            }

            default:
            {
                value = null;
                return false;
            }
        }
    }

    private static bool TryParseFlexibleBool(
        ref Utf8JsonReader reader,
        out bool value,
        bool allowFlexibleStrings,
        bool allowNumbers
    )
    {
        if (reader.TokenType == JsonTokenType.True)
        {
            value = true;
            return true;
        }

        if (reader.TokenType == JsonTokenType.False)
        {
            value = false;
            return true;
        }

        if (allowFlexibleStrings)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                var s = reader.GetString();
                if (bool.TryParse(s, out value))
                {
                    return true;
                }

                if (allowNumbers)
                {
                    if (s == "1")
                    {
                        value = true;
                        return true;
                    }

                    if (s == "0")
                    {
                        value = false;
                        return true;
                    }
                }
            }
        }

        if (allowNumbers)
        {
            if (reader.TokenType == JsonTokenType.Number && reader.TryGetUInt64(out var i))
            {
                if (i == 1)
                {
                    value = true;
                    return true;
                }

                if (i == 0)
                {
                    value = false;
                    return true;
                }
            }
        }

        value = default;
        return false;
    }

    private static string FormatErrorString(string format, ReadOnlySpan<byte> utf8) =>
        string.Format(CultureInfo.InvariantCulture, format, Encoding.UTF8.GetString(utf8));

    private static bool IsGenericStringDictionary(Type type)
    {
        if (IsStringFrozenDictionary(type))
        {
            return true;
        }

        var interfaceType = ReflectionHelpers.FindGenericInterface(type, typeof(IDictionary<,>));
        if (interfaceType == null)
        {
            return false;
        }

        var genericArguments = interfaceType.GetGenericArguments();
        return genericArguments[0] == typeof(string);
    }

    private static bool IsStringFrozenDictionary(Type type)
    {
        if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(FrozenDictionary<,>))
        {
            return false;
        }

        var genericArguments = type.GetGenericArguments();
        return genericArguments[0] == typeof(string);
    }

    private static bool IsGenericIEnumerable(Type type)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IEnumerable<>))
        {
            return true;
        }

        var interfaceType = ReflectionHelpers.FindGenericInterface(type, typeof(IEnumerable<>));
        if (interfaceType == null)
        {
            return false;
        }

        var genericArguments = interfaceType.GetGenericArguments();
        return genericArguments[0] == typeof(string);
    }

    private static string GetFriendlyName(Type type)
    {
        var friendlyNameAttribute = type.GetCustomAttribute<FriendlyNameAttribute>();
        if (friendlyNameAttribute != null)
        {
            return friendlyNameAttribute.Name;
        }

        return type.FullName switch
        {
            "System.Boolean" => "bool",
            "System.Byte" => "uint8",
            "System.SByte" => "int8",
            "System.Int16" => "int16",
            "System.Int32" => "int32",
            "System.Int64" => "int64",
            "System.UInt16" => "uint16",
            "System.UInt32" => "uint32",
            "System.UInt64" => "uint64",
            "System.String" => "string",
            "System.Guid" => "Guid",
            "System.DateOnly" => "DateOnly",
            "System.DateTime" => "DateTime",
            "System.TimeOnly" => "TimeOnly",
            "System.TimeSpan" => "TimeSpan",
            "Newtonsoft.Json.Linq.JObject" => "object",
            "Newtonsoft.Json.Linq.JArray" => "array",
            "System.Text.Json.Nodes.JsonObject" => "object",
            "System.Text.Json.Nodes.JsonArray" => "array",
            "KestrelToolbox.Types.UUIDv1" => "UUIDv1",
            _ => type.FullName ?? type.Name,
        };
    }

    private static bool IsListType(Type type)
    {
        return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
    }

    private static bool IsSetType(Type type)
    {
        return type.IsGenericType
            && (
                type.GetGenericTypeDefinition() == typeof(HashSet<>)
                || type.GetGenericTypeDefinition() == typeof(FrozenSet<>)
            );
    }

    private static bool TryGetCustomParse(Type type, [NotNullWhen(true)] out MethodInfo? methodInfo)
    {
        foreach (var method in type.GetMethods())
        {
            var attribute = method.GetCustomAttribute<JsonSerializableCustomParseAttribute>();
            if (attribute == null)
            {
                continue;
            }

            var parameters = method.GetParameters();
            if (parameters.Length != 3)
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} requires 3 parameters."
                );
            }

            if (parameters[1].ParameterType != type.MakeByRefType())
            {
                continue;
            }

            if (method.ReturnType != typeof(bool))
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} had invalid return type {method.ReturnType}, needed bool."
                );
            }

            if (parameters[0].ParameterType != typeof(Utf8JsonReader).MakeByRefType())
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} requires first parameter to be ref {nameof(Utf8JsonReader)}."
                );
            }

            if (parameters[2].ParameterType != typeof(string).MakeByRefType())
            {
                throw new InvalidOperationException(
                    $"{nameof(JsonSerializableCustomParseAttribute)} {type.FullName}.{method.Name} requires first parameter to be out string."
                );
            }

            methodInfo = method;
            return true;
        }

        methodInfo = default;
        return false;
    }

    private static bool TryGetJsonSerializableAttribute(
        Type type,
        [NotNullWhen(true)] out JsonSerializableAttribute? attribute
    )
    {
        var jsonSerializableAttributes = type.GetCustomAttributes<JsonSerializableAttribute>();
        foreach (var jsonSerializableAttribute in jsonSerializableAttributes)
        {
            if (jsonSerializableAttribute.Types.Length == 0 || jsonSerializableAttribute.Types.Contains(type))
            {
                attribute = jsonSerializableAttribute;
                return true;
            }
        }

        var typedefSerializableAttributes = type.GetCustomAttributes<TypedefAttribute>();
        foreach (var typedefAttribute in typedefSerializableAttributes)
        {
            if (typedefAttribute.Features.HasFlag(TypedefFeatures.JsonSerializable))
            {
                attribute = new JsonSerializableAttribute();
                return true;
            }
        }

        attribute = default;
        return false;
    }

    private CompiledParser Compile(Type type, Dictionary<Type, MethodInfo>? inFlightMethods = null)
    {
        if (this.compiledParsers.TryGetValue(type, out var compiledParser))
        {
            return compiledParser;
        }

        if (
            TryGetJsonSerializableAttribute(type, out var jsonSerializableAttribute)
            && jsonSerializableAttribute.SourceGenerationMode.HasFlag(SourceGenerationMode.Deserialize)
        )
        {
            var method =
                type.GetMethod(
                    jsonSerializableAttribute.ParsingMethodName.Split('.')[^1],
                    BindingFlags.Public | BindingFlags.Static,
                    new[]
                    {
                        typeof(Utf8JsonReader).MakeByRefType(),
                        type.MakeByRefType(),
                        typeof(string).MakeByRefType(),
                    }
                )
                ?? throw new DataException(
                    $"Could not find method {jsonSerializableAttribute.ParsingMethodName} on {type.FullName}."
                );

            compiledParser = new CompiledParser(
                method.CreateDelegate(typeof(TryParseDelegate<>).MakeGenericType(type)),
                method
            );

            _ = this.TryAddCompiledParser(type, compiledParser);
            return compiledParser;
        }

        var jsonSettingsAttribute =
            type.GetCustomAttribute<JsonSerializableSettingsAttribute>() ?? new JsonSerializableSettingsAttribute();

        inFlightMethods ??= new Dictionary<Type, MethodInfo>();
        var dynamicMethod = new DynamicMethod(
            string.Empty,
            typeof(bool),
            new[] { typeof(Utf8JsonReader).MakeByRefType(), type.MakeByRefType(), typeof(string).MakeByRefType() }
        );
        inFlightMethods.Add(type, dynamicMethod);

        var ilGen = dynamicMethod.GetILGenerator();

        var result = ilGen.DeclareLocal(type);

        // Always set result to null.
        if (type.IsValueType)
        {
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.Emit(OpCodes.Initobj, type);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_1);
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Stind_Ref);
        }

        if (this.IsHandledAsIndividualElement(type))
        {
            // if (!reader.Read())
            var readSuccessLabel = ilGen.DefineLabel();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
            ilGen.Emit(OpCodes.Brtrue, readSuccessLabel);

            ReturnError(ilGen, "Cannot parse empty string.");

            ilGen.MarkLabel(readSuccessLabel);

            if (this.compiledCustomParsers.TryGetValue(type, out var customParser))
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(OpCodes.Ldarg_2);
                ilGen.Emit(OpCodes.Call, customParser.Method);
                ilGen.Emit(OpCodes.Ret);
            }
            else
            {
                var value = this.ParseIndividualElement(
                    ilGen,
                    null,
                    type,
                    NoAttributesProvider.Default,
                    inFlightMethods
                );
                ilGen.EmitLdloc(value);
                ilGen.EmitStloc(result);
            }
        }
        else if (IsNullableType(type))
        {
            throw new NotSupportedException($"Nullable complex types are not supported. {type.FullName} was used.");
        }
        else
        {
            Action ldResult;

            var constructor = type.GetConstructor(Type.EmptyTypes);
            if (constructor != null)
            {
                ilGen.Emit(OpCodes.Newobj, constructor);
                ilGen.EmitStloc(result);

                ldResult = () => ilGen.EmitLdloc(result);
            }
            else if (!type.IsValueType)
            {
                // Call RuntimeHelpers.GetUninitializedObject because no default constructor.
                ilGen.Emit(OpCodes.Ldtoken, type);
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(Type).GetMethod("GetTypeFromHandle", new[] { typeof(RuntimeTypeHandle) })!
                );
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(RuntimeHelpers).GetMethod("GetUninitializedObject", new[] { typeof(Type) })!
                );
                ilGen.Emit(OpCodes.Castclass, type);
                ilGen.EmitStloc(result);

                ldResult = () => ilGen.EmitLdloc(result);
            }
            else
            {
                ilGen.EmitLdloca(result);
                ilGen.Emit(OpCodes.Initobj, type);

                ldResult = () => ilGen.EmitLdloca(result);
            }

            var properties = new Dictionary<string, PropertyInfo>();
            var requiredProperties = new Dictionary<string, (string name, LocalBuilder local)>();

            // Loop through all properties and setup required and defaults for them.
            foreach (var property in type.GetProperties())
            {
                var nameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(property);
                if (nameAttributes.Length == 0)
                {
                    continue;
                }

                if (property.SetMethod == null)
                {
                    throw new NullReferenceException(
                        $"{property.DeclaringType!.FullName}.{property.Name} requires a valid setter."
                    );
                }

                foreach (var name in nameAttributes.SelectMany(n => n.Names))
                {
                    properties.Add(name, property);
                }

                if (
                    ReflectionHelpers.HasCustomAttribute<RequiredAttribute>(property)
                    || ReflectionHelpers.HasCustomAttribute<RequiredMemberAttribute>(property)
                )
                {
                    var propertyMissing = ilGen.DeclareLocal(typeof(bool));
                    ilGen.EmitLdc(true);
                    ilGen.EmitStloc(propertyMissing);
                    requiredProperties.Add(property.Name, (nameAttributes[0].Names[0], propertyMissing));
                }

                var defaultValueAttribute = ReflectionHelpers.GetCustomAttribute<DefaultValueAttribute>(property);
                if (defaultValueAttribute != null)
                {
                    GetNullableType(property.PropertyType, out var localPropertyType, out var localPropertyIsNullable);

                    ldResult();
                    switch (localPropertyType.FullName)
                    {
                        case "System.Boolean":
                            ilGen.EmitLdc(Convert.ToBoolean(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Byte":
                            ilGen.EmitLdc(Convert.ToByte(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Int16":
                            ilGen.EmitLdc(Convert.ToInt16(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Int32":
                            ilGen.EmitLdc(Convert.ToInt32(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Int64":
                            ilGen.EmitLdc(Convert.ToInt64(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.SByte":
                            ilGen.EmitLdc(Convert.ToSByte(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.UInt16":
                            ilGen.EmitLdc(Convert.ToUInt16(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.UInt32":
                            ilGen.EmitLdc(Convert.ToUInt32(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.UInt64":
                            ilGen.EmitLdc(Convert.ToUInt64(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Single":
                            ilGen.EmitLdc(Convert.ToSingle(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Double":
                            ilGen.EmitLdc(Convert.ToDouble(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.Decimal":
                            ilGen.EmitLdc(Convert.ToDecimal(defaultValueAttribute.Value, CultureInfo.InvariantCulture));
                            break;
                        case "System.String":
                            ilGen.Emit(OpCodes.Ldstr, (string)defaultValueAttribute.Value);
                            break;

                        case "System.TimeOnly":
                        {
                            if (defaultValueAttribute.Value is not TimeOnly timeOnly)
                            {
                                throw new InvalidCastException(
                                    $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeOnly."
                                );
                            }

                            ilGen.EmitLdc(timeOnly.Ticks);
                            ilGen.Emit(OpCodes.Newobj, typeof(TimeOnly).GetConstructor(new[] { typeof(long) })!);
                            break;
                        }

                        case "System.TimeSpan":
                        {
                            if (defaultValueAttribute.Value is not TimeSpan timespan)
                            {
                                throw new InvalidCastException(
                                    $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeSpan."
                                );
                            }

                            ilGen.EmitLdc(timespan.Ticks);
                            ilGen.Emit(OpCodes.Newobj, typeof(TimeSpan).GetConstructor(new[] { typeof(long) })!);
                            break;
                        }

                        case "System.DateOnly":
                        {
                            if (defaultValueAttribute.Value is not DateOnly dateOnly)
                            {
                                throw new InvalidCastException(
                                    $"{property.DeclaringType!.FullName}.{property.Name} is not a DateOnly."
                                );
                            }

                            ilGen.EmitLdc(dateOnly.ToString("o", DateTimeFormatInfo.InvariantInfo));
                            ilGen.EmitLdc("o");
                            ilGen.Emit(
                                OpCodes.Call,
                                typeof(DateOnly).GetMethod("ParseExact", new[] { typeof(string), typeof(string) })!
                            );
                            break;
                        }

                        case "System.DateTime":
                        {
                            if (defaultValueAttribute.Value is not DateTime dateTime)
                            {
                                throw new InvalidCastException(
                                    $"{property.DeclaringType!.FullName}.{property.Name} is not a DateTime."
                                );
                            }

                            ilGen.EmitLdc(dateTime.ToBinary());
                            ilGen.Emit(OpCodes.Call, typeof(DateTime).GetMethod("FromBinary", new[] { typeof(long) })!);
                            break;
                        }

                        case "System.DateTimeOffset":
                        {
                            if (defaultValueAttribute.Value is not DateTimeOffset dateTimeOffset)
                            {
                                throw new InvalidCastException(
                                    $"{property.DeclaringType!.FullName}.{property.Name} is not a TimeSpan."
                                );
                            }

                            ilGen.EmitLdc(dateTimeOffset.Ticks);
                            ilGen.EmitLdc(dateTimeOffset.Offset.Ticks);
                            ilGen.Emit(OpCodes.Call, typeof(TimeSpan).GetMethod("FromTicks", new[] { typeof(long) })!);
                            ilGen.Emit(
                                OpCodes.Newobj,
                                typeof(DateTimeOffset).GetConstructor(new[] { typeof(long), typeof(TimeSpan) })!
                            );
                            break;
                        }

                        default:
                        {
                            if (property.PropertyType.IsEnum || localPropertyType.IsEnum)
                            {
                                ilGen.EmitLdc((int)defaultValueAttribute.Value);
                            }
                            else
                            {
                                throw new NotSupportedException(
                                    $"Unknown type {property.PropertyType.FullName} has DefaultValue."
                                );
                            }

                            break;
                        }
                    }

                    if (localPropertyIsNullable)
                    {
                        ilGen.Emit(OpCodes.Newobj, property.PropertyType.GetConstructor(new[] { localPropertyType })!);
                    }

                    ilGen.Emit(OpCodes.Callvirt, property.SetMethod!);
                }
            }

            // if (reader.TokenType == JsonTokenType.None && !reader.Read())
            {
                // if (reader.TokenType == JsonTokenType.None)
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                ilGen.EmitLdc((int)JsonTokenType.None);
                var skipBranch = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Bne_Un, skipBranch);

                // if (!reader.Read())
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
                ilGen.Emit(OpCodes.Brtrue, skipBranch);

                ReturnError(ilGen, "Cannot parse empty string.");

                ilGen.MarkLabel(skipBranch);
            }

            // if (reader.TokenType != JsonTokenType.StartObject)
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                ilGen.EmitLdc((int)JsonTokenType.StartObject);
                var skipBranch = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Beq_S, skipBranch);

                ReturnError(ilGen, $"'{GetFriendlyName(type)}' requires an object.");

                ilGen.MarkLabel(skipBranch);
            }

            var readerLoopStartLabel = ilGen.DefineLabel();
            var readerLoopEndLabel = ilGen.DefineLabel();
            ilGen.MarkLabel(readerLoopStartLabel);

            // while (reader.Read())
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
            ilGen.Emit(OpCodes.Brfalse, readerLoopEndLabel);

            // if (reader.TokenType == JsonTokenType.EndObject)
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                ilGen.EmitLdc((int)JsonTokenType.EndObject);
                var isNotEndObjectLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Bne_Un, isNotEndObjectLabel);

                // if (!reader.IsFinalBlock)
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_IsFinalBlock", Type.EmptyTypes)!);
                ilGen.Emit(OpCodes.Brtrue, readerLoopEndLabel);

                // reader.Read() to get through EndObject
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
                ilGen.Emit(OpCodes.Pop);
                ilGen.Emit(OpCodes.Br, readerLoopEndLabel);

                ilGen.MarkLabel(isNotEndObjectLabel);
            }

            // if (reader.TokenType != JsonTokenType.PropertyName)
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                ilGen.EmitLdc((int)JsonTokenType.PropertyName);
                ilGen.Emit(OpCodes.Bne_Un, readerLoopStartLabel); // continue;
            }

            // var propertyName = reader.GetString()
            var propertyNameSpan = ilGen.DeclareLocal(typeof(ReadOnlySpan<byte>));
            var propertyNameSequence = ilGen.DeclareLocal(typeof(ReadOnlySequence<byte>));
            var propertyNameIsSequence = ilGen.DeclareLocal(typeof(bool));

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_HasValueSequence", Type.EmptyTypes)!);
            ilGen.Emit(OpCodes.Dup);
            ilGen.EmitStloc(propertyNameIsSequence);

            // if (reader.HasValueSequence)
            {
                var isSequence = ilGen.DefineLabel();
                var endPropertyNameRead = ilGen.DefineLabel();

                ilGen.Emit(OpCodes.Brtrue, isSequence);

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_ValueSpan", Type.EmptyTypes)!);
                ilGen.EmitStloc(propertyNameSpan);
                ilGen.Emit(OpCodes.Br, endPropertyNameRead);

                ilGen.MarkLabel(isSequence);

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_ValueSequence", Type.EmptyTypes)!);
                ilGen.EmitStloc(propertyNameSequence);
                ilGen.MarkLabel(endPropertyNameRead);
            }

            // reader.Read()
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
            ilGen.Emit(OpCodes.Pop);

            // Loop through properties to find the one that matches the incoming value.
            var nextPropertyLabel = default(Label?);
            foreach (var property in properties.Values)
            {
                var nameAttribute = ReflectionHelpers.GetCustomAttribute<NameAttribute>(property);
                if (nameAttribute == null)
                {
                    continue;
                }

                if (nameAttribute.Names.Count != 1)
                {
                    throw new Exception(
                        $"Name attribute on {type.FullName}.{type.Name} cannot have multiple names [Name({string.Join(", ", nameAttribute.Names)})]."
                    );
                }

                if (nextPropertyLabel.HasValue)
                {
                    ilGen.MarkLabel(nextPropertyLabel.Value);
                }

                nextPropertyLabel = ilGen.DefineLabel();

                var name = nameAttribute.Names[0];

                // if (name == propertyName)
                {
                    CompareStringSpans(
                        ilGen,
                        name,
                        propertyNameSpan,
                        propertyNameSequence,
                        propertyNameIsSequence,
                        nextPropertyLabel.Value
                    );
                }

                var isRequired = requiredProperties.TryGetValue(property.Name, out var requiredLocal);

                var propertyProvider = new PropertyProvider(property);

                if (isRequired)
                {
                    // requiredX = false
                    ilGen.EmitLdc(false);
                    ilGen.EmitStloc(requiredLocal!.local);
                }

                var value = this.ParseIndividualElement(
                    ilGen,
                    name,
                    property.PropertyType,
                    propertyProvider,
                    inFlightMethods
                );

                // Set property to value
                ldResult();
                ilGen.EmitLdloc(value);

                ilGen.Emit(OpCodes.Callvirt, property.SetMethod!);

                ilGen.Emit(OpCodes.Br, readerLoopStartLabel);
            }

            if (nextPropertyLabel.HasValue)
            {
                ilGen.MarkLabel(nextPropertyLabel.Value);
            }

            if (!jsonSettingsAttribute.AllowExtraProperties)
            {
                // error = message
                var method = typeof(JsonParser).GetMethod(
                    nameof(FormatErrorString),
                    BindingFlags.NonPublic | BindingFlags.Static,
                    null,
                    new[] { typeof(string), propertyNameSpan.LocalType },
                    null
                )!;

                ilGen.Emit(OpCodes.Ldarg_2);
                ilGen.Emit(OpCodes.Ldstr, "Unknown property received: {0}");
                ilGen.EmitLdloc(propertyNameSpan);
                ilGen.Emit(OpCodes.Call, method);
                ilGen.Emit(OpCodes.Stind_Ref);

                // return false
                ilGen.EmitLdc(0);
                ilGen.Emit(OpCodes.Ret);
            }
            else
            {
                var tokenType = ilGen.DeclareLocal(typeof(JsonTokenType));
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                ilGen.EmitStloc(tokenType);

                // Property couldn't parse, check to see if it is an object so we can skip it.
                // if (reader.TokenType == JsonTokenType.StartObject)
                {
                    // Skip child object if it is an object
                    ilGen.EmitLdloc(tokenType);
                    ilGen.EmitLdc((int)JsonTokenType.StartObject);
                    var notEndObjectLabel = ilGen.DefineLabel();

                    ilGen.Emit(OpCodes.Bne_Un, notEndObjectLabel);

                    // reader.Skip()
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Skip", Type.EmptyTypes)!);

                    ilGen.MarkLabel(notEndObjectLabel);
                }

                // Also skip any StartArrays
                // if (reader.TokenType == JsonTokenType.StartArray)
                {
                    // Skip child object if it is an object
                    ilGen.EmitLdloc(tokenType);
                    ilGen.EmitLdc((int)JsonTokenType.StartArray);
                    var notEndObjectLabel = ilGen.DefineLabel();

                    ilGen.Emit(OpCodes.Bne_Un, notEndObjectLabel);

                    // reader.Skip()
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Skip", Type.EmptyTypes)!);

                    ilGen.MarkLabel(notEndObjectLabel);
                }
            }

            // End of loop
            ilGen.Emit(OpCodes.Br, readerLoopStartLabel);
            ilGen.MarkLabel(readerLoopEndLabel);

            // Loop through required properties and search for ones that have not been set
            if (requiredProperties.Count != 0)
            {
                // Check if any required property wasn't found.
                ilGen.EmitLdc(0);
                foreach (var propertyPair in requiredProperties)
                {
                    ilGen.EmitLdloc(propertyPair.Value.local);
                    ilGen.Emit(OpCodes.Or);
                }

                var successLabel = ilGen.DefineLabel();

                ilGen.Emit(OpCodes.Brfalse, successLabel);

                // We have a missing property. Loop through and add the names to a list.
                var errorList = ilGen.DeclareLocal(typeof(List<string>));
                ilGen.Emit(OpCodes.Newobj, typeof(List<string>).GetConstructor(Type.EmptyTypes)!);
                ilGen.EmitStloc(errorList);

                var nextLabel = default(Label?);
                foreach (var propertyPair in requiredProperties)
                {
                    if (nextLabel.HasValue)
                    {
                        ilGen.MarkLabel(nextLabel.Value);
                    }

                    nextLabel = ilGen.DefineLabel();
                    ilGen.EmitLdloc(propertyPair.Value.local);
                    ilGen.Emit(OpCodes.Brfalse, nextLabel.Value);

                    ilGen.EmitLdloc(errorList);
                    ilGen.Emit(OpCodes.Ldstr, propertyPair.Value.name);
                    ilGen.Emit(OpCodes.Callvirt, typeof(List<string>).GetMethod("Add", new[] { typeof(string) })!);
                }

                if (nextLabel.HasValue)
                {
                    ilGen.MarkLabel(nextLabel.Value);
                }

                var stringJoin = typeof(string)
                    .GetMethods(BindingFlags.Public | BindingFlags.Static)
                    .First(m =>
                    {
                        if (m.Name != "Join")
                        {
                            return false;
                        }

                        var parameters = m.GetParameters();
                        if (parameters.Length != 2)
                        {
                            return false;
                        }

                        return parameters[0].ParameterType == typeof(string)
                            && parameters[1].ParameterType == typeof(IEnumerable<string>);
                    });
                var errorMessage = ilGen.DeclareLocal(typeof(string));
                ilGen.Emit(OpCodes.Ldstr, "A required property was missing on {0}. Missing: {1}.");
                ilGen.Emit(OpCodes.Ldstr, GetFriendlyName(type));
                ilGen.Emit(OpCodes.Ldstr, ", ");
                ilGen.EmitLdloc(errorList);
                ilGen.Emit(OpCodes.Call, stringJoin);
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(string).GetMethod(
                        "Format",
                        BindingFlags.Public | BindingFlags.Static,
                        new[] { typeof(string), typeof(object), typeof(object) }
                    )!
                );
                ilGen.EmitStloc(errorMessage);

                ReturnError(ilGen, errorMessage);

                ilGen.MarkLabel(successLabel);
            }
        }

        // resultOut = result
        ilGen.Emit(OpCodes.Ldarg_1);
        ilGen.EmitLdloc(result);
        if (type.IsValueType)
        {
            ilGen.Emit(OpCodes.Stobj, type);
        }
        else
        {
            ilGen.Emit(OpCodes.Stind_Ref);
        }

        // error = null
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldnull);
        ilGen.Emit(OpCodes.Stind_Ref);

        ilGen.EmitLdc(true);
        ilGen.Emit(OpCodes.Ret);

        var methodDelegate = dynamicMethod.CreateDelegate(typeof(TryParseDelegate<>).MakeGenericType(type));
        compiledParser = new CompiledParser(methodDelegate, dynamicMethod);

        _ = this.TryAddCompiledParser(type, compiledParser);

        return compiledParser;
    }

    private CompiledParser CompileEnum(Type type)
    {
        if (!type.IsEnum)
        {
            throw new InvalidOperationException($"Cannot compile {type.Name} as enum.");
        }

        if (this.compiledEnumParsers.TryGetValue(type, out var compiledParser))
        {
            return compiledParser;
        }

        var dynamicMethod = new DynamicMethod(
            string.Empty,
            typeof(bool),
            new[] { typeof(Utf8JsonReader).MakeByRefType(), type.MakeByRefType(), typeof(string).MakeByRefType() }
        );

        var ilGen = dynamicMethod.GetILGenerator();

        // Always set result to default.
        ilGen.Emit(OpCodes.Ldarg_1);
        ilGen.EmitLdc(0);
        ilGen.Emit(OpCodes.Stind_I4);

        var enumSerializationAttribute = ReflectionHelpers.GetCustomAttribute<EnumSerializableSettingsAttribute>(type);
        var useNumbers =
            enumSerializationAttribute?.SerializationType == EnumSerializableSettingsSerializationType.Numbers;
        var invalidValue = enumSerializationAttribute?.InvalidValue;

        if (invalidValue != null)
        {
            if (invalidValue.GetType() != type)
            {
                throw new Exception(
                    $"'{type.FullName}' has invalid default value. Must be of type {type.FullName} not {invalidValue.GetType().FullName}."
                );
            }
        }

        LocalBuilder value;
        if (useNumbers)
        {
            if (invalidValue != null)
            {
                throw new Exception($"'{type.FullName}' has both UseNumbers and InvalidValue set.");
            }

            value = TryReadToken(ilGen, type, JsonTokenType.Number, "TryGetInt32", GetFriendlyName(type), "number");

            var successLabel = ilGen.DefineLabel();
            var failureLabel = ilGen.DefineLabel();
            var enumValues = Enum.GetValues(type);
            var enumValuesForError = enumValues.Cast<int>().ToList();
            var ranges = ParsingIntSwitchGenerator.GenerateRanges(enumValues.Cast<int>());
            if (enumValues.Length <= 4 || ranges.Count == 1)
            {
                foreach (var enumValue in enumValues)
                {
                    // if (attribute == value)
                    ilGen.EmitLdloc(value);
                    ilGen.EmitLdc((int)enumValue);
                    ilGen.Emit(OpCodes.Beq, successLabel); // found it
                }
            }
            else
            {
                ParsingIntSwitchGenerator.RecurseSwitch(
                    ilGen,
                    ranges,
                    0,
                    ranges.Count - 1,
                    () => ilGen.EmitLdloc(value),
                    null,
                    successLabel,
                    failureLabel
                );
            }

            ilGen.MarkLabel(failureLabel);
            ReturnError(
                ilGen,
                $"'{GetFriendlyName(type)}' must be one of: {string.Join(", ", enumValuesForError.OrderBy(v => v))}."
            );

            ilGen.MarkLabel(successLabel);
        }
        else
        {
            var stringValue = ReadString(ilGen, GetFriendlyName(type));
            value = ilGen.DeclareLocal(type);

            var enumValues = new Dictionary<string, object>();
            var successLabel = ilGen.DefineLabel();
            var failureLabel = ilGen.DefineLabel();

            foreach (var member in type.GetMembers())
            {
                var enumInfo = member as FieldInfo;
                if (enumInfo == null || enumInfo.FieldType != type)
                {
                    continue;
                }

                var enumNameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(enumInfo);
                if (enumNameAttributes.Length == 0)
                {
                    continue;
                }

                var enumValue = enumInfo.GetValue(null)!;

                foreach (var enumNameAttribute in enumNameAttributes)
                {
                    foreach (var enumName in enumNameAttribute.Names)
                    {
                        if (!enumValues.TryAdd(enumName, enumValue))
                        {
                            throw new Exception($"Duplicate name {enumName} on {type.FullName}.");
                        }
                    }
                }
            }

            if (enumValues.Count <= 4)
            {
                foreach (var (enumName, enumValue) in enumValues)
                {
                    var nextNameLabel = ilGen.DefineLabel();

                    ilGen.EmitLdloc(stringValue);
                    ilGen.EmitLdc(enumName);
                    ilGen.EmitLdc((int)StringComparison.Ordinal);
                    ilGen.Emit(
                        OpCodes.Call,
                        typeof(string).GetMethod(
                            "Equals",
                            BindingFlags.Public | BindingFlags.Static,
                            new[] { typeof(string), typeof(string), typeof(StringComparison) }
                        )!
                    );
                    ilGen.Emit(OpCodes.Brfalse, nextNameLabel);

                    ilGen.EmitLdc((int)enumValue);
                    ilGen.EmitStloc(value);
                    ilGen.Emit(OpCodes.Br, successLabel);

                    ilGen.MarkLabel(nextNameLabel);
                }
            }
            else
            {
                var ranges = ParsingStringSwitchGenerator.GenerateRanges(enumValues.Keys);
                ParsingStringSwitchGenerator.RecurseSwitch(
                    ilGen,
                    ranges,
                    0,
                    ranges.Count - 1,
                    () => ilGen.EmitLdloc(stringValue),
                    s =>
                    {
                        ilGen.EmitLdc((int)enumValues[s]);
                        ilGen.EmitStloc(value);
                    },
                    successLabel,
                    failureLabel
                );
            }

            ilGen.MarkLabel(failureLabel);
            if (invalidValue != null)
            {
                ilGen.EmitLdc((int)invalidValue);
                ilGen.EmitStloc(value);
            }
            else
            {
                ReturnError(
                    ilGen,
                    $"'{GetFriendlyName(type)}' must be one of: {string.Join(", ", enumValues.Select(p => p.Key).OrderBy(v => v))}."
                );
            }

            ilGen.MarkLabel(successLabel);
        }

        // resultOut = result
        ilGen.Emit(OpCodes.Ldarg_1);
        ilGen.EmitLdloc(value);
        ilGen.Emit(OpCodes.Stind_I4);

        // error = null
        ilGen.Emit(OpCodes.Ldarg_2);
        ilGen.Emit(OpCodes.Ldnull);
        ilGen.Emit(OpCodes.Stind_Ref);

        ilGen.EmitLdc(true);
        ilGen.Emit(OpCodes.Ret);

        var methodDelegate = dynamicMethod.CreateDelegate(typeof(TryParseDelegate<>).MakeGenericType(type));
        compiledParser = new CompiledParser(methodDelegate, dynamicMethod);

        // Lock so we can modify CompiledParsers
        _ = this.TryAddCompiledEnumParser(type, compiledParser);

        return compiledParser;
    }

    private LocalBuilder ParseIndividualElement(
        ILGenerator ilGen,
        string? name,
        Type baseElementType,
        IPropertyProvider property,
        Dictionary<Type, MethodInfo> inFlightMethods
    )
    {
        LocalBuilder value;
        var result = default(LocalBuilder);
        GetNullableType(baseElementType, out var elementType, out var isNullable);

        var isNullLabel = default(Label);
        if (isNullable || (!elementType.IsValueType && property.NullabilityState == NullabilityState.Nullable))
        {
            result = ilGen.DeclareLocal(baseElementType);

            isNullLabel = ilGen.DefineLabel();
            var isNotNullLabel = ilGen.DefineLabel();

            // if (reader.TokenType == TokenType.Null)
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
            ilGen.EmitLdc((int)JsonTokenType.Null);
            ilGen.Emit(OpCodes.Bne_Un, isNotNullLabel);

            ilGen.EmitLdc(0);
            ilGen.EmitStloc(result);
            ilGen.Emit(OpCodes.Br, isNullLabel);

            ilGen.MarkLabel(isNotNullLabel);
        }

        var customPropertyParsing = ReflectionHelpers.GetCustomAttribute<JsonSerializableCustomPropertyAttribute>(
            property
        );
        if (customPropertyParsing is { ParseMethod: not null })
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var identifiers = customPropertyParsing.ParseMethod.Split('.');
            var methodName = identifiers[^1];
            Array.Resize(ref identifiers, identifiers.Length - 1);
            for (
                var containingIdentifierIndex = identifiers.Length;
                containingIdentifierIndex > 0;
                containingIdentifierIndex--
            )
            {
                var typeName = string.Join('.', identifiers.Take(containingIdentifierIndex));
                var foundType = assemblies
                    .Select(assembly => assembly.GetType(typeName, false))
                    .OfType<Type>()
                    .FirstOrDefault();

                if (foundType == null)
                {
                    continue;
                }

                // Now walk through all identifiers until finding the correct type.
                var parentType = foundType;
                for (
                    var identifierIndex = containingIdentifierIndex;
                    identifierIndex < identifiers.Length;
                    identifierIndex++
                )
                {
                    typeName = identifiers[identifierIndex];
                    foundType = parentType.GetNestedType(typeName);
                    if (foundType == null)
                    {
                        throw new InvalidOperationException($"Could not find {customPropertyParsing.ParseMethod}.");
                    }
                }

                var method =
                    foundType.GetMethod(
                        methodName,
                        BindingFlags.Public | BindingFlags.Static,
                        new[]
                        {
                            typeof(Utf8JsonReader).MakeByRefType(),
                            elementType.MakeByRefType(),
                            typeof(string).MakeByRefType(),
                        }
                    )
                    ?? throw new InvalidOperationException(
                        $"Could not find {methodName} on {string.Join('.', identifiers)}."
                    );

                value = CallCustomMethod(ilGen, method, name, elementType);

                if (isNullable)
                {
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Newobj, baseElementType.GetConstructor(new[] { elementType })!);
                    ilGen.EmitStloc(result!);
                    ilGen.MarkLabel(isNullLabel);
                    return result!;
                }

                if (!elementType.IsValueType && property.NullabilityState == NullabilityState.Nullable)
                {
                    ilGen.EmitLdloc(value);
                    ilGen.EmitStloc(result!);
                    ilGen.MarkLabel(isNullLabel);
                    return result!;
                }

                return value;
            }
        }

        switch (elementType.FullName)
        {
            case "System.Boolean":
            {
                var boolParsingAttribute = ReflectionHelpers.GetCustomAttribute<BoolSerializableSettingsAttribute>(
                    property
                );
                var boolSerializationAttribute =
                    ReflectionHelpers.GetCustomAttribute<BoolSerializableSettingsAttribute>(property);
                var allowFlexibleStrings = boolParsingAttribute?.AllowFlexibleStrings ?? false;
                var allowNumbers = boolParsingAttribute?.AllowNumbers ?? false;
                allowNumbers |=
                    boolSerializationAttribute?.SerializationType == BoolSerializableSettingsSerializationType.Numbers;

                if (allowFlexibleStrings || allowNumbers)
                {
                    value = ilGen.DeclareLocal(elementType);

                    var method = typeof(JsonParser).GetMethod(
                        nameof(TryParseFlexibleBool),
                        BindingFlags.NonPublic | BindingFlags.Static,
                        null,
                        new[]
                        {
                            typeof(Utf8JsonReader).MakeByRefType(),
                            elementType.MakeByRefType(),
                            typeof(bool),
                            typeof(bool),
                        },
                        null
                    )!;

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.EmitLdloca(value);
                    ilGen.EmitLdc(allowFlexibleStrings);
                    ilGen.EmitLdc(allowNumbers);
                    ilGen.Emit(OpCodes.Call, method);

                    var successLabel = ilGen.DefineLabel();
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    ReturnIncorrectTypeError(ilGen, name, elementType);

                    ilGen.MarkLabel(successLabel);
                }
                else
                {
                    // Simple parsing
                    var tokenType = ilGen.DeclareLocal(typeof(int));

                    var tryTrueLabel = ilGen.DefineLabel();
                    var successLabel = ilGen.DefineLabel();
                    var failureLabel = ilGen.DefineLabel();
                    value = ilGen.DeclareLocal(typeof(bool));

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                    ilGen.EmitStloc(tokenType);

                    // if (reader.TokenType == JsonTokenType.False)
                    ilGen.Emit(OpCodes.Ldloc, tokenType);
                    ilGen.EmitLdc((int)JsonTokenType.False);
                    ilGen.Emit(OpCodes.Bne_Un, tryTrueLabel);

                    // value = false
                    ilGen.EmitLdc(false);
                    ilGen.EmitStloc(value);
                    ilGen.Emit(OpCodes.Br, successLabel);

                    ilGen.MarkLabel(tryTrueLabel);

                    // if (reader.TokenType == JsonTokenType.True)
                    ilGen.Emit(OpCodes.Ldloc, tokenType);
                    ilGen.Emit(OpCodes.Ldc_I4, (int)JsonTokenType.True);
                    ilGen.Emit(OpCodes.Bne_Un, failureLabel);

                    // value = true
                    ilGen.EmitLdc(true);
                    ilGen.Emit(OpCodes.Stloc, value);
                    ilGen.Emit(OpCodes.Br, successLabel);

                    ilGen.MarkLabel(failureLabel);
                    ReturnError(ilGen, $"'{name}' was not of type bool.");

                    ilGen.MarkLabel(successLabel);
                }

                break;
            }

            case "System.SByte":
            case "System.Int16":
            case "System.Int32":
            case "System.Int64":
            case "System.Byte":
            case "System.UInt16":
            case "System.UInt32":
            case "System.UInt64":
            case "System.Single":
            case "System.Double":
            case "System.Decimal":
            {
                var method = $"TryGet{elementType.Name}";
                value = ParseIntegerInternal(ilGen, property, elementType, JsonTokenType.Number, method, name);
                break;
            }

            case "System.String":
            {
                var allowAnyStringAttributes = ReflectionHelpers.GetCustomAttribute<AllowAnyStringAttribute>(property);
                if (allowAnyStringAttributes != null)
                {
                    value = ilGen.DeclareLocal(elementType);

                    var method = typeof(JsonParser).GetMethod(
                        nameof(TryParseString),
                        BindingFlags.NonPublic | BindingFlags.Static,
                        null,
                        new[] { typeof(Utf8JsonReader).MakeByRefType(), elementType.MakeByRefType() },
                        null
                    )!;

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.EmitLdloca(value);
                    ilGen.Emit(OpCodes.Call, method);

                    var successLabel = ilGen.DefineLabel();
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    ReturnIncorrectTypeError(ilGen, name, elementType);

                    ilGen.MarkLabel(successLabel);
                }
                else
                {
                    value = ReadString(ilGen, name);
                }

                var trimAttribute = ReflectionHelpers.GetCustomAttribute<TrimValueAttribute>(property);
                if (trimAttribute is { OnParse: true })
                {
                    // attribute.Trim()
                    ilGen.EmitLdloc(value);
                    if (trimAttribute.TrimChars == null)
                    {
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("Trim", Type.EmptyTypes)!);
                    }
                    else
                    {
                        ilGen.Emit(OpCodes.Ldstr, new string(trimAttribute.TrimChars));
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("ToCharArray", Type.EmptyTypes)!);
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("Trim", new[] { typeof(char[]) })!);
                    }

                    ilGen.EmitStloc(value);
                }

                var minLengthAttribute = ReflectionHelpers.GetCustomAttribute<MinLengthAttribute>(property);
                if (minLengthAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (value.Length < minLengthAttribute.Length)
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                    ilGen.EmitLdc(minLengthAttribute.Length);
                    ilGen.Emit(OpCodes.Bge, successLabel);

                    ReturnError(
                        ilGen,
                        $"'{name}' must be longer than {minLengthAttribute.Length - 1} character{(minLengthAttribute.Length - 1 == 1 ? string.Empty : "s")}."
                    );
                    ilGen.MarkLabel(successLabel);
                }

                var maxLengthAttribute = ReflectionHelpers.GetCustomAttribute<MaxLengthAttribute>(property);
                if (maxLengthAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (value.Length > maxLengthAttribute.Length)
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                    ilGen.EmitLdc(maxLengthAttribute.Length);
                    ilGen.Emit(OpCodes.Ble, successLabel);

                    ReturnError(ilGen, $"'{name}' must be shorter than {maxLengthAttribute.Length} characters.");
                    ilGen.MarkLabel(successLabel);
                }

                var stringLengthAttribute = ReflectionHelpers.GetCustomAttribute<StringLengthAttribute>(property);
                if (stringLengthAttribute != null)
                {
                    if (stringLengthAttribute.Minimum > 0)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (value.Length < stringLengthAttribute.MinimumLength)
                        ilGen.EmitLdloc(value);
                        ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                        ilGen.EmitLdc(stringLengthAttribute.Minimum);
                        ilGen.Emit(OpCodes.Bge, successLabel);

                        ReturnError(
                            ilGen,
                            $"'{name}' must be longer than {stringLengthAttribute.Minimum - 1} character{(stringLengthAttribute.Minimum - 1 == 1 ? string.Empty : "s")}."
                        );
                        ilGen.MarkLabel(successLabel);
                    }

                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (value.Length > stringLengthAttribute.MaximumLength)
                        ilGen.EmitLdloc(value);
                        ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);
                        ilGen.EmitLdc(stringLengthAttribute.Maximum);
                        ilGen.Emit(OpCodes.Ble, successLabel);

                        ReturnError(
                            ilGen,
                            $"'{name}' must be shorter than {stringLengthAttribute.Maximum} characters."
                        );
                        ilGen.MarkLabel(successLabel);
                    }
                }

                var validValuesAttribute = ReflectionHelpers.GetCustomAttribute<ValidValuesAttribute>(property);
                if (validValuesAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();
                    foreach (var attribute in validValuesAttribute.Values)
                    {
                        // if (attribute != value)
                        ilGen.EmitLdloc(value);
                        ilGen.Emit(OpCodes.Ldstr, attribute);
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(string).GetMethod("op_Equality", new[] { typeof(string), typeof(string) })!
                        );
                        ilGen.Emit(OpCodes.Brtrue, successLabel);
                    }

                    ReturnError(
                        ilGen,
                        $"'{name}' not an allowed value. It must be one of: {string.Join(", ", validValuesAttribute.Values.OrderBy(v => v))}."
                    );

                    ilGen.MarkLabel(successLabel);
                }

                var emailAddressAttribute = ReflectionHelpers.GetCustomAttribute<EmailAddressAttribute>(property);
                if (emailAddressAttribute != null)
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (!EmailAddressAttribute.IsValid(value))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(
                        OpCodes.Call,
                        typeof(EmailAddressAttribute).GetMethod(
                            "IsValid",
                            BindingFlags.Public | BindingFlags.Static,
                            new[] { typeof(string) }
                        )!
                    );
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    ReturnError(ilGen, $"'{name}' is not a valid email address.");

                    ilGen.MarkLabel(successLabel);
                }

                var nullOnEmptyAttribute = ReflectionHelpers.GetCustomAttribute<NullOnEmptyAttribute>(property);
                if (nullOnEmptyAttribute is { OnParse: true })
                {
                    var isNonNullLabel = ilGen.DefineLabel();

                    // if (string.IsNullOrWhitespace(attribute))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("IsNullOrEmpty", new[] { typeof(string) })!);
                    ilGen.Emit(OpCodes.Brfalse, isNonNullLabel);

                    ilGen.EmitLdc(null);
                    ilGen.EmitStloc(value);

                    ilGen.MarkLabel(isNonNullLabel);
                }

                var requiredAttribute = ReflectionHelpers.GetCustomAttribute<RequiredAttribute>(property);
                if (requiredAttribute is { AllowOnlyWhiteSpace: false })
                {
                    var successLabel = ilGen.DefineLabel();

                    // if (string.IsNullOrWhitespace(attribute))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("IsNullOrWhiteSpace", new[] { typeof(string) })!);
                    ilGen.Emit(OpCodes.Brfalse, successLabel);

                    ReturnError(ilGen, $"'{name}' must contain characters that are not whitespace.");

                    ilGen.MarkLabel(successLabel);
                }

                break;
            }

            case "System.Byte[]":
            {
                value = TryReadToken(
                    ilGen,
                    elementType,
                    JsonTokenType.String,
                    "TryGetBytesFromBase64",
                    name,
                    "base64 encoded string"
                );
                break;
            }

            case "System.Guid":
            {
                value = TryReadToken(ilGen, elementType, JsonTokenType.String, "TryGetGuid", name);
                break;
            }

            case "KestrelToolbox.Types.UUIDv1":
            {
                var guidValue = TryReadToken(
                    ilGen,
                    typeof(Guid),
                    JsonTokenType.String,
                    "TryGetGuid",
                    name,
                    GetFriendlyName(elementType)
                );
                value = ilGen.DeclareLocal(elementType);
                ilGen.EmitLdloc(guidValue);
                ilGen.Emit(OpCodes.Call, typeof(UUIDv1).GetMethod("op_Implicit", new[] { typeof(Guid) })!);
                ilGen.EmitStloc(value);

                break;
            }

            case "System.Text.Json.Nodes.JsonNode":
            case "System.Text.Json.Nodes.JsonObject":
            case "System.Text.Json.Nodes.JsonArray":
            {
                value = ilGen.DeclareLocal(elementType);

                var methodName = elementType.FullName switch
                {
                    "System.Text.Json.Nodes.JsonNode" => nameof(TryParseJsonNode),
                    "System.Text.Json.Nodes.JsonObject" => nameof(TryParseJsonObject),
                    "System.Text.Json.Nodes.JsonArray" => nameof(TryParseJsonArray),
                    _ => throw new NotImplementedException($"Trying to parse {elementType.FullName}."),
                };

                var method = typeof(JsonParser).GetMethod(
                    methodName,
                    BindingFlags.NonPublic | BindingFlags.Static,
                    null,
                    new[] { typeof(Utf8JsonReader).MakeByRefType(), elementType.MakeByRefType() },
                    null
                )!;

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, method);

                var successLabel = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Brtrue, successLabel);

                ReturnIncorrectTypeError(ilGen, name, elementType);

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.TimeOnly":
            case "System.DateTime":
            case "System.DateTimeOffset":
            {
                var (valueStringSpan, valueStringSequence, valueStringIsSequence) = ReadStringSpan(
                    ilGen,
                    name,
                    elementType
                );

                value = ilGen.DeclareLocal(elementType);
                var isSequenceLabel = ilGen.DefineLabel();
                var errorLabel = ilGen.DefineLabel();
                var successLabel = ilGen.DefineLabel();

                ilGen.EmitLdloc(valueStringIsSequence);
                ilGen.Emit(OpCodes.Brtrue, isSequenceLabel);
                {
                    ilGen.EmitLdloc(valueStringSpan);
                    ilGen.EmitLdloca(value);
                    var methodName = elementType.Name switch
                    {
                        "TimeOnly" => nameof(TryParseTimeOnly),
                        "DateTime" => nameof(TryParseDateTime),
                        "DateTimeOffset" => nameof(TryParseDateTimeOffset),
                        _ => $"TryParse{elementType.Name}",
                    };
                    var method = typeof(JsonParser).GetMethod(
                        methodName,
                        0,
                        BindingFlags.Static | BindingFlags.NonPublic,
                        null,
                        new[] { typeof(ReadOnlySpan<byte>), elementType.MakeByRefType() },
                        null
                    )!;
                    ilGen.Emit(OpCodes.Call, method);

                    ilGen.Emit(OpCodes.Brtrue, successLabel);
                    ilGen.Emit(OpCodes.Br, errorLabel);
                }

                {
                    ilGen.MarkLabel(isSequenceLabel);
                    ilGen.EmitLdloc(valueStringSequence);
                    ilGen.EmitLdloca(value);
                    var method = typeof(JsonParser).GetMethod(
                        $"TryParse{elementType.Name}",
                        0,
                        BindingFlags.Static | BindingFlags.NonPublic,
                        null,
                        new[] { typeof(ReadOnlySequence<byte>), elementType.MakeByRefType() },
                        null
                    )!;
                    ilGen.Emit(OpCodes.Call, method);

                    ilGen.Emit(OpCodes.Brtrue, successLabel);
                }

                ilGen.MarkLabel(errorLabel);

                ReturnIncorrectTypeError(ilGen, name, elementType);

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.DateOnly":
            {
                var dateOnlySettingsAttribute = property.GetCustomAttribute<DateOnlyParseSettingsAttribute>();
                var (valueStringSpan, valueStringSequence, valueStringIsSequence) = ReadStringSpan(
                    ilGen,
                    name,
                    elementType
                );

                value = ilGen.DeclareLocal(elementType);
                var isSequenceLabel = ilGen.DefineLabel();
                var errorLabel = ilGen.DefineLabel();
                var successLabel = ilGen.DefineLabel();

                var format = dateOnlySettingsAttribute?.Format;
                var methodName = format == null ? nameof(TryParseDateOnly) : nameof(TryParseExactDateOnly);
                ilGen.EmitLdloc(valueStringIsSequence);
                ilGen.Emit(OpCodes.Brtrue, isSequenceLabel);
                {
                    ilGen.EmitLdloc(valueStringSpan);
                    if (format != null)
                    {
                        ilGen.EmitLdc(format);
                    }

                    ilGen.EmitLdloca(value);
                    var methodArgs =
                        format == null
                            ? new[] { typeof(ReadOnlySpan<byte>), elementType.MakeByRefType() }
                            : new[] { typeof(ReadOnlySpan<byte>), typeof(string), elementType.MakeByRefType() };
                    var method = typeof(JsonParser).GetMethod(
                        methodName,
                        0,
                        BindingFlags.Static | BindingFlags.NonPublic,
                        null,
                        methodArgs,
                        null
                    )!;
                    ilGen.Emit(OpCodes.Call, method);

                    ilGen.Emit(OpCodes.Brtrue, successLabel);
                    ilGen.Emit(OpCodes.Br, errorLabel);
                }

                {
                    ilGen.MarkLabel(isSequenceLabel);

                    ilGen.EmitLdloc(valueStringSequence);
                    if (format != null)
                    {
                        ilGen.EmitLdc(format);
                    }

                    ilGen.EmitLdloca(value);
                    var methodArgs =
                        format == null
                            ? new[] { typeof(ReadOnlySequence<byte>), elementType.MakeByRefType() }
                            : new[] { typeof(ReadOnlySequence<byte>), typeof(string), elementType.MakeByRefType() };
                    var method = typeof(JsonParser).GetMethod(
                        methodName,
                        0,
                        BindingFlags.Static | BindingFlags.NonPublic,
                        null,
                        methodArgs,
                        null
                    )!;
                    ilGen.Emit(OpCodes.Call, method);

                    ilGen.Emit(OpCodes.Brtrue, successLabel);
                }

                ilGen.MarkLabel(errorLabel);

                ReturnIncorrectTypeError(ilGen, name, elementType);

                ilGen.MarkLabel(successLabel);
                break;
            }

            case "System.TimeSpan":
            {
                var isSequenceLabel = ilGen.DefineLabel();
                var errorLabel = ilGen.DefineLabel();
                var successLabel = ilGen.DefineLabel();
                var isNotNumberLabel = ilGen.DefineLabel();
                value = ilGen.DeclareLocal(elementType);

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                ilGen.EmitLdc((int)JsonTokenType.Number);
                ilGen.Emit(OpCodes.Bne_Un, isNotNumberLabel);

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("GetDouble", Type.EmptyTypes)!);
                ilGen.Emit(OpCodes.Call, typeof(TimeSpan).GetMethod("FromSeconds", new[] { typeof(double) })!);
                ilGen.EmitStloc(value);
                ilGen.Emit(OpCodes.Br, successLabel);

                ilGen.MarkLabel(isNotNumberLabel);

                var (valueStringSpan, valueStringSequence, valueStringIsSequence) = ReadStringSpan(
                    ilGen,
                    name,
                    elementType
                );

                ilGen.EmitLdloc(valueStringIsSequence);
                ilGen.Emit(OpCodes.Brtrue, isSequenceLabel);
                {
                    ilGen.EmitLdloc(valueStringSpan);
                    ilGen.EmitLdloca(value);
                    var method = typeof(TimeSpanParser).GetMethod(
                        "TryParse",
                        new[] { typeof(ReadOnlySpan<byte>), elementType.MakeByRefType() }
                    )!;
                    ilGen.Emit(OpCodes.Call, method);

                    ilGen.Emit(OpCodes.Brtrue, successLabel);
                    ilGen.Emit(OpCodes.Br, errorLabel);
                }

                {
                    ilGen.MarkLabel(isSequenceLabel);
                    ilGen.EmitLdloc(valueStringSequence);
                    ilGen.EmitLdloca(value);
                    var method = typeof(TimeSpanParser).GetMethod(
                        "TryParse",
                        new[] { typeof(ReadOnlySequence<byte>), elementType.MakeByRefType() }
                    )!;
                    ilGen.Emit(OpCodes.Call, method);

                    ilGen.Emit(OpCodes.Brtrue, successLabel);
                }

                ilGen.MarkLabel(errorLabel);

                ReturnIncorrectTypeError(ilGen, name, elementType);

                ilGen.MarkLabel(successLabel);
                break;
            }

            default:
            {
                if (this.compiledCustomParsers.TryGetValue(baseElementType, out var customParser))
                {
                    value = CallCustomMethod(ilGen, customParser.Method, name, elementType);
                }
                else if (TryGetCustomParse(baseElementType, out var customParseMethod))
                {
                    value = CallCustomMethod(ilGen, customParseMethod, name, elementType);
                }
                else if (elementType.IsEnum)
                {
                    var tryParseEnumMethod = this.CompileEnum(elementType).Method;

                    value = ilGen.DeclareLocal(elementType);
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.EmitLdloca(value);
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.Emit(OpCodes.Call, tryParseEnumMethod);

                    var successLabel = ilGen.DefineLabel();
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    // return false, prepend the name of the failing property.
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.EmitLdc($"{name} > ");
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.Emit(OpCodes.Ldind_Ref);
                    ilGen.Emit(
                        OpCodes.Call,
                        typeof(string).GetMethod(
                            "Concat",
                            BindingFlags.Public | BindingFlags.Static,
                            new[] { typeof(string), typeof(string) }
                        )!
                    );
                    ilGen.Emit(OpCodes.Stind_Ref);

                    ilGen.EmitLdc(0);
                    ilGen.Emit(OpCodes.Ret);

                    ilGen.MarkLabel(successLabel);
                }
                else if (elementType.IsArray || IsListType(elementType) || IsSetType(elementType))
                {
                    value = ilGen.DeclareLocal(elementType);

                    // if (reader.TokenType == JsonTokenType.StartArray)
                    {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                        ilGen.EmitLdc((int)JsonTokenType.StartArray);
                        var successLabel = ilGen.DefineLabel();
                        ilGen.Emit(OpCodes.Beq, successLabel);

                        ReturnError(ilGen, $"'{name}' was not an array.");
                        ilGen.MarkLabel(successLabel);
                    }

                    var arrayElementType = elementType.IsArray
                        ? elementType.GetElementType()!
                        : elementType.GenericTypeArguments[0];

                    // var list = new List<type>()
                    var listType = typeof(List<>).MakeGenericType(arrayElementType);
                    var list = ilGen.DeclareLocal(listType);
                    ilGen.Emit(OpCodes.Newobj, listType.GetConstructor(Type.EmptyTypes)!);
                    ilGen.EmitStloc(list);

                    var arrayLoopStart = ilGen.DefineLabel();
                    var arrayLoopEnd = ilGen.DefineLabel();

                    ilGen.MarkLabel(arrayLoopStart);

                    // while(reader.Read())
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
                    ilGen.Emit(OpCodes.Brfalse, arrayLoopEnd);

                    // if (reader.TokenType == JsonTokenType.EndArray)
                    {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                        ilGen.EmitLdc((int)JsonTokenType.EndArray);
                        ilGen.Emit(OpCodes.Beq, arrayLoopEnd);
                    }

                    var elementValue = this.ParseIndividualElement(
                        ilGen,
                        name,
                        arrayElementType,
                        new ArrayChildProvider(property),
                        inFlightMethods
                    );

                    // list.Add(elementValue)
                    ilGen.EmitLdloc(list);
                    ilGen.EmitLdloc(elementValue);
                    ilGen.Emit(OpCodes.Callvirt, listType.GetMethod("Add", new[] { arrayElementType })!);

                    ilGen.Emit(OpCodes.Br, arrayLoopStart);
                    ilGen.MarkLabel(arrayLoopEnd);

                    var arrayLengthAttribute = ReflectionHelpers.GetCustomAttribute<ArrayLengthAttribute>(property);
                    if (arrayLengthAttribute != null && arrayLengthAttribute.Minimum != 0)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (list.Count < minLengthAttribute.Length)
                        ilGen.EmitLdloc(list);
                        ilGen.Emit(OpCodes.Callvirt, listType.GetMethod("get_Count", Type.EmptyTypes)!);
                        ilGen.EmitLdc(arrayLengthAttribute.Minimum);
                        ilGen.Emit(OpCodes.Bge, successLabel);

                        ReturnError(
                            ilGen,
                            $"'{name}' must be longer than {arrayLengthAttribute.Minimum - 1} element{(arrayLengthAttribute.Minimum - 1 == 1 ? string.Empty : "s")}."
                        );
                        ilGen.MarkLabel(successLabel);
                    }

                    if (arrayLengthAttribute != null && arrayLengthAttribute.Maximum != int.MaxValue)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (list.Count > maxLengthAttribute.Length)
                        ilGen.EmitLdloc(list);
                        ilGen.Emit(OpCodes.Callvirt, listType.GetMethod("get_Count", Type.EmptyTypes)!);
                        ilGen.EmitLdc(arrayLengthAttribute.Maximum);
                        ilGen.Emit(OpCodes.Ble, successLabel);

                        ReturnError(ilGen, $"'{name}' must be shorter than {arrayLengthAttribute.Maximum} elements.");

                        ilGen.MarkLabel(successLabel);
                    }

                    ilGen.Emit(OpCodes.Ldloc, list);

                    if (elementType.IsArray)
                    {
                        ilGen.Emit(OpCodes.Callvirt, listType.GetMethod("ToArray", Type.EmptyTypes)!);
                    }
                    else if (elementType.GetGenericTypeDefinition() == typeof(HashSet<>))
                    {
                        var toSetMethod = typeof(Enumerable)
                            .GetMethods(BindingFlags.Public | BindingFlags.Static)
                            .Where(m =>
                                m.Name == "ToHashSet"
                                && m.ContainsGenericParameters
                                && m.GetGenericArguments().Length == 1
                            )
                            .Select(m => m.MakeGenericMethod(arrayElementType))
                            .First(m =>
                            {
                                var parameters = m.GetParameters();
                                if (parameters.Length != 1)
                                {
                                    return false;
                                }

                                return parameters[0].ParameterType
                                    == typeof(IEnumerable<>).MakeGenericType(arrayElementType);
                            });

                        ilGen.Emit(OpCodes.Call, toSetMethod);
                    }
                    else if (elementType.GetGenericTypeDefinition() == typeof(FrozenSet<>))
                    {
                        var toSetMethod = typeof(FrozenSet)
                            .GetMethods(BindingFlags.Public | BindingFlags.Static)
                            .Where(m =>
                                m.Name == "ToFrozenSet"
                                && m.ContainsGenericParameters
                                && m.GetGenericArguments().Length == 1
                            )
                            .Select(m => m.MakeGenericMethod(arrayElementType))
                            .First(m =>
                            {
                                var parameters = m.GetParameters();
                                if (parameters.Length != 2)
                                {
                                    return false;
                                }

                                return parameters[0].ParameterType
                                    == typeof(IEnumerable<>).MakeGenericType(arrayElementType);
                            });

                        ilGen.Emit(OpCodes.Ldnull);
                        ilGen.Emit(OpCodes.Call, toSetMethod);
                    }

                    ilGen.EmitStloc(value);
                }
                else if (IsGenericStringDictionary(elementType))
                {
                    value = ilGen.DeclareLocal(elementType);

                    // if (reader.TokenType == JsonTokenType.StartObject)
                    {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                        ilGen.EmitLdc((int)JsonTokenType.StartObject);
                        var successLabel = ilGen.DefineLabel();
                        ilGen.Emit(OpCodes.Beq, successLabel);

                        ReturnError(ilGen, $"'{name}' was not a dictionary.");
                        ilGen.MarkLabel(successLabel);
                    }

                    LocalBuilder dictionaryValue;
                    Type arrayElementType;
                    Type dictionaryType;
                    MethodInfo tryAddMethod;
                    var isFrozen = IsStringFrozenDictionary(elementType);
                    bool hasTryAddMethod;
                    if (isFrozen)
                    {
                        arrayElementType = elementType.GetGenericArguments()[1];
                        dictionaryType = typeof(Dictionary<,>).MakeGenericType(typeof(string), arrayElementType);
                        tryAddMethod = dictionaryType.GetMethod("TryAdd", new[] { typeof(string), arrayElementType })!;
                        dictionaryValue = value;
                        hasTryAddMethod = true;
                    }
                    else
                    {
                        var dictionaryInterface = ReflectionHelpers.FindGenericInterface(
                            elementType,
                            typeof(IDictionary<,>)
                        )!;
                        arrayElementType = dictionaryInterface.GetGenericArguments()[1];
                        dictionaryType = elementType;
                        hasTryAddMethod = elementType.GetGenericTypeDefinition() == typeof(Dictionary<,>);
                        if (hasTryAddMethod)
                        {
                            tryAddMethod = elementType.GetMethod("TryAdd", new[] { typeof(string), arrayElementType })!;
                        }
                        else
                        {
                            tryAddMethod = typeof(CollectionExtensions)
                                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                                .Where(m =>
                                    m.Name == nameof(CollectionExtensions.TryAdd)
                                    && m.ContainsGenericParameters
                                    && m.GetGenericArguments().Length == 2
                                )
                                .Select(m => m.MakeGenericMethod(typeof(string), arrayElementType))
                                .First(m =>
                                {
                                    var parameters = m.GetParameters();
                                    return parameters.Length == 3;
                                });
                        }

                        dictionaryValue = value;
                    }

                    // var value = new Type() // value = IDictionary type.
                    var constructor =
                        dictionaryType.GetConstructor(Type.EmptyTypes)
                        ?? throw new Exception(
                            $"'{name}' has type {elementType.FullName} which does not have a default constructor as IDictionary<,> and cannot be instantiated."
                        );
                    ilGen.Emit(OpCodes.Newobj, constructor);
                    ilGen.EmitStloc(dictionaryValue);

                    var arrayLoopStart = ilGen.DefineLabel();
                    var arrayLoopEnd = ilGen.DefineLabel();

                    ilGen.MarkLabel(arrayLoopStart);

                    // while(reader.Read())
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
                    ilGen.Emit(OpCodes.Brfalse, arrayLoopEnd);

                    // var tokenType = reader.TokenType
                    var tokenType = ilGen.DeclareLocal(typeof(JsonTokenType));
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("get_TokenType", Type.EmptyTypes)!);
                    ilGen.EmitStloc(tokenType);

                    // if (reader.TokenType == JsonTokenType.EndObject)
                    ilGen.EmitLdloc(tokenType);
                    ilGen.EmitLdc((int)JsonTokenType.EndObject);
                    ilGen.Emit(OpCodes.Beq, arrayLoopEnd);

                    // if (reader.TokenType != JsonTokenType.PropertyName)
                    {
                        var isPropertyNameLabel = ilGen.DefineLabel();
                        ilGen.EmitLdloc(tokenType);
                        ilGen.EmitLdc((int)JsonTokenType.PropertyName);
                        ilGen.Emit(OpCodes.Beq, isPropertyNameLabel);

                        ReturnError(ilGen, $"'{name}' has invalid dictionary. Expected property name.");

                        ilGen.MarkLabel(isPropertyNameLabel);
                    }

                    // var propertyName = reader.GetString()
                    var propertyName = ilGen.DeclareLocal(typeof(string));
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("GetString", Type.EmptyTypes)!);
                    ilGen.EmitStloc(propertyName);

                    // if (!reader.Read())
                    {
                        var successLabel = ilGen.DefineLabel();
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonReader).GetMethod("Read", Type.EmptyTypes)!);
                        ilGen.Emit(OpCodes.Brtrue, successLabel);

                        ReturnError(ilGen, $"'{name}' did not have valid value.");

                        ilGen.MarkLabel(successLabel);
                    }

                    var elementValue = this.ParseIndividualElement(
                        ilGen,
                        name,
                        arrayElementType,
                        new ArrayChildProvider(property),
                        inFlightMethods
                    );

                    // if (!value.TryAdd(propertyName, elementValue))
                    {
                        var successLabel = ilGen.DefineLabel();
                        ilGen.EmitLdloc(dictionaryValue);
                        ilGen.EmitLdloc(propertyName);
                        ilGen.EmitLdloc(elementValue);
                        ilGen.Emit(hasTryAddMethod ? OpCodes.Callvirt : OpCodes.Call, tryAddMethod);
                        ilGen.Emit(OpCodes.Brtrue, successLabel);

                        if (name != null)
                        {
                            ReturnError(ilGen, $"'{name}' has duplicate key.");
                        }
                        else
                        {
                            ReturnError(ilGen, $"Has duplicate key.");
                        }

                        ilGen.MarkLabel(successLabel);
                    }

                    ilGen.Emit(OpCodes.Br, arrayLoopStart);
                    ilGen.MarkLabel(arrayLoopEnd);

                    var arrayLengthAttribute = ReflectionHelpers.GetCustomAttribute<ArrayLengthAttribute>(property);
                    if (arrayLengthAttribute != null && arrayLengthAttribute.Minimum != 0)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (value.Count < arrayLengthAttribute.Minimum.Length)
                        ilGen.EmitLdloc(dictionaryValue);
                        ilGen.Emit(OpCodes.Callvirt, elementType.GetMethod("get_Count", Type.EmptyTypes)!);
                        ilGen.EmitLdc(arrayLengthAttribute.Minimum);
                        ilGen.Emit(OpCodes.Bge, successLabel);

                        ReturnError(
                            ilGen,
                            $"'{name}' must be longer than {arrayLengthAttribute.Minimum - 1} element{(arrayLengthAttribute.Minimum - 1 == 1 ? string.Empty : "s")}."
                        );
                        ilGen.MarkLabel(successLabel);
                    }

                    if (arrayLengthAttribute != null && arrayLengthAttribute.Maximum != int.MaxValue)
                    {
                        var successLabel = ilGen.DefineLabel();

                        // if (list.Count > maxLengthAttribute.Length)
                        ilGen.EmitLdloc(dictionaryValue);
                        ilGen.Emit(OpCodes.Callvirt, elementType.GetMethod("get_Count", Type.EmptyTypes)!);
                        ilGen.EmitLdc(arrayLengthAttribute.Maximum);
                        ilGen.Emit(OpCodes.Ble, successLabel);

                        ReturnError(ilGen, $"'{name}' must be shorter than {arrayLengthAttribute.Maximum} elements.");

                        ilGen.MarkLabel(successLabel);
                    }

                    ilGen.Emit(OpCodes.Ldloc, dictionaryValue);
                    ilGen.EmitStloc(value);
                }
                else
                {
                    if (!inFlightMethods.TryGetValue(elementType, out var tryParseMethod))
                    {
                        tryParseMethod = this.Compile(elementType, inFlightMethods).Method;
                    }

                    value = ilGen.DeclareLocal(elementType);
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.EmitLdloca(value);
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.Emit(OpCodes.Call, tryParseMethod);

                    var successLabel = ilGen.DefineLabel();
                    ilGen.Emit(OpCodes.Brtrue, successLabel);

                    // return false, prepend the name of the failing property.
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.EmitLdc($"{name} > ");
                    ilGen.Emit(OpCodes.Ldarg_2);
                    ilGen.Emit(OpCodes.Ldind_Ref);
                    ilGen.Emit(
                        OpCodes.Call,
                        typeof(string).GetMethod(
                            "Concat",
                            BindingFlags.Public | BindingFlags.Static,
                            new[] { typeof(string), typeof(string) }
                        )!
                    );
                    ilGen.Emit(OpCodes.Stind_Ref);

                    ilGen.EmitLdc(0);
                    ilGen.Emit(OpCodes.Ret);

                    ilGen.MarkLabel(successLabel);
                }

                break;
            }
        }

        if (isNullable)
        {
            ilGen.EmitLdloc(value);
            ilGen.Emit(OpCodes.Newobj, baseElementType.GetConstructor(new[] { elementType })!);
            ilGen.EmitStloc(result!);
            ilGen.MarkLabel(isNullLabel);
            return result!;
        }

        if (!elementType.IsValueType && property.NullabilityState == NullabilityState.Nullable)
        {
            ilGen.EmitLdloc(value);
            ilGen.EmitStloc(result!);
            ilGen.MarkLabel(isNullLabel);
            return result!;
        }

        return value;

        static LocalBuilder CallCustomMethod(ILGenerator ilGen, MethodInfo methodInfo, string? name, Type returnType)
        {
            var value = ilGen.DeclareLocal(returnType);
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdloca(value);
            ilGen.Emit(OpCodes.Ldarg_2);
            ilGen.Emit(OpCodes.Call, methodInfo);

            var successLabel = ilGen.DefineLabel();
            ilGen.Emit(OpCodes.Brtrue, successLabel);

            // return false, prepend the name of the failing property.
            if (name != null)
            {
                ilGen.Emit(OpCodes.Ldarg_2);
                ilGen.EmitLdc($"{name} > ");
                ilGen.Emit(OpCodes.Ldarg_2);
                ilGen.Emit(OpCodes.Ldind_Ref);
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(string).GetMethod(
                        "Concat",
                        BindingFlags.Public | BindingFlags.Static,
                        new[] { typeof(string), typeof(string) }
                    )!
                );
                ilGen.Emit(OpCodes.Stind_Ref);
            }

            ilGen.EmitLdc(0);
            ilGen.Emit(OpCodes.Ret);

            ilGen.MarkLabel(successLabel);
            return value;
        }
    }

    /// <summary>
    /// Gets if a <see cref="Type"/> is parsed as an individual element.
    /// </summary>
    /// <param name="type">Type to parse as an individual element.</param>
    /// <returns>True if the type supports parsing on its own.</returns>
    private bool IsHandledAsIndividualElement(Type type)
    {
        GetNullableType(type, out var baseType, out _);

        if (baseType.IsEnum)
        {
            return true;
        }

        if (type.IsArray || IsListType(type))
        {
            return true;
        }

        if (IsGenericStringDictionary(type))
        {
            return true;
        }

        if (IsGenericIEnumerable(type))
        {
            return true;
        }

        if (this.compiledCustomParsers.ContainsKey(type))
        {
            return true;
        }

        return baseType.FullName switch
        {
            "System.Boolean" => true,
            "System.Byte" => true,
            "System.SByte" => true,
            "System.Int16" => true,
            "System.Int32" => true,
            "System.Int64" => true,
            "System.UInt16" => true,
            "System.UInt32" => true,
            "System.UInt64" => true,
            "System.String" => true,
            "System.Guid" => true,
            "System.DateTime" => true,
            "System.TimeSpan" => true,
            "Newtonsoft.Json.Linq.JObject" => true,
            "Newtonsoft.Json.Linq.JArray" => true,
            "System.Text.Json.Nodes.JsonNode" => true,
            "System.Text.Json.Nodes.JsonObject" => true,
            "System.Text.Json.Nodes.JsonArray" => true,
            "KestrelToolbox.Types.UUIDv1" => true,
            _ => false,
        };
    }

    private bool TryAddCompiledParser(Type type, CompiledParser compiledParser)
    {
        // Lock so we can modify CompiledSerializers
        lock (this.compiledParserLock)
        {
            // Clone CompiledParsers and add to that. We cannot modify active CompiledParsers. GC will collect old one when no more threads
            // are using it.
            var newCompiledParsers = this.compiledParsers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledParsers.TryAdd(type, compiledParser);

            this.compiledParsers = newCompiledParsers;
            return added;
        }
    }

    private bool TryAddCustomCompiledParser(Type type, CompiledParser compiledParser)
    {
        // Lock so we can modify CompiledSerializers
        lock (this.compiledParserLock)
        {
            // Clone CompiledParsers and add to that. We cannot modify active CompiledParsers. GC will collect old one when no more threads
            // are using it.
            var newCompiledCustomParsers = this.compiledCustomParsers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledCustomParsers.TryAdd(type, compiledParser);

            this.compiledCustomParsers = newCompiledCustomParsers;
            return added;
        }
    }

    private bool TryAddCompiledEnumParser(Type type, CompiledParser compiledParser)
    {
        if (!type.IsEnum)
        {
            return true;
        }

        // Lock so we can modify CompiledSerializers
        lock (this.compiledParserLock)
        {
            // Clone CompiledParsers and add to that. We cannot modify active CompiledParsers. GC will collect old one when no more threads
            // are using it.
            var newCompiledEnumParsers = this.compiledEnumParsers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledEnumParsers.TryAdd(type, compiledParser);

            this.compiledEnumParsers = newCompiledEnumParsers;
            return added;
        }
    }
}
