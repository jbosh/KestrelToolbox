// <copyright file="ParsingStringSwitchGenerator.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Reflection.Emit;
using KestrelToolbox.Internal.ILGenExtensions;

namespace KestrelToolbox.Internal;

/// <summary>
/// Generator string switches.
/// </summary>
internal static class ParsingStringSwitchGenerator
{
    /// <summary>
    /// Range of strings.
    /// </summary>
    [SuppressMessage(
        "Minor Code Smell",
        "S1210:\"Equals\" and the comparison operators should be overridden when implementing \"IComparable\"",
        Justification = "Internal class with very specific use."
    )]
    public sealed class Range : IComparable<Range>
    {
        /// <summary>
        /// Gets or sets the start value of the range.
        /// </summary>
        public int StartValue { get; set; }

        /// <summary>
        /// Gets or sets the end value of the range.
        /// </summary>
        public int EndValue { get; set; }

        /// <summary>
        /// Gets the list of strings associated with the range (in order).
        /// </summary>
        public List<string> List { get; } = new();

        /// <summary>
        /// Gets the count of the range (end - start + 1).
        /// </summary>
        public int Count => this.EndValue - this.StartValue + 1;

        /// <inheritdoc />
        public override string ToString() => $"start: {this.StartValue}, end: {this.EndValue}";

        /// <summary>
        /// Initializes a new instance of the <see cref="Range"/> class.
        /// </summary>
        /// <param name="startValue">Initial value for this range.</param>
        public Range(string startValue)
        {
            var hash = startValue.GetHashCode();
            this.StartValue = hash;
            this.EndValue = hash;
            this.List.Add(startValue);
        }

        /// <inheritdoc />
        public int CompareTo(Range? other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }

            return other is null ? 1 : this.StartValue.CompareTo(other.StartValue);
        }
    }

    /// <summary>
    /// Generates ranges based on collection of strings.
    /// </summary>
    /// <param name="values">Collection to turn into ranges.</param>
    /// <returns>List of ranges.</returns>
    public static List<Range> GenerateRanges(IEnumerable<string> values)
    {
        var sorted = values.OrderBy(v => v.GetHashCode());
        var result = new List<Range>();
        var range = default(Range);
        foreach (var value in sorted)
        {
            var hash = value.GetHashCode();
            if (range != null && (hash == range.EndValue + 1 || hash == range.EndValue))
            {
                range.EndValue = hash;
                range.List.Add(value);
            }
            else
            {
                range = new Range(value);
                result.Add(range);
            }
        }

        return result;
    }

    /// <summary>
    /// Generates IL for this switch.
    /// </summary>
    /// <param name="ilGen">IL generator to add instructions to.</param>
    /// <param name="ranges">Ranges to process.</param>
    /// <param name="start">Start of the range (should be 0).</param>
    /// <param name="end">End of range (should be ranges.Count - 1).</param>
    /// <param name="ldValue">Generate op codes to load integer value.</param>
    /// <param name="onSuccess">Generate op codes to store the string at its definition.</param>
    /// <param name="successLabel">Label for successfully finding the string.</param>
    /// <param name="failureLabel">Label for failing to find string.</param>
    /// <param name="hashValue">Hash value (if already generated) for the string. Null if not, it will be generated internally.</param>
    public static void RecurseSwitch(
        ILGenerator ilGen,
        List<Range> ranges,
        int start,
        int end,
        Action ldValue,
        Action<string> onSuccess,
        Label successLabel,
        Label failureLabel,
        LocalBuilder? hashValue = null
    )
    {
        Debug.Assert(start <= end, "Start was too big.");

        LocalBuilder hash;
        if (hashValue != null)
        {
            hash = hashValue;
        }
        else
        {
            hash = ilGen.DeclareLocal(typeof(int));
            ldValue();
            ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("GetHashCode", Type.EmptyTypes)!);
            ilGen.EmitStloc(hash);
        }

        if (start == end)
        {
            var range = ranges[start];
            Debug.Assert(range.Count != 0, "Range should have something in it.");
            foreach (var value in range.List)
            {
                var nextNameLabel = ilGen.DefineLabel();

                ldValue();
                ilGen.Emit(OpCodes.Ldstr, value);
                ilGen.EmitLdc((int)StringComparison.Ordinal);
                ilGen.Emit(
                    OpCodes.Call,
                    typeof(string).GetMethod(
                        "Equals",
                        BindingFlags.Public | BindingFlags.Static,
                        new[] { typeof(string), typeof(string), typeof(StringComparison) }
                    )!
                );
                ilGen.Emit(OpCodes.Brfalse, nextNameLabel);

                onSuccess(value);
                ilGen.Emit(OpCodes.Br, successLabel);

                ilGen.MarkLabel(nextNameLabel);
            }

            ilGen.Emit(OpCodes.Br, failureLabel);
            return;
        }

        var mid = start + ((end - start) / 2);

        var midValue = ranges[mid].EndValue;
        var leftLabel = ilGen.DefineLabel();
        var rightLabel = ilGen.DefineLabel();

        ilGen.EmitLdloc(hash);
        ilGen.EmitLdc(midValue);
        ilGen.Emit(OpCodes.Ble, leftLabel);
        ilGen.Emit(OpCodes.Br, rightLabel);

        ilGen.MarkLabel(leftLabel);
        RecurseSwitch(ilGen, ranges, start, mid, ldValue, onSuccess, successLabel, failureLabel, hash);

        ilGen.MarkLabel(rightLabel);
        RecurseSwitch(ilGen, ranges, mid + 1, end, ldValue, onSuccess, successLabel, failureLabel, hash);
    }
}
