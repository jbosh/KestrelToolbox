// <copyright file="SimpleSpinLock.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Runtime.CompilerServices;

namespace KestrelToolbox.Internal;

/// <summary>
/// Spin lock that is much lighter weight than dotnet built-in because it strips features.
/// </summary>
public struct SimpleSpinLock
{
    private int value;

    /// <summary>
    /// Tries to enter the lock.
    /// </summary>
    /// <returns>True if the lock was acquired, false if not.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool TryEnter()
    {
        var oldValue = Interlocked.CompareExchange(ref this.value, 1, 0);
        return oldValue == 0;
    }

    /// <summary>
    /// Acquires the lock.
    /// </summary>
    public void Enter()
    {
        if (this.TryEnter())
        {
            return;
        }

        var spinner = default(SpinWait);
        while (!this.TryEnter())
        {
            spinner.SpinOnce();
        }
    }

    /// <summary>
    /// Releases the lock.
    /// </summary>
    public void Exit()
    {
        var oldValue = Interlocked.Exchange(ref this.value, 0);
        Debug.Assert(oldValue == 1, "Lock wasn't locked.");
    }
}
