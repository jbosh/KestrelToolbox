// <copyright file="ILGenExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection.Emit;

namespace KestrelToolbox.Internal.ILGenExtensions;

/// <summary>
/// Extension methods to make generating IL a little easier.
/// </summary>
public static class ILGenExtensions
{
    /// <summary>
    /// Emit ldloc instruction efficiently. This will automatically emit ldloc_0, ldloc_1, etc...
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="localBuilder">Variable to load.</param>
    public static void EmitLdloc(this ILGenerator ilGen, LocalBuilder localBuilder)
    {
        switch (localBuilder.LocalIndex)
        {
            case 0:
                ilGen.Emit(OpCodes.Ldloc_0);
                break;
            case 1:
                ilGen.Emit(OpCodes.Ldloc_1);
                break;
            case 2:
                ilGen.Emit(OpCodes.Ldloc_2);
                break;
            case 3:
                ilGen.Emit(OpCodes.Ldloc_3);
                break;
            default:
                ilGen.Emit(OpCodes.Ldloc, localBuilder);
                break;
        }
    }

    /// <summary>
    /// Emit ldloca instruction efficiently. This will automatically emit ldloca or ldloca_s.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="localBuilder">Variable to load.</param>
    public static void EmitLdloca(this ILGenerator ilGen, LocalBuilder localBuilder)
    {
        var op = localBuilder.LocalIndex < byte.MaxValue ? OpCodes.Ldloca_S : OpCodes.Ldloca;
        ilGen.Emit(op, localBuilder);
    }

    /// <summary>
    /// Emit stelem instruction correctly.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="type">The type of element being stored.</param>
    public static void EmitStelem(this ILGenerator ilGen, Type type)
    {
        OpCode op;
        switch (type.FullName)
        {
            case "System.Byte":
            {
                op = OpCodes.Stelem_I1;
                break;
            }

            case "System.SByte":
            {
                op = OpCodes.Stelem_I1;
                break;
            }

            case "System.UInt16":
            {
                op = OpCodes.Stelem_I2;
                break;
            }

            case "System.Int16":
            {
                op = OpCodes.Stelem_I2;
                break;
            }

            case "System.UInt32":
            {
                op = OpCodes.Stelem_I4;
                break;
            }

            case "System.Int32":
            {
                op = OpCodes.Stelem_I4;
                break;
            }

            case "System.UInt64":
            {
                op = OpCodes.Stelem_I8;
                break;
            }

            case "System.Int64":
            {
                op = OpCodes.Stelem_I8;
                break;
            }

            case "nativeint":
            {
                op = OpCodes.Stelem_I;
                break;
            }

            default:
            {
                if (type.IsValueType)
                {
                    ilGen.Emit(OpCodes.Stelem, type);
                    return;
                }

                op = OpCodes.Stelem_Ref;
                break;
            }
        }

        ilGen.Emit(op);
    }

    /// <summary>
    /// Emit ldelem instruction correctly.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="type">The type of element being loaded.</param>
    public static void EmitLdelem(this ILGenerator ilGen, Type type)
    {
        OpCode op;
        switch (type.FullName)
        {
            case "System.Byte":
            {
                op = OpCodes.Ldelem_I1;
                break;
            }

            case "System.SByte":
            {
                op = OpCodes.Ldelem_I1;
                break;
            }

            case "System.UInt16":
            {
                op = OpCodes.Ldelem_I2;
                break;
            }

            case "System.Int16":
            {
                op = OpCodes.Ldelem_I2;
                break;
            }

            case "System.UInt32":
            {
                op = OpCodes.Ldelem_I4;
                break;
            }

            case "System.Int32":
            {
                op = OpCodes.Ldelem_I4;
                break;
            }

            case "System.UInt64":
            {
                op = OpCodes.Ldelem_I8;
                break;
            }

            case "System.Int64":
            {
                op = OpCodes.Ldelem_I8;
                break;
            }

            case "nativeint":
            {
                op = OpCodes.Ldelem_I;
                break;
            }

            default:
            {
                if (type.IsValueType)
                {
                    ilGen.Emit(OpCodes.Ldelem, type);
                    return;
                }

                op = OpCodes.Ldelem_Ref;
                break;
            }
        }

        ilGen.Emit(op);
    }

    /// <summary>
    /// Emit stloc instruction efficiently. This will automatically emit stloc_0, stloc_1, etc...
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="localBuilder">Variable to store.</param>
    public static void EmitStloc(this ILGenerator ilGen, LocalBuilder localBuilder)
    {
        switch (localBuilder.LocalIndex)
        {
            case 0:
                ilGen.Emit(OpCodes.Stloc_0);
                break;
            case 1:
                ilGen.Emit(OpCodes.Stloc_1);
                break;
            case 2:
                ilGen.Emit(OpCodes.Stloc_2);
                break;
            case 3:
                ilGen.Emit(OpCodes.Stloc_3);
                break;
            default:
                ilGen.Emit(OpCodes.Stloc, localBuilder);
                break;
        }
    }

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, bool value) => ilGen.EmitLdc(value ? 1 : 0);

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, int value)
    {
        switch (value)
        {
            case -1:
                ilGen.Emit(OpCodes.Ldc_I4_M1);
                break;
            case 0:
                ilGen.Emit(OpCodes.Ldc_I4_0);
                break;
            case 1:
                ilGen.Emit(OpCodes.Ldc_I4_1);
                break;
            case 2:
                ilGen.Emit(OpCodes.Ldc_I4_2);
                break;
            case 3:
                ilGen.Emit(OpCodes.Ldc_I4_3);
                break;
            case 4:
                ilGen.Emit(OpCodes.Ldc_I4_4);
                break;
            case 5:
                ilGen.Emit(OpCodes.Ldc_I4_5);
                break;
            case 6:
                ilGen.Emit(OpCodes.Ldc_I4_6);
                break;
            case 7:
                ilGen.Emit(OpCodes.Ldc_I4_7);
                break;
            case 8:
                ilGen.Emit(OpCodes.Ldc_I4_8);
                break;
            default:
                if (value is >= sbyte.MinValue and < sbyte.MaxValue)
                {
                    ilGen.Emit(OpCodes.Ldc_I4_S, (sbyte)value);
                }
                else
                {
                    ilGen.Emit(OpCodes.Ldc_I4, value);
                }

                break;
        }
    }

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, uint value) => ilGen.EmitLdc((int)value);

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, ulong value) => ilGen.EmitLdc((long)value);

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, long value)
    {
        if (value is >= int.MinValue and <= int.MaxValue)
        {
            ilGen.EmitLdc((int)value);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldc_I8, value);
        }
    }

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, float value) => ilGen.Emit(OpCodes.Ldc_R4, value);

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, double value) => ilGen.Emit(OpCodes.Ldc_R8, value);

    /// <summary>
    /// Emulate loading a decimal value as a constant using il instructions.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, decimal value)
    {
        var bits = decimal.GetBits(value);
        var sign = (bits[3] & 0x80000000) != 0;
        var scale = (bits[3] >> 16) & 0x7f;
        ilGen.EmitLdc(bits[0]);
        ilGen.EmitLdc(bits[1]);
        ilGen.EmitLdc(bits[2]);
        ilGen.EmitLdc(sign);
        ilGen.EmitLdc(scale);
        var constructor = typeof(decimal).GetConstructor(
            new[] { typeof(int), typeof(int), typeof(int), typeof(bool), typeof(byte) }
        )!;
        ilGen.Emit(OpCodes.Newobj, constructor);
    }

    /// <summary>
    /// Emit ldc instruction efficiently. This will automatically emit the shortest form based on the constant.
    /// </summary>
    /// <param name="ilGen"><see cref="ILGenerator"/> to emit from.</param>
    /// <param name="value">Constant to load.</param>
    public static void EmitLdc(this ILGenerator ilGen, string? value)
    {
        if (value == null)
        {
            ilGen.Emit(OpCodes.Ldnull);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldstr, value);
        }
    }
}
