// <copyright file="BitManipulation.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Internal;

/// <summary>
/// Utility class for doing general bit manipulation.
/// </summary>
public static class BitManipulation
{
    /// <summary>
    /// Byte swaps the endianness of a value.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static short SwapEndian(short value) => (short)SwapEndian((ushort)value);

    /// <summary>
    /// Byte swaps the endianness of a value.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ushort SwapEndian(ushort value)
    {
        var result = (0x00FF & (value >> 8)) | (0xFF00 & (value << 8));
        return (ushort)result;
    }

    /// <summary>
    /// Byte swaps the endianness of a value.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static int SwapEndian(int value) => (int)SwapEndian((uint)value);

    /// <summary>
    /// Byte swaps the endianness of a value.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static uint SwapEndian(uint value)
    {
        var result =
            (0x000000FF & (value >> 24))
            | (0x0000FF00 & (value >> 8))
            | (0x00FF0000 & (value << 8))
            | (0xFF000000 & (value << 24));
        return result;
    }

    /// <summary>
    /// Byte swaps the endianness of a value.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static long SwapEndian(long value) => (long)SwapEndian((ulong)value);

    /// <summary>
    /// Byte swaps the endianness of a value.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ulong SwapEndian(ulong value)
    {
        var result =
            (0x00000000000000FF & (value >> 56))
            | (0x000000000000FF00 & (value >> 40))
            | (0x0000000000FF0000 & (value >> 24))
            | (0x00000000FF000000 & (value >> 8))
            | (0x000000FF00000000 & (value << 8))
            | (0x0000FF0000000000 & (value << 24))
            | (0x00FF000000000000 & (value << 40))
            | (0xFF00000000000000 & (value << 56));
        return result;
    }

    /// <summary>
    /// Byte swaps little endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static short ConvertFromLittleEndian(short value) => (short)ConvertFromLittleEndian((ushort)value);

    /// <summary>
    /// Byte swaps little endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ushort ConvertFromLittleEndian(ushort value) =>
        !BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps little endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static int ConvertFromLittleEndian(int value) => (int)ConvertFromLittleEndian((uint)value);

    /// <summary>
    /// Byte swaps little endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static uint ConvertFromLittleEndian(uint value) => !BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps little endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static long ConvertFromLittleEndian(long value) => (long)ConvertFromLittleEndian((ulong)value);

    /// <summary>
    /// Byte swaps little endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ulong ConvertFromLittleEndian(ulong value) =>
        !BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps big endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static short ConvertFromBigEndian(short value) => (short)ConvertFromBigEndian((ushort)value);

    /// <summary>
    /// Byte swaps big endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ushort ConvertFromBigEndian(ushort value) => BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps big endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static int ConvertFromBigEndian(int value) => (int)ConvertFromBigEndian((uint)value);

    /// <summary>
    /// Byte swaps big endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static uint ConvertFromBigEndian(uint value) => BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps big endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static long ConvertFromBigEndian(long value) => (long)ConvertFromBigEndian((ulong)value);

    /// <summary>
    /// Byte swaps big endian to native endianness of the platform.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ulong ConvertFromBigEndian(ulong value) => BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps native endianness of the platform to little endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static short ConvertToLittleEndian(short value) => (short)ConvertFromLittleEndian((ushort)value);

    /// <summary>
    /// Byte swaps native endianness of the platform to little endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ushort ConvertToLittleEndian(ushort value) =>
        !BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps native endianness of the platform to little endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static int ConvertToLittleEndian(int value) => (int)ConvertToLittleEndian((uint)value);

    /// <summary>
    /// Byte swaps native endianness of the platform to little endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static uint ConvertToLittleEndian(uint value) => !BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps native endianness of the platform to little endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static long ConvertToLittleEndian(long value) => (long)ConvertToLittleEndian((ulong)value);

    /// <summary>
    /// Byte swaps native endianness of the platform to little endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ulong ConvertToLittleEndian(ulong value) => !BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps native endianness of the platform to big endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static short ConvertToBigEndian(short value) => (short)ConvertToBigEndian((ushort)value);

    /// <summary>
    /// Byte swaps native endianness of the platform to big endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ushort ConvertToBigEndian(ushort value) => BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps native endianness of the platform to big endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static int ConvertToBigEndian(int value) => (int)ConvertToBigEndian((uint)value);

    /// <summary>
    /// Byte swaps native endianness of the platform to big endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static uint ConvertToBigEndian(uint value) => BitConverter.IsLittleEndian ? SwapEndian(value) : value;

    /// <summary>
    /// Byte swaps native endianness of the platform to big endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static long ConvertToBigEndian(long value) => (long)ConvertToBigEndian((ulong)value);

    /// <summary>
    /// Byte swaps native endianness of the platform to big endian.
    /// </summary>
    /// <param name="value">Value to swap.</param>
    /// <returns>Swapped version of that value.</returns>
    public static ulong ConvertToBigEndian(ulong value) => BitConverter.IsLittleEndian ? SwapEndian(value) : value;
}
