// <copyright file="WebHelpers.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Primitives;

namespace KestrelToolbox.Internal;

/// <summary>
/// Helpers for web functionality.
/// </summary>
public static class WebHelpers
{
    /// <summary>
    /// Gets if a set of headers contains acceptable content type.
    /// </summary>
    /// <param name="header">The header value to test.</param>
    /// <param name="value">The content type.</param>
    /// <returns>True if the content type is accepted.</returns>
    public static bool IsAcceptableContentType(StringValues header, string value)
    {
        if (header.Count != 1)
        {
            return false;
        }

        var headerValue = header[0];
        if (headerValue == null)
        {
            return false;
        }

        var semiColonIndex = headerValue.IndexOf(';');
        if (semiColonIndex < 0)
        {
            return headerValue.Equals(value, StringComparison.OrdinalIgnoreCase);
        }

        // Ignore encoding information on the end.
        var shortened = headerValue.Remove(semiColonIndex);
        return shortened.Equals(value, StringComparison.OrdinalIgnoreCase);
    }

    /// <summary>
    /// Gets if a set of headers contains acceptable content type.
    /// </summary>
    /// <param name="header">The header value to test.</param>
    /// <param name="value0">First content type.</param>
    /// <param name="value1">Second content type.</param>
    /// <returns>True if the content type is accepted.</returns>
    public static bool IsAcceptableContentType(StringValues header, string value0, string value1)
    {
        if (header.Count != 1)
        {
            return false;
        }

        var headerValue = header[0];
        if (headerValue == null)
        {
            return false;
        }

        var semiColonIndex = headerValue.IndexOf(';');
        if (semiColonIndex < 0)
        {
            return headerValue.Equals(value0, StringComparison.OrdinalIgnoreCase)
                || headerValue.Equals(value1, StringComparison.OrdinalIgnoreCase);
        }

        // Ignore encoding information on the end.
        var shortened = headerValue.Remove(semiColonIndex);
        return shortened.Equals(value0, StringComparison.OrdinalIgnoreCase)
            || shortened.Equals(value1, StringComparison.OrdinalIgnoreCase);
    }

    /// <summary>
    /// Gets if a set of headers contains acceptable content type.
    /// </summary>
    /// <param name="header">The header value to test.</param>
    /// <param name="value0">First content type.</param>
    /// <param name="value1">Second content type.</param>
    /// <param name="value2">Third content type.</param>
    /// <returns>True if the content type is accepted.</returns>
    public static bool IsAcceptableContentType(StringValues header, string value0, string value1, string value2)
    {
        if (header.Count != 1)
        {
            return false;
        }

        var headerValue = header[0];
        if (headerValue == null)
        {
            return false;
        }

        var semiColonIndex = headerValue.IndexOf(';');
        if (semiColonIndex < 0)
        {
            return headerValue.Equals(value0, StringComparison.OrdinalIgnoreCase)
                || headerValue.Equals(value1, StringComparison.OrdinalIgnoreCase)
                || headerValue.Equals(value2, StringComparison.OrdinalIgnoreCase);
        }

        // Ignore encoding information on the end.
        var shortened = headerValue.Remove(semiColonIndex);
        return shortened.Equals(value0, StringComparison.OrdinalIgnoreCase)
            || shortened.Equals(value1, StringComparison.OrdinalIgnoreCase)
            || shortened.Equals(value2, StringComparison.OrdinalIgnoreCase);
    }

    /// <summary>
    /// Gets if a set of headers contains acceptable content type.
    /// </summary>
    /// <param name="header">The header value to test.</param>
    /// <param name="values">The content types.</param>
    /// <returns>True if the content type is accepted.</returns>
    public static bool IsAcceptableContentType(StringValues header, params string[] values)
    {
        if (header.Count != 1)
        {
            return false;
        }

        var headerValue = header[0];
        if (headerValue == null)
        {
            return false;
        }

        var semiColonIndex = headerValue.IndexOf(';');
        if (semiColonIndex < 0)
        {
            foreach (var value in values)
            {
                if (headerValue.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        // Ignore encoding information on the end.
        var shortened = headerValue.Remove(semiColonIndex);

        foreach (var value in values)
        {
            if (shortened.Equals(value, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
        }

        return false;
    }
}
