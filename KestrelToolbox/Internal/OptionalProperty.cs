// <copyright file="OptionalProperty.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Internal;

/// <summary>
/// An optional property.
/// </summary>
/// <typeparam name="T">The type of the property.</typeparam>
public readonly struct OptionalProperty<T>
{
    private readonly T value;

    /// <summary>
    /// Gets a value indicating whether this optional property has value.
    /// </summary>
    public bool HasValue { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="OptionalProperty{T}"/> struct.
    /// </summary>
    /// <param name="value">The value of the property.</param>
    public OptionalProperty(T value)
    {
        this.value = value;
        this.HasValue = true;
    }

    /// <summary>
    /// Casts a value to optional.
    /// </summary>
    /// <param name="value">The value to use.</param>
    /// <returns>The optional property.</returns>
    public static implicit operator OptionalProperty<T>(T value) => new(value);

    /// <summary>
    /// Casts a property to its value.
    /// </summary>
    /// <param name="property">The property to use.</param>
    /// <returns>The value.</returns>
    public static explicit operator T(OptionalProperty<T> property)
    {
        return property.value;
    }
}
