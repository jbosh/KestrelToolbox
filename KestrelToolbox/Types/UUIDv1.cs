// <copyright file="UUIDv1.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using KestrelToolbox.Internal;

namespace KestrelToolbox.Types;

#pragma warning disable SA1625 // Element documentation should not be copied and pasted.

/// <summary>
/// Implementation of Uuid v1 that is compatible with <see cref="Guid"/>.
/// </summary>
[StructLayout(LayoutKind.Sequential)]
[Serializable]
[SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Spec correct.")]
public readonly struct UUIDv1 : IComparable, IComparable<UUIDv1>, IEquatable<UUIDv1>, IFormattable
{
    /// <summary>
    /// Size in bytes of a <see cref="UUIDv1"/>.
    /// </summary>
    public const int SizeInBytes = 16;

    /// <summary>
    /// A read-only instance of the TimeUuid structure whose value is all zeros.
    /// </summary>
    // ReSharper disable once UnassignedReadonlyField
    public static readonly UUIDv1 Empty;

    /// <summary>
    /// Gets the version number of this UUID.
    /// </summary>
    /// <remarks>
    /// This is for verification purposes and should always return 1 unless the UUID is invalid.
    /// </remarks>
    public int Version => this.c >> 12;

    /// <summary>
    /// The timestamp is a 60-bit value. For UUID version 1, this is
    /// represented by Coordinated Universal Time(UTC) as a count of 100-
    /// nanosecond intervals since 00:00:00.00, 15 October 1582 (the date of
    /// Gregorian reform to the Christian calendar).
    /// </summary>
    private static readonly DateTimeOffset BaseDateTime = new(1582, 10, 15, 0, 0, 0, TimeSpan.Zero);

    private readonly int a;
    private readonly short b;
    private readonly short c;
    private readonly byte d;
    private readonly byte e;
    private readonly byte f;
    private readonly byte g;
    private readonly byte h;
    private readonly byte i;
    private readonly byte j;
    private readonly byte k;

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure using now as the timestamp.
    /// </summary>
    /// <returns>A new <see cref="UUIDv1"/> object for now.</returns>
    public static UUIDv1 NewUUIDv1() => NewUUIDv1(DateTimeOffset.UtcNow);

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure using <paramref name="dateTime"/> as the timestamp.
    /// </summary>
    /// <param name="dateTime">Timestamp to generate a <see cref="UUIDv1"/>.</param>
    /// <returns>A new <see cref="UUIDv1"/> object for <paramref name="dateTime"/>.</returns>
    public static unsafe UUIDv1 NewUUIDv1(DateTimeOffset dateTime)
    {
        var buffer = stackalloc byte[SizeInBytes];
        var randomBytes = stackalloc byte[8];

        var ticks = dateTime.Ticks - BaseDateTime.Ticks;

        RandomNumberGenerator.Fill(new Span<byte>(randomBytes, 8));

        // Write clock sequence in big endian. 8 bytes.
        ticks = BitManipulation.ConvertToLittleEndian(ticks);
        *(long*)(buffer + 0) = ticks;

        // Write clock sequence. 2 bytes.
        *(short*)(buffer + 8) = *(short*)(randomBytes + 6);

        // Version field.
        // The version number is in the most significant 4 bits of the time stamp.
        // Version 1 = 0b0001.
        buffer[7] = (byte)((buffer[7] & 0b0000_1111) | 0b0001_0000);

        // Variant field needs to be 0b10x for The variant specified in [RFC 4122].
        // Set top two bits to 0b10.
        buffer[8] = (byte)((buffer[8] & 0b0011_1111) | 0b1000_0000);

        // Write node ID. This could be mac address, but we're using random bytes. 6 bytes.
        for (var index = 0; index < 6; index++)
        {
            buffer[10 + index] = randomBytes[index];
        }

        // RFC 4122:
        // For UUID version 1, the node field consists of an IEEE 802 MAC
        // address, usually the host address.  For systems with multiple IEEE
        // 802 addresses, any available one can be used.  The lowest addressed
        // octet(octet number 10) contains the global / local bit and the
        // unicast / multicast bit, and is the first octet of the address
        // transmitted on an 802.3 LAN.
        //
        // For systems with no IEEE address, a randomly or pseudo-randomly
        // generated value may be used; see Section 4.5.The multicast bit must
        // be set in such addresses, in order that they will never conflict with
        // addresses obtained from network cards.
        buffer[10] |= 1;

        return new UUIDv1(new ReadOnlySpan<byte>(buffer, SizeInBytes));
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure using <paramref name="dateTime"/> as the timestamp.
    /// This will be the lowest possible <see cref="UUIDv1"/> that can represent <paramref name="dateTime"/>.
    /// </summary>
    /// <param name="dateTime">Timestamp to generate a <see cref="UUIDv1"/>.</param>
    /// <returns>A new <see cref="UUIDv1"/> object for <paramref name="dateTime"/>.</returns>
    public static unsafe UUIDv1 NewMinUUIDv1(DateTimeOffset dateTime)
    {
        var buffer = stackalloc byte[SizeInBytes];

        var ticks = dateTime.Ticks - BaseDateTime.Ticks;

        // Write clock sequence in big endian. 8 bytes.
        ticks = BitManipulation.ConvertToLittleEndian(ticks);
        *(long*)(buffer + 0) = ticks;

        // Write clock sequence. 2 bytes.
        *(short*)(buffer + 8) = 0;

        // Version field.
        // The version number is in the most significant 4 bits of the time stamp.
        // Version 1 = 0b0001.
        buffer[7] = (byte)((buffer[7] & 0b0000_1111) | 0b0001_0000);

        // Variant field needs to be 0b10x for The variant specified in [RFC 4122].
        // Set top two bits to 0b10.
        buffer[8] = (byte)((buffer[8] & 0b0011_1111) | 0b1000_0000);

        // Write node ID. This could be mac address, but we're using random bytes. 6 bytes.
        for (var index = 0; index < 6; index++)
        {
            buffer[10 + index] = 0;
        }

        // RFC 4122:
        // For UUID version 1, the node field consists of an IEEE 802 MAC
        // address, usually the host address.  For systems with multiple IEEE
        // 802 addresses, any available one can be used.  The lowest addressed
        // octet(octet number 10) contains the global / local bit and the
        // unicast / multicast bit, and is the first octet of the address
        // transmitted on an 802.3 LAN.
        //
        // For systems with no IEEE address, a randomly or pseudo-randomly
        // generated value may be used; see Section 4.5.The multicast bit must
        // be set in such addresses, in order that they will never conflict with
        // addresses obtained from network cards.
        buffer[10] |= 1;

        return new UUIDv1(new ReadOnlySpan<byte>(buffer, SizeInBytes));
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified integers and bytes.
    /// </summary>
    /// <param name="a">The first 4 bytes of the GUID.</param>
    /// <param name="b">The next 2 bytes of the GUID.</param>
    /// <param name="c">The next 2 bytes of the GUID.</param>
    /// <param name="d">The next byte of the GUID.</param>
    /// <param name="e">The next byte of the GUID.</param>
    /// <param name="f">The next byte of the GUID.</param>
    /// <param name="g">The next byte of the GUID.</param>
    /// <param name="h">The next byte of the GUID.</param>
    /// <param name="i">The next byte of the GUID.</param>
    /// <param name="j">The next byte of the GUID.</param>
    /// <param name="k">The next byte of the GUID.</param>
    public UUIDv1(int a, short b, short c, byte d, byte e, byte f, byte g, byte h, byte i, byte j, byte k)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified integers and bytes.
    /// </summary>
    /// <param name="a">The first 4 bytes of the GUID.</param>
    /// <param name="b">The next 2 bytes of the GUID.</param>
    /// <param name="c">The next 2 bytes of the GUID.</param>
    /// <param name="d">The next 8 bytes of the GUID.</param>
    public UUIDv1(int a, short b, short c, ReadOnlySpan<byte> d)
    {
        if (d.Length != 8)
        {
            throw new ArgumentException($"{nameof(d)} must be exactly 8 bytes.", nameof(d));
        }

        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d[0];
        this.e = d[1];
        this.f = d[2];
        this.g = d[3];
        this.h = d[4];
        this.i = d[5];
        this.j = d[6];
        this.k = d[7];
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified array of bytes.
    /// </summary>
    /// <param name="buffer">A 16-element byte array containing values with which to initialize the GUID.</param>
    public UUIDv1(byte[] buffer)
        : this(buffer.AsSpan()) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified array of bytes.
    /// </summary>
    /// <param name="buffer">A 16-element byte array containing values with which to initialize the GUID.</param>
    /// <param name="offset">An offset into <paramref name="buffer"/> to start reading the 16 elements.</param>
    public UUIDv1(byte[] buffer, int offset)
        : this(buffer.AsSpan(offset)) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified bytes.
    /// </summary>
    /// <param name="buffer">Memory with 16 bytes containing values with which to initialize the GUID.</param>
    public UUIDv1(ReadOnlyMemory<byte> buffer)
        : this(buffer.Span) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified bytes.
    /// </summary>
    /// <param name="buffer">Span with 16 bytes containing values with which to initialize the <see cref="UUIDv1"/>.</param>
    public UUIDv1(ReadOnlySpan<byte> buffer)
    {
        this = CreateFromBytes(buffer);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified bytes in big endian.
    /// </summary>
    /// <param name="buffer">Span with 16 bytes containing values with which to initialize the <see cref="UUIDv1"/>.</param>
    /// <returns>Newly created instance of <see cref="UUIDv1"/>.</returns>
    public static UUIDv1 CreateFromBytesBigEndian(ReadOnlySpan<byte> buffer)
    {
        var id = CreateFromBytes(buffer);
        if (BitConverter.IsLittleEndian)
        {
            id = SwapEndian(id);
        }

        return id;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified bytes in little endian.
    /// </summary>
    /// <param name="buffer">Span with 16 bytes containing values with which to initialize the <see cref="UUIDv1"/>.</param>
    /// <returns>Newly created instance of <see cref="UUIDv1"/>.</returns>
    public static UUIDv1 CreateFromBytesLittleEndian(ReadOnlySpan<byte> buffer)
    {
        var id = CreateFromBytes(buffer);
        if (!BitConverter.IsLittleEndian)
        {
            id = SwapEndian(id);
        }

        return id;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UUIDv1"/> structure by using the specified bytes in native endian.
    /// </summary>
    /// <param name="buffer">Span with 16 bytes containing values with which to initialize the <see cref="UUIDv1"/>.</param>
    /// <returns>Newly created instance of <see cref="UUIDv1"/>.</returns>
    public static UUIDv1 CreateFromBytes(ReadOnlySpan<byte> buffer)
    {
        if (buffer.Length < SizeInBytes)
        {
            throw new ArgumentOutOfRangeException(
                nameof(buffer),
                buffer.Length.ToString(CultureInfo.InvariantCulture),
                $"{nameof(buffer)} must be larger than {SizeInBytes} bytes."
            );
        }

        return new UUIDv1(
            (buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | buffer[0],
            (short)((buffer[5] << 8) | buffer[4]),
            (short)((buffer[7] << 8) | buffer[6]),
            buffer[8],
            buffer[9],
            buffer[10],
            buffer[11],
            buffer[12],
            buffer[13],
            buffer[14],
            buffer[15]
        );
    }

    /// <summary>
    /// Tries to write the current GUID instance into a span of bytes.
    /// </summary>
    /// <param name="destination">When this method returns, the GUID as a span of bytes.</param>
    /// <returns>true if the GUID is successfully written to the specified span; false otherwise.</returns>
    [Pure]
    public bool TryWriteBytes(Span<byte> destination)
    {
        if (destination.Length < SizeInBytes)
        {
            return false;
        }

        WriteBytes(this, destination);
        return true;
    }

    /// <summary>
    /// Tries to write the current GUID instance into a span of bytes in big endian.
    /// </summary>
    /// <param name="destination">When this method returns, the GUID as a span of bytes.</param>
    /// <returns>true if the GUID is successfully written to the specified span; false otherwise.</returns>
    [Pure]
    public bool TryWriteBytesBigEndian(Span<byte> destination)
    {
        if (!BitConverter.IsLittleEndian)
        {
            return this.TryWriteBytes(destination);
        }
        else
        {
            var id = SwapEndian(this);
            return id.TryWriteBytes(destination);
        }
    }

    /// <summary>
    /// Tries to write the current GUID instance into a span of bytes in little endian.
    /// </summary>
    /// <param name="destination">When this method returns, the GUID as a span of bytes.</param>
    /// <returns>true if the GUID is successfully written to the specified span; false otherwise.</returns>
    [Pure]
    public bool TryWriteBytesLittleEndian(Span<byte> destination)
    {
        if (BitConverter.IsLittleEndian)
        {
            return this.TryWriteBytes(destination);
        }
        else
        {
            var id = SwapEndian(this);
            return id.TryWriteBytes(destination);
        }
    }

    /// <summary>
    /// Returns a 16-element byte array that contains the value of this instance.
    /// </summary>
    /// <returns>A 16-element byte array.</returns>
    [Pure]
    public byte[] ToByteArray()
    {
        var bytes = new byte[16];
        WriteBytes(this, bytes);
        return bytes;
    }

    /// <summary>
    /// Returns a 16-element little endian byte array that contains the value of this instance.
    /// </summary>
    /// <returns>A 16-element byte array.</returns>
    [Pure]
    public byte[] ToByteArrayLittleEndian()
    {
        var bytes = new byte[16];
        var success = this.TryWriteBytesLittleEndian(bytes);
        Debug.Assert(success, "Not enough bytes.");
        return bytes;
    }

    /// <summary>
    /// Returns a 16-element big endian byte array that contains the value of this instance.
    /// </summary>
    /// <returns>A 16-element byte array.</returns>
    [Pure]
    public byte[] ToByteArrayBigEndian()
    {
        var bytes = new byte[16];
        var success = this.TryWriteBytesBigEndian(bytes);
        Debug.Assert(success, "Not enough bytes.");
        return bytes;
    }

    private static void WriteBytes(UUIDv1 id, Span<byte> destination)
    {
        Debug.Assert(destination.Length >= SizeInBytes, "Shouldn't pass in buffer that is too small.");
        var guid = (Guid)id;
        var success = guid.TryWriteBytes(destination);
        Debug.Assert(success, "Destination was not correct size.");
    }

    /// <summary>
    /// Implicitly converts a <see cref="UUIDv1"/> to a <see cref="Guid"/>.
    /// </summary>
    /// <param name="id">The <see cref="UUIDv1"/> to convert.</param>
    public static implicit operator Guid(UUIDv1 id) =>
        new(id.a, id.b, id.c, id.d, id.e, id.f, id.g, id.h, id.i, id.j, id.k);

    /// <summary>
    /// Implicitly converts a <see cref="UUIDv1"/> to a <see cref="DateTimeOffset"/>.
    /// </summary>
    /// <param name="id">The <see cref="UUIDv1"/> to convert.</param>
    public static unsafe implicit operator DateTimeOffset(UUIDv1 id)
    {
        var ticks = *(long*)&id;
        ticks = BitManipulation.ConvertFromLittleEndian(ticks);

        // Top 4 bits are version number
        ticks = (long)((ulong)ticks & 0x0FFF_FFFF_FFFF_FFFFUL);

        ticks += BaseDateTime.Ticks;
        return new DateTimeOffset(ticks, TimeSpan.Zero);
    }

    /// <summary>
    /// Converts a <see cref="UUIDv1"/> to a <see cref="DateTimeOffset"/>. You can also use an implicit
    /// operator to convert between the two.
    /// </summary>
    /// <returns>The newly created <see cref="DateTimeOffset"/>.</returns>
    [Pure]
    public DateTimeOffset ToDateTimeOffset() => this;

    /// <summary>
    /// Implicitly converts a <see cref="Guid"/> to a <see cref="UUIDv1"/>.
    /// </summary>
    /// <param name="id">The <see cref="Guid"/> to convert.</param>
    public static implicit operator UUIDv1(Guid id)
    {
        Span<byte> buffer = stackalloc byte[SizeInBytes];
        var success = id.TryWriteBytes(buffer);
        Debug.Assert(success, "Didn't send in enough bytes.");
        return new UUIDv1(buffer);
    }

    /// <summary>
    /// Swaps the endianness of a <see cref="UUIDv1"/>.
    /// </summary>
    /// <param name="id">The <see cref="UUIDv1"/> to swap endianness of.</param>
    /// <returns>The swapped endian <see cref="UUIDv1"/>.</returns>
    public static unsafe UUIDv1 SwapEndian(UUIDv1 id)
    {
        var a = id.a;
        var b = id.b;
        var c = id.c;
        var aPtr = (byte*)&a;
        var bPtr = (byte*)&b;
        var cPtr = (byte*)&c;
        return new UUIDv1(
            (aPtr[0] << 24) | (aPtr[1] << 16) | (aPtr[2] << 8) | aPtr[3],
            (short)((bPtr[0] << 8) | bPtr[1]),
            (short)((cPtr[0] << 8) | cPtr[1]),
            id.d,
            id.e,
            id.f,
            id.g,
            id.h,
            id.i,
            id.j,
            id.k
        );
    }

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="obj">An object to compare to this instance.</param>
    /// <returns>
    /// A signed number indicating the relative values of this instance and value. Return
    /// value Description A negative integer This instance is less than value. Zero This
    /// instance is equal to value. A positive integer This instance is greater than
    /// value.
    /// </returns>
    [Pure]
    public int CompareTo(object? obj) => obj is UUIDv1 id ? this.CompareTo(id) : 1;

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="other">An object to compare to this instance.</param>
    /// <returns>
    /// A signed number indicating the relative values of this instance and value. Return
    /// value Description A negative integer This instance is less than value. Zero This
    /// instance is equal to value. A positive integer This instance is greater than
    /// value.
    /// </returns>
    [Pure]
    public int CompareTo(UUIDv1 other) => Compare(this, other);

    [MethodImpl(MethodImplOptions.AggressiveInlining)] // Use aggressive inlining so <, >, <=, >= inline and can fold some comparisons.
    private static unsafe int Compare(UUIDv1 a, UUIDv1 b)
    {
        var aPtr = (ulong*)&a;
        var bPtr = (ulong*)&b;

        var a0 = aPtr[0];
        var b0 = bPtr[0];
        a0 = BitManipulation.ConvertFromLittleEndian(a0);
        b0 = BitManipulation.ConvertFromLittleEndian(b0);

        if (a0 != b0)
        {
            return a0 < b0 ? -1 : 1;
        }

        var a1 = aPtr[1];
        var b1 = bPtr[1];

        a1 = BitManipulation.ConvertFromLittleEndian(a1);
        b1 = BitManipulation.ConvertFromLittleEndian(b1);

        if (a1 != b1)
        {
            return a1 < b1 ? -1 : 1;
        }

        return 0;
    }

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="a">The first object to compare.</param>
    /// <param name="b">The second object to compare.</param>
    /// <returns>True if <paramref name="a"/> is greater than <paramref name="b"/>, otherwise false.</returns>
    public static bool operator >(UUIDv1 a, UUIDv1 b) => a.CompareTo(b) == 1;

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="a">The first object to compare.</param>
    /// <param name="b">The second object to compare.</param>
    /// <returns>True if <paramref name="a"/> is less than <paramref name="b"/>, otherwise false.</returns>
    public static bool operator <(UUIDv1 a, UUIDv1 b) => a.CompareTo(b) == -1;

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="a">The first object to compare.</param>
    /// <param name="b">The second object to compare.</param>
    /// <returns>True if <paramref name="a"/> is greater than or equal to <paramref name="b"/>, otherwise false.</returns>
    public static bool operator >=(UUIDv1 a, UUIDv1 b) => a.CompareTo(b) >= 0;

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="a">The first object to compare.</param>
    /// <param name="b">The second object to compare.</param>
    /// <returns>True if <paramref name="a"/> is less than or equal to <paramref name="b"/>, otherwise false.</returns>
    public static bool operator <=(UUIDv1 a, UUIDv1 b) => a.CompareTo(b) <= 0;

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="a">The first object to compare.</param>
    /// <param name="b">The second object to compare.</param>
    /// <returns>True if <paramref name="a"/> is equal to <paramref name="b"/>, otherwise false.</returns>
    public static bool operator ==(UUIDv1 a, UUIDv1 b) => Equal(a, b);

    /// <summary>
    /// Compares this instance to a specified <see cref="UUIDv1"/> object and returns an indication of their relative values.
    /// </summary>
    /// <param name="a">The first object to compare.</param>
    /// <param name="b">The second object to compare.</param>
    /// <returns>True if <paramref name="a"/> is not equal to <paramref name="b"/>, otherwise false.</returns>
    public static bool operator !=(UUIDv1 a, UUIDv1 b) => !Equal(a, b);

    /// <summary>
    /// Converts the string representation of a GUID to the equivalent <see cref="UUIDv1"/> structure.
    /// </summary>
    /// <param name="input">The string to convert.</param>
    /// <returns>A structure that contains the value that was parsed.</returns>
    /// <exception cref="FormatException"><paramref name="input"/> is not in a recognized format.</exception>
    public static UUIDv1 Parse(ReadOnlySpan<char> input) => Guid.Parse(input);

    /// <summary>
    /// Converts the string representation of a GUID to the equivalent <see cref="UUIDv1"/> structure.
    /// </summary>
    /// <param name="input">The string to convert.</param>
    /// <returns>A structure that contains the value that was parsed.</returns>
    /// <exception cref="NullReferenceException"><paramref name="input"/> is null.</exception>
    /// <exception cref="FormatException"><paramref name="input"/> is not in a recognized format.</exception>
    public static UUIDv1 Parse(string input)
    {
        var guid = Guid.Parse(input);
        var uuid = (UUIDv1)guid;
        if (uuid.Version != 1)
        {
            throw new FormatException($"UUIDv1 requires version 1. Was version {uuid.Version}.");
        }

        return uuid;
    }

    /// <summary>
    /// Converts the string representation of a GUID to the equivalent <see cref="UUIDv1"/> structure.
    /// </summary>
    /// <param name="input">The string to convert.</param>
    /// <param name="id">Resulting <see cref="UUIDv1"/> of parsing.</param>
    /// <returns>True on successful parsing and false when failed.</returns>
    public static bool TryParse(ReadOnlySpan<char> input, out UUIDv1 id)
    {
        if (!Guid.TryParse(input, out var guid))
        {
            id = default;
            return false;
        }

        var uuid = (UUIDv1)guid;
        if (uuid.Version != 1)
        {
            id = default;
            return false;
        }

        id = guid;
        return true;
    }

    /// <summary>
    /// Converts the string representation of a GUID to the equivalent <see cref="UUIDv1"/> structure.
    /// </summary>
    /// <param name="input">The string to convert.</param>
    /// <param name="id">Resulting <see cref="UUIDv1"/> of parsing.</param>
    /// <returns>True on successful parsing and false when failed.</returns>
    public static bool TryParse([NotNullWhen(true)] string? input, out UUIDv1 id)
    {
        if (!Guid.TryParse(input, out var guid))
        {
            id = default;
            return false;
        }

        var uuid = (UUIDv1)guid;
        if (uuid.Version != 1)
        {
            id = default;
            return false;
        }

        id = uuid;
        return true;
    }

    /// <inheritdoc/>
    [Pure]
    public override int GetHashCode() => ((Guid)this).GetHashCode();

    /// <inheritdoc/>
    [Pure]
    public override bool Equals(object? obj) => obj is UUIDv1 id && Equal(this, id);

    /// <inheritdoc/>
    [Pure]
    public bool Equals(UUIDv1 other) => Equal(this, other);

    /// <inheritdoc />
    [Pure]
    public override string ToString() => ((Guid)this).ToString();

    /// <inheritdoc />
    [Pure]
    public string ToString(string? format, IFormatProvider? formatProvider) =>
        ((Guid)this).ToString(format, formatProvider);

    private static unsafe bool Equal(UUIDv1 a, UUIDv1 b)
    {
        var aPtr = (ulong*)&a;
        var bPtr = (ulong*)&b;

        return aPtr[0] == bPtr[0] && aPtr[1] == bPtr[1];
    }
}
