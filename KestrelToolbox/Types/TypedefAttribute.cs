// <copyright file="TypedefAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Types;

/// <summary>
/// Attribute to signify that this class should be typedef of another type.
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
public sealed class TypedefAttribute : Attribute
{
    /// <summary>
    /// Gets the alias type this attribute references.
    /// </summary>
    public Type Alias { get; }

    /// <summary>
    /// Gets features of typedef generation.
    /// </summary>
    /// <remarks>Default is <see cref="TypedefFeatures.Default"/>.</remarks>
    public TypedefFeatures Features { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="TypedefAttribute"/> class.
    /// </summary>
    /// <param name="alias">The type to alias this class to.</param>
    /// <param name="features">The features to generate.</param>
    public TypedefAttribute(Type alias, TypedefFeatures features = TypedefFeatures.Default)
    {
        this.Alias = alias;
        this.Features = features;
    }
}
