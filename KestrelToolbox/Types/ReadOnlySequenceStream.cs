// <copyright file="ReadOnlySequenceStream.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.IO;

namespace KestrelToolbox.Types;

/// <summary>
/// Creates a stream whose backing store is a ReadOnlySequence&lt;byte&gt;.
/// </summary>
public class ReadOnlySequenceStream : Stream
{
    private ReadOnlySequenceReader reader;

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlySequenceStream"/> class.
    /// </summary>
    /// <param name="sequence">The sequence to read.</param>
    public ReadOnlySequenceStream(ReadOnlySequence<byte> sequence)
    {
        this.reader = new ReadOnlySequenceReader(sequence);
    }

    /// <inheritdoc/>
    public override bool CanRead => true;

    /// <inheritdoc/>
    public override bool CanSeek => true;

    /// <inheritdoc/>
    public override bool CanWrite => false;

    /// <inheritdoc/>
    public override long Length => this.reader.Length;

    /// <inheritdoc/>
    public override void SetLength(long value) =>
        throw new NotSupportedException($"Cannot set Length of {nameof(ReadOnlySequenceStream)}.");

    /// <inheritdoc/>
    public override void Write(byte[] buffer, int offset, int count) =>
        throw new NotSupportedException($"Cannot set write to {nameof(ReadOnlySequenceStream)}.");

    /// <inheritdoc/>
    public override long Position
    {
        get => this.reader.Position;
        set => this.reader.Position = value;
    }

    /// <inheritdoc/>
    public override int Read(byte[] buffer, int offset, int count) => this.Read(buffer.AsSpan(offset, count));

    /// <inheritdoc/>
    public override int Read(Span<byte> buffer) => this.reader.Read(buffer);

    /// <inheritdoc/>
    public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken) =>
        this.ReadAsync(buffer.AsMemory(offset, count), cancellationToken).AsTask();

    /// <inheritdoc/>
    public override ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = default) =>
        this.reader.ReadAsync(buffer);

    /// <inheritdoc/>
    public override Task CopyToAsync(Stream destination, int bufferSize, CancellationToken cancellationToken) =>
        this.reader.CopyToAsync(destination);

    /// <inheritdoc/>
    public override void CopyTo(Stream destination, int bufferSize) => this.reader.CopyTo(destination);

    /// <inheritdoc/>
    public override int ReadByte() => this.reader.ReadByte();

    /// <inheritdoc/>
    public override long Seek(long offset, SeekOrigin origin) => this.reader.Seek(offset, origin);

    /// <inheritdoc/>
    public override void Flush()
    {
        // Nothing to do
    }
}
