// <copyright file="MutexSlimWithValue.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace KestrelToolbox.Types;

/// <summary>
/// Represents a lightweight alternative to System.Threading.Mutex that limits
/// the number of threads that can access a resource or pool of resources concurrently.
/// </summary>
/// <typeparam name="T">Value to protect with <see cref="MutexSlim{T}"/>.</typeparam>
[SuppressMessage(
    "StyleCop.CSharp.DocumentationRules",
    "SA1649:File name should match first type name",
    Justification = "MutexSlim{T} isn't a great filename."
)]
public sealed class MutexSlim<T> : IDisposable
{
    /// <summary>
    /// Value being protected by this mutex.
    /// </summary>
    /// <see cref="MutexSlimLock{T}"/>
    [SuppressMessage(
        "StyleCop.CSharp.MaintainabilityRules",
        "SA1401:FieldsMustBePrivate",
        Justification = "Needed by MutexSlimLocker{T}."
    )]
    internal T Value;

    private readonly SemaphoreSlim semaphore = new(0, int.MaxValue);

    private int waitingThreads;
    private int lockCount;

    /// <summary>
    /// Initializes a new instance of the <see cref="MutexSlim{T}"/> class.
    /// </summary>
    /// <param name="value">Initial value.</param>
    public MutexSlim(T value)
    {
        this.Value = value;
    }

    /// <summary>
    /// Asynchronously waits to enter the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <remarks>
    /// Note that this method is almost 2x slower than WaitAsync equivalent when using try/finally due to the construction of
    /// MutexSlimLock. If you have very high performance code, using that method is recommended.
    /// </remarks>
    /// <returns>MutexSlimLock object that can be used to release the lock. This value can be ignored.</returns>
    public Task<MutexSlimLock<T>> LockAsync(CancellationToken cancellationToken = default)
    {
        for (var spin = MutexSlim.SpinCount; spin > 0; spin--)
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            if (acquiredLock)
            {
                return Task.FromResult(new MutexSlimLock<T>(this));
            }

            _ = Thread.Yield();
        }

        return this.LockAsyncInternal(cancellationToken);
    }

    /// <summary>
    /// Tries to enter the KestrelToolbox.Types.MutexSlim and will return immediately if it is locked.
    /// </summary>
    /// <returns>True if the mutex is locked, false if not..</returns>
    public bool TryWait()
    {
        for (var spin = MutexSlim.SpinCount; spin > 0; spin--)
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            if (acquiredLock)
            {
                return true;
            }

            _ = Thread.Yield();
        }

        return false;
    }

    /// <summary>
    /// Synchronously waits to enter the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>MutexSlimLock object that can be used to release the lock. This value can be ignored.</returns>
    public MutexSlimLock<T> Lock(CancellationToken cancellationToken = default)
    {
        var task = this.LockAsync(cancellationToken);
        _ = Task.WaitAny(new Task[] { task }, CancellationToken.None);
        if (task.Exception != null)
        {
            throw task.Exception.InnerException!;
        }

        if (task.IsCanceled)
        {
            cancellationToken.ThrowIfCancellationRequested();
        }

        return task.Result;
    }

    /// <summary>
    /// Releases the unmanaged resources used by the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    [SuppressMessage(
        "Blocker Code Smell",
        "S3877:Exceptions should not be thrown from unexpected methods",
        Justification = "There are invalid ways to dispose of this structure."
    )]
    public void Dispose()
    {
        if (this.waitingThreads != 0 || this.lockCount != 0)
        {
            throw new InvalidOperationException(
                "Cannot dispose of MutexSlim while threads are waiting to lock or the lock is acquired."
            );
        }

        this.semaphore.Dispose();
    }

    /// <summary>
    /// Releases the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    internal void Release()
    {
        var oldValue = Interlocked.Decrement(ref this.lockCount);
        if (oldValue != 0)
        {
            throw new InvalidOperationException("Cannot release an unowned MutexSlim.");
        }

        if (MutexSlim.IsArm)
        {
            Interlocked.MemoryBarrier();
        }

        if (this.waitingThreads != 0)
        {
            _ = this.semaphore.Release();
        }
    }

    /// <summary>
    /// Use this method if you could not acquire the lock with spins. It is expensive because it is an async method.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>MutexSlimLock object that can be used to release the lock. This value can be ignored.</returns>
    private async Task<MutexSlimLock<T>> LockAsyncInternal(CancellationToken cancellationToken)
    {
        await this.WaitAsyncInternal(cancellationToken);
        return new MutexSlimLock<T>(this);
    }

    /// <summary>
    /// Use this method if you could not acquire the lock with spins. It is expensive because it is an async method.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>void.</returns>
    private async Task WaitAsyncInternal(CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        _ = Interlocked.Increment(ref this.waitingThreads);
        try
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            while (!acquiredLock)
            {
                await this.semaphore.WaitAsync(cancellationToken);
                acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            }
        }
        finally
        {
            _ = Interlocked.Decrement(ref this.waitingThreads);
        }
    }
}

/// <summary>
/// Lock object that will release a lock when disposed.
/// </summary>
/// <typeparam name="T">Value that <see cref="MutexSlim{T}"/> holds.</typeparam>
public readonly struct MutexSlimLock<T> : IDisposable
{
    private readonly MutexSlim<T> mutex;

    /// <summary>
    /// Gets or sets the protected value.
    /// </summary>
    public T Value
    {
        get => this.mutex.Value;
        set => this.mutex.Value = value;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="MutexSlimLock{T}"/> struct.
    /// </summary>
    /// <param name="mutex">The mutex that has been locked.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal MutexSlimLock(MutexSlim<T> mutex)
    {
        this.mutex = mutex;
    }

    /// <summary>
    /// Unlocks the mutex.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Dispose() => this.mutex.Release();
}
