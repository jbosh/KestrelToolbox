// <copyright file="ReadOnlySequenceReader.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.IO;

namespace KestrelToolbox.Types;

/// <summary>
/// Creates a stream whose backing store is a ReadOnlySequence&lt;byte&gt;.
/// </summary>
public struct ReadOnlySequenceReader
{
    private readonly ReadOnlySequence<byte> sequence;
    private long position;

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlySequenceReader"/> struct.
    /// </summary>
    /// <param name="sequence">The sequence to read.</param>
    public ReadOnlySequenceReader(ReadOnlySequence<byte> sequence)
    {
        this.position = 0;
        this.sequence = sequence;
    }

    /// <summary>
    /// Gets the length in bytes of the stream.
    /// </summary>
    public long Length => this.sequence.Length;

    /// <summary>
    /// Gets or sets the position within the current stream.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">The position is not within the bounds of the stream.</exception>
    public long Position
    {
        get => this.position;
        set
        {
            if (value < 0 || value > this.sequence.Length)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(value),
                    $"{nameof(value)} must be >= 0 and <= {nameof(this.Length)}."
                );
            }

            this.position = value;
        }
    }

    /// <summary>
    /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
    /// </summary>
    /// <param name="buffer">A region of memory. When this method returns, the contents of this region are replaced by the bytes read from the current source.</param>
    /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read  from the current stream.</param>
    /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
    /// <returns>
    /// The total number of bytes read into the buffer. This can be less than the number
    /// of bytes requested if that many bytes are not currently available, or zero (0)
    /// if the end of the stream has been reached.
    /// </returns>
    public int Read(byte[] buffer, int offset, int count) => this.Read(buffer.AsSpan(offset, count));

    /// <summary>
    /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
    /// </summary>
    /// <param name="span">The region of memory to write the data into.</param>
    /// <returns>
    /// The total number of bytes read into the span. This can be less than the number
    /// of bytes requested if that many bytes are not currently available, or zero (0)
    /// if the end of the stream has been reached.
    /// </returns>
    public int Read(Span<byte> span)
    {
        var amountCopied = (int)Math.Min(this.sequence.Length - this.position, span.Length);

        var slice = this.sequence.Slice(this.position, amountCopied);
        slice.CopyTo(span.Slice(0, amountCopied));

        this.position += amountCopied;
        return amountCopied;
    }

    /// <summary>
    /// Asynchronously reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
    /// </summary>
    /// <param name="buffer">A region of memory. When this method returns, the contents of this region are replaced by the bytes read from the current source.</param>
    /// <param name="offset">The zero-based byte offset in buffer at which to begin storing the data read  from the current stream.</param>
    /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
    /// <returns>
    /// The total number of bytes read into the buffer. This can be less than the number
    /// of bytes requested if that many bytes are not currently available, or zero (0)
    /// if the end of the stream has been reached.
    /// </returns>
    public Task<int> ReadAsync(byte[] buffer, int offset, int count) =>
        this.ReadAsync(buffer.AsMemory(offset, count)).AsTask();

    /// <summary>
    /// Asynchronously Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
    /// </summary>
    /// <param name="buffer">The region of memory to write the data into.</param>
    /// <returns>
    /// The total number of bytes read into the span. This can be less than the number
    /// of bytes requested if that many bytes are not currently available, or zero (0)
    /// if the end of the stream has been reached.
    /// </returns>
    public ValueTask<int> ReadAsync(Memory<byte> buffer)
    {
        var amountCopied = (int)Math.Min(this.sequence.Length - this.position, buffer.Length);

        var slice = this.sequence.Slice(this.position, amountCopied);
        slice.CopyTo(buffer.Span.Slice(0, amountCopied));

        this.position += amountCopied;
        return new ValueTask<int>(amountCopied);
    }

    /// <summary>
    /// Asynchronously reads the bytes from the current stream and writes them to another stream.
    /// </summary>
    /// <param name="destination">The stream to which the contents of the current stream will be copied.</param>
    /// <returns>void.</returns>
    public Task CopyToAsync(Stream destination)
    {
        this.CopyTo(destination);
        return Task.CompletedTask;
    }

    /// <summary>
    /// Reads the bytes from the current stream and writes them to another stream.
    /// </summary>
    /// <param name="destination">The stream to which the contents of the current stream will be copied.</param>
    public void CopyTo(Stream destination)
    {
        if (this.position != 0)
        {
            throw new InvalidOperationException("Cannot copy to another stream when Position is non-zero.");
        }

        if (this.sequence.IsSingleSegment)
        {
            destination.Write(this.sequence.FirstSpan);
        }
        else
        {
            foreach (var buffer in this.sequence)
            {
                destination.Write(buffer.Span);
            }
        }
    }

    /// <summary>
    /// Reads a byte from the stream and advances the position within the stream by one
    /// byte, or returns -1 if at the end of the stream.
    /// </summary>
    /// <returns>The unsigned byte cast to an Int32, or -1 if at the end of the stream.</returns>
    public int ReadByte()
    {
        if (this.sequence.Length == this.position)
        {
            return -1;
        }

        var result = this.sequence.Slice(this.position, 1).FirstSpan[0];
        this.position++;
        return result;
    }

    /// <summary>
    /// Sets the position within the current stream.
    /// </summary>
    /// <param name="offset">A byte offset relative to the origin parameter.</param>
    /// <param name="origin">
    /// A value of type <see cref="SeekOrigin"/> indicating the reference point used to obtain
    /// the new position.
    /// </param>
    /// <returns>The new position within the current stream.</returns>
    /// <exception cref="ArgumentException">Invalid <paramref name="origin"/>.</exception>
    /// <exception cref="ArgumentOutOfRangeException">The position is not within the bounds of the stream.</exception>
    public long Seek(long offset, SeekOrigin origin)
    {
        switch (origin)
        {
            case SeekOrigin.Begin:
            {
                this.Position = offset;
                break;
            }

            case SeekOrigin.Current:
            {
                this.Position += offset;
                break;
            }

            case SeekOrigin.End:
            {
                this.Position = this.sequence.Length - offset;
                break;
            }

            default:
            {
                throw new ArgumentException($"{nameof(origin)} was not a valid {nameof(SeekOrigin)}.", nameof(origin));
            }
        }

        return this.Position;
    }
}
