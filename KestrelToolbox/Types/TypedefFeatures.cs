// <copyright file="TypedefFeatures.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Types;

/// <summary>
/// A list of features to generate for this typedef.
/// </summary>
[Flags]
public enum TypedefFeatures
{
    /// <summary>
    /// Do not generate any features on this class.
    /// </summary>
    None = 0,

    /// <summary>
    /// Alias all methods of the typedef'd type.
    /// </summary>
    AliasMethods = 1 << 0,

    /// <summary>
    /// Make this class <see cref="IEquatable{T}"/> to itself.
    /// </summary>
    /// <remarks>
    /// This will seal the class also.
    /// See <a href="https://rules.sonarsource.com/csharp/RSPEC-4035/">Sonar 4035</a> for more details.
    /// </remarks>
    Equatable = 1 << 1,

    /// <summary>
    /// Generates <c>From</c> methods.
    /// </summary>
    /// <remarks>
    /// These methods are static methods to generate possibly null versions of the typedef.
    ///
    /// This feature has no effect on non-nullable value types. Only reference types and Nullable value types.
    ///
    /// Example:
    /// <code>
    /// [Typedef(typeof(string))]
    /// public class StringTypedef { }
    ///
    /// public static void Method(string? value)
    /// {
    ///     // Instead of this:
    ///     var typedef = value is null ? null : new StringTypedef(value);
    ///
    ///     // You could use this:
    ///     var typedef = StringTypedef.From(value);
    ///
    ///     // typedef will be null here if value is null.
    ///     Debugger.Assert((typedef == null) == (value == null));
    /// }
    /// </code>
    /// </remarks>
    FromMethods = 1 << 2,

    /// <summary>
    /// Make this class generate json serialize and parsing methods.
    /// </summary>
    /// <remarks>
    /// This will generate the default names and directly write the subtype.
    ///
    /// For example, <c>[Typedef(typeof(int))]</c> will serialize directly to a json int.
    /// </remarks>
    JsonSerializable = 1 << 3,

    /// <summary>
    /// Default features for a typedef. <see cref="AliasMethods"/> | <see cref="Equatable"/>.
    /// </summary>
    Default = AliasMethods | FromMethods | Equatable,

    /// <summary>
    /// All features possible will be enabled.
    /// </summary>
    All = AliasMethods | FromMethods | Equatable | JsonSerializable,
}
