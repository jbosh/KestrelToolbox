// <copyright file="MutexSlim.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace KestrelToolbox.Types;

/// <summary>
/// Represents a lightweight alternative to System.Threading.Mutex that limits
/// the number of threads that can access a resource or pool of resources concurrently.
/// </summary>
public sealed class MutexSlim : IDisposable
{
    /// <summary>
    /// Number of spins <see cref="MutexSlim"/> should take before going to sleep.
    /// </summary>
    internal static readonly int SpinCount = Environment.ProcessorCount == 1 ? 1 : 2000;

    /// <summary>
    /// Field for if the processor architecture is arm. Some actions require memory barriers.
    /// </summary>
    internal static readonly bool IsArm =
        RuntimeInformation.ProcessArchitecture is Architecture.Arm or Architecture.Arm64;

    private readonly SemaphoreSlim semaphore = new(0, int.MaxValue);

    private int waitingThreads;
    private int lockCount;

    /// <summary>
    /// Asynchronously waits to enter the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <remarks>
    /// Note that this method is almost 2x slower than WaitAsync equivalent when using try/finally due to the construction of
    /// MutexSlimLock. If you have very high performance code, using that method is recommended.
    /// </remarks>
    /// <returns>MutexSlimLock object that can be used to release the lock. This value can be ignored.</returns>
    public ValueTask<MutexSlimLock> LockAsync(CancellationToken cancellationToken = default)
    {
        for (var spin = SpinCount; spin > 0; spin--)
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            if (acquiredLock)
            {
                return new ValueTask<MutexSlimLock>(new MutexSlimLock(this));
            }
        }

        return this.LockAsyncInternal(cancellationToken);
    }

    /// <summary>
    /// Tries to enter the KestrelToolbox.Types.MutexSlim and will return immediately if it is locked.
    /// </summary>
    /// <returns>True if the mutex is locked, false if not..</returns>
    public bool TryWait()
    {
        for (var spin = SpinCount; spin > 0; spin--)
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            if (acquiredLock)
            {
                return true;
            }

            _ = Thread.Yield();
        }

        return false;
    }

    /// <summary>
    /// Synchronously waits to enter the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>MutexSlimLock object that can be used to release the lock. This value can be ignored.</returns>
    public MutexSlimLock Lock(CancellationToken cancellationToken = default)
    {
        var valueTask = this.LockAsync(cancellationToken);
        var task = valueTask.AsTask();
        _ = Task.WaitAny(new Task[] { task }, CancellationToken.None);
        if (task.Exception != null)
        {
            throw task.Exception.InnerException!;
        }

        if (task.IsCanceled)
        {
            cancellationToken.ThrowIfCancellationRequested();
        }

        return task.Result;
    }

    /// <summary>
    /// Asynchronously waits to enter the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    /// <remarks>
    /// Note that this method is almost 2x faster than LockAsync equivalent when using try/finally due to the construction of
    /// MutexSlimLock. If you have very high performance code, using this method is recommended.
    /// </remarks>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>void.</returns>
    public ValueTask WaitAsync(CancellationToken cancellationToken = default)
    {
        for (var spin = SpinCount; spin > 0; spin--)
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            if (acquiredLock)
            {
                return ValueTask.CompletedTask;
            }

            _ = Thread.Yield();
        }

        return this.WaitAsyncInternal(cancellationToken);
    }

    /// <summary>
    /// Synchronously waits to enter the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    public void Wait(CancellationToken cancellationToken = default)
    {
        var valueTask = this.WaitAsync(cancellationToken);
        var task = valueTask.AsTask();
        _ = Task.WaitAny(new[] { task }, CancellationToken.None);
        if (task.Exception != null)
        {
            throw task.Exception.InnerException!;
        }
    }

    /// <summary>
    /// Releases the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    public void Release()
    {
        var oldValue = Interlocked.Decrement(ref this.lockCount);
        if (oldValue != 0)
        {
            throw new InvalidOperationException("Cannot release an unowned MutexSlim.");
        }

        if (IsArm)
        {
            Interlocked.MemoryBarrier();
        }

        if (this.waitingThreads != 0)
        {
            _ = this.semaphore.Release();
        }
    }

    /// <summary>
    /// Releases the unmanaged resources used by the KestrelToolbox.Types.MutexSlim.
    /// </summary>
    [SuppressMessage(
        "Blocker Code Smell",
        "S3877:Exceptions should not be thrown from unexpected methods",
        Justification = "There are invalid ways to dispose of this structure."
    )]
    public void Dispose()
    {
        if (this.waitingThreads != 0 || this.lockCount != 0)
        {
            throw new InvalidOperationException(
                "Cannot dispose of MutexSlim while threads are waiting to lock or the lock is acquired."
            );
        }

        this.semaphore.Dispose();
    }

    /// <summary>
    /// Use this method if you could not acquire the lock with spins. It is expensive because it is an async method.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>MutexSlimLock object that can be used to release the lock. This value can be ignored.</returns>
    private async ValueTask<MutexSlimLock> LockAsyncInternal(CancellationToken cancellationToken)
    {
        await this.WaitAsyncInternal(cancellationToken);
        return new MutexSlimLock(this);
    }

    /// <summary>
    /// Use this method if you could not acquire the lock with spins. It is expensive because it is an async method.
    /// </summary>
    /// <param name="cancellationToken">The System.Threading.CancellationToken token to observe.</param>
    /// <returns>void.</returns>
    private async ValueTask WaitAsyncInternal(CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        _ = Interlocked.Increment(ref this.waitingThreads);
        try
        {
            var acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            while (!acquiredLock)
            {
                await this.semaphore.WaitAsync(cancellationToken);
                acquiredLock = Interlocked.CompareExchange(ref this.lockCount, 1, 0) == 0;
            }
        }
        finally
        {
            _ = Interlocked.Decrement(ref this.waitingThreads);
        }
    }
}

/// <summary>
/// Lock object that will release a lock when disposed.
/// </summary>
public readonly struct MutexSlimLock : IDisposable
{
    private readonly MutexSlim mutex;

    /// <summary>
    /// Initializes a new instance of the <see cref="MutexSlimLock"/> struct.
    /// </summary>
    /// <param name="mutex">The mutex that has been locked.</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal MutexSlimLock(MutexSlim mutex)
    {
        this.mutex = mutex;
    }

    /// <summary>
    /// Unlocks the mutex.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Dispose() => this.mutex.Release();
}
