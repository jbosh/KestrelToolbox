// <copyright file="IPAddressRange.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace KestrelToolbox.Types;

/// <summary>
/// Structure to represent a range of IP addresses.
/// </summary>
public unsafe struct IPAddressRange : IEquatable<IPAddressRange>
{
    /// <summary>
    /// Maximum size an address will ever be in bytes. Ipv6 is 16 bytes.
    /// </summary>
    private const int MaxByteArraySize = 16;

    private static readonly Regex CidrMatch = new(
        @"(?<address>[\da-f:\.]+)[\s]*\/[\s]*(?<mask>\d+)",
        RegexOptions.IgnoreCase | RegexOptions.Compiled
    );

    private readonly int length;
#pragma warning disable 649 // Not assigned, it most definitely is.
    private fixed byte beginBytes[MaxByteArraySize];
    private fixed byte endBytes[MaxByteArraySize];
#pragma warning restore 649

    private static void AndBytes(Span<byte> a, ReadOnlySpan<byte> b)
    {
        for (var i = 0; i < a.Length; i++)
        {
            a[i] = (byte)(a[i] & b[i]);
        }
    }

    private static void OrBytes(Span<byte> a, ReadOnlySpan<byte> b)
    {
        for (var i = 0; i < a.Length; i++)
        {
            a[i] = (byte)(a[i] | b[i]);
        }
    }

    private static void NotBytes(Span<byte> a)
    {
        for (var i = 0; i < a.Length; i++)
        {
            a[i] = (byte)~a[i];
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="IPAddressRange"/> struct.
    /// </summary>
    /// <param name="address">The address for this range to represent.</param>
    public IPAddressRange(IPAddress address)
    {
        Span<byte> bytes = stackalloc byte[MaxByteArraySize];
        if (!address.TryWriteBytes(bytes, out var bytesWritten))
        {
            throw new IndexOutOfRangeException("IPAddressRange was not large enough for this IPAddress.");
        }

        this.length = bytesWritten;
        fixed (byte* beginBytesPtr = this.beginBytes)
        {
            bytes.Slice(0, this.length).CopyTo(new Span<byte>(beginBytesPtr, MaxByteArraySize));
        }

        fixed (byte* endBytesPtr = this.endBytes)
        {
            bytes.Slice(0, this.length).CopyTo(new Span<byte>(endBytesPtr, MaxByteArraySize));
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="IPAddressRange"/> struct.
    /// </summary>
    /// <param name="address">The address for this range to represent.</param>
    /// <param name="maskLength">Mask of valid bits in the address.</param>
    public IPAddressRange(IPAddress address, int maskLength)
    {
        Span<byte> bytes = stackalloc byte[MaxByteArraySize];
        Span<byte> maskBytes = stackalloc byte[MaxByteArraySize];
        if (!address.TryWriteBytes(bytes, out var bytesWritten))
        {
            throw new IndexOutOfRangeException("IPAddressRange was not large enough for this IPAddress.");
        }

        this.length = bytesWritten;
        if (this.length * 8 < maskLength)
        {
            throw new ArgumentException($"{nameof(maskLength)} was too long.", nameof(maskLength));
        }

        {
            var byteCount = maskLength / 8;
            var bits = maskLength % 8;
            for (var i = 0; i < byteCount; i++)
            {
                maskBytes[i] = 0xFF;
            }

            if (bits > 0)
            {
                maskBytes[^1] = (byte)~(0xFF >> bits);
            }
        }

        AndBytes(bytes.Slice(0, this.length), maskBytes.Slice(0, this.length));

        fixed (byte* beginBytesPtr = this.beginBytes)
        {
            bytes.Slice(0, this.length).CopyTo(new Span<byte>(beginBytesPtr, MaxByteArraySize));
        }

        NotBytes(maskBytes.Slice(0, this.length));
        OrBytes(bytes, maskBytes);

        fixed (byte* endBytesPtr = this.endBytes)
        {
            bytes.Slice(0, this.length).CopyTo(new Span<byte>(endBytesPtr, MaxByteArraySize));
        }
    }

    /// <summary>
    /// Parses <paramref name="range"/> into a <see cref="IPAddressRange"/>.
    /// </summary>
    /// <param name="range">Cidr string. <value>192.168.0.0/16</value></param>
    /// <returns>Resulting <see cref="IPAddressRange"/>.</returns>
    /// <exception cref="ArgumentException">Occurs when <paramref name="range"/> is invalid.</exception>
    public static IPAddressRange Parse(string range)
    {
        if (!TryParse(range, out var result))
        {
            throw new ArgumentException($"{nameof(range)} was invalid.", nameof(range));
        }

        return result;
    }

    /// <summary>
    /// Try to parse an <see cref="IPAddressRange"/> from cidr formatted string.
    /// </summary>
    /// <param name="range">Cidr string. <value>192.168.0.0/16</value></param>
    /// <param name="addressRange">Resulting address range.</param>
    /// <returns>True if parsing was successful, false if not.</returns>
    public static bool TryParse(string range, out IPAddressRange addressRange)
    {
        Span<byte> bytes = stackalloc byte[MaxByteArraySize];
        var match = CidrMatch.Match(range);
        if (match.Success && IPAddress.TryParse(match.Groups["address"].Value, out var ipAddress))
        {
            var maskLength = int.Parse(match.Groups["mask"].Value, CultureInfo.InvariantCulture);

            if (!ipAddress.TryWriteBytes(bytes, out var bytesWritten))
            {
                throw new IndexOutOfRangeException("IPAddressRange was not large enough for this IPAddress.");
            }

            // too many bits
            if (bytesWritten * 8 < maskLength)
            {
                addressRange = default;
                return false;
            }

            addressRange = new IPAddressRange(ipAddress, maskLength);
            return true;
        }

        if (IPAddress.TryParse(range, out var iPAddress))
        {
            addressRange = new IPAddressRange(iPAddress);
            return true;
        }

        addressRange = default;
        return false;
    }

    /// <summary>
    /// Checks if an address is contained in the range.
    /// </summary>
    /// <param name="address">Address to check.</param>
    /// <returns>True if the address is in range, false if not.</returns>
    [Pure]
    public bool Contains(IPAddress address)
    {
        Span<byte> bytes = stackalloc byte[MaxByteArraySize];
        if (!address.TryWriteBytes(bytes, out var bytesWritten))
        {
            throw new OutOfMemoryException("IPAddressRange was not large enough for this IPAddress.");
        }

        bytes = bytes.Slice(0, bytesWritten);
        if (bytes.Length != this.length)
        {
            return false;
        }

        for (var i = 0; i < bytes.Length; i++)
        {
            if (bytes[i] < this.beginBytes[i])
            {
                return false;
            }
        }

        for (var i = 0; i < bytes.Length; i++)
        {
            if (bytes[i] > this.endBytes[i])
            {
                return false;
            }
        }

        return true;
    }

    /// <inheritdoc />
    [Pure]
    public override string ToString()
    {
        var builder = new StringBuilder(128);
        for (var i = 0; i < this.length - 1; i++)
        {
            _ = builder.Append(this.beginBytes[i]);
            _ = builder.Append('.');
        }

        _ = builder.Append(this.beginBytes[this.length - 1]);
        _ = builder.Append(" - ");

        for (var i = 0; i < this.length - 1; i++)
        {
            _ = builder.Append(this.endBytes[i]);
            _ = builder.Append('.');
        }

        _ = builder.Append(this.endBytes[this.length - 1]);
        return builder.ToString();
    }

    /// <inheritdoc />
    [Pure]
    public bool Equals(IPAddressRange other)
    {
        if (this.length != other.length)
        {
            return false;
        }

        for (var i = 0; i < MaxByteArraySize; i++)
        {
            if (this.beginBytes[i] != other.beginBytes[i])
            {
                return false;
            }
        }

        for (var i = 0; i < MaxByteArraySize; i++)
        {
            if (this.endBytes[i] != other.endBytes[i])
            {
                return false;
            }
        }

        return true;
    }

    /// <inheritdoc />
    [Pure]
    public override bool Equals(object? obj)
    {
        return obj is IPAddressRange other && this.Equals(other);
    }

    /// <inheritdoc />
    [SuppressMessage(
        "Minor Bug",
        "S2328:\"GetHashCode\" should not reference mutable fields",
        Justification = "They're not mutable, compiler just can't know."
    )]
    [Pure]
    public override int GetHashCode()
    {
        unchecked
        {
            var hash = 19;
            hash = (hash * 31) + this.length.GetHashCode();

            for (var i = 0; i < MaxByteArraySize; i++)
            {
                hash = (hash * 31) + this.beginBytes[i];
            }

            for (var i = 0; i < MaxByteArraySize; i++)
            {
                hash = (hash * 31) + this.endBytes[i];
            }

            return hash;
        }
    }

    /// <summary>
    /// Test equivalence of two <see cref="IPAddressRange"/>.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two are equivalent, false if not.</returns>
    public static bool operator ==(IPAddressRange left, IPAddressRange right) => left.Equals(right);

    /// <summary>
    /// Test equivalence of two <see cref="IPAddressRange"/>.
    /// </summary>
    /// <param name="left">Left hand side.</param>
    /// <param name="right">Right hand side.</param>
    /// <returns>True if the two are not equivalent, false if they are.</returns>
    public static bool operator !=(IPAddressRange left, IPAddressRange right) => !(left == right);
}
