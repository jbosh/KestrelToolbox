// <copyright file="AtomicUInt.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;

namespace KestrelToolbox.Types.Atomic;

/// <summary>
/// Atomic representation of <see cref="int"/>. Every operation can be performed safely from multiple threads.
/// </summary>
public class AtomicUInt
{
    private int value;

    /// <summary>
    /// Initializes a new instance of the <see cref="AtomicUInt"/> class.
    /// </summary>
    public AtomicUInt() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="AtomicUInt"/> class.
    /// </summary>
    /// <param name="value">The initial value to store.</param>
    public AtomicUInt(uint value)
    {
        this.value = (int)value;
    }

    /// <summary>
    /// Atomically replaces the value with a non-atomic argument.
    /// </summary>
    /// <param name="value">New value to store.</param>
    public void Store(uint value) => this.value = (int)value;

    /// <summary>
    /// Atomically obtains the value.
    /// </summary>
    /// <returns>The read value.</returns>
    public uint Load() => (uint)Interlocked.CompareExchange(ref this.value, 0, 0);

    /// <summary>
    /// Atomically replaces the value and obtains the value that was held previously.
    /// </summary>
    /// <param name="value">New value to store.</param>
    /// <returns>Previous value.</returns>
    public uint Exchange(uint value) => (uint)Interlocked.Exchange(ref this.value, (int)value);

    /// <summary>
    /// Atomically compares <paramref name="expected"/> with value. If the two values are equivalent,
    /// the stored value is updated to be <paramref name="desired"/>. <paramref name="expected"/> is
    /// updated with the previously stored value.
    /// </summary>
    /// <param name="expected">
    /// Reference to the expected value to be found. Gets stored with the actual value if comparison fails.
    /// </param>
    /// <param name="desired">The value to store if it is the expected value.</param>
    /// <returns>True if the underlying atomic value was successfully changed, false otherwise.</returns>
    /// <seealso cref="CompareExchange(uint,uint)"/>
    public bool CompareExchange(ref uint expected, uint desired)
    {
        var oldValue = (uint)Interlocked.CompareExchange(ref this.value, (int)desired, (int)expected);
        var success = oldValue == expected;
        expected = oldValue;
        return success;
    }

    /// <summary>
    /// Atomically compares <paramref name="expected"/> with value. If the two values are equivalent,
    /// the stored value is updated to be <paramref name="desired"/>.
    /// </summary>
    /// <param name="expected">Reference to the expected value to be found.</param>
    /// <param name="desired">The value to store if it is the expected value.</param>
    /// <returns>True if the underlying atomic value was successfully changed, false otherwise.</returns>
    /// <seealso cref="CompareExchange(ref uint,uint)"/>
    public bool CompareExchange(uint expected, uint desired)
    {
        var oldValue = (uint)Interlocked.CompareExchange(ref this.value, (int)desired, (int)expected);
        return oldValue == expected;
    }

    /// <summary>
    /// Atomically adds <paramref name="value"/> to the stored value.
    /// </summary>
    /// <param name="value">Value to add.</param>
    /// <returns>Previously stored value.</returns>
    public uint FetchAdd(uint value) => (uint)Interlocked.Add(ref this.value, (int)value) - value;

    /// <summary>
    /// Atomically subtracts <paramref name="value"/> from the stored value.
    /// </summary>
    /// <param name="value">Value to subtract.</param>
    /// <returns>Previously stored value.</returns>
    public uint FetchSub(uint value) => this.FetchAdd(0u - value);

    /// <summary>
    /// Atomically ors <paramref name="value"/> to the stored value.
    /// </summary>
    /// <param name="value">Value to or.</param>
    /// <returns>Previously stored value.</returns>
    public uint FetchOr(uint value) => (uint)Interlocked.Or(ref this.value, (int)value);

    /// <summary>
    /// Atomically ands <paramref name="value"/> to the stored value.
    /// </summary>
    /// <param name="value">Value to and.</param>
    /// <returns>Previously stored value.</returns>
    public uint FetchAnd(uint value) => (uint)Interlocked.And(ref this.value, (int)value);

    /// <summary>
    /// Atomically adds 1 to the stored value.
    /// </summary>
    /// <returns>Previously stored value.</returns>
    public uint Increment() => this.FetchAdd(1);

    /// <summary>
    /// Atomically subtracts 1 from the stored value.
    /// </summary>
    /// <returns>Previously stored value.</returns>
    public uint Decrement() => this.FetchSub(1);

    /// <inheritdoc/>
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Matching core lib.")]
    public override string ToString() => this.Load().ToString();

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation, using the specified format.
    /// </summary>
    /// <param name="format">A numeric format string.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="format"/>.</returns>
    /// <exception cref="FormatException"><paramref name="format"/> is invalid or not supported.</exception>
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Matching core lib.")]
    public string ToString(string? format) => this.Load().ToString(format);

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation using the specified format and culture-specific format information.
    /// </summary>
    /// <param name="format">A numeric format string.</param>
    /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="format"/> and <paramref name="provider"/>.</returns>
    public string ToString(string? format, IFormatProvider? provider) => this.Load().ToString(format, provider);

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation using the specified culture-specific format information.
    /// </summary>
    /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="provider"/>.</returns>
    public string ToString(IFormatProvider? provider) => this.Load().ToString(provider);

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is AtomicUInt other && this.Load().Equals(other.Load());

    /// <inheritdoc/>
    public override int GetHashCode() => this.Load().GetHashCode();
}
