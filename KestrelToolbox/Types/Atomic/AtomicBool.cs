// <copyright file="AtomicBool.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;

namespace KestrelToolbox.Types.Atomic;

/// <summary>
/// Atomic representation of <see cref="int"/>. Every operation can be performed safely from multiple threads.
/// </summary>
public class AtomicBool
{
    private int value;

    /// <summary>
    /// Initializes a new instance of the <see cref="AtomicBool"/> class.
    /// </summary>
    public AtomicBool() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="AtomicBool"/> class.
    /// </summary>
    /// <param name="value">The initial value to store.</param>
    public AtomicBool(bool value)
    {
        this.value = value ? 1 : 0;
    }

    /// <summary>
    /// Atomically replaces the value with a non-atomic argument.
    /// </summary>
    /// <param name="value">New value to store.</param>
    public void Store(bool value) => this.value = value ? 1 : 0;

    /// <summary>
    /// Atomically obtains the value.
    /// </summary>
    /// <returns>The read value.</returns>
    public bool Load() => Interlocked.CompareExchange(ref this.value, 0, 0) != 0;

    /// <summary>
    /// Atomically replaces the value and obtains the value that was held previously.
    /// </summary>
    /// <param name="value">New value to store.</param>
    /// <returns>Previous value.</returns>
    public bool Exchange(bool value) => Interlocked.Exchange(ref this.value, value ? 1 : 0) != 0;

    /// <summary>
    /// Atomically compares <paramref name="expected"/> with value. If the two values are equivalent,
    /// the stored value is updated to be <paramref name="desired"/>. <paramref name="expected"/> is
    /// updated with the previously stored value.
    /// </summary>
    /// <param name="expected">
    /// Reference to the expected value to be found. Gets stored with the actual value if comparison fails.
    /// </param>
    /// <param name="desired">The value to store if it is the expected value.</param>
    /// <returns>True if the underlying atomic value was successfully changed, false otherwise.</returns>
    /// <seealso cref="CompareExchange(bool,bool)"/>
    public bool CompareExchange(ref bool expected, bool desired)
    {
        var desiredValue = desired ? 1 : 0;
        var expectedValue = expected ? 1 : 0;
        var oldValue = Interlocked.CompareExchange(ref this.value, desiredValue, expectedValue);
        var success = oldValue == expectedValue;
        expected = oldValue != 0;
        return success;
    }

    /// <summary>
    /// Atomically compares <paramref name="expected"/> with value. If the two values are equivalent,
    /// the stored value is updated to be <paramref name="desired"/>.
    /// </summary>
    /// <param name="expected">Reference to the expected value to be found.</param>
    /// <param name="desired">The value to store if it is the expected value.</param>
    /// <returns>True if the underlying atomic value was successfully changed, false otherwise.</returns>
    /// <seealso cref="CompareExchange(ref bool,bool)"/>
    public bool CompareExchange(bool expected, bool desired)
    {
        var desiredValue = desired ? 1 : 0;
        var expectedValue = expected ? 1 : 0;
        var oldValue = Interlocked.CompareExchange(ref this.value, desiredValue, expectedValue);
        return oldValue == expectedValue;
    }

    /// <inheritdoc/>
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Matching core lib.")]
    public override string ToString() => this.Load().ToString();

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation using the specified culture-specific format information.
    /// </summary>
    /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="provider"/>.</returns>
    public string ToString(IFormatProvider? provider) => this.Load().ToString(provider);

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is AtomicBool other && this.Load().Equals(other.Load());

    /// <inheritdoc/>
    public override int GetHashCode() => this.Load().GetHashCode();
}
