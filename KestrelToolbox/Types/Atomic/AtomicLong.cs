// <copyright file="AtomicLong.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;

namespace KestrelToolbox.Types.Atomic;

/// <summary>
/// Atomic representation of <see cref="long"/>. Every operation can be performed safely from multiple threads.
/// </summary>
public class AtomicLong
{
    private long value;

    /// <summary>
    /// Initializes a new instance of the <see cref="AtomicLong"/> class.
    /// </summary>
    public AtomicLong() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="AtomicLong"/> class.
    /// </summary>
    /// <param name="value">The initial value to store.</param>
    public AtomicLong(long value)
    {
        this.value = value;
    }

    /// <summary>
    /// Atomically replaces the value with a non-atomic argument.
    /// </summary>
    /// <param name="value">New value to store.</param>
    public void Store(long value) => this.value = value;

    /// <summary>
    /// Atomically obtains the value.
    /// </summary>
    /// <returns>The read value.</returns>
    public long Load() => Interlocked.Read(ref this.value);

    /// <summary>
    /// Atomically replaces the value and obtains the value that was held previously.
    /// </summary>
    /// <param name="value">New value to store.</param>
    /// <returns>Previous value.</returns>
    public long Exchange(long value) => Interlocked.Exchange(ref this.value, value);

    /// <summary>
    /// Atomically compares <paramref name="expected"/> with value. If the two values are equivalent,
    /// the stored value is updated to be <paramref name="desired"/>. <paramref name="expected"/> is
    /// updated with the previously stored value.
    /// </summary>
    /// <param name="expected">
    /// Reference to the expected value to be found. Gets stored with the actual value if comparison fails.
    /// </param>
    /// <param name="desired">The value to store if it is the expected value.</param>
    /// <returns>True if the underlying atomic value was successfully changed, false otherwise.</returns>
    /// <seealso cref="CompareExchange(long,long)"/>
    public bool CompareExchange(ref long expected, long desired)
    {
        var oldValue = Interlocked.CompareExchange(ref this.value, desired, expected);
        var success = oldValue == expected;
        expected = oldValue;
        return success;
    }

    /// <summary>
    /// Atomically compares <paramref name="expected"/> with value. If the two values are equivalent,
    /// the stored value is updated to be <paramref name="desired"/>.
    /// </summary>
    /// <param name="expected">Reference to the expected value to be found.</param>
    /// <param name="desired">The value to store if it is the expected value.</param>
    /// <returns>True if the underlying atomic value was successfully changed, false otherwise.</returns>
    /// <seealso cref="CompareExchange(ref long,long)"/>
    public bool CompareExchange(long expected, long desired)
    {
        var oldValue = Interlocked.CompareExchange(ref this.value, desired, expected);
        return oldValue == expected;
    }

    /// <summary>
    /// Atomically adds <paramref name="value"/> to the stored value.
    /// </summary>
    /// <param name="value">Value to add.</param>
    /// <returns>Previously stored value.</returns>
    public long FetchAdd(long value) => Interlocked.Add(ref this.value, value) - value;

    /// <summary>
    /// Atomically subtracts <paramref name="value"/> from the stored value.
    /// </summary>
    /// <param name="value">Value to subtract.</param>
    /// <returns>Previously stored value.</returns>
    public long FetchSub(long value) => this.FetchAdd(-value);

    /// <summary>
    /// Atomically ors <paramref name="value"/> to the stored value.
    /// </summary>
    /// <param name="value">Value to or.</param>
    /// <returns>Previously stored value.</returns>
    public long FetchOr(long value) => Interlocked.Or(ref this.value, value);

    /// <summary>
    /// Atomically ands <paramref name="value"/> to the stored value.
    /// </summary>
    /// <param name="value">Value to and.</param>
    /// <returns>Previously stored value.</returns>
    public long FetchAnd(long value) => Interlocked.And(ref this.value, value);

    /// <summary>
    /// Atomically adds 1 to the stored value.
    /// </summary>
    /// <returns>Previously stored value.</returns>
    public long Increment() => this.FetchAdd(1);

    /// <summary>
    /// Atomically subtracts 1 from the stored value.
    /// </summary>
    /// <returns>Previously stored value.</returns>
    public long Decrement() => this.FetchSub(1);

    /// <inheritdoc/>
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Matching core lib.")]
    public override string ToString() => this.Load().ToString();

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation, using the specified format.
    /// </summary>
    /// <param name="format">A numeric format string.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="format"/>.</returns>
    /// <exception cref="FormatException"><paramref name="format"/> is invalid or not supported.</exception>
    [SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Matching core lib.")]
    public string ToString(string? format) => this.Load().ToString(format);

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation using the specified format and culture-specific format information.
    /// </summary>
    /// <param name="format">A numeric format string.</param>
    /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="format"/> and <paramref name="provider"/>.</returns>
    public string ToString(string? format, IFormatProvider? provider) => this.Load().ToString(format, provider);

    /// <summary>
    /// Converts the numeric value of this instance to its equivalent string representation using the specified culture-specific format information.
    /// </summary>
    /// <param name="provider">An <see cref="IFormatProvider"/> that supplies culture-specific formatting information.</param>
    /// <returns>The string representation of the value of this instance as specified by <paramref name="provider"/>.</returns>
    public string ToString(IFormatProvider? provider) => this.Load().ToString(provider);

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj is AtomicLong other && this.Load().Equals(other.Load());

    /// <inheritdoc/>
    public override int GetHashCode() => this.Load().GetHashCode();
}
