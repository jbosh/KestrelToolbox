// <copyright file="ArrayPoolRental.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;

namespace KestrelToolbox.Types;

/// <summary>
/// Structure to rent and return byte[] from default array pool. It's recommended that you add
/// the new instance to a using expression to return the rental when you are done with it.
/// </summary>
/// <seealso cref="ArrayPool{T}"/>
public struct ArrayPoolRental : IDisposable
{
    /// <summary>
    /// Gets the raw array that was rented.
    /// </summary>
    public byte[] Array
    {
        [DebuggerStepThrough]
        get;
        [DebuggerStepThrough]
        private set;
    }

    /// <summary>
    /// Gets the length of the underlying array.
    /// </summary>
    public int Length => this.Array.Length;

    /// <summary>
    /// Gets or sets a value indicating whether the returned buffer will be cleared before returning it to the
    /// pool.
    /// </summary>
    public bool ClearContentsOnDispose { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether any rented arrays will be cleared before first use.
    /// </summary>
    public bool ClearContentsOnInitialization { get; set; }

    /// <summary>
    /// Creates a new span over the rented array.
    /// </summary>
    /// <returns>Rental as a span.</returns>
    [DebuggerStepThrough]
    public Span<byte> AsSpan() => this.Array.AsSpan();

    /// <summary>
    /// Creates a new span over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <returns>Rental as a span.</returns>
    [DebuggerStepThrough]
    public Span<byte> AsSpan(int start) => this.Array.AsSpan(start);

    /// <summary>
    /// Creates a new span over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <param name="length">The number of items in the span.</param>
    /// <returns>Rental as a span.</returns>
    [DebuggerStepThrough]
    public Span<byte> AsSpan(int start, int length) => this.Array.AsSpan(start, length);

    /// <summary>
    /// Creates a new memory region over the rented array.
    /// </summary>
    /// <returns>Rental as memory.</returns>
    [DebuggerStepThrough]
    public Memory<byte> AsMemory() => this.Array.AsMemory();

    /// <summary>
    /// Creates a new memory region over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <returns>Rental as memory.</returns>
    [DebuggerStepThrough]
    public Memory<byte> AsMemory(int start) => this.Array.AsMemory(start);

    /// <summary>
    /// Creates a new memory region over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <param name="length">The number of items in the span.</param>
    /// <returns>Rental as memory.</returns>
    [DebuggerStepThrough]
    public Memory<byte> AsMemory(int start, int length) => this.Array.AsMemory(start, length);

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayPoolRental"/> struct.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    public ArrayPoolRental(int minimumLength)
        : this(minimumLength, false, false) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayPoolRental"/> struct.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    /// <param name="clearContentsOnInitialization">Indicates whether the contents of the buffer should be cleared on initialization.</param>
    public ArrayPoolRental(int minimumLength, bool clearContentsOnInitialization)
        : this(minimumLength, clearContentsOnInitialization, false) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayPoolRental"/> struct.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    /// <param name="clearContentsOnInitialization">Indicates whether the contents of the buffer should be cleared on initialization.</param>
    /// <param name="clearContentsOnDispose">Indicates whether the contents of the buffer should be cleared on dispose.</param>
    public ArrayPoolRental(int minimumLength, bool clearContentsOnInitialization, bool clearContentsOnDispose)
    {
        this.ClearContentsOnInitialization = clearContentsOnInitialization;
        this.ClearContentsOnDispose = clearContentsOnDispose;
        this.Array = ArrayPool<byte>.Shared.Rent(minimumLength);

        if (clearContentsOnInitialization)
        {
            System.Array.Clear(this.Array);
        }
    }

    /// <summary>
    /// Resizes the array without copying to the new specified <paramref name="minimumLength"/>.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    public void Resize(int minimumLength)
    {
        if (this.Array == null)
        {
            this.Array = ArrayPool<byte>.Shared.Rent(minimumLength);
            if (this.ClearContentsOnInitialization)
            {
                System.Array.Clear(this.Array);
            }

            return;
        }

        if (this.Array.Length >= minimumLength)
        {
            return;
        }

        var array = this.Array;
        this.Array = ArrayPool<byte>.Shared.Rent(minimumLength);
        if (this.ClearContentsOnInitialization)
        {
            System.Array.Clear(this.Array);
        }

        ArrayPool<byte>.Shared.Return(array, this.ClearContentsOnDispose);
    }

    /// <summary>
    /// Resizes the array to the new specified <paramref name="minimumLength"/> and copies the
    /// original array.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    public void ResizeAndKeepContents(int minimumLength)
    {
        if (this.Array.Length >= minimumLength)
        {
            return;
        }

        var array = this.Array;
        this.Array = ArrayPool<byte>.Shared.Rent(minimumLength);
        if (this.ClearContentsOnInitialization)
        {
            System.Array.Clear(this.Array);
        }

        array.CopyTo(this.Array, 0);
        ArrayPool<byte>.Shared.Return(array);
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
        if (this.Array != null)
        {
            ArrayPool<byte>.Shared.Return(this.Array, this.ClearContentsOnDispose);
            this.Array = null!;
        }
    }
}

/// <summary>
/// Structure to rent and return <typeparamref name="T"/>[] from default array pool. It's recommended
/// that you add the new instance to a using expression to return the rental when you are done with it.
/// </summary>
/// <seealso cref="ArrayPool{T}"/>
/// <typeparam name="T">Type of elements in the pool.</typeparam>
public struct ArrayPoolRental<T> : IDisposable
{
    /// <summary>
    /// Gets the raw array that was rented.
    /// </summary>
    public T[] Array
    {
        [DebuggerStepThrough]
        get;
        [DebuggerStepThrough]
        private set;
    }

    /// <summary>
    /// Gets the length of the underlying array.
    /// </summary>
    public int Length => this.Array.Length;

    /// <summary>
    /// Gets or sets a value indicating whether or not the returned buffer will be cleared before returning it to the
    /// pool.
    /// </summary>
    public bool ClearContentsOnDispose { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether or not any rented arrays will be cleared before first use.
    /// </summary>
    public bool ClearContentsOnInitialization { get; set; }

    /// <summary>
    /// Creates a new span over the rented array.
    /// </summary>
    /// <returns>Rental as a span.</returns>
    [DebuggerStepThrough]
    public Span<T> AsSpan() => this.Array.AsSpan();

    /// <summary>
    /// Creates a new span over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <returns>Rental as a span.</returns>
    [DebuggerStepThrough]
    public Span<T> AsSpan(int start) => this.Array.AsSpan(start);

    /// <summary>
    /// Creates a new span over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <param name="length">The number of items in the span.</param>
    /// <returns>Rental as a span.</returns>
    [DebuggerStepThrough]
    public Span<T> AsSpan(int start, int length) => this.Array.AsSpan(start, length);

    /// <summary>
    /// Creates a new memory region over the rented array.
    /// </summary>
    /// <returns>Rental as memory.</returns>
    [DebuggerStepThrough]
    public Memory<T> AsMemory() => this.Array.AsMemory();

    /// <summary>
    /// Creates a new memory region over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <returns>Rental as memory.</returns>
    [DebuggerStepThrough]
    public Memory<T> AsMemory(int start) => this.Array.AsMemory(start);

    /// <summary>
    /// Creates a new memory region over the rented array.
    /// </summary>
    /// <param name="start">The initial index from which the array will be converted.</param>
    /// <param name="length">The number of items in the span.</param>
    /// <returns>Rental as memory.</returns>
    [DebuggerStepThrough]
    public Memory<T> AsMemory(int start, int length) => this.Array.AsMemory(start, length);

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayPoolRental{T}"/> struct.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    public ArrayPoolRental(int minimumLength)
        : this(minimumLength, false, false) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayPoolRental{T}"/> struct.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    /// <param name="clearContentsOnInitialization">Indicates whether the contents of the buffer should be cleared on initialization.</param>
    public ArrayPoolRental(int minimumLength, bool clearContentsOnInitialization)
        : this(minimumLength, clearContentsOnInitialization, false) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayPoolRental{T}"/> struct.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    /// <param name="clearContentsOnInitialization">Indicates whether the contents of the buffer should be cleared on initialization.</param>
    /// <param name="clearContentsOnDispose">Indicates whether the contents of the buffer should be cleared on dispose.</param>
    public ArrayPoolRental(int minimumLength, bool clearContentsOnInitialization, bool clearContentsOnDispose)
    {
        this.ClearContentsOnInitialization = clearContentsOnInitialization;
        this.ClearContentsOnDispose = clearContentsOnDispose;
        this.Array = ArrayPool<T>.Shared.Rent(minimumLength);

        if (clearContentsOnInitialization)
        {
            System.Array.Clear(this.Array);
        }
    }

    /// <summary>
    /// Resizes the array without copying to the new specified <paramref name="minimumLength"/>.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    public void Resize(int minimumLength)
    {
        if (this.Array.Length >= minimumLength)
        {
            return;
        }

        var array = this.Array;
        this.Array = ArrayPool<T>.Shared.Rent(minimumLength);
        if (this.ClearContentsOnInitialization)
        {
            System.Array.Clear(this.Array);
        }

        ArrayPool<T>.Shared.Return(array, this.ClearContentsOnDispose);
    }

    /// <summary>
    /// Resizes the array to the new specified <paramref name="minimumLength"/> and copies the
    /// original array.
    /// </summary>
    /// <param name="minimumLength">The minimum length of the array.</param>
    public void ResizeAndKeepContents(int minimumLength)
    {
        if (this.Array.Length >= minimumLength)
        {
            return;
        }

        var array = this.Array;
        this.Array = ArrayPool<T>.Shared.Rent(minimumLength);
        if (this.ClearContentsOnInitialization)
        {
            System.Array.Clear(this.Array);
        }

        array.CopyTo(this.Array, 0);
        ArrayPool<T>.Shared.Return(array);
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
        if (this.Array != null)
        {
            ArrayPool<T>.Shared.Return(this.Array, this.ClearContentsOnDispose);
            this.Array = null!;
        }
    }
}
