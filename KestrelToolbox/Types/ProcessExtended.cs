// <copyright file="ProcessExtended.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Specialized;
using Microsoft.Win32.SafeHandles;

namespace KestrelToolbox.Types;

/// <summary>
/// Provides access to local and remote processes and enables you to start and stop
/// local system processes. The extended edition always assumes you want stdout and
/// stderr or are firing a one off and want to await.
/// </summary>
public sealed class ProcessExtended : IDisposable
{
    /// <summary>
    /// Callback for when text data is received from stdout or stderr.
    /// </summary>
    /// <param name="data">Text received.</param>
    public delegate void DataReceived(string data);

    /// <summary>
    /// Occurs when stdout lines are received.
    /// </summary>
    public event DataReceived? OnData;

    /// <summary>
    /// Occurs when stderr lines are received.
    /// </summary>
    public event DataReceived? OnError;

    private readonly ProcessStartInfo startInfo;
    private Process? process;
    private TaskCompletionSource<int>? exitSource;
    private int exitCount;

    /// <summary>
    /// Gets search paths for files, directories for temporary files, application-specific options, and other similar information.
    /// </summary>
    /// <returns>
    /// A string dictionary that provides environment variables that apply to this process and child processes. The default is null.
    /// </returns>
    public StringDictionary EnvironmentVariables => this.startInfo.EnvironmentVariables;

    /// <summary>
    /// Gets the environment variables that apply to this process and its child processes.
    /// </summary>
    /// <returns>
    /// A generic dictionary containing the environment variables that apply to this
    /// process and its child processes. The default is null.
    /// </returns>
    public IDictionary<string, string?> Environment => this.startInfo.Environment;

    /// <summary>
    /// Gets or sets the working directory for the process to be started.
    /// </summary>
    /// <returns>
    /// The working directory for the process to be started. The default is an empty string ("").
    /// </returns>
    public string WorkingDirectory
    {
        get => this.startInfo.WorkingDirectory;
        set => this.startInfo.WorkingDirectory = value;
    }

    /// <summary>
    /// Gets the time that the associated process was started.
    /// </summary>
    /// <returns>
    /// An object that indicates when the process started. An exception is thrown if
    /// the process is not running.
    /// </returns>
    /// <exception cref="System.NotSupportedException">
    /// You are attempting to access the System.Diagnostics.Process.StartTime property
    /// for a process that is running on a remote computer. This property is available
    /// only for processes that are running on the local computer.
    /// </exception>
    /// <exception cref="System.InvalidOperationException">
    /// The process has exited. -or- The process has not been started.
    /// </exception>
    /// <exception cref="System.ComponentModel.Win32Exception">
    /// An error occurred in the call to the Windows function.
    /// </exception>
    public DateTime StartTime => this.NonNullProcess.StartTime;

    /// <summary>
    /// Gets the Terminal Services session identifier for the associated process.
    /// </summary>
    /// <returns>
    /// The Terminal Services session identifier for the associated process.
    /// </returns>
    /// <exception cref="System.NullReferenceException">
    /// There is no session associated with this process.
    /// </exception>
    /// <exception cref="System.InvalidOperationException">
    /// There is no process associated with this session identifier. -or- The associated
    /// process is not on this machine.
    /// </exception>
    public int SessionId => this.NonNullProcess.SessionId;

    /// <summary>
    /// Gets the native handle to this process.
    /// </summary>
    /// <returns>
    /// The native handle to this process.
    /// </returns>
    public SafeProcessHandle SafeHandle => this.NonNullProcess.SafeHandle;

    /// <summary>
    /// Gets a value indicating whether the user interface of the process is responding.
    /// </summary>
    /// <returns>
    /// true if the user interface of the associated process is responding to the system;
    /// otherwise, false.
    /// </returns>
    /// <exception cref="System.InvalidOperationException">
    /// There is no process associated with this System.Diagnostics.Process object.
    /// </exception>
    /// <exception cref="System.NotSupportedException">
    /// You are attempting to access the System.Diagnostics.Process.Responding property
    /// for a process that is running on a remote computer. This property is available
    /// only for processes that are running on the local computer.
    /// </exception>
    public bool Responding => this.NonNullProcess.Responding;

    /// <summary>
    /// Gets a value indicating whether the associated process has been terminated.
    /// </summary>
    /// <returns>
    /// true if the operating system process referenced by the System.Diagnostics.Process
    /// component has terminated; otherwise, false.
    /// </returns>
    /// <exception cref="System.InvalidOperationException">
    /// There is no process associated with the object.
    /// </exception>
    /// <exception cref="System.ComponentModel.Win32Exception">
    /// The exit code for the process could not be retrieved.
    /// </exception>
    /// <exception cref="System.NotSupportedException">
    /// You are trying to access the System.Diagnostics.Process.HasExited property for
    /// a process that is running on a remote computer. This property is available only
    /// for processes that are running on the local computer.
    /// </exception>
    public bool HasExited => this.NonNullProcess.HasExited;

    /// <summary>
    /// Gets the time that the associated process exited.
    /// </summary>
    /// <returns>
    /// A System.DateTime that indicates when the associated process was terminated.
    /// </returns>
    /// <exception cref="System.NotSupportedException">
    /// You are trying to access the System.Diagnostics.Process.ExitTime property for
    /// a process that is running on a remote computer. This property is available only
    /// for processes that are running on the local computer.
    /// </exception>
    public DateTime ExitTime => this.NonNullProcess.ExitTime;

    /// <summary>
    /// Gets the value that the associated process specified when it terminated.
    /// </summary>
    /// <returns>
    /// The code that the associated process specified when it terminated.
    /// </returns>
    /// <exception cref="System.InvalidOperationException">
    /// The process has not exited. -or- The process System.Diagnostics.Process.Handle
    /// is not valid.
    /// </exception>
    /// <exception cref="System.NotSupportedException">
    /// You are trying to access the System.Diagnostics.Process.ExitCode property for
    /// a process that is running on a remote computer. This property is available only
    /// for processes that are running on the local computer.
    /// </exception>
    public int ExitCode => this.NonNullProcess.ExitCode;

    private Process NonNullProcess
    {
        get
        {
            if (this.process == null)
            {
                throw new InvalidOperationException("Process has not been started.");
            }

            return this.process;
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessExtended"/> class,
    /// specifies an application file name with which to start the process, and specifies
    /// a set of command-line arguments to pass to the application.
    /// </summary>
    /// <param name="filename">An application with which to start a process.</param>
    /// <param name="args">Command-line arguments to pass to the application when the process starts.</param>
    public ProcessExtended(string filename, params string[] args)
    {
        this.startInfo = new ProcessStartInfo
        {
            FileName = filename,
            CreateNoWindow = true,
            UseShellExecute = false,
        };

        foreach (var arg in args)
        {
            this.startInfo.ArgumentList.Add(arg);
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessExtended"/> class,
    /// specifies an application file name with which to start the process, and specifies
    /// a set of command-line arguments to pass to the application.
    /// </summary>
    /// <param name="filename">An application with which to start a process.</param>
    /// <param name="args">Command-line arguments to pass to the application when the process starts.</param>
    public ProcessExtended(string filename, IEnumerable<string> args)
    {
        this.startInfo = new ProcessStartInfo
        {
            FileName = filename,
            CreateNoWindow = true,
            UseShellExecute = false,
        };

        foreach (var arg in args)
        {
            this.startInfo.ArgumentList.Add(arg);
        }
    }

    /// <summary>
    /// Starts (or reuses) the process.
    /// </summary>
    public void Start()
    {
        this.startInfo.RedirectStandardOutput = this.OnData != null;
        this.startInfo.RedirectStandardError = this.OnError != null;

        this.exitCount = 1; // Exit
        this.exitCount += this.startInfo.RedirectStandardOutput ? 1 : 0;
        this.exitCount += this.startInfo.RedirectStandardError ? 1 : 0;

        this.exitSource = new TaskCompletionSource<int>(TaskCreationOptions.RunContinuationsAsynchronously);
        this.process = new Process { StartInfo = this.startInfo, EnableRaisingEvents = true };

        if (this.startInfo.RedirectStandardOutput)
        {
            this.process.OutputDataReceived += (_, e) =>
            {
                if (e.Data != null)
                {
                    this.OnData!(e.Data);
                }
                else
                {
                    this.AccumulateExit();
                }
            };
        }

        if (this.startInfo.RedirectStandardError)
        {
            this.process.ErrorDataReceived += (_, e) =>
            {
                if (e.Data != null)
                {
                    this.OnError!(e.Data);
                }
                else
                {
                    this.AccumulateExit();
                }
            };
        }

        this.process.Exited += (_, _) => this.AccumulateExit();
        _ = this.process.Start();
        if (this.startInfo.RedirectStandardOutput)
        {
            this.process.BeginOutputReadLine();
        }

        if (this.startInfo.RedirectStandardError)
        {
            this.process.BeginErrorReadLine();
        }
    }

    /// <summary>
    /// Starts (or reuses) the process and wait for exit.
    /// </summary>
    /// <returns>A task that will complete when the process exits. The value is the exit code.</returns>
    public Task<int> RunAsync()
    {
        this.Start();
        return this.exitSource!.Task;
    }

    /// <summary>
    /// Wait on a process to complete.
    /// </summary>
    /// <returns>A task that will complete when the process exits. The value is the exit code.</returns>
    public Task<int> WaitForExitAsync()
    {
        if (this.exitSource == null)
        {
            throw new SystemException("Process has not been started.");
        }

        return this.exitSource.Task;
    }

    /// <summary>
    /// Releases all resources of <see cref="ProcessExtended"/>.
    /// </summary>
    public void Dispose()
    {
        this.process?.Dispose();
    }

    /// <summary>
    /// Immediately stops the associated process, and optionally its child/descendent
    /// processes.
    /// </summary>
    /// <param name="entireProcessTree">
    /// true to kill the associated process and its descendants; false to kill only the
    /// associated process.
    /// </param>
    /// <exception cref="System.ComponentModel.Win32Exception">
    /// The associated process could not be terminated. -or- The process is terminating.
    /// </exception>
    /// <exception cref="System.NotSupportedException">
    /// You are attempting to call System.Diagnostics.Process.Kill for a process that
    /// is running on a remote computer. The method is available only for processes running
    /// on the local computer.
    /// </exception>
    /// <exception cref="System.InvalidOperationException">
    /// The process has already exited. -or- There is no process associated with this
    /// System.Diagnostics.Process object. -or- The calling process is a member of the
    /// associated process' descendant tree.
    /// </exception>
    /// <exception cref="System.AggregateException">
    /// Not all processes in the associated process' descendant tree could be terminated.
    /// </exception>
    public void Kill(bool entireProcessTree) => this.NonNullProcess.Kill(entireProcessTree);

    /// <summary>
    /// Immediately stops the associated process.
    /// </summary>
    /// <exception cref="System.ComponentModel.Win32Exception">
    /// The associated process could not be terminated. -or- The process is terminating.
    /// </exception>
    /// <exception cref="System.NotSupportedException">
    /// You are attempting to call System.Diagnostics.Process.Kill for a process that
    /// is running on a remote computer. The method is available only for processes running
    /// on the local computer.
    /// </exception>
    /// <exception cref="System.InvalidOperationException">
    /// The process has already exited. -or- There is no process associated with this
    /// System.Diagnostics.Process object.
    /// </exception>
    public void Kill() => this.NonNullProcess.Kill();

    /// <summary>
    /// Instructs the System.Diagnostics.Process component to wait indefinitely for the
    /// associated process to exit.
    /// </summary>
    /// <exception cref="System.ComponentModel.Win32Exception">
    /// The wait setting could not be accessed.
    /// </exception>
    /// <exception cref="System.SystemException">
    /// No process System.Diagnostics.Process.Id has been set, and a System.Diagnostics.Process.Handle
    /// from which the System.Diagnostics.Process.Id property can be determined does
    /// not exist. -or- There is no process associated with this System.Diagnostics.Process
    /// object. -or- You are attempting to call System.Diagnostics.Process.WaitForExit
    /// for a process that is running on a remote computer. This method is available
    /// only for processes that are running on the local computer.
    /// </exception>
    public void WaitForExit() => this.NonNullProcess.WaitForExit();

    /// <summary>
    /// Instructs the System.Diagnostics.Process component to wait the specified number
    /// of milliseconds for the associated process to exit.
    /// </summary>
    /// <param name="milliseconds">
    /// The amount of time, in milliseconds, to wait for the associated process to exit.
    /// The maximum is the largest possible value of a 32-bit integer, which represents
    /// infinity to the operating system.
    /// </param>
    /// <returns>
    /// true if the associated process has exited; otherwise, false.
    /// </returns>
    /// <exception cref="System.ComponentModel.Win32Exception">
    /// The wait setting could not be accessed.
    /// </exception>
    /// <exception cref="System.SystemException">
    /// No process System.Diagnostics.Process.Id has been set, and a System.Diagnostics.Process.Handle
    /// from which the System.Diagnostics.Process.Id property can be determined does
    /// not exist. -or- There is no process associated with this System.Diagnostics.Process
    /// object. -or- You are attempting to call System.Diagnostics.Process.WaitForExit(System.Int32)
    /// for a process that is running on a remote computer. This method is available
    /// only for processes that are running on the local computer.
    /// </exception>
    /// <exception cref="System.ArgumentOutOfRangeException">
    /// milliseconds is a negative number other than -1, which represents an infinite
    /// time-out.
    /// </exception>
    public bool WaitForExit(int milliseconds) => this.NonNullProcess.WaitForExit(milliseconds);

    private void AccumulateExit()
    {
        Debug.Assert(this.process != null, "Process wasn't started before exiting.");
        Debug.Assert(this.exitSource != null, "Process wasn't started before exiting.");

        var remaining = Interlocked.Decrement(ref this.exitCount);
        if (remaining == 0)
        {
            this.exitSource.SetResult(this.process.ExitCode);
        }
    }
}
