// <copyright file="PEM.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace KestrelToolbox.Utilities;

/// <summary>
/// A utility class to read PEM encoded files.
/// </summary>
[SuppressMessage(
    "Minor Code Smell",
    "S101:Types should be named in PascalCase",
    Justification = "PEM is the name of something."
)]
public static class PEM
{
    /// <summary>
    /// Reads a private key from PEM text.
    /// </summary>
    /// <param name="text">PEM text for the key. This can be either RSA or Pkcs8.</param>
    /// <returns>The key.</returns>
    /// <remarks>
    /// This is useful for files leading with <c>BEGIN RSA PRIVATE KEY</c> and <c>BEGIN PRIVATE KEY</c>.
    /// </remarks>
    public static RSA ReadPrivateKey(string text)
    {
        const string RSAHeader = "-----BEGIN RSA PRIVATE KEY-----";
        const string RSAFooter = "-----END RSA PRIVATE KEY-----";

        const string Pkcs8Header = "-----BEGIN PRIVATE KEY-----";
        const string Pkcs8Footer = "-----END PRIVATE KEY-----";

        var headerIndex = text.IndexOf(RSAHeader, StringComparison.Ordinal);
        if (headerIndex >= 0)
        {
            var footerIndex = text.IndexOf(RSAFooter, StringComparison.Ordinal);

            var start = headerIndex + RSAHeader.Length;
            var count = footerIndex - start;
            var bytes = Convert.FromBase64String(text.Substring(start, count));

            var rsa = RSA.Create();
            rsa.ImportRSAPrivateKey(bytes, out _);
            return rsa;
        }

        headerIndex = text.IndexOf(Pkcs8Header, StringComparison.Ordinal);
        if (headerIndex >= 0)
        {
            var footerIndex = text.IndexOf(Pkcs8Footer, StringComparison.Ordinal);

            var start = headerIndex + Pkcs8Header.Length;
            var count = footerIndex - start;
            var bytes = Convert.FromBase64String(text.Substring(start, count));

            var rsa = RSA.Create();
            rsa.ImportPkcs8PrivateKey(bytes, out _);
            return rsa;
        }

        throw new InvalidDataException($"Unexpected pem data. Expected something like {RSAHeader}.");
    }

    /// <summary>
    /// Reads a private key from PEM text.
    /// </summary>
    /// <param name="text">PEM text for the key. This can be either RSA or Pkcs8.</param>
    /// <param name="password">The password to decrypt the key. This value is unused if <paramref name="text"/> is unencrypted.</param>
    /// <returns>The key.</returns>
    /// <remarks>
    /// This method is good for files leading with <c>BEGIN ENCRYPTED PRIVATE KEY</c>.
    /// </remarks>
    public static RSA ReadPrivateKey(string text, string password)
    {
        const string RSAHeader = "-----BEGIN ENCRYPTED PRIVATE KEY-----";
        const string RSAFooter = "-----END ENCRYPTED PRIVATE KEY-----";

        var headerIndex = text.IndexOf(RSAHeader, StringComparison.Ordinal);
        if (headerIndex >= 0)
        {
            var footerIndex = text.IndexOf(RSAFooter, StringComparison.Ordinal);

            var start = headerIndex + RSAHeader.Length;
            var count = footerIndex - start;
            var bytes = Convert.FromBase64String(text.Substring(start, count));

            var rsa = RSA.Create();
            rsa.ImportEncryptedPkcs8PrivateKey(password, bytes, out _);
            return rsa;
        }

        return ReadPrivateKey(text);
    }

    /// <summary>
    /// Reads a certificate from PEM text.
    /// </summary>
    /// <param name="text">PEM text for the certificate.</param>
    /// <returns>The certificate.</returns>
    /// <remarks>
    /// This method is good for reading general certificate files.
    /// </remarks>
    public static X509Certificate2 ReadCert(string text)
    {
        var bytes = Encoding.ASCII.GetBytes(text);

#if NET9_0_OR_GREATER
        return X509CertificateLoader.LoadCertificate(bytes);
#else
        return new X509Certificate2(bytes);
#endif // NET9_0_OR_GREATER
    }

    /// <summary>
    /// Reads multiple certificates from PEM text.
    /// </summary>
    /// <param name="text">PEM text for the certificate.</param>
    /// <returns>All the certificates contained in the text.</returns>
    /// <remarks>
    /// This method is good for files leading with <c>BEGIN CERTIFICATE</c>.
    /// </remarks>
    public static IEnumerable<X509Certificate2> ReadCerts(string text)
    {
        const string CertHeader = "-----BEGIN CERTIFICATE-----";
        const string CertFooter = "-----END CERTIFICATE-----";
        var index = 0;
        while (index < text.Length)
        {
            var start = text.IndexOf(CertHeader, index, StringComparison.Ordinal);
            if (start < 0)
            {
                if (index == 0)
                {
                    throw new InvalidDataException(
                        $"Invalid PEM, doesn't contain any valid certs. Expected something like {CertHeader}."
                    );
                }

                break;
            }

            var end = text.IndexOf(CertFooter, index, StringComparison.Ordinal) + CertFooter.Length;
            var count = end - start;
            var cert = text.Substring(start, count);

            yield return ReadCert(cert);

            index = end + 1;
        }
    }

    /// <summary>
    /// Reads a certificate and key pair.
    /// </summary>
    /// <param name="certText">PEM text for the certificate.</param>
    /// <param name="keyText">PEM text for the key.</param>
    /// <returns>A certificate that pairs the two.</returns>
    /// <remarks>
    /// This method is good for reading general certificate files. It will use <c>string.Empty</c> as the key.
    ///
    /// https://github.com/dotnet/runtime/issues/23749
    /// Certificate managers require there to be a password of some kind no matter what. This
    /// automatically passes <c>string.Empty</c> as the password.
    /// </remarks>
    public static X509Certificate2 ReadCertWithKey(string certText, string keyText) =>
        ReadCertWithKey(certText, keyText, string.Empty);

    /// <summary>
    /// Reads a certificate and key pair.
    /// </summary>
    /// <param name="certText">PEM text for the certificate.</param>
    /// <param name="keyText">PEM text for the key.</param>
    /// <param name="password">Password text for the certificate.</param>
    /// <returns>A certificate that pairs the two.</returns>
    /// <remarks>
    /// This method is good for reading general certificate files.
    /// </remarks>
    public static X509Certificate2 ReadCertWithKey(string certText, string keyText, string password)
    {
        using var baseCert = ReadCert(certText);
        var key = ReadPrivateKey(keyText, password);
        using var certWithKey = baseCert.CopyWithPrivateKey(key);

#if NET9_0_OR_GREATER
        var resultCert = X509CertificateLoader.LoadPkcs12(certWithKey.Export(X509ContentType.Pfx, password), password);
#else
        var resultCert = new X509Certificate2(certWithKey.Export(X509ContentType.Pfx, password), password);
#endif // NET9_0_OR_GREATER

        return resultCert;
    }

    /// <summary>
    /// Creates a self-signed X509Certificate2 for use in https.
    /// Uses 2048 bits as the key size.
    /// </summary>
    /// <param name="certificateName">Common name of the certificate.</param>
    /// <param name="domain">Domain name to use for the certificate.</param>
    /// <param name="expiration">Expiration of the certificate.</param>
    /// <returns>X509Certificate2 for use in https.</returns>
    public static X509Certificate2 CreateSelfSignedServerCertificate(
        string certificateName,
        string domain,
        TimeSpan expiration
    ) => CreateSelfSignedServerCertificate(certificateName, domain, expiration, 2048);

    /// <summary>
    /// Creates a self-signed X509Certificate2 for use in https.
    /// </summary>
    /// <param name="certificateName">Common name of the certificate.</param>
    /// <param name="domain">Domain name to use for the certificate.</param>
    /// <param name="expiration">Expiration of the certificate.</param>
    /// <param name="keySizeInBits">The key size, in bits.</param>
    /// <returns>X509Certificate2 for use in https.</returns>
    public static X509Certificate2 CreateSelfSignedServerCertificate(
        string certificateName,
        string domain,
        TimeSpan expiration,
        int keySizeInBits
    )
    {
        using var rsa = RSA.Create(keySizeInBits);

        var sanBuilder = new SubjectAlternativeNameBuilder();
        sanBuilder.AddDnsName(domain);

        var distinguishedName = new X500DistinguishedName($"CN={certificateName}");

        var request = new CertificateRequest(
            distinguishedName,
            rsa,
            HashAlgorithmName.SHA256,
            RSASignaturePadding.Pkcs1
        );
        request.CertificateExtensions.Add(
            new X509KeyUsageExtension(
                X509KeyUsageFlags.DataEncipherment
                    | X509KeyUsageFlags.KeyEncipherment
                    | X509KeyUsageFlags.DigitalSignature,
                false
            )
        );
        request.CertificateExtensions.Add(
            new X509EnhancedKeyUsageExtension(new OidCollection { new("1.3.6.1.5.5.7.3.1") }, false)
        );
        request.CertificateExtensions.Add(sanBuilder.Build());

        var notBefore = DateTimeOffset.UtcNow.AddDays(-1);
        var notAfter = DateTimeOffset.UtcNow.Add(expiration);
        var certificate = request.CreateSelfSigned(notBefore, notAfter);

        // Cannot use a null password, so set to empty.
        var password = string.Empty;
        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
        {
            // OSX does not support empty password, so set to "password".
            password = "password";
        }

        var exported = certificate.Export(X509ContentType.Pfx, password);
#if NET9_0_OR_GREATER
        return X509CertificateLoader.LoadPkcs12(exported, password, X509KeyStorageFlags.MachineKeySet);
#else
        return new X509Certificate2(exported, password, X509KeyStorageFlags.MachineKeySet);
#endif // NET9_0_OR_GREATER
    }
}
