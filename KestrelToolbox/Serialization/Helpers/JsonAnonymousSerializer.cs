// <copyright file="JsonAnonymousSerializer.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using KestrelToolbox.Internal;
using KestrelToolbox.Internal.ILGenExtensions;
using KestrelToolbox.Serialization.DataAnnotations;
using KestrelToolbox.Serialization.DataAnnotations.Serialization;
using KestrelToolbox.Types;

#pragma warning disable S125 // Remove commented out code
#pragma warning disable S3011 // Accessing private members

namespace KestrelToolbox.Serialization.Helpers;

/// <summary>
/// Class for parsing json and validating that its values are valid.
/// </summary>
[SuppressMessage(
    "StyleCop.CSharp.OrderingRules",
    "SA1204:Static elements should appear before instance elements",
    Justification = "This file is complicated enough. Static vs. non static are pretty interchangeable."
)]
public sealed class JsonAnonymousSerializer
{
    /// <summary>
    /// Delegate for compiled serializers.
    /// </summary>
    /// <param name="writer">The writer to write to.</param>
    /// <param name="value">Value to serialize.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    public delegate void SerializeDelegate<in T>(Utf8JsonWriter writer, T value);

    private delegate void CompareToDefaultCallback(LocalBuilder value, object? initialDefaultValue);

    private delegate void CompareToEmptyCallback(LocalBuilder value);

    private delegate void WriteValueCallback(LocalBuilder value);

    private readonly struct CompiledSerializer
    {
        public Delegate Delegate { get; }

        public MethodInfo Method { get; }

        public CompiledSerializer(Delegate @delegate, MethodInfo method)
        {
            this.Method = method;
            this.Delegate = @delegate;
        }

        public SerializeDelegate<T> GetDelegate<T>()
        {
            return (SerializeDelegate<T>)this.Delegate;
        }
    }

#if NET9_0_OR_GREATER
    private readonly Lock compiledSerializersLock = new();
#else
    private readonly object compiledSerializersLock = new();
#endif // NET9_0_OR_GREATER
    private Dictionary<Type, CompiledSerializer> compiledSerializers = new();

    /// <summary>
    /// Adds a custom serialization callback for <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type to add custom serialization for.</typeparam>
    /// <param name="callback">Callback to call when this type is encountered.</param>
    /// <exception cref="ArgumentException">An serializer with the same type already exists.</exception>
    public void AddSerializer<T>(SerializeDelegate<T> callback)
    {
        var compiledSerializer = new CompiledSerializer(callback, callback.Method);
        if (!this.TryAddCompiledSerializer(typeof(T), compiledSerializer))
        {
            throw new ArgumentException($"{typeof(T)} has already been added.");
        }
    }

    /// <summary>
    /// Asynchronously serialize an object of type <typeparamref name="T"/> to a stream.
    /// </summary>
    /// <param name="stream">The stream to write to.</param>
    /// <param name="value">The object to serialize.</param>
    /// <param name="options">Options to initialize <see cref="Utf8JsonWriter"/> with.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    /// <returns>void.</returns>
    public async Task SerializeAsync<T>(
        Stream stream,
        T value,
        JsonWriterOptions options = default,
        CancellationToken cancellationToken = default
    )
    {
        await using var writer = new Utf8JsonWriter(stream, options);
        this.Serialize(writer, value);
        await writer.FlushAsync(cancellationToken);
    }

    /// <summary>
    /// Asynchronously serialize an object of type <typeparamref name="T"/> to a stream.
    /// </summary>
    /// <param name="bufferWriter">The <see cref="IBufferWriter{T}"/> to write to.</param>
    /// <param name="value">The object to serialize.</param>
    /// <param name="options">Options to initialize <see cref="Utf8JsonWriter"/> with.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    /// <returns>void.</returns>
    public async Task SerializeAsync<T>(
        IBufferWriter<byte> bufferWriter,
        T value,
        JsonWriterOptions options = default,
        CancellationToken cancellationToken = default
    )
    {
        await using var writer = new Utf8JsonWriter(bufferWriter, options);
        this.Serialize(writer, value);
        await writer.FlushAsync(cancellationToken);
    }

    /// <summary>
    /// Serialize an object of type <typeparamref name="T"/> to a string.
    /// </summary>
    /// <param name="value">The object to serialize.</param>
    /// <param name="options">Options to initialize <see cref="Utf8JsonWriter"/> with.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    /// <returns>Json encoded string value.</returns>
    public string Serialize<T>(T value, JsonWriterOptions options = default)
    {
        using var memoryStream = new MemoryStream();
        this.Serialize(memoryStream, value, options);
        return Encoding.UTF8.GetString(memoryStream.ToArray());
    }

    /// <summary>
    /// Serialize an object of type <typeparamref name="T"/> to a stream.
    /// </summary>
    /// <param name="stream">The stream to write to.</param>
    /// <param name="value">The object to serialize.</param>
    /// <param name="options">Options to initialize <see cref="Utf8JsonWriter"/> with.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    public void Serialize<T>(Stream stream, T value, JsonWriterOptions options = default)
    {
        using var writer = new Utf8JsonWriter(stream, options);
        this.Serialize(writer, value);
        writer.Flush();
    }

    /// <summary>
    /// Serialize an object of type <typeparamref name="T"/> to a stream.
    /// </summary>
    /// <param name="bufferWriter">The <see cref="IBufferWriter{T}"/> to write to.</param>
    /// <param name="value">The object to serialize.</param>
    /// <param name="options">Options to initialize <see cref="Utf8JsonWriter"/> with.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    public void Serialize<T>(IBufferWriter<byte> bufferWriter, T value, JsonWriterOptions options = default)
    {
        using var writer = new Utf8JsonWriter(bufferWriter, options);
        this.Serialize(writer, value);
        writer.Flush();
    }

    /// <summary>
    /// Serialize an object of type <typeparamref name="T"/> to a <see cref="Utf8JsonWriter"/>.
    /// </summary>
    /// <param name="writer">The <see cref="Utf8JsonWriter"/> to write to.</param>
    /// <param name="value">The object to serialize.</param>
    /// <typeparam name="T">The type to serialize.</typeparam>
    public void Serialize<T>(Utf8JsonWriter writer, T value)
    {
        var type = typeof(T);

        if (!this.compiledSerializers.TryGetValue(typeof(T), out var compiledSerializer))
        {
            compiledSerializer = this.CompileSerializer(type);
        }

        compiledSerializer.GetDelegate<T>()(writer, value);
    }

    private static bool TryGetJsonSerializableAttribute(
        Type type,
        [NotNullWhen(true)] out JsonSerializableAttribute? attribute
    )
    {
        var jsonSerializableAttributes = type.GetCustomAttributes<JsonSerializableAttribute>();
        foreach (var jsonSerializableAttribute in jsonSerializableAttributes)
        {
            if (jsonSerializableAttribute.Types.Length == 0 || jsonSerializableAttribute.Types.Contains(type))
            {
                attribute = jsonSerializableAttribute;
                return true;
            }
        }

        var typedefSerializableAttributes = type.GetCustomAttributes<TypedefAttribute>();
        foreach (var typedefAttribute in typedefSerializableAttributes)
        {
            if (typedefAttribute.Features.HasFlag(TypedefFeatures.JsonSerializable))
            {
                attribute = new JsonSerializableAttribute();
                return true;
            }
        }

        attribute = default;
        return false;
    }

    private CompiledSerializer CompileSerializer(Type type, Dictionary<Type, MethodInfo>? inFlightMethods = null)
    {
        if (this.TryGetCompiledSerializer(type, out var compiledSerializer))
        {
            return compiledSerializer;
        }

        inFlightMethods ??= new Dictionary<Type, MethodInfo>();

        var dynamicMethod = new DynamicMethod(string.Empty, typeof(void), new[] { typeof(Utf8JsonWriter), type });
        inFlightMethods.Add(type, dynamicMethod);

        var serializeFieldsByDefault = IsAnonymousType(type);
        var ilGen = dynamicMethod.GetILGenerator();

        var propertyNames = new HashSet<string>();

        if (IsHandledAsIndividualElement(type))
        {
            if (
                TryGetJsonSerializableAttribute(type, out var jsonSerializableAttribute)
                && jsonSerializableAttribute.SourceGenerationMode.HasFlag(SourceGenerationMode.Serialize)
            )
            {
                var method =
                    type.GetMethod(
                        jsonSerializableAttribute.SerializingMethodName.Split('.')[^1],
                        BindingFlags.Public | BindingFlags.Static,
                        new[] { typeof(Utf8JsonWriter), type }
                    )
                    ?? throw new DataException(
                        $"Could not find method {jsonSerializableAttribute.SerializingMethodName} on {type.FullName}."
                    );

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(OpCodes.Call, method);
                ilGen.Emit(OpCodes.Ret);
            }
            else
            {
                var value = ilGen.DeclareLocal(type);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.EmitStloc(value);
                this.WriteIndividualElement(ilGen, null, value, type, new NoAttributesProvider(), inFlightMethods);
                ilGen.Emit(OpCodes.Ret);
            }
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteStartObject", Type.EmptyTypes)!);

            foreach (var property in type.GetProperties())
            {
                string name;
                var nameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(property);
                if (nameAttributes.Length != 0)
                {
                    if (nameAttributes.Length != 1 || nameAttributes[0].Names.Count != 1)
                    {
                        throw new Exception(
                            $"Name attribute on {type.FullName}.{type.Name} cannot have multiple names [Name({string.Join(", ", nameAttributes.SelectMany(n => n.Names))})]."
                        );
                    }

                    name = nameAttributes[0].Names[0];
                }
                else if (serializeFieldsByDefault)
                {
                    name = property.Name;
                }
                else
                {
                    continue;
                }

                if (!propertyNames.Add(name))
                {
                    throw new DuplicateNameException($"{name} is on multiple properties.");
                }

                if (property.GetMethod == null)
                {
                    throw new NullReferenceException(
                        $"{property.DeclaringType!.FullName}.{property.Name} requires a valid getter."
                    );
                }

                var value = ilGen.DeclareLocal(property.PropertyType);
                if (type.IsValueType)
                {
                    ilGen.Emit(OpCodes.Ldarga_S, 1);
                    ilGen.Emit(OpCodes.Call, property.GetMethod!);
                }
                else
                {
                    ilGen.Emit(OpCodes.Ldarg_1);
                    ilGen.Emit(OpCodes.Callvirt, property.GetMethod!);
                }

                ilGen.EmitStloc(value);
                this.WriteIndividualElement(
                    ilGen,
                    name,
                    value,
                    property.PropertyType,
                    new PropertyProvider(property),
                    inFlightMethods
                );
            }

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteEndObject", Type.EmptyTypes)!);
            ilGen.Emit(OpCodes.Ret);
        }

        var methodDelegate = dynamicMethod.CreateDelegate(typeof(SerializeDelegate<>).MakeGenericType(type));
        compiledSerializer = new CompiledSerializer(methodDelegate, dynamicMethod);

        _ = this.TryAddCompiledSerializer(type, compiledSerializer);

        return compiledSerializer;
    }

    private bool TryGetCompiledSerializer(Type type, out CompiledSerializer compiledSerializer)
    {
        return this.compiledSerializers.TryGetValue(type, out compiledSerializer);
    }

    private bool TryAddCompiledSerializer(Type type, CompiledSerializer compiledSerializer)
    {
        // Lock so we can modify CompiledSerializers
        lock (this.compiledSerializersLock)
        {
            // Clone CompiledSerializers and add to that. We cannot modify active CompiledSerializers. GC will collect old one when no more threads
            // are using it.
            var newCompiledSerializers = this.compiledSerializers.ToDictionary(p => p.Key, p => p.Value);

            // Don't care if we add correctly or not because it means another thread is adding simultaneously. Both should be identical.
            var added = newCompiledSerializers.TryAdd(type, compiledSerializer);

            this.compiledSerializers = newCompiledSerializers;
            return added;
        }
    }

    private void WriteIndividualElement(
        ILGenerator ilGen,
        string? name,
        LocalBuilder value,
        Type propertyType,
        IPropertyProvider property,
        Dictionary<Type, MethodInfo> inFlightMethods
    )
    {
        GetNullableType(propertyType, out var localPropertyType, out _);
        switch (localPropertyType.FullName)
        {
            case "System.String":
            {
                var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
                var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;
                var defaultValueAttribute = ReflectionHelpers.GetCustomAttribute<DefaultValueAttribute>(property);

                var nextPropertyLabel = ilGen.DefineLabel();

                var trimAttribute = ReflectionHelpers.GetCustomAttribute<TrimValueAttribute>(property);
                if (trimAttribute != null && trimAttribute.OnSerialize)
                {
                    var isNullLabel = ilGen.DefineLabel();
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Brfalse, isNullLabel);

                    // attribute.Trim()
                    ilGen.EmitLdloc(value);
                    if (trimAttribute.TrimChars == null)
                    {
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("Trim", Type.EmptyTypes)!);
                    }
                    else
                    {
                        ilGen.Emit(OpCodes.Ldstr, new string(trimAttribute.TrimChars));
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("ToCharArray", Type.EmptyTypes)!);
                        ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("Trim", new[] { typeof(char[]) })!);
                    }

                    ilGen.EmitStloc(value);

                    ilGen.MarkLabel(isNullLabel);
                }

                var nullOnEmptyAttribute = ReflectionHelpers.GetCustomAttribute<NullOnEmptyAttribute>(property);
                if (nullOnEmptyAttribute is { OnSerialize: true })
                {
                    var isNonNullLabel = ilGen.DefineLabel();

                    // if (string.IsNullOrWhitespace(attribute))
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Call, typeof(string).GetMethod("IsNullOrEmpty", new[] { typeof(string) })!);
                    ilGen.Emit(OpCodes.Brfalse, isNonNullLabel);

                    ilGen.EmitLdc(null);
                    ilGen.EmitStloc(value);

                    ilGen.MarkLabel(isNonNullLabel);
                }

                if (skipWritingCondition.HasFlag(Condition.WhenDefault))
                {
                    ilGen.EmitLdloc(value);
                    if (defaultValueAttribute?.Value == null)
                    {
                        ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
                    }
                    else
                    {
                        var defaultValue = (string)
                            Convert.ChangeType(defaultValueAttribute.Value, propertyType, CultureInfo.InvariantCulture);
                        ilGen.EmitLdc(defaultValue);
                        ilGen.EmitLdc((int)StringComparison.Ordinal);
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(string).GetMethod(
                                "Equals",
                                BindingFlags.Public | BindingFlags.Static,
                                new[] { typeof(string), typeof(string), typeof(StringComparison) }
                            )!
                        );

                        ilGen.Emit(OpCodes.Brtrue, nextPropertyLabel);
                    }
                }

                if (skipWritingCondition.HasFlag(Condition.WhenNull))
                {
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
                }
                else
                {
                    var notNullLabel = ilGen.DefineLabel();
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Brtrue, notNullLabel);

                    WriteJsonNull(ilGen, name);
                    ilGen.Emit(OpCodes.Br, nextPropertyLabel);

                    ilGen.MarkLabel(notNullLabel);
                }

                if (skipWritingCondition.HasFlag(Condition.WhenEmpty))
                {
                    ilGen.EmitLdloc(value);
                    ilGen.Emit(OpCodes.Callvirt, typeof(string).GetMethod("get_Length")!);

                    ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
                }

                WriteJsonString(ilGen, name, value);

                ilGen.MarkLabel(nextPropertyLabel);
                break;
            }

            case "System.SByte":
            case "System.Int16":
            case "System.Int32":
            case "System.Int64":
            case "System.Byte":
            case "System.UInt16":
            case "System.UInt32":
            case "System.UInt64":
            case "System.Single":
            case "System.Double":
            case "System.Decimal":
            {
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        ilGen.EmitLdloc(compareValue);
                        dynamic defaultValue = Convert.ChangeType(
                            initialDefaultValue ?? 0M,
                            localPropertyType,
                            CultureInfo.InvariantCulture
                        );
                        ILGenExtensions.EmitLdc(ilGen, defaultValue);
                        if (localPropertyType == typeof(decimal))
                        {
                            ilGen.Emit(
                                OpCodes.Call,
                                typeof(decimal).GetMethod(
                                    "op_Equality",
                                    BindingFlags.Public | BindingFlags.Static,
                                    new[] { typeof(decimal), typeof(decimal) }
                                )!
                            );
                        }
                        else
                        {
                            ilGen.Emit(OpCodes.Ceq);
                        }
                    },
                    writtenValue => WriteNumber(ilGen, localPropertyType, name, writtenValue)
                );

                break;
            }

            case "System.Boolean":
            {
                var boolSerializableSettingsAttribute =
                    ReflectionHelpers.GetCustomAttribute<BoolSerializableSettingsAttribute>(property)
                    ?? new BoolSerializableSettingsAttribute();
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        ilGen.EmitLdloc(compareValue);
                        dynamic defaultValue = Convert.ChangeType(
                            initialDefaultValue ?? false,
                            localPropertyType,
                            CultureInfo.InvariantCulture
                        );
                        ILGenExtensions.EmitLdc(ilGen, defaultValue);
                        ilGen.Emit(OpCodes.Ceq);
                    },
                    (writtenValue) =>
                    {
                        if (
                            boolSerializableSettingsAttribute.SerializationType
                            == BoolSerializableSettingsSerializationType.Numbers
                        )
                        {
                            var numberValue = ilGen.DeclareLocal(typeof(int));
                            var oneLabel = ilGen.DefineLabel();
                            var writeLabel = ilGen.DefineLabel();

                            ilGen.EmitLdloc(writtenValue);
                            ilGen.Emit(OpCodes.Brtrue, oneLabel);
                            ilGen.EmitLdc(0);
                            ilGen.Emit(OpCodes.Br, writeLabel);
                            ilGen.MarkLabel(oneLabel);
                            ilGen.EmitLdc(1);
                            ilGen.MarkLabel(writeLabel);
                            ilGen.EmitStloc(numberValue);

                            WriteNumber(ilGen, typeof(int), name, numberValue);
                        }
                        else
                        {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            if (name != null)
                            {
                                ilGen.EmitLdc(name);
                                ilGen.EmitLdloc(writtenValue);
                                ilGen.Emit(
                                    OpCodes.Callvirt,
                                    typeof(Utf8JsonWriter).GetMethod(
                                        "WriteBoolean",
                                        new[] { typeof(string), localPropertyType }
                                    )!
                                );
                            }
                            else
                            {
                                ilGen.EmitLdloc(writtenValue);
                                ilGen.Emit(
                                    OpCodes.Callvirt,
                                    typeof(Utf8JsonWriter).GetMethod("WriteBooleanValue", new[] { localPropertyType })!
                                );
                            }
                        }
                    }
                );

                break;
            }

            case "System.DateTime":
            case "System.DateTimeOffset":
            {
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        var defaultValueLocal = ilGen.DeclareLocal(localPropertyType);

                        if (initialDefaultValue != null)
                        {
                            throw new NotSupportedException(
                                $"{localPropertyType.Name} does not support default values and SkipSerialization.WhenDefault at runtime."
                            );
                        }

                        ilGen.EmitLdloca(defaultValueLocal);
                        ilGen.Emit(OpCodes.Initobj, localPropertyType);

                        ilGen.EmitLdloc(compareValue);
                        ilGen.EmitLdloc(defaultValueLocal);
                        ilGen.Emit(
                            OpCodes.Call,
                            localPropertyType.GetMethod("op_Equality", new[] { localPropertyType, localPropertyType })!
                        );
                    },
                    (writtenValue) =>
                    {
                        var valueLocal = ilGen.DeclareLocal(localPropertyType);
                        var stringLocal = ilGen.DeclareLocal(typeof(string));
                        ilGen.EmitLdloca(writtenValue);
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("ToUniversalTime", Type.EmptyTypes)!);
                        ilGen.EmitStloc(valueLocal);

                        ilGen.EmitLdloca(valueLocal);
                        ilGen.EmitLdc("yyyy-MM-dd'T'HH:mm:ss.fffZ");
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(CultureInfo).GetMethod(
                                "get_InvariantCulture",
                                BindingFlags.Public | BindingFlags.Static,
                                Type.EmptyTypes
                            )!
                        );
                        ilGen.Emit(
                            OpCodes.Call,
                            localPropertyType.GetMethod("ToString", new[] { typeof(string), typeof(IFormatProvider) })!
                        );
                        ilGen.EmitStloc(stringLocal);

                        WriteJsonString(ilGen, name, stringLocal);
                    }
                );
                break;
            }

            case "System.DateOnly":
            {
                var dateOnlySettingsAttribute = property.GetCustomAttribute<DateOnlyParseSettingsAttribute>();
                var format = dateOnlySettingsAttribute?.Format;

                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    null,
                    (writtenValue) =>
                    {
                        var stringLocal = ilGen.DeclareLocal(typeof(string));
                        ilGen.EmitLdloca(writtenValue);
                        if (format != null)
                        {
                            ilGen.EmitLdc(format);
                        }

                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(CultureInfo).GetMethod(
                                "get_InvariantCulture",
                                BindingFlags.Public | BindingFlags.Static,
                                Type.EmptyTypes
                            )!
                        );
                        if (format != null)
                        {
                            ilGen.Emit(
                                OpCodes.Call,
                                localPropertyType.GetMethod(
                                    "ToString",
                                    new[] { typeof(string), typeof(IFormatProvider) }
                                )!
                            );
                        }
                        else
                        {
                            ilGen.Emit(
                                OpCodes.Call,
                                localPropertyType.GetMethod("ToString", new[] { typeof(IFormatProvider) })!
                            );
                        }

                        ilGen.EmitStloc(stringLocal);

                        WriteJsonString(ilGen, name, stringLocal);
                    }
                );
                break;
            }

            case "System.TimeOnly":
            {
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        ilGen.EmitLdloca(compareValue);

                        var defaultValue = (TimeOnly)
                            Convert.ChangeType(
                                initialDefaultValue ?? TimeOnly.MinValue,
                                localPropertyType,
                                CultureInfo.InvariantCulture
                            );
                        ilGen.EmitLdc(defaultValue.ToString(CultureInfo.InvariantCulture));
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(TimeOnly).GetMethod(
                                "Parse",
                                BindingFlags.Public | BindingFlags.Static,
                                new[] { typeof(string) }
                            )!
                        );
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("Equals", new[] { localPropertyType })!);
                    },
                    (writtenValue) =>
                    {
                        var stringLocal = ilGen.DeclareLocal(typeof(string));
                        ilGen.EmitLdloca(writtenValue);
                        ilGen.EmitLdc("HH:mm:ss.FFFFFF");
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("ToString", new[] { typeof(string) })!);
                        ilGen.EmitStloc(stringLocal);

                        WriteJsonString(ilGen, name, stringLocal);
                    }
                );
                break;
            }

            case "System.TimeSpan":
            {
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        ilGen.EmitLdloca(compareValue);

                        var defaultValue = (TimeSpan)
                            Convert.ChangeType(
                                initialDefaultValue ?? TimeSpan.Zero,
                                localPropertyType,
                                CultureInfo.InvariantCulture
                            );
                        ilGen.EmitLdc(defaultValue.ToString());
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(TimeSpan).GetMethod(
                                "Parse",
                                BindingFlags.Public | BindingFlags.Static,
                                new[] { typeof(string) }
                            )!
                        );
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("Equals", new[] { localPropertyType })!);
                    },
                    (writtenValue) =>
                    {
                        var stringLocal = ilGen.DeclareLocal(typeof(string));
                        ilGen.EmitLdloca(writtenValue);
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("ToString", Type.EmptyTypes)!);
                        ilGen.EmitStloc(stringLocal);

                        WriteJsonString(ilGen, name, stringLocal);
                    }
                );
                break;
            }

            case "System.Byte[]":
            {
                WriteSimpleReference(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    compareToDefaultCallback: null,
                    compareToEmptyCallback: (writtenValue) =>
                    {
                        ilGen.EmitLdloc(writtenValue);
                        ilGen.Emit(OpCodes.Callvirt, typeof(byte[]).GetMethod("get_Length", Type.EmptyTypes)!);
                        ilGen.EmitLdc(0);
                        ilGen.Emit(OpCodes.Ceq);
                    },
                    (writtenValue) =>
                    {
                        var spanLocal = ilGen.DeclareLocal(typeof(ReadOnlySpan<byte>));
                        ilGen.EmitLdloc(writtenValue);
                        var asSpanMethod = typeof(MemoryExtensions)
                            .GetMethods(BindingFlags.Public | BindingFlags.Static)
                            .Where(m =>
                                m.Name == "AsSpan" && m.GetParameters().Length == 1 && m.ContainsGenericParameters
                            )
                            .Select(m => m.MakeGenericMethod(typeof(byte)))
                            .First(m => m.GetParameters()[0].ParameterType == typeof(byte[]));

                        ilGen.Emit(OpCodes.Call, asSpanMethod);
                        ilGen.EmitStloc(spanLocal);

                        ilGen.Emit(OpCodes.Ldarg_0);
                        if (name != null)
                        {
                            ilGen.EmitLdc(name);
                        }

                        ilGen.EmitLdloc(spanLocal);
                        if (name != null)
                        {
                            ilGen.Emit(
                                OpCodes.Callvirt,
                                typeof(Utf8JsonWriter).GetMethod(
                                    "WriteBase64String",
                                    new[] { typeof(string), typeof(ReadOnlySpan<byte>) }
                                )!
                            );
                        }
                        else
                        {
                            ilGen.Emit(
                                OpCodes.Callvirt,
                                typeof(Utf8JsonWriter).GetMethod(
                                    "WriteBase64StringValue",
                                    new[] { typeof(ReadOnlySpan<byte>) }
                                )!
                            );
                        }
                    }
                );
                break;
            }

            case "System.Guid":
            {
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        ilGen.EmitLdloca(compareValue);

                        var defaultValue = (Guid)
                            Convert.ChangeType(
                                initialDefaultValue ?? Guid.Empty,
                                localPropertyType,
                                CultureInfo.InvariantCulture
                            );
                        ilGen.EmitLdc(defaultValue.ToString());
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(Guid).GetMethod(
                                "Parse",
                                BindingFlags.Public | BindingFlags.Static,
                                new[] { typeof(string) }
                            )!
                        );
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("Equals", new[] { localPropertyType })!);
                    },
                    (writtenValue) =>
                    {
                        var stringLocal = ilGen.DeclareLocal(typeof(string));
                        ilGen.EmitLdloca(writtenValue);
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("ToString", Type.EmptyTypes)!);
                        ilGen.EmitStloc(stringLocal);

                        WriteJsonString(ilGen, name, stringLocal);
                    }
                );
                break;
            }

            case "KestrelToolbox.Types.UUIDv1":
            {
                WriteSimpleValue(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    (compareValue, initialDefaultValue) =>
                    {
                        ilGen.EmitLdloca(compareValue);

                        var defaultValue = (UUIDv1)
                            Convert.ChangeType(
                                initialDefaultValue ?? UUIDv1.Empty,
                                localPropertyType,
                                CultureInfo.InvariantCulture
                            );
                        ilGen.EmitLdc(defaultValue.ToString());
                        ilGen.Emit(
                            OpCodes.Call,
                            typeof(Guid).GetMethod(
                                "Parse",
                                BindingFlags.Public | BindingFlags.Static,
                                new[] { typeof(string) }
                            )!
                        );
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("Equals", new[] { localPropertyType })!);
                    },
                    (writtenValue) =>
                    {
                        var stringLocal = ilGen.DeclareLocal(typeof(string));
                        ilGen.EmitLdloca(writtenValue);
                        ilGen.Emit(OpCodes.Call, localPropertyType.GetMethod("ToString", Type.EmptyTypes)!);
                        ilGen.EmitStloc(stringLocal);

                        WriteJsonString(ilGen, name, stringLocal);
                    }
                );
                break;
            }

            case "System.Text.Json.Nodes.JsonNode":
            case "System.Text.Json.Nodes.JsonObject":
            case "System.Text.Json.Nodes.JsonArray":
            {
                var getCountMethod = localPropertyType.FullName switch
                {
                    "System.Text.Json.Nodes.JsonObject" => localPropertyType.GetMethod("get_Count", Type.EmptyTypes),
                    "System.Text.Json.Nodes.JsonArray" => localPropertyType.GetMethod("get_Count", Type.EmptyTypes),
                    _ => null,
                };

                WriteSimpleReference(
                    ilGen,
                    propertyType,
                    property,
                    name,
                    value,
                    compareToDefaultCallback: null,
                    compareToEmptyCallback: getCountMethod == null
                        ? null
                        : (writtenValue) =>
                        {
                            ilGen.EmitLdloc(writtenValue);
                            ilGen.Emit(OpCodes.Callvirt, getCountMethod);
                            ilGen.EmitLdc(0);
                            ilGen.Emit(OpCodes.Ceq);
                        },
                    (writtenValue) =>
                    {
                        var method = typeof(JsonNode).GetMethod(
                            "WriteTo",
                            new[] { typeof(Utf8JsonWriter), typeof(JsonSerializerOptions) }
                        )!;

                        if (name != null)
                        {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.EmitLdc(name);
                            ilGen.Emit(
                                OpCodes.Call,
                                typeof(Utf8JsonWriter).GetMethod("WritePropertyName", new[] { typeof(string) })!
                            );
                        }

                        ilGen.EmitLdloc(writtenValue);
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldnull);
                        ilGen.Emit(OpCodes.Callvirt, method);
                    }
                );
                break;
            }

            default:
            {
                if (TryGetCustomSerializeMethod(localPropertyType, out var customSerializeMethod))
                {
                    WriteSerializeMethod(ilGen, propertyType, property, name, value, customSerializeMethod);
                }
                else if (localPropertyType.IsEnum)
                {
                    WriteEnumValue(ilGen, propertyType, property, name, value);
                }
                else if (localPropertyType.IsArray || IsListType(localPropertyType))
                {
                    this.WriteArray(ilGen, propertyType, property, name, value, inFlightMethods);
                }
                else if (IsGenericStringDictionary(localPropertyType))
                {
                    this.WriteDictionary(ilGen, propertyType, property, name, value, inFlightMethods);
                }
                else if (IsGenericIEnumerable(localPropertyType))
                {
                    this.WriteIEnumerable(ilGen, propertyType, property, name, value, inFlightMethods);
                }
                else
                {
                    this.WriteObject(ilGen, propertyType, property, name, value, inFlightMethods);
                }

                break;
            }
        }
    }

    private static void WriteSimpleReference(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder value,
        CompareToDefaultCallback? compareToDefaultCallback,
        CompareToEmptyCallback? compareToEmptyCallback,
        WriteValueCallback writeValueCallback
    )
    {
        Debug.Assert(!type.IsValueType, "Cannot call this method with a value type.");

        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;
        var defaultValueAttribute = ReflectionHelpers.GetCustomAttribute<DefaultValueAttribute>(property);

        var nextPropertyLabel = ilGen.DefineLabel();

        if (compareToDefaultCallback != null && skipWritingCondition.HasFlag(Condition.WhenDefault))
        {
            compareToDefaultCallback(value, defaultValueAttribute?.Value);
            ilGen.Emit(OpCodes.Brtrue, nextPropertyLabel);
        }

        if (skipWritingCondition.HasFlag(Condition.WhenNull))
        {
            ilGen.EmitLdloc(value);
            ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
        }
        else
        {
            var notNullLabel = ilGen.DefineLabel();
            ilGen.EmitLdloc(value);
            ilGen.Emit(OpCodes.Brtrue, notNullLabel);

            WriteJsonNull(ilGen, name);
            ilGen.Emit(OpCodes.Br, nextPropertyLabel);

            ilGen.MarkLabel(notNullLabel);
        }

        if (compareToEmptyCallback != null && skipWritingCondition.HasFlag(Condition.WhenEmpty))
        {
            compareToEmptyCallback(value);
            ilGen.Emit(OpCodes.Brtrue, nextPropertyLabel);
        }

        writeValueCallback(value);

        ilGen.MarkLabel(nextPropertyLabel);
    }

    private static void WriteSimpleValue(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder value,
        CompareToDefaultCallback? compareToDefaultCallback,
        WriteValueCallback writeValueCallback
    )
    {
        GetNullableType(type, out var localPropertyType, out var localPropertyIsNullable);

        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;
        var defaultValueAttribute = ReflectionHelpers.GetCustomAttribute<DefaultValueAttribute>(property);
        if (localPropertyIsNullable)
        {
            var failureLabel = ilGen.DefineLabel();
            var nextPropertyLabel = ilGen.DefineLabel();
            ilGen.EmitLdloca(value);
            ilGen.Emit(OpCodes.Call, type.GetMethod("get_HasValue", Type.EmptyTypes)!);
            if (skipWritingCondition.HasFlag(Condition.WhenNull))
            {
                ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
            }
            else
            {
                ilGen.Emit(OpCodes.Brfalse, failureLabel);
            }

            var valueLocal = ilGen.DeclareLocal(localPropertyType);
            ilGen.EmitLdloca(value);
            ilGen.Emit(OpCodes.Call, type.GetMethod("get_Value", Type.EmptyTypes)!);
            ilGen.EmitStloc(valueLocal);

            if (compareToDefaultCallback != null && skipWritingCondition.HasFlag(Condition.WhenDefault))
            {
                compareToDefaultCallback(valueLocal, defaultValueAttribute?.Value);
                ilGen.Emit(OpCodes.Brtrue, nextPropertyLabel);

                writeValueCallback(valueLocal);
            }
            else
            {
                writeValueCallback(valueLocal);
            }

            ilGen.Emit(OpCodes.Br, nextPropertyLabel);

            if (!skipWritingCondition.HasFlag(Condition.WhenNull))
            {
                ilGen.MarkLabel(failureLabel);
                WriteJsonNull(ilGen, name);
            }

            ilGen.MarkLabel(nextPropertyLabel);
        }
        else
        {
            if (compareToDefaultCallback != null && skipWritingCondition.HasFlag(Condition.WhenDefault))
            {
                var nextPropertyLabel = ilGen.DefineLabel();
                compareToDefaultCallback(value, defaultValueAttribute?.Value);
                ilGen.Emit(OpCodes.Brtrue, nextPropertyLabel);

                writeValueCallback(value);

                ilGen.MarkLabel(nextPropertyLabel);
            }
            else
            {
                writeValueCallback(value);
            }
        }
    }

    private static void WriteNumber(ILGenerator ilGen, Type type, string? name, LocalBuilder value)
    {
        // WriteNumber doesn't have a byte overload.
        if (type == typeof(byte))
        {
            type = typeof(uint);
        }

        if (name != null)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdc(name);
            ilGen.EmitLdloc(value);
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(Utf8JsonWriter).GetMethod("WriteNumber", new[] { typeof(string), type })!
            );
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdloc(value);
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteNumberValue", new[] { type })!);
        }
    }

    private static void WriteEnumValue(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder value
    )
    {
        GetNullableType(type, out var localPropertyType, out var localPropertyIsNullable);

        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;
        var defaultValueAttribute = ReflectionHelpers.GetCustomAttribute<DefaultValueAttribute>(property);
        var enumSerializationAttribute = ReflectionHelpers.GetCustomAttribute<EnumSerializableSettingsAttribute>(
            localPropertyType
        );

        var serializeAsNumbers =
            (enumSerializationAttribute?.SerializationType ?? EnumSerializableSettingsSerializationType.Default)
            == EnumSerializableSettingsSerializationType.Numbers;
        var invalidValue = enumSerializationAttribute?.InvalidValue;
        if (invalidValue != null)
        {
            if (invalidValue.GetType() != type)
            {
                throw new Exception(
                    $"'{type.FullName}' has invalid default value. Must be of type {type.FullName} not {invalidValue.GetType().FullName}."
                );
            }
        }

        if (serializeAsNumbers && invalidValue != null)
        {
            throw new Exception($"'{type.FullName}' has both UseNumbers and InvalidValue set.");
        }

        if (localPropertyIsNullable)
        {
            var failureLabel = ilGen.DefineLabel();
            var nextPropertyLabel = ilGen.DefineLabel();
            ilGen.EmitLdloca(value);
            ilGen.Emit(OpCodes.Call, type.GetMethod("get_HasValue", Type.EmptyTypes)!);
            if (skipWritingCondition.HasFlag(Condition.WhenNull))
            {
                ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
            }
            else
            {
                ilGen.Emit(OpCodes.Brfalse, failureLabel);
            }

            if (skipWritingCondition.HasFlag(Condition.WhenDefault))
            {
                var valueLocal = ilGen.DeclareLocal(localPropertyType);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, type.GetMethod("get_Value", Type.EmptyTypes)!);
                ilGen.EmitStloc(valueLocal);
                ilGen.EmitLdloc(valueLocal);

                var defaultValue = (int)
                    Convert.ChangeType(
                        defaultValueAttribute?.Value ?? Activator.CreateInstance(localPropertyType)!,
                        localPropertyType,
                        CultureInfo.InvariantCulture
                    );
                ilGen.EmitLdc(defaultValue);
                ilGen.Emit(OpCodes.Beq, nextPropertyLabel);

                if (serializeAsNumbers)
                {
                    WriteNumber(ilGen, typeof(int), name, valueLocal);
                }
                else
                {
                    var valueLocalString = GetEnumStringValue(valueLocal);
                    WriteJsonString(ilGen, name, valueLocalString);
                }
            }
            else
            {
                var valueLocal = ilGen.DeclareLocal(localPropertyType);
                ilGen.EmitLdloca(value);
                ilGen.Emit(OpCodes.Call, type.GetMethod("get_Value", Type.EmptyTypes)!);
                ilGen.EmitStloc(valueLocal);

                if (serializeAsNumbers)
                {
                    WriteNumber(ilGen, typeof(int), name, valueLocal);
                }
                else
                {
                    var valueLocalString = GetEnumStringValue(valueLocal);
                    WriteJsonString(ilGen, name, valueLocalString);
                }
            }

            ilGen.Emit(OpCodes.Br, nextPropertyLabel);

            if (!skipWritingCondition.HasFlag(Condition.WhenNull))
            {
                ilGen.MarkLabel(failureLabel);
                WriteJsonNull(ilGen, name);
            }

            ilGen.MarkLabel(nextPropertyLabel);
        }
        else
        {
            if (skipWritingCondition.HasFlag(Condition.WhenDefault))
            {
                var nextPropertyLabel = ilGen.DefineLabel();

                ilGen.EmitLdloc(value);

                var defaultValue = (int)
                    Convert.ChangeType(
                        defaultValueAttribute?.Value ?? Activator.CreateInstance(localPropertyType)!,
                        localPropertyType,
                        CultureInfo.InvariantCulture
                    );
                ilGen.EmitLdc(defaultValue);
                ilGen.Emit(OpCodes.Beq, nextPropertyLabel);

                if (serializeAsNumbers)
                {
                    WriteNumber(ilGen, typeof(int), name, value);
                }
                else
                {
                    var valueLocalString = GetEnumStringValue(value);
                    WriteJsonString(ilGen, name, valueLocalString);
                }

                ilGen.MarkLabel(nextPropertyLabel);
            }
            else
            {
                if (serializeAsNumbers)
                {
                    WriteNumber(ilGen, typeof(int), name, value);
                }
                else
                {
                    var valueLocalString = GetEnumStringValue(value);
                    WriteJsonString(ilGen, name, valueLocalString);
                }
            }
        }

        LocalBuilder GetEnumStringValue(LocalBuilder valueLocal)
        {
            var enumValues = new Dictionary<int, string>();
            var invalidValueString = default(string);
            var result = ilGen.DeclareLocal(typeof(string));
            var successLabel = ilGen.DefineLabel();
            var failureLabel = ilGen.DefineLabel();

            foreach (var member in localPropertyType.GetMembers())
            {
                var enumInfo = member as FieldInfo;
                if (enumInfo == null || enumInfo.FieldType != localPropertyType)
                {
                    continue;
                }

                var enumNameAttributes = ReflectionHelpers.GetCustomAttributes<NameAttribute>(enumInfo);
                if (enumNameAttributes.Length == 0)
                {
                    continue;
                }

                var enumValue = enumInfo.GetValue(null)!;

                foreach (var enumNameAttribute in enumNameAttributes)
                {
                    if (enumNameAttribute.Names.Count == 0)
                    {
                        continue;
                    }

                    if (!enumValues.TryAdd((int)enumValue, enumNameAttribute.Names[0]))
                    {
                        throw new Exception(
                            $"Duplicate name {enumNameAttribute.Names[0]} on {localPropertyType.FullName}."
                        );
                    }

                    if (invalidValue != null && invalidValueString == null)
                    {
                        if (invalidValue.Equals(enumValue))
                        {
                            invalidValueString = enumNameAttribute.Names[0];
                        }
                    }
                }
            }

            if (enumValues.Count <= 4)
            {
                foreach (var (enumValue, enumName) in enumValues)
                {
                    var nextNameLabel = ilGen.DefineLabel();

                    ilGen.EmitLdloc(valueLocal);
                    ilGen.EmitLdc(enumValue);
                    ilGen.Emit(OpCodes.Bne_Un, nextNameLabel);

                    ilGen.EmitLdc(enumName);
                    ilGen.EmitStloc(result);
                    ilGen.Emit(OpCodes.Br, successLabel);

                    ilGen.MarkLabel(nextNameLabel);
                }
            }
            else
            {
                var ranges = ParsingIntSwitchGenerator.GenerateRanges(enumValues.Keys);
                ParsingIntSwitchGenerator.RecurseSwitch(
                    ilGen,
                    ranges,
                    0,
                    ranges.Count - 1,
                    () => ilGen.EmitLdloc(valueLocal),
                    i =>
                    {
                        ilGen.EmitLdc(enumValues[i]);
                        ilGen.EmitStloc(result);
                    },
                    successLabel,
                    failureLabel
                );
            }

            ilGen.MarkLabel(failureLabel);
            if (invalidValue != null)
            {
                if (invalidValueString == null)
                {
                    throw new InvalidDataException($"{localPropertyType.FullName} default value was not found.");
                }

                ilGen.EmitLdc(invalidValueString);
                ilGen.EmitStloc(result);
            }
            else
            {
                ilGen.EmitLdloca(valueLocal);
                ilGen.Emit(OpCodes.Callvirt, typeof(int).GetMethod("ToString", Type.EmptyTypes)!);
                ilGen.EmitStloc(result);
            }

            ilGen.MarkLabel(successLabel);
            return result;
        }
    }

    private void WriteArray(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder array,
        Dictionary<Type, MethodInfo> inFlightMethods
    )
    {
        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;

        var arrayElementType = type.IsArray ? type.GetElementType()! : type.GenericTypeArguments[0];
        if (arrayElementType.IsGenericType && arrayElementType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
        {
            if (arrayElementType.GetGenericArguments()[0] == typeof(string))
            {
                // Special case KeyValuePair<string, _>[] to be a dictionary.
                this.WriteDictionary(ilGen, type, property, name, array, inFlightMethods);
                return;
            }
        }

        var nextPropertyLabel = ilGen.DefineLabel();
        if (skipWritingCondition.HasFlag(Condition.WhenNull) || skipWritingCondition.HasFlag(Condition.WhenDefault))
        {
            ilGen.EmitLdloc(array);
            ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
        }
        else
        {
            var notNullLabel = ilGen.DefineLabel();
            ilGen.EmitLdloc(array);
            ilGen.Emit(OpCodes.Brtrue, notNullLabel);

            WriteJsonNull(ilGen, name);
            ilGen.Emit(OpCodes.Br, nextPropertyLabel);

            ilGen.MarkLabel(notNullLabel);
        }

        if (skipWritingCondition.HasFlag(Condition.WhenEmpty))
        {
            if (type.IsArray)
            {
                ilGen.EmitLdloc(array);
                ilGen.Emit(OpCodes.Ldlen);
            }
            else
            {
                ilGen.EmitLdloc(array);
                ilGen.Emit(OpCodes.Callvirt, type.GetMethod("get_Count", Type.EmptyTypes)!);
            }

            ilGen.EmitLdc(0);
            ilGen.Emit(OpCodes.Beq, nextPropertyLabel);
        }

        if (name == null)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonWriter).GetMethod("WriteStartArray", Type.EmptyTypes)!);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdc(name);
            ilGen.Emit(OpCodes.Call, typeof(Utf8JsonWriter).GetMethod("WriteStartArray", new[] { typeof(string) })!);
        }

        var arrayCount = ilGen.DeclareLocal(typeof(int));
        var arrayLoopIndex = ilGen.DeclareLocal(typeof(int));
        var arrayLoopValue = ilGen.DeclareLocal(arrayElementType);

        if (type.IsArray)
        {
            ilGen.EmitLdloc(array);
            ilGen.Emit(OpCodes.Ldlen);
            ilGen.EmitStloc(arrayCount);
        }
        else
        {
            ilGen.EmitLdloc(array);
            ilGen.Emit(OpCodes.Callvirt, type.GetMethod("get_Count", Type.EmptyTypes)!);
            ilGen.EmitStloc(arrayCount);
        }

        var arrayLoopLogic = ilGen.DefineLabel();
        var arrayLoopStart = ilGen.DefineLabel();

        // var index = 0;
        ilGen.EmitLdc(0);
        ilGen.EmitStloc(arrayLoopIndex);
        ilGen.Emit(OpCodes.Br, arrayLoopLogic);

        ilGen.MarkLabel(arrayLoopStart);

        // arrayValue = array[index];
        if (type.IsArray)
        {
            ilGen.EmitLdloc(array);
            ilGen.EmitLdloc(arrayLoopIndex);
            ilGen.EmitLdelem(arrayElementType);
            ilGen.EmitStloc(arrayLoopValue);
        }
        else
        {
            ilGen.EmitLdloc(array);
            ilGen.EmitLdloc(arrayLoopIndex);
            ilGen.Emit(OpCodes.Callvirt, type.GetMethod("get_Item", new[] { typeof(int) })!);
            ilGen.EmitStloc(arrayLoopValue);
        }

        this.WriteIndividualElement(
            ilGen,
            null,
            arrayLoopValue,
            arrayElementType,
            new ArrayChildProvider(property),
            inFlightMethods
        );

        // index++
        ilGen.EmitLdloc(arrayLoopIndex);
        ilGen.EmitLdc(1);
        ilGen.Emit(OpCodes.Add);
        ilGen.EmitStloc(arrayLoopIndex);

        // if (index < array.Length)
        ilGen.MarkLabel(arrayLoopLogic);
        ilGen.EmitLdloc(arrayLoopIndex);
        ilGen.EmitLdloc(arrayCount);
        ilGen.Emit(OpCodes.Clt);
        ilGen.Emit(OpCodes.Brtrue, arrayLoopStart);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Call, typeof(Utf8JsonWriter).GetMethod("WriteEndArray", Type.EmptyTypes)!);

        ilGen.MarkLabel(nextPropertyLabel);
    }

    private void WriteDictionary(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder dictionary,
        Dictionary<Type, MethodInfo> inFlightMethods
    )
    {
        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;

        var isDictionary = IsGenericStringDictionary(type);
        Type arrayElementType;
        if (isDictionary)
        {
            var dictionaryInterface = ReflectionHelpers.FindGenericInterface(type, typeof(IDictionary<,>))!;
            arrayElementType = dictionaryInterface.GetGenericArguments()[1];
        }
        else
        {
            // We must be IEnumerable<KeyValuePair<string, _>>
            var enumerableInterface =
                type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                    ? type
                    : ReflectionHelpers.FindGenericInterface(type, typeof(IEnumerable<>))!;

            var keyValuePairType = enumerableInterface.GetGenericArguments()[0];
            arrayElementType = keyValuePairType.GetGenericArguments()[1];
        }

        var nextPropertyLabel = ilGen.DefineLabel();
        if (skipWritingCondition.HasFlag(Condition.WhenNull) || skipWritingCondition.HasFlag(Condition.WhenDefault))
        {
            ilGen.EmitLdloc(dictionary);
            ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
        }
        else
        {
            var notNullLabel = ilGen.DefineLabel();
            ilGen.EmitLdloc(dictionary);
            ilGen.Emit(OpCodes.Brtrue, notNullLabel);

            WriteJsonNull(ilGen, name);
            ilGen.Emit(OpCodes.Br, nextPropertyLabel);

            ilGen.MarkLabel(notNullLabel);
        }

        if (skipWritingCondition.HasFlag(Condition.WhenEmpty))
        {
            // WriteDictionary can sometimes write IEnumerable and array types.
            if (type.IsArray)
            {
                ilGen.EmitLdloc(dictionary);
                ilGen.Emit(OpCodes.Ldlen);

                ilGen.EmitLdc(0);
                ilGen.Emit(OpCodes.Beq, nextPropertyLabel);
            }
            else
            {
                var getCountMethod = type.GetMethod("get_Count", Type.EmptyTypes);
                if (getCountMethod != null)
                {
                    ilGen.EmitLdloc(dictionary);
                    ilGen.Emit(OpCodes.Callvirt, getCountMethod);

                    ilGen.EmitLdc(0);
                    ilGen.Emit(OpCodes.Beq, nextPropertyLabel);
                }
            }
        }

        if (name == null)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteStartObject", Type.EmptyTypes)!);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdc(name);
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(Utf8JsonWriter).GetMethod("WriteStartObject", new[] { typeof(string) })!
            );
        }

        var pairType = typeof(KeyValuePair<,>).MakeGenericType(typeof(string), arrayElementType);
        var enumerableType = typeof(IEnumerable<>).MakeGenericType(pairType);
        var enumeratorType = typeof(IEnumerator<>).MakeGenericType(pairType);
        var enumerator = ilGen.DeclareLocal(enumeratorType);
        ilGen.EmitLdloc(dictionary);
        ilGen.Emit(OpCodes.Callvirt, enumerableType.GetMethod("GetEnumerator", Type.EmptyTypes)!);
        ilGen.EmitStloc(enumerator);

        // Try
        _ = ilGen.BeginExceptionBlock();

        var finallyEndLabel = ilGen.DefineLabel();
        var pair = ilGen.DeclareLocal(pairType);
        var propertyName = ilGen.DeclareLocal(typeof(string));
        var propertyValue = ilGen.DeclareLocal(arrayElementType);

        var arrayLoopLogic = ilGen.DefineLabel();
        var arrayLoopStart = ilGen.DefineLabel();

        ilGen.Emit(OpCodes.Br, arrayLoopLogic);

        ilGen.MarkLabel(arrayLoopStart);

        // var pair = enumerator.Current;
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Callvirt, enumeratorType.GetMethod("get_Current", Type.EmptyTypes)!);
        ilGen.EmitStloc(pair);

        // var propertyName = pair.First;
        ilGen.EmitLdloca(pair);
        ilGen.Emit(OpCodes.Call, pairType.GetMethod("get_Key", Type.EmptyTypes)!);
        ilGen.EmitStloc(propertyName);

        // var propertyValue = pair.First;
        ilGen.EmitLdloca(pair);
        ilGen.Emit(OpCodes.Call, pairType.GetMethod("get_Value", Type.EmptyTypes)!);
        ilGen.EmitStloc(propertyValue);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.EmitLdloc(propertyName);
        ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WritePropertyName", new[] { typeof(string) })!);

        this.WriteIndividualElement(
            ilGen,
            null,
            propertyValue,
            arrayElementType,
            new ArrayChildProvider(property),
            inFlightMethods
        );

        // if (!enumerator.MoveNext()) break;
        ilGen.MarkLabel(arrayLoopLogic);
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Callvirt, typeof(System.Collections.IEnumerator).GetMethod("MoveNext", Type.EmptyTypes)!);
        ilGen.Emit(OpCodes.Brtrue, arrayLoopStart);

        // Finally
        ilGen.BeginFinallyBlock();

        // if (enumerator != null)
        var isNullLabelInFinally = ilGen.DefineLabel();
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Brfalse, isNullLabelInFinally);

        // enumerator.Dispose()
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Callvirt, typeof(IDisposable).GetMethod("Dispose", Type.EmptyTypes)!);

        ilGen.MarkLabel(isNullLabelInFinally);

        ilGen.EndExceptionBlock();
        ilGen.MarkLabel(finallyEndLabel);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteEndObject", Type.EmptyTypes)!);

        ilGen.MarkLabel(nextPropertyLabel);
    }

    private void WriteIEnumerable(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder enumerable,
        Dictionary<Type, MethodInfo> inFlightMethods
    )
    {
        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;

        var enumerableInterface =
            type.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                ? type
                : ReflectionHelpers.FindGenericInterface(type, typeof(IEnumerable<>))!;

        var arrayElementType = enumerableInterface.GetGenericArguments()[0];
        if (arrayElementType.IsGenericType && arrayElementType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
        {
            if (arrayElementType.GetGenericArguments()[0] == typeof(string))
            {
                // Special case IEnumerable<KeyValuePair<string, _>> to be a dictionary.
                this.WriteDictionary(ilGen, type, property, name, enumerable, inFlightMethods);
                return;
            }
        }

        var nextPropertyLabel = ilGen.DefineLabel();
        if (skipWritingCondition.HasFlag(Condition.WhenNull))
        {
            ilGen.EmitLdloc(enumerable);
            ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
        }
        else
        {
            var notNullLabel = ilGen.DefineLabel();
            ilGen.EmitLdloc(enumerable);
            ilGen.Emit(OpCodes.Brtrue, notNullLabel);

            WriteJsonNull(ilGen, name);
            ilGen.Emit(OpCodes.Br, nextPropertyLabel);

            ilGen.MarkLabel(notNullLabel);
        }

        if (name == null)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteStartArray", Type.EmptyTypes)!);
        }
        else
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdc(name);
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(Utf8JsonWriter).GetMethod("WriteStartArray", new[] { typeof(string) })!
            );
        }

        var enumeratorType = typeof(IEnumerator<>).MakeGenericType(arrayElementType);
        var enumerator = ilGen.DeclareLocal(enumeratorType);
        ilGen.EmitLdloc(enumerable);
        ilGen.Emit(
            OpCodes.Callvirt,
            typeof(System.Collections.IEnumerable).GetMethod("GetEnumerator", Type.EmptyTypes)!
        );
        ilGen.EmitStloc(enumerator);

        // Try
        _ = ilGen.BeginExceptionBlock();
        var finallyEndLabel = ilGen.DefineLabel();

        var element = ilGen.DeclareLocal(arrayElementType);

        var arrayLoopLogic = ilGen.DefineLabel();
        var arrayLoopStart = ilGen.DefineLabel();

        ilGen.Emit(OpCodes.Br, arrayLoopLogic);

        ilGen.MarkLabel(arrayLoopStart);

        // var pair = enumerator.Current;
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Callvirt, enumeratorType.GetMethod("get_Current", Type.EmptyTypes)!);
        ilGen.EmitStloc(element);

        this.WriteIndividualElement(
            ilGen,
            null,
            element,
            arrayElementType,
            new ArrayChildProvider(property),
            inFlightMethods
        );

        // if (!enumerator.MoveNext()) break;
        ilGen.MarkLabel(arrayLoopLogic);
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Callvirt, typeof(System.Collections.IEnumerator).GetMethod("MoveNext", Type.EmptyTypes)!);
        ilGen.Emit(OpCodes.Brtrue, arrayLoopStart);

        // Finally
        ilGen.BeginFinallyBlock();

        // if (enumerator != null)
        var isNullLabelInFinally = ilGen.DefineLabel();
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Brfalse, isNullLabelInFinally);

        // enumerator.Dispose()
        ilGen.EmitLdloc(enumerator);
        ilGen.Emit(OpCodes.Callvirt, typeof(IDisposable).GetMethod("Dispose", Type.EmptyTypes)!);

        ilGen.MarkLabel(isNullLabelInFinally);

        ilGen.EndExceptionBlock();
        ilGen.MarkLabel(finallyEndLabel);

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteEndArray", Type.EmptyTypes)!);

        ilGen.MarkLabel(nextPropertyLabel);
    }

    private void WriteObject(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder value,
        Dictionary<Type, MethodInfo> inFlightMethods
    )
    {
        if (!inFlightMethods.TryGetValue(type, out var serializeMethod))
        {
            serializeMethod = this.CompileSerializer(type, inFlightMethods).Method;
        }

        WriteSerializeMethod(ilGen, type, property, name, value, serializeMethod);
    }

    private static void WriteSerializeMethod(
        ILGenerator ilGen,
        Type type,
        ICustomAttributeProvider property,
        string? name,
        LocalBuilder value,
        MethodInfo customSerialize
    )
    {
        var skipWritingAttribute = ReflectionHelpers.GetCustomAttribute<SkipSerializationAttribute>(property);
        var skipWritingCondition = skipWritingAttribute?.SerializationCondition ?? Condition.None;

        var nextPropertyLabel = ilGen.DefineLabel();
        if (!type.IsValueType)
        {
            if (skipWritingCondition.HasFlag(Condition.WhenNull))
            {
                ilGen.EmitLdloc(value);
                ilGen.Emit(OpCodes.Brfalse, nextPropertyLabel);
            }
            else
            {
                var notNullLabel = ilGen.DefineLabel();
                ilGen.EmitLdloc(value);
                ilGen.Emit(OpCodes.Brtrue, notNullLabel);

                WriteJsonNull(ilGen, name);
                ilGen.Emit(OpCodes.Br, nextPropertyLabel);

                ilGen.MarkLabel(notNullLabel);
            }
        }

        if (name != null)
        {
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.EmitLdc(name);
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(Utf8JsonWriter).GetMethod("WritePropertyName", new[] { typeof(string) })!
            );
        }

        ilGen.Emit(OpCodes.Ldarg_0);
        ilGen.EmitLdloc(value);
        ilGen.Emit(OpCodes.Call, customSerialize);

        ilGen.MarkLabel(nextPropertyLabel);
    }

    private static void WriteJsonString(ILGenerator ilGen, string? name, LocalBuilder value)
    {
        ilGen.Emit(OpCodes.Ldarg_0);
        if (name != null)
        {
            ilGen.EmitLdc(name);
        }

        ilGen.EmitLdloc(value);
        if (name != null)
        {
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(Utf8JsonWriter).GetMethod("WriteString", new[] { typeof(string), typeof(string) })!
            );
        }
        else
        {
            ilGen.Emit(
                OpCodes.Callvirt,
                typeof(Utf8JsonWriter).GetMethod("WriteStringValue", new[] { typeof(string) })!
            );
        }
    }

    private static void WriteJsonNull(ILGenerator ilGen, string? name)
    {
        ilGen.Emit(OpCodes.Ldarg_0);
        if (name != null)
        {
            ilGen.EmitLdc(name);
        }

        if (name != null)
        {
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteNull", new[] { typeof(string) })!);
        }
        else
        {
            ilGen.Emit(OpCodes.Callvirt, typeof(Utf8JsonWriter).GetMethod("WriteNullValue", Type.EmptyTypes)!);
        }
    }

    private static bool IsAnonymousType(Type type)
    {
        return Attribute.IsDefined(type, typeof(CompilerGeneratedAttribute), false)
            && type.IsGenericType
            && type.Name.Contains("AnonymousType")
            && (
                type.Name.StartsWith("<>", StringComparison.Ordinal)
                || type.Name.StartsWith("VB$", StringComparison.Ordinal)
            )
            && type.Attributes.HasFlag(TypeAttributes.NotPublic);
    }

    private static bool IsListType(Type type)
    {
        return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
    }

    /// <summary>
    /// Gets if a <see cref="Type"/> is parsed as an individual element.
    /// </summary>
    /// <param name="type">Type to parse as an individual element.</param>
    /// <returns>True if the type supports parsing on its own.</returns>
    private static bool IsHandledAsIndividualElement(Type type)
    {
        GetNullableType(type, out var baseType, out _);

        if (baseType.IsEnum)
        {
            return true;
        }

        if (type.IsArray || IsListType(type))
        {
            return true;
        }

        if (
            TryGetJsonSerializableAttribute(type, out var jsonSerializableAttribute)
            && jsonSerializableAttribute.SourceGenerationMode.HasFlag(SourceGenerationMode.Serialize)
        )
        {
            return true;
        }

        if (IsGenericStringDictionary(type))
        {
            return true;
        }

        if (IsGenericIEnumerable(type))
        {
            return true;
        }

        return baseType.FullName switch
        {
            "System.Boolean" => true,
            "System.Byte" => true,
            "System.SByte" => true,
            "System.Int16" => true,
            "System.Int32" => true,
            "System.Int64" => true,
            "System.UInt16" => true,
            "System.UInt32" => true,
            "System.UInt64" => true,
            "System.String" => true,
            "System.Guid" => true,
            "System.DateTime" => true,
            "System.TimeSpan" => true,
            "System.Text.Json.Nodes.JsonNode" => true,
            "System.Text.Json.Nodes.JsonObject" => true,
            "System.Text.Json.Nodes.JsonArray" => true,
            "KestrelToolbox.Types.UUIDv1" => true,
            _ => false,
        };
    }

    private static bool IsGenericStringDictionary(Type type)
    {
        var interfaceType = ReflectionHelpers.FindGenericInterface(type, typeof(IDictionary<,>));
        if (interfaceType == null)
        {
            return false;
        }

        var genericArguments = interfaceType.GetGenericArguments();
        return genericArguments[0] == typeof(string);
    }

    private static bool IsGenericIEnumerable(Type type)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IEnumerable<>))
        {
            return true;
        }

        var interfaceType = ReflectionHelpers.FindGenericInterface(type, typeof(IEnumerable<>));
        if (interfaceType == null)
        {
            return false;
        }

        var genericArguments = interfaceType.GetGenericArguments();
        return genericArguments[0] == typeof(string);
    }

    private static bool TryGetCustomSerializeMethod(Type type, [NotNullWhen(true)] out MethodInfo? methodInfo)
    {
        foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Public))
        {
            var customSerializeAttribute = method.GetCustomAttribute<JsonSerializableCustomSerializeAttribute>();
            if (customSerializeAttribute != null)
            {
                methodInfo = method;
                return true;
            }
        }

        methodInfo = default;
        return false;
    }

    private static void GetNullableType(Type type, out Type baseType, out bool isNullable)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
            isNullable = true;
            baseType = type.GenericTypeArguments[0];
        }
        else
        {
            baseType = type;
            isNullable = false;
        }
    }

    /// <summary>
    /// Interface for providing attributes and states of fields/properties.
    /// </summary>
    private interface IPropertyProvider : ICustomAttributeProvider
    {
        /// <summary>
        /// Returns a custom attributes defined on this member, identified by type, or null if there are no custom attributes of that type.
        /// </summary>
        /// <param name="inherit">When true, look up the hierarchy chain for the inherited custom attribute.</param>
        /// <typeparam name="T">Type of custom custom attribute.</typeparam>
        /// <returns>The custom attribute or null.</returns>
        /// <exception cref="Exception">Multiple attributes were found.</exception>
        /// <exception cref="TypeLoadException">The custom attribute type cannot be loaded.</exception>
        public T? GetCustomAttribute<T>(bool inherit = false)
        {
            var arr = this.GetCustomAttributes(typeof(T), inherit);
            return arr.Length switch
            {
                0 => default,
                1 => (T)arr[0],
                _ => throw new Exception("Multiple attributes found."),
            };
        }
    }

    private sealed class PropertyProvider : IPropertyProvider
    {
        public PropertyInfo PropertyInfo { get; }

        public PropertyProvider(PropertyInfo propertyInfo)
        {
            this.PropertyInfo = propertyInfo;
            var context = new NullabilityInfoContext();
            var nullabilityInfo = context.Create(propertyInfo);

            // You can mismatch ReadState and WriteState by using [AllowNull] and [MaybeNull].
            // [AllowNull] = ReadState becoming nullable.
            // [MaybeNull] = WriteState becoming nullable.
            // I'm choosing to check both for setting NullabilityState because it's probably better to
            // allow nulls in the event a user isn't using #nullable (which sets both). If neither of them
            // is NullabilityState.Nullable, use ReadState because the user will be reading from this value
            // and that is likely what they want. In practice, it means the state is not nullable though.
            var isNullable =
                nullabilityInfo.WriteState == NullabilityState.Nullable
                || nullabilityInfo.ReadState == NullabilityState.Nullable;
            if (isNullable)
            {
                this.NullabilityState = NullabilityState.Nullable;
            }
            else
            {
                this.NullabilityState = nullabilityInfo.ReadState;
            }
        }

        public object[] GetCustomAttributes(bool inherit) => this.PropertyInfo.GetCustomAttributes(inherit);

        public object[] GetCustomAttributes(Type attributeType, bool inherit) =>
            this.PropertyInfo.GetCustomAttributes(attributeType, inherit);

        public bool IsDefined(Type attributeType, bool inherit) => this.PropertyInfo.IsDefined(attributeType, inherit);

        public NullabilityState NullabilityState { get; }
    }

    private sealed class ArrayChildProvider : IPropertyProvider
    {
        private readonly ICustomAttributeProvider provider;

        public ArrayChildProvider(ICustomAttributeProvider provider)
        {
            this.provider = provider;
        }

        public object[] GetCustomAttributes(bool inherit) => throw new NotSupportedException();

        public object[] GetCustomAttributes(Type attributeType, bool inherit) =>
            this.provider.GetCustomAttributes(attributeType, inherit);

        public bool IsDefined(Type attributeType, bool inherit) => throw new NotSupportedException();
    }

    private sealed class NoAttributesProvider : IPropertyProvider
    {
        public object[] GetCustomAttributes(bool inherit)
        {
            return Array.Empty<object>();
        }

        public object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            return Array.Empty<object>();
        }

        public bool IsDefined(Type attributeType, bool inherit)
        {
            return false;
        }
    }
}
