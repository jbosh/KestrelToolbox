// <copyright file="QueryStringParser.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;

namespace KestrelToolbox.Serialization.Helpers;

/// <summary>
/// Class for parsing query strings and validating that their values are valid.
/// </summary>
public static class QueryStringParser
{
    /// <summary>
    /// Tries to parse a string into a boolean value. This supports 1 and 0.
    /// </summary>
    /// <param name="value">Value to parse.</param>
    /// <param name="result">Resulting bool.</param>
    /// <returns>True if the value was parsed successfully, false if not.</returns>
    public static bool TryParseBool(string value, out bool result)
    {
        if (bool.TryParse(value, out result))
        {
            return true;
        }

        if (value.Equals("1", StringComparison.Ordinal))
        {
            result = true;
            return true;
        }

        if (value.Equals("0", StringComparison.Ordinal))
        {
            result = false;
            return true;
        }

        result = default;
        return false;
    }

    /// <summary>
    /// Tries to parse a base64_url string into a byte[] string efficiently.
    /// </summary>
    /// <param name="value">String value to attempt parsing.</param>
    /// <param name="result">Bytes if they were parsed correctly.</param>
    /// <returns>True if the value was successfully parsed, false if not.</returns>
    public static bool TryParseBase64String(string value, [NotNullWhen(true)] out byte[]? result)
    {
        var maxBytes = Base64.GetMaxDecodedFromUtf8Length(value.Length, Base64Encoding.Url);
        result = new byte[maxBytes];

        if (!Base64.TryDecodeFromString(value, result, out var decodedBytes, Base64Encoding.Url))
        {
            result = default;
            return false;
        }

        Debug.Assert(decodedBytes == result.Length, "Should have decoded all bytes.");
        return true;
    }
}
