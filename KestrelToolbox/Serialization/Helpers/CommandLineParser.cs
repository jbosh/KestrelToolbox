// <copyright file="CommandLineParser.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.Helpers;

/// <summary>
/// Helper functionality for command line serializable.
/// </summary>
public static class CommandLineParser
{
    /// <summary>
    /// Tries to parse a bool value.
    /// </summary>
    /// <param name="value">Value to parse.</param>
    /// <param name="result">Resulting value, or default if failed.</param>
    /// <returns>True if parsing was successful, false if not.</returns>
    public static bool TryParseBool(string value, out bool result)
    {
        if (
            value.Equals("false", StringComparison.OrdinalIgnoreCase)
            || value.Equals("f", StringComparison.OrdinalIgnoreCase)
            || value == "0"
        )
        {
            result = false;
            return true;
        }

        if (
            value.Equals("true", StringComparison.OrdinalIgnoreCase)
            || value.Equals("t", StringComparison.OrdinalIgnoreCase)
            || value == "1"
        )
        {
            result = true;
            return true;
        }

        result = default;
        return false;
    }
}
