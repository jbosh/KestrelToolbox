// <copyright file="JsonParsing.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace KestrelToolbox.Serialization.Helpers;

/// <summary>
/// Helpers for parsing json.
/// </summary>
public static class JsonParsing
{
    private static readonly UTF8Encoding Utf8Encoding = new(
        encoderShouldEmitUTF8Identifier: false,
        throwOnInvalidBytes: true
    );

    /// <summary>
    /// Try to parse a string from <see cref="Utf8JsonReader"/> in any token type. This means numbers and bools are read and
    /// turned into strings.
    /// </summary>
    /// <param name="reader">Reader to read from.</param>
    /// <param name="value">Output value.</param>
    /// <returns>True if the value was read successfully, false if not.</returns>
    public static bool TryParseAnyString(ref Utf8JsonReader reader, [NotNullWhen(true)] out string? value)
    {
#pragma warning disable IDE0010
        switch (reader.TokenType)
#pragma warning restore IDE0010
        {
            case JsonTokenType.String:
            {
                value = reader.GetString();
                Debug.Assert(value != null, "Value should never be null because token type was not null.");
                return true;
            }

            case JsonTokenType.False:
            {
                value = "false";
                return true;
            }

            case JsonTokenType.True:
            {
                value = "true";
                return true;
            }

            case JsonTokenType.Number:
            {
                if (reader.HasValueSequence)
                {
                    var sequence = reader.ValueSequence;
                    value = Utf8Encoding.GetString(sequence);
                }
                else
                {
                    var span = reader.ValueSpan;
                    Debug.Assert(!reader.ValueIsEscaped, "Numbers should not be escaped.");
                    value = Utf8Encoding.GetString(span);
                }

                return true;
            }

            default:
            {
                value = null;
                return false;
            }
        }
    }

    /// <summary>
    /// Parses a bool with from <paramref name="reader"/> with flexible type support.
    /// </summary>
    /// <param name="reader">Reader to read from.</param>
    /// <param name="value">Out value.</param>
    /// <param name="allowFlexibleStrings">Allow strings to be parsed.</param>
    /// <param name="allowNumbers">Allow numbers and strings as numbers to be parsed.</param>
    /// <returns>True if parsing was successful, false if not.</returns>
    public static bool TryParseFlexibleBool(
        ref Utf8JsonReader reader,
        out bool value,
        bool allowFlexibleStrings,
        bool allowNumbers
    )
    {
        if (reader.TokenType == JsonTokenType.True)
        {
            value = true;
            return true;
        }

        if (reader.TokenType == JsonTokenType.False)
        {
            value = false;
            return true;
        }

        if (allowFlexibleStrings)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                var s = reader.GetString();
                if (bool.TryParse(s, out value))
                {
                    return true;
                }

                if (allowNumbers)
                {
                    if (s == "1")
                    {
                        value = true;
                        return true;
                    }

                    if (s == "0")
                    {
                        value = false;
                        return true;
                    }
                }
            }
        }

        if (allowNumbers)
        {
            if (reader.TokenType == JsonTokenType.Number && reader.TryGetUInt64(out var i))
            {
                if (i == 1)
                {
                    value = true;
                    return true;
                }

                if (i == 0)
                {
                    value = false;
                    return true;
                }
            }
        }

        value = default;
        return false;
    }

    /// <summary>
    /// Tries to parse a <see cref="JsonNode"/> from <paramref name="reader"/>.
    /// </summary>
    /// <param name="reader">Reader to read from.</param>
    /// <param name="value">Output value.</param>
    /// <returns>True if successful, false if not.</returns>
    public static bool TryParseJsonNode(ref Utf8JsonReader reader, [NotNullWhen(true)] out JsonNode? value)
    {
        value = JsonNode.Parse(ref reader);
        return value != null;
    }

    /// <summary>
    /// Tries to parse a <see cref="JsonObject"/> from <paramref name="reader"/>.
    /// </summary>
    /// <param name="reader">Reader to read from.</param>
    /// <param name="value">Output value.</param>
    /// <returns>True if successful, false if not.</returns>
    public static bool TryParseJsonObject(ref Utf8JsonReader reader, [NotNullWhen(true)] out JsonObject? value)
    {
        if (reader.TokenType != JsonTokenType.StartObject)
        {
            value = default;
            return false;
        }

        var node = JsonNode.Parse(ref reader);
        if (node == null)
        {
            value = default;
            return false;
        }

        value = node.AsObject();
        return true;
    }

    /// <summary>
    /// Tries to parse a <see cref="JsonArray"/> from <paramref name="reader"/>.
    /// </summary>
    /// <param name="reader">Reader to read from.</param>
    /// <param name="value">Output value.</param>
    /// <returns>True if successful, false if not.</returns>
    public static bool TryParseJsonArray(ref Utf8JsonReader reader, [NotNullWhen(true)] out JsonArray? value)
    {
        if (reader.TokenType != JsonTokenType.StartArray)
        {
            value = default;
            return false;
        }

        var node = JsonNode.Parse(ref reader);
        if (node == null)
        {
            value = default;
            return false;
        }

        value = node.AsArray();
        return true;
    }
}
