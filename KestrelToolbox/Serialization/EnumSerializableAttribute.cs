// <copyright file="EnumSerializableAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Serialization.DataAnnotations.Serialization;

namespace KestrelToolbox.Serialization;

/// <summary>
/// Specifies that a class will have AOT generated enum parsing and serialization added.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class EnumSerializableAttribute : Attribute
{
    /// <summary>
    /// Gets the types of enums this attribute will allow serialization of.
    /// </summary>
    public Type[] Types { get; }

    /// <summary>
    /// Gets or sets the source generation mode for this enum. Default is both serialize and deserialize support.
    /// </summary>
    public SourceGenerationMode SourceGenerationMode { get; set; } = SourceGenerationMode.Default;

    /// <summary>
    /// Gets or sets the method name to use in generated parsing code. Defaults to <c>TryParse</c>.
    /// </summary>
    public string ParsingMethodName { get; set; } = "TryParseEnum";

    /// <summary>
    /// Gets or sets the method name to use in generated serialization code. Defaults to <c>Serialize</c>.
    /// </summary>
    public string SerializingMethodName { get; set; } = "SerializeEnum";

    /// <summary>
    /// Gets or sets a method name that is used for parsing (instead of generating).
    /// </summary>
    public string? CustomParsingMethodName { get; set; }

    /// <summary>
    /// Gets or sets a method name that is used for parsing (instead of generating).
    /// </summary>
    public string? CustomSerializingMethodName { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="EnumSerializableAttribute"/> class.
    /// </summary>
    /// <param name="types">The types to add for serialization.</param>
    public EnumSerializableAttribute(params Type[] types)
    {
        this.Types = types;
    }
}
