// <copyright file="JsonSerializableAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Serialization.DataAnnotations.Serialization;

namespace KestrelToolbox.Serialization;

/// <summary>
/// Specifies that a class will have AOT generated json parsing and serialization added.
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
public class JsonSerializableAttribute : Attribute
{
    /// <summary>
    /// Gets the types of struct/class this attribute will allow serialization of.
    /// </summary>
    public Type[] Types { get; }

    /// <summary>
    /// Gets or sets the source generation mode for this json type. Default is both serialize and deserialize support.
    /// </summary>
    public SourceGenerationMode SourceGenerationMode { get; set; } = SourceGenerationMode.Default;

    /// <summary>
    /// Gets or sets the method name to use in generated parsing code. Defaults to <c>TryParse</c>.
    /// </summary>
    public string ParsingMethodName { get; set; } = "TryParseJson";

    /// <summary>
    /// Gets or sets the method name to use in generated serialization code. Defaults to <c>Serialize</c>.
    /// </summary>
    public string SerializingMethodName { get; set; } = "SerializeJson";

    /// <summary>
    /// Gets or sets a method name that is used for parsing (instead of generating).
    /// </summary>
    public string? CustomParsingMethodName { get; set; }

    /// <summary>
    /// Gets or sets a method name that is used for parsing (instead of generating).
    /// </summary>
    public string? CustomSerializingMethodName { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="JsonSerializableAttribute"/> class.
    /// </summary>
    /// <param name="types">The types to add for serialization.</param>
    /// <remarks>
    /// If no types are specified, the class that this attribute is attached to will be the only type.
    /// </remarks>
    public JsonSerializableAttribute(params Type[] types)
    {
        this.Types = types;
    }
}
