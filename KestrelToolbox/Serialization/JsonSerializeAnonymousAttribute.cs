// <copyright file="JsonSerializeAnonymousAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Serialization.Helpers;

namespace KestrelToolbox.Serialization;

/// <summary>
/// Specifies that a class will have JIT generated parsing support for things like anonymous types.
/// </summary>
/// <remarks>
/// This attribute works in tandem with <see cref="JsonSerializableAttribute"/> to avoid JITing types that are already
/// compiled ahead of time.
/// </remarks>
[AttributeUsage(AttributeTargets.Class)]
public class JsonSerializeAnonymousAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the method name to generate for serializing anonymous types. Defaults to SerializeJsonAnonymous.
    /// </summary>
    public string SerializingMethodName { get; set; } = "SerializeJsonAnonymous";

    /// <summary>
    /// Gets or sets the field name for storing the instance of <see cref="JsonAnonymousSerializer"/>.
    /// </summary>
    public string FieldName { get; set; } = "JsonAnonymousSerializer";
}
