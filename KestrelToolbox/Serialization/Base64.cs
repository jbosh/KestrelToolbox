// <copyright file="Base64.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using System.Text;
using KestrelToolbox.Types;

namespace KestrelToolbox.Serialization;

/// <summary>
/// Flags for how to encode base64.
/// </summary>
[Flags]
[SuppressMessage(
    "Major Code Smell",
    "S4070:Non-flags enums should not be marked with \"FlagsAttribute\"",
    Justification = "It's actually flags, sorta."
)]
[SuppressMessage(
    "Critical Code Smell",
    "S2346:Flags enumerations zero-value members should be named \"None\"",
    Justification = "Flags for this one are a little weird because of packing."
)]
public enum Base64Encoding : uint
{
    /// <summary>
    /// Rfc 4648, standard encoding.
    /// </summary>
    Standard = 0,

    /// <summary>
    /// Rfc 4648, url and filename-safe encoding.
    /// </summary>
    Url = 1,

    /// <summary>
    /// Mask for bits used for encoding type.
    /// </summary>
    TypeMask = ~0u >> 1,

    /// <summary>
    /// Bit for if padding is encoded or decoded into the base64 data.
    /// </summary>
    Padding = 1u << 31,

    /// <summary>
    /// Rfc 4848, standard encoding with padding.
    /// </summary>
    [SuppressMessage(
        "Design",
        "CA1069:Enums values should not be duplicated",
        Justification = "Default needs to exist, even if it's the same value as Padding."
    )]
    Default = Standard | Padding,
}

/// <summary>
/// Helper class to encode and decode base64 with different encoding types.
/// </summary>
public static class Base64
{
    /// <summary>
    /// Gets the maximum number of bytes of utf8 characters that would be encoded for the specified number of bytes.
    /// </summary>
    /// <param name="byteCount">Number of bytes to encode.</param>
    /// <param name="encoding">Encoding type the resulting base64 bytes will be.</param>
    /// <returns>Total number of characters that will be written.</returns>
    /// <remarks>
    /// This number is the equivalent number of characters required if encoding to a <c>string</c>.
    /// </remarks>
    public static int GetMaxEncodedToUtf8Length(int byteCount, Base64Encoding encoding = Base64Encoding.Default)
    {
        if (encoding.HasFlag(Base64Encoding.Padding))
        {
            return (byteCount + 2) / 3 * 4;
        }

        var extraChars = (byteCount % 3) switch
        {
            0 => 0,
            1 => 2,
            2 => 3,
            _ => 0,
        };

        return (byteCount / 3 * 4) + extraChars;
    }

    /// <summary>
    /// Encodes bytes to another a string.
    /// </summary>
    /// <param name="input">Input to encode.</param>
    /// <param name="encoding">Encoding type the resulting base64 bytes will be.</param>
    /// <returns>String representation of base64 encoded <paramref name="input"/>.</returns>
    [SkipLocalsInit]
    public static string EncodeToString(ReadOnlySpan<byte> input, Base64Encoding encoding = Base64Encoding.Default)
    {
        if (input.Length == 0)
        {
            return string.Empty;
        }

        var maxLength = GetMaxEncodedToUtf8Length(input.Length);
        if (maxLength < 2048)
        {
            Span<byte> utf8 = stackalloc byte[maxLength];

            var bytesWritten = EncodeToUtf8(input, utf8, encoding);

            return Encoding.ASCII.GetString(utf8.Slice(0, bytesWritten));
        }
        else
        {
            using var utf8Rental = new ArrayPoolRental(maxLength);

            var bytesWritten = EncodeToUtf8(input, utf8Rental.Array, encoding);

            using var charsRental = new ArrayPoolRental<char>(maxLength);
            return Encoding.ASCII.GetString(utf8Rental.AsSpan(0, bytesWritten));
        }
    }

    /// <summary>
    /// Encodes bytes to a span of utf8 bytes.
    /// </summary>
    /// <param name="input">Input to encode.</param>
    /// <param name="output">Destination utf8 span.</param>
    /// <param name="encoding">Encoding type the resulting base64 bytes will be.</param>
    /// <returns>
    /// Number of bytes written into <paramref name="output"/>. If there is not enough space, this method will throw
    /// <see cref="ArgumentOutOfRangeException"/>.
    /// </returns>
    /// <exception cref="ArgumentOutOfRangeException">
    /// Thrown when <paramref name="output"/> doesn't contain enough bytes to hold the result.
    /// </exception>
    public static unsafe int EncodeToUtf8(
        ReadOnlySpan<byte> input,
        Span<byte> output,
        Base64Encoding encoding = Base64Encoding.Default
    )
    {
        if (input.Length == 0)
        {
            return 0;
        }

        var maxLength = GetMaxEncodedToUtf8Length(input.Length, encoding);
        if (output.Length < maxLength)
        {
            throw new ArgumentOutOfRangeException(
                nameof(output),
                $"{nameof(output)} must be longer than {maxLength} bytes. Please use {nameof(GetMaxEncodedToUtf8Length)}."
            );
        }

        var status = System.Buffers.Text.Base64.EncodeToUtf8(
            input,
            output,
            out var bytesConsumed,
            out var bytesWritten,
            true
        );
        if (status == OperationStatus.DestinationTooSmall)
        {
            Debug.Assert(
                !encoding.HasFlag(Base64Encoding.Padding),
                "Should always have enough space if padding exists."
            );

            Span<byte> spareBytes = stackalloc byte[6];
            status = System.Buffers.Text.Base64.EncodeToUtf8(
                input.Slice(bytesConsumed),
                spareBytes,
                out bytesConsumed,
                out var spareWritten,
                true
            );

            spareBytes.Slice(0, spareWritten).CopyTo(output.Slice(bytesWritten));
            bytesWritten += spareWritten;
        }

        switch (status)
        {
            case OperationStatus.DestinationTooSmall:
            case OperationStatus.NeedMoreData:
            case OperationStatus.InvalidData:
            {
                // DestinationTooSmall and NeedMoreData shouldn't happen in practice.
                throw new InvalidDataException($"{nameof(input)} had invalid bytes.");
            }

            default:
            case OperationStatus.Done:
            {
                // Fall through.
                break;
            }
        }

#pragma warning disable IDE0010
        switch (encoding & Base64Encoding.TypeMask)
#pragma warning restore IDE0010
        {
            case Base64Encoding.Default:
            {
                // Already correct.
                break;
            }

            case Base64Encoding.Url:
            {
                // Needs conversion.
                // '+' -> '-'
                // '/' -> '_'
                var count = bytesWritten;
                var count32 = bytesWritten & ~0x1F;
                var count16 = bytesWritten & ~0xF;

                var iterator = 0;
                if (iterator < count32 && Vector256.IsHardwareAccelerated)
                {
                    fixed (byte* src = output)
                    {
                        var plusMask = Vector256.Create(0x2B2B2B2B).AsByte();
                        var slashMask = Vector256.Create(0x2F2F2F2F).AsByte();
                        var minusMask = Vector256.Create(0x2D2D2D2D).AsByte();
                        var underscoreMask = Vector256.Create(0x5F5F5F5F).AsByte();
                        for (; iterator < count32; iterator += 32)
                        {
                            var vec = Vector256.Load(src + iterator);
                            var pluses = Vector256.Equals(vec, plusMask);
                            var slashes = Vector256.Equals(vec, slashMask);
                            vec = Vector256.ConditionalSelect(pluses, minusMask, vec);
                            vec = Vector256.ConditionalSelect(slashes, underscoreMask, vec);
                            vec.Store(src + iterator);
                        }
                    }
                }

                if (iterator < count16 && Vector128.IsHardwareAccelerated)
                {
                    fixed (byte* src = output)
                    {
                        var plusMask = Vector128.Create(0x2B2B2B2B).AsByte();
                        var slashMask = Vector128.Create(0x2F2F2F2F).AsByte();
                        var minusMask = Vector128.Create(0x2D2D2D2D).AsByte();
                        var underscoreMask = Vector128.Create(0x5F5F5F5F).AsByte();
                        for (; iterator < count16; iterator += 16)
                        {
                            var vec = Vector128.Load(src + iterator);
                            var pluses = Vector128.Equals(vec, plusMask);
                            var slashes = Vector128.Equals(vec, slashMask);
                            vec = Vector128.ConditionalSelect(pluses, minusMask, vec);
                            vec = Vector128.ConditionalSelect(slashes, underscoreMask, vec);
                            vec.Store(src + iterator);
                        }
                    }
                }

                // Scalar
                for (; iterator < count; iterator++)
                {
                    var ch = output[iterator];
                    if (ch == (byte)'+')
                    {
                        output[iterator] = (byte)'-';
                    }
                    else if (ch == (byte)'/')
                    {
                        output[iterator] = (byte)'_';
                    }
                }

                break;
            }

            default:
            {
                // Don't know what to do. Just assume default.
                break;
            }
        }

        if (!encoding.HasFlag(Base64Encoding.Padding))
        {
            for (var i = bytesWritten - 1; i >= 0; i--)
            {
                var c = output[i];
                if (c != (byte)'=')
                {
                    return i + 1;
                }
            }
        }

        return bytesWritten;
    }

    /// <summary>
    /// Gets the maximum number of bytes that would be decoded for the specified number of characters.
    /// </summary>
    /// <param name="charCount">Number of characters to decode.</param>
    /// <param name="encoding">Encoding type the resulting base64 bytes will be.</param>
    /// <returns>Total number of characters that will be written.</returns>
    [SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Left here for future compatibility.")]
    public static int GetMaxDecodedFromUtf8Length(int charCount, Base64Encoding encoding = Base64Encoding.Default) =>
        charCount * 3 / 4;

    /// <summary>
    /// Decodes to a byte[] from an input string.
    /// </summary>
    /// <param name="input">Base64 encoded string to decode.</param>
    /// <param name="encoding">Encoding type the source base64 bytes are in.</param>
    /// <returns>byte[] of the decoded string.</returns>
    public static byte[] DecodeFromString(string input, Base64Encoding encoding = Base64Encoding.Default)
    {
        if (input.Length == 0)
        {
            return Array.Empty<byte>();
        }

        var maxUtf8ByteCount = Encoding.ASCII.GetMaxByteCount(input.Length);
        using var rental = new ArrayPoolRental(maxUtf8ByteCount);
        var utf8ByteCount = Encoding.ASCII.GetBytes(input, rental.Array);

        var maxBytes = GetMaxDecodedFromUtf8Length(input.Length, encoding);
        if (encoding.HasFlag(Base64Encoding.Padding))
        {
            // Padding requires there to be at least 4 characters
            if (input.Length < 4 || (input.Length & 0x3) != 0)
            {
                throw new InvalidDataException("Invalid padding.");
            }

            if (input[^1] == '=')
            {
                maxBytes -= input[^2] == '=' ? 2 : 1;
            }
        }

        var result = new byte[maxBytes];

        if (!TryDecodeFromUtf8(rental.Array.AsSpan(0, utf8ByteCount), result, out var decodedBytes, encoding))
        {
            throw new InvalidDataException("Could not parse string. Invalid characters.");
        }

        Debug.Assert(decodedBytes == result.Length, "Should have decoded all bytes.");
        return result;
    }

    /// <summary>
    /// Decodes a span from an input string.
    /// </summary>
    /// <param name="input">Base64 encoded string to decode.</param>
    /// <param name="output">Destination byte span.</param>
    /// <param name="writtenBytes">Number of bytes written to <paramref name="output"/>.</param>
    /// <param name="encoding">Encoding type the source base64 bytes are in.</param>
    /// <returns>True if decoding was successful, false if not.</returns>
    public static bool TryDecodeFromString(
        string input,
        Span<byte> output,
        out int writtenBytes,
        Base64Encoding encoding = Base64Encoding.Default
    )
    {
        if (input.Length == 0)
        {
            writtenBytes = 0;
            return true;
        }

        var maxUtf8ByteCount = Encoding.ASCII.GetMaxByteCount(input.Length);
        using var rental = new ArrayPoolRental(maxUtf8ByteCount);
        var utf8ByteCount = Encoding.ASCII.GetBytes(input, rental.Array);

        return TryDecodeFromUtf8(rental.Array.AsSpan(0, utf8ByteCount), output, out writtenBytes, encoding);
    }

    /// <summary>
    /// Decodes a byte[] from an input sequence.
    /// </summary>
    /// <param name="utf8">Base64 encoded utf8 string to decode.</param>
    /// <param name="output">Destination byte span.</param>
    /// <param name="encoding">Encoding type the source base64 bytes are in.</param>
    /// <returns>True if decoding was successful, false if not.</returns>
    public static bool TryDecodeFromUtf8(
        ReadOnlySequence<byte> utf8,
        [NotNullWhen(true)] out byte[]? output,
        Base64Encoding encoding = Base64Encoding.Default
    )
    {
        if (utf8.IsSingleSegment)
        {
            return TryDecodeFromUtf8(utf8.FirstSpan, out output, encoding);
        }

        // Using a rental and copying is super inefficient, but should hopefully not happen a ton in practice
        var rental = new ArrayPoolRental((int)utf8.Length);
        utf8.CopyTo(rental.Array);

        return TryDecodeFromUtf8(rental.AsSpan(0, (int)utf8.Length), out output, encoding);
    }

    /// <summary>
    /// Decodes a byte[] from an input span.
    /// </summary>
    /// <param name="utf8">Base64 encoded utf8 string to decode.</param>
    /// <param name="output">Destination byte span.</param>
    /// <param name="encoding">Encoding type the source base64 bytes are in.</param>
    /// <returns>True if decoding was successful, false if not.</returns>
    public static bool TryDecodeFromUtf8(
        ReadOnlySpan<byte> utf8,
        [NotNullWhen(true)] out byte[]? output,
        Base64Encoding encoding = Base64Encoding.Default
    )
    {
        var rental = new ArrayPoolRental(GetMaxDecodedFromUtf8Length(utf8.Length));
        if (TryDecodeFromUtf8(utf8, rental.Array, out var decodedBytes, encoding))
        {
            output = new byte[decodedBytes];
            Array.Copy(rental.Array, output, output.Length);
            return true;
        }

        output = null;
        return false;
    }

    /// <summary>
    /// Decodes a span from an input span.
    /// </summary>
    /// <param name="utf8">Base64 encoded utf8 string to decode.</param>
    /// <param name="output">Destination byte span.</param>
    /// <param name="bytesWritten">Number of bytes written to <paramref name="output"/>.</param>
    /// <param name="encoding">Encoding type the source base64 bytes are in.</param>
    /// <returns>True if decoding was successful, false if not. Can be false if there were not enough bytes available in <paramref name="output"/>.</returns>
    [SkipLocalsInit]
    public static unsafe bool TryDecodeFromUtf8(
        ReadOnlySpan<byte> utf8,
        Span<byte> output,
        out int bytesWritten,
        Base64Encoding encoding = Base64Encoding.Default
    )
    {
        if (utf8.Length == 0)
        {
            bytesWritten = 0;
            return true;
        }

        using var rental = default(ArrayPoolRental);
        var input = utf8;

#pragma warning disable IDE0010
        switch (encoding & Base64Encoding.TypeMask)
#pragma warning restore IDE0010
        {
            case Base64Encoding.Default:
            {
                // Already correct.
                break;
            }

            case Base64Encoding.Url:
            {
                // Needs conversion.
                // '-' -> '+'
                // '_' -> '/'

                // Decoding from URL is lame because it requires a conversion up front and memory is allocated to stash
                // it off.
                rental.Resize(input.Length);
                fixed (byte* src = utf8)
                {
                    fixed (byte* dst = rental.AsSpan())
                    {
                        var pSrc = src;
                        var pDst = dst;

                        var count = utf8.Length;
                        var count32 = utf8.Length & ~0x1F;
                        var count16 = utf8.Length & ~0xF;

                        var iterator = 0;
                        if (iterator < count32 && Vector256.IsHardwareAccelerated)
                        {
                            var plusMask = Vector256.Create(0x2B2B2B2B).AsByte();
                            var slashMask = Vector256.Create(0x2F2F2F2F).AsByte();
                            var minusMask = Vector256.Create(0x2D2D2D2D).AsByte();
                            var underscoreMask = Vector256.Create(0x5F5F5F5F).AsByte();
                            var invalid = Vector256.CreateScalar(0).AsByte();

                            var end = src + count32;
                            for (; pSrc < end; pSrc += 32)
                            {
                                var vec = Vector256.Load(pSrc);

                                // Validate that there are no invalid characters.
                                var pluses = Vector256.Equals(vec, plusMask);
                                var slashes = Vector256.Equals(vec, slashMask);
                                invalid = Vector256.BitwiseOr(invalid, Vector256.BitwiseOr(pluses, slashes));

                                var minuses = Vector256.Equals(vec, minusMask);
                                var underscores = Vector256.Equals(vec, underscoreMask);
                                vec = Vector256.ConditionalSelect(minuses, plusMask, vec);
                                vec = Vector256.ConditionalSelect(underscores, slashMask, vec);
                                vec.Store(pDst);
                                pDst += 32;
                            }

                            var invalidBits = invalid.ExtractMostSignificantBits();
                            if (invalidBits != 0)
                            {
                                bytesWritten = 0;
                                return false;
                            }
                        }

                        if (iterator < count16 && Vector128.IsHardwareAccelerated)
                        {
                            var plusMask = Vector128.Create(0x2B2B2B2B).AsByte();
                            var slashMask = Vector128.Create(0x2F2F2F2F).AsByte();
                            var minusMask = Vector128.Create(0x2D2D2D2D).AsByte();
                            var underscoreMask = Vector128.Create(0x5F5F5F5F).AsByte();
                            var invalid = Vector128.CreateScalar(0).AsByte();

                            var end = src + count16;
                            for (; pSrc < end; pSrc += 16)
                            {
                                var vec = Vector128.Load(pSrc);

                                // Validate that there are no invalid characters.
                                var pluses = Vector128.Equals(vec, plusMask);
                                var slashes = Vector128.Equals(vec, slashMask);
                                invalid = Vector128.BitwiseOr(invalid, Vector128.BitwiseOr(pluses, slashes));

                                var minuses = Vector128.Equals(vec, minusMask);
                                var underscores = Vector128.Equals(vec, underscoreMask);
                                vec = Vector128.ConditionalSelect(minuses, plusMask, vec);
                                vec = Vector128.ConditionalSelect(underscores, slashMask, vec);
                                vec.Store(pDst);
                                pDst += 16;
                            }

                            var invalidBits = invalid.ExtractMostSignificantBits();
                            if (invalidBits != 0)
                            {
                                bytesWritten = 0;
                                return false;
                            }
                        }

                        // Scalar
                        {
                            var end = src + count;
                            for (; pSrc < end; pSrc++)
                            {
                                var ch = *pSrc;
                                switch (ch)
                                {
                                    case (byte)'-':
                                    {
                                        *pDst = (byte)'+';
                                        break;
                                    }

                                    case (byte)'_':
                                    {
                                        *pDst = (byte)'/';
                                        break;
                                    }

                                    case (byte)'+':
                                    case (byte)'/':
                                    {
                                        bytesWritten = 0;
                                        return false;
                                    }

                                    default:
                                    {
                                        *pDst = ch;
                                        break;
                                    }
                                }

                                pDst++;
                            }
                        }
                    }
                }

                input = rental.AsSpan(0, utf8.Length);
                break;
            }

            default:
            {
                // Don't know what to do. Just assume default.
                break;
            }
        }

        // Mark as final block if we have all the padding. Otherwise we'll have to handle TooSmall on 100% of the decodes.
        var finalBlock = encoding.HasFlag(Base64Encoding.Padding);
        var status = System.Buffers.Text.Base64.DecodeFromUtf8(
            input,
            output,
            out var bytesConsumed,
            out var localBytesWritten,
            finalBlock
        );
        if (status == OperationStatus.NeedMoreData)
        {
            Debug.Assert(
                !encoding.HasFlag(Base64Encoding.Padding),
                "Should always have enough space if padding exists."
            );

            Span<byte> spareBytes = stackalloc byte[4];
            input.Slice(bytesConsumed).CopyTo(spareBytes);
            var remainingBytes = input.Length - bytesConsumed;
            for (var i = remainingBytes; i < spareBytes.Length; i++)
            {
                spareBytes[i] = (byte)'=';
            }

            status = System.Buffers.Text.Base64.DecodeFromUtf8(
                spareBytes,
                output.Slice(localBytesWritten),
                out bytesConsumed,
                out var spareWritten,
                true
            );

            Debug.Assert(bytesConsumed == 4, "Unexpected number of bytes consumed.");
            localBytesWritten += spareWritten;
        }

        switch (status)
        {
            case OperationStatus.DestinationTooSmall:
            case OperationStatus.NeedMoreData:
            {
                // DestinationTooSmall and NeedMoreData shouldn't happen in practice.
                bytesWritten = 0;
                return false;
            }

            case OperationStatus.InvalidData:
            {
                bytesWritten = 0;
                return false;
            }

            default:
            case OperationStatus.Done:
            {
                // Fall through.
                break;
            }
        }

        bytesWritten = localBytesWritten;
        return true;
    }
}
