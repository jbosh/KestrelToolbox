// <copyright file="QueryStringSerializableCustomParseAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies a class has custom query string parsing. It must be of the format <c>static bool TryParse(StringValues stringValues, out T? value, out string? error)</c>
/// Value should not be nullable if it is a struct but should be nullable if it is a class. value = default should be standard practice for
/// setting that value on error.
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class QueryStringSerializableCustomParseAttribute : Attribute { }
