// <copyright file="NullOnEmptyAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies that a data field will become null if it is empty.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class NullOnEmptyAttribute : Attribute
{
    /// <summary>
    /// Gets or sets a value indicating whether gets or sets if the property will trim on serialization.
    /// </summary>
    public bool OnSerialize { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether gets or sets if the property will trim when parsing.
    /// </summary>
    public bool OnParse { get; set; } = true;

    /// <summary>
    /// Initializes a new instance of the <see cref="NullOnEmptyAttribute"/> class.
    /// </summary>
    public NullOnEmptyAttribute() { }
}
