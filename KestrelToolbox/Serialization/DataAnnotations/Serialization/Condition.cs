// <copyright file="Condition.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations.Serialization;

/// <summary>
/// Condition under which to skip serialization.
/// </summary>
[Flags]
public enum Condition
{
    /// <summary>
    /// Always serialize the property.
    /// </summary>
    None = 0,

    /// <summary>
    /// If the value is the default, property is ignored during serialization.
    /// </summary>
    WhenDefault = 1 << 0,

    /// <summary>
    /// If the value is null, property is ignored during serialization.
    /// </summary>
    WhenNull = 1 << 1,

    /// <summary>
    /// If the value is empty, property is ignored during serialization.
    /// </summary>
    /// <remarks>
    /// This is for strings and collections.
    /// </remarks>
    WhenEmpty = 1 << 2,

    /// <summary>
    /// If the value is null or default value, property is ignored during serialization.
    /// </summary>
    WhenNullOrDefault = WhenDefault | WhenNull,

    /// <summary>
    /// If the value is null or empty, property is ignored during serialization.
    /// </summary>
    /// <remarks>
    /// This is for strings and collections.
    /// </remarks>
    WhenNullOrEmpty = WhenNull | WhenEmpty,
}
