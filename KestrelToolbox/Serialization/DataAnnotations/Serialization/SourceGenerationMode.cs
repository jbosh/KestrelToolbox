// <copyright file="SourceGenerationMode.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations.Serialization;

/// <summary>
/// Generation mode for KestrelToolbox source generator.
/// </summary>
[Flags]
public enum SourceGenerationMode
{
    /// <summary>
    /// Source should be generated for serialization.
    /// </summary>
    Serialize = 1 << 0,

    /// <summary>
    /// Source should be generated for deserialization.
    /// </summary>
    Deserialize = 1 << 1,

    /// <summary>
    /// Default mode which is to generate both serialization and deserialization.
    /// </summary>
    /// <remarks>
    /// If a mode is unavailable for a type (for example <see cref="QueryStringSerializableAttribute"/> cannot parse) it will
    /// be silently ignored.
    /// </remarks>
    Default = Serialize | Deserialize,
}
