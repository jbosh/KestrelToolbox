// <copyright file="JsonSerializableSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Internal;

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Attribute to change json parser settings for this structure.
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
public class JsonSerializableSettingsAttribute : Attribute
{
    /// <summary>
    /// Gets or sets a value indicating whether strict parsing is used. When strict is enabled,
    /// extra properties are not allowed on objects. Defaults to true.
    /// </summary>
    public bool AllowExtraProperties { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether fast parsing is enabled.
    /// </summary>
    /// <remarks>
    /// Use this value to indicate that none of the properties are anything except <c>default</c> and there is a default
    /// constructor that does the same.
    ///
    /// Speed on this one is gained because every single value will be initialized to something and
    /// <see cref="OptionalProperty{T}"/> is not used in generated code. <see cref="DefaultValueAttribute"/>s are still
    /// respected
    ///
    /// This does not affect jitted parsing speed because the jitter can access setters that are inaccessible from C#
    /// directly.
    /// </remarks>
    public bool FastParsingAllPropertiesAreDefault { get; set; }
}
