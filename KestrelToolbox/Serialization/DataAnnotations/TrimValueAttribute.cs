// <copyright file="TrimValueAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies that a data field value will be trimmed when parsed and serialized.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class TrimValueAttribute : Attribute
{
    /// <summary>
    /// Gets the leading and trailing characters that will be trimmed from the string value.
    /// Default is to white-space characters.
    /// </summary>
    public char[]? TrimChars { get; }

    /// <summary>
    /// Gets or sets a value indicating whether gets or sets if the property will trim on serialization.
    /// </summary>
    public bool OnSerialize { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether gets or sets if the property will trim when parsing.
    /// </summary>
    public bool OnParse { get; set; } = true;

    /// <summary>
    /// Initializes a new instance of the <see cref="TrimValueAttribute"/> class.
    /// </summary>
    public TrimValueAttribute() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="TrimValueAttribute"/> class.
    /// </summary>
    /// <param name="trimChars">The leading and trailing characters that will be trimmed from the string value.</param>
    public TrimValueAttribute(params char[] trimChars)
    {
        this.TrimChars = trimChars;
    }
}
