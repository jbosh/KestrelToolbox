// <copyright file="DateOnlyParseSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies parser and serializer parameters for <see cref="DateOnly"/>.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
public class DateOnlyParseSettingsAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the expected format for parsing and serializing. Setting this value will cause parser to use
    /// <see cref="DateOnly.ParseExact(string, string)"/>.
    /// </summary>
    /// <remarks>
    /// The format for this string can be found at https://learn.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings.
    /// </remarks>
    public string? Format { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="DateOnlyParseSettingsAttribute"/> class.
    /// </summary>
    public DateOnlyParseSettingsAttribute() { }
}
