// <copyright file="BoolSerializableSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Type of serialization that a <see cref="BoolSerializableSettingsAttribute"/> uses.
/// </summary>
public enum BoolSerializableSettingsSerializationType
{
    /// <summary>
    /// Defaults to serializing boolean values as json bools. (true, false).
    /// </summary>
    Default,

    /// <summary>
    /// Defaults to serializing boolean values as numbers. (1, 0).
    /// </summary>
    Numbers,
}

/// <summary>
/// Attribute used to specify settings for serializing booleans.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public class BoolSerializableSettingsAttribute : Attribute
{
    /// <summary>
    /// Gets or sets a value indicating whether the bool can be parsed from values that are mostly correct.
    /// Examples would be <c>True</c>, <c>FALSE</c> or properties with whitespace in them.
    /// </summary>
    public bool AllowFlexibleStrings { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the bool can be parsed from '0' and '1'.
    /// </summary>
    public bool AllowNumbers { get; set; }

    /// <summary>
    /// Gets or sets the type of serialization that will occur.
    /// </summary>
    public BoolSerializableSettingsSerializationType SerializationType { get; set; }
}
