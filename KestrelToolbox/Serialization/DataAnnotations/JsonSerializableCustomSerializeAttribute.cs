// <copyright file="JsonSerializableCustomSerializeAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies a method as custom json serialization. It must be of the format <c>static void Serialize(Utf8JsonWriter writer, T value)</c>.
/// /// Value should not be nullable if it is a struct but should be nullable if it is a class.
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class JsonSerializableCustomSerializeAttribute : Attribute { }
