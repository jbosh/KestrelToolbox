// <copyright file="StringLengthAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies the minimum and maximum length of characters that are allowed in a data field.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
public class StringLengthAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the minimum length of a string.
    /// </summary>
    public int Minimum { get; set; }

    /// <summary>
    /// Gets or sets the maximum length of a string.
    /// </summary>
    /// <remarks>The default is <see cref="int.MaxValue"/>.</remarks>
    public int Maximum { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="StringLengthAttribute"/> class.
    /// </summary>
    public StringLengthAttribute()
    {
        this.Minimum = 0;
        this.Maximum = int.MaxValue;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="StringLengthAttribute"/> class by using a specified maximum length.
    /// </summary>
    /// <param name="maximumLength">The maximum length of a valid string.</param>
    public StringLengthAttribute(int maximumLength)
        : this(0, maximumLength) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="StringLengthAttribute"/> class by using a specified minimum and maximum length.
    /// </summary>
    /// <param name="minimum">The minimum length of a valid string.</param>
    /// <param name="maximum">The maximum length of a valid string.</param>
    public StringLengthAttribute(int minimum, int maximum)
    {
        this.Minimum = minimum;
        this.Maximum = maximum;
    }
}
