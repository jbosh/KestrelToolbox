// <copyright file="JsonSerializableCustomPropertyAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies a property has custom json parsing and/or serializing.
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class JsonSerializableCustomPropertyAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the name of the method used to parse this property.
    /// </summary>
    /// <remarks>
    /// This method should be fully qualified because it will be inserted verbatim.
    /// </remarks>
    public string? ParseMethod { get; set; }

    /// <summary>
    /// Gets or sets the name of the method used to serialize this property.
    /// </summary>
    /// <remarks>
    /// This method should be fully qualified because it will be inserted verbatim.
    /// </remarks>
    public string? SerializeMethod { get; set; }
}
