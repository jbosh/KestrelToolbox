// <copyright file="DescriptionAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies a description for a property or event.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class DescriptionAttribute : Attribute
{
    /// <summary>
    /// Gets the description stored in this attribute.
    /// </summary>
    public string Description { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="DescriptionAttribute"/> class with a description.
    /// </summary>
    /// <param name="description">The description text.</param>
    public DescriptionAttribute(string description)
    {
        this.Description = description;
    }
}
