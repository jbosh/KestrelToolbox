// <copyright file="EmptyArgumentValueAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Globalization;

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies the command line parameter may be null and the default value for when an argument is unspecified. A useful scenario is for flags.
///
/// <code>
/// [Name("-enabled")]<br/>
/// [EmptyArgumentValue(true)]<br/>
/// public bool Enabled { get; set; }<br/>
/// </code>
/// <br/>
/// On the command line you would specify `-enabled` and it would be set to true.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
public class EmptyArgumentValueAttribute : Attribute
{
    /// <summary>
    /// Gets the value of the property this attribute is bound to.
    /// </summary>
    public object? Value { get; }

    /// <inheritdoc/>
    public override string ToString() => $"EmptyArgumentValue({this.Value})";

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(bool value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(byte value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(ushort value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(uint value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(ulong value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(sbyte value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(short value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(int value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(long value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(char value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(string? value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(object? value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmptyArgumentValueAttribute"/> class.
    /// </summary>
    /// <param name="type">The type of object to convert to.</param>
    /// <param name="value">The default value.</param>
    public EmptyArgumentValueAttribute(Type type, string? value)
    {
        this.Value =
            value == null
                ? null
                : type.FullName switch
                {
                    "System.TimeOnly" => TimeOnly.Parse(value, CultureInfo.InvariantCulture),
                    "System.TimeSpan" => TimeSpanParser.Parse(value),
                    "System.DateOnly" => DateOnly.Parse(value, CultureInfo.InvariantCulture),
                    "System.DateTime" => DateTime.Parse(value, CultureInfo.InvariantCulture),
                    "System.DateTimeOffset" => DateTimeOffset.Parse(value, CultureInfo.InvariantCulture),
                    "System.Decimal" => decimal.Parse(value, CultureInfo.InvariantCulture),
                    _ => throw new NotImplementedException(
                        $"Not implemented type {type.FullName} used with EmptyArgumentValue."
                    ),
                };
    }
}
