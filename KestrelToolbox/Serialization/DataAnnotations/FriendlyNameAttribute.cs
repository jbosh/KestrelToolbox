// <copyright file="FriendlyNameAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Friendly name for a structure when printing errors. Is not used for other purposes.
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
public class FriendlyNameAttribute : Attribute
{
    /// <summary>
    /// Gets the name that this structure can be referred to by.
    /// </summary>
    public string Name { get; }

    /// <inheritdoc />
    public override string ToString() => this.Name;

    /// <summary>
    /// Initializes a new instance of the <see cref="FriendlyNameAttribute"/> class.
    /// </summary>
    /// <param name="name">Name that this structure can be referred to in error messaging.</param>
    public FriendlyNameAttribute(string name)
    {
        this.Name = name;
    }
}
