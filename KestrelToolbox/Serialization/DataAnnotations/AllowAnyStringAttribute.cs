// <copyright file="AllowAnyStringAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Attribute for a string field that can accept any basic json type (bool, number, object, array).
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public class AllowAnyStringAttribute : Attribute { }
