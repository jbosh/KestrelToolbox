// <copyright file="EmailAddressAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Net.Mail;

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies a property should be an email address.
/// </summary>
/// <remarks>
/// Validation occurs through System.Net.Mail.MailAddress parsing. This does not validate domain names have MX records or that the email address exists in the wild.
/// It should be used as a quick test if an email address could possibly be valid.
/// </remarks>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class EmailAddressAttribute : Attribute
{
    /// <summary>
    /// Checks to see if an email address is valid through regular expressions.
    /// </summary>
    /// <param name="value">The email address to validate.</param>
    /// <returns>True if the email address is considered valid, false if not.</returns>
    /// <remarks>
    /// This is not a sure-fire way to validate email addresses. It is only a quick check to see if the email address
    /// matches RFC5322.
    /// </remarks>
    public static bool IsValid(string value)
    {
        if (!MailAddress.TryCreate(value, out var address))
        {
            return false;
        }

        return string.IsNullOrEmpty(address.DisplayName);
    }
}
