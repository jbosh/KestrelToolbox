// <copyright file="ValidRangeAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Globalization;

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies the numeric range constraints for the value of a data field.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
public class ValidRangeAttribute : Attribute
{
    /// <summary>
    /// Gets minimum value for the data field.
    /// </summary>
    public object Minimum { get; }

    /// <summary>
    /// Gets maximum value for data field.
    /// </summary>
    public object Maximum { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="minimum">Specifies the minimum value allowed for the data field value.</param>
    /// <param name="maximum">Specifies the maximum value allowed for the data field value.</param>
    public ValidRangeAttribute(uint minimum, uint maximum)
    {
        this.Minimum = minimum;
        this.Maximum = maximum;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="minimum">Specifies the minimum value allowed for the data field value.</param>
    /// <param name="maximum">Specifies the maximum value allowed for the data field value.</param>
    public ValidRangeAttribute(int minimum, int maximum)
    {
        this.Minimum = minimum;
        this.Maximum = maximum;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="minimum">Specifies the minimum value allowed for the data field value.</param>
    /// <param name="maximum">Specifies the maximum value allowed for the data field value.</param>
    public ValidRangeAttribute(double minimum, double maximum)
    {
        this.Minimum = minimum;
        this.Maximum = maximum;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="minimum">Specifies the minimum value allowed for the data field value.</param>
    /// <param name="maximum">Specifies the maximum value allowed for the data field value.</param>
    public ValidRangeAttribute(ulong minimum, ulong maximum)
    {
        this.Minimum = minimum;
        this.Maximum = maximum;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="minimum">Specifies the minimum value allowed for the data field value.</param>
    /// <param name="maximum">Specifies the maximum value allowed for the data field value.</param>
    public ValidRangeAttribute(long minimum, long maximum)
    {
        this.Minimum = minimum;
        this.Maximum = maximum;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidRangeAttribute"/> class.
    /// </summary>
    /// <param name="type">Specifies the type of the object to test.</param>
    /// <param name="minimum">Specifies the minimum value allowed for the data field value.</param>
    /// <param name="maximum">Specifies the maximum value allowed for the data field value.</param>
    public ValidRangeAttribute(Type type, string minimum, string maximum)
    {
        this.Minimum = Convert.ChangeType(minimum, type, CultureInfo.InvariantCulture);
        this.Maximum = Convert.ChangeType(maximum, type, CultureInfo.InvariantCulture);
    }
}
