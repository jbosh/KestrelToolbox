// <copyright file="RequiredAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies that a data field value is required.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class RequiredAttribute : Attribute
{
    /// <summary>
    /// Gets or sets a value indicating whether only whitespace is allowed. If white space only
    /// is not allowed, the string <c>"\t "</c> would trigger an error. This is only valid for strings. Default is <c>true</c>.
    /// </summary>
    public bool AllowOnlyWhiteSpace { get; set; } = true;
}
