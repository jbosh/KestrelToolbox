// <copyright file="ValidValuesAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Immutable;
using System.Globalization;

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies valid value constraints for the value of a data field. This only works with integers and strings.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class ValidValuesAttribute : Attribute
{
    /// <summary>
    /// Gets the set of valid string values.
    /// </summary>
    public ImmutableHashSet<string> Values { get; }

    /// <summary>
    /// Gets the set of valid values as integers. This value will be null if the attribute was not initialized using integers.
    /// </summary>
    public ImmutableHashSet<long>? ValuesAsInts { get; }

    /// <inheritdoc/>
    public override string ToString()
    {
        if (this.ValuesAsInts != null)
        {
            return $"Valid Values: {string.Join(", ", this.ValuesAsInts.OrderBy(v => v))}";
        }
        else
        {
            return $"Valid Values: {string.Join(", ", this.Values.OrderBy(v => v))}";
        }
    }

    /// <summary>
    /// Checks to see if a string value is valid.
    /// </summary>
    /// <param name="value">String value to check.</param>
    /// <returns>True if the value is valid, false if not.</returns>
    public bool Contains(string value) => this.Values.Contains(value);

    /// <summary>
    /// Checks to see if an integer value is valid.
    /// </summary>
    /// <param name="value">String value to check.</param>
    /// <returns>True if the value is valid, false if not.</returns>
    public bool Contains(long value) => this.ValuesAsInts?.Contains(value) ?? false;

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidValuesAttribute"/> class.
    /// </summary>
    /// <param name="values">A case-sensitive array of strings that are valid.</param>
    public ValidValuesAttribute(params string[] values)
    {
        this.Values = ImmutableHashSet.CreateRange(values);
        this.ValuesAsInts = null;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidValuesAttribute"/> class.
    /// </summary>
    /// <param name="values">An array of integers that are valid.</param>
    public ValidValuesAttribute(params long[] values)
    {
        this.Values = ImmutableHashSet.CreateRange(values.Select(v => v.ToString(CultureInfo.InvariantCulture)));
        this.ValuesAsInts = ImmutableHashSet.CreateRange(values);
    }
}
