// <copyright file="PositionalArgumentAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies the data field is required for command line parsing any excess arguments.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
public class PositionalArgumentAttribute : Attribute
{
    /// <summary>
    /// Gets name to output when printing help.
    /// </summary>
    public string Name { get; }

    /// <inheritdoc/>
    public override string ToString() => $"PositionalArg({this.Name})";

    /// <summary>
    /// Initializes a new instance of the <see cref="PositionalArgumentAttribute"/> class.
    /// </summary>
    /// <param name="name">The output name when printing help.</param>
    public PositionalArgumentAttribute(string name)
    {
        this.Name = name;
    }
}
