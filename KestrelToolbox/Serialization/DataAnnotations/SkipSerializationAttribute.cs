// <copyright file="SkipSerializationAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using KestrelToolbox.Serialization.DataAnnotations.Serialization;

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies a property has custom json serialization. This is useful for skipping serialization if a field has a particular
/// value.
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
[SuppressMessage(
    "Info Code Smell",
    "S1133:Deprecated code should be removed",
    Justification = "Backward compatibility of public library."
)]
public class SkipSerializationAttribute : Attribute
{
    /// <summary>
    /// Gets the conditions under which this property will skip serialization.
    /// </summary>
    public Condition SerializationCondition { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="SkipSerializationAttribute"/> class.
    /// </summary>
    /// <param name="condition">Conditions under which this property will skip serialization.</param>
    public SkipSerializationAttribute(Condition condition)
    {
        this.SerializationCondition = condition;
    }
}
