// <copyright file="EnumSerializableSettingsAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Type of serialization that a <see cref="EnumSerializableSettingsAttribute"/> uses.
/// </summary>
public enum EnumSerializableSettingsSerializationType
{
    /// <summary>
    /// Defaults to serializing enum values as strings from <see cref="NameAttribute"/>.
    /// </summary>
    Default,

    /// <summary>
    /// Defaults to serializing enum values as numbers.
    /// </summary>
    Numbers,
}

/// <summary>
/// Attribute used to specify settings for serializing and parsing enums.
/// </summary>
[AttributeUsage(AttributeTargets.Enum)]
public class EnumSerializableSettingsAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the value of parsing and serializing for invalid values. This should
    /// be the value of the enum directly. This value is ignored if <see cref="SerializationType"/> is
    /// set to use numbers.
    /// </summary>
    public object? InvalidValue { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether not any number is allowed for deserialization.
    /// </summary>
    public bool AllowAnyNumber { get; set; } = true;

    /// <summary>
    /// Gets or sets the type of serialization and deserialization that will occur.
    /// </summary>
    public EnumSerializableSettingsSerializationType SerializationType { get; set; }
}
