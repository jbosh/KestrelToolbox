// <copyright file="DefaultValueAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies the default value for a property.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class DefaultValueAttribute : Attribute
{
    /// <summary>
    /// Gets the default value of the property this attribute is bound to.
    /// </summary>
    public object Value { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(bool value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(byte value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(double value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(short value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(int value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(long value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(object value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(sbyte value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(float value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(string value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(ushort value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(uint value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(ulong value) => this.Value = value;

    /// <summary>
    /// Initializes a new instance of the <see cref="DefaultValueAttribute"/> class.
    /// </summary>
    /// <param name="type">The type to convert <paramref name="value"/> to.</param>
    /// <param name="value">The default value.</param>
    public DefaultValueAttribute(Type type, string value)
    {
        var convertedValue = System.ComponentModel.TypeDescriptor.GetConverter(type).ConvertFromInvariantString(value);
        this.Value =
            convertedValue ?? throw new ArgumentException($"Conversion from {value} to {type.Name} cannot be null.");
    }
}
