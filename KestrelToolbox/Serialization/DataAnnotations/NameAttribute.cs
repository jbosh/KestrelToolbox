// <copyright file="NameAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization.DataAnnotations;

/// <summary>
/// Specifies the name or names that a data field can be referred to as.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
public class NameAttribute : Attribute
{
    /// <summary>
    /// Gets the list of names that a data field can be.
    /// </summary>
    public IReadOnlyList<string> Names { get; }

    /// <inheritdoc />
    public override string ToString() => $"Name: {string.Join(", ", this.Names)}";

    /// <summary>
    /// Initializes a new instance of the <see cref="NameAttribute"/> class.
    /// </summary>
    /// <param name="name">Name of the thing.</param>
    public NameAttribute(string name)
    {
        this.Names = new[] { name };
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="NameAttribute"/> class.
    /// </summary>
    /// <param name="names">Array of case-sensitive names that the data field can be referred to by.</param>
    public NameAttribute(params string[] names)
    {
        this.Names = names;
    }
}
