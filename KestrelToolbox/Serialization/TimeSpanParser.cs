// <copyright file="TimeSpanParser.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.Buffers.Text;
using KestrelToolbox.Extensions.StringExtensions;

namespace KestrelToolbox.Serialization;

/// <summary>
/// Helper class for parsing <see cref="TimeSpan"/> from more complicated input.
/// </summary>
public static class TimeSpanParser
{
    /// <summary>
    /// Parses the timespan in the format of 0.0 parses into seconds. 1:23 parses to 1 hour, 23 minutes.
    /// </summary>
    /// <param name="input">Formatted timespan.</param>
    /// <returns>An object that is the TimeSpan value coming from <paramref name="input"/>.</returns>
    /// <remarks>
    /// Any times that have a suffix will be parsed as a floating point value.
    /// Valid suffixes (case insensitive).
    /// <ul>
    /// <li>s - minute</li>
    /// <li>m - minute</li>
    /// <li>h - hour</li>
    /// <li>d - day</li>
    /// </ul>
    /// <ul>
    /// <li>ns - nanosecond</li>
    /// <li>ms - millisecond</li>
    /// <li>min - minute</li>
    /// <li>hr - hour</li>
    /// <li>yr - year</li>
    /// </ul>
    /// </remarks>
    public static TimeSpan Parse(string input)
    {
        ArgumentNullException.ThrowIfNull(input);

        if (!TryParse(input, out var result))
        {
            throw new FormatException($"{nameof(input)} does not contain a valid TimeSpan.");
        }

        return result;
    }

    /// <summary>
    /// Parses the timespan in the format of 0.0 parses into seconds. 1:23 parses to 1 hour, 23 minutes.
    /// </summary>
    /// <param name="utf8Input">Formatted timespan.</param>
    /// <returns>An object that is the TimeSpan value coming from <paramref name="utf8Input"/>.</returns>
    /// <exception cref="FormatException"><paramref name="utf8Input"/> was not a valid timespan.</exception>
    /// <remarks>
    /// Any times that have a suffix will be parsed as a floating point value.
    /// Valid suffixes (case insensitive).
    /// <ul>
    /// <li>s - minute</li>
    /// <li>m - minute</li>
    /// <li>h - hour</li>
    /// <li>d - day</li>
    /// </ul>
    /// <ul>
    /// <li>ns - nanosecond</li>
    /// <li>ms - millisecond</li>
    /// <li>min - minute</li>
    /// <li>hr - hour</li>
    /// <li>yr - year</li>
    /// </ul>
    /// </remarks>
    public static TimeSpan Parse(ReadOnlySpan<byte> utf8Input)
    {
        if (!TryParse(utf8Input, out var result))
        {
            throw new FormatException($"{nameof(utf8Input)} does not contain a valid TimeSpan.");
        }

        return result;
    }

    /// <summary>
    /// Tries to parse timespan in the format of 0.0 parses into seconds. 1:23 parses to 1 hour, 23 minutes.
    /// </summary>
    /// <param name="input">Formatted timespan.</param>
    /// <param name="result">Result of the parsing.</param>
    /// <returns>True if parsing is successful, false otherwise.</returns>
    /// <remarks>
    /// Any times that have a suffix will be parsed as a floating point value.
    /// Valid suffixes.
    /// <ul>
    /// <li>s - minute</li>
    /// <li>m - minute</li>
    /// <li>h - hour</li>
    /// <li>d - day</li>
    /// </ul>
    /// <ul>
    /// <li>ns - nanosecond</li>
    /// <li>ms - millisecond</li>
    /// <li>min - minute</li>
    /// <li>hr - hour</li>
    /// <li>yr - year</li>
    /// </ul>
    /// </remarks>
    public static bool TryParse(string? input, out TimeSpan result)
    {
        result = TimeSpan.Zero;

        if (string.IsNullOrEmpty(input))
        {
            return false;
        }

        if (IsLetter(input[^1]))
        {
            // has a suffix, we should just have a double value
            var suffixIndex = input.Length - 1;
            for (; suffixIndex >= 0; suffixIndex--)
            {
                if (!IsLetter(input[suffixIndex]))
                {
                    break;
                }
            }

            suffixIndex++;

            if (suffixIndex <= 0)
            {
                return false;
            }

            var suffix = input.AsSpan(suffixIndex);
            var number = input.AsSpan(0, suffixIndex);
            if (!double.TryParse(number, out var d))
            {
                return false;
            }

            switch (suffix)
            {
                case "y":
                case "yr":
                case "yrs":
                case "years":
                {
                    result = TimeSpan.FromDays(d * 365.0);
                    break;
                }

                case "d":
                case "day":
                case "days":
                {
                    result = TimeSpan.FromDays(d);
                    break;
                }

                case "h":
                case "hr":
                case "hrs":
                {
                    result = TimeSpan.FromHours(d);
                    break;
                }

                case "m":
                case "min":
                {
                    result = TimeSpan.FromMinutes(d);
                    break;
                }

                case "s":
                case "sec":
                {
                    result = TimeSpan.FromSeconds(d);
                    break;
                }

                case "ms":
                {
                    result = TimeSpan.FromMilliseconds(d);
                    break;
                }

                case "us":
                {
                    result = TimeSpan.FromMicroseconds(d);
                    break;
                }

                case "ns":
                {
                    result = TimeSpan.FromMicroseconds(d / 1000.0);
                    break;
                }

                default:
                {
                    return false;
                }
            }
        }
        else
        {
            var enumerator = input.SplitAndEnumerate(':', 3);

            if (!enumerator.MoveNext())
            {
                // No input, this shouldn't happen in practice
                return false;
            }

            var hours = 0.0;
            var minutes = 0.0;
            if (!double.TryParse(enumerator.Current.Span, out var seconds))
            {
                return false;
            }

            if (enumerator.MoveNext())
            {
                hours = seconds;
                seconds = 0.0;
                if (!double.TryParse(enumerator.Current.Span, out minutes))
                {
                    return false;
                }

                if (enumerator.MoveNext())
                {
                    if (!double.TryParse(enumerator.Current.Span, out seconds))
                    {
                        return false;
                    }

                    if (enumerator.MoveNext())
                    {
                        return false;
                    }
                }
            }

            var totalSeconds = (((hours * 60) + minutes) * 60) + seconds;
            var ticks = totalSeconds * TimeSpan.TicksPerSecond;
            result = new TimeSpan((long)ticks);
        }

        return true;
    }

    /// <summary>
    /// Tries to parse the timespan in the format of 0.0 parses into seconds. 1:23 parses to 1 hour, 23 minutes.
    /// </summary>
    /// <param name="utf8Input">Formatted timespan.</param>
    /// <param name="result">An object that is the TimeSpan value coming from <paramref name="utf8Input"/>.</param>
    /// <returns>True if the parsing was successful, false if not.</returns>
    /// <remarks>
    /// Any times that have a suffix will be parsed as a floating point value.
    /// Valid suffixes.
    /// <ul>
    /// <li>s - minute</li>
    /// <li>m - minute</li>
    /// <li>h - hour</li>
    /// <li>d - day</li>
    /// </ul>
    /// <ul>
    /// <li>ns - nanosecond</li>
    /// <li>ms - millisecond</li>
    /// <li>min - minute</li>
    /// <li>hr - hour</li>
    /// <li>yr - year</li>
    /// </ul>
    /// </remarks>
    public static bool TryParse(ReadOnlySequence<byte> utf8Input, out TimeSpan result)
    {
        if (utf8Input.IsSingleSegment)
        {
            return TryParse(utf8Input.FirstSpan, out result);
        }
        else
        {
            // Stack allocating and copying is super inefficient, but should hopefully not happen a ton in practice
            if (utf8Input.Length > 512)
            {
                result = default;
                return false;
            }

            Span<byte> sourceSpan = stackalloc byte[(int)utf8Input.Length];
            utf8Input.CopyTo(sourceSpan);

            return TryParse(sourceSpan, out result);
        }
    }

    /// <summary>
    /// Tries to parse timespan in the format of 0.0 parses into seconds. 1:23 parses to 1 hour, 23 minutes.
    /// </summary>
    /// <param name="utf8Input">Formatted timespan.</param>
    /// <param name="result">Result of the parsing.</param>
    /// <returns>True if parsing is successful, false otherwise.</returns>
    /// <remarks>
    /// Any times that have a suffix will be parsed as a floating point value.
    /// Valid suffixes.
    /// <ul>
    /// <li>s - minute</li>
    /// <li>m - minute</li>
    /// <li>h - hour</li>
    /// <li>d - day</li>
    /// </ul>
    /// <ul>
    /// <li>ns - nanosecond</li>
    /// <li>ms - millisecond</li>
    /// <li>min - minute</li>
    /// <li>hr - hour</li>
    /// <li>yr - year</li>
    /// </ul>
    /// </remarks>
    public static bool TryParse(ReadOnlySpan<byte> utf8Input, out TimeSpan result)
    {
        result = TimeSpan.Zero;

        if (utf8Input.Length == 0)
        {
            return false;
        }

        if (IsLetter(utf8Input[^1]))
        {
            // has a suffix, we should just have a double value
            var suffixIndex = utf8Input.Length - 1;
            for (; suffixIndex >= 0; suffixIndex--)
            {
                if (!IsLetter(utf8Input[suffixIndex]))
                {
                    break;
                }
            }

            suffixIndex++;

            if (suffixIndex <= 0)
            {
                return false;
            }

            var suffixSpan = utf8Input.Slice(suffixIndex);
            var number = utf8Input.Slice(0, suffixIndex);
            if (!Utf8Parser.TryParse(number, out double d, out _))
            {
                return false;
            }

            Span<char> suffix = stackalloc char[suffixSpan.Length];
            for (var i = 0; i < suffix.Length; i++)
            {
                suffix[i] = (char)suffixSpan[i];
            }

            switch (suffix)
            {
                case "y":
                case "yr":
                case "yrs":
                case "years":
                {
                    result = TimeSpan.FromDays(d * 365.0);
                    break;
                }

                case "d":
                case "day":
                case "days":
                {
                    result = TimeSpan.FromDays(d);
                    break;
                }

                case "h":
                case "hr":
                case "hrs":
                {
                    result = TimeSpan.FromHours(d);
                    break;
                }

                case "m":
                case "min":
                {
                    result = TimeSpan.FromMinutes(d);
                    break;
                }

                case "s":
                case "sec":
                {
                    result = TimeSpan.FromSeconds(d);
                    break;
                }

                case "ms":
                {
                    result = TimeSpan.FromMilliseconds(d);
                    break;
                }

                case "us":
                {
                    result = TimeSpan.FromMicroseconds(d);
                    break;
                }

                case "ns":
                {
                    result = TimeSpan.FromMicroseconds(d / 1000.0);
                    break;
                }

                default:
                {
                    return false;
                }
            }
        }
        else
        {
            var iterator = utf8Input.IndexOf((byte)':');
            switch (iterator)
            {
                case 0:
                {
                    return false;
                }

                case -1:
                {
                    if (
                        !Utf8Parser.TryParse(utf8Input, out double seconds, out var bytesRead)
                        || bytesRead != utf8Input.Length
                    )
                    {
                        return false;
                    }

                    result = TimeSpan.FromSeconds(seconds);
                    return true;
                }

                default:
                {
                    var hours = 0.0;
                    var minutes = 0.0;
                    if (
                        !Utf8Parser.TryParse(utf8Input.Slice(0, iterator), out double seconds, out var bytesRead)
                        || bytesRead != iterator
                    )
                    {
                        return false;
                    }

                    utf8Input = utf8Input.Slice(iterator + 1);

                    if (utf8Input.Length > 0)
                    {
                        hours = seconds;
                        seconds = 0.0;
                        iterator = utf8Input.IndexOf((byte)':');
                        switch (iterator)
                        {
                            case 0:
                                return false;
                            case -1:
                            {
                                if (!Utf8Parser.TryParse(utf8Input, out minutes, out _))
                                {
                                    return false;
                                }

                                break;
                            }

                            default:
                            {
                                if (
                                    !Utf8Parser.TryParse(utf8Input.Slice(0, iterator), out minutes, out bytesRead)
                                    || bytesRead != iterator
                                )
                                {
                                    return false;
                                }

                                utf8Input = utf8Input.Slice(iterator + 1);

                                if (utf8Input.Length > 0)
                                {
                                    iterator = utf8Input.IndexOf((byte)':');
                                    if (iterator != -1)
                                    {
                                        return false;
                                    }

                                    if (
                                        !Utf8Parser.TryParse(utf8Input, out seconds, out bytesRead)
                                        || bytesRead != utf8Input.Length
                                    )
                                    {
                                        return false;
                                    }
                                }

                                break;
                            }
                        }
                    }

                    var totalSeconds = (((hours * 60) + minutes) * 60) + seconds;
                    var ticks = totalSeconds * TimeSpan.TicksPerSecond;
                    result = new TimeSpan((long)ticks);
                    break;
                }
            }
        }

        return true;
    }

    private static bool IsLetter(char c) => c is (>= 'a' and <= 'z') or (>= 'A' and <= 'Z');

    private static bool IsLetter(byte c) => (char)c is (>= 'a' and <= 'z') or (>= 'A' and <= 'Z');
}
