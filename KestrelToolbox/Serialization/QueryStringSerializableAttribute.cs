// <copyright file="QueryStringSerializableAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Serialization.DataAnnotations.Serialization;

namespace KestrelToolbox.Serialization;

/// <summary>
/// Specifies that a class will have AOT generated enum parsing and serialization added.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class QueryStringSerializableAttribute : Attribute
{
    /// <summary>
    /// Gets the types of struct/class this attribute will allow serialization of.
    /// </summary>
    public Type[] Types { get; }

    /// <summary>
    /// Gets or sets the source generation mode for this enum. Default is both serialize and deserialize support.
    /// </summary>
    public SourceGenerationMode SourceGenerationMode { get; set; } = SourceGenerationMode.Default;

    /// <summary>
    /// Gets or sets the method name to use in generated parsing code. Defaults to <c>TryParse</c>.
    /// </summary>
    public string ParsingMethodName { get; set; } = "TryParseQueryString";

    /// <summary>
    /// Initializes a new instance of the <see cref="QueryStringSerializableAttribute"/> class.
    /// </summary>
    /// <param name="types">The types to add for serialization.</param>
    /// <remarks>
    /// If no types are specified, the class that this attribute is attached to will be the only type.
    /// </remarks>
    public QueryStringSerializableAttribute(params Type[] types)
    {
        this.Types = types;
    }
}
