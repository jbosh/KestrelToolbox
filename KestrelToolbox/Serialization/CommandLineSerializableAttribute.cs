// <copyright file="CommandLineSerializableAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Serialization;

/// <summary>
/// Specifies that a class will have AOT generated command line parsing.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class CommandLineSerializableAttribute : Attribute
{
    /// <summary>
    /// Gets the types of enums this attribute will allow serialization of.
    /// </summary>
    public Type[] Types { get; }

    /// <summary>
    /// Gets or sets the method name to use in generated parsing code. Defaults to <c>TryParse</c>.
    /// </summary>
    public string ParsingMethodName { get; set; } = "TryParseCommandLine";

    /// <summary>
    /// Gets or sets a method name that is used for parsing (instead of generating).
    /// </summary>
    public string? CustomParsingMethodName { get; set; }

    /// <summary>
    /// Gets or sets the method name to use for generating print code. Defaults to <c>PrintHelp</c>.
    /// </summary>
    /// <remarks>
    /// If this value is <c>null</c> then help printing will not be generated.
    /// </remarks>
    public string? PrintHelpMethodName { get; set; } = "PrintHelp";

    /// <summary>
    /// Initializes a new instance of the <see cref="CommandLineSerializableAttribute"/> class.
    /// </summary>
    /// <param name="types">The types to add for serialization.</param>
    /// <remarks>
    /// If no types are specified, the class that this attribute is attached to will be the only type.
    /// </remarks>
    public CommandLineSerializableAttribute(params Type[] types)
    {
        this.Types = types;
    }
}
