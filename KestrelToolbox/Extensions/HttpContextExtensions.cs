// <copyright file="HttpContextExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.AspNetCore.Http;

namespace KestrelToolbox.Extensions;

/// <summary>
/// Extensions for HttpContext.
/// </summary>
public static class HttpContextExtensions
{
    /// <summary>
    /// Sets "Cache-Control" header on a response.
    /// </summary>
    /// <param name="response">Response to write header to.</param>
    /// <param name="duration">Amount of time this response will remain cached for.</param>
    /// <param name="isPrivate">If the cache control is private to the user or not.</param>
    public static void SetCacheControl(this HttpResponse response, TimeSpan duration, bool isPrivate = true)
    {
        response.Headers["Cache-Control"] = $"{(isPrivate ? "private" : "public")}, max-age={duration.TotalSeconds:0}";
    }

    /// <summary>
    /// Checks if a user needs to be sent the data or not. If a request is not stale, the user should respond with 304 - Not Modified.
    /// </summary>
    /// <param name="request">Request to check freshness for.</param>
    /// <param name="etag">Entity tag to test against.</param>
    /// <returns>True if the user needs to receive the data again, false if the user already has it.</returns>
    public static bool IsStale(this HttpRequest request, string etag)
    {
        if (!request.Headers.TryGetValue("If-None-Match", out var ifNoneMatchValues))
        {
            return true;
        }

        var ifNoneMatch = (string?)ifNoneMatchValues;

        // Strip the leading and trailing quotes if they exist.
        if (ifNoneMatch != null && ifNoneMatch.Length > 2 && ifNoneMatch[0] == '"' && ifNoneMatch[^1] == '"')
        {
            ifNoneMatch = ifNoneMatch.Substring(1, ifNoneMatch.Length - 2);
        }

        return ifNoneMatch != etag;
    }
}
