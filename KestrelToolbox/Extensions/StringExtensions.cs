// <copyright file="StringExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections;

namespace KestrelToolbox.Extensions.StringExtensions;

/// <summary>
/// Extension for strings.
/// </summary>
public static class StringExtensions
{
    /// <summary>
    /// Allocation free way to split a string into substrings based on the provided character separator.
    /// </summary>
    /// <param name="s">The string to split.</param>
    /// <param name="separator">A character that delimits the substrings in this string.</param>
    /// <param name="options">
    /// One of the enumeration values that determines whether the split operation should
    /// omit empty substrings from the return value.
    /// </param>
    /// <returns>
    /// Enumerator whose elements contain the substrings from this instance that are delimited
    /// by separator.
    /// </returns>
    public static StringSplitEnumerator SplitAndEnumerate(
        this string s,
        char separator,
        StringSplitOptions options = StringSplitOptions.None
    ) => s.SplitAndEnumerate(separator, int.MaxValue, options);

    /// <summary>
    /// Allocation free way to split a string into substrings based on the provided character separator.
    /// </summary>
    /// <param name="s">The string to split.</param>
    /// <param name="separator">A character that delimits the substrings in this string.</param>
    /// <param name="count">The maximum number of substrings to return.</param>
    /// <param name="options">
    /// One of the enumeration values that determines whether the split operation should
    /// omit empty substrings from the return value.
    /// </param>
    /// <returns>
    /// Enumerator whose elements contain the substrings from this instance that are delimited
    /// by separator.
    /// </returns>
    public static StringSplitEnumerator SplitAndEnumerate(
        this string s,
        char separator,
        int count,
        StringSplitOptions options = StringSplitOptions.None
    ) => new(s.AsMemory(), separator, count, options);

    /// <summary>
    /// Allocation free way to split a string into substrings based on the provided character separator.
    /// </summary>
    /// <param name="s">The string to split.</param>
    /// <param name="separator">A character array that delimits the substrings in this string.</param>
    /// <returns>
    /// Enumerator whose elements contain the substrings from this instance that are delimited
    /// by separator.
    /// </returns>
    public static StringSplitArrayEnumerator SplitAndEnumerate(this string s, params char[] separator) =>
        s.SplitAndEnumerate(separator, int.MaxValue);

    /// <summary>
    /// Allocation free way to split a string into substrings based on the provided character separator.
    /// </summary>
    /// <param name="s">The string to split.</param>
    /// <param name="separator">A character array that delimits the substrings in this string.</param>
    /// <param name="options">
    /// One of the enumeration values that determines whether the split operation should
    /// omit empty substrings from the return value.
    /// </param>
    /// <returns>
    /// Enumerator whose elements contain the substrings from this instance that are delimited
    /// by separator.
    /// </returns>
    public static StringSplitArrayEnumerator SplitAndEnumerate(
        this string s,
        char[] separator,
        StringSplitOptions options
    ) => s.SplitAndEnumerate(separator, int.MaxValue, options);

    /// <summary>
    /// Allocation free way to split a string into substrings based on the provided character separator.
    /// </summary>
    /// <param name="s">The string to split.</param>
    /// <param name="separator">A character array that delimits the substrings in this string.</param>
    /// <param name="count">The maximum number of substrings to return.</param>
    /// <param name="options">
    /// One of the enumeration values that determines whether the split operation should
    /// omit empty substrings from the return value.
    /// </param>
    /// <returns>
    /// Enumerator whose elements contain the substrings from this instance that are delimited
    /// by separator.
    /// </returns>
    public static StringSplitArrayEnumerator SplitAndEnumerate(
        this string s,
        char[] separator,
        int count,
        StringSplitOptions options = StringSplitOptions.None
    ) => new(s.AsMemory(), separator, count, options);

    /// <summary>
    /// Enumerator for string splits.
    /// </summary>
    public struct StringSplitEnumerator : IEnumerable<ReadOnlyMemory<char>>, IEnumerator<ReadOnlyMemory<char>>
    {
        private readonly ReadOnlyMemory<char> memory;
        private readonly char separator;
        private readonly StringSplitOptions options;
        private readonly int maxCount;
        private int offset;
        private int count;

        /// <inheritdoc />
        public ReadOnlyMemory<char> Current { get; private set; }

        /// <inheritdoc />
        object IEnumerator.Current => this.Current;

        /// <inheritdoc />
        IEnumerator<ReadOnlyMemory<char>> IEnumerable<ReadOnlyMemory<char>>.GetEnumerator() => this;

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator() => this;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringSplitEnumerator"/> struct.
        /// </summary>
        /// <param name="memory">Memory for string to split.</param>
        /// <param name="separator">Character to separate the string by.</param>
        /// <param name="count">Maximum number of split points to make. <see cref="int.MaxValue"/> can be used when you want all of them.</param>
        /// <param name="options">Options on how to split the string.</param>
        public StringSplitEnumerator(
            ReadOnlyMemory<char> memory,
            char separator,
            int count,
            StringSplitOptions options = StringSplitOptions.None
        )
        {
            this.memory = memory;
            this.offset = 0;
            this.count = 0;
            this.maxCount = count;
            this.separator = separator;
            this.options = options;
            this.Current = default;
        }

        /// <inheritdoc />
        public void Reset()
        {
            this.offset = 0;
            this.count = 0;
            this.Current = ReadOnlyMemory<char>.Empty;
        }

        /// <inheritdoc />
        public bool MoveNext()
        {
            while (true)
            {
                var memory = this.memory.Slice(this.offset);
                if (memory.Length == 0 || this.count >= this.maxCount)
                {
                    if (this.count == 0 && this.maxCount != 0)
                    {
                        this.count++;
                        return this.options != StringSplitOptions.RemoveEmptyEntries;
                    }

                    return false;
                }

                this.count++;
                if (this.count >= this.maxCount)
                {
                    this.Current = memory;
                    this.offset = this.memory.Length;
                    return true;
                }

                var index = memory.Span.IndexOf(this.separator);

                // no more strings
                if (index == -1)
                {
                    this.offset = this.memory.Length;
                    this.Current = memory;
                    return true;
                }

                if (index == 0 && this.options == StringSplitOptions.RemoveEmptyEntries)
                {
                    this.offset++;
                    continue;
                }

                this.Current = memory.Slice(0, index);
                this.offset += index + 1;
                return true;
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            // This is empty because there's nothing to dispose.
        }
    }

    /// <summary>
    /// Enumerator for string splits with multiple characters.
    /// </summary>
    public struct StringSplitArrayEnumerator : IEnumerable<ReadOnlyMemory<char>>, IEnumerator<ReadOnlyMemory<char>>
    {
        private readonly ReadOnlyMemory<char> memory;
        private readonly char[] separators;
        private readonly StringSplitOptions options;
        private readonly int maxCount;
        private int offset;
        private int count;

        /// <inheritdoc />
        public ReadOnlyMemory<char> Current { get; private set; }

        /// <inheritdoc />
        object IEnumerator.Current => this.Current;

        /// <inheritdoc />
        IEnumerator<ReadOnlyMemory<char>> IEnumerable<ReadOnlyMemory<char>>.GetEnumerator() => this;

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator() => this;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringSplitArrayEnumerator"/> struct.
        /// </summary>
        /// <param name="memory">Memory for string to split.</param>
        /// <param name="separators">Characters to separate the string by.</param>
        /// <param name="count">Maximum number of split points to make. <see cref="int.MaxValue"/> can be used when you want all of them.</param>
        /// <param name="options">Options on how to split the string.</param>
        public StringSplitArrayEnumerator(
            ReadOnlyMemory<char> memory,
            char[] separators,
            int count,
            StringSplitOptions options = StringSplitOptions.None
        )
        {
            this.memory = memory;
            this.offset = 0;
            this.count = 0;
            this.maxCount = count;
            this.separators = separators;
            this.options = options;
            this.Current = default;
        }

        /// <inheritdoc />
        public void Reset()
        {
            this.offset = 0;
            this.count = 1;
            this.Current = ReadOnlyMemory<char>.Empty;
        }

        /// <inheritdoc />
        public bool MoveNext()
        {
            while (true)
            {
                var memory = this.memory.Slice(this.offset);
                if (memory.Length == 0 || this.count >= this.maxCount)
                {
                    if (this.count == 0 && this.maxCount != 0)
                    {
                        this.count++;
                        return this.options != StringSplitOptions.RemoveEmptyEntries;
                    }

                    return false;
                }

                this.count++;
                if (this.count >= this.maxCount)
                {
                    this.Current = memory;
                    this.offset = this.memory.Length;
                    return true;
                }

                var index = memory.Span.IndexOfAny(this.separators);

                // no more strings
                if (index == -1)
                {
                    this.offset = this.memory.Length;
                    this.Current = memory;
                    return true;
                }

                if (index == 0 && this.options == StringSplitOptions.RemoveEmptyEntries)
                {
                    this.offset++;
                    continue;
                }

                this.Current = memory.Slice(0, index);
                this.offset += index + 1;
                return true;
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            // This is empty because there's nothing to dispose.
        }
    }
}
