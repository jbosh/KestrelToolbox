// <copyright file="JsonObjectExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Text.Json;
using System.Text.Json.Nodes;

namespace KestrelToolbox.Extensions.JsonObjectExtensions;

/// <summary>
/// Extensions for <see cref="JsonObject"/>.
/// </summary>
public static class JsonObjectExtensions
{
    /// <summary>
    /// Merge <paramref name="content"/> into <paramref name="destination"/>.
    /// </summary>
    /// <param name="destination">The destination to store merged content.</param>
    /// <param name="content">Content to merge in.</param>
    /// <param name="settings">Settings for how to merge the two objects.</param>
    public static void Merge(this JsonObject destination, JsonObject? content, JsonMergeSettings settings = default)
    {
        if (content == null)
        {
            return;
        }

        foreach (var (key, value) in content)
        {
            if (value is null || value.GetValueKind() == JsonValueKind.Null)
            {
                switch (settings.MergeNullValueHandling)
                {
                    case MergeNullValueHandling.Drop:
                    {
                        // Ignore the entire property and continue.
                        continue;
                    }

                    case MergeNullValueHandling.Keep:
                    {
                        destination[key] = null;
                        break;
                    }

                    default:
                    {
                        throw new ArgumentOutOfRangeException(
                            $"{nameof(settings)}.{nameof(settings.MergeNullValueHandling)} was unknown value {settings.MergeNullValueHandling}."
                        );
                    }
                }

                continue;
            }

            if (!destination.TryGetPropertyValue(key, out var property))
            {
                destination[key] = value.DeepClone();
                continue;
            }

            switch (value)
            {
                case JsonObject jsonObject:
                {
                    if (property is JsonObject propertyJsonObject)
                    {
                        propertyJsonObject.Merge(jsonObject, settings);
                    }
                    else
                    {
                        destination[key] = value.DeepClone();
                    }

                    break;
                }

                case JsonArray jsonArray:
                {
                    if (property is JsonArray propertyJsonArray)
                    {
                        switch (settings.MergeArrayHandling)
                        {
                            case MergeArrayHandling.Concat:
                            {
                                foreach (var item in jsonArray)
                                {
                                    propertyJsonArray.Add(item?.DeepClone());
                                }

                                break;
                            }

                            case MergeArrayHandling.Union:
                            {
                                var distinct = propertyJsonArray
                                    .Concat(jsonArray)
                                    .Distinct(NodeDeepEqualityComparer.Instance)
                                    .Select(n => n?.DeepClone())
                                    .ToArray();
                                destination[key] = new JsonArray(distinct);
                                break;
                            }

                            case MergeArrayHandling.Replace:
                            {
                                destination[key] = value.DeepClone();
                                break;
                            }

                            case MergeArrayHandling.Merge:
                            {
                                // This is crazy inefficient but json merging hopefully isn't performance critical.
                                // If it is, somebody can write a more efficient version.
                                for (var i = 0; i < jsonArray.Count; i++)
                                {
                                    if (i < propertyJsonArray.Count)
                                    {
                                        propertyJsonArray[i] = jsonArray[i]?.DeepClone();
                                    }
                                    else
                                    {
                                        propertyJsonArray.Add(jsonArray[i]?.DeepClone());
                                    }
                                }

                                break;
                            }

                            default:
                            {
                                throw new ArgumentOutOfRangeException(
                                    $"{nameof(settings)}.{nameof(settings.MergeArrayHandling)} was unknown value {settings.MergeArrayHandling}."
                                );
                            }
                        }
                    }
                    else
                    {
                        destination[key] = value.DeepClone();
                    }

                    break;
                }

                case JsonValue:
                {
                    destination[key] = value.DeepClone();
                    break;
                }

                default:
                {
                    throw new NotImplementedException($"Unknown type {value.GetType()}");
                }
            }
        }
    }

    /// <summary>
    /// Compares two <see cref="JsonNode"/> for deep equality.
    /// </summary>
    private sealed class NodeDeepEqualityComparer : IEqualityComparer<JsonNode?>
    {
        public static NodeDeepEqualityComparer Instance { get; } = new();

        /// <inheritdoc />
        public bool Equals(JsonNode? x, JsonNode? y) => JsonNode.DeepEquals(x, y);

        /// <inheritdoc />
        /// <remarks>
        /// This is a dumb hack cause <see cref="JsonNode"/> GetHashCode hashes on reference so they
        /// are all unique.
        ///
        /// Basically, the hash code is the type. There's no point calling DeepEquals on different types.
        /// </remarks>
        public int GetHashCode(JsonNode node) =>
            node switch
            {
                JsonArray => 0,
                JsonObject => 1,
                JsonValue => 2,
                _ => 3,
            };
    }
}

/// <summary>
/// Specifies how JSON arrays are merged together.
/// </summary>
public enum MergeArrayHandling
{
    /// <summary>
    /// Concatenate arrays together.
    /// </summary>
    Concat = 0,

    /// <summary>
    /// Union arrays. Only keep distinct items, skipping items that already exist.
    /// </summary>
    Union = 1,

    /// <summary>
    /// Replace the array wholesale.
    /// </summary>
    Replace = 2,

    /// <summary>
    /// Merge array items together, matched by index.
    /// </summary>
    /// <remarks>
    /// Merging will combine two arrays and set by index.
    /// <c>destination.array[index] = source.array[index]</c>.
    /// </remarks>
    Merge = 3,
}

/// <summary>
/// Specifies how null value properties are merged together.
/// </summary>
public enum MergeNullValueHandling
{
    /// <summary>
    /// Any null value properties being merged in will be dropped as if they didn't exist.
    /// </summary>
    Drop = 0,

    /// <summary>
    /// Any null value properties being merged in will be kept and use other settings to decide what to do.
    /// </summary>
    Keep = 1,
}

/// <summary>
/// Settings to use when merging two <see cref="JsonObject"/> together.
/// </summary>
/// <seealso cref="JsonObjectExtensions"/>
public struct JsonMergeSettings
{
    /// <summary>
    /// Gets or sets how array merging is handled.
    /// </summary>
    public MergeArrayHandling MergeArrayHandling { get; set; }

    /// <summary>
    /// Gets or sets how merging null value properties is handled.
    /// </summary>
    public MergeNullValueHandling MergeNullValueHandling { get; set; }
}
