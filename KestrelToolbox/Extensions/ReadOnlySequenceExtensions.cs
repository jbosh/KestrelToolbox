// <copyright file="ReadOnlySequenceExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Buffers;
using System.Text;

namespace KestrelToolbox.Extensions.ReadOnlySequenceExtensions;

/// <summary>
/// Extension methods for <see cref="ReadOnlySequence{T}"/>.
/// </summary>
public static class ReadOnlySequenceExtensions
{
    /// <summary>
    /// Determines whether a sequence of utf8 bytes is equal to a string.
    /// </summary>
    /// <param name="utf8Sequence">Sequence of utf8 bytes.</param>
    /// <param name="comparison">String to compare to.</param>
    /// <returns>True if the sequence and string are equivalent.</returns>
    public static bool SequenceEqualsUTF8(this ReadOnlySequence<byte> utf8Sequence, string comparison) =>
        SequenceEqualsUTF8(utf8Sequence, (ReadOnlySpan<char>)comparison);

    /// <summary>
    /// Determines whether a sequence of utf8 bytes is equal to a string.
    /// </summary>
    /// <param name="utf8Sequence">Sequence of utf8 bytes.</param>
    /// <param name="comparison">Span of characters to compare to.</param>
    /// <returns>True if the sequence and string are equivalent.</returns>
    public static bool SequenceEqualsUTF8(this ReadOnlySequence<byte> utf8Sequence, ReadOnlySpan<char> comparison)
    {
        var byteCount = Encoding.UTF8.GetByteCount(comparison);
        if (byteCount != utf8Sequence.Length)
        {
            return false;
        }

        Span<byte> bytes = stackalloc byte[byteCount];
        var numberOfBytes = Encoding.UTF8.GetBytes(comparison, bytes);
        Debug.Assert(numberOfBytes == byteCount, "Number of bytes should have already been accurately computed.");
        bytes = bytes.Slice(0, numberOfBytes);
        if (utf8Sequence.IsSingleSegment)
        {
            return utf8Sequence.FirstSpan.SequenceEqual(bytes);
        }

        foreach (var memory in utf8Sequence)
        {
            if (!bytes.Slice(0, memory.Length).SequenceEqual(memory.Span))
            {
                return false;
            }

            bytes = bytes.Slice(memory.Length);
        }

        return true;
    }

    /// <summary>
    /// Determines whether a sequence of ascii bytes is equal to a string.
    /// </summary>
    /// <param name="asciiSequence">Sequence of ascii bytes.</param>
    /// <param name="comparison">String to compare to.</param>
    /// <returns>True if the sequence and string are equivalent.</returns>
    public static bool SequenceEqualsASCII(this ReadOnlySequence<byte> asciiSequence, string comparison) =>
        SequenceEqualsASCII(asciiSequence, comparison.AsSpan());

    /// <summary>
    /// Determines whether a sequence of ascii bytes is equal to a string.
    /// </summary>
    /// <param name="asciiSequence">Sequence of ascii bytes.</param>
    /// <param name="comparison">Span of characters to compare to.</param>
    /// <returns>True if the sequence and string are equivalent.</returns>
    public static bool SequenceEqualsASCII(this ReadOnlySequence<byte> asciiSequence, ReadOnlySpan<char> comparison)
    {
        if (comparison.Length != asciiSequence.Length)
        {
            return false;
        }

        var offset = 0;
        foreach (var memory in asciiSequence)
        {
            var span = memory.Span;
            if (!span.SpanEqualsASCII(comparison.Slice(offset, span.Length)))
            {
                return false;
            }

            offset += memory.Length;
        }

        return true;
    }

    /// <summary>
    /// Determines whether a span of ascii bytes is equal to a string.
    /// </summary>
    /// <param name="asciiSpan">Span of ascii bytes.</param>
    /// <param name="comparison">String to compare to.</param>
    /// <returns>True if the span and string are equivalent.</returns>
    public static bool SpanEqualsASCII(this Span<byte> asciiSpan, string comparison) =>
        SpanEqualsASCII((ReadOnlySpan<byte>)asciiSpan, comparison.AsSpan());

    /// <summary>
    /// Determines whether a span of ascii bytes is equal to a string.
    /// </summary>
    /// <param name="asciiSpan">Span of ascii bytes.</param>
    /// <param name="comparison">Span of characters to compare to.</param>
    /// <returns>True if the span and string are equivalent.</returns>
    public static bool SpanEqualsASCII(this Span<byte> asciiSpan, ReadOnlySpan<char> comparison) =>
        SpanEqualsASCII((ReadOnlySpan<byte>)asciiSpan, comparison);

    /// <summary>
    /// Determines whether a span of ascii bytes is equal to a string.
    /// </summary>
    /// <param name="asciiSpan">Span of ascii bytes.</param>
    /// <param name="comparison">String to compare to.</param>
    /// <returns>True if the span and string are equivalent.</returns>
    public static bool SpanEqualsASCII(this ReadOnlySpan<byte> asciiSpan, string comparison) =>
        SpanEqualsASCII(asciiSpan, comparison.AsSpan());

    /// <summary>
    /// Determines whether a span of ascii bytes is equal to a string.
    /// </summary>
    /// <param name="asciiSpan">Span of ascii bytes.</param>
    /// <param name="comparison">Span of characters to compare to.</param>
    /// <returns>True if the span and string are equivalent.</returns>
    public static bool SpanEqualsASCII(this ReadOnlySpan<byte> asciiSpan, ReadOnlySpan<char> comparison)
    {
        if (comparison.Length != asciiSpan.Length)
        {
            return false;
        }

        for (var i = 0; i < asciiSpan.Length; i++)
        {
            if (comparison[i] != (char)asciiSpan[i])
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Converts a sequence of utf8 bytes into a single string.
    /// </summary>
    /// <param name="utf8Sequence">Sequence of utf8 bytes.</param>
    /// <returns>Decoded string.</returns>
    public static string ToUTF8String(this ReadOnlySequence<byte> utf8Sequence)
    {
        if (utf8Sequence.Length > int.MaxValue)
        {
            throw new NotSupportedException($"Maximum size string of {int.MaxValue} characters.");
        }

        if (utf8Sequence.IsSingleSegment)
        {
            return Encoding.UTF8.GetString(utf8Sequence.FirstSpan);
        }

        var builder = new StringBuilder((int)utf8Sequence.Length);
        foreach (var segment in utf8Sequence)
        {
            var s = Encoding.UTF8.GetString(segment.Span);
            _ = builder.Append(s);
        }

        return builder.ToString();
    }
}
