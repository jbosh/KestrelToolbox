// <copyright file="PipeReaderExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.IO.Pipelines;
using KestrelToolbox.Extensions.ReadOnlySequenceExtensions;

namespace KestrelToolbox.Extensions.PipeReaderExtensions;

/// <summary>
/// Extensions for PipeReader.
/// </summary>
public static class PipeReaderExtensions
{
    /// <summary>
    /// Reads a pipe until all the bytes are available.
    /// </summary>
    /// <param name="reader">PipeReader to read.</param>
    /// <returns>Read result from the final read.</returns>
    public static async ValueTask<ReadResult> ReadToEndAsync(this PipeReader reader)
    {
        var readResult = await reader.ReadAsync();
        while (!readResult.IsCompleted)
        {
            reader.AdvanceTo(readResult.Buffer.GetPosition(0), readResult.Buffer.End);
            readResult = await reader.ReadAsync();
        }

        return readResult;
    }

    /// <summary>
    /// Reads a pipe until all the utf8 bytes are available and convert it to a string.
    /// </summary>
    /// <param name="reader">PipeReader to read.</param>
    /// <returns>Decoded string.</returns>
    public static async ValueTask<string> ReadUTF8StringAsync(this PipeReader reader)
    {
        var readResult = await reader.ReadToEndAsync();
        var buffer = readResult.Buffer;
        var result = buffer.ToUTF8String();
        reader.AdvanceTo(buffer.End);
        return result;
    }
}
