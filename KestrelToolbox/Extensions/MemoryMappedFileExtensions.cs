// <copyright file="MemoryMappedFileExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics.CodeAnalysis;
using System.IO.MemoryMappedFiles;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace KestrelToolbox.Extensions.MemoryMappedFileExtensions;

/// <summary>
/// A set of extensions for <see cref="MemoryMappedFile"/>.
/// </summary>
public static class MemoryMappedFileExtensions
{
    /// <summary>
    /// Reads a span of bytes from the accessor into the provided reference.
    /// </summary>
    /// <param name="accessor">Accessor to read from.</param>
    /// <param name="position">The position in the accessor in which to begin reading.</param>
    /// <param name="destination">The span to contain the read data.</param>
    /// <returns>
    /// The number of bytes read. Will be 0 if <paramref name="position"/> is less than zero or greater than
    /// the capacity of the accessor.
    /// </returns>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    public static unsafe int Read(this MemoryMappedViewAccessor accessor, long position, Span<byte> destination)
    {
        ArgumentNullException.ThrowIfNull(accessor);

        var capacity = accessor.Capacity;
        if (position >= capacity || position < 0)
        {
            return 0;
        }

        var end = Math.Min(position + destination.Length, capacity);
        var amountToRead = (int)Math.Min(destination.Length, end - position);

        var data = default(byte*);
        try
        {
            accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref data);
            var source = new Span<byte>(data + position, amountToRead);
            source.CopyTo(destination);
        }
        finally
        {
            if (data != null)
            {
                accessor.SafeMemoryMappedViewHandle.ReleasePointer();
            }
        }

        return amountToRead;
    }

    /// <summary>
    /// Creates a <see cref="MemoryMappedViewAccessorExtended"/> that maps to a view of the memory-mapped file.
    /// </summary>
    /// <param name="file">The file to create the accessor from.</param>
    /// <returns>A randomly accessible block of memory.</returns>
    public static MemoryMappedViewAccessorExtended CreateViewAccessorExtended(this MemoryMappedFile file) =>
        new(file.CreateViewAccessor());

    /// <summary>
    /// Creates a <see cref="MemoryMappedViewAccessorExtended"/> that maps to a view of the memory-mapped file.
    /// </summary>
    /// <param name="file">The file to create the accessor from.</param>
    /// <param name="offset">The byte at which to start the view.</param>
    /// <param name="size">The size of the view. Specify 0 (zero) to create a view that starts at offset and ends approximately at the end of the memory-mapped file.</param>
    /// <returns>A randomly accessible block of memory.</returns>
    public static MemoryMappedViewAccessorExtended CreateViewAccessorExtended(
        this MemoryMappedFile file,
        long offset,
        long size
    ) => new(file.CreateViewAccessor(offset, size));

    /// <summary>
    /// Creates a <see cref="MemoryMappedViewAccessorExtended"/> that maps to a view of the memory-mapped file.
    /// </summary>
    /// <param name="file">The file to create the accessor from.</param>
    /// <param name="offset">The byte at which to start the view.</param>
    /// <param name="size">The size of the view. Specify 0 (zero) to create a view that starts at offset and ends approximately at the end of the memory-mapped file.</param>
    /// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="MemoryMappedFileAccess.ReadWrite"/>.</param>
    /// <returns>A randomly accessible block of memory.</returns>
    public static MemoryMappedViewAccessorExtended CreateViewAccessorExtended(
        this MemoryMappedFile file,
        long offset,
        long size,
        MemoryMappedFileAccess access
    ) => new(file.CreateViewAccessor(offset, size, access));
}

/// <summary>
/// Represents a randomly accessed view of a memory-mapped file. This should be equivalent to <see cref="MemoryMappedViewAccessor"/>
/// except having some more modern methods and being faster.
/// </summary>
/// <remarks>
/// Disposing this structure is much more important than <see cref="MemoryMappedViewAccessor"/> due to caching off the handle
/// and sitting on a reference to the file. The finalizer queue will pick up the object at some point in the future.
///
/// Also please note that this class does not work on platforms where misaligned access is not supported. This includes all ARM platforms
/// before v7.
/// </remarks>
public unsafe class MemoryMappedViewAccessorExtended : IDisposable
{
    [MethodImpl(MethodImplOptions.NoOptimization)] // Disable optimization so that we don't get stack alignment test optimized out.
    [SuppressMessage(
        "Minor Code Smell",
        "S3963:\"static\" fields should be initialized inline",
        Justification = "Impossible to do this much logic inline."
    )]
    static MemoryMappedViewAccessorExtended()
    {
        // Test if misaligned writes and reads are supported.
        PlatformSupported = true;
        var stackValues = stackalloc byte[16];
        const int TestValue = 0x01020304;
        for (var offset = 0; offset < 4; offset++)
        {
            for (var i = 0; i < 16; i++)
            {
                stackValues[i] = 0;
            }

            *(int*)(stackValues + offset) = TestValue;
            if (BitConverter.IsLittleEndian)
            {
                if (
                    stackValues[offset + 0] != 0x04
                    || stackValues[offset + 1] != 0x03
                    || stackValues[offset + 2] != 0x02
                    || stackValues[offset + 3] != 0x01
                )
                {
                    PlatformSupported = false;
                }
            }
            else
            {
                if (
                    stackValues[offset + 0] != 0x01
                    || stackValues[offset + 1] != 0x02
                    || stackValues[offset + 2] != 0x03
                    || stackValues[offset + 3] != 0x04
                )
                {
                    PlatformSupported = false;
                }
            }
        }
    }

    /// <summary>
    /// Whether or not this platform supports MemoryMappedViewAccessor. This is tested and initialized
    /// at runtime.
    /// </summary>
    private static readonly bool PlatformSupported;

    private readonly MemoryMappedViewAccessor accessor;
    private byte* basePtr;

    /// <summary>
    /// Initializes a new instance of the <see cref="MemoryMappedViewAccessorExtended"/> class.
    /// </summary>
    /// <param name="accessor">Accessor to reference.</param>
    public MemoryMappedViewAccessorExtended(MemoryMappedViewAccessor accessor)
    {
        if (!PlatformSupported)
        {
            throw new PlatformNotSupportedException(
                "The platform you are on does not support misaligned memory reads and writes."
            );
        }

        this.accessor = accessor;
        this.accessor.SafeMemoryMappedViewHandle.AcquirePointer(ref this.basePtr);
        this.Capacity = this.accessor.Capacity;
    }

    /// <summary>
    /// Gets the number of bytes by which the starting position of this view is offset from the beginning of the memory-mapped file.
    /// </summary>
    /// <exception cref="InvalidOperationException">
    /// The object from which this instance was created is null.
    /// </exception>
    /// <returns>
    /// The number of bytes between the starting position of this view and the beginning of the memory-mapped file.
    /// </returns>
    public long PointerOffset => this.accessor.PointerOffset;

    /// <summary>
    /// Gets a handle to the view of a memory-mapped file.
    /// </summary>
    /// <returns>A wrapper for the operating system's handle to the view of the file.</returns>
    public SafeMemoryMappedViewHandle SafeMemoryMappedViewHandle => this.accessor.SafeMemoryMappedViewHandle;

    /// <summary>
    /// Clears all buffers for this view and causes any buffered data to be written to the underlying file.
    /// </summary>
    /// <exception cref="ObjectDisposedException">Methods were called after the accessor was closed.</exception>
    public void Flush() => this.accessor.Flush();

    /// <summary>
    /// Gets a value indicating whether determines whether the accessory is writable.
    /// </summary>
    /// <returns>true if the accessor is writable; otherwise, false.</returns>
    public bool CanWrite => this.accessor.CanWrite;

    /// <summary>
    /// Gets a value indicating whether determines whether the accessor is readable.
    /// </summary>
    /// <returns>true if the accessor is readable; otherwise, false.</returns>
    public bool CanRead => this.accessor.CanRead;

    /// <summary>
    /// Gets the capacity of the accessor.
    /// </summary>
    /// <returns>The capacity of the accessor.</returns>
    public long Capacity { get; }

    /// <summary>
    /// Gets a writable span into the memory mapped file.
    /// </summary>
    /// <param name="position">The number of bytes in the accessor at which to begin.</param>
    /// <param name="length">Length of the bytes to read.</param>
    /// <returns>A writable span. This span is unsafe to use after destruction of the <see cref="MemoryMappedViewAccessorExtended"/>.</returns>
    public Span<byte> GetSpan(long position, int length)
    {
        this.ThrowWriteExceptions(position, length);
        return new Span<byte>(this.basePtr + position, length);
    }

    /// <summary>
    /// Gets a readonly span into the memory mapped file.
    /// </summary>
    /// <param name="position">The number of bytes in the accessor at which to begin.</param>
    /// <param name="length">Length of the bytes to read.</param>
    /// <returns>A read only span. This span is unsafe to use after destruction of the <see cref="MemoryMappedViewAccessorExtended"/>.</returns>
    public ReadOnlySpan<byte> GetReadOnlySpan(long position, int length)
    {
        this.ThrowSimpleReadExceptions(position, length);
        return new ReadOnlySpan<byte>(this.basePtr + position, length);
    }

    /// <summary>
    /// Reads a structure of type T from the accessor into a provided reference.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <exception cref="ArgumentException">
    /// There are not enough bytes after position to read in a structure of type T. -or-
    /// T is a value type that contains one or more reference types.
    /// </exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <returns>The element read from <paramref name="position"/>.</returns>
    public T Read<T>(long position)
        where T : struct
    {
        this.Read(position, out T structure);
        return structure;
    }

    /// <summary>
    /// Reads a structure of type T from the accessor into a provided reference.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <param name="structure">The structure to contain the read data.</param>
    /// <exception cref="ArgumentException">
    /// There are not enough bytes after position to read in a structure of type T. -or-
    /// T is a value type that contains one or more reference types.
    /// </exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Read<T>(long position, out T structure)
        where T : struct
    {
        this.ThrowSimpleReadExceptions(position, Marshal.SizeOf<T>());

        structure = default;
        var dstStructure = MemoryMarshal.CreateSpan(ref structure, 1);
        var dst = MemoryMarshal.AsBytes(dstStructure);
        var src = new Span<byte>(this.basePtr + position, dst.Length);
        src.CopyTo(dst);
    }

    /// <summary>
    /// Reads structures of type T from the accessor into an array of type T.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <param name="array">The array to contain the structures read from the accessor.</param>
    /// <exception cref="ArgumentException">
    /// array is not large enough to contain count of structures (starting from position).
    /// </exception>
    /// <exception cref="ArgumentNullException">array is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <returns>
    /// The number of structures read into array. This value can be less than count if
    /// there are fewer structures available, or zero if the end of the accessor is reached.
    /// </returns>
    public int ReadArray<T>(long position, T[] array)
        where T : struct => this.ReadArray(position, array.AsSpan());

    /// <summary>
    /// Reads structures of type T from the accessor into an array of type T.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <param name="array">The array to contain the structures read from the accessor.</param>
    /// <param name="offset">The index in array in which to place the first copied structure.</param>
    /// <param name="count">The number of structures of type T to read from the accessor.</param>
    /// <exception cref="ArgumentException">
    /// array is not large enough to contain count of structures (starting from position).
    /// </exception>
    /// <exception cref="ArgumentNullException">array is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <returns>
    /// The number of structures read into array. This value can be less than count if
    /// there are fewer structures available, or zero if the end of the accessor is reached.
    /// </returns>
    public int ReadArray<T>(long position, T[] array, int offset, int count)
        where T : struct => this.ReadArray(position, array.AsSpan(offset, count));

    /// <summary>
    /// Reads structures of type T from the accessor into a span of type T.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <param name="array">The span to contain the structures read from the accessor.</param>
    /// <exception cref="ArgumentException">
    /// array is not large enough to contain count of structures (starting from position).
    /// </exception>
    /// <exception cref="ArgumentNullException">array is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <returns>
    /// The number of structures read into array. This value can be less than count if
    /// there are fewer structures available, or zero if the end of the accessor is reached.
    /// </returns>
    public int ReadArray<T>(long position, Span<T> array)
        where T : struct
    {
        var sizeofT = Marshal.SizeOf<T>();
        if (position < 0 || position >= this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                $"{nameof(position)} must be greater than or equal to zero and less than {nameof(this.Capacity)}."
            );
        }

        if (!this.accessor.CanRead)
        {
            throw new NotSupportedException("Reading is not supported.");
        }

        ObjectDisposedException.ThrowIf(this.basePtr == null, typeof(MemoryMappedViewAccessorExtended));

        var end = Math.Min(position + (array.Length * sizeofT), this.Capacity);
        var amountToRead = (int)Math.Min(array.Length * sizeofT, end - position);
        var countToRead = amountToRead / sizeofT;

        if (countToRead == 0)
        {
            return 0;
        }

        var dst = MemoryMarshal.AsBytes(array);

        var src = new Span<byte>(this.basePtr + position, countToRead * sizeofT);
        src.CopyTo(dst);

        return countToRead;
    }

    /// <summary>
    /// Reads bytes from the accessor into an array of bytes.
    /// </summary>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <param name="count">The number of bytes to read from the accessor.</param>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <returns>
    /// The bytes read. This array can contain fewer than count if there are fewer bytes available, or zero if the
    /// end of the accessor is reached.
    /// </returns>
    public byte[] ReadBytes(long position, int count)
    {
        if (position < 0 || position >= this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                $"{nameof(position)} must be greater than or equal to zero and less than {nameof(this.Capacity)}."
            );
        }

        if (!this.accessor.CanRead)
        {
            throw new NotSupportedException("Reading is not supported.");
        }

        ObjectDisposedException.ThrowIf(this.basePtr == null, typeof(MemoryMappedViewAccessorExtended));

        var end = Math.Min(position + count, this.Capacity);
        var amountToRead = (int)Math.Min(count, end - position);
        var arr = new byte[amountToRead];

        var source = new Span<byte>(this.basePtr + position, amountToRead);
        source.CopyTo(arr.AsSpan());

        return arr;
    }

    /// <summary>
    /// Reads bytes from the accessor into a span of bytes.
    /// </summary>
    /// <param name="position">The number of bytes in the accessor at which to begin reading.</param>
    /// <param name="span">The span to contain the bytes read from the accessor.</param>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    /// <returns>
    /// The number of bytes read into span. This value can be less than count if there are fewer bytes available, or zero if the
    /// end of the accessor is reached.
    /// </returns>
    public int ReadBytes(long position, Span<byte> span)
    {
        if (position < 0 || position >= this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                $"{nameof(position)} must be greater than or equal to zero and less than {nameof(this.Capacity)}."
            );
        }

        if (!this.accessor.CanRead)
        {
            throw new NotSupportedException("Reading is not supported.");
        }

        ObjectDisposedException.ThrowIf(this.basePtr == null, typeof(MemoryMappedViewAccessorExtended));

        var end = Math.Min(position + span.Length, this.Capacity);
        var amountToRead = (int)Math.Min(span.Length, end - position);

        var source = new Span<byte>(this.basePtr + position, amountToRead);
        source.CopyTo(span);

        return amountToRead;
    }

    /// <summary>
    /// Reads a Boolean value from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>true or false.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public bool ReadBoolean(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(bool));
        return *(this.basePtr + position) == 1;
    }

    /// <summary>
    /// Reads a byte value from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public byte ReadByte(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(byte));
        return *(this.basePtr + position);
    }

    /// <summary>
    /// Reads a character from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The character that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public char ReadChar(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(char));
        return *(char*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a decimal value from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public decimal ReadDecimal(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(decimal));
        return *(decimal*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a double-precision floating-point value from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public double ReadDouble(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(double));
        return *(double*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a 16-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public short ReadInt16(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(short));
        return *(short*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a 32-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public int ReadInt32(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(bool));
        return *(int*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a 64-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public long ReadInt64(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(long));
        return *(long*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a 64-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public sbyte ReadSByte(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(sbyte));
        return *(sbyte*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads a single-precision floating-point value from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public float ReadSingle(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(float));
        return *(float*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads an unsigned 16-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public ushort ReadUInt16(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(ushort));
        return *(ushort*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads an unsigned 32-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public uint ReadUInt32(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(uint));
        return *(uint*)(this.basePtr + position);
    }

    /// <summary>
    /// Reads an unsigned 64-bit integer from the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin reading.</param>
    /// <returns>The value that was read.</returns>
    /// <exception cref="ArgumentException">There are not enough bytes after position to read a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support reading.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public ulong ReadUInt64(long position)
    {
        this.ThrowSimpleReadExceptions(position, sizeof(ulong));
        return *(ulong*)(this.basePtr + position);
    }

    /// <summary>
    /// Writes a Boolean value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, bool value)
    {
        this.ThrowWriteExceptions(position, sizeof(bool));
        *(this.basePtr + position) = (byte)(value ? 1 : 0);
    }

    /// <summary>
    /// Writes an unsigned 64-bit integer value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, ulong value)
    {
        this.ThrowWriteExceptions(position, sizeof(ulong));
        *(ulong*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes an unsigned 32-bit integer value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, uint value)
    {
        this.ThrowWriteExceptions(position, sizeof(uint));
        *(uint*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a byte value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, byte value)
    {
        this.ThrowWriteExceptions(position, sizeof(byte));
        *(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a Single into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, float value)
    {
        this.ThrowWriteExceptions(position, sizeof(float));
        *(float*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes an unsigned 16-bit integer into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, ushort value)
    {
        this.ThrowWriteExceptions(position, sizeof(ushort));
        *(ushort*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a 64-bit integer into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, long value)
    {
        this.ThrowWriteExceptions(position, sizeof(long));
        *(long*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a 32-bit integer into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, int value)
    {
        this.ThrowWriteExceptions(position, sizeof(int));
        *(int*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a 16-bit integer into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, short value)
    {
        this.ThrowWriteExceptions(position, sizeof(short));
        *(short*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a Double value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, double value)
    {
        this.ThrowWriteExceptions(position, sizeof(double));
        *(double*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a decimal value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, decimal value)
    {
        this.ThrowWriteExceptions(position, sizeof(decimal));
        *(decimal*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes an 8-bit integer value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, sbyte value)
    {
        this.ThrowWriteExceptions(position, sizeof(sbyte));
        *(sbyte*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a character value into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="value">The value to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write(long position, char value)
    {
        this.ThrowWriteExceptions(position, sizeof(char));
        *(char*)(this.basePtr + position) = value;
    }

    /// <summary>
    /// Writes a structure into the accessor.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="structure">The structure to write.</param>
    /// <exception cref="ArgumentException">There are not enough bytes after position to write a value.</exception>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void Write<T>(long position, ref T structure)
        where T : struct
    {
        var sizeofT = Marshal.SizeOf<T>();
        this.ThrowWriteExceptions(position, sizeofT);

        var srcStructure = MemoryMarshal.CreateSpan(ref structure, 1);
        var src = MemoryMarshal.AsBytes(srcStructure);
        var dst = new Span<byte>(this.basePtr + position, src.Length);
        src.CopyTo(dst);
    }

    /// <summary>
    /// Writes structures from an array of type T into the accessor.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="array">The array to write into the accessor.</param>
    /// <exception cref="ArgumentException">
    /// There are not enough bytes in the accessor after position to write the number
    /// of structures specified by count.
    /// </exception>
    /// <exception cref="ArgumentNullException">array is null.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void WriteArray<T>(long position, T[] array)
        where T : struct => this.WriteArray(position, array.AsSpan());

    /// <summary>
    /// Writes structures from an array of type T into the accessor.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="array">The array to write into the accessor.</param>
    /// <param name="offset">The index in array to start writing from.</param>
    /// <param name="count">The number of structures in array to write.</param>
    /// <exception cref="ArgumentException">
    /// There are not enough bytes in the accessor after position to write the number
    /// of structures specified by count.
    /// </exception>
    /// <exception cref="ArgumentOutOfRangeException">
    /// position is less than zero or greater than the capacity of the accessor. -or-
    /// offset or count is less than zero.
    /// </exception>
    /// <exception cref="ArgumentNullException">array is null.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void WriteArray<T>(long position, T[] array, int offset, int count)
        where T : struct => this.WriteArray(position, array.AsSpan(offset, count));

    /// <summary>
    /// Writes structures from a span of type T into the accessor.
    /// </summary>
    /// <typeparam name="T">The type of structure.</typeparam>
    /// <param name="position">The number of bytes into the accessor at which to begin writing.</param>
    /// <param name="array">The span to write into the accessor.</param>
    /// <exception cref="ArgumentException">
    /// There are not enough bytes in the accessor after position to write the number
    /// of structures specified by count.
    /// </exception>
    /// <exception cref="ArgumentOutOfRangeException">
    /// position is less than zero or greater than the capacity of the accessor. -or-
    /// offset or count is less than zero.
    /// </exception>
    /// <exception cref="ArgumentNullException">array is null.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void WriteArray<T>(long position, Span<T> array)
        where T : struct
    {
        var sizeofT = Marshal.SizeOf<T>();
        this.ThrowWriteExceptions(position, array.Length * sizeofT);

        var src = MemoryMarshal.AsBytes(array);
        var dst = new Span<byte>(this.basePtr + position, src.Length);
        src.CopyTo(dst);
    }

    /// <summary>
    /// Writes bytes from a span into the accessor.
    /// </summary>
    /// <param name="position">The number of bytes in the accessor at which to begin writing.</param>
    /// <param name="span">The span to read bytes from.</param>
    /// <exception cref="ArgumentOutOfRangeException">position is less than zero or greater than the capacity of the accessor.</exception>
    /// <exception cref="NotSupportedException">The accessor does not support writing.</exception>
    /// <exception cref="ObjectDisposedException">The accessor has been disposed.</exception>
    public void WriteBytes(long position, Span<byte> span)
    {
        this.ThrowWriteExceptions(position, span.Length);

        var destination = new Span<byte>(this.basePtr + position, span.Length);
        span.CopyTo(destination);
    }

    /// <summary>
    /// Finalizes an instance of the <see cref="MemoryMappedViewAccessorExtended"/> class.
    /// </summary>
    ~MemoryMappedViewAccessorExtended() => this.Dispose();

    /// <summary>
    /// Releases all resources used by the <see cref="MemoryMappedViewAccessorExtended"/>.
    /// </summary>
    public void Dispose()
    {
        GC.SuppressFinalize(this);
        if (this.basePtr != null)
        {
            this.accessor.SafeMemoryMappedViewHandle.ReleasePointer();
            this.basePtr = null;
        }

        this.accessor.Dispose();
    }

    private void ThrowWriteExceptions(long position, int size)
    {
        if (position < 0 || position > this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                $"{nameof(position)} must be greater than or equal to zero or less than {nameof(this.Capacity)}."
            );
        }

        ObjectDisposedException.ThrowIf(this.basePtr == null, typeof(MemoryMappedViewAccessorExtended));

        if (!this.accessor.CanWrite)
        {
            throw new NotSupportedException("Writing is not supported.");
        }

        var end = position + size;
        if (end > this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                "There are not enough bytes available to complete this write."
            );
        }
    }

    private void ThrowSimpleReadExceptions(long position, int size)
    {
        if (position < 0 || position > this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                $"{nameof(position)} must be greater than or equal to zero or less than {nameof(this.Capacity)}."
            );
        }

        ObjectDisposedException.ThrowIf(this.basePtr == null, typeof(MemoryMappedViewAccessorExtended));

        if (!this.accessor.CanRead)
        {
            throw new NotSupportedException("Reading is not supported.");
        }

        var end = position + size;
        if (end > this.Capacity)
        {
            throw new ArgumentOutOfRangeException(
                nameof(position),
                "There are not enough bytes available to complete this read."
            );
        }
    }
}
