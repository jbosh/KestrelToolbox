// <copyright file="QueryParametersAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Web;

/// <summary>
/// Specifies this parameter should be parsed from query parameters.
/// </summary>
[AttributeUsage(AttributeTargets.Parameter)]
public class QueryParametersAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the parse method (either fully qualified or reachable by this method) to use if custom parsing is needed.
    /// </summary>
    public string? ParseMethod { get; set; }
}
