// <copyright file="RouteInfo.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace KestrelToolbox.Web;

/// <summary>
/// Info for a route.
/// </summary>
public sealed class RouteInfo
{
    /// <summary>
    /// Gets the pattern for a route. See <a href="https://learn.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-5.0#route-constraint-reference">https://learn.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-5.0#route-constraint-reference</a>
    /// for more information on what this info is. The named elements should correspond to method arguments.
    /// </summary>
    public required string Pattern { get; init; }

    /// <summary>
    /// Gets the method supported by this route.
    /// </summary>
    public required string[] Methods { get; init; }

    /// <summary>
    /// Gets the callback for this route.
    /// </summary>
    public required RequestDelegate Callback { get; init; }

    /// <summary>
    /// Gets any authorization data for this route.
    /// </summary>
    public IAuthorizeData[]? AuthorizationPolicies { get; init; }

    /// <summary>
    /// Gets a value indicating whether or not to allow anonymous requests to this route.
    /// </summary>
    public bool AllowAnonymous { get; init; }

    /// <inheritdoc />
    public override string ToString() => $"{string.Join("|", this.Methods)} {this.Pattern}";
}
