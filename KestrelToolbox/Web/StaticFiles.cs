// <copyright file="StaticFiles.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Globalization;
using System.IO;
using KestrelToolbox.Extensions;
using Microsoft.AspNetCore.Http;

namespace KestrelToolbox.Web;

/// <summary>
/// Handles sending static files safely from a specific directory.
/// </summary>
public class StaticFiles
{
    private readonly string rootPath;

    /// <summary>
    /// Gets or sets default max age to set cache tell clients to cache files for. Defaults to TimeSpan.Zero.
    /// </summary>
    public TimeSpan MaxAge { get; set; } = TimeSpan.Zero;

    /// <summary>
    /// Gets or sets a value indicating whether to search for .br sidecar files and send those if they exist. Default is true.
    /// </summary>
    public bool StaticBr { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether to search for .gz sidecar files and send those if they exist. Default is true.
    /// </summary>
    public bool StaticGz { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether the cache control headers should be private, false for public. Default is private.
    /// </summary>
    public bool CacheControlPrivate { get; set; } = true;

    /// <summary>
    /// Gets the set of extension to mime types that will be output when a file is sent. The default is application/octet-stream.
    /// The extensions must contain a leading period (.), for example ".txt".
    /// </summary>
    public Dictionary<string, string> MimeMappings { get; } =
        new()
        {
            { ".css", "text/css" },
            { ".flac", "audio/flac" },
            { ".gif", "image/gif" },
            { ".htm", "text/html" },
            { ".html", "text/html" },
            { ".jpeg", "image/jpeg" },
            { ".jpg", "image/jpeg" },
            { ".js", "application/javascript" },
            { ".json", "application/json" },
            { ".m4a", "audio/mp4" },
            { ".mp3", "audio/mp3" },
            { ".mp4", "video/mp4" },
            { ".png", "image/png" },
            { ".svg", "image/svg+xml" },
            { ".txt", "text/plain" },
            { ".wasm", "application/wasm" },
            { ".webmanifest", "application/manifest+json" },
        };

    private static readonly char[] RangeSeparatorCharacter = { '-' };

    /// <summary>
    /// Initializes a new instance of the <see cref="StaticFiles"/> class.
    /// </summary>
    /// <param name="rootPath">Path to directory from which to serve files.</param>
    /// <exception cref="DirectoryNotFoundException">Thrown if the <paramref name="rootPath"/> does not exist.</exception>
    public StaticFiles(string rootPath)
    {
        this.rootPath = rootPath;
        this.rootPath = Path.GetFullPath(this.rootPath);

        if (!this.rootPath.EndsWith(Path.DirectorySeparatorChar))
        {
            this.rootPath += Path.DirectorySeparatorChar;
        }

        if (!Directory.Exists(this.rootPath))
        {
            throw new DirectoryNotFoundException($"Could not find directory {this.rootPath}.");
        }
    }

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public ValueTask<bool> ServeFileAsync(HttpContext context) => this.ServeFileAsync(context, null, null, default);

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <param name="path">Path of the file on local disk to override the request. This can be relative to the root path of <see cref="StaticFiles"/> instance.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public ValueTask<bool> ServeFileAsync(HttpContext context, string path) =>
        this.ServeFileAsync(context, path, null, default);

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <param name="path">Path of the file on local disk to override the request. This can be relative to the root path of <see cref="StaticFiles"/> instance.</param>
    /// <param name="cancellationToken">Cancellation token if the request needs to be aborted early.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public ValueTask<bool> ServeFileAsync(HttpContext context, string path, CancellationToken cancellationToken) =>
        this.ServeFileAsync(context, path, null, cancellationToken);

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <param name="maxAgeOverride">Override the default max age of <see cref="StaticFiles"/> instance.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public ValueTask<bool> ServeFileAsync(HttpContext context, TimeSpan maxAgeOverride) =>
        this.ServeFileAsync(context, null, maxAgeOverride, default);

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <param name="maxAgeOverride">Override the default max age of <see cref="StaticFiles"/> instance.</param>
    /// <param name="cancellationToken">Cancellation token if the request needs to be aborted early.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public ValueTask<bool> ServeFileAsync(
        HttpContext context,
        TimeSpan maxAgeOverride,
        CancellationToken cancellationToken
    ) => this.ServeFileAsync(context, null, maxAgeOverride, cancellationToken);

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <param name="path">Path of the file on local disk to override the request. This can be relative to the root path of <see cref="StaticFiles"/> instance.</param>
    /// <param name="maxAgeOverride">Override the default max age of <see cref="StaticFiles"/> instance.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public ValueTask<bool> ServeFileAsync(HttpContext context, string? path, TimeSpan? maxAgeOverride) =>
        this.ServeFileAsync(context, path, maxAgeOverride, default);

    /// <summary>
    /// Serves a file based on request in <see cref="HttpContext"/>.
    /// </summary>
    /// <param name="context">Context with request to serve.</param>
    /// <param name="path">Path of the file on local disk to override the request. This can be relative to the root path of <see cref="StaticFiles"/> instance.</param>
    /// <param name="maxAgeOverride">Override the default max age of <see cref="StaticFiles"/> instance.</param>
    /// <param name="cancellationToken">Cancellation token if the request needs to be aborted early.</param>
    /// <returns>True if the file was served successfully, false if not.</returns>
    /// <remarks>
    /// All files served must be inside the root directory of <see cref="StaticFiles"/> otherwise they will not be found.
    /// </remarks>
    public async ValueTask<bool> ServeFileAsync(
        HttpContext context,
        string? path,
        TimeSpan? maxAgeOverride,
        CancellationToken cancellationToken
    )
    {
        var request = context.Request;
        var response = context.Response;

        path ??= request.Path.Value!;

        var fullPath = path.StartsWith(this.rootPath, StringComparison.Ordinal)
            ? Path.GetFullPath(path)
            : Path.GetFullPath(this.rootPath + path);

        if (!fullPath.StartsWith(this.rootPath, StringComparison.Ordinal))
        {
            return false;
        }

        if (!File.Exists(fullPath))
        {
            return false;
        }

        var extension = Path.GetExtension(fullPath).ToLowerInvariant();
        if (!this.MimeMappings.TryGetValue(extension, out var contentType))
        {
            contentType = "application/octet-stream";
        }

        var maxAge = maxAgeOverride ?? this.MaxAge;

        var fileInfo = new FileInfo(fullPath);
        var fileLength = fileInfo.Length;
        var disableRangeRequest = false;

        var lastModified = fileInfo.LastWriteTimeUtc.Ticks;
        var etag = $"\"{lastModified:x}-{fileLength:x}\"";

        if (request.Headers.TryGetValue("If-None-Match", out var ifNoneMatchValues))
        {
            var ifNoneMatch = (string?)ifNoneMatchValues;
            if (ifNoneMatch == etag)
            {
                response.StatusCode = StatusCodes.Status304NotModified;
                return true;
            }
        }

        if (request.Headers.TryGetValue("If-Match", out var ifMatchValues))
        {
            var ifMatch = (string?)ifMatchValues;
            if (ifMatch != etag)
            {
                response.StatusCode = StatusCodes.Status412PreconditionFailed;
                return true;
            }
        }

        if (request.Headers.TryGetValue("If-Range", out var ifRangeValues))
        {
            var ifRange = (string?)ifRangeValues;
            if (ifRange != etag)
            {
                disableRangeRequest = true;
            }
        }

        if (request.Headers.TryGetValue("If-Modified-Since", out var ifModifiedSinceValues))
        {
            if (DateTimeOffset.TryParse(ifModifiedSinceValues, CultureInfo.InvariantCulture, out var ifModifiedSince))
            {
                if (ifModifiedSince <= DateTimeOffset.Now && ifModifiedSince >= fileInfo.LastWriteTime)
                {
                    response.StatusCode = StatusCodes.Status304NotModified;
                    return true;
                }
            }
            else
            {
                response.StatusCode = StatusCodes.Status400BadRequest;
                await response.WriteAsync("Could not parse If-Modified-Since header.", cancellationToken);
                return true;
            }
        }

        if (request.Headers.TryGetValue("If-Unmodified-Since", out var ifUnmodifiedSinceValues))
        {
            if (
                DateTimeOffset.TryParse(
                    ifUnmodifiedSinceValues,
                    CultureInfo.InvariantCulture,
                    out var ifUnmodifiedSince
                )
            )
            {
                if (ifUnmodifiedSince <= DateTimeOffset.Now && ifUnmodifiedSince < fileInfo.LastWriteTime)
                {
                    response.StatusCode = StatusCodes.Status412PreconditionFailed;
                    return true;
                }
            }
            else
            {
                response.StatusCode = StatusCodes.Status400BadRequest;
                await response.WriteAsync("Could not parse If-Modified-Since header.", cancellationToken);
                return true;
            }
        }

        response.Headers["etag"] = etag;
        response.Headers["Content-Type"] = contentType;
        response.Headers["Accept-Ranges"] = "bytes";

        if (!disableRangeRequest && TryGetRange(request, out var range))
        {
            var (rangeStart, rangeEnd) = range;
            if (rangeEnd < 0 || rangeEnd >= fileLength)
            {
                rangeEnd = fileLength - 1;
            }

            if (rangeStart >= fileLength || rangeStart > rangeEnd)
            {
                response.StatusCode = StatusCodes.Status400BadRequest;
                await response.WriteAsync("Invalid file range.", cancellationToken);
                return true;
            }

            var length = rangeEnd - rangeStart + 1;

            if (maxAge != TimeSpan.Zero)
            {
                response.SetCacheControl(maxAge, this.CacheControlPrivate);
            }

            response.StatusCode = StatusCodes.Status206PartialContent;
            response.Headers["Content-Range"] = $"bytes {rangeStart}-{rangeEnd}/{fileLength}";
            response.Headers["Content-Length"] = length.ToString(CultureInfo.InvariantCulture);

            if (request.Method == "GET")
            {
                await response.SendFileAsync(fullPath, rangeStart, length, cancellationToken);
            }
        }
        else
        {
            if (maxAge != TimeSpan.Zero)
            {
                response.SetCacheControl(maxAge, this.CacheControlPrivate);
            }

            if (request.Headers.TryGetValue("Accept-Encoding", out var encoding))
            {
                var types = encoding.ToString().Split(",").Select(s => s.Trim()).ToArray();
                if (this.StaticBr && types.Contains("br") && File.Exists(fullPath + ".br"))
                {
                    response.Headers["Vary"] = "Accept-Encoding";
                    response.Headers["Content-Encoding"] = "br";
                    fullPath += ".br";
                    fileLength = new FileInfo(fullPath).Length;
                }
                else if (this.StaticGz && types.Contains("gzip") && File.Exists(fullPath + ".gz"))
                {
                    response.Headers["Vary"] = "Accept-Encoding";
                    response.Headers["Content-Encoding"] = "gzip";
                    fullPath += ".gz";
                    fileLength = new FileInfo(fullPath).Length;
                }
            }

            response.Headers["Content-Length"] = fileLength.ToString(CultureInfo.InvariantCulture);
            response.StatusCode = StatusCodes.Status200OK;
            if (request.Method == "GET")
            {
                await response.SendFileAsync(fullPath, 0, null, cancellationToken);
            }
        }

        return true;
    }

    /// <summary>
    /// Tries to get the range (in bytes) of the <see cref="HttpRequest"/>. This value is in <c>request.Headers["Range"]</c>.
    /// </summary>
    /// <param name="request">Request to get range from.</param>
    /// <param name="range">Range values if they are parsed. end will be -1 if there is no end.</param>
    /// <returns>True if the range is valid, false if not.</returns>
    /// <remarks>
    /// The returned values are exactly what is in the header and may have off by one errors if the caller does not use them
    /// correctly. See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range for more information.
    /// </remarks>
    private static bool TryGetRange(HttpRequest request, out (long start, long end) range)
    {
        range = (-1, -1);
        if (!request.Headers.TryGetValue("Range", out var rangeStringValues))
        {
            return false;
        }

        var rangeString = (string?)rangeStringValues;
        if (rangeString == null || !rangeString.StartsWith("bytes=", StringComparison.Ordinal))
        {
            return false;
        }

        rangeString = rangeString.Substring("bytes=".Length).Trim();
        var values = rangeString.Split(RangeSeparatorCharacter, StringSplitOptions.RemoveEmptyEntries);

        if (values.Length is 0 or > 2)
        {
            return false;
        }

        if (!long.TryParse(values[0], out var start))
        {
            return false;
        }

        var end = -1L;
        if (values.Length == 2 && !long.TryParse(values[1], out end))
        {
            return false;
        }

        range = (start, end);
        return true;
    }
}
