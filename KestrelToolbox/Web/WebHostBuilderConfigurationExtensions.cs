// <copyright file="WebHostBuilderConfigurationExtensions.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.IO.Compression;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace KestrelToolbox.Web.Extensions;

/// <summary>
/// Extensions methods to help configure <see cref="IWebHost"/>.
/// </summary>
/// <seealso cref="WebHostBuilderConfiguration"/>
public static class WebHostBuilderConfigurationExtensions
{
    /// <summary>
    /// A callback that will be invoked to dynamically select a server certificate. If SNI is not available then the name
    /// parameter will be null.
    /// </summary>
    /// <param name="context">Connection context.</param>
    /// <param name="name">Domain name from SNI.</param>
    /// <returns>Certificate to be used in this connection.</returns>
    public delegate X509Certificate2 SelectCertificateDelegate(ConnectionContext context, string? name);

    /// <summary>
    /// Creates a <see cref="IWebHostBuilder"/> that sets log level and removes header.
    /// </summary>
    /// <param name="minimumLogLevel">Kestrel built-in logging level.</param>
    /// <returns>New instance of <see cref="IWebHostBuilder"/>.</returns>
    /// <remarks>
    /// This is super basic, but is nice to have.
    /// </remarks>
    public static IWebHostBuilder CreateDefaultBuilder(LogLevel minimumLogLevel = LogLevel.Warning)
    {
        return WebHost
            .CreateDefaultBuilder()
            .ConfigureLogging(l => l.SetMinimumLevel(minimumLogLevel))
            .ConfigureKestrel(options => options.AddServerHeader = false);
    }

    /// <summary>
    /// Configures <see cref="IWebHost"/> to use http port.
    /// </summary>
    /// <param name="builder">The builder to configure.</param>
    /// <param name="port">Http port number.</param>
    /// <returns><paramref name="builder"/>.</returns>
    public static IWebHostBuilder ConfigureHttp(this IWebHostBuilder builder, int port)
    {
        return builder.ConfigureKestrel(options => options.ListenAnyIP(port));
    }

    /// <summary>
    /// Configures <see cref="IWebHost"/> to use https.
    /// </summary>
    /// <param name="builder">The builder to configure.</param>
    /// <param name="port">Https port number.</param>
    /// <param name="selectCertificate">Certificate selection callback to choose new certs.</param>
    /// <returns><paramref name="builder"/>.</returns>
    public static IWebHostBuilder ConfigureHttps(
        this IWebHostBuilder builder,
        int port,
        SelectCertificateDelegate selectCertificate
    )
    {
        return builder.ConfigureKestrel(options =>
            options.ListenAnyIP(
                port,
                listenOptions =>
                {
                    _ = listenOptions.UseHttps(httpsOptions =>
                    {
                        httpsOptions.SslProtocols =
                            System.Security.Authentication.SslProtocols.Tls12
                            | System.Security.Authentication.SslProtocols.Tls13;

                        var certificateSelector =
                            (Func<ConnectionContext?, string?, X509Certificate2?>)
                                Delegate.CreateDelegate(
                                    typeof(Func<ConnectionContext?, string?, X509Certificate2?>),
                                    selectCertificate.Target,
                                    selectCertificate.Method
                                );
                        httpsOptions.ServerCertificateSelector = certificateSelector;
                    });
                }
            )
        );
    }

    /// <summary>
    /// Configures <see cref="IWebHost"/> to add response compression.
    /// </summary>
    /// <param name="services">The services to configure.</param>
    /// <param name="mimeTypes">Mime types that should be compressed using this method.</param>
    /// <param name="compressionLevel">Compression level to use.</param>
    /// <returns><paramref name="services"/>.</returns>
    /// <remarks>
    /// This call enables both brotli and gzip.
    ///
    /// The <see cref="IWebHostBuilder"/> should also configure the <see cref="IApplicationBuilder"/> to <c>UseResponseCompression</c>.
    /// This should be one of the first things configured.
    /// </remarks>
    public static IServiceCollection AddCompression(
        this IServiceCollection services,
        IEnumerable<string> mimeTypes,
        CompressionLevel compressionLevel = CompressionLevel.Optimal
    )
    {
        _ = services.AddResponseCompression(options =>
        {
            options.EnableForHttps = true;
            options.Providers.Add<BrotliCompressionProvider>();
            options.Providers.Add<GzipCompressionProvider>();
            options.MimeTypes = mimeTypes;
        });

        _ = services.Configure<BrotliCompressionProviderOptions>(options => options.Level = compressionLevel);
        _ = services.Configure<GzipCompressionProviderOptions>(options => options.Level = compressionLevel);
        return services;
    }

    /// <summary>
    /// Configure <see cref="IWebHost"/> with route information.
    /// </summary>
    /// <param name="app">App to configure.</param>
    /// <param name="routes">Collection of routes to use.</param>
    /// <param name="requireAuthorization">A value indicating whether or not to default all routes to use authentication.</param>
    /// <returns><paramref name="app"/>.</returns>
    /// <remarks>
    /// These routes are usually generated but can easily be hand written.
    ///
    /// <paramref name="requireAuthorization"/> can be overriden on a per route basis using <see cref="AllowAnonymousAttribute"/> or setting
    /// <c>AllowAnonymous</c> to true in <see cref="RouteInfo"/>.
    /// </remarks>
    public static IApplicationBuilder UseRouteInfo(
        this IApplicationBuilder app,
        IEnumerable<RouteInfo> routes,
        bool requireAuthorization = false
    )
    {
        return app.UseEndpoints(endpoints =>
        {
            foreach (var route in routes)
            {
                var callback = route.Callback;
                var endpointBuilder = endpoints.MapMethods(route.Pattern, route.Methods, callback);

                if (route.AuthorizationPolicies != null)
                {
                    _ = endpointBuilder.RequireAuthorization(route.AuthorizationPolicies);
                }
                else if (requireAuthorization && !route.AllowAnonymous)
                {
                    _ = endpointBuilder.RequireAuthorization();
                }
            }
        });
    }
}
