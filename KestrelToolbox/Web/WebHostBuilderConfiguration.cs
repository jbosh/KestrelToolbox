// <copyright file="WebHostBuilderConfiguration.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace KestrelToolbox.Web;

/// <summary>
/// Helper class and methods to help configure <see cref="IWebHost"/>.
/// </summary>
public class WebHostBuilderConfiguration
{
    /// <summary>
    /// Delegate for configuring additional services for the host or web application. This occurs after KestrelToolbox services have been
    /// configured.
    /// </summary>
    /// <param name="services">Services to configure.</param>
    public delegate void ConfigureServicesDelegate(IServiceCollection services);

    /// <summary>
    /// Delegate for configuring the application. This occurs after KestrelToolbox initialization is setup (exception page, routing, etc..) but
    /// before the routing or auth callback is installed. You can use this to add custom authentication.
    /// </summary>
    /// <param name="app">App to configure.</param>
    public delegate void ConfigureApplicationDelegate(IApplicationBuilder app);

    /// <summary>
    /// Gets or sets port to use for http. If not set, default for Kestrel is used.
    /// </summary>
    public int? HttpPort { get; set; }

    /// <summary>
    /// Gets or sets port to use for https/http2. If not set, https is unavailable. If this value is set, <see cref="SelectCertificate"/>
    /// must not be null.
    /// </summary>
    public int? HttpsPort { get; set; }

    /// <summary>
    /// Gets or sets a callback that is called on connection initialization to get the certificate a server will use.
    /// </summary>
    public WebHostBuilderConfigurationExtensions.SelectCertificateDelegate? SelectCertificate { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether to enable compression for built-in compression types.
    /// </summary>
    public bool EnableCompression { get; set; } = true;

    /// <summary>
    /// Gets or sets the set mimetypes that are compressed. The default for this list is
    /// <see cref="ResponseCompressionDefaults.MimeTypes" />.
    /// </summary>
    public HashSet<string> CompressionMimeTypes { get; set; } = new(ResponseCompressionDefaults.MimeTypes);

    /// <summary>
    /// Gets or sets kestrel built-in logging level.
    /// </summary>
    public LogLevel MinimumLogLevel { get; set; } = LogLevel.Warning;

    /// <summary>
    /// Gets or sets a value indicating whether built-in developer exception page. This setting
    /// should not be enabled in production builds.
    /// </summary>
    public bool UseDeveloperExceptionPage { get; set; }

    /// <summary>
    /// Gets or sets callback for handling exceptions. Supersedes UseDeveloperExceptionPage if it is set.
    /// </summary>
    public RequestDelegate? ExceptionHandler { get; set; }

    /// <summary>
    /// Gets or sets a callback for configuring Kestrel.
    /// </summary>
    public Action<KestrelServerOptions>? ConfigureKestrel { get; set; }

    /// <summary>
    /// Gets or sets the callback that should occur to configure services for server startup.
    /// </summary>
    public ConfigureServicesDelegate? ConfigureServices { get; set; }

    /// <summary>
    /// Gets or sets the callback that should occur to configure app for server startup.
    /// </summary>
    public ConfigureApplicationDelegate? ConfigureApp { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether routes require authorization by default.
    /// </summary>
    /// <remarks>
    /// If this value is true, <see cref="AllowAnonymousAttribute"/> is required to access the route anonymously or <see cref="AuthorizeAttribute"/> can be used
    /// to access using authentication policies provided. The default authentication provider will be used if neither are provided.
    /// </remarks>
    public bool RequireAuthorization { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether websocket support is enabled.
    /// </summary>
    public bool UseWebSockets { get; set; }

    /// <summary>
    /// Gets or sets routes that will be used in server.
    /// </summary>
    public List<RouteInfo> Routes { get; set; } = new();

    /// <summary>
    /// Adds a collection of routes to <see cref="Routes"/>.
    /// </summary>
    /// <param name="routes">The routes to add.</param>
    /// <remarks>
    /// This method is shorthand for <c><see cref="Routes"/>.AddRange(<paramref name="routes"/>)</c>.
    /// </remarks>
    public void AddRoutes(IEnumerable<RouteInfo> routes) => this.Routes.AddRange(routes);

    /// <summary>
    /// Initializes a new instance of the <see cref="IWebHost"/> class.
    /// </summary>
    /// <returns>New instance of <see cref="IWebHost"/>.</returns>
    public IWebHost CreateWebHost()
    {
        var webHostBuilder = WebHostBuilderConfigurationExtensions.CreateDefaultBuilder(this.MinimumLogLevel);
        if (this.HttpPort.HasValue)
        {
            _ = webHostBuilder.ConfigureHttp(this.HttpPort.Value);
        }

        if (this.HttpsPort.HasValue)
        {
            if (this.SelectCertificate == null)
            {
#pragma warning disable S3928
                throw new ArgumentNullException(
                    nameof(this.SelectCertificate),
                    $"{nameof(this.SelectCertificate)} must not be null if {nameof(this.HttpsPort)} is set."
                );
#pragma warning restore S3928
            }

            _ = webHostBuilder.ConfigureHttps(this.HttpsPort.Value, this.SelectCertificate);
        }

        if (this.ConfigureKestrel != null)
        {
            _ = webHostBuilder.ConfigureKestrel(options => this.ConfigureKestrel(options));
        }

        return webHostBuilder
            .ConfigureServices(services =>
            {
                if (this.EnableCompression)
                {
                    _ = services.AddCompression(this.CompressionMimeTypes.ToArray());
                }

                this.ConfigureServices?.Invoke(services);
            })
            .Configure(app =>
            {
                if (this.EnableCompression)
                {
                    _ = app.UseResponseCompression();
                }

                if (this.ExceptionHandler != null)
                {
                    _ = app.UseExceptionHandler(err =>
                        err.Use((HttpContext context, Func<Task> next) => this.ExceptionHandler(context))
                    );
                }
                else if (this.UseDeveloperExceptionPage)
                {
                    _ = app.UseDeveloperExceptionPage();
                }

                if (this.UseWebSockets)
                {
                    var options = new WebSocketOptions();
                    _ = app.UseWebSockets(options);
                }

                _ = app.UseRouting();

                this.ConfigureApp?.Invoke(app);

                _ = app.UseRouteInfo(this.Routes);
            })
            .Build();
    }
}
