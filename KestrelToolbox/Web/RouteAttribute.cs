// <copyright file="RouteAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using KestrelToolbox.Web.Extensions;

namespace KestrelToolbox.Web;

/// <summary>
/// Specifies a route that <see cref="WebHostBuilderConfigurationExtensions"/> will provide.
/// </summary>
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class RouteAttribute : Attribute
{
    /// <summary>
    /// Gets URL that you want to call this method with.
    /// Some examples:
    /// - /hello - Matches any path that starts with exactly `/hello`
    /// - /hello:{name:alpha} - Matches any path that starts with `/hello/[a-zA-Z]+` and passes value to `name` parameter.
    /// - /hello/{*world} - Matches any path that matches `/hello/[^/]*` and passes the value to `world` parameter.
    /// - /hello/{**worlds} - Matches any path that matches `/hello/.*` and passes the value to `worlds` parameter.
    ///
    /// You can find more information about paths at https://learn.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-8.0#route-constraints.
    /// </summary>
    public string Pattern { get; }

    /// <summary>
    /// Gets a list of methods that this route will accept.
    /// </summary>
    public string[] Methods { get; }

    /// <inheritdoc />
    public override string ToString() => this.Pattern;

    /// <summary>
    /// Initializes a new instance of the <see cref="RouteAttribute"/> class.
    /// </summary>
    /// <param name="pattern">
    /// URL that you want to call this method with.
    /// Some examples:
    /// - /hello - Matches any path that starts with exactly `/hello`
    /// - /hello:{name:alpha} - Matches any path that starts with `/hello/[a-zA-Z]+` and passes value to `name` parameter.
    /// - /hello/{*world} - Matches any path that matches `/hello/[^/]*` and passes the value to `world` parameter.
    /// - /hello/{**worlds} - Matches any path that matches `/hello/.*` and passes the value to `worlds` parameter.
    ///
    /// You can find more information about paths at https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-5.0#route-constraint-reference.
    /// </param>
    /// <param name="methods">Comma separated list of HEAD, GET, PUT, DELETE. Default is GET.</param>
    public RouteAttribute(string pattern, string? methods = null)
    {
        this.Pattern = pattern;
        this.Methods =
            methods == null
                ? new[] { "GET" }
                : methods.Split(',').Select(m => m.Trim().ToUpperInvariant()).Where(m => m.Length != 0).ToArray();
    }
}
