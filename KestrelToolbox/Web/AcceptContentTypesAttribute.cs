// <copyright file="AcceptContentTypesAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Immutable;

namespace KestrelToolbox.Web;

/// <summary>
/// Specifies the content types that a route will accept.
/// </summary>
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class AcceptContentTypesAttribute : Attribute
{
    /// <summary>
    /// Gets the list of names that a data field can be.
    /// </summary>
    public ImmutableHashSet<string> Types { get; }

    /// <inheritdoc />
    public override string ToString() => $"Name: {string.Join(", ", this.Types)}";

    /// <summary>
    /// Initializes a new instance of the <see cref="AcceptContentTypesAttribute"/> class.
    /// </summary>
    /// <param name="types">Array of case-sensitive content types that are accepted by the endpoint.</param>
    public AcceptContentTypesAttribute(params string[] types)
    {
        this.Types = types.ToImmutableHashSet();
    }
}
