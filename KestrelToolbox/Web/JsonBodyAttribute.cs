// <copyright file="JsonBodyAttribute.cs" company="KestrelToolbox">
// Copyright (c) KestrelToolbox. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KestrelToolbox.Web;

/// <summary>
/// Specifies this parameter should be parsed from body as json type.
/// </summary>
[AttributeUsage(AttributeTargets.Parameter)]
public class JsonBodyAttribute : Attribute
{
    /// <summary>
    /// Gets or sets the mime type accepted for requests with this attribute. The default is "application/json".
    /// </summary>
    public string MimeType { get; set; } = "application/json";

    /// <summary>
    /// Gets or sets the parse method (either fully qualified or reachable by this method) to use if custom parsing is needed.
    /// </summary>
    public string? ParseMethod { get; set; }
}
